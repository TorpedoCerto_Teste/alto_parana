$(function () {
    $("input[name='year']").TouchSpin({});

    $(".copy").click(function () {
        var field = $(this).val();
        for (i = parseInt(field); i < 13; i++) {
            $('#flag_' + i).val($('#flag_' + field).val());
            $('#increase_' + i).val($('#increase_' + field).val());
            $('#month_' + i).removeClass('label-success').removeClass('label-warning').removeClass('label-danger');
            if ($('#flag_' + field).val() == 'VERDE') {
                $('#month_' + i).addClass('label-success');
            } else if ($('#flag_' + field).val() == 'AMARELA') {
                $('#month_' + i).addClass('label-warning');
            } else {
                $('#month_' + i).addClass('label-danger');
            }


        }
    });

    $(".select").change(function () {
        var id = $(this).attr('id');
        var spl = id.split("_");
        $('#month_' + spl[1]).removeClass('label-success').removeClass('label-warning').removeClass('label-danger');
        if ($(this).val() == 'VERDE') {
            $('#month_' + spl[1]).addClass('label-success');
        } else if ($(this).val() == 'AMARELA') {
            $('#month_' + spl[1]).addClass('label-warning');
        } else {
            $('#month_' + spl[1]).addClass('label-danger');
        }
    });

    $('#year').blur(function () {
        make_ajax();
    });
    $('.bootstrap-touchspin-up').click(function () {
        make_ajax();
    });
    $('.bootstrap-touchspin-down').click(function () {
        make_ajax();
    });




});


function make_ajax() {
    for (var i = 1; i < 13; i++) {
        $('#increase_' + i).val('0.00').addClass('spinner');
    }
    $.get($('#base_url').val() + $('#year').val(),
            function (data, status) {
                data = JSON.parse(data);
                for (var k in data) {
                    if (data.hasOwnProperty(k)) {
                        $('#increase_' + k).val(data[k]['increase']).removeClass('spinner');
                        $('#flag_' + k).val(data[k]['flag']);
                        $('#month_' + k).removeClass('label-success').removeClass('label-warning').removeClass('label-danger');
                        $('#month_' + k).addClass('label-' + data[k]['color']);
                    }
                }
            });
    return true;

}