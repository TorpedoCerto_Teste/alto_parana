$(function () {
    $('#btn_new').click(function() {
        $('#div_create').toggle();
    });
    
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    
    $('#current').click(function() {
        $('#end_date').prop('disabled', $('#current').is(":checked"));
    });
    
    if ($('#div_captive_situation').length) {
        $('#div_captive_situation').click(function () {
            $('#plus_captive_situation').toggle('slow');
        });
    }
        
    
});