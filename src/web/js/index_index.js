$('.responsive-data-table').DataTable({
    "PaginationType": "bootstrap",
    responsive: true,
    dom: '<"tbl-top clearfix"lfr>,t,<"tbl-footer clearfix"<"tbl-info pull-left"i><"tbl-pagin pull-right"p>>'
});

$(document).ready(function() {
    $('tr[data-href]').click(function() {
        window.location.href = $(this).data('href');
    });
});