var app = {
    count: 1,
    init: () => {
        app.initTouchSpin();
        app.initPicker();
        app.ckeckListener();
        app.addNew();
    },

    initTouchSpin: () => {
        $(".year").TouchSpin({
            min: 1980,
            max: 2050,
            step: 1,
            decimals: 0,
            boostat: 10,
            maxboostedstep: false,
            mousewheel: true,
        });        
    },
    
    initPicker: () => {
        $('.default-date-picker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            gotoCurrent: true
        });
    },

    ckeckListener: () => {
        $('#set_origin').on('change', function () {
            $('#div_fk_agent').removeClass('hidden');
            if (!$(this).is(':checked')) {
                $('#div_fk_agent').addClass('hidden');
            }
        });        
    },

    addNew: () => {
        $('.add_new').on('click', function (e) {
            e.preventDefault();
            var lastInput = $(".year").last();
            var currentYear = parseInt(lastInput.val());
            currentYear = currentYear+1;
            var model = `
                <tr id="year`+app.count+`">
                    <td>
                        <input type="text" id="year" name="year[]" class="form-control year" alt="year" value="`+currentYear+`"
                        />
                    </td>
                    <td>
                        <input class="form-control" id="value" name="value[]" type="text" alt="valor2">
                    </td>
                    <td>
                        <div class="btn btn-danger" onclick="return app.removeItem('year`+app.count+`')">
                            <i class="fa fa-close"></i>
                        </div>
                    </td>
                </tr>            
            `;
            $('.myTable tbody').append(model);
            app.initTouchSpin();
            var input = $('#value');
            $('input:text').setMask();
            app.count++;
        });
    },

    removeItem: (tr) => {
        $('#'+tr).remove();
        return false;
    }
    
}


$(document).ready(() => {
    app.init();
})
