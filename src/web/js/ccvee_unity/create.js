$(function () {
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#unity_apportionment').change(function () {
        if ($('#unity_apportionment').val() == "CONSUMO") {
            $('#div_volumn_mwm').hide();
        } else {
            $('#div_volumn_mwm').show();
        }
        
        //persiste a escolha no banco -> tabela: ccvee_general
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "ccvee_general/ajax_change_unity_apportionment",
            data: {
                unity_apportionment: $("#unity_apportionment").val(),
                pk_ccvee_general: $("#pk_ccvee_general").val()
            }
        }).done(function (value) {
            return true;
        });
        
    });


    $('#unity_differentiated_validate').click(function () {
        var checked = 0;
        if ($("#unity_differentiated_validate").is(":checked")) {
            $('#div_vicence').show();
            checked = 1;
        } else {
            $('#div_vicence').hide();
        }
        
        //persiste a escolha no banco -> tabela: ccvee_general
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "ccvee_general/ajax_change_unity_differentiated_validate",
            data: {
                unity_differentiated_validate: checked,
                pk_ccvee_general: $("#pk_ccvee_general").val()
            }
        }).done(function (value) {
            return true;
        });
        
    });


});