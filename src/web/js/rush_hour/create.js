$(function () {
    $("#current").click(function () {
        if ($("#current").is(":checked")) {
            $("#end_date").attr('disabled','disabled');
        } else {
            $("#end_date").removeAttr('disabled');
        }
    });
});