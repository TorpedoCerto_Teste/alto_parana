$("input[name='year']").TouchSpin({});
$("#fk_agent").change(function () {
    var fk_agent = $(this).val();
    find_unities(fk_agent);
    find_devec_customer();
});

$("#fk_unity").change(function () {
    find_devec_customer();
});

$("#month").change(function () {
    find_devec_customer();
});

$("#year").blur(function () {
    find_devec_customer();
});

$(".bootstrap-touchspin-down").click(function () {
    find_devec_customer();
});

$(".bootstrap-touchspin-up").click(function () {
    find_devec_customer();
});

function find_devec_customer() {
    var fk_agent = $("#fk_agent").val();
    var fk_unity = $("#fk_unity").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "" && fk_unity != "") {
        $("#upsert").val("false")
        $("#frm_devec_customer").submit();
    }
}

function find_unities(fk_agent) {
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity/ajax_fetch_unity_from_agent",
        dataType: "json",
        data: {
            fk_agent: fk_agent
        },
    }).done(function (values) {
        fill_select(values);
    });
}

function fill_select(values) {
    $('#fk_unity').children().remove().end();
    var selected = values.length ===1 ? 'selected' : '';
    $(values).each(function (i, val) {
        $('#fk_unity').append('<option value="'+val.pk_unity+'" '+selected+'>'+val.community_name+'</option>') ;
    });
}

$("#all").click(function () {
    var checked = $(this).is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});
