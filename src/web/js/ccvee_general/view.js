//TAB GERAL
$(function () {
    $('.wysihtml5').wysihtml5({
        "stylesheets": false
    });

//    $('.default-date-picker').datepicker({
//        format: 'dd/mm/yyyy',
//        autoclose: true
//    });

});

$('.faturamento').DataTable({
    "PaginationType": "bootstrap",
    responsive: true,
    dom: '<"tbl-top clearfix"lfr>,t,<"tbl-footer clearfix"<"tbl-info pull-left"i><"tbl-pagin pull-right"p>>',
    "oLanguage": {
        "sLengthMenu": "_MENU_ itens por página",
        "sZeroRecords": "Nenhum registro",
        "sInfo": "_START_ até _END_ para o total de _TOTAL_",
        "sInfoEmpty": "Nenhum registro",
        "sInfoFiltered": "(filtrados de _MAX_ itens)",
        "sSearch": "Pesquisar"
    },
    "aaSorting": [[0, "desc"]]    
}); 

$('#source').change(function () {
    if ($(this).val() === "RFQ") {
        $('#div_rfq').show();
    } else {
        $('#div_rfq').hide();
    }
});

$(".default-date-picker").blur(function () { 
    check_date();
    generate_volume();
});

function check_date() {
    var supply_start = $("#supply_start").val();
    var supply_end   = $("#supply_end").val();
    
    if (get_month_year($.trim(supply_start)) === get_month_year($.trim(supply_end)) ) {
        $('#differentiated_volume_period').prop( "checked", true ).attr("disabled", "disabled");
    }
    else {
        $('#differentiated_volume_period').removeAttr("disabled");
    }
    
}

function get_month_year(human_date) {
    var arr_human_date = human_date.split("/");
    return arr_human_date[1]+""+arr_human_date[2];
}

function format_date(dateTime) {
    var retrn = "sem data informada";
    if (dateTime) {
        var date = new Date(dateTime);
        retrn = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    }
    return retrn;
}

function check_differentiated_volume() {
    if (!$("#differentiated_volume_period").is(":checked")) {
        $("#unit_measurement").val("MWm").attr("disabled", "disabled");
    }
    else {
        $("#unit_measurement").removeAttr("disabled");
    }
}

//TAB ENERGIA
$(function () {
    check_date();
    check_differentiated_volume();
//    generate_volume();
    
    $("input[name='seasonal_inferior']").TouchSpin({
        min: -100,
        max: 0,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $("input[name='seasonal_superior']").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

    $("input[name='flexibility_inferior']").TouchSpin({
        min: -100,
        max: 0,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $("input[name='flexibility_superior']").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

});


$('#differentiated_volume_period').click(function () {
    generate_volume();
});

$('.calcular').click(function () {
    generate_volume();
});

$("#seasonality").click(function () {
    generate_seasonality();
});

$("#flexibility").click(function () {
    process_flexibility();
});


$("input[name=modulation_radio]").click(function () {
    process_modulation();
});

$("#unit_measurement").change(function () {
    change_unit_measurement($(this).val());
});

$("#losses_type").change(function () {
    if ($("#losses_type").val() == "valor_unico") {
        $("#div_losses_unique").show();
    } else {
        $("#div_losses_unique").hide();
    }
    generate_volume();
});

//verificar data inicial e final do campo de fornecimento e gerar os campos para períodos
function generate_volume() {
    check_differentiated_volume();
    
    var start = $('#supply_start').val();
    var end = $('#supply_end').val();
    var pk_ccvee_energy = $('#pk_ccvee_energy').val();

    if (start == "") {
        $('#supply_start').addClass("error");
    } else {
        $('#supply_start').removeClass("error");
    }

    if (end == "") {
        $('#supply_end').addClass("error");
    } else {
        $('#supply_end').removeClass("error");
    }

    if (start != "" && end != "") {
        var diff = $("#differentiated_volume_period").is(":checked");
        if (diff) {
            $.ajax({
                method: "POST",
                url: $('#base_url').val() + "ccvee_energy/ajax_proccess_supply",
                data: {
                    supply_start: start,
                    supply_end: end,
                    pk_ccvee_energy: pk_ccvee_energy
                }
            }).done(function (value) {
                generate_table(value);
            });

        } else {
            generate_table(null);
        }

    }
}

function generate_table(value) {
    var table = "";

    var diff = $("#differentiated_volume_period").is(":checked");

    if (diff) {
        table += "<table class=\"table responsive-data-table data-table\">" +
                "<thead>" +
                "<tr>" +
                "<th>Período</th>" +
                "<th>Previsto</th>" +
                "<th>Sazonalizado</th>" +
                "<th>Ajustado</th>";
        if ($("#losses_type").val() === "manual") {
            table += "<th>% Perdas</th>";
        }
        table += "<th>% Atendimento</th>";
        table +=
                "<th>Copiar</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>";

        var json = $.parseJSON(value);
        $('#total_months').val(json.length);
        $(json).each(function (i, val) {
            table += "<tr>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"periodo" + i + "\" name=\"volume[" + i + "][periodo]\" class=\"form-control\" value=\"" + val.periodo + "\" disabled >" +
                    "<input type=\"hidden\" placeholder=\"\" id=\"period" + i + "\" name=\"volume[" + i + "][period]\" class=\"form-control\" value=\"" + val.periodo + "\"    ></td>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"forecast" + i + "\" name=\"volume[" + i + "][forecast]\" class=\"form-control\" value=\"" + parseFloat(val.forecast).toFixed(6) + "\" alt=\"valor6\" ></td>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"seasonal" + i + "\" name=\"volume[" + i + "][seasonal]\" class=\"form-control\" value=\"" + parseFloat(val.seasonal).toFixed(6) + "\" alt=\"valor6\" ></td>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"adjusted" + i + "\" name=\"volume[" + i + "][adjusted]\" class=\"form-control\" value=\"" + parseFloat(val.adjusted).toFixed(6) + "\" alt=\"valor6\" ></td>";
            if ($("#losses_type").val() === "manual") {
                table += "<td><input type=\"text\" placeholder=\"\" id=\"losses" + i + "\" name=\"volume[" + i + "][losses]\" class=\"form-control\" value=\"" + val.losses + "\" alt=\"valor2\" ></td>";
            }
            table += "<td><input type=\"text\" placeholder=\"\" id=\"attendance" + i + "\" name=\"volume[" + i + "][attendance]\" class=\"form-control\" value=\"" + parseFloat(val.attendance).toFixed(2) + "\" alt=\"valor2\" ></td>";

            table +=
                    "<td>" +
                    "<button data-toggle=\"button\" class=\"btn btn-sm btn-default copy\" value=\"copy" + i + "\" onclick=\"copy_values(" + i + ")\">" +
                    "<i class=\"fa fa-copy\"></i>" +
                    "</button>" +
                    "</td>" +
                    "</tr>";
        });
        table += " </tbody>" +
                "</table>";

    } else {
        table += "<table class=\"table responsive-data-table data-table\">" +
                "<thead>" +
                "<tr>" +
                "<th>Previsto</th>" +
                "<th>Sazonalizado</th>" +
                "<th>Ajustado</th>";
        if ($("#losses_type").val() === "manual") {
            table += "<th>% Perdas</th>";
        }
        table += "<th>% Atendimento</th>";
        table +=
                "</tr>" +
                "</thead>" +
                "<tbody>";
        table += "<tr>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"forecast\" name=\"forecast\" class=\"form-control\" value=\""+$("#forecast").val()+"\" alt=\"valor6\" ></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"seasonal\" name=\"seasonal\" class=\"form-control\" value=\""+$("#seasonal").val()+"\" alt=\"valor6\" ></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"adjusted\" name=\"adjusted\" class=\"form-control\" value=\""+$("#adjusted").val()+"\" alt=\"valor6\" ></td>";
        if ($("#losses_type").val() === "manual") {
            table += "<td><input type=\"text\" placeholder=\"\" id=\"losses\" name=\"losses\" class=\"form-control\" value=\"\" alt=\"valor2\" ></td>";
        }
        table += "<td><input type=\"text\" placeholder=\"\" id=\"attendance\" name=\"attendance\" class=\"form-control\" value=\""+$("#attendance").val()+"\" alt=\"valor6\" ></td>";
        table += "</tr>" +
                "</tbody>" +
                "</table>";
    }
    $('#volumes').html(table);
    $('input:text').setMask();
}

function copy_values(position) {
    var total_months = $("#total_months").val();
    var forecast = $("#forecast" + position).val();
    var seasonal = $("#seasonal" + position).val();
    var adjusted = $("#adjusted" + position).val();
    var losses = $("#losses" + position).val();
    var attendance = $("#attendance" + position).val();

    for (var i = position; i <= total_months; i++) {
        $("#forecast" + i).val(forecast);
        $("#seasonal" + i).val(seasonal);
        $("#adjusted" + i).val(adjusted);
        $("#losses" + i).val(losses);
        $("#attendance" + i).val(attendance);
    }
}

function generate_seasonality() {
    var seasonality = $("#seasonality").is(":checked");

    if (seasonality) {

        var start = $('#supply_start').val();
        var end = $('#supply_end').val();
        var pk_ccvee_energy = $('#pk_ccvee_energy').val();

        if (start == "") {
            $('#supply_start').addClass("error");
        } else {
            $('#supply_start').removeClass("error");
        }

        if (end == "") {
            $('#supply_end').addClass("error");
        } else {
            $('#supply_end').removeClass("error");
        }

        if (start != "" && end != "") {
            $.ajax({
                method: "POST",
                url: $('#base_url').val() + "ccvee_energy/ajax_proccess_seasonal",
                data: {
                    supply_start: start,
                    supply_end: end,
                    pk_ccvee_energy: pk_ccvee_energy
                }
            }).done(function (value) {
                generate_table_seasonal(value);
            });

            $("#seasonality_div").show();
        }




    } else {
        $("#seasonal_limit").html();
        $("#seasonality_div").hide();
    }
}

function generate_table_seasonal(value) {
    var table = "";
    table += "<table class=\"table responsive-data-table data-table\">" +
            "<thead>" +
            "<tr>" +
            "<th>Ano</th>" +
            "<th>Data limite para informar sazonalidade</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody>";

    var json = $.parseJSON(value);
    $('#total_months').val(json.length);
    $(json).each(function (i, val) {
        table += "<tr>" +
                "<td class=\"col-lg-2\">" +
                "<input type=\"text\"  placeholder=\"\" id=\"seasonal_limit_year_show\" name=\"seasonal_limit_year_show\" class=\"form-control\" value=\"" + val.year + "\" disabled=\"disabled\" >" +
                "<input type=\"hidden\" placeholder=\"\" id=\"seasonal_limit_year\" name=\"seasonal_limit[" + i + "][year]\" class=\"form-control\" value=\"" + val.year + "\" >" +
                "</td>" +
                "<td class=\"col-lg-4\">" +
                "<input type=\"text\" class=\"form-control default-date-picker\"  id=\"seasonal_limit_limit\" name=\"seasonal_limit[" + i + "][limit]\"  alt=\"date\" value=\"" + val.limit + "\">" +
                "</td>" +
                "</tr>";
    });
    table += " </tbody>" +
            "</table>";

    $("#seasonal_limit").html(table);
    $('input:text').setMask();
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

}

function process_flexibility() {
    var flexibility = $("#flexibility").is(":checked");
    if (flexibility) {
        $("#flexibility_div").show();
    } else {
        $("#flexibility_div").hide();
    }
}

function process_modulation() {
    var modulation = $("input[name=modulation_radio]:checked").val();
    if (modulation == "modulada") {
        $("#modulation_div").show();
    } else {
        $("#modulation_div").hide();
    }
}

function change_unit_measurement(unit) {
    var diff = $("#differentiated_volume_period").is(":checked");
    if (diff) {
        var start = $('#supply_start').val();
        var end = $('#supply_end').val();

        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "ccvee_energy/ajax_proccess_unit_measurement",
            dataType: "json",
            data: {
                supply_start: start,
                supply_end: end,
                unit_measurement: unit
            }
        }).done(function (value) {
            canculate_unit_measurement(value);
        });


    }
}

function canculate_unit_measurement(value) {
    var calc_forecast = 1.0;
    var calc_seasonal = 1.0;   
    var calc_adjusted = 1.0;
    var ret_forecast = 1.0;
    var ret_seasonal = 1.0;
    var ret_adjusted = 1.0;
    var unit_measurement = $("#unit_measurement").val();
    $(value).each(function (i, val) {
        var forecast = $("#forecast"+i).val().replace(",", ".");
        var seasonal = $("#seasonal"+i).val().replace(",", ".");
        var adjusted = $("#adjusted"+i).val().replace(",", ".");
        if (unit_measurement === "MWm") {
            calc_forecast = forecast/val.total;
            calc_seasonal = seasonal/val.total;
            calc_adjusted = adjusted/val.total;
        }
        else {
            calc_forecast = forecast*val.total;
            calc_seasonal = seasonal*val.total;
            calc_adjusted = adjusted*val.total;
        }
        ret_forecast = calc_forecast.toFixed(6);
        ret_seasonal = calc_seasonal.toFixed(6);
        ret_adjusted = calc_adjusted.toFixed(6);

        $("#forecast"+i).val(ret_forecast.replace(".",","));
        $("#seasonal"+i).val(ret_seasonal.replace(".",","));
        $("#adjusted"+i).val(ret_adjusted.replace(".",","));
    });
}

//TAB PAGAMENTOS
$('input[name=first_adjustment]').click(function () {
    var first_adjustment = $('input[name=first_adjustment]:checked').val();
    if (first_adjustment == 0) {
        $("#div_date_adjustment").show();
    } else {
        $("#div_date_adjustment").hide();
    }
});

$("input[name=different_price_period]").click(function () {
    var different_price_period = $('input[name=different_price_period]:checked').val();

    if (different_price_period == 0) {
        $("#div_not_different_price_period").show();
        $("#div_different_price_period").hide();
    } else {
        $("#div_not_different_price_period").hide();
        $("#div_different_price_period").show();
        //ajax para pegar a quantidade de campos referentes ao perído de fornecimento de energia
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "ccvee_payment/ajax_get_supply_period/" + $('#fk_ccvee_general').val(),
            data: {
                first_adjustment: $('input[name=first_adjustment]:checked').val(),
                date_adjustment: $('#date_adjustment').val(),
                date_adjustment_frequency: $('#date_adjustment_frequency').val()
            }
        }).done(function (value) {
            generate_table_payment(value);
        });


    }
});


function generate_table_payment(value) {
    var table = "";

    table += "  <table class=\"table responsive-data-table data-table\">" +
            "<thead>" +
            "<tr>" +
            "<td>Período</td>" +
            "<td>Previsto/Base</td>" +
            "<td>Corrigido/Reajustado</td>" +
            "<td>Ajustado Manualmente</td>" +
            "<td>Previsão de Preço Ajustado</td>" +
            "<td>Apontamento de Mês Reajuste</td>" +
            "<td>Copiar</td>" +
            "</tr>" +
            "</thead>" +
            "<tbody>";
    var json = $.parseJSON(value);
    $('#total_months').val(json.length);
    var checked = 0;
    $(json).each(function (i, val) {
        checked = parseInt(val.checked) === 1 ? "checked" : "";
        table += "<tr>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"period" + i + "\" name=\"payment_prices[" + i + "][period]\" class=\"form-control\" value=\"" + val.periodo + "\" ></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"provided_base" + i + "\" name=\"payment_prices[" + i + "][provided_base]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"fixed" + i + "\" name=\"payment_prices[" + i + "][fixed]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"manual_fixed" + i + "\" name=\"payment_prices[" + i + "][manual_fixed]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"provided_fixed" + i + "\" name=\"payment_prices[" + i + "][provided_fixed]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>" +
                "<td><input type=\"checkbox\" placeholder=\"\" id=\"point_monthly" + i + "\" name=\"payment_prices[" + i + "][point_monthly]\" class=\"form-control\" value=\"1\" " + checked + "></td>" +
                "<td><button data-toggle=\"button\" class=\"btn btn-sm btn-default copy\" value=\"copy" + i + "\" onclick=\"copy_values_payment(" + i + ")\"><i class=\"fa fa-copy\"></i></button></td>" +
                "</tr>";
    });
    table += "</tbody>" +
            "</table>";

    $('#table_payment_prices').html(table);
    $('input:text').setMask();
}


function copy_values_payment(position) {
    var total_months = $("#total_months").val();
    var provided_base = $("#provided_base" + position).val();
    var fixed = $("#fixed" + position).val();
    var manual_fixed = $("#manual_fixed" + position).val();
    var provided_fixed = $("#provided_fixed" + position).val();

    for (var i = position; i <= total_months; i++) {
        $("#total_months" + i).val(total_months);
        $("#provided_base" + i).val(provided_base);
        $("#fixed" + i).val(fixed);
        $("#manual_fixed" + i).val(manual_fixed);
        $("#provided_fixed" + i).val(provided_fixed);
    }
}


//TAB UNIDADES
$('#unity_apportionment').change(function () {
    if ($('#unity_apportionment').val() == "CONSUMO") {
        $('#div_volumn_mwm').hide();
    } else {
        $('#div_volumn_mwm').show();
    }

    //persiste a escolha no banco -> tabela: ccvee_general
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "ccvee_general/ajax_change_unity_apportionment",
        data: {
            unity_apportionment: $("#unity_apportionment").val(),
            pk_ccvee_general: $("#pk_ccvee_general").val()
        }
    }).done(function (value) {
        return true;
    });

});


$('#unity_differentiated_validate').click(function () {
    var checked = 0;
    if ($("#unity_differentiated_validate").is(":checked")) {
        $('#div_vicence').show();
        checked = 1;
    } else {
        $('#div_vicence').hide();
    }

    //persiste a escolha no banco -> tabela: ccvee_general
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "ccvee_general/ajax_change_unity_differentiated_validate",
        data: {
            unity_differentiated_validate: checked,
            pk_ccvee_general: $("#pk_ccvee_general").val()
        }
    }).done(function (value) {
        return true;
    });

});

$("#financial_index").change(function() {
    var financial_index = $(this).val();
    if (financial_index == "SEM_RE") {
        $("#div_financial_index").hide();
    }
    else {
        $("#div_financial_index").show();
    }
}); 

$("#all").click(function () {
    $(".chk_contact").prop("checked", !$(".chk_contact").prop("checked"));
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});