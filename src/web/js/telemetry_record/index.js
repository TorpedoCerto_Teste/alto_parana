$("input[name='year']").TouchSpin({});

$("#fk_gauge").change(function () {
    send_form();
});

$("#month").change(function () {
    send_form();
});

$("#year").blur(function () {
    send_form();
});

$(".bootstrap-touchspin-down").click(function () {
    send_form();
});

$(".bootstrap-touchspin-down").click(function () {
    send_form();
});

$("#btn-export").click(function() {
    $("#export_excel").val("true");
    send_form();
});

function send_form() {
    var month = $("#month").val();
    var year = $("#year").val();
    var fk_gauge = $("#fk_gauge").val();
    
    if (parseInt(month) > 0 && parseInt(year) > 0 && parseInt(fk_gauge) > 0) {
        $("#frm_telemetry_record").submit();
    }
}