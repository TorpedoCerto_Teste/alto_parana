$(".muda_data").click(function () {
    $("#event_date").val($(this).attr("date"));
    $("#frm_calendar").submit();
});


$(".chk").click(function () {
    var pk_calendar = $(this).val();
    var check = 0;
    if ($(this).is(":checked")) {
        $("#chk_c_" + pk_calendar).prop("checked", true);
        $("#chk_l_" + pk_calendar).prop("checked", true);
        check = 1;
    } else {
        $("#chk_c_" + pk_calendar).prop("checked", false);
        $("#chk_l_" + pk_calendar).prop("checked", false);
        check = 0;
    }

    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "calendar/ajax_set_done",
        dataType: "json",
        data: {
            pk_calendar: pk_calendar,
            check: check
        }
    }).done(function (value) {
    });

});