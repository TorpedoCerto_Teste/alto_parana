//mensal_por_unidade
//percentual_da_economia
//valor_sobre_a_energia
//valor_fechado
//mensal_por_contraparte
//valor_fixo_mais_percentual_de_economia

$(function () {

    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

});

$("#remuneration_type").change(function () {
    only_div_active($("#remuneration_type").val());
    

});

$("#adjustment_montly_manually").click(function() {
    if ($("#adjustment_montly_manually").is(":checked")) {
        $("#div_date_adjustment").show();
    }
    else {
        $("#div_date_adjustment").hide();
    }
});


$("#closed_installment").click(function () {
    if ($("#closed_installment").is(":checked")) {
        $("#div_closed_installment_quantity").show();
    }
    else {
        $("#div_closed_installment_quantity").hide();
    }
});

function only_div_active(div_name) {
    var options = [
        "mensal_por_unidade",
        "percentual_da_economia",
        "valor_sobre_a_energia",
        "valor_fechado",
        "mensal_por_contraparte",
        "valor_fixo_mais_percentual_de_economia"
    ];

    for (i = 0; i < options.length; i++) {
        $("#div_"+options[i]).hide();
    }
    $("#div_"+div_name).fadeIn("slow");
}

