$(function () {

    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

});
