$(function () {

    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#fk_agent_power_distributor').change(function () {
        var fk_agent = $(this).val();
        $('#btn_search').hide();
        if (parseInt(fk_agent) > 0) {
            $.get($('#base_url').val() + 'resolution/' + fk_agent,
                    function (data, status) {
                        data = JSON.parse(data);
                        $('#tbl_select tbody').empty();
                        for (var k in data) {
                            if (data.hasOwnProperty(k)) {
                                var rowContent =
                                        '<tr>' +
                                        '<td>' + data[k]['resolution'] + '</td>' +
                                        '<td>' + data[k]['date_resolution'] + '</td>' +
                                        '<td>' + data[k]['beginning_term'] + '</td>' +
                                        '<td>' + data[k]['end_term'] + '</td>' +
                                        '<td><button class="btn btn-success btn-xs btn_check" value="' + data[k]['pk_bank_charges'] + '" onclick="select_resolution(' + data[k]['pk_bank_charges'] + ')">' +
                                        '<i class="fa fa-check"></i>' + '</button>' +
                                        '</td>' +
                                        '</tr>';
                                $('#tbl_select tbody').append(rowContent);
                            }
                        }
                        $('#btn_search').show();
                    });
            return true;
        }
    });

    $('#btn_search').click(function () {
        $('#select').modal();
    });


});

function select_resolution(pk_bank_charges) {
    window.location.href = $('#base_url').val() + 'index/' + pk_bank_charges;
    $('#select').modal('hide');
}