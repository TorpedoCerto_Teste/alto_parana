$('.responsive-data-table').DataTable({
    "PaginationType": "bootstrap",
    responsive: true,
    dom: '<"tbl-top clearfix"lfr>,t,<"tbl-footer clearfix"<"tbl-info pull-left"i><"tbl-pagin pull-right"p>>',
    "oLanguage": {
        "sLengthMenu": "_MENU_ itens por página",
        "sZeroRecords": "Nenhum registro",
        "sInfo": "_START_ até _END_ para o total de _TOTAL_",
        "sInfoEmpty": "Nenhum registro",
        "sInfoFiltered": "(filtrados de _MAX_ itens)",
        "sSearch": "Pesquisar"
    },
    "aaSorting": [[0, "desc"]],
    "aoColumns": [
        {
            "bSearchable": false,
            "bVisible": false,
        },
        /* ano/mes*/
        {
            "iDataSort": 0 //ordena pela coluna anterior
        },
        null,
        null,
        null
    ]
    
});
