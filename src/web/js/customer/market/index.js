$(function () {
        var _2016 = [
            [0, 44.4],
            [1, 50.89],
            [2, 58.27],
            [3, 57.55],
            [4, 56.66],
            [5, 56.04],
            [6, 51.49],
            [7, 45.99],
            [8, 40.13],
            [9, 34.76],
            [10, 33.4],
            [11, 33.72]

        ];
        var _2017 = [
            [11, ],
            [10, ],
            [9, ],
            [8, ],
            [7, ],
            [6, ],
            [5, ],
            [4, ],
            [3, ],
            [2, ],
            [1, 40.23],
            [0, 37.43]
        ];
        var ticks = [
            [0, "Jan"],
            [1, "Fev"],
            [2, "Mar"],
            [3, "Abr"],
            [4, "Mai"],
            [5, "Jun"],
            [6, "Jul"],
            [7, "Ago"],
            [8, "Set"],
            [9, "Out"],
            [10, "Nov"],
            [11, "Dez"]
        ];
        var data = [{
        }, {
            label: "2016",
            data: _2016,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }, {
            label: "2017",
            data: _2017,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }];

        $.plot($("#combine-chart #combine-chart-container"), data, {
            series: {
                lines: {
                    show: true
                }
            },
            valueLabels: {
                show: true
            },
            xaxis: {
                ticks: ticks
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            colors: ["#975197", "#6bd3f3"],
            tooltip: true,
            tooltipOpts: {
                content: "%s %y.2",
                defaultTheme: false
            }


        });
        
//ena
        var _2016 = [
            [0, 127.05],
            [1, 85.65],
            [2, 98.48],
            [3, 71.56],
            [4, 89.61],
            [5, 120.59],
            [6, 91.55],
            [7, 103.97],
            [8, 93.79],
            [9, 84.93],
            [10, 88.96],
            [11, 78.59]

        ];
        var _2017 = [
            [11, ],
            [10, ],
            [9, ],
            [8, ],
            [7, ],
            [6, ],
            [5, ],
            [4, ],
            [3, ],
            [2, ],
            [1, 70.25],
            [0, 68.88]
        ];
        var ticks = [
            [0, "Jan"],
            [1, "Fev"],
            [2, "Mar"],
            [3, "Abr"],
            [4, "Mai"],
            [5, "Jun"],
            [6, "Jul"],
            [7, "Ago"],
            [8, "Set"],
            [9, "Out"],
            [10, "Nov"],
            [11, "Dez"]
        ];
        var data = [{
        }, {
            label: "2016",
            data: _2016,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }, {
            label: "2017",
            data: _2017,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }];

        $.plot($("#ena"), data, {
            series: {
                lines: {
                    show: true
                }
            },
            valueLabels: {
                show: true
            },
            xaxis: {
                ticks: ticks
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            colors: ["#975197", "#6bd3f3"],
            tooltip: true,
            tooltipOpts: {
                content: "%s %y.2",
                defaultTheme: false
            }
        });
        
//carga        
        var _2016 = [
            [0, 36566.19],
            [1, 39533],
            [2, 38473.38],
            [3, 38319.29],
            [4, 34011.81],
            [5, 33329.67],
            [6, 33241.34],
            [7, 34363.23],
            [8, 34836.73],
            [9, 35195.9],
            [10, 34814.24],
            [11, 35817.75]

        ];
        var _2017 = [
            [11, ],
            [10, ],
            [9, ],
            [8, ],
            [7, ],
            [6, ],
            [5, ],
            [4, ],
            [3, ],
            [2, ],
            [1, 40025.48],
            [0, 38971.3]
        ];
        var ticks = [
            [0, "Jan"],
            [1, "Fev"],
            [2, "Mar"],
            [3, "Abr"],
            [4, "Mai"],
            [5, "Jun"],
            [6, "Jul"],
            [7, "Ago"],
            [8, "Set"],
            [9, "Out"],
            [10, "Nov"],
            [11, "Dez"]
        ];
        var data = [{
        }, {
            label: "2016",
            data: _2016,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }, {
            label: "2017",
            data: _2017,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }];

        $.plot($("#carga"), data, {
            series: {
                lines: {
                    show: true
                }
            },
            valueLabels: {
                show: true
            },
            xaxis: {
                ticks: ticks
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            colors: ["#975197", "#6bd3f3"],
            tooltip: true,
            tooltipOpts: {
                content: "%s %y.2",
                defaultTheme: false
            }
        });        

//pld        
        var _2016 = [
            [0, 33.56],
            [1, 30.42],
            [2, 37.73],
            [3, 49.42],
            [4, 75.93],
            [5, 61.32],
            [6, 83.43],
            [7, 115.58],
            [8, 149.02],
            [9, 200.21],
            [10, 166.05],
            [11, 122.29]

        ];
        var _2017 = [
            [11, ],
            [10, ],
            [9, ],
            [8, ],
            [7, ],
            [6, ],
            [5, ],
            [4, ],
            [3, ],
            [2, ],
            [1, 128.43],
            [0, 121.44]
        ];
        var ticks = [
            [0, "Jan"],
            [1, "Fev"],
            [2, "Mar"],
            [3, "Abr"],
            [4, "Mai"],
            [5, "Jun"],
            [6, "Jul"],
            [7, "Ago"],
            [8, "Set"],
            [9, "Out"],
            [10, "Nov"],
            [11, "Dez"]
        ];
        var data = [{
        }, {
            label: "2016",
            data: _2016,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }, {
            label: "2017",
            data: _2017,
            lines: {
                show: true
            },
            points: {
                show: false
            }
        }];

        $.plot($("#pld"), data, {
            series: {
                lines: {
                    show: true
                }
            },
            valueLabels: {
                show: true
            },
            xaxis: {
                ticks: ticks
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            colors: ["#975197", "#6bd3f3"],
            tooltip: true,
            tooltipOpts: {
                content: "%s %y.2",
                defaultTheme: false
            }


        });

});