$("input[name='year']").TouchSpin({});

$(window).resize(function () {
    drawVisualization();
});

google.charts.load('current', {'packages': ['corechart']});
google.charts.setOnLoadCallback(drawVisualization);

function drawVisualization() {
    // Some raw data (not necessarily accurate)
    var chart_data = $("#chart_data").html();
    var data = new google.visualization.DataTable(JSON.parse(chart_data));

    var options = {
        legend: {position: 'bottom', maxLines: 3},
        series: {1: {type: 'area'}, 2: {type: 'line', lineDashStyle: [10, 2]}, 3: {type: 'line', lineDashStyle: [10, 2]}},
        bar: {groupWidth: '65%'},
        isStacked: true,
        colors: ['#69c2fe', '#5fc29d', '#f3b49f', '#FFD700', '#f6c7b6'],
        seriesType: 'bars',
        chartArea: {
            left: "5%",
            top: "5%",
            width: "100%"
        },
        vAxis: {title: 'MWh'}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('atendimento_contrato_chart'));
    chart.draw(data, options);
}

$(function () {
    setInterval(change_image, 3000);
    
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    
    $('#btn_unity_detail').click(function(e) {
        e.preventDefault();
        $('#unity_detail').modal();
    });
    
});

$("#month").change(function () {
    check_filters();
});
$("#year").blur(function () {
    check_filters();
});
$("#bootstrap-touchspin-down").click(function () {
    check_filters();
});
$("#bootstrap-touchspin-up").click(function () {
    check_filters();
});
$("#pk_ccvee_general").change(function () {
    check_filters();
});

function check_filters() {
    var month = $("#month").val();
    var year = $("#year").val();

    if (month > 0 && year > 0) {
        var dt = year + "-" + month + "-01";
        change_date(dt);
    }
}

function change_date(dt) {
    if (dt !== "" && date != dt) {
        $("#date").val(dt);
        $("#frm_dashboard").submit();
    }
}

