var dashboard = new Vue({
    el: '#app',
    data: {
        baseUrl: '../api_vue/',
        agentToken: null,
        show_loading: true,
        show_loading_chart: false,
        conversion: 'MWh',
        isPdf: false,
        filter: {
            month: null,
            year: null,
            ccvee: 0,
            source: 'w2',
            hour: 0,
        },
        resultFilter: {
            fk_gauges: [],
            gauges: [],
            hours: 0,
            records: [],
            unities: [],
            daysInMonth: 0,
        },
        widgets: {
            consumo: 0,
            consumoLiquido: 0,
            horasMedidas: 0,
            horasFaltantes: 0,
            perdas: 0,
            perdasCcvee: 0,
            proinfa: 0,
            contrato: 0,
            consumoversuscontrato: 0,
            flexInferior: 0,
            flexSuperior: 0,
            sobra: 0,
            deficit: 0,
            percentFlexSuperior: 0,
            percentFlexInferior: 0,
            consumoProjetado: 0,
            consumoLiquidoProjetado: 0,
            perdasProjetado: 0,
            sobraProjetado: 0,
            deficitProjetado: 0,
            attendance: 0,
            calculated: {
                mwh: {
                    has_values: false,
                    consumoLiquido: 0,
                    perdas: 0,
                    proinfa: 0,
                    contrato: 0,
                    sobra: 0,
                    deficit: 0,
                    flexInferior: 0,
                    flexSuperior: 0,
                    consumoLiquidoProjetado: 0,
                    perdasProjetado: 0,
                    sobraProjetado: 0,
                    deficitProjetado: 0
                },
                mwm: {
                    has_values: false,
                    consumoLiquido: 0,
                    perdas: 0,
                    proinfa: 0,
                    contrato: 0,
                    sobra: 0,
                    deficit: 0,
                    flexInferior: 0,
                    flexSuperior: 0,
                    consumoLiquidoProjetado: 0,
                    perdasProjetado: 0,
                    sobraProjetado: 0,
                    deficitProjetado: 0
                }
            },
            pld: 0,


        },
        default_calculated: {
            mwh: {
                has_values: false,
                consumoLiquido: 0,
                perdas: 0,
                proinfa: 0,
                contrato: 0,
                sobra: 0,
                deficit: 0,
                flexInferior: 0,
                flexSuperior: 0,
                consumoLiquidoProjetado: 0,
                perdasProjetado: 0,
                sobraProjetado: 0,
                deficitProjetado: 0
            },
            mwm: {
                has_values: false,
                consumoLiquido: 0,
                perdas: 0,
                proinfa: 0,
                contrato: 0,
                sobra: 0,
                deficit: 0,
                flexInferior: 0,
                flexSuperior: 0,
                consumoLiquidoProjetado: 0,
                perdasProjetado: 0,
                sobraProjetado: 0,
                deficitProjetado: 0
            }
        },
        resumeModal: {},
        resumeModalTotal: {},
        contractIndex: [],
        selectedContract: 0,

        graficoContrato: 0,
        graficoFlexInferior: 0,
        graficoFlexSuperior: 0,

        ccvee_generals: [],
        unities: [],
        pld: {},
        submarket: "",
        pld_if_liquidate: 0,

        chartData: [],
        graph: null,
        registroGrafico: {},
        filtered: false
    },
    mounted() {
        //pega um widgets vazio
        this.emptyWidgets = JSON.parse(JSON.stringify(this.widgets));

        var d = new Date();
        this.filter.month = d.getDate() <= 10 ? d.getMonth() : d.getMonth() + 1;
        this.filter.year = d.getDate() <= 10 && d.getMonth() + 1 == 1 ? d.getFullYear() - 1 : d.getFullYear();

        this.startProcess();

    },
    created() {
        this.startProcess();
    },
    computed: {},
    methods: {
        configurePDF() {
            if (window.location.pathname.indexOf('pdf') >= 0) {
                let urlParams = new URLSearchParams(window.location.search);
                this.filter.month = urlParams.get('month');
                this.filter.year = urlParams.get('year');
                this.filter.ccvee = urlParams.get('ccvee');
                sessionStorage.setItem('filter_ccvee', this.filter.ccvee);
                this.agentToken = urlParams.get('agent_token');
                this.isPdf = true;
            }
        },

        checkSupplyEnd(ccvee_general) {
            var ccvee_supply_end = new Date(ccvee_general.supply_end);
            var current_filter = new Date(this.filter.year + "-" + this.filter.month + "-01");
            return (ccvee_supply_end > current_filter);
        },

        startProcess() {
            this.conversion = 'MWh';
            this.configurePDF();
            this.show_loading_chart = true;
            if (this.graph) {
                this.graph.destroy();
            }

            let current_filter = JSON.stringify(this.filter);
            let session_filter = sessionStorage.getItem('filter');
            this.widgets.calculated.mwh.has_values = false;
            this.widgets.calculated.mwm.has_values = false;

            if (current_filter !== session_filter || this.isPdf) {
                this.getAgentToken();
                sessionStorage.setItem('filter', current_filter);
            } else {
                var self = this;
                this.agentToken = sessionStorage.getItem('agent_token');
                this.ccvee_generals = JSON.parse(sessionStorage.getItem('ccvee_generals'));
                this.unities = JSON.parse(sessionStorage.getItem('unities'));
                this.filter.ccvee = JSON.parse(sessionStorage.getItem('filter_ccvee'));
                this.contractIndex = JSON.parse(sessionStorage.getItem('contract_index'));
                this.resultFilter = JSON.parse(sessionStorage.getItem('result_filter'));
                this.processResultFilter();
                this.processUnityResultFilter(0);

                setTimeout(function() {
                    self.chart();
                    self.show_loading_chart = false;
                }, 3000);

            }
        },
        /**
         * Geração do gráfico
         */
        chart() {
            var self = this;

            self.graph = new Chart(document.getElementById("mixedchart"), {
                type: 'bar',
                data: {
                    labels: self.registroGrafico.days,
                    datasets: [{
                            label: "Flex. Inferior",
                            type: "line",
                            borderColor: "#FFD700",
                            data: self.registroGrafico.graficoFlexInferior,
                            fill: false
                        }, {
                            label: "Flex. Superior",
                            type: "line",
                            borderColor: "#f3b49f",
                            data: self.registroGrafico.graficoFlexSuperior,
                            fill: false
                        },
                        {
                            label: "Consumo",
                            // type: "bar",
                            backgroundColor: "#69c2fe",
                            backgroundColorHover: "#3e95cd",
                            data: self.registroGrafico.graficoData
                        },
                        {
                            label: "Contrato",
                            type: "line",
                            borderColor: "#5fc29d",
                            backgroundColor: 'rgb(181, 242, 219, 0.2)',
                            data: self.registroGrafico.graficoContrato,
                            fill: true
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    showScale: false,
                    title: {
                        display: false,
                    },
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'MWh',
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    }
                }
            });

        },

        /**
         * Processamento de TOKEN 
         */
        getAgentToken() {
            if (this.isPdf) {
                sessionStorage.setItem('agent_token', this.agentToken);
                this.processCcveeList();
            } else {
                var self = this;
                axios.get(this.baseUrl + 'getAgentToken')
                    .then(function(response) {
                        if (response.status === 200) {
                            if (response.data.status === 'success' && response.data.data.token) {
                                self.agentToken = response.data.data.token;
                                sessionStorage.setItem('agent_token', self.agentToken);
                                self.processCcveeList();
                            }
                        }
                        this.show_loading_chart = false;
                    });
            }
        },

        /**
         * Pega a lista de CCVEE e aplica os filtros
         */
        processCcveeList() {
            if (!_.isNull(this.agentToken)) {
                var self = this;
                axios.get(this.baseUrl + 'getCcveeList', {
                    params: {
                        month: self.filter.month,
                        year: self.filter.year,
                        token: self.agentToken
                    }
                }).then(function(response) {
                    if (response.status === 200) {
                        if (response.data.status === 'success') {
                            self.ccvee_generals = response.data.data.ccvee_generals;
                            self.unities = response.data.data.unities;
                            self.pld = response.data.data.pld;
                            sessionStorage.setItem('ccvee_generals', JSON.stringify(self.ccvee_generals));
                            sessionStorage.setItem('unities', JSON.stringify(self.unities));
                            sessionStorage.setItem('pld', JSON.stringify(self.pld));

                            if (self.ccvee_generals.length === 1) {
                                self.filter.ccvee = self.ccvee_generals[0].pk_ccvee_general;
                                sessionStorage.setItem('filter_ccvee', JSON.stringify(self.filter.ccvee));
                            } else {
                                var temp = [];
                                for (var i in self.ccvee_generals) {
                                    self.contractIndex[self.ccvee_generals[i].pk_ccvee_general] = i;
                                }
                                sessionStorage.setItem('contract_index', JSON.stringify(self.contractIndex));
                            }
                        }
                        self.applyFilters();
                    }
                });
            }
        },

        applyFilters() {
            this.filtered = true;
            if (this.validateFilter) {
                var self = this;

                self.show_loading_chart = true;
                axios.get(this.baseUrl + 'filter', {
                    params: {
                        filter: self.filter
                    }
                }).then(function(response) {
                    if (parseInt(response.status) === 200) {
                        self.resultFilter = response.data.data;
                        sessionStorage.setItem('result_filter', JSON.stringify(self.resultFilter));
                        self.processResultFilter();
                        self.processUnityResultFilter(0);

                        setTimeout(function() {
                            self.chart();
                            self.show_loading_chart = false;
                        }, 3000);

                    }
                }).catch(function(error) {
                    console.log(error);
                });
            }
        },

        processResultFilter() {
            this.widgets.consumo = 0;
            this.widgets.horasMedidas = 0;
            this.widgets.horasFaltantes = 0;

            for (var index in this.resultFilter.records) {
                var record = this.resultFilter.records[index];
                if (parseInt(record.error) === 0) {
                    this.widgets.horasMedidas++;
                    this.widgets.consumo += parseFloat(record.value) / 1000;
                } else {
                    this.widgets.horasFaltantes++;
                }
            }
            this.processLosses();

            //total de horas no mes
            this.widgets.totalHorasMes = this.resultFilter.hours;
            //porcentagem de horas medidas no mes
            this.widgets.avgHorasMedidas = parseInt(this.widgets.horasMedidas) > 0 ? (this.widgets.horasMedidas * 100) / this.widgets.totalHorasMes : 0;

            this.widgets.proinfa = this.resultFilter.proinfa;

            this.processContractAndFlex();

        },

        //Resultado de processamento de unidades
        processUnityResultFilter(converted) {
            this.resumeModal = {};
            //verificar se o total de unidades é maior que 1
            if (parseInt(this.ccvee_unities) > 1 && !_.isUndefined(this.resultFilter.unities)) {
                for (var i in this.resultFilter.unities) {
                    //inicializa a variável e insere a variável
                    this.resumeModal[this.resultFilter.unities[i].fk_unity] = this.resultFilter.unities[i];
                    if (_.isUndefined(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume)) {
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume = {
                            value: 0,
                            hours: 0,
                            consumo: 0,
                            perdas: 0,
                            proinfa: 0,
                            liquido: 0
                        };
                    } else {
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.value = 0;
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.hours = 0;
                    }

                    //adiciona o medidor a unidade
                    if (!_.isUndefined(this.resultFilter.gauges)) {
                        for (var k in this.resultFilter.gauges) {
                            if (parseInt(this.resultFilter.gauges[k].fk_unity) === parseInt(this.resultFilter.unities[i].fk_unity)) {
                                this.resumeModal[this.resultFilter.unities[i].fk_unity].fk_gauge = this.resultFilter.gauges[k].pk_gauge;
                            }
                        }
                    }

                    //soma as horas e os valores
                    if (!_.isUndefined(this.resultFilter.records)) {
                        for (var j in this.resultFilter.records) {
                            if (parseInt(this.resultFilter.records[j].error) === 0 && parseInt(this.resultFilter.records[j].fk_gauge) === parseInt(this.resumeModal[this.resultFilter.unities[i].fk_unity].fk_gauge)) {
                                this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.value += parseFloat(this.resultFilter.records[j].value) / 1000;
                                this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.hours++;
                            }
                        }
                    }

                    //calcula o consumo, perdas, proinfa
                    if (converted === 1) {
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo = this.conversion === 'MWh' ? (this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo * this.widgets.totalHorasMes).toFixed(3) : (this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.value / this.widgets.totalHorasMes).toFixed(6);
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.proinfa = this.conversion === 'MWh' ? (parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.proinfa) * this.widgets.totalHorasMes).toFixed(3) : (parseFloat(this.resultFilter.proinfa_unity[this.resultFilter.unities[i].fk_unity]) / this.widgets.totalHorasMes).toFixed(6);
                        var perdas = this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo * (this.widgets.perdasCcvee / 100);
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.perdas = this.conversion === 'MWh' ? perdas.toFixed(3) : perdas.toFixed(6);
                        var liquido = ((parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo) + parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.perdas)) - parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.proinfa));
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.liquido = this.conversion === 'MWh' ? liquido.toFixed(3) : liquido.toFixed(6);
                    } else {
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo = (this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.value).toFixed(3);
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.proinfa = parseFloat(this.resultFilter.proinfa_unity[this.resultFilter.unities[i].fk_unity]).toFixed(3);
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.perdas = (this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo * (this.widgets.perdasCcvee / 100)).toFixed(3);
                        var liquido = ((parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.consumo) + parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.perdas)) - parseFloat(this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.proinfa));
                        this.resumeModal[this.resultFilter.unities[i].fk_unity].resume.liquido = liquido.toFixed(3);
                    }

                }

                //somatórias
                this.resumeModalTotal = {
                    value: 0,
                    hours: 0,
                    consumo: 0,
                    proinfa: 0,
                    perdas: 0,
                    liquido: 0
                };
                for (var i in this.resumeModal) {
                    this.resumeModalTotal.value += parseFloat(this.resumeModal[i].resume.value);
                    this.resumeModalTotal.hours += parseInt(this.resumeModal[i].resume.hours);
                    this.resumeModalTotal.consumo += parseFloat(this.resumeModal[i].resume.consumo);
                    this.resumeModalTotal.proinfa += parseFloat(this.resumeModal[i].resume.proinfa);
                    this.resumeModalTotal.perdas += (parseFloat(this.resumeModal[i].resume.perdas));
                    this.resumeModalTotal.liquido += parseFloat(this.resumeModal[i].resume.liquido);
                }

                if (this.resumeModal) {
                    this.resumeModalTotal.value = this.resumeModalTotal.value > 0 ? String(this.resumeModalTotal.value.toFixed(3)).replace(".", ",") : "0,000";
                    this.resumeModalTotal.value = this.resumeModalTotal.value > 0 ? String(this.resumeModalTotal.value.toFixed(3)).replace(".", ",") : "0,000";
                    this.resumeModalTotal.hours = this.resumeModalTotal.hours > 0 ? String(this.resumeModalTotal.hours.toFixed(3)).replace(".", ",") : "0,000";
                    this.resumeModalTotal.consumo = this.resumeModalTotal.consumo > 0 ? String(this.resumeModalTotal.consumo.toFixed(3)).replace(".", ",") : "0,000";
                    this.resumeModalTotal.proinfa = this.resumeModalTotal.proinfa > 0 ? String(this.resumeModalTotal.proinfa.toFixed(3)).replace(".", ",") : "0,000";
                    this.resumeModalTotal.perdas = this.resumeModalTotal.perdas > 0 ? String(this.resumeModalTotal.perdas.toFixed(3)).replace(".", ",") : "0,000";
                    this.resumeModalTotal.liquido = this.resumeModalTotal.liquido > 0 ? String(this.resumeModalTotal.liquido.toFixed(3)).replace(".", ",") : "0,000";
                }

            }
        },

        processLosses() {
            this.selectedContract = parseInt(this.selectedContract) > 0 ? this.selectedContract : 0;
            this.widgets.perdasCcvee = parseInt(this.ccvee_generals[this.selectedContract].losses);
            this.widgets.perdas = this.widgets.consumo * (this.widgets.perdasCcvee / 100);
        },

        processContractAndFlex() {
            var indexContract = 0;

            if (typeof this.resultFilter.energy_bulk[0] !== 'undefined' && typeof this.resultFilter.energy_bulk[0].seasonal !== 'undefined' && parseFloat(this.resultFilter.energy_bulk[0].seasonal) > 0) {
                indexContract = parseFloat(this.resultFilter.energy_bulk[0].seasonal);
            } else if (typeof this.resultFilter.energy_bulk[0] !== 'undefined' && typeof this.resultFilter.energy_bulk[0].forecast !== 'undefined' && parseFloat(this.resultFilter.energy_bulk[0].forecast) > 0) {
                indexContract = parseFloat(this.resultFilter.energy_bulk[0].forecast);
            }

            this.widgets.attendance = parseFloat(this.resultFilter.energy_bulk[0].attendance);
            this.widgets.consumo = (this.widgets.consumo * this.widgets.attendance) / 100;
            this.widgets.perdas = (this.widgets.perdas * this.widgets.attendance) / 100;
            this.widgets.proinfa = (this.widgets.proinfa * this.widgets.attendance) / 100;

            this.widgets.consumoLiquido = this.widgets.consumo + this.widgets.perdas - this.widgets.proinfa;

            this.widgets.contrato = indexContract * this.widgets.totalHorasMes;
            this.widgets.consumoversuscontrato = this.widgets.contrato > 0 ? Math.ceil((this.widgets.consumoLiquido / this.widgets.contrato) * 100) : 0;
            this.widgets.flexInferior = this.widgets.contrato + (this.widgets.contrato * (parseFloat(this.ccvee_generals[this.selectedContract].flexibility_inferior) / 100));
            this.widgets.flexSuperior = this.widgets.contrato + (this.widgets.contrato * (parseFloat(this.ccvee_generals[this.selectedContract].flexibility_superior) / 100));
            this.widgets.sobra = this.widgets.consumoLiquido > this.widgets.flexInferior ? 0 : this.widgets.flexInferior - this.widgets.consumoLiquido;
            this.widgets.deficit = this.widgets.consumoLiquido < this.widgets.flexSuperior ? 0 : this.widgets.consumoLiquido - this.widgets.flexSuperior;
            this.widgets.percentFlexSuperior = this.ccvee_generals[this.selectedContract].flexibility_superior;
            this.widgets.percentFlexInferior = this.ccvee_generals[this.selectedContract].flexibility_inferior;

            //Projetado
            this.widgets.consumoProjetado = ((this.widgets.consumo * this.widgets.totalHorasMes) / this.widgets.horasMedidas) * this.resultFilter.unities.length;
            this.widgets.consumoLiquidoProjetado = ((this.widgets.consumoLiquido * this.widgets.totalHorasMes) / this.widgets.horasMedidas) * this.resultFilter.unities.length;
            this.widgets.perdasProjetado = ((this.widgets.perdas * this.widgets.totalHorasMes) / this.widgets.horasMedidas) * this.resultFilter.unities.length;

            var sobra = this.widgets.flexInferior - this.widgets.consumoLiquidoProjetado;
            this.widgets.sobraProjetado = sobra > 0 ? sobra : 0;

            var deficit = this.widgets.consumoLiquidoProjetado - this.widgets.flexSuperior;
            this.widgets.deficitProjetado = deficit > 0 ? deficit : 0;

            this.graficoContrato = (this.widgets.contrato / this.widgets.totalHorasMes * 24);
            this.graficoFlexInferior = this.widgets.flexInferior / this.widgets.totalHorasMes * 24;
            this.graficoFlexSuperior = this.widgets.flexSuperior / this.widgets.totalHorasMes * 24;

            //RESUMO FINANCEIRO POR CONTRATO
            //PLD (R$/MWh)
            this.widgets.pld = this.pld;
            this.widgets.submarket = this.ccvee_generals[this.selectedContract].submarket;
            this.widgets.pld_if_liquidate = parseFloat(this.pld.pld_value) > 0 ? parseFloat(this.widgets.sobraProjetado) * parseFloat(this.pld.pld_value) : 0;
            this.widgets.credit_date = this.pld.credit_date;
            this.widgets.short_term_price = parseFloat(this.pld.pld_value + this.pld.spread); 

            if (parseFloat(this.widgets.sobraProjetado) === 0.00 && this.widgets.deficitProjetado === 0.00) {
                this.widgets.short_term_total = 0;
                this.widgets.short_term_text1 = 'VENDA/COMPRA';
                this.widgets.short_term_text2 = 'Pagamento ou Recebimento';
            } else if (parseFloat(this.widgets.sobraProjetado) > 0.00) {
                this.widgets.short_term_total = parseFloat(this.widgets.sobraProjetado) * this.widgets.short_term_price;
                this.widgets.short_term_text1 = 'VENDA';
                this.widgets.short_term_text2 = 'Recebimento';
            } else {
                this.widgets.short_term_total = parseFloat(this.widgets.deficitProjetado) * this.widgets.short_term_price;
                this.widgets.short_term_text1 = 'COMPRA';
                this.widgets.short_term_text2 = 'Pagamento';
            }

            //gera os dados para o gráfico
            this.processChartData();
        },

        processChartData() {
            var self = this;
            self.registroGrafico = {
                    days: [],
                    graficoContrato: [],
                    graficoFlexSuperior: [],
                    graficoFlexInferior: [],
                    graficoData: []
                }
                //gerar array vazio, com todos os dias do mês
            for (i = 1; i <= parseInt(self.resultFilter.daysInMonth); i++) {
                self.registroGrafico.days.push(i);
                self.registroGrafico.graficoContrato.push(self.graficoContrato.toFixed(2));
                self.registroGrafico.graficoFlexSuperior.push(self.graficoFlexSuperior.toFixed(2));
                self.registroGrafico.graficoFlexInferior.push(self.graficoFlexInferior.toFixed(2));
                self.registroGrafico.graficoData.push(0);

            }
            //percorre os resultados e adiciona os dados
            for (var index in this.resultFilter.records) {
                var idx = this.resultFilter.records[index].day - 1;
                self.registroGrafico.graficoData[idx] += parseFloat(this.resultFilter.records[index].value) / 1000;
            }

            if (self.registroGrafico.graficoData.length) {
                for (i in self.registroGrafico.graficoData) {
                    if (self.registroGrafico.graficoData[i] > 0) {
                        let calc = (self.registroGrafico.graficoData[i] * this.widgets.attendance) / 100;
                        self.registroGrafico.graficoData[i] = calc.toFixed(4);

                    } else {
                        self.registroGrafico.graficoData[i] = 0;
                    }
                }
            }

        },

        floatToString(value, decimals = 2) {
            if (value != "" && typeof(value) != 'undefined') {
                var valTemp = value.toFixed(decimals);
                valTemp = String(valTemp);
                return valTemp.replace(".", ",");
            } else {
                return "0,00";
            }
        },

        getWindowResize(event) {
            setTimeout(() => {
                this.chart();
            }, 500);
        },

        openGraphNewTab() {
            localStorage.setItem("graph", JSON.stringify(this.registroGrafico));
            window.open(`${window.location.origin}/customer/dashboard/graph`);
        }

    },
    computed: {
        validateFilter() {
            if (_.isNull(this.filter.month) || _.isNull(this.filter.year) || String(this.filter.year).length !== 4 || _.isNull(this.filter.ccvee) || parseInt(this.filter.ccvee) == 0) {
                return false;
            }
            return true;
        },
        ccvee_unities() {
            if (!_.isNull(this.filter.ccvee)) {
                for (var i in this.ccvee_generals) {
                    if (parseInt(this.ccvee_generals[i].pk_ccvee_general) === parseInt(this.filter.ccvee)) {
                        return parseInt(this.ccvee_generals[i].unities);
                    }
                }
            }
            return 1;
        },
        ccvee_contract() {
            if (!_.isNull(this.filter.ccvee) && this.ccvee_generals.length > 0) {
                for (var i in this.ccvee_generals) {
                    if (parseInt(this.ccvee_generals[i].pk_ccvee_general) === parseInt(this.filter.ccvee)) {
                        return this.ccvee_generals[i];
                    }
                }
            }
            return {}
        }
    },
    watch: {
        conversion(v) {
            this.widgets.calculated = this.default_calculated;

            if (v === 'MWh') {
                if (!this.widgets.calculated.mwh.has_values) {
                    this.widgets.calculated.mwh.has_values = true;

                    this.widgets.calculated.mwh.consumoLiquido = this.widgets.consumoLiquido * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.perdas = this.widgets.perdas * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.proinfa = this.widgets.proinfa * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.contrato = this.widgets.contrato * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.sobra = this.widgets.sobra * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.deficit = this.widgets.deficit * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.flexInferior = this.widgets.flexInferior * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.flexSuperior = this.widgets.flexSuperior * this.widgets.totalHorasMes;

                    this.widgets.calculated.mwh.consumoLiquidoProjetado = (this.widgets.calculated.mwh.consumoLiquido / this.widgets.horasMedidas) * this.widgets.totalHorasMes;
                    this.widgets.calculated.mwh.perdasProjetado = (this.widgets.calculated.mwh.perdas / this.widgets.horasMedidas) * this.widgets.totalHorasMes;

                    var sobra = this.widgets.calculated.mwh.flexInferior - (this.widgets.calculated.mwh.consumoLiquidoProjetado * this.resultFilter.unities.length);
                    this.widgets.calculated.mwh.sobraProjetado = sobra > 0 ? sobra : 0;

                    var deficit = this.widgets.calculated.mwh.consumoLiquidoProjetado - (this.widgets.calculated.mwh.flexSuperior * this.resultFilter.unities.length);
                    this.widgets.calculated.mwh.deficitProjetado = deficit > 0 ? deficit : 0;
                }

                if (this.widgets.calculated.mwh.has_values) {
                    this.widgets.consumoLiquido = this.widgets.calculated.mwh.consumoLiquido;
                    this.widgets.perdas = this.widgets.calculated.mwh.perdas;
                    this.widgets.proinfa = this.widgets.calculated.mwh.proinfa;
                    this.widgets.contrato = this.widgets.calculated.mwh.contrato;
                    this.widgets.sobra = this.widgets.calculated.mwh.sobra;
                    this.widgets.deficit = this.widgets.calculated.mwh.deficit;
                    this.widgets.flexInferior = this.widgets.calculated.mwh.flexInferior;
                    this.widgets.flexSuperior = this.widgets.calculated.mwh.flexSuperior;
                    this.widgets.consumoLiquidoProjetado = this.widgets.calculated.mwh.consumoLiquidoProjetado * this.resultFilter.unities.length;
                    this.widgets.perdasProjetado = this.widgets.calculated.mwh.perdasProjetado * this.resultFilter.unities.length;
                    this.widgets.sobraProjetado = this.widgets.calculated.mwh.sobraProjetado;
                    this.widgets.deficitProjetado = this.widgets.calculated.mwh.deficitProjetado;

                }

            } else if (v === 'MWm') {
                if (!this.widgets.calculated.mwm.has_values) {
                    this.widgets.calculated.mwm.has_values = true;

                    this.widgets.calculated.mwm.consumoLiquido = this.widgets.consumoLiquido / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.perdas = this.widgets.perdas / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.proinfa = this.widgets.proinfa / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.contrato = this.widgets.contrato / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.sobra = this.widgets.sobra / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.deficit = this.widgets.deficit / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.flexInferior = this.widgets.flexInferior / this.widgets.totalHorasMes;
                    this.widgets.calculated.mwm.flexSuperior = this.widgets.flexSuperior / this.widgets.totalHorasMes;

                    this.widgets.calculated.mwm.consumoLiquidoProjetado = (this.widgets.calculated.mwm.consumoLiquido * this.widgets.totalHorasMes) / this.widgets.horasMedidas;
                    this.widgets.calculated.mwm.perdasProjetado = (this.widgets.calculated.mwm.perdas * this.widgets.totalHorasMes) / this.widgets.horasMedidas;

                    var sobra = this.widgets.calculated.mwm.flexInferior - (this.widgets.calculated.mwm.consumoLiquidoProjetado * this.resultFilter.unities.length);
                    this.widgets.calculated.mwm.sobraProjetado = sobra > 0 ? sobra : 0;

                    var deficit = this.widgets.calculated.mwm.consumoLiquidoProjetado - (this.widgets.calculated.mwm.flexSuperior * this.resultFilter.unities.length);
                    this.widgets.calculated.mwm.deficitProjetado = deficit > 0 ? deficit : 0;
                }

                if (this.widgets.calculated.mwm.has_values) {
                    this.widgets.consumoLiquido = this.widgets.calculated.mwm.consumoLiquido;
                    this.widgets.perdas = this.widgets.calculated.mwm.perdas;
                    this.widgets.proinfa = this.widgets.calculated.mwm.proinfa;
                    this.widgets.contrato = this.widgets.calculated.mwm.contrato;
                    this.widgets.sobra = this.widgets.calculated.mwm.sobra;
                    this.widgets.deficit = this.widgets.calculated.mwm.deficit;
                    this.widgets.flexInferior = this.widgets.calculated.mwm.flexInferior;
                    this.widgets.flexSuperior = this.widgets.calculated.mwm.flexSuperior;
                    this.widgets.consumoLiquidoProjetado = this.widgets.calculated.mwm.consumoLiquidoProjetado * this.resultFilter.unities.length;
                    this.widgets.perdasProjetado = this.widgets.calculated.mwm.perdasProjetado * this.resultFilter.unities.length;
                    this.widgets.sobraProjetado = this.widgets.calculated.mwm.sobraProjetado;
                    this.widgets.deficitProjetado = this.widgets.calculated.mwm.deficitProjetado;
                }

            }

            this.processUnityResultFilter(1);
        },

        'filter.ccvee' (v) {
            this.filtered = false;
            if (_.isArray(this.contractIndex)) {
                this.selectedContract = parseInt(this.contractIndex[v]);
            } else {
                this.selectedContract = 0;
            }
        },

    }
});