var dashboard = new Vue({
    el: '#app',
    data: {
        graph: null,
        registroGrafico: null,
    },
    mounted() {
        this.getGraphData();
    },
    methods: {
        getGraphData() {
            let registroGrafico = localStorage.getItem("graph");
            if (registroGrafico) {
                this.registroGrafico = JSON.parse(registroGrafico);
                this.chart();
            } else {

            }
            console.log(registroGrafico)
        },
        chart() {
            var self = this;
                        
            self.graph = new Chart(document.getElementById("mixedchart"), {
                type: 'bar',
                data: {
                    labels: self.registroGrafico.days,
                    datasets: [
                        {
                            label: "Flex. Inferior",
                            type: "line",
                            borderColor: "#FFD700",
                            data: self.registroGrafico.graficoFlexInferior,
                            fill: false
                        }, {
                            label: "Flex. Superior",
                            type: "line",
                            borderColor: "#f3b49f",
                            data: self.registroGrafico.graficoFlexSuperior,
                            fill: false
                        },
                        {
                            label: "Consumo",
                            type: "bar",
                            backgroundColor: "#69c2fe",
                            backgroundColorHover: "#3e95cd",
                            data: self.registroGrafico.graficoData
                        },
                        {
                            label: "Contrato",
                            type: "line",
                            borderColor: "#5fc29d",
                            backgroundColor:
                                    'rgb(181, 242, 219, 0.2)'
                            ,
                            data: self.registroGrafico.graficoContrato,
                            fill: true
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    showScale: false,                   
                    title: {
                        display: false,
                    },
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'MWh',
                                },
                                ticks: {
                                    beginAtZero: true,
                                }
                            }]
                    }
                }
            });
            
        },

    }
});

