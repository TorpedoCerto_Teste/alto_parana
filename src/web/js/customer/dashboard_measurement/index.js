$("input[name='year']").TouchSpin({});

$(function () {

    $('.calendar-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function (e) {
        var choose_date = $(this).val();
        var dt = choose_date.split("/").reverse().join("-");
        change_date(dt);
    });
});

function change_date(dt) {
    if (dt !== "") {
        $("#date").val(dt);
        $("#frm_dashboard").submit();
    }
}


$("#month").change(function () {
    check_filters();
});
$("#year").blur(function () {
    check_filters();
});
$("#bootstrap-touchspin-down").click(function () {
    check_filters();
});
$("#bootstrap-touchspin-up").click(function () {
    check_filters();
});
$("#fk_unity").change(function () {
    check_filters();
});
$("#fk_gauge").change(function () {
    check_filters();
});

function check_filters() {
    var month = $("#month").val();
    var year = $("#year").val();
    var fk_unity = $("#fk_unity").val();
    var fk_gauge = $("#fk_gauge").val();

    if (month > 0 && year > 0 && fk_unity > 0 && fk_gauge > 0) {
        var dt = year + "-" + month + "-01";
        change_date(dt);
    }
}

//mudança de datas
$(".muda_data").click(function () {
    var dt = $(this).attr("date");
    change_date(dt);
});

//GRÁFICOS
google.charts.load('current', {'packages': ['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    //Consumo Diário no Mês
    //
    //dados para gerar o gráfico
    var fk_unity = $("#fk_unity").val();
    var fk_gauge = $("#fk_gauge").val();
    var date = $("#date").val();

    var jsonData_consumo_diario_mes = null;
    if (parseInt(fk_unity) > 0 && parseInt(fk_gauge) > 0 && date !== "") {
        jsonData_consumo_diario_mes = $.ajax({
            method: "POST",
            url: $('#base_url').val() + "telemetry_record/ajax_consumo_diario_mes",
            dataType: "json",
            data: {
                fk_unity: fk_unity,
                fk_gauge: fk_gauge,
                date: date
            },
            async: false
        }).responseText;
    }

    var data_consumo_diario_mes = new google.visualization.DataTable(jsonData_consumo_diario_mes);
    var options_consumo_diario_mes = {
        legend: {position: 'bottom', maxLines: 3},
        bar: {groupWidth: '80%'},
        isStacked: true,
        colors: ['#69c2fe', '#5fc29d', '#FFD700', '#f3b49f', '#f6c7b6'],
        seriesType: 'bars',
        chartArea: {
            left: "5%",
            top: "5%",
            width: "100%"
        },
        vAxis: {format: '###0.000'}
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('consumo_diario_mes_chart'));
    chart.draw(data_consumo_diario_mes, options_consumo_diario_mes);



    //Consumo Horário no Dia 
    var jsonData_consumo_horario_dia = null;
    if (parseInt(fk_unity) > 0 && parseInt(fk_gauge) > 0 && date !== "") {
        jsonData_consumo_horario_dia = $.ajax({
            method: "POST",
            url: $('#base_url').val() + "telemetry_record/ajax_consumo_horario_dia",
            dataType: "json",
            data: {
                fk_unity: fk_unity,
                fk_gauge: fk_gauge,
                date: date
            },
            async: false
        }).responseText;
    }
    var data_consumo_horario_dia = new google.visualization.DataTable(jsonData_consumo_horario_dia);
    var options_consumo_horario_dia = {
        legend: {position: 'bottom', maxLines: 3},
        bar: {groupWidth: '50%'},
        colors: ['#69c2fe', '#5fc29d', '#FFD700', '#f3b49f', '#f6c7b6'],
        vAxis: {
            title: 'MWh'
        },
        chartArea: {
            left: "2%",
            top: "5%",
            width: "100%"
        }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('consumo_horario_dia_chart'));
    chart.draw(data_consumo_horario_dia, options_consumo_horario_dia);


    //Demanda Medida Diária 
    var jsonData_demanda_medida_diaria = null;
    if (parseInt(fk_unity) > 0 && parseInt(fk_gauge) > 0 && date !== "") {
        jsonData_demanda_medida_diaria = $.ajax({
            method: "POST",
            url: $('#base_url').val() + "telemetry_record/ajax_demanda_medida_diaria",
            dataType: "json",
            data: {
                fk_unity: fk_unity,
                fk_gauge: fk_gauge,
                date: date
            },
            async: false
        }).responseText;
    }
    var data_demanda_medida_diaria = new google.visualization.DataTable(jsonData_demanda_medida_diaria);
    var options_demanda_medida_diaria = {
        legend: {position: 'bottom', maxLines: 3},
        bar: {groupWidth: '80%'},
        isStacked: true,
        colors: ['#69c2fe', '#5fc29d', '#FFD700', '#f3b49f', '#f6c7b6'],
        seriesType: 'bars',
        series: {2: {type: 'line'}, 3: {type: 'line', lineDashStyle: [10, 2]}},
        hAxis: {maxValue: 12},
        vAxis: {
            title: 'MW'
        },
        chartArea: {
            left: "5%",
            top: "5%",
            width: "100%"
        }

    };
    var chart = new google.visualization.ComboChart(document.getElementById('demanda_medida_diaria_chart'));
    chart.draw(data_demanda_medida_diaria, options_demanda_medida_diaria);


    //Demandas Máximas Diárias No Mês 
    var jsonData_demandas_maximas_diarias_mes = null;
    if (parseInt(fk_unity) > 0 && parseInt(fk_gauge) > 0 && date !== "") {
        jsonData_demandas_maximas_diarias_mes = $.ajax({
            method: "POST",
            url: $('#base_url').val() + "telemetry_record/ajax_demandas_maximas_diarias_mes",
            dataType: "json",
            data: {
                fk_unity: fk_unity,
                fk_gauge: fk_gauge,
                date: date
            },
            async: false
        }).responseText;
    }
    var data_demandas_maximas_diarias_mes = new google.visualization.DataTable(jsonData_demandas_maximas_diarias_mes);
    var options_demandas_maximas_diarias_mes = {
        legend: {position: 'bottom', maxLines: 3},
        bar: {groupWidth: '80%'},
        isStacked: false,
        colors: ['#69c2fe', '#5fc29d', '#69c2fe', '#5fc29d'],
        seriesType: 'bars',
        series: {2: {type: 'line'}, 3: {type: 'line'}},
        chartArea: {
            left: "5%",
            top: "5%",
            width: "100%"
        }
    };
    var chart = new google.visualization.ComboChart(document.getElementById('demandas_maximas_diarias_mes_chart'));
    chart.draw(data_demandas_maximas_diarias_mes, options_demandas_maximas_diarias_mes);

    //Consumo por Posto Tarifário
    var jsonData_consumo_posto_tarifario = null;
    if (parseInt(fk_unity) > 0 && parseInt(fk_gauge) > 0 && date !== "") {
        jsonData_consumo_posto_tarifario = $.ajax({
            method: "POST",
            url: $('#base_url').val() + "telemetry_record/ajax_consumo_posto_tarifario",
            dataType: "json",
            data: {
                fk_unity: fk_unity,
                fk_gauge: fk_gauge,
                date: date
            },
            async: false
        }).responseText;
    }
    var data_consumo_posto_tarifario = new google.visualization.DataTable(jsonData_consumo_posto_tarifario);
    var options_consumo_posto_tarifario = {
        pieHole: 0.4,
        legend: {position: 'bottom', maxLines: 3},
        colors: ['#69c2fe', '#5fc29d', '#FFD700', '#f3b49f', '#f6c7b6'],
        chartArea: {
            left: "2%",
            top: "5%",
            width: "100%"
        }
    };
    var chart = new google.visualization.PieChart(document.getElementById('consumo_posto_tarifario_chart'));
    chart.draw(data_consumo_posto_tarifario, options_consumo_posto_tarifario);


}

$(window).resize(function () {
    drawChart();
});

//unity_consumption_limit
$(".btn_unity_consumption_limit").click(function () {
    var month = $("#month").val();
    var year = $("#year").val();
    var fk_unity = $("#fk_unity").val();
    var pk_unity_consumption_limit = $("#pk_unity_consumption_limit").val();
    var consumption_limit = $("#consumption_limit").val() <= 0 ? 0 : $("#consumption_limit").val();
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity_consumption_limit/ajax_upsert",
        dataType: "json",
        data: {
            fk_unity: fk_unity,
            month: month,
            year: year,
            consumption_limit: consumption_limit,
            pk_unity_consumption_limit: pk_unity_consumption_limit
        }
    }).done(function (value) {
        if (parseInt(value) > 0) {
            var consumo_mensal = $("#div_consumo_mensal").html();
            consumption_limit = parseFloat(consumption_limit.replace(",", "."));
            var percent = consumption_limit > 0 && parseInt(consumo_mensal) > 0 ? (consumo_mensal * 100) / consumption_limit : 0;
            percent = Math.round(percent);
            generate_limit(consumption_limit, percent);
        }
        $('#unity_consumption_limit_upsert').modal('hide');
    });



});

function generate_limit(consumption_limit, percent) {
    var bar = percent < 100 ? "success" : "danger";
    var width = percent < 100 ? percent : 100;
    var text =
            "<div class=\"progress tooltips\" style=\"margin-bottom: 5px;\" data-placement=\"bottom\" data-toggle=\"tooltip\" data-original-title=\"Limite de consumo informado: " + consumption_limit + " MWh\">" +
            "    <div class=\"progress-bar progress-bar-" + bar + "\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " + width + "%;\"> " + percent + "% </div>" +
            "</div>";

    $("#div_limite_consumo").html(text);
}
