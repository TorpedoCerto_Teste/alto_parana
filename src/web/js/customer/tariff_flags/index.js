$(function () {
    $("input[name='year']").TouchSpin({});

    $('#year').blur(function () {
        $('#loading').modal('show');
        make_ajax();
    });
    $('.bootstrap-touchspin-up').click(function () {
        $('#loading').modal('show');
        make_ajax();
    });
    $('.bootstrap-touchspin-down').click(function () {
        $('#loading').modal('show');
        make_ajax();
    });

});


function make_ajax() {
    $.get($('#base_url').val() + $('#year').val(),
            function (data, status) {
                data = JSON.parse(data);
                for (var k in data) {
                    if (data.hasOwnProperty(k)) {
                        $('#increase_' + k).html(data[k]['increase']).removeClass('spinner');
                        $('#flag_' + k).html(data[k]['flag']);
                        $('#tr_' + k).removeClass('alert-success').removeClass('alert-warning').removeClass('alert-danger');
                        $('#tr_' + k).addClass('alert-' + data[k]['color']);
                    }
                }
                $('#loading').modal('hide');
            });
    return true;

}
