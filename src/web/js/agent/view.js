$(function() {
   $('.wysihtml5').wysihtml5();
   
    //busca cep
    if ($('#postal_code').length) {
        $('#postal_code').blur(function () {
             $("#state").addClass('spinner');
             $("#city").addClass('spinner');
             $("#district").addClass('spinner');
             $("#address").addClass('spinner');
            $.get("https://viacep.com.br/ws/"+$('#postal_code').val()+"/json/",
                function(data, status){
                    $("#state").val(data.uf).removeClass('spinner');
                    $("#city").val(data.localidade).removeClass('spinner');
                    $("#district").val(data.bairro).removeClass('spinner');
                    $("#address").val(data.logradouro).removeClass('spinner');
            });
            return true;
        });
    }
    
});

$(".all").click(function () {
    var div_name = $(this).val();
    if ($(this).is(":checked")) {
        $('.chk_group').prop("checked", true);
    }
    else {
        $('.chk_group').prop("checked", false);
    }
});

$("#fk_contact").change(function() {
    $('.chk_group').prop("checked", false);
    var fk_contact = $(this).val();
    var fk_agent = $('#fk_agent').val();
    if (fk_contact != "") {
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "group_agent_contact/ajax_fetch_by_contact",
            dataType: "json",
            data: {
                fk_contact: fk_contact,
                fk_agent: fk_agent
            }
        }).done(function (value) {
            $(value).each(function (i, val) {
                $("#group_" + val.fk_group).prop("checked", true);
            });

        });
    }
    
});

$("#all_contacts").click(function () {
    if ($(this).is(":checked")) {
        $('.chk_contact').prop("checked", true);
    }
    else {
        $('.chk_contact').prop("checked", false);
    }
});
