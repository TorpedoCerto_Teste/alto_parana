$("#my-awesome-dropzone").dropzone({
    maxFiles: 2000,
    success: function (file, response) {
        var obj = JSON.parse(response);
        
        if (obj.error == false) { // succeeded
            return file.previewElement.classList.add("dz-success"); // from source
        } else {  //  error
            // below is from the source code too
            var node, _i, _len, _ref, _results;
            var message = obj.message // modify it to your error message
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        }
    }
});

