$(function() {
    if ($('#postal_code').length) {
        $('#postal_code').blur(function () {
             $("#state").addClass('spinner');
             $("#city").addClass('spinner');
             $("#district").addClass('spinner');
             $("#address").addClass('spinner');
            $.get("https://viacep.com.br/ws/"+$('#postal_code').val()+"/json/",
                function(data, status){
                    $("#state").val(data.uf).removeClass('spinner');
                    $("#city").val(data.localidade).removeClass('spinner');
                    $("#district").val(data.bairro).removeClass('spinner');
                    $("#address").val(data.logradouro).removeClass('spinner');
            });
            return true;
        });
    }

});