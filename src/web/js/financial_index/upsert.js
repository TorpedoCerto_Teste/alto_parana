get();

$("#month").change(function () {
    get();
});

$("#year").change(function () {
    get();
});

$('input[type=radio][name=type]').change(function () {
    get();
});

function get() {
    var month = $("#month").val();
    var year = $("#year").val();
    var type = $("input:checked").val();

    if (month > 0 && year > 0 && type != null) {
        $("#value").addClass('spinner');

        $.get($('#base_url').val() + '/' + month + '/' + year + '/' + type,
                function (data, status) {
                    $("#value").val(data).removeClass('spinner');
                });

    }
}