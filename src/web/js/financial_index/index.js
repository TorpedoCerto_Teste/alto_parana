$("#month").change(function () {
    get();
});

$("#year").change(function () {
    get();
});

$("#month_base").change(function () {
    get();
});

$("#year_base").change(function () {
    get();
});

$('input[type=radio][name=type]').change(function () {
    get();
});


$('#value').keyup(function () {
    show_hide_button();
});

function get() {
    var type = $("input:checked").val();

    var month = $("#month").val();
    var year = $("#year").val();

    if (month > 0 && year > 0 && type != null) {
        $("#index").addClass('spinner');

        $.get($('#base_url').val() + '/' + month + '/' + year + '/' + type,
                function (data, status) {
                    $("#index").val(data).removeClass('spinner');
                });

    }

    var month_base = $("#month_base").val();
    var year_base = $("#year_base").val();

    if (month_base > 0 && year_base > 0 && type != null) {
        $("#index_base").addClass('spinner');

        $.get($('#base_url').val() + '/' + month_base + '/' + year_base + '/' + type,
                function (data, status) {

                    $("#index_base").val(data).removeClass('spinner');
                });

    }

}

function calculate() {
    var index = $("#index").val();
    var index_base = $("#index_base").val();
    var value = $("#value").val();
    
    if (index && index_base && value) {
        var ind = index.replace(',', '.');
        var ind_base = index_base.replace(',', '.');
        var val = value.replace(',', '.');
        
        var variable = (ind/ind_base);
        var percent = ((ind/ind_base)-1)*100;
        var result = variable * val;
        $('#variable').val(percent.toFixed(3).replace('.', ','));
        $('#result').val(result.toFixed(3).replace('.', ','));
    }
    else {
        alert('Valores inconsistentes! Verifique...');
    }
    
}

function show_hide_button() {


}