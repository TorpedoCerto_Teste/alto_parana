$("input[name='year']").TouchSpin({});

jQuery(document).ready(function () {
    $('.wysihtml5').wysihtml5();
});

$("#chk_all").click(function () {
    var checked = $("#chk_all").is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});

$("#fk_agent").change(function () {
    find_pen001();
});

$("#month").change(function () {
    find_pen001();
});

$("#year").blur(function () {
    find_pen001();
});

$(".bootstrap-touchspin-down").click(function () {
    find_pen001();
});

$(".bootstrap-touchspin-up").click(function () {
    find_pen001();
});



function find_pen001() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "") {
        $("#upsert").val("false")
        $("#frm_pen001").submit();
    }
}

function find_unities(fk_agent) {
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity/ajax_fetch_unity_from_agent",
        dataType: "json",
        data: {
            fk_agent: fk_agent
        },
        beforeSend: function (xhr) {
            clean_table();
        }
    }).done(function (value) {
        generate_table(value);
    });
}

function clean_table() {
    $("#div_unity").hide();
    $("#tbl_unity").html("");
}

function generate_table(value) {
    var table = "";
    $.each(value, function(i, val) {
        table += "<tr>";
        table += "<td><label class=\"checkbox-custom check-success\"><input type=\"checkbox\" class=\"chk\" value=\"" + val.pk_unity + "\" name=\"chk[]\" id=\"chk_" + val.pk_unity + "\" /><label for=\"chk_" + val.pk_unity + "\">&nbsp;</label>" + val.community_name + "</label></td>";
        table += "</tr>";
    });
    $("#tbl_unity").html(table);
    $("#div_unity").show();
}

$("#all").click(function () {
    $(".chk_contact").prop("checked", $("#all").prop("checked"));
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});