$("input[name='year']").TouchSpin({});

$(".reporting_date").blur(function () {
    var month = $(this).attr("month");
    var reporting_date = $(this).val();
    process_accounting_market(month, reporting_date, "reporting_date");
});

$(".debit_date").blur(function () {
    var month = $(this).attr("month");
    var debit_date = $(this).val();
    process_accounting_market(month, debit_date, "debit_date");
});

$(".credit_date").blur(function () {
    var month = $(this).attr("month");
    var credit_date = $(this).val();
    process_accounting_market(month, credit_date, "credit_date");
});

$(".gfn001_date").blur(function () {
    var month = $(this).attr("month");
    var gfn001_date = $(this).val();
    process_accounting_market(month, gfn001_date, "gfn001_date");
});

$(".warranty_limit_date").blur(function () {
    var month = $(this).attr("month");
    var warranty_limit_date = $(this).val();
    process_accounting_market(month, warranty_limit_date, "warranty_limit_date");
});

function process_accounting_market(month, value, field) {
    var year = $("#year").val();
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "accounting_market/ajax_upsert",
        data: {
            month: month,
            year: year,
            val: value,
            field: field
        }
    }).done(function (value) {
    });

}

$("#year").blur(function () {
    $(".form-horizontal").submit();
});

$(".bootstrap-touchspin-up").click(function () {
    $(".form-horizontal").submit();
});

$(".bootstrap-touchspin-down").click(function () {
    $(".form-horizontal").submit();
});