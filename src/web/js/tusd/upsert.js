$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    find_unities($(this).val());
});

$("#fk_unity").change(function () {
    find_tusd();
});

$("#month").change(function () {
    find_tusd();
});

$("#year").blur(function () {
    find_tusd();
});
$(".bootstrap-touchspin-down").click(function () {
    find_tusd();
});

$(".bootstrap-touchspin-up").click(function () {
    find_tusd();
});

$(function () {

    $('.calendar-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function (e) {
        var choose_date = $(this).val();
        var dt = choose_date.split("/").reverse().join("-");
        change_date(dt);
    });
});

function find_tusd() {
    var fk_agent = $("#fk_agent").val();
    var fk_unity = $("#fk_unity").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "" && fk_unity != "") {
        $("#upsert").val("false")
        $("#frm_tusd").submit();
    }
}


function find_unities(fk_agent) {
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity/ajax_fetch_unity_from_agent",
        dataType: "json",
        data: {
            fk_agent: fk_agent
        },
    }).done(function (values) {
        fill_select(values);
    });
}

function fill_select(values) {
    $('#fk_unity').children().remove().end();
    var selected = values.length === 1 ? 'selected' : '';
    $(values).each(function (i, val) {
        $('#fk_unity').append('<option value="' + val.pk_unity + '" ' + selected + '>' + val.community_name + '</option>');
    });
}
 
$("#all").click(function () {
    $(".chk_contact").prop("checked", !$(".chk_contact").prop("checked"));
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});
