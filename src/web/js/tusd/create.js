$(function () {

    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

});

$("#fk_unity").change(function () {
    get_demand();
    get_distributor();
});

$("#month").change(function () {
    get_demand();
    get_distributor();
});

$("#year").blur(function () {
    get_demand();
    get_distributor();
});


function get_demand() {
    $("#error_demand").hide();
    var fk_unity = $("#fk_unity").val();

    if (fk_unity > 0) {
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "demand/ajax_find_unity",
            dataType: "json",
            data: {
                fk_unity: fk_unity,
                year: $("#year").val(),
                month: $("#month").val()
            }
        }).done(function (value) {
            if (value) {
                $("#tension").val(value._tension);
                $("#modality").val(value._tariff_mode);
                $("#contracted_demand_end").val(value._end_demand);
                $("#contracted_demand_tip_out").val(value._demand_tip_out);
            } else {
                $("#error_demand").fadeIn("slow");
                $("#tension").val("");
                $("#modality").val("");
                $("#contracted_demand_end").val("");
                $("#contracted_demand_tip_out").val("");
            }
        });
    }
}

function get_distributor() {
    var pk_unity = $("#fk_unity").val();

    if (pk_unity > 0) {
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "unity/ajax_find_unity",
            dataType: "json",
            data: {
                pk_unity: pk_unity
            }
        }).done(function (value) {
            var json = $.parseJSON(JSON.stringify(value));
            $("#power_distributor").html(json[0].power_distributor_name);
        });
    }

}

$("#show_hide_uploader").click(function () {
    $('#div_upload').toggle();
});

$("#my-awesome-dropzone").dropzone({
    maxFiles: 2000,
    success: function (file, response) {
        var obj = JSON.parse(response);

        if (obj.error == false) { // succeeded
            return file.previewElement.classList.add("dz-success"); // from source
        } else {  //  error
            // below is from the source code too
            var node, _i, _len, _ref, _results;
            var message = obj.message // modify it to your error message
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        }
    }
});

$(".delete_file").click(function () {
    var tr = $(this).attr("key");
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "tusd/ajax_delete_file",
        dataType: "json",
        data: {
            file_name: $(this).attr("value")
        }
    }).done(function (value) {
        $("#tr_" + tr).remove();
        if (value <= 0) {
            $("#alert_temp_files").hide();
        }
    });
});
 