$(function () {
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });


    $('input[name=first_adjustment]').click(function () {
        var first_adjustment = $('input[name=first_adjustment]:checked').val();
        if (first_adjustment == 0) {
            $("#div_date_adjustment").show();
        } else {
            $("#div_date_adjustment").hide();
        }
    });

    $("input[name=different_price_period]").click(function () {
        var different_price_period = $('input[name=different_price_period]:checked').val();

        if (different_price_period == 0) {
            $("#div_not_different_price_period").show();
            $("#div_different_price_period").hide();
        } else {
            $("#div_not_different_price_period").hide();
            $("#div_different_price_period").show();
            //ajax para pegar a quantidade de campos referentes ao perído de fornecimento de energia
            $.ajax({
                method: "POST",
                url: $('#base_url').val() + "ccvee_payment/ajax_get_supply_period/"+$('#fk_ccvee_general').val(),
                data: {
                    first_adjustment: $('input[name=first_adjustment]:checked').val(),
                    date_adjustment: $('#date_adjustment').val(),
                    date_adjustment_frequency: $('#date_adjustment_frequency').val()
                }
            }).done(function (value) {
                generate_table_payment(value);
            });
            
            
        }
    });

});


function generate_table_payment(value) {
    var table = "";
    
    table += "  <table class=\"table responsive-data-table data-table\">"+
                    "<thead>"+
                        "<tr>"+
                            "<td>Período</td>"+
                            "<td>Previsto/Base</td>"+
                            "<td>Corrigido/Reajustado</td>"+
                            "<td>Ajustado Manualmente</td>"+
                            "<td>Previsão de Preço Ajustado</td>"+
                            "<td>Apontamento de Mês Reajuste</td>"+
                            "<td>Copiar</td>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var json = $.parseJSON(value);
    $('#total_months').val(json.length);
    var checked = 0;
    $(json).each(function (i, val) {
        checked = parseInt(val.checked) === 1 ? "checked" : "";
        table +=        "<tr>"+
                            "<td><input type=\"text\" placeholder=\"\" id=\"period"+i+"\" name=\"payment_prices["+i+"][period]\" class=\"form-control\" value=\""+val.periodo+"\" ></td>"+
                            "<td><input type=\"text\" placeholder=\"\" id=\"provided_base"+i+"\" name=\"payment_prices["+i+"][provided_base]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>"+
                            "<td><input type=\"text\" placeholder=\"\" id=\"fixed"+i+"\" name=\"payment_prices["+i+"][fixed]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>"+
                            "<td><input type=\"text\" placeholder=\"\" id=\"manual_fixed"+i+"\" name=\"payment_prices["+i+"][manual_fixed]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>"+
                            "<td><input type=\"text\" placeholder=\"\" id=\"provided_fixed"+i+"\" name=\"payment_prices["+i+"][provided_fixed]\" class=\"form-control\" value=\"\" alt=\"valor4\"></td>"+
                            "<td><input type=\"checkbox\" placeholder=\"\" id=\"point_monthly"+i+"\" name=\"payment_prices["+i+"][point_monthly]\" class=\"form-control\" value=\"1\" "+checked+"></td>"+
                            "<td><button data-toggle=\"button\" class=\"btn btn-sm btn-default copy\" value=\"copy"+i+"\" onclick=\"copy_values_payment("+i+")\"><i class=\"fa fa-copy\"></i></button></td>"+
                        "</tr>";
    });
    table +=        "</tbody>" +
                "</table>";

    $('#table_payment_prices').html(table);
    $('input:text').setMask();
}


function copy_values_payment(position) {
    var total_months = $("#total_months").val();
    var provided_base = $("#provided_base" + position).val();
    var fixed = $("#fixed" + position).val();
    var manual_fixed = $("#manual_fixed" + position).val();
    var provided_fixed = $("#provided_fixed" + position).val();

    for (var i = position; i <= total_months; i++) {
        $("#total_months" + i).val(total_months);
        $("#provided_base" + i).val(provided_base);
        $("#fixed" + i).val(fixed);
        $("#manual_fixed" + i).val(manual_fixed);
        $("#provided_fixed" + i).val(provided_fixed);
    }
}