jQuery(document).ready(function () {
    
    $('.wysihtml5').wysihtml5();
    
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    
});
