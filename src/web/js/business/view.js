jQuery(document).ready(function () {

    $('.wysihtml5').wysihtml5();

    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
});

$('.faturamento').DataTable({
    "PaginationType": "bootstrap",
    responsive: true,
    dom: '<"tbl-top clearfix"lfr>,t,<"tbl-footer clearfix"<"tbl-info pull-left"i><"tbl-pagin pull-right"p>>',
    "oLanguage": {
        "sLengthMenu": "_MENU_ itens por página",
        "sZeroRecords": "Nenhum registro",
        "sInfo": "_START_ até _END_ para o total de _TOTAL_",
        "sInfoEmpty": "Nenhum registro",
        "sInfoFiltered": "(filtrados de _MAX_ itens)",
        "sSearch": "Pesquisar"
    },
    "aaSorting": [[0, "desc"]]    
});    


$("#all").click(function () {
    var checked = $(this).is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});

$("#unity_differentiated_duration").click(function () {
    var unity_differentiated_duration = $("#unity_differentiated_duration").is(":checked") ? 1 : 0;
    
    if (unity_differentiated_duration == 1) {
        $("#div_unity_differentiated_duration").fadeIn();
    } else {
        $("#div_unity_differentiated_duration").fadeOut();
    }

    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "business/ajax_process_unity_differentiated_duration",
        data: {
            unity_differentiated_duration: unity_differentiated_duration,
            pk_business: $('#pk_business').val()
        }
    }).done(function (value) {
    });
});


$("#remuneration_type").change(function () {
    only_div_active($("#remuneration_type").val());
    

});

$("#adjustment_montly_manually").click(function() {
    if ($("#adjustment_montly_manually").is(":checked")) {
        $("#div_date_adjustment").show();
    }
    else {
        $("#div_date_adjustment").hide();
    }
});


$("#closed_installment").click(function () {
    if ($("#closed_installment").is(":checked")) {
        $("#div_closed_installment_quantity").show();
    }
    else {
        $("#div_closed_installment_quantity").hide();
    }
});

function only_div_active(div_name) {
    var options = [
        "mensal_por_unidade",
        "percentual_da_economia",
        "valor_sobre_a_energia",
        "valor_fechado",
        "mensal_por_contraparte",
        "valor_fixo_mais_percentual_de_economia"
    ];

    for (i = 0; i < options.length; i++) {
        $("#div_"+options[i]).hide();
    }
    $("#div_"+div_name).fadeIn("slow");
}


// funções para serviços
$("#fk_service").change(function () {
    $(".prd").prop("checked", false);
    $("#div_personalizar").hide();
    var fk_service = $("#fk_service").val();
    if (fk_service !== "") {
        //ajax para pegar os IDs que serão marcados nos checkboxes
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "service_product/ajax_get_service_product",
            dataType: "json",
            data: {
                fk_service: fk_service
            }
        }).done(function (value) {
            $(value).each(function (i, val) {
                $("#product_" + val.fk_product).prop("checked", true);
            });
            $("#div_personalizar").show();

        });
    }
});

$(".btn-personalizar").click(function () {
    enable_checkboxes();
    $("#personalized").val("1");
});

$(".category").click(function () {
    var div_name = $(this).val();
    if ($(this).is(":checked")) {
        $('.' + div_name).prop("checked", true);
    } else {
        $('.' + div_name).prop("checked", false);
    }
});


function enable_checkboxes() {
    $(".prd").prop("disabled", false);
}

$('#amount').blur(function () {
    calculate_total();
});

$('#discount').blur(function () {
    calculate_total();
});

$('#total').blur(function () {
    calculate_total();
});

function calculate_total() {
    var amount = $('#amount').val();
    amount = amount.replaceAll('.', '').replace(',', '.');
    var discount = $('#discount').val();
    discount = discount.replaceAll('.', '').replace(',', '.');
    var total = (parseFloat(amount) - parseFloat(discount)).toFixed(2);
    total = total < 0 ? 0 : total;
    var input = $('#total');
    $('#total').val(total).setMask(input.data('mask'));
}