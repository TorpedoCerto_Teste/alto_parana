$("input[name='year']").TouchSpin({});

$(".reporting_date").blur(function () {
    var month = $(this).attr("month");
    var reporting_date = $(this).val();
    process_ercap_market(month, reporting_date, "reporting_date");
});

$(".payment_date").blur(function () {
    var month = $(this).attr("month");
    var payment_date = $(this).val();
    process_ercap_market(month, payment_date, "payment_date");
});

$(".value").blur(function () {
    var month = $(this).attr("month");
    var value = $(this).val();
    process_ercap_market(month, value, "value");
});

function process_ercap_market(month, value, field) {
    var year = $("#year").val();
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "ercap_market/ajax_upsert",
        data: {
            month: month,
            year: year,
            val: value,
            field: field
        }
    }).done(function (value) {
    });

}