$("#fk_service").change(function () {
    $(".prd").prop("checked", false);
    $("#div_personalizar").hide();
    var fk_service = $("#fk_service").val();
    if (fk_service !== "") {
        //ajax para pegar os IDs que serão marcados nos checkboxes
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "service_product/ajax_get_service_product",
            data: {
                fk_service: fk_service
            }
        }).done(function (value) {
            $(value).each(function (i, val) {
                $("#product_" + val.fk_product).prop("checked", true);
            });
            $("#div_personalizar").show();

        });



    }
});

$(".btn-personalizar").click(function () {
    enable_checkboxes();
    $("#personalized").val("1");
});

$(".category").click(function () {
    var div_name = $(this).val();
    if ($(this).is(":checked")) {
        $('.' + div_name).prop("checked", true);
    } else {
        $('.' + div_name).prop("checked", false);
    }
});


function enable_checkboxes() {
    $(".prd").prop("disabled", false);
}