$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    var fk_agent = $("#fk_agent").val();
    if (parseInt(fk_agent) > 0) {
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "telemetry_comparison/ajax_fetch_gauge_by_fk_agent",
            dataType: "json",
            data: {
                fk_agent: fk_agent
            }
        }).done(function (value) {
            process_result(value);
        });
    }
});


$("#fk_gauge").change(function () {
    $("#frm_telemetry_comparison").submit();
});

$("#month").change(function () {
    if ($('#fk_gauge').val()) {
        $("#frm_telemetry_comparison").submit();
    }
});

$("#year").blur(function () {
    if ($('#fk_gauge').val()) {
        $("#frm_telemetry_comparison").submit();
    }
});

$(".bootstrap-touchspin-down").click(function () {
    if ($('#fk_gauge').val()) {
        $("#frm_telemetry_comparison").submit();
    }
});

$(".bootstrap-touchspin-down").click(function () {
    if ($('#fk_gauge').val()) {
        $("#frm_telemetry_comparison").submit();
    }
});

$("#btn-export").click(function() {
    $("#export_excel").val("true");
    $("#frm_telemetry_comparison").submit();
});

function process_result(value) {
    if (value) {
        $("#fk_gauge").children().remove().end().append("<option></option>");
        $(value).each(function (i, val) {
            $("#fk_gauge").append("<option value=\"" + val.pk_gauge + "\">" + val.internal_name + "</option>");
        });
    }
}