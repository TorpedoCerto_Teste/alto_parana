$("input[name='year']").TouchSpin({});

$(".bootstrap-touchspin-down").click(function () {
    $("#frm_pen001_market").submit();
});

$(".bootstrap-touchspin-up").click(function () {
    $("#frm_pen001_market").submit();
});

//  payment_slip_date
//  validate
//  total_market

$(".payment_slip_date").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'payment_slip_date';
    upsert_payment_slip_ccee_market(month, value, field);
});

$(".validate").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'validate';
    upsert_payment_slip_ccee_market(month, value, field);
});

$(".total_market").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'total_market';
    upsert_payment_slip_ccee_market(month, value, field);
});

function upsert_payment_slip_ccee_market(month, value, field) {
    var year = $("#year").val();

    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "payment_slip_ccee_market/ajax_upsert_payment_slip_ccee_market",
        dataType: "json",
        data: {
            year: year,
            month: month,
            value: value,
            field: field
        }
    }).done(function (value) {
    });

}