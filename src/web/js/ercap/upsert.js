jQuery(document).ready(function () {
    var fk_agent = $("#fk_agent").val();
    if (parseInt(fk_agent) > 0) {
        find_unities(fk_agent);
        find_ercap();
    }
});

$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    var fk_agent = $("#fk_agent").val();
    if (parseInt(fk_agent) > 0) {
        submit_form();
    } else {
        clean_table();
    }
});

$("#chk_all").click(function () {
    var checked = $("#chk_all").is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});

$("#month").change(function () {
    submit_form();
});

$("#year").blur(function () {
    submit_form();
});

$(".bootstrap-touchspin-down").click(function() { submit_form();});

$(".bootstrap-touchspin-down").click(function() { submit_form();});

function submit_form() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (parseInt(fk_agent) > 0 && parseInt(month) > 0 && parseInt(year) > 0) {
        $("#upsert").val("false");
        $("#frm_ercap").submit();        
    }
}

function find_ercap() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    $("#value").val("0,00");
    $("#pk_ercap").val("");
    $(".chk").prop("checked", false);

    if (parseInt(fk_agent) > 0 && parseInt(month) > 0 && parseInt(year) > 0) {
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "ercap/ajax_find",
            dataType: "json",
            data: {
                fk_agent: fk_agent,
                month: month,
                year: year
            }
        }).done(function (value) {
            process_result(value);
        });
    }
}

function find_unities(fk_agent) {
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity/ajax_fetch_unity_from_agent",
        dataType: "json",
        data: {
            fk_agent: fk_agent
        },
        beforeSend: function (xhr) {
            clean_table();
        }
    }).done(function (value) {
        generate_table(value);
    });
}

function clean_table() {
    $("#div_unity").hide();
    $("#tbl_unity").html("");
}

function generate_table(value) {
    var table = "";
    $.each(value, function (i, val) {
        table += "<tr>";
        table += "<td><label class=\"checkbox-custom check-success\"><input type=\"checkbox\" class=\"chk\" value=\"" + val.pk_unity + "\" name=\"chk[]\" id=\"chk_" + val.pk_unity + "\" /><label for=\"chk_" + val.pk_unity + "\">&nbsp;</label>" + val.community_name + "</label></td>";
        table += "<td>" + format_date(val.free_market_entry) + "</td>";
        table += "</tr>";
    });
    $("#tbl_unity").html(table);
    $("#div_unity").show();
}

function format_date(dateTime) {
    var retrn = "sem data informada";
    if (dateTime) {
        var date = new Date(dateTime);
        retrn = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    }
    return retrn;
}

function process_result(value) {
    if (value) {
        $("#pk_ercap").val(value.ercap._pk_ercap);
        $("#value").val(parseFloat(value.ercap._value).toFixed(2).replace(".", ","));

        $(value.ercap_apportionment).each(function (i, val) {
            $("#chk_" + val.fk_unity).prop("checked", true);
        });
    }
}

$("#all").click(function () {
    var checked = $(this).is(":checked");
    if (checked) {
        $(".chk_contact").prop("checked", true);
    } else {
        $(".chk_contact").prop("checked", false);
    }
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});
