
var data7_4 = [{
    data:  [
        [1000000, 20],
        [2000000, 70],
        [3000000, 130],
        [4000000, 93],
        [5000000, 148],
        [6000000, 175],
        [7000000, 50]
   ],
    label: "Sul"
},
{
    data:  [
        [1000000, 50],
        [2000000, 90],
        [3000000, 30],
        [4000000, 193],
        [5000000, 48],
        [6000000, 75],
        [7000000, 150]
    ],
    label: "Sudeste"
},
{
    data:  [
        [1000000, 90],
        [2000000, 10],
        [3000000, 60],
        [4000000, 93],
        [5000000, 148],
        [6000000, 115],
        [7000000, 90]
    ],
    label: "Norte"
}
];

var x = $('#graph').html();

$(function() {
    $.plot($("#multi-sates #multi-states-container"),
        x ,
        {
            series: {
                lines: {
                    show: true,
                    fill: false
                },
                points: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: "#ffffff",
                    symbol: "circle",
                    radius: 5
                },
                shadowSize: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#dddddd"
            },
            xaxis: {
//                mode: "categories"

            },
            yaxes: [{
                /* First y axis */
            }, {
                /* Second y axis */
                position: "right" /* left or right */
            }]
        }
    );
});
