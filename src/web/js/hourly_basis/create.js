$('#from').blur(function () {
    var date = $('#from').val();
    if (date != "") {
        var segments = date.split("/");
        var date1 = new Date(segments[2], segments[1], segments[0]);
        date1.setDate(date1.getDate()+5);
        var day = date1.getDate() >= 10 ? date1.getDate() : '0' + date1.getDate();
        var month = date1.getMonth() >= 10 ? date1.getMonth() : '0' + date1.getMonth();        
        $('#to').val(day + '/' + month + '/' + date1.getFullYear());
    }
});