$(function () {
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    if ($('#postal_code').length) {
        $('#postal_code').blur(function () {
            $("#state").addClass('spinner');
            $("#city").addClass('spinner');
            $("#district").addClass('spinner');
            $("#address").addClass('spinner');
            $.get("https://viacep.com.br/ws/" + $('#postal_code').val() + "/json/",
                    function (data, status) {
                        $("#state").val(data.uf).removeClass('spinner');
                        $("#city").val(data.localidade).removeClass('spinner');
                        $("#district").val(data.bairro).removeClass('spinner');
                        $("#address").val(data.logradouro).removeClass('spinner');
                    });
            return true;
        });
    }

});

jQuery(document).ready(function () {

    $('.wysihtml5').wysihtml5();

    window.onkeydown = function (e) {
        if (e.keyCode == 32 && e.target == document.body) {
            e.preventDefault();
        }
    };

    $("input[name='icms']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

    $("input[name='credit_icms']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

    $('.iCheck-flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue',
        increaseArea: '20%' // optional
    });

    $("#same_address").click(function () {
        var addr = JSON.parse($('#agent_address').val());
        $('#postal_code').val(addr._postal_code);
        $('#address').val(addr._address);
        $('#number').val(addr._number);
        $('#complement').val(addr._complement);
        $('#district').val(addr._district);
        $('#city').val(addr._city);
        $('#state').val(addr._state);
        return false;
    });

});
