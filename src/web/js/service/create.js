$(".category").click(function () {
    var div_name = $(this).val();
    if ($(this).is(":checked")) {
        $('.'+div_name).prop("checked", true);
    }
    else {
        $('.'+div_name).prop("checked", false);
    }
});


//update
$("#pk_service").change(function () {
    $(".prd").prop("checked", false);
    $("#div_service").hide();
    var pk_service = $("#pk_service").val();
    if (pk_service !== "") {
        $("#div_service").show();
        $("#service").val($("#pk_service :selected").text());
        //ajax para pegar os IDs que serão marcados nos checkboxes
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "service_product/ajax_get_service_product",
            data: {
                fk_service: pk_service
            }
        }).done(function (value) {
            $(value).each(function (i, val) {
                $("#product_" + val.fk_product).prop("checked", true);
            });

        });
    }
});
