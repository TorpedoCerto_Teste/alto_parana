$(function () {
    $('#div_birthdate_as_password').hide();
    
    if ($('#allow_access').length) {
        $('#allow_access').click(function () {
            $('#div_password').toggle('slow');
        });
    }

    $("#password").keyup(function () {
        $("#password_text").val($(this).val());
    });
    $("#password_text").keyup(function () {
        $("#password").val($(this).val());
    });

    $(".mostrar").click(function () {
        $("#div_asterisk").toggle();
        $("#div_text").toggle();
    });

    $('#birthdate').on("blur", function() {
        if ($(this).val().length === 10) {
            $('#div_birthdate_as_password').show();
        } else {
            $('#div_birthdate_as_password').hide();
        }
    });
    
    if ($('#birthdate_as_password').length) {
        $('#birthdate_as_password').click(function () {
            if ($(this).is(':checked')) {
                $('#allow_access').prop("checked", true);
                $('#div_password').toggle('slow');
                $('#password').val($('#birthdate').val().replace(/\D+/g, ''));
                if ($('#password_text').length) {
                    $("#password_text").val($('#birthdate').val().replace(/\D+/g, ''));
                }
                console.log($("#password_text").val(), $('#birthdate').val().replace(/\D+/g, ''));
            } else {
                $('#allow_access').prop("checked", false);
            }
        });
    }

});
