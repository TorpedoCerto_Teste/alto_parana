$(function () {
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $("input[name='seasonal_inferior']").TouchSpin({
        min: -100,
        max: 0,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $("input[name='seasonal_superior']").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

    $("input[name='flexibility_inferior']").TouchSpin({
        min: -100,
        max: 0,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $("input[name='flexibility_superior']").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

});



$('#differentiated_volume_period').click(function () {
    generate_volume();
});

$('.calcular').click(function () {
    generate_volume();
});

$("#seasonality").click(function () {
    generate_seasonality();
});

$("#flexibility").click(function () {
    process_flexibility();
});


$("input[name=modulation_radio]").click(function () {
    process_modulation();
});

$("#unit_measurement").change(function () {
    change_unit_measurement($(this).val());
});

$("#losses_type").change(function () {
    if ($("#losses_type").val() == "valor_unico") {
        $("#div_losses_unique").show();
    }
    else {
         $("#div_losses_unique").hide();
    }
    generate_volume();
});

//verificar data inicial e final do campo de fornecimento e gerar os campos para períodos
function generate_volume() {
    var start = $('#supply_start').val();
    var end = $('#supply_end').val();
    var pk_ccvee_energy = $('#pk_ccvee_energy').val();

    if (start == "") {
        $('#supply_start').addClass("error");
    } else {
        $('#supply_start').removeClass("error");
    }

    if (end == "") {
        $('#supply_end').addClass("error");
    } else {
        $('#supply_end').removeClass("error");
    }

    if (start != "" && end != "") {
        var diff = $("#differentiated_volume_period").is(":checked");
        if (diff) {
            $.ajax({
                method: "POST",
                url: $('#base_url').val() + "ccvee_energy/ajax_proccess_supply",
                data: {
                    supply_start: start,
                    supply_end: end,
                    pk_ccvee_energy: pk_ccvee_energy
                }
            }).done(function (value) {
                generate_table(value);
            });

        } else {
            generate_table(null);
        }

    }
}

function generate_table(value) {
    var table = "";

    var diff = $("#differentiated_volume_period").is(":checked");

    if (diff) {
        table += "<table class=\"table responsive-data-table data-table\">" +
                "<thead>" +
                "<tr>" +
                "<th>Período</th>" +
                "<th>Previsto</th>" +
                "<th>Sazonalizado</th>" +
                "<th>Ajustado</th>";
        if ($("#losses_type").val() === "manual") {
            table += "<th>% Perdas</th>";
        }
        table += 
                "<th>Copiar</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>";

        var json = $.parseJSON(value);
        $('#total_months').val(json.length);
        $(json).each(function (i, val) {
            table += "<tr>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"periodo" + i + "\" name=\"volume[" + i + "][periodo]\" class=\"form-control\" value=\"" + val.periodo + "\" disabled >" +
                    "<input type=\"hidden\" placeholder=\"\" id=\"period" + i + "\" name=\"volume[" + i + "][period]\" class=\"form-control\" value=\"" + val.periodo + "\"    ></td>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"forecast" + i + "\" name=\"volume[" + i + "][forecast]\" class=\"form-control\" value=\""+val.forecast+"\" alt=\"valor3\" ></td>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"seasonal" + i + "\" name=\"volume[" + i + "][seasonal]\" class=\"form-control\" value=\""+val.seasonal+"\" alt=\"valor3\" ></td>" +
                    "<td><input type=\"text\" placeholder=\"\" id=\"adjusted" + i + "\" name=\"volume[" + i + "][adjusted]\" class=\"form-control\" value=\""+val.adjusted+"\" alt=\"valor3\" ></td>";
            if ($("#losses_type").val() === "manual") {
                table += "<td><input type=\"text\" placeholder=\"\" id=\"losses" + i + "\" name=\"volume[" + i + "][losses]\" class=\"form-control\" value=\""+val.losses+"\" alt=\"valor2\" ></td>";
            }

            table +=
                    "<td>" +
                    "<button data-toggle=\"button\" class=\"btn btn-sm btn-default copy\" value=\"copy" + i + "\" onclick=\"copy_values(" + i + ")\">" +
                    "<i class=\"fa fa-copy\"></i>" +
                    "</button>" +
                    "</td>" +
                    "</tr>";
        });
        table += " </tbody>" +
                "</table>";

    } else {
        table += "<table class=\"table responsive-data-table data-table\">" +
                "<thead>" +
                "<tr>" +
                "<th>Previsto</th>" +
                "<th>Sazonalizado</th>" +
                "<th>Ajustado</th>";
        if ($("#losses_type").val() === "manual") {
            table += "<th>% Perdas</th>";
        }
        table+= 
            "</tr>" +
                "</thead>" +
                "<tbody>";
        table += "<tr>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"forecast\" name=\"forecast\" class=\"form-control\" value=\"\" alt=\"valor3\" ></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"seasonal\" name=\"seasonal\" class=\"form-control\" value=\"\" alt=\"valor3\" ></td>" +
                "<td><input type=\"text\" placeholder=\"\" id=\"adjusted\" name=\"adjusted\" class=\"form-control\" value=\"\" alt=\"valor3\" ></td>";
        if ($("#losses_type").val() === "manual") {
            table += "<td><input type=\"text\" placeholder=\"\" id=\"losses\" name=\"losses\" class=\"form-control\" value=\"\" alt=\"valor2\" ></td>";
        }
        table += "</tr>" +
                "</tbody>" +
                "</table>";
    }
    $('#volumes').html(table);
    $('input:text').setMask();
}

function copy_values(position) {
    var total_months = $("#total_months").val();
    var forecast = $("#forecast" + position).val();
    var seasonal = $("#seasonal" + position).val();
    var adjusted = $("#adjusted" + position).val();
    var losses   = $("#losses" + position).val();

    for (var i = position; i <= total_months; i++) {
        $("#forecast" + i).val(forecast);
        $("#seasonal" + i).val(seasonal);
        $("#adjusted" + i).val(adjusted);
        $("#losses" + i).val(losses);
    }
}

function generate_seasonality() {
    var seasonality = $("#seasonality").is(":checked");

    if (seasonality) {

        var start = $('#supply_start').val();
        var end = $('#supply_end').val();
        var pk_ccvee_energy = $('#pk_ccvee_energy').val();

        if (start == "") {
            $('#supply_start').addClass("error");
        } else {
            $('#supply_start').removeClass("error");
        }

        if (end == "") {
            $('#supply_end').addClass("error");
        } else {
            $('#supply_end').removeClass("error");
        }

        if (start != "" && end != "") {
            $.ajax({
                method: "POST",
                url: $('#base_url').val() + "ccvee_energy/ajax_proccess_seasonal",
                data: {
                    supply_start: start,
                    supply_end: end,
                    pk_ccvee_energy: pk_ccvee_energy
                }
            }).done(function (value) {
                generate_table_seasonal(value);
            });

            $("#seasonality_div").show();
        }




    } else {
        $("#seasonal_limit").html();
        $("#seasonality_div").hide();
    }
}

function generate_table_seasonal(value) {
    var table = "";
    table += "<table class=\"table responsive-data-table data-table\">" +
            "<thead>" +
            "<tr>" +
            "<th>Ano</th>" +
            "<th>Data limite para informar sazonalidade</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody>";

    var json = $.parseJSON(value);
    $('#total_months').val(json.length);
    $(json).each(function (i, val) {
        table +=    "<tr>" +
                    "<td class=\"col-lg-2\">" +
                    "<input type=\"text\"  placeholder=\"\" id=\"seasonal_limit_year_show\" name=\"seasonal_limit_year_show\" class=\"form-control\" value=\"" + val.year + "\" disabled=\"disabled\" >" +
                    "<input type=\"hidden\" placeholder=\"\" id=\"seasonal_limit_year\" name=\"seasonal_limit[" + i + "][year]\" class=\"form-control\" value=\"" + val.year + "\" >" +
                    "</td>" +
                    "<td class=\"col-lg-4\">" +
                    "<input type=\"text\" class=\"form-control default-date-picker\"  id=\"seasonal_limit_limit\" name=\"seasonal_limit[" + i + "][limit]\"  alt=\"date\" value=\"" + val.limit + "\">" +
                    "</td>" +
                    "</tr>";
    });
    table += " </tbody>" +
            "</table>";

    $("#seasonal_limit").html(table);
    $('input:text').setMask();
    $('.default-date-picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

}

function process_flexibility() {
    var flexibility = $("#flexibility").is(":checked");
    if (flexibility) {
        $("#flexibility_div").show();
    } else {
        $("#flexibility_div").hide();
    }
}

function process_modulation() {
    var modulation = $("input[name=modulation_radio]:checked").val();
    if (modulation == "modulada") {
        $("#modulation_div").show();
    } else {
        $("#modulation_div").hide();
    }
}

function change_unit_measurement(unit) {
    var diff = $("#differentiated_volume_period").is(":checked");
    var total_months = $("#total_months").val();
    if (diff) {
        var start = $('#supply_start').val();
        var end = $('#supply_end').val();

        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "ccvee_energy/ajax_proccess_unit_measurement",
            data: {
                supply_start: start,
                supply_end: end,
                unit_measurement: unit
            }
        }).done(function (value) {
            //generate_table(value);
        });


    }
}