$("input[name='year']").TouchSpin({});
$("#fk_agent").change(function () {
    find_free_captive();
});

$("#month").change(function () {
    find_free_captive();
});

$("#year").blur(function () {
    find_free_captive();
});

$(".bootstrap-touchspin-down").click(function () {
    find_free_captive();
});

$(".bootstrap-touchspin-up").click(function () {
    find_free_captive();
});

function find_free_captive() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "") {
        $("#upsert").val("false")
        $("#frm_free_captive").submit();
    }
}

$(".economy").blur(function () {
    var pk_unity = $(this).attr("pk_unity");
    calculate_values(pk_unity);
});
$(".captive_total").blur(function () {
    var pk_unity = $(this).attr("pk_unity");
    calculate_values(pk_unity);
});
$(".free_total").blur(function () {
    var pk_unity = $(this).attr("pk_unity");
    calculate_values(pk_unity);
});

function calculate_values(pk_unity) {
    var economy = $("#economy_" + pk_unity).val();
    var captive_total = $("#captive_total_" + pk_unity).val();
    var free_total = $("#free_total_" + pk_unity).val();
    economy = economy.replace(",", ".");
    captive_total = captive_total.replace(",", ".");
    free_total = free_total.replace(",", ".");

    if (economy <= 0 && captive_total > 0 && free_total > 0) {
        economy = captive_total - ((free_total * 100) / captive_total);
        economy = economy.toFixed(2);
        economy = economy.replace(".", ",");
        $("#economy_" + pk_unity).val(economy);
    } else if (economy > 0 && captive_total > 0 && free_total <= 0) {
        free_total = captive_total - ((captive_total * economy) / 100);
        free_total = free_total.toFixed(2);
        free_total = free_total.replace(".", ",");
        $("#free_total_" + pk_unity).val(free_total);
    } else if (economy > 0 && captive_total <= 0 && free_total > 0) {
        captive_total = free_total + ((free_total * economy) / 100);
        captive_total = captive_total.toFixed(2);
        captive_total = captive_total.replace(".", ",");
        $("#captive_total_" + pk_unity).val(captive_total);
    }
}

$("#all").click(function () {
    $(".chk_contact").prop("checked", !$(".chk_contact").prop("checked"));
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});
