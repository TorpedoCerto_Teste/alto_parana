$("input[name='year']").TouchSpin({});

$(".bootstrap-touchspin-down").click(function () {
    $("#frm_pen001_market").submit();
});

$(".bootstrap-touchspin-up").click(function () {
    $("#frm_pen001_market").submit();
});



$(".reporting_date").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'reporting_date';
    upsert_pen001_market(month, value, field);
});

$(".value_reference").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'value_reference';
    upsert_pen001_market(month, value, field);
});

$(".PLD_average").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'PLD_average';
    upsert_pen001_market(month, value, field);
});

function upsert_pen001_market(month, value, field) {
    var year = $("#year").val();

    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "pen001_market/ajax_upsert_pen001_market",
        dataType: "json",
        data: {
            year: year,
            month: month,
            value: value,
            field: field
        }
    }).done(function (value) {
    });

}