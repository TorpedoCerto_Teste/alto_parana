$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    find_lfpen001();
});

$("#month").change(function () {
    find_lfpen001();
});

$("#year").blur(function () {
    find_lfpen001();
});

$(".bootstrap-touchspin-down").click(function () {
    find_lfpen001();
});

$(".bootstrap-touchspin-up").click(function () {
    find_lfpen001();
});


function find_lfpen001() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "") {
        $("#upsert").val("false")
        $("#frm_lfpen001").submit();
    }
}

$("#all").click(function () {
    $(".chk_contact").prop("checked", !$(".chk_contact").prop("checked"));
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});