$(function() {
    $("input[name='year']").TouchSpin({});

    $('#year').blur(function() {
        make_ajax();
    });
    $('.bootstrap-touchspin-up').click(function() {
        make_ajax();
    });
    $('.bootstrap-touchspin-down').click(function() {
        make_ajax();
    });
});

function make_ajax() {
    location.href = `//${location.host}/pld/upsert/${$('#year').val()}`;
}