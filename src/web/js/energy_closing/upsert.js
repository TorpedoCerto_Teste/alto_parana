$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    find_energy_closing();
});

$("#month").change(function () {
    find_energy_closing();
});

$("#year").blur(function () {
    find_energy_closing();
});

$(".bootstrap-touchspin-down").click(function () {
    find_energy_closing();
});

$(".bootstrap-touchspin-up").click(function () {
    find_energy_closing();
});

function find_energy_closing() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "") {
        $("#upsert").val("false")
        $("#frm_energy_closing").submit();
    }
}

