$("input[name='year']").TouchSpin({});
$("#fk_agent").change(function () {
    find_payment_slip_ccee();
});

$("#month").change(function () {
    find_payment_slip_ccee();
});

$("#year").blur(function () {
    find_payment_slip_ccee();
});

$(".bootstrap-touchspin-down").click(function () {
    find_payment_slip_ccee();
});

$(".bootstrap-touchspin-up").click(function () {
    find_payment_slip_ccee();
});

function find_payment_slip_ccee() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    if (fk_agent != "" && month != "" && year != "") {
        $("#upsert").val("false")
        $("#frm_payment_slip_ccee").submit();
    }
}

$("#chk_all").click(function () {
    var checked = $("#chk_all").is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});


$("#all").click(function () {
    $(".chk_contact").prop("checked", $("#all").prop("checked"));
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});