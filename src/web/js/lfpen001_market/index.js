$("input[name='year']").TouchSpin({});

$(".bootstrap-touchspin-down").click(function () {
    $("#frm_pen001_market").submit();
});

$(".bootstrap-touchspin-up").click(function () {
    $("#frm_pen001_market").submit();
});



$(".reporting_date").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'reporting_date';
    upsert_lfpen001_market(month, value, field);
});

$(".payment_date").blur(function () {
    var value = $(this).val();
    var month = $(this).attr("month");
    var field = 'payment_date';
    upsert_lfpen001_market(month, value, field);
});

function upsert_lfpen001_market(month, value, field) {
    var year = $("#year").val();

    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "lfpen001_market/ajax_upsert_lfpen001_market",
        dataType: "json",
        data: {
            year: year,
            month: month,
            value: value,
            field: field
        }
    }).done(function (value) {
    });

}