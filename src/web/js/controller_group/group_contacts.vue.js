let groupContacts = new Vue({
    el: "#app",
    data: {
        contacts: [],
        groupsList: [],
        selectedGroup: '',
        agentId: 0,
        unityId: 0,
        baseUrl: '',
        controllerName: ''
    },
    mounted() {
        this.baseUrl = this.$refs.base_url.value;
        this.controllerName = this.$refs.controller_name.value;
        this.fetchGroups();
    },
    methods: {
        fetchGroups() {
            let self = this;
            const url = `${this.baseUrl}Controller_group/ajax_get_groups`;

            const formData = new FormData();
            formData.append('controller_name', self.controllerName);

            axios.post(url, formData)
                .then(response => {
                    if (typeof response.data !== 'undefined' && response.data.length) {
                        self.groupsList = response.data;
                    }
                })
                .catch(error => {
                    console.error('Erro ao receber dados:', error);
                });
        },


        fetchData() {
            this.agentId = typeof this.$refs.fk_agent !== 'undefined' && typeof this.$refs.fk_agent.value !== 'undefined' ?  this.$refs.fk_agent.value : 0;
            this.unityId = typeof this.$refs.fk_unity !== 'undefined' && typeof this.$refs.fk_unity.value !== 'undefined' ?  this.$refs.fk_unity.value : 0;

            if (parseInt(this.agentId) > 0 || (parseInt(this.agentId) === 0 && parseInt(this.unityId) > 0)) {
                const url = `${this.baseUrl}Controller_group/ajax_get_contact_by_group`;
                
                const formData = new FormData();
                formData.append('fk_group_controller', this.selectedGroup);
                formData.append('fk_agent', this.agentId);
                if (parseInt(this.unityId) > 0) {
                    formData.append('fk_unity', this.unityId);
                }
            
                axios.post(url, formData)
                    .then(response => {
                        if (response.data && response.data.contacts && response.data.contacts.length > 0) {
                            this.contacts = response.data.contacts;
                        } else {
                            this.contacts = [];
                        }
                    })
                    .catch(error => {
                        console.error('Erro ao receber dados:', error);
                    });
            } 
        }
    },
    watch: {
        groupsList(newVal) {
            newVal.forEach(obj => {
                if (parseInt(obj.selected) === 1) {
                    this.selectedGroup = parseInt(obj.pk_group);
                }
            })
        },

        selectedGroup(newVal, oldVal) {
            if (newVal !== oldVal) 
                this.fetchData();
        }
    }
});
