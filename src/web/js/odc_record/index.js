// Load google charts
google.charts.load('current', {'packages': ['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
    var options = {
        legend: {position: 'bottom', maxLines: 3},
        bar: {groupWidth: '65%'},
        colors: ['#69c2fe', '#5fc29d', '#f3b49f', '#FFD700', '#f6c7b6'],
        seriesType: 'bars',
        chartArea: {
            left: "5%",
            top: "5%",
            width: "100%"
        },
        isStacked: true
    };
    var chart_data = $("#chart_data").html();
    var data = new google.visualization.DataTable(JSON.parse(chart_data));
    var chart = new google.visualization.ColumnChart(document.getElementById('columnChart'));
    chart.draw(data, options);
}
