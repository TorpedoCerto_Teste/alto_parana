$('#date').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '0d'
});

$('.responsive-data-table').DataTable({
    "PaginationType": "bootstrap",
    responsive: true,
    dom: '<"tbl-top clearfix"lfr>,t,<"tbl-footer clearfix"<"tbl-info pull-left"i><"tbl-pagin pull-right"p>>',
    "oLanguage": {
        "sLengthMenu": "_MENU_ itens por página",
        "sZeroRecords": "Nenhum registro",
        "sInfo": "_START_ até _END_ para o total de _TOTAL_",
        "sInfoEmpty": "Nenhum registro",
        "sInfoFiltered": "(filtrados de _MAX_ itens)",
        "sSearch": "Pesquisar"
    },
    "aaSorting": [[0, "desc"]],
    "aoColumns": [
        {
            "bSearchable": false,
            "bVisible": false
        },
        /* ano/mes*/
        {
            "iDataSort": 0 //ordena pela coluna anterior
        },
        null,
        null,
        null,
        {
            "bSearchable": false,
            "bVisible": true
        }
    ]
    
});

$('#test').on("change", function() {
    $("#div_test_email").removeClass("has-error");
    $("#alert_success").addClass("hidden");
    $("#alert_danger").addClass("hidden");

    if ($("#test").is(":checked")) {
        $("#agendamento").fadeOut();
        $("#div_test_email").removeClass("hidden");
        $("#footer_agendar").addClass("hidden");
        $("#footer_test").removeClass("hidden");
    } else {
        $("#div_test_email").addClass("hidden");
        $("#agendamento").fadeIn();
        $("#footer_agendar").removeClass("hidden");
        $("#footer_test").addClass("hidden");
    }
});

$("#btn_test").on("click", function() {
    var email = $("#test_email").val();
    $("#alert_success").addClass("hidden");
    $("#alert_danger").addClass("hidden");
    $(this).attr("disabled", "disabled");
    if (validateEmail(email) && email.trim() !== "") {
        var data = {
            "email": email,
            "pk_energy_market": $('#pk_energy_market_schedule').val()
        };
        $("#div_test_email").removeClass("has-error");

        $.post("/api/test_energy_market", data, function(res) {
            if (typeof res.status !== 'undefined' && res.status === "success") {
                $("#alert_success").removeClass("hidden");
            } else {
                $("#alert_danger").removeClass("hidden");
            }
            $("#btn_test").removeAttr("disabled");
        });

    } else {
        $("#div_test_email").addClass("has-error");
    }
});

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}