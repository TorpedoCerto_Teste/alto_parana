$("input[name='year']").TouchSpin({});
$("input[name='week']").TouchSpin({});

var vm = new Vue({
    el: "#sendmail",
    data: {
        agents: {},
        pk_agent: null,
        contacts: {},
        checkAll: false,
    },
    created() {
        fetch('../../api/agents')
                .then(response => response.json())
                .then(json => {
                    this.agents = json
                })
    },
    methods: {
    },
    watch: {
        pk_agent() {
            fetch('../../api/contacts/'+this.pk_agent)
                .then(response => response.json())
                .then(json => {
                    this.contacts = json
                })
        },
        checkAll() {
            for (var i in this.contacts) {
                this.contacts[i].isChecked = this.checkAll;
            }
        }
    }
});