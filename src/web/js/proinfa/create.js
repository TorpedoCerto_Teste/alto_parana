$(function () {
    $('#year').blur(function () {
        for (var i = 1; i < 13; i++) {
            $('#value_mwh_'+i).val('0.00').addClass('spinner');
        }
        $.get($('#base_url').val() + '/' + $('#year').val(),
                function (data, status) {
                    data = JSON.parse(data);
                    for (var k in data) {
                        if (data.hasOwnProperty(k)) {
                            $('#value_mwh_'+k).val(data[k]['value_mwh']).removeClass('spinner');
                        }
                    }
                });
        return true;
    });
});