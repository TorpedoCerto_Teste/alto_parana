$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    //ajax pegar a lista de unidades deste agente
    var fk_agent = $("#fk_agent").val();
    if (parseInt(fk_agent) > 0) {
        find_unities(fk_agent);
    } else {
        clean_table();
    }

//imprimir as unidades na table
});

$("#chk_all").click(function () {
    var checked = $("#chk_all").is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});


function find_unities(fk_agent) {
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity/ajax_fetch_unity_from_agent",
        data: {
            fk_agent: fk_agent
        },
        beforeSend: function (xhr) {
            clean_table();
        }
    }).done(function (value) {
        generate_table(value);
    });
}

function clean_table() {
    $("#div_unity").hide();
    $("#tbl_unity").html("");
}

function generate_table(value) {
    var table = "";
    var json = $.parseJSON(value);
    $(json).each(function (i, val) {
        table += "<tr>";
        table += "<td><label class=\"checkbox-custom check-success\"><input type=\"checkbox\" class=\"chk\" value=\"" + val.pk_unity + "\" name=\"chk[]\" id=\"chk_" + val.pk_unity + "\" /><label for=\"chk_" + val.pk_unity + "\">&nbsp;</label>" + val.community_name + "</label></td>";
        table += "<td>" + format_date(val.free_market_entry) + "</td>";
        table += "</tr>";
    });
    $("#tbl_unity").html(table);
    $("#div_unity").show();
}

function format_date(dateTime) {
    var retrn = "sem data informada";
    if (dateTime) {
        var date = new Date(dateTime);
        retrn = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    }
    return retrn;
}
