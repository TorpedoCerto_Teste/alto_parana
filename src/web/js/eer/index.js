$(".vlr").blur(function () {
  var pk_eer = $(this).attr("pk_eer");
  var value = $(this).val();

  $.ajax({
    method: "POST",
    url: $("#base_url").val() + "eer/ajax_update",
    data: {
      pk_eer: pk_eer,
      value: value,
    },
  }).done(function (value) {});
});

$(".btn_confirm").click(function () {
  var all = $(".vlr")
    .map(function () {
      $(this).val("0,00");
      return $(this).attr("pk_eer");
    })
    .get();

  $.ajax({
    method: "POST",
    url: $("#base_url").val() + "eer/ajax_empty_value",
    data: {
      pk_eer: all.join(),
    },
  }).done(function (value) {
    $("#delete").modal("hide");
  });
});

$(".redirect").click(function () {
  var data = $(this).data();
  $.redirectPost("/eer/upsert", data);
});

// jquery extend function
$.extend({
  redirectPost: function (location, args) {
    var form = "";
    $.each(args, function (key, value) {
      value = value;
      form += '<input type="hidden" name="' + key + '" value="' + value + '">';
    });
    $('<form action="' + location + '" method="POST">' + form + "</form>")
      .appendTo($(document.body))
      .submit();
  },
});

$(document).ready(function () {
  $(".data-table").DataTable({
    PaginationType: "bootstrap",
    responsive: true,
    dom:
      '<"tbl-top clearfix"lfr>,t,<"tbl-footer clearfix"<"tbl-info pull-left"i><"tbl-pagin pull-right"p>>',
    order: [[0, "desc"]],
  });
});
