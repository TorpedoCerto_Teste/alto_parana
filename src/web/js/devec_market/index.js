$(".chk").click(function () {
    var state = $(this).attr("state");
    upsert(state);
});

$(".start_date").blur(function () {
    var state = $(this).attr("state");
    upsert(state);
});

function upsert(state) {
    var devec = $("#devec_"+state).is(":checked") ? 1 : 0;
    var start_date = $("#start_date_"+state).val();
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "devec_market/ajax_upsert",
        data: {
            uf: state,
            start_date: start_date,
            devec: devec
        }
    });
}