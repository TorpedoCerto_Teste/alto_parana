$(function() {
    var updating = $("#updating").val();
    if (parseInt(updating) > 0) {
        find_unities($("#fk_agent").val());
        find_accounting();
    }
});

$("input[name='year']").TouchSpin({});

$("#fk_agent").change(function () {
    var fk_agent = $("#fk_agent").val();
    if (parseInt(fk_agent) > 0) {
        find_unities(fk_agent);
        find_accounting();
    } else {
        clean_table();
    }
});

$("#month").change(function () {
    find_accounting();
});

$("#year").blur(function () {
    find_accounting();
});

$(".bootstrap-touchspin-down").click(function() {
    find_accounting();
});

$(".bootstrap-touchspin-up").click(function() {
    find_accounting();
});

function find_unities(fk_agent) {
    $.ajax({
        method: "POST",
        url: $('#base_url').val() + "unity/ajax_fetch_unity_from_agent",
        dataType: "json",
        data: {
            fk_agent: fk_agent
        },
        beforeSend: function (xhr) {
            clean_table();
        }
    }).done(function (value) {
        generate_table(value);
    });
}

function clean_table() {
    $("#div_unity").hide();
    $("#tbl_unity").html("");
}

function generate_table(value) {
    var table = "";
    $(value).each(function (i, val) {
        table += "<tr>";
        table += "<td><label class=\"checkbox-custom check-success\"><input type=\"checkbox\" class=\"chk\" value=\"" + val.pk_unity + "\" name=\"chk[]\" id=\"chk_" + val.pk_unity + "\" /><label for=\"chk_" + val.pk_unity + "\">&nbsp;</label>" + val.community_name + "</label></td>";
        table += "<td>" + format_date(val.free_market_entry) + "</td>";
        table += "</tr>";
    });
    $("#tbl_unity").html(table);
    $("#div_unity").show();
}

function format_date(dateTime) {
    var retrn = "sem data informada";
    if (dateTime) {
        var date = new Date(dateTime);
        retrn = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    }
    return retrn;
}

$("#all").click(function () {
    var checked = $(this).is(":checked");
    if (checked) {
        $(".chk_email").prop("checked", true);
    } else {
        $(".chk_email").prop("checked", false);
    }
});

$("#chk_all").click(function () {
    var checked = $(this).is(":checked");
    if (checked) {
        $(".chk").prop("checked", true);
    } else {
        $(".chk").prop("checked", false);
    }
});

$("#test").click(function() {
    var status = $(this).is(":checked");
    if (status) {
        if (localStorage.getItem('test_email')) {
            $('#test_email').val(localStorage.getItem('test_email'));
        }
        $('#div_test_email').removeClass('hidden');
        $('#email_list').addClass('hidden');
    }
    else {
        $('#div_test_email').addClass('hidden');
        $('#email_list').removeClass('hidden');
    }
});

$('#test_email').blur(function() {
    localStorage.setItem('test_email', $(this).val());
});

function find_accounting() {
    var fk_agent = $("#fk_agent").val();
    var month = $("#month").val();
    var year = $("#year").val();
    
    clean_fields();
    
    if (parseInt(fk_agent) > 0 && parseInt(month) > 0 && parseInt(year) > 0) {
        $.ajax({
            method: "POST",
            url: $('#base_url').val() + "accounting/ajax_find",
            dataType: "json",
            data: {
                fk_agent: fk_agent,
                month: month,
                year: year
            }
        }).done(function (value) {
            process_result(value);
        });
    }
}

function clean_fields() {
    $("#pk_accounting").val("");
    $("#value").val("0,00");
    $("#value_paid").val("0,00");
    $("#ess").val("0,00");
    $("#previous_summary").val("0,00");
    $("#mcp").val("0,00");
    $("#adjust").val("0,00");
    $("#total").val("0,00");
    $(".chk").prop("checked", false);
    $(".alert").fadeOut();
    $("#div_rel_gfn001").html('<span class="help-block">Relatório GFN001.PDF</span><input type="file" id="rel_gfn001" name="rel_gfn001" class="form-control" value="" />');
    $("#div_mem_gfn001").html('<span class="help-block">Mem. Calc. GFN001.PDF</span><input type="file" id="mem_gfn001" name="mem_gfn001" class="form-control" value="" />');
    $("#div_sum001").html('<span class="help-block">SUM001.PDF</span><input type="file" id="sum001" name="sum001" class="form-control" value="" />');
    $("#div_lfn002").html('<span class="help-block">LFN002.PDF</span><input type="file" id="lfn002" name="lfn002" class="form-control" value="" />');
}

function process_result(value) {
    if (value) {
        $("#pk_accounting").val(value.accounting._pk_accounting);
        $("#value").val(parseFloat(value.accounting._value).toFixed(2).replace(".", ","));
        $("#ess").val(parseFloat(value.accounting._ess).toFixed(2).replace(".", ","));
        $("#previous_summary").val(parseFloat(value.accounting._previous_summary).toFixed(2).replace(".", ","));
        $("#mcp").val(parseFloat(value.accounting._mcp).toFixed(2).replace(".", ","));
        $("#adjust").val(parseFloat(value.accounting._adjust).toFixed(2).replace(".", ","));
        $("#total").val(parseFloat(value.accounting._total).toFixed(2).replace(".", ","));
        $("#type").val(value.accounting._type);
        $("#value_paid").val(parseFloat(value.accounting._value_paid).toFixed(2).replace(".", ","));
        
        $(value.accounting_apportionment).each(function (i, val) {
            $("#chk_"+val.fk_unity).prop("checked", true);
        });
        
        if (value.accounting._rel_gfn001 !== "" && value.accounting._rel_gfn001 !== null) {
            $("#div_rel_gfn001").html('<a href="https://docs.google.com/viewer?embedded=true&url='+$('#base_url').val()+'uploads/accounting/'+value.accounting._pk_accounting+'/'+value.accounting._rel_gfn001+'" class="btn btn-info" target="_blank" >'+value.accounting._rel_gfn001+'</a>');
        }
        if (value.accounting._mem_gfn001 !== "" && value.accounting._mem_gfn001 !== null) {
            $("#div_mem_gfn001").html('<a href="https://docs.google.com/viewer?embedded=true&url='+$('#base_url').val()+'uploads/accounting/'+value.accounting._pk_accounting+'/'+value.accounting._mem_gfn001+'" class="btn btn-info" target="_blank" >'+value.accounting._mem_gfn001+'</a>');
        }
        if (value.accounting._sum001 !== "" && value.accounting._sum001 !== null) {
            $("#div_sum001").html('<a href="https://docs.google.com/viewer?embedded=true&url='+$('#base_url').val()+'uploads/accounting/'+value.accounting._pk_accounting+'/'+value.accounting._sum001+'" class="btn btn-info" target="_blank" >'+value.accounting._sum001+'</a>');
        }
        if (value.accounting._lfn002 !== "" && value.accounting._lfn002 !== null) {
            $("#div_lfn002").html('<a href="https://docs.google.com/viewer?embedded=true&url='+$('#base_url').val()+'uploads/accounting/'+value.accounting._pk_accounting+'/'+value.accounting._lfn002+'" class="btn btn-info" target="_blank" >'+value.accounting._lfn002+'</a>');
        }
    }
}