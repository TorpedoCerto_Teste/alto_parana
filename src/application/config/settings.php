<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
//dados da APE
$config["config_phone"]                = "41 99267-3065";
$config["pk_agent_type_distribuidora"] = 6;
$config["pk_agent_type_representante"] = 7;
$config["measurement_unity"]           = "MWh";
$config["way2_token"]                  = "2d6a0dca-bf91-4f93-8088-a0b588cbc1c6";
$config["colors"][1]                   = "danger"; //vermelho
$config["colors"][2]                   = "info"; //azul
$config["colors"][3]                   = "warning"; //amarelo
$config["colors"][4]                   = "success"; //verde
$config["colors"][5]                   = "primary"; //roxo
$config["colors"][6]                   = "default"; //cinza
$config["url_sendmail"]                = "http://localhost:5000/sendmail";

