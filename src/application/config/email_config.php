<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['remetente']    = 'Alto Paraná Energia';
$config['email_debug']  = false; //set true to don't send email to anyone, except test account
$config['to']           = ['comercial@altoparanaenergia.com.br', 'comercial@altoparanaenergia.com']; //destinatario que controla todos os emails enviados - deve estar em array
$config['reply_to']     = ['comercial@altoparanaenergia.com']; 
$config['protocol']     = 'smtp';
$config['smtp_host']    = 'smtp.zoho.com';
$config['smtp_port']    = '587';
$config['smtp_timeout'] = '7';
$config['smtp_user']    = 'comercial@altoparanaenergia.com.br';
$config['smtp_pass']    = 'yePjPS9i6qd7';
$config['smtp_crypto']  = 'tls';
$config['charset']      = 'utf-8';
$config['newline']      = "\r\n";
$config['mailtype']     = 'html'; // text or html
$config['validate']     = TRUE; // bool whether to validate email or not
$ci                     = & get_instance();
$ci->load->library('email');
$ci->email->initialize($config);
