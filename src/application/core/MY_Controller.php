<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

}

class Customer_Controller extends MY_Controller {

    public $message_total_unread;
    public $all_message;
    public $agents_list;
    public $fk_agent;

    function __construct() {
        parent::__construct();

        $this->load->model('Message_agent_model');
        $this->Message_agent_model->_fk_agent = $this->session->userdata('fk_agent');

        //pega o total de mensagens não lidas
        $this->Message_agent_model->_status = $this->Message_agent_model->_status_active;
        $this->message_total_unread         = $this->Message_agent_model->get_total();

        //pega todas as mensagens, limitadas pelas 5 últimas
        $this->Message_agent_model->_status = "";
        $this->all_message                  = $this->Message_agent_model->fetch_by_agent();
        
        //seta a lista de agentes
        $this->fk_agent = $this->session->userdata('fk_agent');
        $this->agents_list = $this->session->userdata('agents_list');
    }

}

class Public_Controller extends MY_Controller {
    use commom_controller;
}

class Group_Controller extends MY_Controller {

    use commom_controller;

    public $controller_group;

    function __construct() {
        parent::__construct();

        $this->controller_group = $this->_get_controller_group();
    }

    private function _get_controller_group() {
        $return                     = array();
        $this->load->model("Group_model");
        $this->Group_model->_status = $this->Group_model->_status_active;
        $groups                     = $this->Group_model->fetch();

        if ($groups) {
            $this->load->model("Controller_group_model");
            $this->Controller_group_model->_status     = $this->Controller_group_model->_status_active;
            $this->Controller_group_model->_controller = $this->router->fetch_class();
            $read                                      = $this->Controller_group_model->read();

            foreach ($groups as $key => $group) {
                $return[$key]             = $group;
                $return[$key]['selected'] = $group['pk_group'] == $this->Controller_group_model->_fk_group ? 1 : 0;
            }
        }
        return $return;
    }
    
    //métodos utilizados por vários controllers
    public function _process_group_contact($fk_agent) {
        $return   = false;
        $contacts = $this->_process_contact($fk_agent);
        if ($contacts) {
            foreach ($contacts as $key => $contact) {
                $return[$key]             = $contact;
                $return[$key]['selected'] = 0;
            }
            $group_agents = $this->_process_group_agent($fk_agent);
            if ($group_agents) {
                foreach ($return as $k => $ret) {
                    foreach ($group_agents as $group_agent) {
                        if ($group_agent['fk_contact'] == $ret['pk_contact'] && $group_agent['fk_group'] == $this->Controller_group_model->_fk_group) {
                            $return[$k]['selected'] = 1;
                        }
                    }
                }
            }
        }
        return $return;
    }
    
    private function _process_contact($fk_agent) {
        $this->load->model("Contact_model");
        $this->Contact_model->_status   = $this->Contact_model->_status_active;
        $this->Contact_model->_fk_agent = $fk_agent;
        return $this->Contact_model->fetch_to();
    }
    
    private function _process_group_agent($fk_agent) {
        $this->load->model("Group_agent_contact_model");
        $this->Group_agent_contact_model->_status   = $this->Group_agent_contact_model->_status_active;
        $this->Group_agent_contact_model->_fk_agent = $fk_agent;
        return $this->Group_agent_contact_model->fetch();
    }

    public function _fetch_email_notification($year, $month, $fk_agent) {
        $this->load->model("Email_notification_model");
        $this->Email_notification_model->_reference  = $year . "-" . $month . "-01";
        $this->Email_notification_model->_fk_agent   = $fk_agent;
        $this->Email_notification_model->_controller = $this->router->fetch_class();
        return $this->Email_notification_model->fetch();
    }
    
    public function _get_agent($pk_agent) {
        $this->load->model("Agent_model");
        $this->Agent_model->_status   = $this->Agent_model->_status_active;
        $this->Agent_model->_pk_agent = $pk_agent;
        return $this->Agent_model->read();
    }

    public function _get_unity($pk_unity) {
        $this->load->model("Unity_model");
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $this->Unity_model->_pk_unity = $pk_unity;
        return $this->Unity_model->read();
    }

    public function _get_contact($pk_contact, $fk_agent) {
        $this->load->model("Contact_model");
        $this->Contact_model->_status     = $this->Contact_model->_status_active;
        $this->Contact_model->_pk_contact = $pk_contact;
        $this->Contact_model->_fk_agent   = $fk_agent;
        return $this->Contact_model->read();
    }
    
    public function _process_email_notification($year, $month) {
        $this->load->model("Email_notification_model");
        $this->Email_notification_model->_reference  = $year . "-" . $month . "-01";
        $this->Email_notification_model->_fk_agent   = $this->Agent_model->_pk_agent;
        $this->Email_notification_model->_fk_contact = $this->Contact_model->_pk_contact;
        $this->Email_notification_model->_controller = $this->router->fetch_class();
        $this->Email_notification_model->_created_at = date("Y-m-d H:i:s");
        $this->Email_notification_model->create();
    }
    
    public function _process_agents_group() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }
    
    
}

trait commom_controller {

    public $events;
    public $colors;

    function __construct() {
        parent::__construct();

        $this->colors = $this->config->item("colors");

        $this->events = $this->_get_calendar();

    }

    private function _get_calendar() {
        $this->load->model('Calendar_model');
        $this->Calendar_model->_event_date = date("Y-m-d");
        return $this->Calendar_model->fetch_by_day();
    }

}
