<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Liquidação de Penalidades da CCEE</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Liquidação de Penalidades da CCEE </p>
        <p align="justify">Prezados, encaminhamos em anexo e disponibilizamos na <a href="<?= base_url() ?>customer/lfpen001" target="_blank">área do cliente</a> o relatório de Liquidação de Penalidades da CCEE. </p>            
        <?php if (!$this->Lfpen001_model->_penalty_not_apply || $this->Lfpen001_model->_total > 0) : ?>
            <b>Para este ciclo de cobrança, existem penalidades a serem pagas pelo agente <?= $this->Agent_model->_community_name ?>, <br>e que podem ser identificadas em detalhes por meio dos relatórios PEN001 enviados anteriormente, e também disponíveis em sua </b> <a href="<?= base_url() ?>customer/lfpen001" target="_blank">área do cliente</a>.            
            <br/>
            <br/>
            <b>Importante:</b> o valor devido deverá ser transferido para sua conta da Ag. Trianon do Banco Bradesco, conforme os dados e prazos abaixo:
            <br/>
            <br/>
            <br/>
            <table style="border:0px; width: 100%">
                <tr>
                    <td width="300px">Agente</td>
                    <td><?= $this->Agent_model->_community_name ?></td>
                </tr>
                <tr>
                    <td>Ciclo de cobrança</td>
                    <td><?= $this->Lfpen001_model->_month ?>/<?= $this->Lfpen001_model->_year ?></td>
                </tr>
                <tr>
                    <td>Valor (R$)</td>
                    <td><?= number_format($this->Lfpen001_model->_total, 2, ",", ".") ?></td>
                </tr>
                <tr>
                    <td>Data para pagamento</td>
                    <td><?= date_to_human_date($this->Lfpen001_model->_payment_date) ?></td>
                </tr>
            </table>
        <?php endif; ?>
        <?php if ($this->Lfpen001_model->_penalty_not_apply || $this->Lfpen001_model->_total == 0) : ?>
            <b>Para este ciclo de cobrança, não foram apuradas penalidades a serem pagas pelo agente <?= $this->Agent_model->_community_name ?></b>.
        <?php endif; ?>
        <br/>
        <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?= $this->config->item("config_phone") ?> e em nosso atendimento on-line, via chat da área do cliente.</p>
        <br/>
        <br/>
        <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
        <div>
            <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
            <a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
        </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>
    </body>
</html>