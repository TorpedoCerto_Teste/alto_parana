<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Submercados
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>submarket">Submercados</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Submercado
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>submarket/update/<?= $this->Submarket_model->_pk_submarket ?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <div class="form-group <?= form_error('initials') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="initials">Iniciais</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Iniciais" id="initials" name="initials" class="form-control" value="<?= set_value("initials") != "" ? set_value("initials") : $this->Submarket_model->_initials ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('submarket') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="submarket">Submercado</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Submercado" id="submarket" name="submarket" class="form-control" value="<?= set_value("submarket") != "" ? set_value("submarket") : $this->Submarket_model->_submarket ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
