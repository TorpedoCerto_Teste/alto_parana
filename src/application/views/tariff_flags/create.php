<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Nova Bandeira Tarifária
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Nova</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Nova Bandeira Tarifária
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>tariff_flags/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>tariff_flags/ajax/" disabled=""/>
                        
                        
                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                    <input id="year"
                                           type="text"
                                           value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>"
                                           name="year"
                                           data-bts-min="0"
                                           data-bts-max="9999"
                                           data-bts-init-val=""
                                           data-bts-step="1"
                                           data-bts-decimal="0"
                                           data-bts-force-step-divisibility="round"
                                           data-bts-prefix=""
                                           data-bts-postfix=""
                                           data-bts-prefix-extra-class=""
                                           data-bts-postfix-extra-class=""
                                           data-bts-booster="true"
                                           data-bts-boostat="10"
                                           data-bts-max-boosted-step="false"
                                           data-bts-mousewheel="true"
                                           data-bts-button-down-class="btn btn-default"
                                           data-bts-button-up-class="btn btn-default"
                                           alt="year" 
                                            />
                            </div>
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Mês</th>
                                    <th>Bandeira</th>
                                    <th>Acréscimo</th>
                                    <th>Copiar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($tariffs as $month => $tariff) : ?>
                                    <tr>
                                        <td><span class="label label-<?=$tariff['color'] ?>" id="month_<?=$month?>" name="month_<?=$month?>"><?= retorna_mes($month)?></span></td>
                                        <td>
                                            <select class="form-control select" name="flag_<?=$month?>" id="flag_<?=$month?>">
                                                <option value="VERDE" <?= set_value('flag_'.$month) == 'VERDE' || $tariff['flag'] == 'VERDE' ? 'selected' : ''?>>VERDE</option>
                                                <option value="AMARELA" <?= set_value('flag_'.$month) == 'AMARELA' || $tariff['flag'] == 'AMARELA' ? 'selected' : ''?>>AMARELA</option>
                                                <option value="VERMELHA" <?= set_value('flag_'.$month) == 'VERMELHA' || $tariff['flag'] == 'VERMELHA' ? 'selected' : ''?>>VERMELHA</option>
                                                <option value="VERMELHA II" <?= set_value('flag_'.$month) == 'VERMELHA II' || $tariff['flag'] == 'VERMELHA II' ? 'selected' : ''?>>VERMELHA II</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" id="increase_<?=$month?>" name="increase_<?=$month?>" class="form-control" value="<?= set_value('increase_'.$month) > 0 ? set_value('increase_'.$month) : number_format($tariff['increase'],2, ',', '') ?>" alt="valor">
                                        </td>
                                        <td>
                                            <button data-toggle="button" class="btn btn-sm btn-default copy" value="<?=$month?>">
                                                <i class="fa fa-copy"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
