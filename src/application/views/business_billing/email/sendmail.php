<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Faturamento de Serviços</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Faturamento de Serviços </p>
            <p> <b>Atenção!</b> Já estão disponíveis, em anexo à este e-mail e na <a href="<?=base_url()?>customer/business/billing">área do cliente</a>, os arquivos referentes ao faturamento dos serviços de Gestão e Representação na CCEE, conforme os dados abaixo:
            </p>
            <table style="border:0px; width: 100%">
                <tr>
                    <td width="300px">Referência:</td>
                    <td><?= $month ?>/<?= $year?></td>
                </tr>
                <tr>
                    <td width="300px">Unidade:</td>
                    <td><?= $unity_name ?></td>
                </tr>
                <tr>
                    <td>Vencimento:</td>
                    <td><?= date_to_human_date($expiration_date) ?></td>
                </tr>
                <?php if ($amount > 0) : ?>
                    <?php if ($discount > 0) : ?>
                        <tr>
                            <td>Valor:</td>
                            <td>R$ <?= number_format($amount,2,",","") ?></td>
                        </tr>
                        <tr>
                            <td>Desconto:</td>
                            <td>-R$ <?= number_format($discount,2,",","") ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Total a pagar:</td>
                        <td>R$ <?= number_format($total,2,",","") ?></td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <td>Valor:</td>
                        <td>R$ <?= number_format($total,2,",","") ?></td>
                    </tr>
                <?php endif; ?>
            </table>
        <br/>
        <?php if ($payment_method == 'deposito') : ?>
            <br/>
            <div style="text-align: justify">
                <p>Para realizar o pagamento, use os dados do QRCODE abaixo, a CHAVE PIX, ou os dados bancários para depósito.</p>
                <br/>
                <p><b>Dados Bancários para Depósito:</b></p>
                <p>Banco 341 - Itaú</p>
                <p>Agência 4015</p>
                <p>Conta 39795-5</p>
                <br/>
                <p><b>Chave Pix: CNPJ 24.782.611/0001-05</b></p>
                <br/>
                <img src="https://www.altoparanaenergia.com.br/img/pix.png" alt="">
            </div>
        <?php endif; ?>
        <br/>
              <div style="text-align: justify"> Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?=$this->config->item("config_phone")?> e em nosso atendimento on-line, via chat da área do cliente.</div>
		<br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a>
        <br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>

    </body>
</html>