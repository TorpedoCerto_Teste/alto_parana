<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Faturamento
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Faturamento</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Faturamento
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>business_billing/create/<?= $this->Business_billing_model->_pk_business_billing ?>'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo
                        </button>
                    </span>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Ano</th>
                                <th>Mês</th>
                                <th>Unidade</th>
                                <th>Vencimento</th>
                                <th>Valor</th>
                                <th>Desconto</th>
                                <th>Valor total</th>
                                <th>Método de pagamento</th>
                                <th>Status do pagamento</th>
                                <th>Data de pagamento</th>
                                <th>Boleto</th>
                                <th>NFE</th>
                                <th>XML</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['year'] ?></td>
                                    <td><?= $item['month'] ?></td>
                                    <td><?= $item['fk_unity'] ?></td>
                                    <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                    <td><?= $item['amount'] ?></td>
                                    <td><?= $item['discout'] ?></td>
                                    <td><?= $item['total'] ?></td>
                                    <td><?= $item['payment_method'] ?></td>
                                    <td><?= $item['payment_status'] ?></td>
                                    <td><?= date_to_human_date($item['payment_date']) ?></td>
                                    <td><?= $item['payment_slip'] ?></td>
                                    <td><?= $item['invoice'] ?></td>
                                    <td><?= $item['xml'] ?></td>

                                    <td class="hidden-xs">
                                        <a class="btn btn-success btn-xs" href = '<?= base_url() ?>business_billing/update/<?= $item['pk_business_billing'] ?>'><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_business_billing').val(<?= $item['pk_business_billing'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>business_billing/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_business_billing" id="pk_business_billing" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
