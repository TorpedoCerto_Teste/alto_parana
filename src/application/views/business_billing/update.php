<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Faturamento 
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>business_billing/index">Faturamento</a></li>
            <li class="active">Editar Faturamento</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Faturamento
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_billing/update/<?=$this->Business_billing_model->_pk_business_billing?>" enctype="multipart/form-data">
                        <input type="hidden" name="update" id="update" value="true" />

                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : $this->Business_billing_model->_year ?>" alt="year">
                            </div>
                        </div>
                        
                         <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-2">
                                <select id="month" name="month" class="form-control">
                                    <option value="01" <?= $this->Business_billing_model->_month == "01" ? "selected" : "" ?>>JANEIRO</option>
                                    <option value="02" <?= $this->Business_billing_model->_month == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                    <option value="03" <?= $this->Business_billing_model->_month == "03" ? "selected" : "" ?>>MARÇO</option>
                                    <option value="04" <?= $this->Business_billing_model->_month == "04" ? "selected" : "" ?>>ABRIL</option>
                                    <option value="05" <?= $this->Business_billing_model->_month == "05" ? "selected" : "" ?>>MAIO</option>
                                    <option value="06" <?= $this->Business_billing_model->_month == "06" ? "selected" : "" ?>>JUNHO</option>
                                    <option value="07" <?= $this->Business_billing_model->_month == "07" ? "selected" : "" ?>>JULHO</option>
                                    <option value="08" <?= $this->Business_billing_model->_month == "08" ? "selected" : "" ?>>AGOSTO</option>
                                    <option value="09" <?= $this->Business_billing_model->_month == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                    <option value="10" <?= $this->Business_billing_model->_month == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                    <option value="11" <?= $this->Business_billing_model->_month == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                    <option value="12" <?= $this->Business_billing_model->_month == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                </select>
                            </div>
                        </div>     
                         <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity" >
                                    <option></option>
                                    <?php foreach ($units2 as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= $this->Business_billing_model->_fk_unity == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                         <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Vencimento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control default-date-picker" value="<?= set_value('expiration_date') != "" ? set_value('expiration_date') : $this->Business_billing_model->_expiration_date ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('amount') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="amount">Valor</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="amount" name="amount" class="form-control" value="<?= set_value('amount') != "" ? set_value('amount') : $this->Business_billing_model->_amount ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('discount') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="discount">Desconto</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="discount" name="discount" class="form-control" value="<?= set_value('discount') != "" ? set_value('discount') : $this->Business_billing_model->_discount ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('total') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="total">Valor Total</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="total" name="total" class="form-control" value="<?= set_value('total') != "" ? set_value('total') : $this->Business_billing_model->_total ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_status') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_status">Status do pagamento</label>
                            <div class="col-lg-4">
                                <select id="payment_status" name="payment_status" class="form-control">
                                    <option value="programado" >PROGRAMADO</option>
                                    <option value="nao_programado" >NÃO PROGRAMADO</option>
                                    <option value="quitado" >QUITADO</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_date">Data de pagamento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="payment_date" name="payment_date" class="form-control default-date-picker" value="<?= set_value('payment_date') != "" ? set_value('payment_date') : $this->Business_billing_model->_payment_date ?>" alt="date">
                            </div>
                        </div>

                       <div class="form-group <?= form_error('payment_slip') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_slip">Boleto</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="payment_slip" name="payment_slip" class="form-control" value="<?= set_value('payment_slip') != "" ? set_value('payment_slip') : $this->Business_billing_model->_payment_slip?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('invoice') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="invoice">Nota Fiscal</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="invoice" name="invoice" class="form-control" value="<?= set_value('invoice') != "" ? set_value('invoice') : $this->Business_billing_model->_invoice?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('xml') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="xml">XML</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="xml" name="xml" class="form-control" value="<?= set_value('xml') != "" ? set_value('xml') : $this->Business_billing_model->_xml?>">
                            </div>
                        </div>                            

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
