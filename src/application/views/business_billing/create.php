<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Faturamento
    </h3>
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Faturamento
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_billing/create/" enctype="multipart/form-data">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= date("Y") ?>" alt="year">
                            </div>
                        </div>


                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-2">
                                <select id="month" name="month" class="form-control">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>JANEIRO</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>MARÇO</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>ABRIL</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>MAIO</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>JUNHO</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>JULHO</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>AGOSTO</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                </select>
                            </div>
                        </div>                    

                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($list_business_units as $unit) : ?>
                                        <option value="<?= $unit['fk_unity'] ?>" <?= set_value('pk_unity') == $unit['fk_unity'] ? 'selected' : "" ?>><?= $unit['community_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Vencimento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control default-date-picker" value="<?= set_value('expiration_date') ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('amount') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="amount">Valor</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="amount" name="amount" class="form-control" value="<?= set_value('amount') ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('discount') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="discount">Desconto</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="discount" name="discount" class="form-control" value="<?= set_value('discount') ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('total') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="total">Valor Total</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="total" name="total" class="form-control" value="<?= set_value('total') ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_method') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_method">Método de pagamento</label>
                            <div class="col-lg-4">
                                <select id="payment_method" name="payment_method" class="form-control">
                                    <option value="boleto" >BOLETO</option>
                                    <option value="deposito" >DEPÓSITO/TRANSFERÊNCIA/PIX</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_status') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_status">Status do pagamento</label>
                            <div class="col-lg-4">
                                <select id="payment_status" name="payment_status" class="form-control">
                                    <option value="programado" >PROGRAMADO</option>
                                    <option value="nao_programado" >NÃO PROGRAMADO</option>
                                    <option value="quitado" >QUITADO</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_date">Data de pagamento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="payment_date" name="payment_date" class="form-control default-date-picker" value="<?= set_value('payment_date') ?>" alt="date">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('payment_slip') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_slip">Boleto</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="payment_slip" name="payment_slip" class="form-control" value="<?= set_value('payment_slip') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('invoice') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="invoice">Nota Fiscal</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="invoice" name="invoice" class="form-control" value="<?= set_value('invoice') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('xml') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="xml">XML</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="xml" name="xml" class="form-control" value="<?= set_value('xml') ?>">
                            </div>
                        </div>                        

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 

                </div>
                <div>
                    <?php if (!$list_business_billing) : ?>
                        <div class="alert alert-warning fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                        </div>
                    <?php else : ?>
                        <table class="table responsive-data-table data-table">
                            <thead>
                                <tr>
                                    <th>Ano</th>
                                    <th>Mês</th>
                                    <th>Unidade</th>
                                    <th>Vencimento</th>
                                    <th>Valor total</th>
                                    <th>Data de pagamento</th>
                                    <th>Status do pagamento</th>
                                    <th>Boleto</th>
                                    <th>NFE</th>
                                    <th>XML</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_business_billing as $item) : ?>
                                    <tr>
                                        <td><?= $item['year'] ?></td>
                                        <td><?= $item['month'] ?></td>
                                        <td><?= $item['fk_unity'] ?></td>
                                        <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                        <td><?= $item['total'] ?></td>
                                        <td><?= date_to_human_date($item['payment_date']) ?></td>
                                        <td>
                                            <?php if ($item['payment_status'] != "quitado" && preg_replace("/([^\d]*)/", "", $item['expiration_date']) < preg_replace("/([^\d]*)/", "", $item['payment_date'])) : ?>
                                                <span class="label label-danger">ATRASADO</span>
                                            <?php else: ?>
                                                <span class="label label-success">EM DIA</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if ($item['payment_slip'] !== "") : ?>
                                            <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['payment_slip'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if ($item['invoice'] !== "") : ?>
                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['invoice'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if ($item['xml'] !== "") : ?>
                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['xml'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                            <?php endif; ?>
                                        </td>

                                        <td class="hidden-xs">
                                            <a class="btn btn-danger btn-xs" href='#delete_billing' onclick="$('#pk_business_billing').val(<?= $item['pk_business_billing'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_billing" class="modal fade">
    <form method="POST" action="<?= base_url() ?>business_billing/delete/<?= $this->Business_model->_pk_business ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_business_billing" id="pk_business_billing" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
