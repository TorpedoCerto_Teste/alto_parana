<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Negócios
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>business/index">Negócios</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Negócios
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business/create" enctype="multipart/form-data">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select id="fk_unity" name="fk_unity" class="form-control select2">
                                    <option></option>
                                    <?php if ($units) : ?>
                                        <?php foreach ($units as $matriz => $filiais) : ?>
                                            <optgroup label="<?= $matriz ?>">
                                                <?php foreach ($filiais as $filial) : ?>
                                                    <option value='<?= $filial['pk_unity'] ?>' <?= set_value('fk_unity') == $filial['pk_unity'] ? 'selected' : '' ?>><?= $filial['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>                                                
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_user">Gestor da Conta</label>
                            <div class="col-lg-6">
                                <select id="fk_user" name="fk_user" class="form-control select2">
                                    <option></option>
                                    <?php if ($users) : ?>
                                        <?php foreach ($users as $user) : ?>
                                            <option value='<?= $user['pk_user'] ?>' <?= $user['pk_user'] == $this->session->userdata('pk_user') ? 'selected' : '' ?>><?= $user['name'] . " " . $user['surname'] . " - " . $user['email'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Representante</label>
                            <div class="col-lg-6">
                                <select id="fk_agent" name="fk_agent" class="form-control select2">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $type => $agent) : ?>
                                            <optgroup label="<?= $type ?>">
                                                <?php foreach ($agent as $agt) : ?>
                                                    <option value='<?= $agt['pk_agent'] ?>' <?= set_value('fk_agent') == $agt['pk_agent'] ? 'selected' : '' ?>><?= $agt['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>                                                
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div id="div_vicence" class="form-group <?= form_error('start_date') != "" || form_error('end_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência do Contrato</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="<?= set_value('start_date') ?>" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="<?= set_value('end_date') ?>" alt="date" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('automatic_renovation') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="automatic_renovation">Renovação Automática</label>
                            <div class="col-lg-10">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" name="automatic_renovation" id="automatic_renovation" <?= set_value('automatic_renovation') == 1 ? "checked" : "" ?> /><label for="automatic_renovation">&nbsp;</label>
                                </label>

                            </div>
                        </div>

                        <div class="form-group <?= form_error('memo') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="memo">Observações</label>
                            <div class="col-lg-10">
                                <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="6" ><?= set_value('memo') ?></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Arquivo</label>
                            <div class="col-lg-6">
                                <input type="file" class="form-control" name="business_file" id="business_file" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
