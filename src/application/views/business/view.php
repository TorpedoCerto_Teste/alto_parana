<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Negócio / Unidades / Pagamento / Faturamento 
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>business">Negócio</a></li>
            <li class="active">Visualizar</li>
        </ol>
    </div>
</div>
<!-- page head end-->
<div id="app">
    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">

                <!--tab nav start-->
                <section class="panel">
                    <header class="panel-heading tab-dark tab-right ">
                        <ul class="nav nav-tabs pull-right">
                            <li class="<?= $this->uri->segment(4, "") == "" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#negocio">
                                    <i class="fa fa-home"></i>
                                    Negócio
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "servicos" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#servicos">
                                    <i class="fa fa-wrench"></i>
                                    Serviços
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "unidades" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#unidades">
                                    <i class="fa fa-flash"></i>
                                    Unidades
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "pagamento" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#pagamento">
                                    <i class="fa fa-money"></i>
                                    Pagamento
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "faturamentos" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#faturamentos">
                                    <i class="fa fa-sliders"></i>
                                    Faturamentos
                                </a>
                            </li>
                        </ul>
                        <span class="hidden-sm wht-color"><?= $this->Business_model->_pk_business ?> </span>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <!--NEGÓCIO-->
                            <div id="negocio" class="tab-pane <?= $this->uri->segment(4, "") == "" ? 'active' : '' ?>">
                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business/update/<?= $this->Business_model->_pk_business ?>" enctype="multipart/form-data">
                                    <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled" />
                                    <input type="hidden" name="pk_business" id="pk_business" value="<?= $this->Business_model->_pk_business ?>" disabled="disabled" />
                                    <input type="hidden" name="update" id="update" value="true" />

                                    <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                                        <div class="col-lg-6">
                                            <select id="fk_unity" name="fk_unity" class="form-control select2">
                                                <option></option>
                                                <?php if ($units) : ?>
                                                    <?php foreach ($units as $matriz => $filiais) : ?>
                                                        <optgroup label="<?= $matriz ?>">
                                                            <?php foreach ($filiais as $filial) : ?>
                                                                <option value='<?= $filial['pk_unity'] ?>' <?= $this->Business_model->_fk_unity == $filial['pk_unity'] ? 'selected' : '' ?>><?= $filial['community_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </optgroup>                                                
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_user">Gestor da Conta</label>
                                        <div class="col-lg-6">
                                            <select id="fk_user" name="fk_user" class="form-control select2">
                                                <option></option>
                                                <?php if ($users) : ?>
                                                    <?php foreach ($users as $user) : ?>
                                                        <option value='<?= $user['pk_user'] ?>' <?= $user['pk_user'] == $this->Business_model->_fk_user ? 'selected' : '' ?>><?= $user['name'] . " " . $user['surname'] . " - " . $user['email'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Representante</label>
                                        <div class="col-lg-6">
                                            <select id="fk_agent" name="fk_agent" class="form-control select2">
                                                <option></option>
                                                <?php if ($agents) : ?>
                                                    <?php foreach ($agents as $type => $agent) : ?>
                                                        <optgroup label="<?= $type ?>">
                                                            <?php foreach ($agent as $agt) : ?>
                                                                <option value='<?= $agt['pk_agent'] ?>' <?= $this->Business_model->_fk_agent == $agt['pk_agent'] ? 'selected' : '' ?>><?= $agt['community_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </optgroup>                                                
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="div_vicence" class="form-group <?= form_error('start_date') != "" || form_error('end_date') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label">Vigência do Contrato</label>
                                        <div class="col-md-4">
                                            <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="<?= date_to_human_date($this->Business_model->_start_date) ?>" alt="date" >
                                                <span class="input-group-addon">a</span>
                                                <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="<?= date_to_human_date($this->Business_model->_end_date) ?>" alt="date" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('automatic_renovation') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="automatic_renovation">Renovação Automática</label>
                                        <div class="col-lg-10">
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" value="1" name="automatic_renovation" id="automatic_renovation" <?= $this->Business_model->_automatic_renovation == 1 ? "checked" : "" ?> /><label for="automatic_renovation">&nbsp;</label>
                                            </label>

                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('memo') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="memo">Observações</label>
                                        <div class="col-lg-10">
                                            <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="6" ><?= $this->Business_model->_memo ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Arquivo</label>
                                        <?php if ($this->Business_model->_business_file) : ?>
                                            <div class="col-lg-2">
                                                <a href="<?= base_url() ?>uploads/business/<?= $this->Business_model->_pk_business ?>/<?= $this->Business_model->_business_file ?>" class="btn btn-info" download><?= $this->Business_model->_business_file ?></a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="business_file" id="business_file" />
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-success" type="submit">Salvar</button>
                                        </div>
                                    </div>
                                </form> 

                            </div>

                            <!--SERVIÇOS-->
                            <div id="servicos" class="tab-pane <?= $this->uri->segment(4, "") == "servicos" ? 'active' : '' ?>">
                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_service/upsert/<?= $this->Business_model->_pk_business ?>" onsubmit="enable_checkboxes()">
                                    <input type="hidden" name="create" id="create" value="true" />
                                    <input type="hidden" name="personalized" id="personalized" value="0" />

                                    <div class="form-group <?= form_error('fk_service') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_service">Serviço</label>
                                        <div class="col-lg-6">
                                            <select name="fk_service" id="fk_service" class="form-control select2">
                                                <option></option>
                                                <?php if ($services) : ?>
                                                    <?php foreach ($services as $service) : ?>
                                                        <option value="<?= $service['pk_service'] ?>" <?= $this->Business_service_model->_fk_service == $service['pk_service'] ? "selected" : "" ?>><?= $service['service'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div name="div_personalizar" id="div_personalizar" class="col-lg-2" style="display: <?= $this->Business_service_model->_fk_service != "" ? "block" : "none" ?>">
                                            <a class="btn btn-info btn-personalizar">Personalizar</a>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('lis_product') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="lis_product">Categorias e Produtos</label>
                                        <div class="col-lg-10">
                                            <table id="lis_product" name="lis_product" >
                                                <?php if ($products) : ?>
                                                    <?php foreach ($products as $category_name => $pk_category_prds) : ?>
                                                        <?php foreach ($pk_category_prds as $pk_category_product => $prds) : ?>
                                                            <tr>
                                                                <td>
                                                                    <label class="checkbox-custom check-success">
                                                                        <input class="category prd" type="checkbox" value="<?= $pk_category_product ?>" name="<?= $pk_category_product ?>" id="<?= $pk_category_product ?>" disabled="disabled" /><label for="<?= $pk_category_product ?>">&nbsp;</label>
                                                                    </label>
                                                                </td>
                                                                <td colspan="2"><b><?= $category_name ?></b></td>
                                                            </tr>
                                                            <?php foreach ($prds as $prd) : ?>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <label class="checkbox-custom check-success">
                                                                            <?php $checked = false;
                                                                            if (!empty($business_service_products)) : ?>
                                                                                <?php foreach ($business_service_products as $bsp) : ?>
                                                                                    <?php if ($bsp['fk_product'] == $prd['pk_product']) {
                                                                                        $checked = true;
                                                                                    } ?>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                            <input class="<?= $pk_category_product ?> prd" type="checkbox" value="<?= $prd['pk_product'] ?>" id="product_<?= $prd['pk_product'] ?>" name="product[]" disabled="disabled" <?= $checked ? "checked" : "" ?> /><label for="product_<?= $prd['pk_product'] ?>">&nbsp;</label>
                                                                        </label>
                                                                    </td>
                                                                    <td><?= $prd['product'] ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        <?php endforeach; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </table>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-success" type="submit">Salvar</button>
                                        </div>
                                    </div>


                                </form> 

                            </div>


                            <!--UNIDADES-->
                            <div id="unidades" class="tab-pane <?= $this->uri->segment(4, "") == "unidades" ? 'active' : '' ?>">
                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_unity/create/<?= $this->Business_model->_pk_business ?>">
                                    <input type="hidden" name="create" id="create" value="true" />

                                    <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="fk_unity" id="fk_unity">
                                                <option></option>
                                                    <?php foreach ($units2 as $type => $unity) : ?>
                                                    <optgroup label="<?= $type ?>">
                                                    <?php foreach ($unity as $unit) : ?>
                                                            <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
        <?php endforeach; ?>
                                                    </optgroup>
    <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('unity_differentiated_duration') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="unity_differentiated_duration">Vigências Diferenciadas por Unidade</label>
                                        <div class="col-lg-1">
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" id="unity_differentiated_duration" name="unity_differentiated_duration" class="form-control" value="1" <?= $this->Business_model->_unity_differentiated_duration == 1 ? "checked" : "" ?> ><label for="unity_differentiated_duration">&nbsp;</label>
                                            </label>
                                        </div>
                                    </div>

                                    <div id="div_unity_differentiated_duration" name="div_unity_differentiated_duration" class="form-group" style="display: <?= $this->Business_model->_unity_differentiated_duration == 1 ? "block" : "none" ?>">
                                        <label class="col-lg-2 col-sm-2 control-label">Vigência diferenciada</label>
                                        <div class="col-md-4">
                                            <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                                <span class="input-group-addon">a</span>
                                                <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-success" type="submit">Adicionar</button>
                                        </div>
                                    </div>

                                    <div>
    <?php if (!$list_business_units) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
    <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Unidade
                                                        </th>
        <?php if ($this->Business_model->_unity_differentiated_duration == 1) : ?>
                                                            <th>
                                                                Data inicial
                                                            </th>
                                                            <th>
                                                                Data final
                                                            </th>
                                                    <?php endif; ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <?php foreach ($list_business_units as $item) : ?>
                                                        <tr>
                                                            <td><?= $item['community_name'] ?></td>
                                                            <?php if ($this->Business_model->_unity_differentiated_duration == 1) : ?>
                                                                <td><?= date_to_human_date($item['start_date']) ?></td>
                                                                <td><?= date_to_human_date($item['end_date']) ?></td>
            <?php endif; ?>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-danger btn-xs" href='#delete_unity' onclick="$('#pk_business_unity').val(<?= $item['pk_business_unity'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                            <?php endforeach; ?>
                                                </tbody>
                                            </table>
    <?php endif; ?>
                                    </div>
                                </form> 
                            </div>

                            <!--PAGAMENTO-->
                            <div id="pagamento" class="tab-pane <?= $this->uri->segment(4, "") == "pagamento" ? 'active' : '' ?>">
                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_payment/update/<?= $this->Business_model->_pk_business ?>">
                                    <input type="hidden" name="update" id="update" value="true" />

                                    <div class="form-group <?= form_error('remuneration_type') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="remuneration_type">Forma de Remuneração</label>
                                        <div class="col-lg-6">
                                            <select id="remuneration_type" name="remuneration_type" class="form-control select2">
                                                <option></option>
                                                <option value="mensal_por_unidade" <?= $this->Business_payment_model->_remuneration_type == "mensal_por_unidade" ? "selected" : "" ?>>MENSAL POR UNIDADE</option>
                                                <option value="percentual_da_economia" <?= $this->Business_payment_model->_remuneration_type == "percentual_da_economia" ? "selected" : "" ?>>PERCENTUAL DA ECONOMIA</option>
                                                <option value="valor_sobre_a_energia" <?= $this->Business_payment_model->_remuneration_type == "valor_sobre_a_energia" ? "selected" : "" ?>>VALOR SOBRE A ENERGIA</option>
                                                <option value="valor_fechado" <?= $this->Business_payment_model->_remuneration_type == "valor_fechado" ? "selected" : "" ?>>VALOR FECHADO</option>
                                                <option value="mensal_por_contraparte" <?= $this->Business_payment_model->_remuneration_type == "mensal_por_contraparte" ? "selected" : "" ?>>MENSAL POR CONTRAPARTE</option>
                                                <option value="valor_fixo_mais_percentual_de_economia" <?= $this->Business_payment_model->_remuneration_type == "valor_fixo_mais_percentual_de_economia" ? "selected" : "" ?>>VALOR FIXO MAIS PERCENTUAL DE ECONOMIA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div name="div_mensal_por_unidade" id="div_mensal_por_unidade" style="display: <?= $this->Business_payment_model->_remuneration_type == "mensal_por_unidade" ? "block" : "none" ?>">

                                        <div class="form-group <?= form_error('form_mensal_por_unidade') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="form_mensal_por_unidade">Mensal por Unidade</label>
                                            <div class="col-lg-6">
    <?php if ($unities) : ?>
                                                    <table class="table responsive-data-table data-table" name="table_mensal_por_unidade">
                                                        <thead>
                                                            <tr>
                                                                <th>Unidade</th>
                                                                <th>Valor R$</th>
                                                            </tr>
                                                        </thead> 
                                                        <tbody>
        <?php foreach ($monthly_unities as $pk_unity => $monthly_unity) : ?>
                                                                <tr>
                                                                    <td><?= $monthly_unity['community_name'] ?></td>
                                                                    <td>
                                                                        <input type="text" placeholder="" id="monthly_value<?= $pk_unity ?>" name="monthly_value[<?= $pk_unity ?>]" class="form-control" value="<?= number_format($monthly_unity['monthly_value'], 2, ".", "") ?>" alt="valor2" />
                                                                    </td>
                                                                </tr>
                                                    <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
    <?php else : ?>
                                                    <div class="alert alert-warning fade in">
                                                        <button type="button" class="close close-sm" data-dismiss="alert">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <strong>Atenção!</strong> Nenhuma unidade foi inserida. Clique em <a href="#" class="btn btn-success">Adicionar Unidades</a> para continuar...
                                                    </div>
    <?php endif; ?>
                                            </div>
                                        </div>

                                    </div>

                                    <div name="div_percentual_da_economia" id="div_percentual_da_economia" style="display: <?= $this->Business_payment_model->_remuneration_type == "percentual_da_economia" ? "block" : "none" ?>">

                                        <div class="form-group <?= form_error('form_percentual_da_economia') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="form_percentual_da_economia">Percentual da Economia</label>
                                            <div class="col-lg-2">
                                                <span class="help-block">Percentual %</span>
                                                <input type="text" name="percent_economy_value" id="percent_economy_value" value="<?= $this->Business_payment_model->_percent_economy_value ?>" alt="valor2">
                                            </div>
                                            <div class="col-lg-2">
                                                <span class="help-block">Piso R$</span>
                                                <input type="text" name="percent_economy_floor" id="percent_economy_floor" value="<?= $this->Business_payment_model->_percent_economy_floor ?>" alt="valor2">
                                            </div>
                                            <div class="col-lg-2">
                                                <span class="help-block">Teto R$</span>
                                                <input type="text" name="percent_economy_roof" id="percent_economy_roof" value="<?= $this->Business_payment_model->_percent_economy_roof ?>" alt="valor2">
                                            </div>
                                        </div>

                                    </div>

                                    <div name="div_valor_sobre_a_energia" id="div_valor_sobre_a_energia" style="display: <?= $this->Business_payment_model->_remuneration_type == "valor_sobre_a_energia" ? "block" : "none" ?>">

                                        <div class="form-group <?= form_error('form_valor_sobre_a_energia') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="form_valor_sobre_a_energia">Valor Sobre a Energia</label>
                                            <div class="col-lg-2">
                                                <span class="help-block">Valor R$/MWh</span>
                                                <input type="text" name="over_energy_value" id="over_energy_value" value="<?= $this->Business_payment_model->_over_energy_value ?>" alt="valor2">
                                            </div>
                                            <div class="col-lg-2">
                                                <span class="help-block">Piso R$</span>
                                                <input type="text" name="over_energy_floor" id="over_energy_floor" value="<?= $this->Business_payment_model->_over_energy_floor ?>" alt="valor2">
                                            </div>
                                            <div class="col-lg-2">
                                                <span class="help-block">Teto R$</span>
                                                <input type="text" name="over_energy_roof" id="over_energy_roof" value="<?= $this->Business_payment_model->_over_energy_roof ?>" alt="valor2">
                                            </div>
                                        </div>

                                    </div>

                                    <div name="div_valor_fechado" id="div_valor_fechado" style="display: <?= $this->Business_payment_model->_remuneration_type == "valor_fechado" ? "block" : "none" ?>">

                                        <div class="form-group <?= form_error('form_valor_fechado') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="form_valor_fechado">Valor Fechado</label>
                                            <div class="col-lg-2">
                                                <span class="help-block">Valor</span>
                                                <input type="text" name="closed_value" id="closed_value" value="<?= $this->Business_payment_model->_closed_value ?>" alt="valor2">
                                            </div>
                                            <div class="col-lg-2">
                                                <span class="help-block">Parcelado</span>
                                                <label class="checkbox-custom check-success">
                                                    <input type="checkbox" value="1" name="closed_installment" id="closed_installment" <?= $this->Business_payment_model->_closed_installment == 1 ? "checked" : "" ?>/><label for="closed_installment">Sim&nbsp;</label>
                                                </label>
                                            </div>
                                            <div name="div_closed_installment_quantity" id="div_closed_installment_quantity" class="col-lg-2" style="display: <?= $this->Business_payment_model->_closed_installment == 1 ? "block" : "none" ?>">
                                                <span class="help-block">Quantidade de Parcelas</span>
                                                <input type="text" name="closed_installment_quantity" id="closed_installment_quantity" value="<?= $this->Business_payment_model->_closed_installment_quantity ?>" alt="year">
                                            </div>
                                        </div>

                                    </div>

                                    <div name="div_mensal_por_contraparte" id="div_mensal_por_contraparte" style="display: <?= $this->Business_payment_model->_remuneration_type == "mensal_por_contraparte" ? "block" : "none" ?>">

                                        <div class="form-group <?= form_error('form_mensal_por_contraparte') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="form_mensal_por_contraparte">Mensal por Contraparte</label>
                                            <div class="col-lg-10">
                                                <span class="help-block">Tipo de Valor</span>

                                                <div class="radio-custom radio-success">
                                                    <input name="monthly_counterpart_unity_quantity_consumption" id="ratear_quantidade_unidades" type="radio" value="ratear_quantidade_unidades" <?= $this->Business_payment_model->_monthly_counterpart_unity_quantity_consumption == "ratear_quantidade_unidades" ? "checked" : "" ?> >
                                                    <label for="ratear_quantidade_unidades">Ratear pela Quantidade de Unidades</label>
                                                </div>
                                                <div class="radio-custom radio-success">
                                                    <input name="monthly_counterpart_unity_quantity_consumption" id="ratear_comumo_energia_unidades" type="radio" value="ratear_comumo_energia_unidades" <?= $this->Business_payment_model->_monthly_counterpart_unity_quantity_consumption == "ratear_comumo_energia_unidades" ? "checked" : "" ?> >
                                                    <label for="ratear_comumo_energia_unidades">Ratear pelo Consumo de Energia das Unidades</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group <?= form_error('monthly_counterpart_value') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="monthly_counterpart_value">Valor</label>
                                            <div class="col-lg-2">
                                                <input type="text" name="monthly_counterpart_value" id="monthly_counterpart_value" value="<?= $this->Business_payment_model->_monthly_counterpart_value ?>" alt="valor2">
                                            </div>
                                        </div>
                                    </div>

                                    <div name="div_valor_fixo_mais_percentual_de_economia" id="div_valor_fixo_mais_percentual_de_economia" style="display: <?= $this->Business_payment_model->_remuneration_type == "valor_fixo_mais_percentual_de_economia" ? "block" : "none" ?>">

                                        <div class="form-group <?= form_error('form_valor_fixo_mais_percentual_de_economia') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="form_valor_fixo_mais_percentual_de_economia">Valor Fixo Mais Percentual de Economia</label>
                                            <div class="col-lg-10">
    <?php if ($unities) : ?>
                                                    <table class="table responsive-data-table data-table" name="table_valor_fixo_mais_percentual_de_economia">
                                                        <thead>
                                                            <tr>
                                                                <th>Unidade</th>
                                                                <th>Valor Fixo R$</th>
                                                                <th>Percentual</th>
                                                                <th>Piso R$</th>
                                                                <th>Teto R$</th>
                                                            </tr>
                                                        </thead> 
                                                        <tbody>
                                                                    <?php foreach ($payment_fixed_value_unities as $pk_unity => $payment_fixed_value_unity) : ?>
                                                                <tr>
                                                                    <td>
            <?= $payment_fixed_value_unity['community_name'] ?>
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" placeholder="" id="fixed_value<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][fixed_value]" class="form-control" value="<?= number_format($payment_fixed_value_unity['fixed_value'], 2, ".", "") ?>" alt="valor2" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" placeholder="" id="percent<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][percent]" class="form-control" value="<?= number_format($payment_fixed_value_unity['percent'], 2, ".", "") ?>" alt="valor2" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" placeholder="" id="floor<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][floor]" class="form-control" value="<?= number_format($payment_fixed_value_unity['floor'], 2, ".", "") ?>" alt="valor2" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" placeholder="" id="roof<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][roof]" class="form-control" value="<?= number_format($payment_fixed_value_unity['roof'], 2, ".", "") ?>" alt="valor2" />                                                    </td>
                                                                </tr>
                                                    <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
    <?php else : ?>
                                                    <div class="alert alert-warning fade in">
                                                        <button type="button" class="close close-sm" data-dismiss="alert">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <strong>Atenção!</strong> Nenhuma unidade foi inserida. Clique em <a href="#" class="btn btn-success">Adicionar Unidades</a> para continuar...
                                                    </div>
    <?php endif; ?>
                                            </div>
                                        </div>

                                    </div>

                                    <div class=" s-row">
                                    </div>

                                    <div class="form-group <?= form_error('due') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="due">Dia de vencimento</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" id="due" name="due">
                                                <option value="dia_util" <?= $this->Business_payment_model->_due == "dia_util" ? "selected" : "" ?>>Dia útil</option>
                                                <option value="dia_fixo" <?= $this->Business_payment_model->_due == "dia_fixo" ? "selected" : "" ?>>Dia fixo</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <select class="form-control" id="due_day" name="due_day">
    <?php for ($i = 1; $i <= 28; $i++) : ?>
                                                    <option value="<?= $i ?>" <?= $this->Business_payment_model->_due_day == $i ? "selected" : "" ?>><?= $i ?></option>
    <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('due_period') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="due_period">Mês de vencimento</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" id="due_period" name="due_period">
                                                <option value="M" <?= $this->Business_payment_model->_due_period == "M" ? "selected" : "" ?>>Mês Fornecimento</option>
                                                <option value="MA" <?= $this->Business_payment_model->_due_period == "MA" ? "selected" : "" ?>>Meses Antes</option>
                                                <option value="MS" <?= $this->Business_payment_model->_due_period == "MS" ? "selected" : "" ?>>Meses Subsequentes</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="text" placeholder="" id="due_month" name="due_month" class="form-control" value="<?= set_value('due_month') != "" ? set_value('due_month') : $this->Business_payment_model->_due_month ?>" alt="year">
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('financial_index') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="financial_index">Índice de Reajuste</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" id="financial_index" name="financial_index">
                                                <option value="IGPM" <?= $this->Business_payment_model->_financial_index == "IGPM" ? "selected" : "" ?>>IGP-M</option>
                                                <option value="IPCA" <?= $this->Business_payment_model->_financial_index == "IPCA" ? "selected" : "" ?>>IPCA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('index_readjustment_calculation') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="index_readjustment_calculation">Modo de Cálculo do Índice de Reajuste</label>
                                        <div class="col-lg-4">
                                            <select class="form-control" id="index_readjustment_calculation" name="index_readjustment_calculation">
                                                <option value="1" <?= $this->Business_payment_model->_index_readjustment_calculation == "1" ? "selected" : "" ?>>Padrão</option>
                                                <option value="2" <?= $this->Business_payment_model->_index_readjustment_calculation == "2" ? "selected" : "" ?>>Não realizar o reajuste se o valor ajustado for menor que o valor atual</option>
                                                <option value="3" <?= $this->Business_payment_model->_index_readjustment_calculation == "3" ? "selected" : "" ?>>Não considerar índices negativos no cálculo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('database') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="database">Data base</label>
                                        <div class="col-lg-2">
                                            <input type="text" placeholder="" id="database" name="database" class="form-control default-date-picker" value="<?= set_value('database') != "" ? set_value('database') : date_to_human_date($this->Business_payment_model->_database) ?>" alt="date">
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('adjustment_montly_manually') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="adjustment_montly_manually">Ajustar manualmente a data do primeiro reajuste</label>
                                        <div class="col-lg-10">
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" value="1" name="adjustment_montly_manually" id="adjustment_montly_manually" <?= $this->Business_payment_model->_adjustment_montly_manually == 1 ? "checked" : "" ?>/><label for="adjustment_montly_manually">&nbsp;</label>
                                            </label>
                                        </div>
                                    </div>

                                    <div id="div_date_adjustment" name="div_date_adjustment" class="form-group <?= form_error('date_adjustment') != "" ? "has-error" : ""; ?>" style="display: <?= $this->Business_payment_model->_adjustment_montly_manually == 1 ? "block" : "none" ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="date_adjustment">Inserir Data</label>
                                        <div class="col-lg-2">
                                            <input type="text" placeholder="" id="date_adjustment" name="date_adjustment" class="form-control default-date-picker" value="<?= set_value('date_adjustment') != "" ? set_value('date_adjustment') : date_to_human_date($this->Business_payment_model->_date_adjustment) ?>" alt="date">
                                        </div>
                                        <div class="col-lg-2">
                                            <select name="date_adjustment_frequency" id="date_adjustment_frequency" class="form-control">
                                                <option value="12em12" <?= $this->Business_payment_model->_date_adjustment_frequency == "12em12" ? "selected" : "" ?>>12 em 12</option>
                                                <option value="todoJaneiro" <?= $this->Business_payment_model->_date_adjustment_frequency == "todoJaneiro" ? "selected" : "" ?>>Todo Janeiro</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?= form_error('form_penalty_interest') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="form_penalty_interest">Multas e Juros</label>
                                        <div class="col-lg-2">
                                            <span class="help-block">Multa R$</span>
                                            <input type="text" name="penalty" id="penalty" value="<?= $this->Business_payment_model->_penalty ?>" alt="valor2">
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="help-block">Juros %</span>
                                            <input type="text" name="interest" id="interest" value="<?= $this->Business_payment_model->_interest ?>" alt="valor2">
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="help-block">Frequência</span>
                                            <select name='interest_frequency' id='interest_frequency' class='form-control'>
                                                <option value='diaria' <?= $this->Business_payment_model->_interest_frequency == "diaria" ? "selected" : "" ?>>DIÁRIA</option>
                                                <option value='mensal' <?= $this->Business_payment_model->_interest_frequency == "mensal" ? "selected" : "" ?>>MENSAL</option>
                                                <option value='anual' <?= $this->Business_payment_model->_interest_frequency == "anual" ? "selected" : "" ?>>ANUAL</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-success" type="submit">Salvar</button>
                                        </div>
                                    </div>
                                </form> 

                            </div>

                            <!--FATURAMENTOS-->
                            <div id="faturamentos" class="tab-pane <?= $this->uri->segment(4, "") == "faturamentos" ? 'active' : '' ?>">
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_billing/create/<?=$this->Business_model->_pk_business?>" enctype="multipart/form-data">
                                        <input type="hidden" name="create" id="create" value="true" />

                                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                                            <div class="col-lg-2">
                                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= date("Y") ?>" alt="year">
                                            </div>
                                        </div>


                                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                                            <div class="col-lg-2">
                                                <select id="month" name="month" class="form-control">
                                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>JANEIRO</option>
                                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>MARÇO</option>
                                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>ABRIL</option>
                                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>MAIO</option>
                                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>JUNHO</option>
                                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>JULHO</option>
                                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>AGOSTO</option>
                                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                                </select>
                                            </div>
                                        </div>                    

                                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                                            <div class="col-lg-4">
                                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                                    <option></option>
                                                    <?php foreach ($list_business_units as $unit) : ?>
                                                        <option value="<?= $unit['fk_unity'] ?>" <?= set_value('pk_unity') == $unit['fk_unity'] ? 'selected' : "" ?>><?= $unit['community_name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Vencimento</label>
                                            <div class="col-lg-2">
                                                <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control default-date-picker" value="<?= set_value('expiration_date') ?>" alt="date" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('amount') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="amount">Valor</label>
                                            <div class="col-lg-2">
                                                <input type="text" placeholder="" id="amount" name="amount" class="form-control" value="<?= set_value('amount') != "" ? set_value('amount') : $this->Business_billing_model->_amount ?>" alt="decimal">
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('discount') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="discount">Desconto</label>
                                            <div class="col-lg-2">
                                                <input type="text" placeholder="" id="discount" name="discount" class="form-control" value="<?= set_value('discount') != "" ? set_value('discount') : $this->Business_billing_model->_discount ?>" alt="decimal">
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('total') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="total">Valor Total</label>
                                            <div class="col-lg-2">
                                                <input type="text" placeholder="" id="total" name="total" class="form-control" value="<?= set_value('total') ?>" alt="decimal">
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('payment_method') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="payment_method">Método de pagamento</label>
                                            <div class="col-lg-4">
                                                <select id="payment_method" name="payment_method" class="form-control">
                                                    <option value="boleto" >BOLETO</option>
                                                    <option value="deposito" >DEPÓSITO/TRANSFERÊNCIA/PIX</option>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('payment_status') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="payment_status">Status do pagamento</label>
                                            <div class="col-lg-4">
                                                <select id="payment_status" name="payment_status" class="form-control">
                                                    <option value="programado" >PROGRAMADO</option>
                                                    <option value="nao_programado" >NÃO PROGRAMADO</option>
                                                    <option value="quitado" >QUITADO</option>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('payment_date') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="payment_date">Data de pagamento</label>
                                            <div class="col-lg-2">
                                                <input type="text" placeholder="" id="payment_date" name="payment_date" class="form-control default-date-picker" value="<?= set_value('payment_date') ?>" alt="date">
                                            </div>
                                        </div>

                                        <div class="form-group <?= form_error('payment_slip') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="payment_slip">Boleto</label>
                                            <div class="col-lg-4">
                                                <input type="file" placeholder="" id="payment_slip" name="payment_slip" class="form-control" value="<?= set_value('payment_slip') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('invoice') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="invoice">Nota Fiscal</label>
                                            <div class="col-lg-4">
                                                <input type="file" placeholder="" id="invoice" name="invoice" class="form-control" value="<?= set_value('invoice') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group <?= form_error('xml') != "" ? "has-error" : ""; ?>">
                                            <label class="col-lg-2 col-sm-2 control-label" for="xml">XML</label>
                                            <div class="col-lg-4">
                                                <input type="file" placeholder="" id="xml" name="xml" class="form-control" value="<?= set_value('xml') ?>">
                                            </div>
                                        </div>                        

                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                                <button class="btn btn-success" type="submit">Salvar</button>
                                            </div>
                                        </div>
                                    </form> 

                                </div>
                                <div>
                                    <?php if (!$list_business_billing) : ?>
                                        <div class="alert alert-warning fade in">
                                            <button type="button" class="close close-sm" data-dismiss="alert">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                        </div>
                                    <?php else : ?>
                                        <table class="table responsive-data-table data-table faturamento">
                                            <thead>
                                                <tr>
                                                    <th>Ano</th>
                                                    <th>Mês</th>
                                                    <th>Unidade</th>
                                                    <th>Vencimento</th>
                                                    <th>Valor</th>
                                                    <th>Desconto</th>
                                                    <th>Valor total</th>
                                                    <th>Método de pagamento</th>
                                                    <th>Data de pagamento</th>
                                                    <th>Status do pagamento</th>
                                                    <th>Boleto</th>
                                                    <th>NFE</th>
                                                    <th>XML</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($list_business_billing as $item) :  ?>
                                                    <tr>
                                                        <td><?= $item['year'] ?></td>
                                                        <td><?= retorna_mes($item['month']) ?></td>
                                                        <td><?= $item['unity_name'] ?></td>
                                                        <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                                        <td><?= number_format($item['amount'],2,",","") ?></td>
                                                        <td><?= number_format($item['discount'],2,",","") ?></td>
                                                        <td><?= number_format($item['total'],2,",","") ?></td>
                                                        <td>
                                                            <?php if ($item['payment_method'] == "boleto") : ?>
                                                                <span class="label label-warning">BOLETO</span>
                                                            <?php else: ?>
                                                                <span class="label label-warning">DEPÓSITO/TRANSFERÊNCIA/PIX</span>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?= date_to_human_date($item['payment_date']) ?></td>
                                                        <td>
                                                            <?php if ($item['payment_status'] != "quitado" && preg_replace("/([^\d]*)/", "", $item['expiration_date']) < preg_replace("/([^\d]*)/", "", $item['payment_date'])) : ?>
                                                                <span class="label label-danger">ATRASADO</span>
                                                            <?php else: ?>
                                                                <span class="label label-success">EM DIA</span>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($item['payment_slip'] != "") : ?>
                                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['payment_slip'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($item['invoice'] != "") : ?>
                                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['invoice'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($item['xml'] != "") : ?>
                                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['xml'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                            <?php endif; ?>
                                                        </td>

                                                        <td class="hidden-xs">
                                                            <a class="btn btn-danger btn-xs" href='#delete_billing' onclick="$('#pk_business_billing').val(<?= $item['pk_business_billing'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                            <a class="btn btn-info btn-xs" href='#sendmail' onclick="$('#pk_business_billing_sendmail').val(<?= $item['pk_business_billing'] ?>); $('#sendmail_fk_unity').val(<?= $item['fk_unity']?>);" @click="fetchData()" data-toggle="modal"><i class="fa fa-envelope "></i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    <?php endif; ?>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>
                <!--tab nav start-->

            </div>

        </div>
    </div>


    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_unity" class="modal fade">
        <form method="POST" action="<?= base_url() ?>business_unity/delete/<?= $this->Business_model->_pk_business ?>">
            <input type="hidden" name="delete" id="delete" value="true" />
            <input type="hidden" name="pk_business_unity" id="pk_business_unity" value="0" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Deseja deletar este registro?</h4>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-danger" type="submit">Deletar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_billing" class="modal fade">
        <form method="POST" action="<?= base_url() ?>business_billing/delete/<?= $this->Business_model->_pk_business ?>">
            <input type="hidden" name="delete" id="delete" value="true" />
            <input type="hidden" name="pk_business_billing" id="pk_business_billing" value="0" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Deseja deletar este registro?</h4>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-danger" type="submit">Deletar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sendmail" class="modal fade">
        <form method="POST" action="<?= base_url() ?>business_billing/sendmail/<?= $this->Business_model->_pk_business ?>">
            <input type="hidden" name="sendmail" id="sendmail" value="true" />
            <input type="hidden" name="pk_business_billing_sendmail" id="pk_business_billing_sendmail" value="0" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Notificar por email</h4>
                    </div>
                    <div class="modal-body">
                        <label class="checkbox-custom check-success">
                            <input type="checkbox" name="test" id="test" value="test"/>
                            <label for="test">Teste</label>
                        </label>
    
                        <div class="clearfix"></div>
                        <div class="form-group hidden" id="div_test_email">
                            <label class="col-lg-2 col-sm-2 control-label" for="test_email">Email de teste</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="test_email" id="test_email" />
                            </div>
                        </div>
                        
                        <div id="email_list">
                            <div class="tools pull-right" id="group_selector">
                                <input type="hidden" ref="controller_name" value="<?= $this->router->fetch_class() ?>" />
                                <input type="hidden" ref="fk_agent" value="0" />
                                <input type="hidden" ref="fk_unity" id="sendmail_fk_unity" value="<?= $this->Business_model->_fk_unity ?>" />
                                <input type="hidden" ref="base_url" value="<?= base_url() ?>" />
                                
                                <select class="form-control" name="fk_group_controller" ref="selectedGroup" v-model="selectedGroup" id="fk_group_controller">
                                    <option v-for="item in groupsList" :key="item.pk_group" :value="item.pk_group">
                                        {{item.group_short_name}}
                                    </option>
                                </select>
                            </div>

                            <table class="table table-responsive table-hover">
                                <thead>
                                    <tr>
                                        <th>TODOS</th>
                                        <th>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="all" name="all" id="all" value=""/><label for="all">&nbsp;</label>
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody v-if="contacts.length > 0">
                                    <tr v-for="contact in contacts" :key="contact.pk_contact">
                                        <td>{{ contact.name }} {{ contact.surname }} <br> {{ contact.email }}</td>
                                        <td>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="chk_contact" :name="'contacts[]'" :id="'contact_' + contact.pk_contact" :value="contact.pk_contact" :checked="contact.selected"/>
                                                <label :for="'contact_' + contact.pk_contact">&nbsp;</label>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody v-else>
                                    <tr>
                                        <td colspan="2">
                                            <div class="alert alert-danger fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Não existem contatos cadastrados para este agente. 
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->
</div>
