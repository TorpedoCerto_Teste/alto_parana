<div id="app">
    <!-- page head start-->
    <div class="page-head">
        <h3 class="m-b-less">
            Nova Oferta de Energia
        </h3>
        <!--<span class="sub-title">Welcome to Static Table</span>-->
        <div class="state-information">
            <ol class="breadcrumb m-b-less bg-less">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li><a href="<?= base_url() ?>energy_offer/">Ofertas de Energia</a></li>
                <li class="active">Novo</li>
            </ol>
        </div>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading ">
                        Nova Oferta de Energia
                    </header>
                    <?php if ($error) : ?>
                        <div class="alert alert-danger fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Houve um erro ao salvar os dados.
                        </div>
                    <?php endif; ?>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>energy_offer/create">
                            <input type="hidden" name="create" id="create" value="true" />

                            <div class="form-group <?= form_error('offer_date') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="offer_date">Data da oferta</label>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <input class="form-control default-date-picker" type="text" id="offer_date" name="offer_date" alt="date" value="<?=date("d/m/Y")?>">
                                </div>
                            </div>

                            <div class="form-group <?= form_error('fk_energy_type') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="fk_energy_type">Tipo de Energia</label>
                                <div class="col-lg-4">
                                    <select id="multiple" class="form-control  select2" name="fk_energy_type" id="fk_energy_type">
                                        <?php if ($energy_types) : ?>
                                            <?php foreach ($energy_types as $energy_type) : ?>
                                                <option value="<?= $energy_type['pk_energy_type'] ?>" <?= $energy_type['name'] == 'I5' ? 'selected' : '' ?>><?= $energy_type['name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group <?= form_error('fk_submarket') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="fk_submarket">Submercado</label>
                                <div class="col-lg-4">
                                    <select id="multiple" class="form-control  select2" name="fk_submarket" id="fk_submarket">
                                        <?php if ($submarkets) : ?>
                                            <?php foreach ($submarkets as $submarket) : ?>
                                                <option value="<?= $submarket['pk_submarket'] ?>" <?= $submarket['initials'] == 'SE/CO' ? 'selected' : '' ?>><?= $submarket['submarket'] ?> (<?= $submarket['initials'] ?>)</option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group <?= form_error('set_origin') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="set_origin">Informar origem</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" id="set_origin" name="set_origin" value="1" />
                                </div>
                            </div>

                            <div class="form-group hidden <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>" id="div_fk_agent">
                                <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Origem</label>
                                <div class="col-lg-4">
                                    <select id="multiple" class="form-control  select2" name="fk_agent" id="fk_agent">
                                        <?php if ($agents) : ?>
                                            <?php foreach ($agents as $agent) : ?>
                                                <option value="<?= $agent['pk_agent'] ?>"><?= $agent['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <hr />

                            <section class="panel form-group col-lg-6">
                                <header class="panel-heading ">
                                    Histórico de preços
                                    <span class="tools pull-right">
                                        <button class="btn btn-secondary addon-btn m-b-10 add_new">
                                            <i class="fa fa-plus pull-right"></i>
                                            Adicionar
                                        </button>
                                    </span>
                                </header>

                                <table class="table responsive-data-table data-table myTable">
                                    <thead>
                                        <tr>
                                            <th>Ano</th>
                                            <th>Preço</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input type="text" id="year" name="year[]" class="form-control year" value="<?=date("Y")?>" alt="year"
                                                />
                                            </td>
                                            <td>
                                                <input class="form-control" id="value" name="value[]" type="text" alt="valor2">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </section>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">Salvar</button>
                                    <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>energy_offer'; return false;">Cancelar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
