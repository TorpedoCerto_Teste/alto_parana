<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Ercap Mercado
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ercap_market/">Ercap Mercado</a></li>
            <li class="active">Editar Ercap Mercado</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Ercap Mercado
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ercap_market/update/<?= $this->ercap_market_model->_pk_ercap_market ?>">
                        <input type="hidden" name="update" id="update" value="true" />

                        <div class="form-group <?= form_error('') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for=""></label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="" name="" class="form-control" value="<?= set_value('') != "" ? set_value('') : $this->ercap_market_model->_ercap_market ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>

