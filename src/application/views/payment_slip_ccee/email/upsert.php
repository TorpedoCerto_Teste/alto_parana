<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Boleto de Contribuição Associativa CCEE</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Boleto de Contribuição Associativa CCEE </p>
      	<p>Encaminhamos em anexo e disponibilizamos na <a href="<?=base_url()?>customer/payment_slip_ccee" target="_blank">área do cliente</a> o boleto de contribuição associativa da Câmara de Comercialização de Energia Elétrica, para programação de pagamento. <br/>Seguem dados detalhados para lançamento: </p>
        <br/>
        <table style="border:0px; width: 100%">
            <tr>
                <td width="300px">Agente</td>
                <td><?=$this->Agent_model->_community_name?></td>
            </tr>
            <tr>
                <td>Referência</td>
                <td><?=$this->Payment_slip_ccee_model->_month?>/<?=$this->Payment_slip_ccee_model->_year?></td>
            </tr>
            <tr>
                <td>Valor</td>
                <td>R$ <?=number_format($this->Payment_slip_ccee_model->_value, 2, ",", "")?></td>
            </tr>
            <tr>
                <td>Vencimento</td>
                <td><?= date_to_human_date($this->Payment_slip_ccee_model->_validate)?></td>
            </tr>
        </table>
        <br/>
      <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?=$this->config->item("config_phone")?> e em nosso atendimento on-line, via chat da área do cliente.</p>
        <br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>
    </body>
</html>