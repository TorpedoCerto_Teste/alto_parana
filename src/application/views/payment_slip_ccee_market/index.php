<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Boleto CCEE Mercado
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Boleto CCEE Mercado</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Boleto CCEE Mercado
                </header>
                <div class="panel-body">
                    <form id="frm_payment_slip_ccee_market" role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>payment_slip_ccee_market">
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group ">
                            <div class="col-lg-2">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div> 
                    </form> 


                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Data de Disponibilização do Boleto</th>
                                <th>Data de Vencimento</th>
                                <th>Total Mercado (R$)</th>
                            </tr>
                        </thead>
                        <tbody id="tbl_lfpen001">
                            <?php foreach ($list as $key => $item) : ?>
                                <tr>
                                    <td><?= $item['month'] . "/" . $item['year'] ?></td>
                                    <td><input type="text" class="form-control  payment_slip_date"  month="<?= $key ?>" value="<?= $item['payment_slip_date'] ?>" alt="date"></td>
                                    <td><input type="text" class="form-control  validate"  month="<?= $key ?>"  value="<?= $item['validate'] ?>" alt="date"></td>
                                    <td><input type="text" class="form-control  total_market"  month="<?= $key ?>"  value="<?= $item['total_market'] ?>" alt="valor2"></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>

    </div>
</div>

