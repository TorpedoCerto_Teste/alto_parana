<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Eer
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Eer</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Eer
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>eer/upsert'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo / Editar
                        </button>
                    </span>
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>eer">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group ">
                            <div class="col-lg-4">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control ">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>JANEIRO</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>MARÇO</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>ABRIL</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>MAIO</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>JUNHO</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>JULHO</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>AGOSTO</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Ano</span>
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= date("Y") ?>" alt="year">
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-lg-4">
                                <span class="help-block">Agente</span>
                                <select id="fk_agent" name="fk_agent" class="form-control select2">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $type => $agent) : ?>
                                            <optgroup label="<?= $type ?>">
                                                <?php foreach ($agent as $agt) : ?>
                                                    <option value="<?= $agt['pk_agent'] ?>" <?= set_value('fk_agent') == $agt['pk_agent'] ? "selected" : "" ?>><?= $agt['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Estado</span>
                                <select id="state" name="state" class="form-control">
                                    <option value="" <?= set_value('state') == '' ? 'selected' : '' ?>>TODOS</option>
                                    <?php foreach (retorna_estados() as $sigla => $estado) : ?>
                                        <option value="<?= $sigla ?>" <?= set_value('state') == $sigla ? 'selected' : '' ?>><?= $estado ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <span class="tools pull-right">
                            <button class="btn btn-info addon-btn m-b-10" type="submit">
                                <i class="fa fa-filter pull-right"></i>
                                Filtrar
                            </button>
                        </span>
                        <div style="clear: both"></div>
                    </form>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Selecione um filtro para o relatório.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Ano/Mês</th>
                                <th>Agente</th>
                                <th>Valor <button class="btn btn-danger btn-xs btn_delete" href='#delete' data-toggle="modal">Zerar Todos os Valores</button></th>
                                <th></th>
                                <th>Dt. Pgto.</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['year'].'/'.$item['month']?></td>
                                    <td><?= $item['community_name'] ?></td>
                                    <td>
                                        <input type="text" class="form-control vlr" name="value_<?= $item['pk_eer'] ?>" id="value_<?= $item['pk_eer'] ?>" pk_eer="<?= $item['pk_eer'] ?>" value="<?= number_format($item['value'],2,'.','') ?>" alt="valor2" />
                                    </td>
                                    <td>
                                        <?php if ($item['eer_file']) : ?>
                                            <a class='btn btn-info btn-sm' href='https://docs.google.com/viewer?embedded=true&url=<?=base_url()?>uploads/eer/<?=$item['pk_eer']?>/<?=$item['eer_file']?>' target="_blank" >
                                                <i class="fa fa-file"></i>
                                            </a>
                                        <?php endif;?>
                                    </td>
                                    <td><?= $item['payment_date'] ? date("d/m/Y", strtotime($item['payment_date'])) : '' ?></td>
                                    <td>
                                        <button class="btn btn-primary btn-sm redirect" data-fk_agent="<?=$item['fk_agent']?>" data-month="<?=$item['month']?>" data-year="<?=$item['year']?>" data-upsert="false"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Deseja realmente zerar os valores?</h4>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                <button class="btn btn-danger btn_confirm" type="button">Zerar</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
