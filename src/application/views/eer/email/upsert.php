<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Encargo de Energia de Reserva - EER</title>
    </head>
    <body>
        <div style="background-color: #CCC; height:50px; text-align: center">
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999">Encargo de Energia de Reserva - EER</p>
        <p align="justify">Prezados, encaminhamos em anexo e disponibilizamos na <a href="<?= base_url() ?>customer/payment_slip_ccee" target="_blank">área do cliente</a> o relatório do recolhimento mensal de Encargo de Energia de Reserva - EER, conforme divulgado pela Câmara de Comercialização de Energia Elétrica - CCEE. O encargo é de pagamento obrigatório e foi criado em 2008, pela ANEEL, com o objetivo de cobrir os custos de contratação de usinas que venderam em leilões de Energia de Reserva - que têm por objetivo garantir a segurança no fornecimento de energia elétrica ao SIN - Sistema Interligado Nacional.</p>
        <br/>
        <br/>
        <?php if ($this->Eer_model->_value == 0) : ?>
            <!-- Caso o valor do EER do agente seja 0,00 para o mês em questão -->
            <b>Para este mês de referência, não há valor de Encargo de Energia de Reserva a ser recolhido. Como as usinas que venderam em leilões de reserva têm sua energia liquidada a PLD, em meses de PLD alto, ou quando há saldo remanescente na CONER (Conta de Energia de Reserva), os consumidores não são chamados a recolherem valores na forma de Encargo de Energia de Reserva, pois a própria liquidação a PLD cobriu o valor dos Contratos de Energia de Reserva (CER).</b>
            <br/>
            <br/>
            <table style="border: 0 px; width: 100%">
                <tr>
                    <td width="200px">Agente</td>
                    <td><?= $this->Agent_model->_community_name ?></td>
                </tr>
                <tr>
                    <td>Mês de referência</td>
                    <td><?= retorna_mes($this->Eer_model->_month) ?>/<?= $this->Eer_model->_year ?></td>
                </tr>
            </table>
            <br/>
            <br/>
        <?php else : ?>
            <!-- Caso o valor do EER do agente seja > 0,00 no mês de referência -->
            <b>Para este mês de referência, será necessário aportar valor referente à Encargo de Energia de Reserva. Como as usinas que venderam em leilões de reserva têm sua energia liquidada a PLD, em meses de PLD baixo, ou quando não há saldo remanescente na CONER (Conta de Energia de Reserva), os consumidores são chamados a recolherem valores na forma de Encargo de Energia de Reserva, pois a liquidação a PLD não foi suficiente para cobrir o valor dos Contratos de Energia de Reserva (CER).</b>
            <br/>
            <br/><b>Importante:</b> O valor a ser recolhido deverá ser transferido para a conta corrente Bradesco/CCEE (Ag. Trianon), do respectivo Agente, até a data limite, apresentada abaixo. Importante: O valor aportado como Garantia Financeira fica como caução, ou seja, bloqueado em sua conta da Ag. Trianon / Banco Bradesco, não podendo, portanto, ser utilizado para honrar o débito de Encargo de Energia de Reserva. <br/>
            <br/>
            <br/>
            <table style="border: 0 px; width: 100%">
                <tr>
                    <td width="200px">Agente</td>
                    <td><?= $this->Agent_model->_community_name ?></td>
                </tr>
                <tr>
                    <td>Mês de referência</td>
                    <td><?= retorna_mes($this->Eer_model->_month) ?>/<?= $this->Eer_model->_year ?></td>
                </tr>
                <tr>
                    <td>Valor a recolher (R$)</td>
                    <td>R$ <?=number_format($this->Eer_model->_value, 2, ",", "")?></td>
                </tr>
                <tr>
                    <td>Data pagamento</td>
                    <td><?=date("d/m/Y", strtotime($this->Eer_market_model->_payment_date))?></td>
                </tr>
            </table>
            <br/>
            <br/>
        <?php endif; ?>
        <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?= $this->config->item("config_phone") ?> e em nosso atendimento on-line, via chat da área do cliente.</p>
        <br/>
        <br/>
        <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
        <div>
            <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
            <a href="mailto:contato@altoparanaenergia.com" style="text-decoration: none; color: #999">contato@altoparanaenergia.com</a>
        </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis.
        </div>
    </body>
</html>
