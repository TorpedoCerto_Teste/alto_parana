<html>
    <head>
    </head>
    <body>
        <div id="graph" class="panel-body">
            <canvas id="mixed-chart"></canvas>
        </div>
    </body>
    <footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script>
//            var graph = new Vue({
//                el: '#graph',
//                data: {
//                    days: [],
//                    min: [],
//                    max: [],
//                    contract: [],
//                    consumo: []
//
//                },
//                mounted() {
//                    this.getDays();
//                    this.min = this.getMinMax(500);
//                    this.max = this.getMinMax(2000);
//                    this.contract = this.getMinMax(1200);
//                    this.getRandom();
//                    this.createGraph();
//
//                },
//                methods: {
//                    getDays() {
//                        for (var i = 1; i <= 30; i++) {
//                            this.days.push(i);
//                        }
//                    },
//                    getMinMax(val) {
//                        var ret = [];
//                        for (var i = 0; i < this.days.length; i++) {
//                            ret.push(val);
//                        }
//                        return ret;
//                    },
//                    getRandom() {
//                        for (var i = 0; i < this.days.length; i++) {
//                            this.consumo.push(Math.floor(Math.random() * 1000) + 500);
//                        }
//                    },
//                    createGraph() {
//                        new Chart(document.getElementById("mixed-chart"), {
//                            type: 'bar',
//                            data: {
//                                labels: this.days,
//                                datasets: [
//                                    {
//                                        label: "Mínimo",
//                                        type: "line",
//                                        borderColor: "#FFD700",
//                                        data: this.min,
//                                        fill: false
//                                    }, {
//                                        label: "Máximo",
//                                        type: "line",
//                                        borderColor: "#f3b49f",
//                                        data: this.max,
//                                        fill: false
//                                    },
//                                    {
//                                        label: "Consumida",
//                                        type: "bar",
//                                        backgroundColor: "#69c2fe",
//                                        backgroundColorHover: "#3e95cd",
//                                        data: this.consumo
//                                    },
//                                    {
//                                        label: "Contratada",
//                                        type: "line",
//                                        borderColor: "#5fc29d",
//                                        backgroundColor:
//                                                'rgb(181, 242, 219, 0.2)'
//                                        ,
//                                        data: this.contract,
//                                        fill: true
//                                    }
//                                ]
//                            },
//                            options: {
//                                title: {
//                                    display: false,
//                                },
//                                legend: {
//                                    display: true,
//                                    position: 'bottom'
//                                },
//                                scales: {
//                                    yAxes: [{
//                                            scaleLabel: {
//                                                display: true,
//                                                labelString: 'MWh',
//                                            },
//                                            ticks: {
//                                                beginAtZero: true,
//                                                steps: 1000,
//                                                stepValue: 500,
//                                                max: 2500
//                                            }
//                                        }]
//                                }
//                            }
//                        });
//                    }
//                }
//            });

            var days = getDays();
            function getDays() {
                var ret = [];
                for (var i = 1; i <= 30; i++) {
                    ret.push(i);
                }
                return ret;
            }
            ;

            var min = getMinMax(500);
            var max = getMinMax(2000);
            var contract = getMinMax(1200);
            var consumo = getRandom();

            function getMinMax(val) {
                var ret = [];
                for (var i = 0; i < this.days.length; i++) {
                    ret.push(val);
                }
                return ret;
            }

            function getRandom() {
                var ret = [];
                for (var i = 0; i < this.days.length; i++) {
                    ret.push(Math.floor(Math.random() * 1000) + 500);
                }
                return ret;
            }



            new Chart(document.getElementById("mixed-chart"), {
                type: 'bar',
                data: {
                    labels: this.days,
                    datasets: [
                        {
                            label: "Mínimo",
                            type: "line",
                            borderColor: "#FFD700",
                            data: this.min,
                            fill: false
                        }, {
                            label: "Máximo",
                            type: "line",
                            borderColor: "#f3b49f",
                            data: this.max,
                            fill: false
                        },
                        {
                            label: "Consumida",
                            type: "bar",
                            backgroundColor: "#69c2fe",
                            backgroundColorHover: "#3e95cd",
                            data: this.consumo
                        },
                        {
                            label: "Contratada",
                            type: "line",
                            borderColor: "#5fc29d",
                            backgroundColor:
                                    'rgb(181, 242, 219, 0.2)'
                            ,
                            data: this.contract,
                            fill: true
                        }
                    ]
                },
                options: {
                    title: {
                        display: false,
                    },
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'MWh',
                                },
                                ticks: {
                                    beginAtZero: true,
                                    steps: 1000,
                                    stepValue: 500,
                                    max: 2500
                                }
                            }]
                    }
                }
            });

        </script>          
    </footer>
</html>