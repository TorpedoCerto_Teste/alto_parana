<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Eventos do Calendário</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Eventos do Calendário </p>
        <?php if (isset($today) && is_array($today) && !empty($today)) : ?>
            <b><?= date("d/m/Y") ?></b>
            <br/>
            <table style="border:1px solid black; width: 100%">
                <?php if (isset($today) && !empty($today)) : ?>
                    <?php foreach ($today as $event) : ?>
                        <tr>
                            <td width="300px"><?= $event['agent'] != "" ? $event['agent'] : $event['responsible'] ?></td>
                            <td><?= $event['calendar'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>                    
            </table>
        <?php endif; ?>
        <?php if (isset($tomorrow) && is_array($tomorrow) && !empty($tomorrow)) :
            ?>
            <br/>
            <b><?= date("d/m/Y", strtotime("+1 DAY")) ?></b>
            <br/>
            <table style="border:1px solid black; width: 100%">
                <?php foreach ($tomorrow as $event) : ?>
                    <tr>
                        <td width="300px"><?= $event['agent'] != "" ? $event['agent'] : $event['responsible'] ?></td>
                        <td><?= $event['calendar'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
        <br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>

    </body>
</html>