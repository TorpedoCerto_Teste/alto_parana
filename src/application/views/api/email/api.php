<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Resultado de Processamento de Arquivo</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Resultado de Processamento de Arquivo </p>
        <br>
        <table>
            <tr>
                <td>Data</td>
                <td><?= date("d/m/Y H:i")?></td>
            </tr>
            <tr>
                <td>Arquivo</td>
                <td><?= $file ?></td>
            </tr>
            <tr>
                <td>Procedimento</td>
                <td><?= $function?></td>
            </tr>
        </table>
        <br/>
        <br/>
        <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
        <div>
            <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
            <a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
        </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>

    </body>
</html>