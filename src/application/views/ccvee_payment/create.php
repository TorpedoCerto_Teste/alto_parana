<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Grupo
    </h3>
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Grupo
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>group/create/">
                        <input type="hidden" name="create" id="create" value="true" />
                        
                        <div class="form-group <?= form_error('group') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="group">Grupo</label>
                            <div class="col-lg-6">
                                <input type="text" placeholder="" id="group" name="group" class="form-control" value="<?= set_value('group') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('group_short_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="group">Nome Curto</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="group_short_name" name="group_short_name" class="form-control" value="<?= set_value('group_short_name') ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
