<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Pagamentos
    </h3>
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Pagamentos</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Pagamentos
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_payment/update/<?= $this->Ccvee_payment_model->_fk_ccvee_general ?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <input type="hidden" name="base_url" id="base_url" disabled="disabled" value="<?= base_url() ?>" />
                        <input type="hidden" name="fk_ccvee_general" id="fk_ccvee_general" disabled="disabled" value="<?= $this->Ccvee_payment_model->_fk_ccvee_general ?>"/>
                        <input type="hidden" name="total_months" id="total_months" disabled="disabled" value="<?= $payment_prices ? count($payment_prices) : 0 ?>" />

                        <div class="form-group <?= form_error('due') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="due">Dia de vencimento</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="due" name="due">
                                    <option value="dia_util" <?= $this->Ccvee_payment_model->_due == "dia_util" ? "selected" : "" ?>>Dia útil</option>
                                    <option value="dia_fixo" <?= $this->Ccvee_payment_model->_due == "dia_fixo" ? "selected" : "" ?>>Dia fixo</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control" id="due_day" name="due_day">
                                    <?php for ($i = 1; $i <= 28; $i++) : ?>
                                        <option value="<?= $i ?>" <?= $this->Ccvee_payment_model->_due_day == $i ? "selected" : "" ?>><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('due_period') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="due_period">Mês de vencimento</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="due_period" name="due_period">
                                    <option value="M" <?= $this->Ccvee_payment_model->_due_period == "M" ? "selected" : "" ?>>Mês Fornecimento</option>
                                    <option value="MA" <?= $this->Ccvee_payment_model->_due_period == "MA" ? "selected" : "" ?>>Meses Antes</option>
                                    <option value="MS" <?= $this->Ccvee_payment_model->_due_period == "MS" ? "selected" : "" ?>>Meses Subsequentes</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="due_month" name="due_month" class="form-control" value="<?= set_value('due_month') != "" ? set_value('due_month') : $this->Ccvee_payment_model->_due_month ?>" alt="year">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('financial_index') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="financial_index">Índice de Reajuste</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="financial_index" name="financial_index">
                                    <option value="IGPM" <?= $this->Ccvee_payment_model->_financial_index == "IGPM" ? "selected" : "" ?>>IGP-M</option>
                                    <option value="IPCA" <?= $this->Ccvee_payment_model->_financial_index == "IPCA" ? "selected" : "" ?>>IPCA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('index_readjustment_calculation') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="index_readjustment_calculation">Modo de Cálculo do Índice de Reajuste</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="index_readjustment_calculation" name="index_readjustment_calculation">
                                    <option value="1" <?= $this->Ccvee_payment_model->_index_readjustment_calculation == "1" ? "selected" : "" ?>>Padrão</option>
                                    <option value="2" <?= $this->Ccvee_payment_model->_index_readjustment_calculation == "2" ? "selected" : "" ?>>Não realizar o reajuste se o valor ajustado for menor que o valor atual</option>
                                    <option value="3" <?= $this->Ccvee_payment_model->_index_readjustment_calculation == "3" ? "selected" : "" ?>>Não considerar índices negativos no cálculo</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('database') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="database">Data base</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="database" name="database" class="form-control default-date-picker" value="<?= set_value('database') != "" ? set_value('database') : date_to_human_date($this->Ccvee_payment_model->_database) ?>" alt="date">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('first_adjustment') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="first_adjustment">Data do Primeiro Reajuste</label>
                            <div class="col-lg-10">
                                <input type="radio" name="first_adjustment" id="insert_date" value="0" <?= $this->Ccvee_payment_model->_adjustment_montly_manually != 1 ? "checked" : "" ?>/> Inserir Data
                                <br/>
                                <input type="radio" name="first_adjustment" id="insert_manually" value="1" <?= $this->Ccvee_payment_model->_adjustment_montly_manually == 1 ? "checked" : "" ?>/> Apontar Pontos Mensais de Reajuste Manualmente
                            </div>
                        </div>

                        <div id="div_date_adjustment" name="div_date_adjustment" class="form-group <?= form_error('date_adjustment') != "" ? "has-error" : ""; ?>" style="display: <?= $this->Ccvee_payment_model->_adjustment_montly_manually != 1 ? "block" : "none" ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="date_adjustment">Inserir Data</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="date_adjustment" name="date_adjustment" class="form-control default-date-picker" value="<?= set_value('date_adjustment') != "" ? set_value('date_adjustment') : date_to_human_date($this->Ccvee_payment_model->_date_adjustment) ?>" alt="date">
                            </div>
                            <div class="col-lg-2">
                                <select name="date_adjustment_frequency" id="date_adjustment_frequency" class="form-control">
                                    <option value="12em12" <?= $this->Ccvee_payment_model->_date_adjustment_frequency == "12em12" ? "selected" : "" ?>>12 em 12</option>
                                    <option value="todoJaneiro" <?= $this->Ccvee_payment_model->_date_adjustment_frequency == "todoJaneiro" ? "selected" : "" ?>>Todo Janeiro</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('price_flexibility') != "" ? "has-error" : ""; ?>" >
                            <label class="col-lg-2 col-sm-2 control-label" for="price_flexibility">Flexibilidade de Preço</label>
                            <div class="col-lg-4">
                                <select name="price_flexibility" id="price_flexibility" class="form-control">
                                    <option value="fixo" <?= $this->Ccvee_payment_model->_price_flexibility == "fixo" ? "selected" : "" ?>>Fixo em R$/MWh</option>
                                    <option value="pld" <?= $this->Ccvee_payment_model->_price_flexibility == "pld" ? "selected" : "" ?>>PLD +/- Spread</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('different_price_period') != "" ? "has-error" : ""; ?>" >
                            <label class="col-lg-2 col-sm-2 control-label" for="different_price_period">Preço Diferenciado por Período de Consumo</label>
                            <div class="col-lg-10">
                                <input type="radio" name="different_price_period" id="different_price_period" value="0" <?= $this->Ccvee_payment_model->_different_price_period != 1 ? "checked" : "" ?>/> Não
                                <br/>
                                <input type="radio" name="different_price_period" id="different_price_period" value="1" <?= $this->Ccvee_payment_model->_different_price_period == 1 ? "checked" : "" ?>/> Sim
                            </div>
                        </div>

                        <!-- campos de preços  SE preço não for diferenciado, vai mostrar apenas 1 campo de preço, senão, vários campos -->

                        <!-- não diferenciado -->
                        <div name="div_not_different_price_period" id="div_not_different_price_period" style="display: <?= $this->Ccvee_payment_model->_different_price_period != 1 ? "block" : "none" ?>">
                            <div class="form-group <?= form_error('price_mwh') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="price_mwh">Preço em R$/MWh</label>
                                <div class="col-lg-2">
                                    <input type="text" placeholder="" id="price_mwh" name="price_mwh" class="form-control" value="<?= set_value('price_mwh') != "" ? set_value('price_mwh') : number_format($this->Ccvee_payment_model->_price_mwh, 4, ",", "") ?>" alt="valor4">
                                </div>
                            </div>

                        </div>

                        <!--diferenciado-->
                        <div name="div_different_price_period" id="div_different_price_period" style="display: <?= $this->Ccvee_payment_model->_different_price_period == 1 ? "block" : "none" ?>">
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="payment_prices">Preço em R$/MWh</label>
                                <div class="col-lg-10" name="table_payment_prices" id="table_payment_prices">
                                    <table class="table responsive-data-table data-table">
                                        <thead>
                                            <tr>
                                                <td>Período</td>
                                                <td>Previsto/Base</td>
                                                <td>Corrigido/Reajustado</td>
                                                <td>Ajustado Manualmente</td>
                                                <td>Previsão de Preço Ajustado</td>
                                                <td>Apontamento de Mês Reajuste</td>
                                                <td>Copiar</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($payment_prices) : ?>
                                                <?php foreach ($payment_prices as $k => $payment_price) : ?>
                                                    <tr>
                                                        <td><input type="text" placeholder="" id="period<?= $k ?>" name="payment_prices[<?= $k ?>][period]" class="form-control" value="<?= date("m/Y", strtotime($payment_price['period'])) ?>"></td>
                                                        <td><input type="text" placeholder="" id="provided_base<?= $k ?>" name="payment_prices[<?= $k ?>][provided_base]" class="form-control" value="<?= number_format($payment_price['provided_base'], 4, ",", "") ?>" alt="valor4"></td>
                                                        <td><input type="text" placeholder="" id="fixed<?= $k ?>" name="payment_prices[<?= $k ?>][fixed]" class="form-control" value="<?= number_format($payment_price['fixed'], 4, ",", "") ?>" alt="valor4"></td>
                                                        <td><input type="text" placeholder="" id="manual_fixed<?= $k ?>" name="payment_prices[<?= $k ?>][manual_fixed]" class="form-control" value="<?= number_format($payment_price['manual_fixed'], 4, ",", "") ?>" alt="valor4"></td>
                                                        <td><input type="text" placeholder="" id="provided_fixed<?= $k ?>" name="payment_prices[<?= $k ?>][provided_fixed]" class="form-control" value="<?= number_format($payment_price['provided_fixed'], 4, ",", "") ?>" alt="valor4"></td>
                                                        <td><input type="checkbox" placeholder="" id="point_monthly<?= $k ?>" name="payment_prices[<?= $k ?>][point_monthly]" class="form-control" value="1" <?= $payment_price['point_monthly'] == 1 ? "checked" : "" ?>></td>
                                                        <td><button data-toggle="button" class="btn btn-sm btn-default copy" value="copy<?= $k ?>" onclick="copy_values(<?= $k ?>)"><i class="fa fa-copy"></i></button></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>                                        
                                </div>
                            </div>

                        </div>



                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
