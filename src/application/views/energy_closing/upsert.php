<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Fechamento Energia
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Fechamento Energia</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Fechamento Energia
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <?php if ($success) : ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Dados gravados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form id='frm_energy_closing' role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>energy_closing/upsert" enctype="multipart/form-data">
                        <input type="hidden" name="upsert" id="upsert" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Agente</label>
                            <div class="col-lg-6">
                                <select id="fk_agent" name="fk_agent" class="form-control select2">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $type => $agent) : ?>
                                            <optgroup label="<?= $type ?>">
                                                <?php foreach ($agent as $agt) : ?>
                                                    <option value="<?= $agt['pk_agent'] ?>" <?= $this->Energy_closing_model->_fk_agent == $agt['pk_agent'] ? "selected" : "" ?>><?= $agt['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Período</label>
                            <div class="col-lg-4 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= $i ?>" <?= $this->Energy_closing_model->_month == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= $this->Energy_closing_model->_year != "" ? $this->Energy_closing_model->_year : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div>

                        <?php if ($unities) : ?>
                            <table class="table table-responsive table-striped">
                                <thead>
                                    <tr>
                                        <th>Unidade</th>
                                        <th>Consumo (MWh)</th>
                                        <th>Perdas (MWh)</th>
                                        <th>Proinfa (MWh)</th>
                                        <th>Total Faturado (MWh)</th>
                                        <th>Preço Médio ACL (R$/MWh)</th>
                                        <th>Relatório</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($unities as $pk_unity => $unity) : ?>
                                        <tr>
                                            <td><?= $unity['community_name'] ?></td>
                                            <td><input type="text" class="form-control consumption" pk_unity="<?= $unity['pk_unity'] ?>" id="consumption_<?= $unity['pk_unity'] ?>" name="unity[<?= $unity['pk_unity'] ?>][consumption]" value="<?= isset($unity['consumption']) ? number_format($unity['consumption'], 3, ",", "") : 0 ?>" alt="valor3"></td>
                                            <td><input type="text" class="form-control loosing" pk_unity="<?= $unity['pk_unity'] ?>" id="loosing_<?= $unity['pk_unity'] ?>" name="unity[<?= $unity['pk_unity'] ?>][loosing]" value="<?= isset($unity['loosing']) ? number_format($unity['loosing'], 3, ",", "") : 0 ?>" alt="valor3"></td>
                                            <td><input type="text" class="form-control proinfa" pk_unity="<?= $unity['pk_unity'] ?>" id="proinfa_<?= $unity['pk_unity'] ?>" name="unity[<?= $unity['pk_unity'] ?>][proinfa]" value="<?= isset($unity['proinfa']) ? number_format($unity['proinfa'], 3, ",", "") : 0 ?>" alt="valor3"></td>
                                            <td><input type="text" class="form-control total" pk_unity="<?= $unity['pk_unity'] ?>" id="total_<?= $unity['pk_unity'] ?>" name="unity[<?= $unity['pk_unity'] ?>][total]" value="<?= isset($unity['total']) ? number_format($unity['total'], 3, ",", "") : 0 ?>" alt="valor3"></td>
                                            <td><input type="text" class="form-control acl_average" pk_unity="<?= $unity['pk_unity'] ?>" id="acl_average_<?= $unity['pk_unity'] ?>" name="unity[<?= $unity['pk_unity'] ?>][acl_average]" value="<?= isset($unity['acl_average']) ? number_format($unity['acl_average'], 2, ",", "") : 0 ?>" alt="valor2"></td>
                                            <td>
                                                <?php if ($unity['energy_closing_unity_file'] != "") : ?>
                                                    <a class="btn btn-info" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/energy_closing_unity/<?= $unity['fk_energy_closing'] ?>/<?= $unity['energy_closing_unity_file'] ?>" target="_blank">Visualizar Arquivo</a>
                                                <?php else : ?>
                                                    <input type="file" class="form-control" id="report_<?= $unity['pk_unity'] ?>" name="report[<?= $unity['pk_unity'] ?>]" >
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">Salvar</button>
                                </div>
                            </div>
                        <?php endif; ?>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>

