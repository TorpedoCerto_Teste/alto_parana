<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Way2 - API
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>telemetry_record">Way2 - Medições</a></li>
            <li class="active">Way2 - API</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Way2 - Importar Medições por API
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: block">
                    <form role="form" class="form-inline" method="POST" action="<?= base_url() ?>telemetry_record/api" id="frm_telemetry_record" name="frm_telemetry_record">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <div class="alert alert-info fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Importante!</strong> Ao executar a API, não saia desta janela ou feche o browser. Este processo pode ser demorado, dependendo do período selecionado.
                        </div>

                        <div class="form-group col-lg-12">
                            <div class="col-md-4">
                                <span class="help-block">Período de Medições</span>
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="<?= set_value('start_date') != "" ? set_value('start_date') : date("01/m/Y") ?>" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="<?= set_value('end_date') != "" ? set_value('end_date') : date("d/m/Y") ?>" alt="date" >
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <span class="help-block">Medidor</span>
                                <select id="fk_gauge" name="fk_gauge" class="form-control select2">
                                    <?php if ($gauges) : ?>
                                        <?php foreach ($gauges as $gauge) : ?>
                                            <option>TODOS MEDIDORES</option>
                                            <optgroup label="<?= $gauge['community_name'] ?>">
                                                <?php foreach ($gauge['gauges'] as $medidor) : ?>
                                                    <option value="<?= $medidor['pk_gauge'] ?>"><?= $medidor['gauge'] ?> - <?= $medidor['internal_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">&nbsp;</span>
                                <button class="btn btn-success" type="submit">Iniciar Importação de Medições</button>
                            </div>
                            
                        </div>

                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>


