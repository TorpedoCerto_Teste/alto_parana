<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Way2 - Medições
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Way2 - Medições</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Way2 - Medições
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>telemetry_record/api/'">
                            <i class="fa fa-plus pull-right"></i>
                            Importar Medições via API
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: none">
                    <form role="form" class="form-inline" method="POST" action="<?= base_url() ?>telemetry_record" id="frm_telemetry_record" name="frm_telemetry_record">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>
                        <input type="hidden" name="export_excel" id="export_excel" value="false" />

                        <div class="form-group col-lg-12">
                            
                            <div class="col-lg-2 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control select2">
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?= date("m", strtotime($date)) == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= date("Y", strtotime($date)) ?>" alt="year"
                                       data-bts-min="2010"
                                       data-bts-max="<?= date("Y") ?>"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                            
                            <div class="col-lg-4">
                                <span class="help-block">Medidor</span>
                                <select id="fk_gauge" name="fk_gauge" class="form-control select2">
                                    <?php if ($gauges) : ?>
                                        <?php foreach ($gauges as $gauge) : ?>
                                            <option></option>
                                            <optgroup label="<?= $gauge['community_name'] ?>">
                                                <?php foreach ($gauge['gauges'] as $medidor) : ?>
                                                    <option value="<?= $medidor['pk_gauge'] ?>"><?= $medidor['gauge'] ?> - <?= $medidor['internal_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            
                            <div class="col-lg-2">
                                <span class="help-block">Tipo</span>
                                <select id="greatness" name="greatness" class="form-control">
                                    <option value="Eneat">Eneat (Horário)</option>
                                    <option value="Demat">Demat (A cada 15 minutos)</option>
                                </select>
                            </div>
                            
                        </div>
                    </form> 
                </div>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <div id="chart_data" style="display: none"><?= $graphic ?></div>
                    <div id="columnChart"></div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Valor</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr style="color: <?= $item['quality'] == 0 ? 'green' : 'red' ?>">
                                    <td><?= date("d/m/Y H:i", strtotime($item['record_date'])) ?></td>
                                    <td><?= number_format($item['value'], 2, ",", "") ?></td>
                                    <td><?= $item['quality'] == 0 ? "OK" : "ERRO" ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


