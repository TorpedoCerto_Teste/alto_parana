<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="author" content="Mosaddek" />
        <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina" />
        <meta name="description" content="" />
        <link rel="shortcut icon" href="<?= base_url() ?>img/ico/favicon.ico">

        <title><?php echo $_SERVER['CI_ENV'] != 'production' ? "[STAGE]" : "" ?>Alto Paraná Energia</title>

        <!--jquery-ui-->
        <link href="<?= base_url() ?>js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />

        <!--common style-->
        <link href="<?= base_url() ?>css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/style-responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?= base_url() ?>js/html5shiv.js"></script>
        <script src="<?= base_url() ?>js/respond.min.js"></script>
        <![endif]-->
        <?php if (isset($css_include)) print_r($css_include); ?>
    </head>

    <body class="sticky-header <?= $this->session->userdata('sidebar_collapsed') ? "sidebar-collapsed" : "" ?>">
        <div id="body_base_url" style="display: none"><?= base_url() ?></div>
        <section>
            <!-- sidebar left start-->
            <div class="sidebar-left sticky-sidebar">
                <!--responsive view logo start-->
                <div class="logo dark-logo-bg" >
                    <a href="<?= base_url() ?>">
                        <img src="<?= base_url() ?>img/APE_3.png" alt="">
                    </a>
                </div> 
                <!--responsive view logo end-->

                <div class="sidebar-left-info">
                    <!-- visible small devices start-->
                    <div class=" search-field">  </div>
                    <!-- visible small devices end-->

                    <!--sidebar nav start-->
                    <ul class="nav nav-pills nav-stacked side-navigation">
                        <li class="<?= $this->router->fetch_class() == 'index' ? 'active' : '' ?>"><a href="<?= base_url() ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li>
                            <h3 class="navigation-title">Geral</h3>
                        </li>
                        <li class="<?= $this->router->fetch_class() == 'agent' ? 'active' : '' ?>"><a href="<?= base_url() ?>agent"><i class="fa fa-user"></i> <span>Contas / Agentes</span></a></li>
                        
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'submarket' ||
                                $this->router->fetch_class() == 'agent_type' ||
                                $this->router->fetch_class() == 'hourly_basis' ||
                                $this->router->fetch_class() == 'category' ||
                                $this->router->fetch_class() == 'standard' ||
                                $this->router->fetch_class() == 'gauge_record' ||
                                $this->router->fetch_class() == 'energy_type' ||
                                $this->router->fetch_class() == 'product' ||
                                $this->router->fetch_class() == 'gauge_record' ||
                                $this->router->fetch_class() == 'pld'
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-laptop"></i>  <span>Cadastros CCEE</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'category' ? 'active' : '' ?>"><a href="<?= base_url() ?>category"> Categorias</a></li>
                                <li class="<?= $this->router->fetch_class() == 'standard' ? 'active' : '' ?>"><a href="<?= base_url() ?>standard"> Classes</a></li>
                                <li class="<?= $this->router->fetch_class() == 'submarket' ? 'active' : '' ?>"><a href="<?= base_url() ?>submarket"> Submercados</a></li>
                                <li class="<?= $this->router->fetch_class() == 'agent_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>agent_type"> Tipos de Agente</a></li>
                                <li class="<?= $this->router->fetch_class() == 'energy_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>energy_type"> Tipos de Energia</a></li>
                                <li class="<?= $this->router->fetch_class() == 'gauge_record' ? 'active' : '' ?>"><a href="<?= base_url() ?>gauge_record"> SCDE - Medições </a></li>
                                <li class="<?= $this->router->fetch_class() == 'pld' ? 'active' : '' ?>"><a href="<?= base_url() ?>pld"> PLD </a></li>
                            </ul>
                        </li>
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'user' ||
                                $this->router->fetch_class() == 'contact' ||
                                $this->router->fetch_class() == 'user_profile' ||
                                $this->router->fetch_class() == 'message' ||
                                $this->router->fetch_class() == 'group' ||
                                $this->router->fetch_class() == 'agent_contact' ||
                                $this->router->fetch_class() == 'calendar' ||
                                $this->router->fetch_class() == 'financial_index' ||
                                $this->router->fetch_class() == 'icms' ||
                                $this->router->fetch_class() == 'free_captive' ||
                                $this->router->fetch_class() == 'newsletter' ||
                                $this->router->fetch_class() == 'energy_market' ||
                                $this->router->fetch_class() == 'devec_customer'
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-coffee"></i>  <span>Administrativo</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'user' ? 'active' : '' ?>"><a href="<?= base_url() ?>user"> Usuários</a></li>
                                <li class="<?= $this->router->fetch_class() == 'contact' ? 'active' : '' ?>"><a href="<?= base_url() ?>contact"> Contatos</a></li>
                                <li class="<?= $this->router->fetch_class() == 'user_profile' ? 'active' : '' ?>"><a href="<?= base_url() ?>user_profile"> Perfis de Usuário</a></li>
                                <li class="<?= $this->router->fetch_class() == 'group' ? 'active' : '' ?>"><a href="<?= base_url() ?>group"> Grupos</a></li>
                                <li class="<?= $this->router->fetch_class() == 'agent_contact' ? 'active' : '' ?>"><a href="<?= base_url() ?>agent_contact"> Agentes e Contatos</a></li>
                                <li class="<?= $this->router->fetch_class() == 'message' ? 'active' : '' ?>"><a href="<?= base_url() ?>message"> Mensagens</a></li>
                                <li class="<?= $this->router->fetch_class() == 'calendar' ? 'active' : '' ?>"><a href="<?= base_url() ?>calendar"> Calendário</a></li>
                                <li class="<?= $this->router->fetch_class() == 'financial_index' ? 'active' : '' ?>"><a href="<?= base_url() ?>financial_index"> Cálculo de Reajuste</a></li>
                                <li class="<?= $this->router->fetch_class() == 'icms' ? 'active' : '' ?>"><a href="<?= base_url() ?>icms"> ICMS</a></li>
                                <li class="<?= $this->router->fetch_class() == 'energy_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>energy_market"> Energy Market</a></li>
                                <li class="<?= $this->router->fetch_class() == 'newsletter' ? 'active' : '' ?>"><a href="<?= base_url() ?>newsletter"> Newsletter </a></li>
                                <li class="<?= $this->router->fetch_class() == 'free_captive' ? 'active' : '' ?>"><a href="<?= base_url() ?>free_captive"> Relatório de Energia </a></li>
                                <li class="<?= $this->router->fetch_class() == 'devec_customer' ? 'active' : '' ?>"><a href="<?= base_url() ?>devec_customer"> DEVEC </a></li>
                                <li class="<?= $this->router->fetch_class() == 'energy_offer' ? 'active' : '' ?>"><a href="<?= base_url() ?>energy_offer"> Ofertas de Energia</a></li>


                            </ul>
                        </li>
                        
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'rush_hour' ||
                                $this->router->fetch_class() == 'bank_charges' ||
                                $this->router->fetch_class() == 'tariff_flags' ||
                                $this->router->fetch_class() == 'tusd' ||
                                $this->router->fetch_class() == 'devec_market'
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-dollar"></i>  <span>Tarifas</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'rush_hour' ? 'active' : '' ?>"><a href="<?= base_url() ?>rush_hour"> Horário de Ponta</a></li>
                                <li class="<?= $this->router->fetch_class() == 'bank_charges' ? 'active' : '' ?>"><a href="<?= base_url() ?>bank_charges"> Banco de Tarifas</a></li>
                                <li class="<?= $this->router->fetch_class() == 'tariff_flags' ? 'active' : '' ?>"><a href="<?= base_url() ?>tariff_flags"> Bandeiras Tarifárias</a></li>
                                <li class="<?= $this->router->fetch_class() == 'tusd' ? 'active' : '' ?>"><a href="<?= base_url() ?>tusd/upsert"> TUSD</a></li>
                                <li class="<?= $this->router->fetch_class() == 'devec_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>devec_market"> DEVEC por UF</a></li>
                            </ul>
                        </li>                        
                        
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'ccvee_general' ||
                                $this->router->fetch_class() == 'modality_warranty' 
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-file-text-o"></i>  <span>Contratos de Energia</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'ccvee_general' ? 'active' : '' ?>"><a href="<?= base_url() ?>ccvee_general"> CCVEE</a></li>
                                <li class="<?= $this->router->fetch_class() == 'modality_warranty' ? 'active' : '' ?>"><a href="<?= base_url() ?>modality_warranty"> Modalidades de Garantias</a></li>
                            </ul>
                        </li>                        
                        
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'payment_slip_ccee_market' ||
                                $this->router->fetch_class() == 'eer_market' ||
                                $this->router->fetch_class() == 'ercap_market' ||
                                $this->router->fetch_class() == 'accounting_market' ||
                                $this->router->fetch_class() == 'pen001_market' ||
                                $this->router->fetch_class() == 'lfpen001_market' 
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa  fa-pencil-square"></i>  <span>Operação CCEE Mercado</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'payment_slip_ccee_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>payment_slip_ccee_market"> Contribuição Associativa Mercado </a></li>
                                <li class="<?= $this->router->fetch_class() == 'eer_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>eer_market"> EER Mercado</a></li>
                                <li class="<?= $this->router->fetch_class() == 'ercap_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>ercap_market"> ERCAP Mercado</a></li>
                                <li class="<?= $this->router->fetch_class() == 'accounting_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>accounting_market"> Contabilização Mercado</a></li>
                                <li class="<?= $this->router->fetch_class() == 'pen001_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>pen001_market"> PEN001 Mercado</a></li>
                                <li class="<?= $this->router->fetch_class() == 'lfpen001_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>lfpen001_market"> LFPEN001 Mercado</a></li>
                            </ul>
                        </li>                        
                        
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'payment_slip_ccee' || 
                                $this->router->fetch_class() == 'eer' || 
                                $this->router->fetch_class() == 'ercap' || 
                                $this->router->fetch_class() == 'accounting' || 
                                $this->router->fetch_class() == 'pen001' || 
                                $this->router->fetch_class() == 'lfpen001' 
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-pencil"></i>  <span>Operação CCEE Individual</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'payment_slip_ccee' ? 'active' : '' ?>"><a href="<?= base_url() ?>payment_slip_ccee"> Contribuição Associativa </a></li>
                                <li class="<?= $this->router->fetch_class() == 'eer' ? 'active' : '' ?>"><a href="<?= base_url() ?>eer"> EER</a></li>
                                <li class="<?= $this->router->fetch_class() == 'ercap' ? 'active' : '' ?>"><a href="<?= base_url() ?>ercap"> ERCAP</a></li>
                                <li class="<?= $this->router->fetch_class() == 'accounting' ? 'active' : '' ?>"><a href="<?= base_url() ?>accounting"> Contabilização</a></li>
                                <li class="<?= $this->router->fetch_class() == 'pen001' ? 'active' : '' ?>"><a href="<?= base_url() ?>pen001"> PEN001 </a></li>
                                <li class="<?= $this->router->fetch_class() == 'lfpen001' ? 'active' : '' ?>"><a href="<?= base_url() ?>lfpen001"> LFPEN001 </a></li>
                            </ul>
                        </li>                        
                        
                        <li class="menu-list <?php
                        if (
                                $this->router->fetch_class() == 'product_category' || 
                                $this->router->fetch_class() == 'service' || 
                                $this->router->fetch_class() == 'business'
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-keyboard-o"></i>  <span>Contrato de Serviços</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'product_category' || $this->router->fetch_class() == 'product' ? 'active' : '' ?>"><a href="<?= base_url() ?>product_category"> Categorias de Produtos</a></li>
                                <li class="<?= $this->router->fetch_class() == 'service' ? 'active' : '' ?>"><a href="<?= base_url() ?>service"> Serviços</a></li>
                                <li class="<?= $this->router->fetch_class() == 'business' ? 'active' : '' ?>"><a href="<?= base_url() ?>business"> Negócios</a></li>
                            </ul>
                        </li>                        
                        
                        <li class="menu-list <?php
                        if (
                                
                                $this->router->fetch_class() == 'telemetry_comparison' || 
                                $this->router->fetch_class() == 'telemetry_record' || 
                                $this->router->fetch_class() == 'gauge_record' ||
                                $this->router->fetch_class() == 'odc_record'
                        ) {
                            echo "nav-active";
                        }
                        ?> ">
                            <a href="#"><i class="fa fa-rss"></i>  <span>Telemetrias</span></a>
                            <ul class="child-list">
                                <li class="<?= $this->router->fetch_class() == 'telemetry_comparison' ? 'active' : '' ?>"><a href="<?= base_url() ?>telemetry_comparison"> Comparação de Telemetrias</a></li>
                                <li class="<?= $this->router->fetch_class() == 'telemetry_record' ? 'active' : '' ?>"><a href="<?= base_url() ?>telemetry_record"> Telemetrias Way2</a></li>
                                <li class="<?= $this->router->fetch_class() == 'gauge_record' ? 'active' : '' ?>"><a href="<?= base_url() ?>gauge_record"> Registros SCDE MC</a></li>
                                <li class="<?= $this->router->fetch_class() == 'odc_record' ? 'active' : '' ?>"><a href="<?= base_url() ?>odc_record"> Registros SCDE ODC</a></li>
                            </ul>
                        </li>                        
                        
                    </ul>
                    <!--sidebar nav end-->

                    <!--sidebar widget start-->
                    <div class="sidebar-widget">
                    </div>
                    <!--sidebar widget end-->

                </div>
            </div>
            <!-- sidebar left end-->
            <!-- body content start-->
            <div class="body-content" >

                <!-- header section start-->
                <div class="header-section">

                    <!--logo and logo icon start-->
                    <div class="logo dark-logo-bg hidden-xs hidden-sm">
                        <a href="<?= base_url() ?>">
                            <img src="<?= base_url() ?>img/APE_3.png" alt="">
                        </a>
                    </div>

                    <div class="icon-logo dark-logo-bg hidden-xs hidden-sm">
                        <a href="<?= base_url() ?>">
                            <img src="<?= base_url() ?>img/APE_1.png" width="24px">
                        </a>
                    </div>
                    <!--logo and logo icon end-->

                    <!--toggle button start-->
                    <a class="toggle-btn" id="toggle_sidebar"><i class="fa fa-outdent"></i></a>
                    <!--toggle button end-->

                    <!--mega menu start-->
                    <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                        <ul class="nav navbar-nav">

                        </ul>
                    </div>
                    <!--mega menu end-->
                    <div class="notification-wrap">
                        <!--left notification start-->
                        <div class="left-notification">
                            <ul class="notification-menu">
                                <!--mail info start-->
                                <li class="d-none">
                                    <?php if ($this->events) : ?>
                                        <a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                                            <i class="fa fa-calendar"></i>
                                            <span class="badge bg-primary"><?= count($this->events) ?></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-title">
                                            <div class="title-row">
                                                <h5 class="title purple">
                                                    &nbsp;
                                                </h5>
                                                <a href="<?= base_url() ?>calendar" class="btn-primary btn-view-all">Abrir Calendário</a>
                                            </div>
                                            <div class="notification-list mail-list">
                                                <?php foreach ($this->events as $event) : ?>
                                                    <a href="<?= base_url() ?>calendar" class="single-mail">
                                                        <span class="icon bg-<?= $this->colors[$event['fk_calendar_responsible']] ?>">
                                                            <?= substr($event['responsible'], 0, 1) ?> 
                                                        </span>
                                                        <strong><?= $event['responsible'] ?></strong>
                                                        <small>Referência: <?= $event['month_reference'] ?>/<?= $event['year_reference'] ?></small>
                                                        <p>
                                                            <small><small><?= $event['calendar'] ?></small></small>
                                                        </p>
                                                    </a>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                    <a href="<?=base_url()?>calendar" class="btn btn-default dropdown-toggle info-number">
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                    <?php endif; ?>
                                </li>
                                <!--mail info end-->

                            </ul>
                        </div>
                        <!--left notification end-->


                        <!--right notification start-->
                        <div class="right-notification">
                            <ul class="notification-menu">
                                <li>
                                    <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <?= $this->session->userdata('name') ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                                        <li><a href="<?= base_url() ?>index/logout"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                        <!--right notification end-->
                    </div>

                </div>
                <!-- header section end-->        
