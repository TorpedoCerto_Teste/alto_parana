</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url() ?>js/jquery-1.10.2.min.js"></script>

<!--jquery-ui-->
<!--<script src="<?= base_url() ?>js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>-->
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
crossorigin="anonymous"></script>

<script src="<?= base_url() ?>js/jquery-migrate.js"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/modernizr.min.js"></script>

<!--Nice Scroll-->
<script src="<?= base_url() ?>js/jquery.nicescroll.js" type="text/javascript"></script>

<script src="<?= base_url() ?>js/scripts.js"></script>
<?php if (isset($js_include)) print_r($js_include); ?>


</body>


</html>
