<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Base Horária
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Base Horária</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Bases Horárias
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>hourly_basis/create'">
                            <i class="fa fa-plus pull-right"></i>
                            Inserir valores
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: none">
                    <form role="form" class="form-inline" method="POST" action="<?= base_url() ?>hourly_basis">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="month" name="month">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>Janeiro</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>Fevereiro</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>Março</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>Abril</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>Maio</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>Junho</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>Julho</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>Agosto</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>Setembro</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>Outubro</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>Novembro</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>Dezembro</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group <?= form_error('week') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-4 col-sm-2 control-label" for="week">Semana</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="week" name="week">
                                    <option value="1" <?= $previous_week == "1" ? "selected" : "" ?>>1</option>
                                    <option value="2" <?= $previous_week == "2" ? "selected" : "" ?>>2</option>
                                    <option value="3" <?= $previous_week == "3" ? "selected" : "" ?>>3</option>
                                    <option value="4" <?= $previous_week == "4" ? "selected" : "" ?>>4</option>
                                    <option value="5" <?= $previous_week == "5" ? "selected" : "" ?>>5</option>
                                    <option value="6" <?= $previous_week == "6" ? "selected" : "" ?>>6</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Filtrar</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>
                                    Data
                                </th>
                                <th>
                                    Tarifa
                                </th>
                                <th>
                                    Tipo
                                </th>
                                <th>
                                    PLD Sul
                                </th>
                                <th>
                                    PLD Sudeste
                                </th>
                                <th>
                                    PLD Norte
                                </th>
                                <th>
                                    PLD Nordeste
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= date("d/m/Y H:i", strtotime($item['date'])) ?></td>
                                    <td><?= $item['landing'] ?></td>
                                    <td><?= $item['hour_type'] ?></td>
                                    <td><?= $item['pld_south'] ?></td>
                                    <td><?= $item['pld_southeast'] ?></td>
                                    <td><?= $item['pld_north'] ?></td>
                                    <td><?= $item['pld_northeast'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>unity/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_unity" id="pk_unity" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
