<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Inserir Base Horária
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>hourly_basis">Base Horária</a></li>
            <li class="active">Inserir Base Horária</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Inserir Base Horária Manualmente
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>hourly_basis/upload'">
                            <i class="fa fa-upload pull-right"></i>
                            Upload de CSV
                        </button>
                    </span>
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <span name="base_url" id="base_url" style="display: none"><?= base_url() ?></span>
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>hourly_basis/create">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Datas da semana</label>
                            <div class="col-md-10">
                                <div class="input-group col-lg-4 custom-date-range" data-date="" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control dpd1" name="from" id="from" alt="date" value="<?= set_value('from') ?>">
                                    <span class="input-group-addon">Até</span>
                                    <input type="text" class="form-control dpd2" name="to" id="to" alt="date" value="<?= set_value('to') ?>">
                                </div>
                                <span class="help-block">Insira as datas correspondentes ao início e fim da semana</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Carga</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-2">
                                        Pesado
                                    </div>
                                    <div class="col-lg-2">
                                        Médio
                                    </div>
                                    <div class="col-lg-2">
                                        Leve
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Sul</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <input type="text" id="south_heavy" name="south_heavy" class="form-control" value="<?= set_value('south_heavy') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="south_medium" name="south_medium" class="form-control" value="<?= set_value('south_medium') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="south_soft" name="south_soft" class="form-control" value="<?= set_value('south_soft') ?>" alt="valor">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Sudeste</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <input type="text" id="southeast_heavy" name="southeast_heavy" class="form-control" value="<?= set_value('southeast_heavy') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="southeast_medium" name="southeast_medium" class="form-control" value="<?= set_value('southeast_medium') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="southeast_soft" name="southeast_soft" class="form-control" value="<?= set_value('southeast_soft') ?>" alt="valor">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Norte</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <input type="text" id="north_heavy" name="north_heavy" class="form-control" value="<?= set_value('north_heavy') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="north_medium" name="north_medium" class="form-control" value="<?= set_value('north_medium') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="north_soft" name="north_soft" class="form-control" value="<?= set_value('north_soft') ?>" alt="valor">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Nordeste</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <input type="text" id="northeast_heavy" name="northeast_heavy" class="form-control" value="<?= set_value('northeast_heavy') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="northeast_medium" name="northeast_medium" class="form-control" value="<?= set_value('northeast_medium') ?>" alt="valor">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" id="northeast_soft" name="northeast_soft" class="form-control" value="<?= set_value('northeast_soft') ?>" alt="valor">
                                    </div>
                                </div>
                            </div>
                        </div>

                       <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
