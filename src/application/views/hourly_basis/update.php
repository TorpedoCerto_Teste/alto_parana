<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Unidades
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity">Unidades</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Unidade
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <span name="base_url" id="base_url" style="display: none"><?=base_url()?></span>
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>unity/update/<?=$this->Unity_model->_pk_unity?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <div class="form-group <?= form_error('fk_agent_power_distributor') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Agente</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_agent_power_distributor" name="fk_agent_power_distributor">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $agent) : ?>
                                            <option value="<?= $agent['pk_agent'] ?>" <?= set_value('fk_agent_power_distributor') == $agent['pk_agent']|| $this->Unity_model->_fk_agent_power_distributor == $agent['pk_agent']  ? "selected" : "" ?>><?= $agent['company_name']." - ".$agent['type'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('fk_profile') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Perfil</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_profile" name="fk_profile">
                                    <option></option>
                                    <?php if ($profiles) : ?>
                                        <?php foreach ($profiles as $profile) : ?>
                                            <option value="<?= $profile['pk_profile'] ?>" <?= set_value('fk_profile') == $profile['pk_profile'] || $this->Unity_model->_fk_profile == $profile['pk_profile'] ? "selected" : "" ?>><?= $profile['profile']." - ".$profile['type']." - ".$profile['submarket'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('cnpj') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="cnpj">CNPJ</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CNPJ" id="cnpj" name="cnpj" class="form-control" value="<?= set_value('cnpj') != "" ? set_value('cnpj') : str_pad($this->Unity_model->_cnpj, 14, 0, STR_PAD_LEFT) ?>" alt="cnpj">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('company_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="company_name">Empresa</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Nome da Empresa" id="company_name" name="company_name" class="form-control" value="<?= set_value('company_name') != "" ? set_value('company_name') : $this->Unity_model->_company_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('community_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="community_name">Nome fantasia</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Nome fantasia" id="community_name" name="community_name" class="form-control" value="<?= set_value('community_name') != "" ? set_value('community_name') : $this->Unity_model->_community_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('siga') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="siga">Código SIGA</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Código SIGA na CCEE" id="siga" name="siga" class="form-control" value="<?= set_value('siga') != "" ? set_value('siga') : $this->Unity_model->_siga ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('postal_code') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="postal_code">CEP</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CEP" id="postal_code" name="postal_code" class="form-control" value="<?= set_value('postal_code') != "" ? set_value('postal_code') : $this->Unity_model->_postal_code ?>" alt="cep">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('address') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="address">Endereço</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Endereço" id="address" name="address" class="form-control" value="<?= set_value('address') != "" ? set_value('address') : $this->Unity_model->_address ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('number') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="number">Número</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Número" id="number" name="number" class="form-control" value="<?= set_value('number') != "" ? set_value('number') : $this->Unity_model->_number ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('complement') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="complement">Complemento</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Complemento" id="complement" name="complement" class="form-control" value="<?= set_value('complement') != "" ? set_value('complement') : $this->Unity_model->_complement ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('district') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="district">Bairro</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Bairro" id="district" name="district" class="form-control" value="<?= set_value('district') != "" ? set_value('district') : $this->Unity_model->_district ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('city') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Cidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Cidade" id="city" name="city" class="form-control" value="<?= set_value('city') != "" ? set_value('city') : $this->Unity_model->_city ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                            <div class="col-lg-10">
                                <select id="state" name="state" class="form-control">
                                    <option
                                        value="AC" <?= set_value('state') == 'AC' || $this->Unity_model->_state == 'AC' ? 'selected' : '' ?>>
                                        Acre
                                    </option>
                                    <option
                                        value="AL" <?= set_value('state') == 'AL' || $this->Unity_model->_state == 'AL' ? 'selected' : '' ?>>
                                        Alagoas
                                    </option>
                                    <option
                                        value="AP" <?= set_value('state') == 'AP' || $this->Unity_model->_state == 'AP' ? 'selected' : '' ?>>
                                        Amapá
                                    </option>
                                    <option
                                        value="AM" <?= set_value('state') == 'AM' || $this->Unity_model->_state == 'AM' ? 'selected' : '' ?>>
                                        Amazonas
                                    </option>
                                    <option
                                        value="BA" <?= set_value('state') == 'BA' || $this->Unity_model->_state == 'BA' ? 'selected' : '' ?>>
                                        Bahia
                                    </option>
                                    <option
                                        value="CE" <?= set_value('state') == 'CE' || $this->Unity_model->_state == 'CE'  ? 'selected' : '' ?>>
                                        Ceará
                                    </option>
                                    <option
                                        value="DF" <?= set_value('state') == 'DF' || $this->Unity_model->_state == 'DF'  ? 'selected' : '' ?>>
                                        Distrito Federal
                                    </option>
                                    <option
                                        value="ES" <?= set_value('state') == 'ES' || $this->Unity_model->_state == 'ES'  ? 'selected' : '' ?>>
                                        Espirito Santo
                                    </option>
                                    <option
                                        value="GO" <?= set_value('state') == 'GO' || $this->Unity_model->_state == 'GO'  ? 'selected' : '' ?>>
                                        Goiás
                                    </option>
                                    <option
                                        value="MA" <?= set_value('state') == 'MA' || $this->Unity_model->_state == 'MA'  ? 'selected' : '' ?>>
                                        Maranhão
                                    </option>
                                    <option
                                        value="MS" <?= set_value('state') == 'MS' || $this->Unity_model->_state == 'MS'  ? 'selected' : '' ?>>
                                        Mato Grosso do Sul
                                    </option>
                                    <option
                                        value="MT" <?= set_value('state') == 'MT' || $this->Unity_model->_state == 'MT'  ? 'selected' : '' ?>>
                                        Mato Grosso
                                    </option>
                                    <option
                                        value="MG" <?= set_value('state') == 'MG' || $this->Unity_model->_state == 'MG'  ? 'selected' : '' ?>>
                                        Minas Gerais
                                    </option>
                                    <option
                                        value="PA" <?= set_value('state') == 'PA' || $this->Unity_model->_state == 'PA'  ? 'selected' : '' ?>>
                                        Pará
                                    </option>
                                    <option
                                        value="PB" <?= set_value('state') == 'PB' || $this->Unity_model->_state == 'PB'  ? 'selected' : '' ?>>
                                        Paraíba
                                    </option>
                                    <option
                                        value="PR" <?= set_value('state') == 'PR' || $this->Unity_model->_state == 'PR'  ? 'selected' : '' ?>>
                                        Paraná
                                    </option>
                                    <option
                                        value="PE" <?= set_value('state') == 'PE' || $this->Unity_model->_state == 'PE'  ? 'selected' : '' ?>>
                                        Pernambuco
                                    </option>
                                    <option
                                        value="PI" <?= set_value('state') == 'PI' || $this->Unity_model->_state == 'PI'  ? 'selected' : '' ?>>
                                        Piauí
                                    </option>
                                    <option
                                        value="RJ" <?= set_value('state') == 'RJ' || $this->Unity_model->_state == 'RJ'  ? 'selected' : '' ?>>
                                        Rio de Janeiro
                                    </option>
                                    <option
                                        value="RN" <?= set_value('state') == 'RN' || $this->Unity_model->_state == 'RN'  ? 'selected' : '' ?>>
                                        Rio Grande do Norte
                                    </option>
                                    <option
                                        value="RS" <?= set_value('state') == 'RS' || $this->Unity_model->_state == 'RS'  ? 'selected' : '' ?>>
                                        Rio Grande do Sul
                                    </option>
                                    <option
                                        value="RO" <?= set_value('state') == 'RO' || $this->Unity_model->_state == 'RO'  ? 'selected' : '' ?>>
                                        Rondônia
                                    </option>
                                    <option
                                        value="RR" <?= set_value('state') == 'RR' || $this->Unity_model->_state == 'RR'  ? 'selected' : '' ?>>
                                        Roraima
                                    </option>
                                    <option
                                        value="SC" <?= set_value('state') == 'SC' || $this->Unity_model->_state == 'SC'  ? 'selected' : '' ?>>
                                        Santa Catarina
                                    </option>
                                    <option
                                        value="SP" <?= set_value('state') == 'SP' || $this->Unity_model->_state == 'SP'  ? 'selected' : '' ?>>
                                        São Paulo
                                    </option>
                                    <option
                                        value="SE" <?= set_value('state') == 'SE' || $this->Unity_model->_state == 'SE'  ? 'selected' : '' ?>>
                                        Sergipe
                                    </option>
                                    <option
                                        value="TO" <?= set_value('state') == 'TO' || $this->Unity_model->_state == 'TO'  ? 'selected' : '' ?>>
                                        Tocantins
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
