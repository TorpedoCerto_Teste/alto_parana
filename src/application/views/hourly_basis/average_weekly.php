<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Base Horária por Semana (Gráficos)
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Base Horária por Semana</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Bases Horárias por Semana
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: none">
                    <form role="form" class="form-inline" method="POST" action="<?= base_url() ?>hourly_basis/average_weekly">
                        <input type="hidden" name="filter" id="filter" value="true" />

                        <div class="form-group <?= form_error('year_start') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year_start">Ano Inicial</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="year_start" name="year_start" class="form-control" value="<?= set_value('year_start') != "" ? set_value('year_start') : date("Y") ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('month_start') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month_start">Mês</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="month_start" name="month_start">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>Janeiro</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>Fevereiro</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>Março</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>Abril</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>Maio</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>Junho</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>Julho</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>Agosto</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>Setembro</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>Outubro</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>Novembro</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>Dezembro</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('week_start') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-4 col-sm-2 control-label" for="week_start">Semana</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="week_start" name="week_start">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group <?= form_error('year_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year_end">Ano Final</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="year_end" name="year_end" class="form-control" value="<?= set_value('year_end') != "" ? set_value('year_end') : date("Y") ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('month_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month_end">Mês</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="month_end" name="month_end">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>Janeiro</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>Fevereiro</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>Março</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>Abril</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>Maio</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>Junho</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>Julho</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>Agosto</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>Setembro</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>Outubro</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>Novembro</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>Dezembro</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('week_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-4 col-sm-2 control-label" for="week_end">Semana</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="week_end" name="week_end">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Filtrar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <span id="graph"><?=json_encode($graph)?></span>
                    <div class="panel-body">
                        <div id="multi-sates">
                            <div id="multi-states-container" class="f-c-space">
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
            </section>
        </div>

    </div>
</div>

