<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Importação de Arquivos de Preços
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>hourly_basis">Base Horária</a></li>
            <li><a href="<?= base_url() ?>hourly_basis/create">Inserir Base Horária</a></li>
            <li class="active">Importação de Arquivos</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Importação de Arquivos de Preços
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.open('http://www.ccee.org.br/portal/faces/pages_publico/o-que-fazemos/como_ccee_atua/precos/precos_csv', '_blank')"   >
                            <i class="fa fa-link pull-right"></i>
                            Acessar o site da CCEE
                        </button>
                    </span>
                </header>
                <div class="alert alert-info fade in">
                    <strong>Atenção!</strong> Os arquivos CSV transferidos são processados a cada hora. <br/>
                    O formato do arquivo CSV deve estar com as colunas dispostas da seguinte forma: (inclusive deve-se manter este cabeçalho) <br/>
                    Ano;Mês;Semana;Data Início;Data Fim;Pesado SE;Médio SE;Leve SE;Pesado S;Médio S;Leve S;Pesado NE;Médio NE;Leve NE;Pesado N;Médio N;Leve N
                </div>
                <div class="panel-body">
                    <span name="base_url" id="base_url" style="display:none"><?= base_url() ?>hourly_basis/dropzone</span>
                    <form id="my-awesome-dropzone" action="<?= base_url() ?>hourly_basis/dropzone" class="dropzone"></form>
                </div>
            </section>
        </div>

    </div>
</div>
