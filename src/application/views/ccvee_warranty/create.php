<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Garantia
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ccvee_warranty/index">Garantia</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Nova Garantia
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_warranty/create">
                        <input type="hidden" name="create" id="create" value="true" />


                        <div class="form-group <?= form_error('fk_modality_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_modality_warranty">Modalidade de Garantia</label>
                            <div class="col-lg-4">
                                <select class="form-control select2" id="fk_modality_warranty" name="fk_modality_warranty">
                                    <option></option>
                                    <?php if ($modality_warranties) : ?>
                                        <?php foreach ($modality_warranties as $modality_warranty) : ?>
                                            <option value="<?= $modality_warranty['pk_modality_warranty'] ?>"><?= $modality_warranty['modality'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <!--<input type="text" placeholder=""  class="form-control" value="<?= set_value('fk_modality_warranty') ?>">-->
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') ?>" alt="year">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('deadline_status') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="deadline_status">Status prazo</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="deadline_status" name="deadline_status">
                                    <option value="pendente">PENDENTE</option>
                                    <option value="apresentada">APRESENTADA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('customer_communication_status') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="customer_communication_status">Status comunicação ao cliente</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="customer_communication_status" name="customer_communication_status">
                                    <option value="comunicada">COMUNICADA</option>
                                    <option value="nao_comunicada">NÃO COMUNICADA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('start_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="start_date">Vigência</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('deadline_contract_presentation') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="deadline_contract_presentation">Prazo de apresentação do contrato</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="deadline_contract_presentation" name="deadline_contract_presentation" class="form-control" value="<?= set_value('deadline_contract_presentation') ?>" alt="date">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('presentation_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="presentation_date">Data de apresentação</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="presentation_date" name="presentation_date" class="form-control" value="<?= set_value('presentation_date') ?>" alt="date">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 


                    <?php if (!$list_warranties) : ?>
                        <div class="alert alert-warning fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                        </div>
                    <?php else : ?>
                        <table class="table responsive-data-table data-table">
                            <thead>
                                <tr>
                                    <th>
                                        Modalidade
                                    </th>
                                    <th>
                                        Ano
                                    </th>
                                    <th>
                                        Status prazo
                                    </th>
                                    <th>
                                        Status comunicação ao cliente
                                    </th>
                                    <th>
                                        Data inicial
                                    </th>
                                    <th>
                                        Data final
                                    </th>
                                    <th>
                                        Apresentação do contrato
                                    </th>
                                    <th>
                                        Data de apresentação
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_warranties as $item) : ?>
                                    <tr>
                                        <td><?= $item['modality'] ?></td>
                                        <td><?= $item['year'] ?></td>
                                        <td><?= mb_strtoupper($item['deadline_status'], "UTF-8") ?></td>
                                        <td><?= mb_strtoupper($item['customer_communication_status'], "UTF-8") ?></td>
                                        <td><?= date_to_human_date($item['start_date']) ?></td>
                                        <td><?= date_to_human_date($item['end_date']) ?></td>
                                        <td><?= date_to_human_date($item['deadline_contract_presentation']) ?></td>
                                        <td><?= date_to_human_date($item['presentation_date']) ?></td>
                                        <td class="hidden-xs">
                                            <a class="btn btn-danger btn-xs" href='#delete_warranty' onclick="$('#pk_ccvee_warranty').val(<?= $item['pk_ccvee_warranty'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>


                </div>
            </section>


        </div>


    </div>

</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_warranty" class="modal fade">
    <form method="POST" action="<?= base_url() ?>ccvee_warranty/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_ccvee_warranty" id="pk_ccvee_warranty" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
