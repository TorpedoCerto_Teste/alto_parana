<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Garantia 
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ccvee_warranty/index">Garantia</a></li>
            <li class="active">Editar Garantia</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Garantia
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_warranty/update/<?= $this->uri->segment(3, 0) ?>">
                        <input type="hidden" name="update" id="update" value="true" />

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : $this->Ccvee_warranty_model->_year ?>">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="deadline_status">Status prazo</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="deadline_status" name="deadline_status" class="form-control" value="<?= set_value('deadline_status') != "" ? set_value('deadline_status') : $this->Ccvee_warranty_model->_deadline_status ?>">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="customer_communication_status">Status comunicação ao cliente</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="customer_communication_status" name="customer_communication_status" class="form-control" value="<?= set_value('customer_communication_status') != "" ? set_value('customer_communication_status') : $this->Ccvee_warranty_model->_customer_communication_status ?>">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="start_date">Data inicial</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="start_date" name="start_date" class="form-control" value="<?= set_value('start_date') != "" ? set_value('start_date') : $this->Ccvee_warranty_model->_start_date ?>">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="end_date">Data final</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="end_date" name="end_date" class="form-control" value="<?= set_value('end_date') != "" ? set_value('end_date') : $this->Ccvee_warranty_model->_end_date ?>">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="deadline_contract_presentation">Prazo de apresentação do contrato</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="deadline_contract_presentation" name="deadline_contract_presentation" class="form-control" value="<?= set_value('deadline_contract_presentation') != "" ? set_value('deadline_contract_presentation') : $this->Ccvee_warranty_model->_deadline_contract_presentation ?>">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="presentation_date">Data de apresentação</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="presentation_date" name="presentation_date" class="form-control" value="<?= set_value('presentation_date') != "" ? set_value('presentation_date') : $this->Ccvee_warranty_model->_presentation_date ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
