<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Ponto de Medição
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/pontos_medicao">Pontos de Medição</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Ponto de Medição
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>gauge/create/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('gauge') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="gauge">Ponto de Medição</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="gauge" name="gauge" class="form-control" value="<?= set_value('gauge') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('internal_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="internal_name">Nome Interno</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="internal_name" name="internal_name" class="form-control" value="<?= set_value('internal_name') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('telemetry_code') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="telemetry_code">ID Telemetria</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="telemetry_code" name="telemetry_code" class="form-control" value="<?= set_value('telemetry_code') ?>" alt="cpfcnpj">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-lg-2 col-sm-2 control-label" for="summer_time">Horário de Verão</label>
                            <div class="col-lg-1">
                                <label class="checkbox-custom check-success">
                                    <input value="1" name="summer_time" id="summer_time" type="checkbox"><label for="summer_time">&nbsp;</label>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/pontos_medicao'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
