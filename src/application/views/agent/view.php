<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contas / Contatos / Grupos / Perfis / Arquivos / Unidades
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent">Contas</a></li>
            <li class="active">Visualizar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">

            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-dark tab-right ">
                    <ul class="nav nav-tabs pull-right">
                        <li class="<?= $this->uri->segment(4, "") == "" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#agente">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(4, "") == "contatos" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#contatos">
                                <i class="fa fa-users"></i>
                                Contatos
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(4, "") == "grupos" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#grupos">
                                <i class="fa fa-share-alt"></i>
                                Grupos
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(4, "") == "perfis" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#perfis">
                                <i class="fa fa-tags"></i>
                                Perfis
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(4, "") == "arquivos" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#arquivos">
                                <i class="fa fa-file"></i>
                                Arquivos
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(4, "") == "unidades" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#unidades">
                                <i class="fa fa-cubes"></i>
                                Unidades
                            </a>
                        </li>
                    </ul>
                    <span class="hidden-sm wht-color"><?= $this->Agent_model->_community_name ?> 
                        <?php if ($this->Agent_model->_status == $this->Agent_model->_status_active) : ?>
                            <span class="label label-success">Ativo</span></span>
                    <?php else : ?>
                        <span class="label label-warning">Inativo</span></span>
                    <?php endif; ?>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                        <!--AGENTE-->

                        <header class="panel-heading ">
                            &nbsp;
                            <?php if ($this->Agent_model->_status == $this->Agent_model->_status_active) : ?>
                                <span class=" pull-right">
                                    <a data-toggle="modal" href="#status_agent">
                                        <button class="btn btn-sm btn-warning addon-btn m-b-10">
                                            <i class="fa fa-warning pull-right"></i>
                                            Inativar
                                        </button>
                                    </a>                            
                                <?php else : ?>
                                    <span class=" pull-right">
                                        <a data-toggle="modal" href="#status_agent">
                                            <button class="btn btn-sm btn-success addon-btn m-b-10">
                                                <i class="fa fa-warning pull-right"></i>
                                                Ativar
                                            </button>
                                        </a>                            
                                    <?php endif; ?>
                                    <a data-toggle="modal" href="#delete_agent">
                                        <button class="btn btn-sm btn-danger addon-btn m-b-10">
                                            <i class="fa fa-trash pull-right"></i>
                                            Deletar
                                        </button>
                                    </a>                            
                                </span>
                        </header>

                        <div id="agente" class="tab-pane <?= $this->uri->segment(4, "") == "" ? 'active' : '' ?>">
                            <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>agent/update/<?= $this->Agent_model->_pk_agent ?>">
                                <input type="hidden" name="update" id="update" value="true" />
                                <div class="form-group <?= form_error('community_name') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="community_name">Nome Fantasia/Unidade</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Nome Fantasia ou Nome da Unidade" id="community_name" name="community_name" class="form-control" value="<?= set_value('community_name') != "" ? set_value('community_name') : $this->Agent_model->_community_name ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('company_name') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="company_name">Razão Social</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Razão Social da Empresa" id="company_name" name="company_name" class="form-control" value="<?= set_value('company_name') != "" ? set_value('company_name') : $this->Agent_model->_company_name ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('cnpj') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="cnpj">CNPJ</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="CNPJ" id="cnpj" name="cnpj" class="form-control" value="<?= set_value('cnpj') != "" ? set_value('cnpj') : str_pad($this->Agent_model->_cnpj, 14, 0, STR_PAD_LEFT) ?>" alt="cnpj">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('ie') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="ie">Inscrição Estadual</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Inscrição Estadual" id="ie" name="ie" class="form-control" value="<?= set_value('ie') != "" ? set_value('ie') : $this->Agent_model->_ie ?>" >
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('fk_agent_type') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label">Tipo</label>
                                    <div class="col-lg-10">
                                        <select class="form-control select2-allow-clear" id="fk_agent_type" name="fk_agent_type">
                                            <option></option>
                                            <?php foreach ($agent_types as $agent_type) : ?>
                                                <option value="<?= $agent_type['pk_agent_type'] ?>" <?= set_value('fk_agent_type') == $agent_type['pk_agent_type'] || $this->Agent_model->_fk_agent_type == $agent_type['pk_agent_type'] ? "selected" : "" ?>><?= $agent_type['type'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('postal_code') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="postal_code">CEP</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="CEP" id="postal_code" name="postal_code" class="form-control" value="<?= set_value('postal_code') != "" ? set_value('postal_code') : $this->Agent_model->_postal_code ?>" alt="cep">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('address') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="address">Endereço</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Endereço" id="address" name="address" class="form-control" value="<?= set_value('address') != "" ? set_value('address') : $this->Agent_model->_address ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('number') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="number">Número</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Número" id="number" name="number" class="form-control" value="<?= set_value('number') != "" ? set_value('number') : $this->Agent_model->_number ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('complement') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="complement">Complemento</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Complemento" id="complement" name="complement" class="form-control" value="<?= set_value('complement') != "" ? set_value('complement') : $this->Agent_model->_complement ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('district') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="district">Bairro</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Bairro" id="district" name="district" class="form-control" value="<?= set_value('district') != "" ? set_value('district') : $this->Agent_model->_district ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('city') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="city">Cidade</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Cidade" id="city" name="city" class="form-control" value="<?= set_value('city') != "" ? set_value('city') : $this->Agent_model->_city ?>">
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                                    <div class="col-lg-10">
                                        <select id="state" name="state" class="form-control">
                                            <?php foreach (retorna_estados() as $sigla => $estado) : ?>
                                                <option value="<?= $sigla ?>" <?= set_value('state') == $sigla || $this->Agent_model->_state == $sigla ? 'selected' : '' ?>><?= $estado ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label">Proprietário da conta</label>
                                    <div class="col-lg-10">
                                        <select class="form-control select2-allow-clear" id="fk_user" name="fk_user" >
                                            <option></option>
                                            <?php foreach ($users as $user) : ?>
                                                <option value="<?= $user['pk_user'] ?>" <?= set_value('fk_user') == $user['pk_user'] || $this->Agent_model->_fk_user == $user['pk_user'] ? "selected" : "" ?>><?= $user['name'] . ' ' . $user['surname'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group <?= form_error('fk_sector') != "" ? "has-error" : ""; ?>">
                                    <label class="col-lg-2 col-sm-2 control-label">Setor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control select2-allow-clear" id="fk_sector" name="fk_sector" >
                                            <option></option>
                                            <?php foreach ($sectors as $sector) : ?>
                                                <option value="<?= $sector['pk_sector'] ?>" <?= set_value('fk_sector') == $sector['pk_sector'] || $this->Agent_model->_fk_sector == $sector['pk_sector'] ? "selected" : "" ?>><?= $sector['sector'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Observações</label>
                                    <div class="col-lg-10">
                                        <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="18" ><?= set_value('fk_sector') != "" ? set_value('fk_sector') : $this->Agent_model->_memo ?></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-success" type="submit">Salvar</button>
                                    </div>
                                </div>

                            </form> 
                        </div>

                        <!--CONTATOS-->
                        <div id="contatos" class="tab-pane <?= $this->uri->segment(4, "") == "contatos" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Contatos
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>contact/create/<?= $this->Agent_model->_pk_agent ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo
                                                </button>
                                                <button class="btn btn-info addon-btn m-b-10" href='#attach_contact' data-toggle="modal">
                                                    <i class="fa fa-chain pull-right"></i>
                                                    Vincular contatos
                                                </button>
                                            </span>
                                        </header>
                                        <?php if (!$contacts) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Contato</th>
                                                        <th>Telefone</th>
                                                        <th>Ramal</th>
                                                        <th>Celular</th>
                                                        <th>Email</th>
                                                        <th>Cargo</th>
                                                        <th>Status</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($contacts as $item) : ?>
                                                        <tr>
                                                            <td><?= $item['name'] ?> <?= $item['surname'] ?></td>
                                                            <td><?= $item['phone'] ?></td>
                                                            <td><?= $item['extension'] ?></td>
                                                            <td><?= $item['mobile'] ?></td>
                                                            <td><?= $item['email'] ?></td>
                                                            <td><?= $item['responsibility'] ?></td>
                                                            <td>
                                                                <?php if ($item['status'] == $this->Agent_model->_status_active) : ?>
                                                                    <span class="label label-success">Ativo</span></span>
                                                                <?php else : ?>
                                                                    <span class="label label-warning">Inativo</span></span>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-info btn-xs tooltips" href = '<?= base_url() ?>contact/update/<?= $this->uri->segment(3) ?>/<?= $item['fk_contact'] ?>' data-placement="top" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                                                <a class="btn btn-warning btn-xs tooltips" href = '<?= base_url() ?>contact/status/<?= $this->uri->segment(3) ?>/<?= $item['fk_contact'] ?>' data-placement="top" data-toggle="tooltip" data-original-title="<?= $item['status'] == $this->Agent_contact_model->_status_active ? 'Inativar' : 'Ativar' ?>"><i class="fa fa-warning"></i></a>
                                                                <a class="btn btn-danger btn-xs tooltips" href='#delete_contato' onclick="$('#pk_contact').val(<?= $item['fk_contact'] ?>)" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o "></i></a>
                                                                <a class="btn btn-info btn-xs tooltips" href='#deattatch_contato' onclick="$('#fk_contact_deattach').val(<?= $item['fk_contact'] ?>); " data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Desvincular deste agente"><i class="fa fa-chain-broken"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>                            
                        </div>

                        <!--GRUPOS-->
                        <div id="grupos" class="tab-pane <?= $this->uri->segment(4, "") == "grupos" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Grupos
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>group/create/<?= $this->Agent_model->_pk_agent ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo Grupo
                                                </button>
                                            </span>
                                        </header>
                                        <div class="panel-body">
                                            <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>group_agent_contact/upsert/<?= $this->Agent_model->_pk_agent ?>">
                                                <input type="hidden" name="upsert" id="upsert" value="true"/>
                                                <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled" />

                                                <div class="form-group <?= form_error('fk_contact') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="fk_contact">Contato</label>
                                                    <div class="col-lg-4">
                                                        <select class="form-control select2-allow-clear" id="fk_contact" name="fk_contact">
                                                            <option></option>
                                                            <?php if ($contacts) : ?>
                                                                <?php foreach ($contacts as $agent => $contact) : ?>
                                                                    <option value="<?= $contact['pk_contact'] ?>" <?= set_value('fk_contact') == $contact['pk_contact'] ? "selected" : "" ?>><?= $contact['name'] . " " . $contact['surname'] ?></option>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group <?= form_error('groups') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="groups">Grupos</label>
                                                    <div class="col-lg-8">
                                                        <?php if ($groups) : ?>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <label class="checkbox-custom check-success">
                                                                                <input type="checkbox" class="all" name="all" id="all" value=""/><label for="all">&nbsp;</label>
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <b>TODOS</b>
                                                                        </td>

                                                                    </tr>
                                                                    <?php foreach ($groups as $group) : ?>
                                                                        <tr>
                                                                            <td>
                                                                                <label class="checkbox-custom check-success">
                                                                                    <input type="checkbox" class="chk_group" name="groups[]" id="group_<?= $group['pk_group'] ?>" value="<?= $group['pk_group'] ?>"/><label for="group_<?= $group['pk_group'] ?>">&nbsp;</label>
                                                                                </label>
                                                                            </td>
                                                                            <td><?= $group['group'] ?> (<?= $group['group_short_name'] ?>)</td>
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        <?php else: ?>
                                                            <div class="alert alert-danger fade in">
                                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                                <strong>Atenção!</strong> Nenhum grupo cadastrado. 
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button class="btn btn-success" type="submit">Salvar</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </section>
                                </div>

                            </div>
                        </div>

                        <!--PERFIS-->
                        <div id="perfis" class="tab-pane <?= $this->uri->segment(4, "") == "perfis" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Perfis
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>profile/create/<?= $this->Agent_model->_pk_agent ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo
                                                </button>
                                            </span>
                                        </header>
                                        <?php if (!$profiles) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Perfil</th>
                                                        <th>Código</th>
                                                        <th>Submercado</th>
                                                        <th>Classe</th>
                                                        <th>Categoria</th>
                                                        <th>Status</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($profiles as $item) : ?>
                                                        <tr>
                                                            <td><?= $item['profile'] ?></td>
                                                            <td><?= $item['code'] ?></td>
                                                            <td><?= $item['submarket'] ?></td>
                                                            <td><?= $item['standard'] ?></td>
                                                            <td><?= $item['category'] ?></td>
                                                            <td>
                                                                <?php if ($item['status'] == $this->Profile_model->_status_active) : ?>
                                                                    <span class="label label-success">Ativo</span></span>
                                                                <?php else : ?>
                                                                    <span class="label label-warning">Inativo</span></span>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-info btn-xs tooltips" href = '<?= base_url() ?>profile/update/<?= $this->Agent_model->_pk_agent ?>/<?= $item['pk_profile'] ?>' data-placement="top" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                                                <a class="btn btn-warning btn-xs tooltips" href = '<?= base_url() ?>profile/status/<?= $this->Agent_model->_pk_agent ?>/<?= $item['pk_profile'] ?>' data-placement="top" data-toggle="tooltip" data-original-title="<?= $item['status'] == $this->Profile_model->_status_active ? 'Inativar' : 'Ativar' ?>"><i class="fa fa-warning"></i></a>
                                                                <a class="btn btn-danger btn-xs tooltips" href='#delete_perfil' onclick="$('#pk_profile').val(<?= $item['pk_profile'] ?>)" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>

                        </div>

                        <!--UNIDADES-->
                        <div id="unidades" class="tab-pane <?= $this->uri->segment(4, "") == "unidades" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Unidades
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>unity/create/<?= $this->Agent_model->_pk_agent ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo
                                                </button>
                                            </span>
                                        </header>
                                        <?php if (!$units) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Unidade</th>
                                                        <th>CNPJ</th>
                                                        <th>Cidade</th>
                                                        <th>Tipo Agente</th>
                                                        <th>Status</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($units as $item) : ?>
                                                        <tr>
                                                            <td><a href="<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $item['pk_unity'] ?>"><?= $item['community_name'] ?></td>
                                                            <td><?= $item['cnpj'] ?></td>
                                                            <td><?= $item['city'] ?></td>
                                                            <td><?= $item['agent_type'] ?></td>
                                                            <td>
                                                                <?php if ($item['status'] == $this->Agent_model->_status_active) : ?>
                                                                    <span class="label label-success">Ativo</span></span>
                                                                <?php else : ?>
                                                                    <span class="label label-warning">Inativo</span></span>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <a data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips" data-original-title="Editar" href = '<?= base_url() ?>unity/update/<?= $this->Agent_model->_pk_agent ?>/<?= $item['pk_unity'] ?>'><i class="fa fa-pencil"></i></a>
                                                                <a data-placement="top" data-toggle="modal" class="btn btn-danger btn-xs tooltips" data-original-title="Excluir" href='#delete_unity' onclick="$('#fk_unity').val('<?= $item['pk_unity'] ?>')"><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>                        
                        </div>

                        <!--ARQUIVOS-->
                        <div id="arquivos" class="tab-pane <?= $this->uri->segment(4, "") == "arquivos" ? 'active' : '' ?>">
                            <div class="panel-body">
                                <form id="my-awesome-dropzone" action="<?= base_url() ?>agent/dropzone/<?= $this->Agent_model->_pk_agent ?>" class="dropzone"></form>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <?php if (!$upload) : ?>
                                                <div class="alert alert-warning fade in">
                                                    <button type="button" class="close close-sm" data-dismiss="alert">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    <strong>Atenção!</strong> Nenhum arquivo! Para inserir, arraste e solte na área pontilhada.
                                                </div>
                                            <?php else : ?>
                                                <table class="table responsive-data-table data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Arquivo</th>
                                                            <th>Opções</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($upload as $item) : ?>
                                                            <?php if (!is_array($item)) : ?>
                                                                <tr>
                                                                    <td><a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/agent/<?= $this->Agent_model->_pk_agent ?>/<?= $item ?>" target="_blank"><?= $item ?></td>
                                                                    <td class="hidden-xs">
                                                                        <a class="btn btn-danger btn-xs tooltips" href='#delete_file' onclick="$('#file_to_delete').val('<?= $item ?>')" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o "></i></a>
                                                                    </td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php endif; ?>

                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--tab nav start-->

        </div>

    </div>
</div>


<!-- Modal delete perfil -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_contato" class="modal fade">
    <form method="POST" action="<?= base_url() ?>contact/delete/<?= $this->Agent_model->_pk_agent ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_contact" id="pk_contact" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal delete file -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_file" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent/delete_file">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="file_to_delete" id="file_to_delete" value="0" />
        <input type="hidden" name="pk_agent" id="pk_agent" value="<?= $this->Agent_model->_pk_agent ?>" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este arquivo?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal  perfil -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_perfil" class="modal fade">
    <form method="POST" action="<?= base_url() ?>profile/delete/<?= $this->Agent_model->_pk_agent ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_profile" id="pk_profile" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_group_agent_contact" class="modal fade">
    <form method="POST" action="<?= base_url() ?>group_agent_contact/delete/<?= $this->Agent_model->_pk_agent ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_group_agent_contact" id="pk_group_agent_contact" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->


<!-- Modal Deletar -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_agent" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_agent" id="pk_agent" value="<?= $this->Agent_model->_pk_agent ?>" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja realmente apagar este agente?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal Status -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="status_agent" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent/status/<?= $this->Agent_model->_pk_agent ?>">
        <input type="hidden" name="status" id="status" value="true" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-warning">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja <?= $this->Agent_model->_status == $this->Agent_model->_status_active ? "INATIVAR" : "ATIVAR" ?> este agente?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-warning" type="submit">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal Vincular Contato -->
<div aria-hidden="true" aria-labelledby="attach_contact" role="dialog" tabindex="-1" id="attach_contact" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent_contact/create_multi">
        <input type="hidden" name="fk_agent" id="fk_agent" value="<?= $this->Agent_model->_pk_agent ?>" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Vincular contatos externos ao agente</h4>
                </div>
                <div class="modal-content">
                    <?php if ($external_contacts) : ?>
                        <table class="table table-responsive table-hover" id="email_list">
                            <thead>
                                <tr>
                                    <th>TODOS</th>
                                    <th></th>
                                    <th>
                                        <label class="checkbox-custom check-success">
                                            <input type="checkbox" class="all" name="all_contacts" id="all_contacts" value=""/>
                                            <label for="all_contacts">&nbsp;</label>
                                        </label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($external_contacts as $contact) : ?>
                                    <tr>
                                        <td><?= $contact['name'] ?> <?= $contact['surname'] ?></td>
                                        <td><?= $contact['email']?></td>
                                        <td>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="chk_contact" name="contacts[]" id="contact_<?= $contact['pk_contact'] ?>" value="<?= $contact['pk_contact'] ?>" />
                                                <label for="contact_<?= $contact['pk_contact'] ?>">&nbsp;</label>
                                            </label>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>                        
                    <?php else : ?>
                        <div class="alert alert-warning fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Nenhum contato encontrado.
                        </div>
                    <?php endif; ?>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-info" type="submit">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!--MODAL DE CONFIRMACAO PARA DESVINCULAR CONTATO-->
<div aria-hidden="true" aria-labelledby="deattatch_contato" role="dialog" tabindex="-1" id="deattatch_contato" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent_contact/deattach">
        <input type="hidden" name="deattach" id="delete" value="true" />
        <input type="hidden" name="fk_agent_deattach" id="fk_agent_deattach" value="<?= $this->Agent_model->_pk_agent ?>" />
        <input type="hidden" name="fk_contact_deattach" id="fk_contact_deattach" value="" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja realmente desvincular este contado do agente?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Desvincular</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal Deletar unidade-->
<div aria-hidden="true" aria-labelledby="modalDeleteUnity" role="dialog" tabindex="-1" id="delete_unity" class="modal fade">
    <form method="POST" action="<?= base_url() ?>unity/delete_by_agent/<?= $this->Agent_model->_pk_agent ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="fk_unity" id="fk_unity" value="" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja realmente apagar esta unidade?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->