<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Agentes
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent">Agentes</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Agente 
                    <?php if ($this->Agent_model->_status == $this->Agent_model->_status_active) : ?>
                        <span class="label label-success">Ativo</span>
                        <span class=" pull-right">
                            <a data-toggle="modal" href="#status">
                                <button class="btn btn-sm btn-warning addon-btn m-b-10">
                                    <i class="fa fa-warning pull-right"></i>
                                    Inativar
                                </button>
                            </a>                            
                    <?php else : ?>
                        <span class="label label-warning">Inativo</span>
                        <span class=" pull-right">
                            <a data-toggle="modal" href="#status">
                                <button class="btn btn-sm btn-success addon-btn m-b-10">
                                    <i class="fa fa-warning pull-right"></i>
                                    Ativar
                                </button>
                            </a>                            
                    <?php endif; ?>
                        <a data-toggle="modal" href="#delete">
                            <button class="btn btn-sm btn-danger addon-btn m-b-10">
                                <i class="fa fa-trash pull-right"></i>
                                Deletar
                            </button>
                        </a>                            
                    </span>
                    
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>agent/update/<?=$this->Agent_model->_pk_agent?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <div class="form-group <?= form_error('community_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="community_name">Nome Fantasia/Unidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Nome Fantasia ou Nome da Unidade" id="community_name" name="community_name" class="form-control" value="<?= set_value('community_name') != "" ? set_value('community_name') : $this->Agent_model->_community_name ?>">
                            </div>
                        </div>
                         <div class="form-group <?= form_error('company_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="company_name">Razão Social</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Razão Social da Empresa" id="company_name" name="company_name" class="form-control" value="<?= set_value('company_name') != "" ? set_value('company_name') : $this->Agent_model->_company_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('cnpj') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="cnpj">CNPJ</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CNPJ" id="cnpj" name="cnpj" class="form-control" value="<?= set_value('cnpj') != "" ? set_value('cnpj') : str_pad($this->Agent_model->_cnpj, 14, 0, STR_PAD_LEFT) ?>" alt="cnpj">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ie') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="ie">Inscrição Estadual</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Inscrição Estadual" id="ie" name="ie" class="form-control" value="<?= set_value('ie') != "" ? set_value('ie') : $this->Agent_model->_ie ?>" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_agent_type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Tipo</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_agent_type" name="fk_agent_type">
                                    <option></option>
                                    <?php foreach ($agent_types as $agent_type) : ?>
                                        <option value="<?= $agent_type['pk_agent_type'] ?>" <?= set_value('fk_agent_type') == $agent_type['pk_agent_type'] || $this->Agent_model->_fk_agent_type == $agent_type['pk_agent_type'] ? "selected" : "" ?>><?= $agent_type['type'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('postal_code') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="postal_code">CEP</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CEP" id="postal_code" name="postal_code" class="form-control" value="<?= set_value('postal_code') != "" ? set_value('postal_code') : $this->Agent_model->_postal_code ?>" alt="cep">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('address') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="address">Endereço</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Endereço" id="address" name="address" class="form-control" value="<?= set_value('address') != "" ? set_value('address') : $this->Agent_model->_address ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('number') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="number">Número</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Número" id="number" name="number" class="form-control" value="<?= set_value('number') != "" ? set_value('number') : $this->Agent_model->_number ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('complement') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="complement">Complemento</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Complemento" id="complement" name="complement" class="form-control" value="<?= set_value('complement') != "" ? set_value('complement') : $this->Agent_model->_complement ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('district') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="district">Bairro</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Bairro" id="district" name="district" class="form-control" value="<?= set_value('district') != "" ? set_value('district') : $this->Agent_model->_district ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('city') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Cidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Cidade" id="city" name="city" class="form-control" value="<?= set_value('city') != "" ? set_value('city') : $this->Agent_model->_city ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                            <div class="col-lg-10">
                                <select id="state" name="state" class="form-control">
                                    <option
                                        value="AC" <?= set_value('state') == 'AC' || $this->Agent_model->_state == 'AC' ? 'selected' : '' ?>>
                                        Acre
                                    </option>
                                    <option
                                        value="AL" <?= set_value('state') == 'AL' || $this->Agent_model->_state == 'AL' ? 'selected' : '' ?>>
                                        Alagoas
                                    </option>
                                    <option
                                        value="AP" <?= set_value('state') == 'AP' || $this->Agent_model->_state == 'AP' ? 'selected' : '' ?>>
                                        Amapá
                                    </option>
                                    <option
                                        value="AM" <?= set_value('state') == 'AM' || $this->Agent_model->_state == 'AM' ? 'selected' : '' ?>>
                                        Amazonas
                                    </option>
                                    <option
                                        value="BA" <?= set_value('state') == 'BA' || $this->Agent_model->_state == 'BA' ? 'selected' : '' ?>>
                                        Bahia
                                    </option>
                                    <option
                                        value="CE" <?= set_value('state') == 'CE' || $this->Agent_model->_state == 'CE'  ? 'selected' : '' ?>>
                                        Ceará
                                    </option>
                                    <option
                                        value="DF" <?= set_value('state') == 'DF' || $this->Agent_model->_state == 'DF'  ? 'selected' : '' ?>>
                                        Distrito Federal
                                    </option>
                                    <option
                                        value="ES" <?= set_value('state') == 'ES' || $this->Agent_model->_state == 'ES'  ? 'selected' : '' ?>>
                                        Espirito Santo
                                    </option>
                                    <option
                                        value="GO" <?= set_value('state') == 'GO' || $this->Agent_model->_state == 'GO'  ? 'selected' : '' ?>>
                                        Goiás
                                    </option>
                                    <option
                                        value="MA" <?= set_value('state') == 'MA' || $this->Agent_model->_state == 'MA'  ? 'selected' : '' ?>>
                                        Maranhão
                                    </option>
                                    <option
                                        value="MS" <?= set_value('state') == 'MS' || $this->Agent_model->_state == 'MS'  ? 'selected' : '' ?>>
                                        Mato Grosso do Sul
                                    </option>
                                    <option
                                        value="MT" <?= set_value('state') == 'MT' || $this->Agent_model->_state == 'MT'  ? 'selected' : '' ?>>
                                        Mato Grosso
                                    </option>
                                    <option
                                        value="MG" <?= set_value('state') == 'MG' || $this->Agent_model->_state == 'MG'  ? 'selected' : '' ?>>
                                        Minas Gerais
                                    </option>
                                    <option
                                        value="PA" <?= set_value('state') == 'PA' || $this->Agent_model->_state == 'PA'  ? 'selected' : '' ?>>
                                        Pará
                                    </option>
                                    <option
                                        value="PB" <?= set_value('state') == 'PB' || $this->Agent_model->_state == 'PB'  ? 'selected' : '' ?>>
                                        Paraíba
                                    </option>
                                    <option
                                        value="PR" <?= set_value('state') == 'PR' || $this->Agent_model->_state == 'PR'  ? 'selected' : '' ?>>
                                        Paraná
                                    </option>
                                    <option
                                        value="PE" <?= set_value('state') == 'PE' || $this->Agent_model->_state == 'PE'  ? 'selected' : '' ?>>
                                        Pernambuco
                                    </option>
                                    <option
                                        value="PI" <?= set_value('state') == 'PI' || $this->Agent_model->_state == 'PI'  ? 'selected' : '' ?>>
                                        Piauí
                                    </option>
                                    <option
                                        value="RJ" <?= set_value('state') == 'RJ' || $this->Agent_model->_state == 'RJ'  ? 'selected' : '' ?>>
                                        Rio de Janeiro
                                    </option>
                                    <option
                                        value="RN" <?= set_value('state') == 'RN' || $this->Agent_model->_state == 'RN'  ? 'selected' : '' ?>>
                                        Rio Grande do Norte
                                    </option>
                                    <option
                                        value="RS" <?= set_value('state') == 'RS' || $this->Agent_model->_state == 'RS'  ? 'selected' : '' ?>>
                                        Rio Grande do Sul
                                    </option>
                                    <option
                                        value="RO" <?= set_value('state') == 'RO' || $this->Agent_model->_state == 'RO'  ? 'selected' : '' ?>>
                                        Rondônia
                                    </option>
                                    <option
                                        value="RR" <?= set_value('state') == 'RR' || $this->Agent_model->_state == 'RR'  ? 'selected' : '' ?>>
                                        Roraima
                                    </option>
                                    <option
                                        value="SC" <?= set_value('state') == 'SC' || $this->Agent_model->_state == 'SC'  ? 'selected' : '' ?>>
                                        Santa Catarina
                                    </option>
                                    <option
                                        value="SP" <?= set_value('state') == 'SP' || $this->Agent_model->_state == 'SP'  ? 'selected' : '' ?>>
                                        São Paulo
                                    </option>
                                    <option
                                        value="SE" <?= set_value('state') == 'SE' || $this->Agent_model->_state == 'SE'  ? 'selected' : '' ?>>
                                        Sergipe
                                    </option>
                                    <option
                                        value="TO" <?= set_value('state') == 'TO' || $this->Agent_model->_state == 'TO'  ? 'selected' : '' ?>>
                                        Tocantins
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Proprietário da conta</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_user" name="fk_user" >
                                    <option></option>
                                    <?php foreach ($users as $user) : ?>
                                        <option value="<?= $user['pk_user'] ?>" <?= set_value('fk_user') == $user['pk_user'] || $this->Agent_model->_fk_user == $user['pk_user'] ? "selected" : "" ?>><?= $user['name'] . ' ' . $user['surname'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_sector') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Setor</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_sector" name="fk_sector" >
                                    <option></option>
                                    <?php foreach ($sectors as $sector) : ?>
                                        <option value="<?= $sector['pk_sector'] ?>" <?= set_value('fk_sector') == $sector['pk_sector'] || $this->Agent_model->_fk_sector == $sector['pk_sector'] ? "selected" : "" ?>><?= $sector['sector'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>agent/view/<?=$this->Agent_model->_pk_agent?>'; return false;">Cancelar</button>
                            </div>
                        </div>
                        
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>


<!-- Modal Deletar -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_agent" id="pk_agent" value="<?=$this->Agent_model->_pk_agent?>" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal Status -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="status" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent/status/<?=$this->Agent_model->_pk_agent?>">
        <input type="hidden" name="status" id="status" value="true" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-warning">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja <?=$this->Agent_model->_status == $this->Agent_model->_status_active ?  "INATIVAR" : "ATIVAR" ?> este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-warning" type="submit">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
