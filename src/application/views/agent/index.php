<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contas
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Contas</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Contas
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>                        
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>agent/create'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: <?= (is_array($fk_agent_types) && !empty($fk_agent_types)) ? 'block' : 'none' ?>">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>agent">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Tipo de Agente</label>
                                <div class="col-lg-6">
                                    <select class="form-control select2" multiple id="fk_agent_type" name="fk_agent_type[]" >
                                        <?php foreach ($agent_types as $type) : ?>
                                            <option value="<?= $type['pk_agent_type']?>" <?= in_array($type['pk_agent_type'], $fk_agent_types) ? 'selected' : '' ?>><?= $type['type'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success" type="submit">Filtrar</button>
                                    <?php if (is_array($fk_agent_types) && !empty($fk_agent_types)) : ?>
                                        <a href="<?= base_url() ?>agent" class="btn btn-info">Resetar</a>
                                    <?php endif; ?>
                                </div>
                        </div>
                    </form>
                </div>                
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table hover">
                        <thead>
                            <tr>
                                <th>
                                    Agente
                                </th>
                                <th>
                                    Tipo
                                </th>
                                <th>
                                    Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr data-href="<?=base_url() ?>agent/view/<?=$item['pk_agent']?>" style="cursor: pointer">
                                    <td><a href="<?=base_url() ?>agent/view/<?=$item['pk_agent']?>"><?= $item['community_name'] ?></a></td>
                                    <td><?= $item['type'] ?></td>
                                    <td>
                                        <?php if ($item['status'] == $this->Agent_model->_status_active) : ?>
                                            <span class="label label-success">Ativo</span></span>
                                        <?php else : ?>
                                            <span class="label label-warning">Inativo</span></span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>agent/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_agent" id="pk_agent" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
