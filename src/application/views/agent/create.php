<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Agentes
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent">Agentes</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Agente
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>agent/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('community_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="community_name">Nome Fantasia/Unidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Nome Fantasia ou Nome da Unidade" id="community_name" name="community_name" class="form-control" value="<?= set_value('community_name') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('company_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="company_name">Razão Social</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Razão Social da Empresa" id="company_name" name="company_name" class="form-control" value="<?= set_value('company_name') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('cnpj') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="cnpj">CNPJ</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CNPJ" id="cnpj" name="cnpj" class="form-control" value="<?= set_value('cnpj') ?>" alt="cnpj">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ie') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="ie">Inscrição Estadual</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Inscrição Estadual" id="ie" name="ie" class="form-control" value="<?= set_value('ie') ?>" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('pk_agent_type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Tipo</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_agent_type" name="fk_agent_type">
                                    <option></option>
                                    <?php if ($agent_types) : ?>
                                        <?php foreach ($agent_types as $agent_type) : ?>
                                            <option value="<?= $agent_type['pk_agent_type'] ?>" <?= set_value('fk_agent_type') == $agent_type['pk_agent_type'] ? "selected" : "" ?>><?= $agent_type['type'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('postal_code') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="postal_code">CEP</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CEP" id="postal_code" name="postal_code" class="form-control" value="<?= set_value('postal_code') ?>" alt="cep">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('address') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="address">Endereço</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Endereço" id="address" name="address" class="form-control" value="<?= set_value('address') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('number') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="number">Número</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Número" id="number" name="number" class="form-control" value="<?= set_value('number') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('complement') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="complement">Complemento</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Complemento" id="complement" name="complement" class="form-control" value="<?= set_value('complement') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('district') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="district">Bairro</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Bairro" id="district" name="district" class="form-control" value="<?= set_value('district') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('city') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Cidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Cidade" id="city" name="city" class="form-control" value="<?= set_value('city') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                            <div class="col-lg-10">
                                <select id="state" name="state" class="form-control">
                                    <option
                                        value="AC" <?= set_value('state') == 'AC' ? 'selected' : '' ?>>
                                        Acre
                                    </option>
                                    <option
                                        value="AL" <?= set_value('state') == 'AL' ? 'selected' : '' ?>>
                                        Alagoas
                                    </option>
                                    <option
                                        value="AP" <?= set_value('state') == 'AP' ? 'selected' : '' ?>>
                                        Amapá
                                    </option>
                                    <option
                                        value="AM" <?= set_value('state') == 'AM' ? 'selected' : '' ?>>
                                        Amazonas
                                    </option>
                                    <option
                                        value="BA" <?= set_value('state') == 'BA' ? 'selected' : '' ?>>
                                        Bahia
                                    </option>
                                    <option
                                        value="CE" <?= set_value('state') == 'CE' ? 'selected' : '' ?>>
                                        Ceará
                                    </option>
                                    <option
                                        value="DF" <?= set_value('state') == 'DF' ? 'selected' : '' ?>>
                                        Distrito Federal
                                    </option>
                                    <option
                                        value="ES" <?= set_value('state') == 'ES' ? 'selected' : '' ?>>
                                        Espirito Santo
                                    </option>
                                    <option
                                        value="GO" <?= set_value('state') == 'GO' ? 'selected' : '' ?>>
                                        Goiás
                                    </option>
                                    <option
                                        value="MA" <?= set_value('state') == 'MA' ? 'selected' : '' ?>>
                                        Maranhão
                                    </option>
                                    <option
                                        value="MS" <?= set_value('state') == 'MS' ? 'selected' : '' ?>>
                                        Mato Grosso do Sul
                                    </option>
                                    <option
                                        value="MT" <?= set_value('state') == 'MT' ? 'selected' : '' ?>>
                                        Mato Grosso
                                    </option>
                                    <option
                                        value="MG" <?= set_value('state') == 'MG' ? 'selected' : '' ?>>
                                        Minas Gerais
                                    </option>
                                    <option
                                        value="PA" <?= set_value('state') == 'PA' ? 'selected' : '' ?>>
                                        Pará
                                    </option>
                                    <option
                                        value="PB" <?= set_value('state') == 'PB' ? 'selected' : '' ?>>
                                        Paraíba
                                    </option>
                                    <option
                                        value="PR" <?= set_value('state') == 'PR' ? 'selected' : '' ?>>
                                        Paraná
                                    </option>
                                    <option
                                        value="PE" <?= set_value('state') == 'PE' ? 'selected' : '' ?>>
                                        Pernambuco
                                    </option>
                                    <option
                                        value="PI" <?= set_value('state') == 'PI' ? 'selected' : '' ?>>
                                        Piauí
                                    </option>
                                    <option
                                        value="RJ" <?= set_value('state') == 'RJ' ? 'selected' : '' ?>>
                                        Rio de Janeiro
                                    </option>
                                    <option
                                        value="RN" <?= set_value('state') == 'RN' ? 'selected' : '' ?>>
                                        Rio Grande do Norte
                                    </option>
                                    <option
                                        value="RS" <?= set_value('state') == 'RS' ? 'selected' : '' ?>>
                                        Rio Grande do Sul
                                    </option>
                                    <option
                                        value="RO" <?= set_value('state') == 'RO' ? 'selected' : '' ?>>
                                        Rondônia
                                    </option>
                                    <option
                                        value="RR" <?= set_value('state') == 'RR' ? 'selected' : '' ?>>
                                        Roraima
                                    </option>
                                    <option
                                        value="SC" <?= set_value('state') == 'SC' ? 'selected' : '' ?>>
                                        Santa Catarina
                                    </option>
                                    <option
                                        value="SP" <?= set_value('state') == 'SP' ? 'selected' : '' ?>>
                                        São Paulo
                                    </option>
                                    <option
                                        value="SE" <?= set_value('state') == 'SE' ? 'selected' : '' ?>>
                                        Sergipe
                                    </option>
                                    <option
                                        value="TO" <?= set_value('state') == 'TO' ? 'selected' : '' ?>>
                                        Tocantins
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Usuário</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_user" name="fk_user" >
                                    <option></option>
                                    <?php foreach ($users as $user) : ?>
                                        <option value="<?= $user['pk_user'] ?>" <?= set_value('fk_user') == $user['pk_user'] || $user['pk_user'] == $this->session->userdata("pk_user") ? "selected" : "" ?>><?= $user['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_sector') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Setor</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_sector" name="fk_sector" >
                                    <option></option>
                                    <?php foreach ($sectors as $sector) : ?>
                                        <option value="<?= $sector['pk_sector'] ?>" <?= set_value('fk_sector') == $sector['pk_sector'] ? "selected" : "" ?>><?= $sector['sector'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Unidade</label>
                            <div class="col-lg-1">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" id="create_unity" name="create_unity" checked="checked"> <label for="create_unity">Criar uma unidade com os dados deste agente</label>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
