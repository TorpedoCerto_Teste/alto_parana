<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Pagamento
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>payment/">Pagamentos</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Pagamento
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">

                    <form method="post" class="form-horizontal" action="<?= base_url() ?>payment/create">
                        <input type="hidden" name="create" id="create" value="true" />


                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-3 control-label">Mês:</label>
                            <div class="col-lg-3">
                                <select class="form-control" name="month" id="month">
                                    <?php for ($i = 1; $i<=12; $i++) : ?>
                                    <option value="<?=$i?>" <?= set_value('month') == $i || $i == date("m") ? 'selected' : '' ?>><?= retorna_mes($i)?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-3 control-label">Ano:</label>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" placeholder="" name="year" id="year" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('value') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-3 control-label">Valor:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control money" placeholder="" name="value" id="value" value="<?= set_value('value') > 0 ? set_value('value') : '' ?>" alt="valor">
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-success"><i class="icon-check position-right"></i> Salvar </button>
                        </div>
                    </form>                    
                </div>
            </section>    
        </div>
    </div>
</div>