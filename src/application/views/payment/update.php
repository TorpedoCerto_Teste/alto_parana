<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Editar Payment
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php if ($error) : ?>
                            <div class="alert alert-danger">
                                <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                            </div>
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="<?= base_url() ?>payment/update/<?=$this->Payment_model->_pk_payment?>">
                            <input type="hidden" name="update" id="update" value="true" />


                                            <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                                <label class="col-lg-3 control-label">Month:</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control numeric" placeholder="" name="month" id="month" value="<?= set_value('month') != "" ? set_value('month') : $this->Payment_model->_month  ?>" >
                                                </div>
                                            </div>
                    
                                            <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                                <label class="col-lg-3 control-label">Year:</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control numeric" placeholder="" name="year" id="year" value="<?= set_value('year') != "" ? set_value('year') : $this->Payment_model->_year  ?>" >
                                                </div>
                                            </div>
                    
                                            <div class="form-group <?= form_error('value') != "" ? "has-error" : ""; ?>">
                                                <label class="col-lg-3 control-label">Value:</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control money" placeholder="" name="value" id="value" value="<?= set_value('value') != "" ? set_value('value') : $this->Payment_model->_value  ?>" >
                                                </div>
                                            </div>
                    

                            <div class="text-right">
                                <button type="submit" class="btn btn-success"><i class="icon-check position-right"></i> Salvar </button>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url() ?>payment';"> Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>