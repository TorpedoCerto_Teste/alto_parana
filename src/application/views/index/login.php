<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Admin Template">
        <meta name="keywords" content="admin dashboard, admin, flat, flat ui, ui kit, app, web app, responsive">
        <link rel="shortcut icon" href="<?=base_url()?>img/ico/favicon.ico">
        <title>Login - ALTO PARANÁ ENERGIA</title>

        <!-- Base Styles -->
        <link href="<?=base_url()?>css/style.css" rel="stylesheet">
        <link href="<?=base_url()?>css/style-responsive.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?=base_url()?>js/html5shiv.min.js"></script>
        <script src="<?=base_url()?>js/respond.min.js"></script>
        <![endif]-->


    </head>

    <body class="login-body">

        <div class="login-logo" style="background-color: black !important">
            <img src="<?=base_url()?>img/APE_1.png" alt=""/>
        </div>

        <h2 class="form-heading">ALTO PARANÁ ENERGIA</h2>
        <div class="container log-row">
            <form class="form-signin" action="<?= base_url() ?>index/login" method="post">
                <input type="hidden" name="login" id="login" value="true" />
                <div class="login-wrap">
                    
                    <?php if ($expiration == 2) : ?>
                        <div class="alert alert-warning">
                            <button data-dismiss="alert" class="close">
                                ×
                            </button>
                            <i class="fa fa-exclamation-triangle"></i>
                            <strong>Atenção!</strong> Sua conta irá expirar em <?= 16 - date("d") ?> dias.
                        </div>
                    <?php endif; ?>
                    <?php if ($expiration == 3) : ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-times-circle"></i>
                            <strong>CONTA SUSPENSA!</strong> Conta temporariamente suspensa. Clique <a href="<?=base_url()?>payment/create">aqui</a> para ativar.
                        </div>                    
                    <?php else : ?>
                        <div class="alert alert-block alert-danger fade in" style="display: <?=$error == 1 ? "block": "none"?>">
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Confira seu email ou senha! 
                        </div>
                        <input type="email" name="email" id="email" class="form-control <?= form_error('email') != "" ? "error" : ""; ?>" placeholder="E-mail" autofocus required type="email">
                        <input type="password" name="password" id="password" class="form-control <?= form_error('password') != "" ? "error" : ""; ?>" placeholder="Password" required >
                        <button class="btn btn-lg btn-success btn-block" type="submit">LOGIN</button>
                        <label class="checkbox-custom check-success">
                            <a class="pull-right" data-toggle="modal" href="#forgotPass"> Esqueceu a senha?</a>
                            <a class="pull-left btn btn-sm btn-info" href="<?=base_url()?>/customer"> Área do Cliente</a>
                        </label>
                    <?php endif; ?>
                    
                </div>

                <!-- Modal -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="forgotPass" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Esqueceu a senha?</h4>
                            </div>
                            <div class="modal-body">
                                <p>Entre com seu e-mail que enviaremos instruções de acesso.</p>
                                <input type="text" name="emailForgot" id="emailForgot" placeholder="E-mail" autocomplete="off" class="form-control placeholder-no-fix <?= form_error('emailForgot') != "" ? "error" : ""; ?>" type="email">

                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                                <button class="btn btn-success" type="button">Recuperar Senha</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->

            </form>
        </div>


        <!--jquery-1.10.2.min-->
        <script src="<?=base_url()?>js/jquery-1.11.1.min.js"></script>
        <!--Bootstrap Js-->
        <script src="<?=base_url()?>js/bootstrap.min.js"></script>
        <!--<script src="js/jrespond..min.html"></script>-->

    </body>
</html>
