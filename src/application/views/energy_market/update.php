<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Energy Market
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>energy_market/">Energy Market</a></li>
            <li class="active">Editar Energy Market</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar energy_market
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                        <?php if ($errors) : ?>
                            <?php foreach ($errors as $k => $err ) : ?>
                                <p><?= $k ?> - <?= strip_tags($err) ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>energy_market/update/<?= $this->Energy_market_model->_pk_energy_market ?>" enctype="multipart/form-data">
                        <input type="hidden" name="update" id="update" value="true" />

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Período</label>
                            <div class="col-lg-4 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= $i ?>" <?= $this->Energy_market_model->_month == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : $this->Energy_market_model->_year ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Semana</span>
                                <input type="text" id="week" name="week" class="form-control" value="<?= set_value('week') != "" ? set_value('week') : $this->Energy_market_model->_week ?>" alt="year"
                                       data-bts-min="1"
                                       data-bts-max="6"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div>                        

                        <div class="form-group <?= form_error('energy_market_file_indicator') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="energy_market_file_indicator">Arquivo Indicadores</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="energy_market_file_indicator" name="energy_market_file_indicator" class="form-control" accept="application/pdf">
                            </div>
                            <div class="col-lg-4">
                                <?php if ($this->Energy_market_model->_energy_market_file_indicator) : ?>
                                    <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/energy_market/<?= $this->Energy_market_model->_pk_energy_market ?>/<?= $this->Energy_market_model->_energy_market_file_indicator ?>" class="btn btn-info btn-xs" target="_blank"><?= $this->Energy_market_model->_energy_market_file_indicator ?></a>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('energy_market_file_price') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="energy_market_file_price">Arquivo Preços</label>
                            <div class="col-lg-4">
                                <input type="file" placeholder="" id="energy_market_file_price" name="energy_market_file_price" class="form-control" accept="application/pdf">
                            </div>
                            <div class="col-lg-4">
                                <?php if ($this->Energy_market_model->_energy_market_file_price) : ?>
                                    <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/energy_market/<?= $this->Energy_market_model->_pk_energy_market ?>/<?= $this->Energy_market_model->_energy_market_file_price ?>" class="btn btn-info btn-xs" target="_blank"><?= $this->Energy_market_model->_energy_market_file_price ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
