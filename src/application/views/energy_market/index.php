<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Energy Market
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Energy Market</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Energy Market
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>energy_market/create'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo
                        </button>
                    </span>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th style="display:none">Ano/Mês (oculto)</th>
                                <th>Ano/Mês</th>
                                <th>Semana</th>
                                <th>Indicadores</th>
                                <th>Preços</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td style="display: none"><?= $item['year'] .str_pad($item['month'], 2, '0', STR_PAD_LEFT) ?></td>
                                    <td><?= $item['year'] . "/" . retorna_mes($item['month']) ?></td>
                                    <td><?= $item['week'] ?></td>
                                    <td>
                                        <?php if ($item['energy_market_file_indicator']) : ?>
                                            <a href="https://docs.google.com/viewer?url=<?= base_url() ?>uploads/energy_market/<?= $item['pk_energy_market'] ?>/<?= $item['energy_market_file_indicator'] ?>&embedded=true" class="btn btn-info btn-xs" target="_blank"><?= $item['energy_market_file_indicator'] ?></a>
                                        <?php else: ?>
                                            <a href="#" class="btn btn-dark btn-xs">Sem arquivo</a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($item['energy_market_file_price']) : ?>
                                            <a href="https://docs.google.com/viewer?url=<?= base_url() ?>uploads/energy_market/<?= $item['pk_energy_market'] ?>/<?= $item['energy_market_file_price'] ?>&embedded=true" class="btn btn-info btn-xs" target="_blank"><?= $item['energy_market_file_price'] ?></a>
                                        <?php else: ?>
                                            <a href="#" class="btn btn-dark btn-xs">Sem arquivo</a>
                                        <?php endif; ?>
                                    </td>
                                    <td class="hidden-xs">
                                        <a class="btn btn-success btn-xs" href = '<?= base_url() ?>energy_market/update/<?= $item['pk_energy_market'] ?>'><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_energy_market').val(<?= $item['pk_energy_market'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                        <?php if (!$item['schedule']) : ?>
                                            <a class="btn btn-primary btn-xs tooltips" href="#schedule" onclick="$('#pk_energy_market_schedule').val(<?= $item['pk_energy_market'] ?>)" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Agendar envio de email"><i class="fa  fa-envelope-o "></i></a>
                                        <?php else : ?>
                                            <a class="btn btn-primary btn-xs tooltips" href = '<?= base_url() ?>energy_market/delete_schedule/<?= $item['pk_energy_market'] ?>' data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Remover Agendamento"><i class="fa  fa-envelope"></i></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?> 
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>energy_market/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_energy_market" id="pk_energy_market" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="schedule" class="modal fade">
    <form method="POST" class="form-horizontal" action="<?= base_url() ?>energy_market/schedule">
        <input type="hidden" name="pk_energy_market_schedule" id="pk_energy_market_schedule" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Agendamento de envio de Energy Market</h4>
                </div>
                <div class="modal-body">

                    <label class="checkbox-custom check-success">
                        <input type="checkbox" name="test" id="test" value="test"/>
                        <label for="test">Teste</label>
                    </label>

                    <div class="clearfix"></div>
                    <div class="form-group hidden" id="div_test_email">
                        <label class="col-lg-2 col-sm-2 control-label" for="test_email">Email de teste</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="test_email" id="test_email" />
                        </div>
                    </div>

                    <div class="alert alert-success alert-block fade in hidden" id="alert_success">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="fa fa-check"></i>
                            Successo!
                        </h4>
                        <p>Verifique sua caixa de email</p>
                    </div>                    

                    <div class="alert alert-danger alert-block fade in hidden" id="alert_danger">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="fa fa-close"></i>
                            Erro!
                        </h4>
                        <p>Houve algum erro ao enviar o email</p>
                    </div>                    

                    <div id="agendamento">                        
                        <div class="form-group <?= form_error('date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="date">Data para disparo</label>
                            <div class="col-lg-6 input-append date">
                                <input type="text" placeholder="" id="date" name="date" class="form-control" value="<?= set_value('date') != "" ? set_value('date') : date("d/m/Y") ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('time') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="time">Hora</label>
                            <div class="col-lg-4 input-append time">
                                <input type="text" placeholder="" id="time" name="time" class="form-control" value="<?= set_value('time') != "" ? set_value('time') : date("H:00", strtotime("+1 HOUR")) ?>" alt="time">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer" id="footer_agendar">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-primary" type="submit">Agendar</button>
                </div>
                <div class="modal-footer hidden" id="footer_test">
                    <button class="btn btn-success" type="button" id="btn_test">Testar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->