<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo CCVEE (etapa 1)
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo CCVEE (etapa 1)
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        
                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Cliente de Serviço</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach($units as $type => $unity) : ?>
                                    <optgroup label="<?=$type?>">
                                        <?php foreach($unity as $unit) : ?>
                                        <option value="<?=$unit['pk_unity']?>" <?=set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?=$unit['agent_company_name']." - ".$unit['community_name']?></option>
                                        <?php endforeach; ?>
                                    </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_user">Cadastrado Por</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="fk_user" id="fk_user">
                                    <option></option>
                                        <?php foreach($users as $user) : ?>
                                        <option value="<?=$user['pk_user']?>" <?=$user['pk_user'] == $this->session->userdata('pk_user') ? 'selected':'' ?>><?=$user['name']." ".$user['surname']." - ".$user['email'] ?></option>
                                        <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Continuar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>product_category'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
    
    
</div>
