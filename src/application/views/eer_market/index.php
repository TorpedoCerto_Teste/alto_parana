<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        EER Mercado
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">EER Mercado</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    EER Mercado
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>eer_market">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group ">
                            <div class="col-lg-2">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div> 
                        <span class="tools pull-right">
                            <button class="btn btn-info addon-btn m-b-10" type="submit">
                                <i class="fa fa-filter pull-right"></i>
                                Filtrar
                            </button>
                        </span>
                        <div style="clear: both"></div>
                    </form>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Data do Relatório</th>
                                <th>Data do Pagamento</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= retorna_mes($item['month']) . "/" . $item['year'] ?></td>
                                    <td><input type="text" class="form-control  reporting_date"  month="<?= $item['month'] ?>" value="<?= date_to_human_date($item['reporting_date'], 0) ?>" alt="date"></td>
                                    <td><input type="text" class="form-control  payment_date"  month="<?= $item['month'] ?>"  value="<?= date_to_human_date($item['payment_date'], 0) ?>" alt="date"></td>
                                    <td><input type="text" class="form-control value" month="<?= $item['month'] ?>" value="<?= number_format($item['value'],2,",","") ?>" alt="valor2"></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>

