<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Produto
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>product/index/<?=$this->Product_category_model->_pk_product_category?>">Produtos</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Produto para <?= $this->Product_category_model->_category ?>
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>product/create/<?=$this->Product_category_model->_pk_product_category?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="category">Categoria</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="category" name="category" class="form-control" value="<?= $this->Product_category_model->_category ?>" disabled="disabled">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('product_short') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="product_short">Descrição Curta</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="product_short" name="product_short" class="form-control" value="<?= set_value('product_short') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('product') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="product">Descrição Longa</label>
                            <div class="col-lg-10">
                                <textarea id="product" name="product" class="form-control"><?= set_value('product')  ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>product/index/<?=$this->Product_category_model->_pk_product_category?>'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
