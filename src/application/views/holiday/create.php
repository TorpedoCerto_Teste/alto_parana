<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Feriado
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>holiday">Feriado</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Feriado
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>holiday/create">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group <?= form_error('date_holiday') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="date_holiday">Data</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="date_holiday" name="date_holiday" class="form-control" value="<?= set_value('date_holiday') ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('holiday') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="holiday">Descrição</label>
                            <div class="col-lg-6">
                                <input type="text" placeholder="" id="holiday" name="holiday" class="form-control" value="<?= set_value('holiday') ?>" alt="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
