<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Feriados
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Feriados</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Feriados
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>holiday/create'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo
                        </button>
                    </span>
                </header>
                <div class="panel-body">
                    <form id='frm_holiday' role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>holiday/index" >

                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" id="year" name="year" class="form-control" value="<?= $this->Holiday_model->_year != "" ? $this->Holiday_model->_year : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div>
                    </form>
                </div>

                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Descrição</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= date("d/m", strtotime($item['date_holiday'])) ?></td>
                                    <td><?= $item['holiday'] ?></td>
                                    <td class="hidden-xs">
                                        <a class="btn btn-success btn-xs" href = '<?= base_url() ?>holiday/update/<?= $item['pk_holiday'] ?>'><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_holiday').val(<?= $item['pk_holiday'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>holiday/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_holiday" id="pk_holiday" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
