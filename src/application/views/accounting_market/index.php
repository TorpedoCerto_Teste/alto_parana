<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contabilização Mercado
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Contabilização Mercado</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Contabilização Mercado
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>accounting_market">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group ">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div> 
                        <div style="clear: both"></div>
                    </form>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Selecione um agente e um ano para filtrar.
                    </div>
                <?php else : ?>
                    <div id="div_table">
                        <table class="table responsive-data-table data-table">
                            <thead>
                                <tr>

                                    <th>Mês/Ano</th>
                                    <th>Data do Relatório</th>
                                    <th>Data de Débito</th>
                                    <th>Data de Crédito</th>
                                    <th>Data do Relatório GFN001</th>
                                    <th>Data Limite Aporte Garantia</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list as $item) : ?>
                                    <tr>
                                        <td><?= retorna_mes($item['month']) . "/" . $item['year'] ?></td>
                                        <td><input type="text" class="form-control date reporting_date"  month="<?= $item['month'] ?>" value="<?= date_to_human_date($item['reporting_date'], 0) ?>" alt="date"></td>
                                        <td><input type="text" class="form-control date debit_date"  month="<?= $item['month'] ?>"  value="<?= date_to_human_date($item['debit_date'], 0) ?>" alt="date"></td>
                                        <td><input type="text" class="form-control date credit_date"  month="<?= $item['month'] ?>"  value="<?= date_to_human_date($item['credit_date'], 0) ?>" alt="date"></td>
                                        <td><input type="text" class="form-control date gfn001_date"  month="<?= $item['month'] ?>"  value="<?= date_to_human_date($item['gfn001_date'], 0) ?>" alt="date"></td>
                                        <td><input type="text" class="form-control date warranty_limit_date"  month="<?= $item['month'] ?>"  value="<?= date_to_human_date($item['warranty_limit_date'], 0) ?>" alt="date"></td>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>

