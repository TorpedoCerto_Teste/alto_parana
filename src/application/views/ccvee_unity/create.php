<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo CCVEE Unidades
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ccvee_general">CCVEE</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->
<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo CCVEE Unidades
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_unity/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url()?>" disabled="disabled" />
                        <input type="hidden" name="pk_ccvee_general" id="pk_ccvee_general" value="3" />

                        <?php if (!$list_units) : ?>
                            <div class="form-group <?= form_error('unity_apportionment') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="unity_apportionment">Rateio</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="unity_apportionment" id="unity_apportionment">
                                        <option value="CONSUMO" <?= $this->Ccvee_general_model->_unity_apportionment == "CONSUMO" ? 'selected' : '' ?>>CONSUMO</option>
                                        <option value="MONTANTE" <?= $this->Ccvee_general_model->_unity_apportionment == "MONTANTE" ? 'selected' : '' ?>>MONTANTE</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('unity_differentiated_validate') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="unity_differentiated_validate">Vigências Diferenciadas por Unidade</label>
                                <div class="col-lg-1">
                                    <input type="checkbox" id="unity_differentiated_validate" name="unity_differentiated_validate" class="form-control" value="1" <?= $this->Ccvee_general_model->_unity_differentiated_validate == "1" ? "checked" : "" ?> >
                                </div>
                            </div>
                        <?php endif; ?>


                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($units as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div id="div_volumn_mwm" class="form-group <?= form_error('volumn_mwm') != "" ? "has-error" : ""; ?>" style="display: <?= $this->Ccvee_general_model->_unity_apportionment == "CONSUMO" ? "none" : "block" ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="volumn_mwm">Volume MW Médio</label>
                            <div class="col-lg-2">
                                <input type="text" id="volumn_mwm" name="volumn_mwm" class="form-control" value="" alt="valor6" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('contract_cliqccee') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="contract_cliqccee">Número Contrato CLIQCCEE</label>
                            <div class="col-lg-2">
                                <input type="text" id="contract_cliqccee" name="contract_cliqccee" class="form-control" alt="cpfcnpj" >
                            </div>
                        </div>
                        <div id="div_vicence" class="form-group" style="display: <?= $this->Ccvee_general_model->_unity_differentiated_validate == "0" ? "none" : "block" ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência diferenciada</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Adicionar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if (!$list_units) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Agente/Unidade</th>
                                <?php if ($this->Ccvee_general_model->_unity_apportionment == "MONTANTE") : ?>
                                    <th>Volume MW Médio</th>
                                <?php endif; ?>
                                <th># Contrato CLIQCCEE</th>
                                <?php if ($this->Ccvee_general_model->_unity_differentiated_validate == 1) : ?>
                                    <th>Data Início</th>
                                    <th>Data Fim</th>
                                <?php endif;?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list_units as $item) : ?>
                                <tr>
                                    <td><?= $item['agent_community_name']." - ".$item['unity_community_name'] ?></td>
                                    <?php if ($this->Ccvee_general_model->_unity_apportionment == "MONTANTE") : ?>
                                        <td><?= number_format($item['volumn_mwm'], 6, ",","") ?></td>
                                    <?php endif; ?>
                                    <td><?= $item['contract_cliqccee'] ?></td>
                                    <?php if ($this->Ccvee_general_model->_unity_differentiated_validate == 1) : ?>
                                        <td><?= $item['start_date'] ?></td>
                                        <td><?= $item['end_date'] ?></td>
                                    <?php endif; ?>
                                    <td class="hidden-xs">
                                        <a class="btn btn-success btn-xs" href = '<?= base_url() ?>'><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" href='#delete_product_category' onclick="$('#pk_product_category').val()" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>


</div>


<!-- Modal Deletar Unidade -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_product_category" class="modal fade">
    <form method="POST" action="<?= base_url() ?>product_category/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_product_category" id="pk_product_category" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
