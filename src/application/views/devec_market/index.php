<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        DEVEC
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">DEVEC</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    DEVEC
                </header>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>devec_market/upsert">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>" disabled="disabled"/>

                        <table class="dataTable table-hover table-responsive table-striped" style="width: 50%">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>DEVEC</th>
                                    <th>Data de Início</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($devecs) : ?>
                                    <?php foreach ($devecs as $uf => $devec) : ?>
                                        <tr>
                                            <td><?= $uf ?></td>
                                            <td><input type="checkbox" class="form-control chk" id="devec_<?=$uf?>" name="devec_<?=$uf?>" <?= $devec['devec'] == 1 ?  "checked" : "" ?> state="<?=$uf?>" value="1"></td>
                                            <td><input type="text" class="form-control start_date"  id="start_date_<?=$uf?>" name="start_date_<?=$uf?>" value="<?= date_to_human_date($devec['start_date']) ?>" state="<?=$uf?>" alt="date"></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>

                        </table>

                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
