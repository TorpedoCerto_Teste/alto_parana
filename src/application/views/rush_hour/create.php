<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Horário de Ponta
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>rush_hour">Horário de Ponta</a></li>
            <li class="active">Novo Horário de Ponta</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Horário de Ponta
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>rush_hour/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        
                        <div class="form-group <?= form_error('fk_agent_power_distributor') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Agente Distribuidor</label>
                            <div class="col-lg-6">
                                <select class="form-control select2-allow-clear" id="fk_agent_power_distributor" name="fk_agent_power_distributor">
                                    <option></option>
                                    <?php if ($power_distributors) : ?>
                                        <?php foreach ($power_distributors as $power_distributor) : ?>
                                            <option value="<?= $power_distributor['pk_agent'] ?>" <?= set_value('fk_agent_power_distributor') == $power_distributor['pk_agent'] ? "selected" : "" ?>><?= $power_distributor['community_name'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('start_hour') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="start_hour">Hora Inicial</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="start_hour" name="start_hour" class="form-control" value="<?= set_value('start_hour') ?>" alt="time">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('end_hour') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="end_hour">Hora Final</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="end_hour" name="end_hour" class="form-control" value="<?= set_value('end_hour') ?>" alt="time">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('current') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="current">Atual</label>
                            <div class="col-lg-2">
                                <input type="checkbox" placeholder="" id="current" name="current" class="form-control" <?= set_value('current') == 1 ? "checked" : "" ?> value="1" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>rush_hour/'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
