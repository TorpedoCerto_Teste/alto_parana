<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Perfil
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent/view/<?= $this->Agent_model->_pk_agent ?>/perfis">Agente</a></li>
            <li class="active">Novo Perfil</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Perfil - <?= $this->Agent_model->_community_name ?>
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>profile/create/<?= $this->Agent_model->_pk_agent ?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('profile') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="profile">Perfil</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Perfil" id="profile" name="profile" class="form-control" value="<?= set_value('profile') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('code') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="code">Código</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Código" id="code" name="code" class="form-control" value="<?= set_value('code') ?>" alt="fone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_submarket') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Submercado</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_submarket" name="fk_submarket">
                                    <option></option>
                                    <?php if ($submarkets) : ?>
                                        <?php foreach ($submarkets as $submarket) : ?>
                                            <option value="<?= $submarket['pk_submarket'] ?>" <?= set_value('fk_submarket') == $submarket['pk_submarket'] ? "selected" : "" ?>><?= $submarket['submarket'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_standard') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Classe</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_standard" name="fk_standard">
                                    <option></option>
                                    <?php if ($standards) : ?>
                                        <?php foreach ($standards as $standard) : ?>
                                            <option value="<?= $standard['pk_standard'] ?>" <?= set_value('fk_standard') == $standard['pk_standard'] ? "selected" : "" ?>><?= $standard['standard'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_category') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Categoria</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_category" name="fk_category">
                                    <option></option>
                                    <?php if ($categories) : ?>
                                        <?php foreach ($categories as $category) : ?>
                                            <option value="<?= $category['pk_category'] ?>" <?= set_value('fk_category') == $category['pk_category'] ? "selected" : "" ?>><?= $category['category'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>agent/view/<?= $this->Agent_model->_pk_agent ?>/perfis'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
