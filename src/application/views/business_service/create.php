<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Serviços
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>business_service/index">Serviços</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Serviço
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business/create">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group <?= form_error('fk_service') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_service">Serviço</label>
                            <div class="col-lg-6">
                                <select name="fk_service" id="fk_service" class="form-control select2">
                                    <option></option>
                                    <?php if ($services) : ?>
                                        <?php foreach ($services as $service) : ?>
                                            <option value="<?= $service['pk_service'] ?>"><?= $service['service'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-info">Personalizar</button>
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('lis_product') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="lis_product">Categorias e Produtos</label>
                            <div class="col-lg-10">
                                <table id="lis_product" name="lis_product" >
                                    <?php if ($products) : ?>
                                        <?php foreach ($products as $category_name => $pk_category_prds) : ?>
                                            <?php foreach ($pk_category_prds as $pk_category_product => $prds) : ?>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox-custom check-success">
                                                            <input class="category" type="checkbox" value="<?= $pk_category_product ?>" name="<?= $pk_category_product ?>" id="<?= $pk_category_product ?>"  /><label for="<?= $pk_category_product ?>">&nbsp;</label>
                                                        </label>
                                                    </td>
                                                    <td colspan="2"><b><?= $category_name ?></b></td>
                                                </tr>
                                                <?php foreach ($prds as $prd) : ?>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <label class="checkbox-custom check-success">
                                                                <input class="<?= $pk_category_product ?>" type="checkbox" value="<?=$prd['pk_product']?>" id="product_<?=$prd['pk_product'] ?>" name="product[]" /><label for="product_<?=$prd['pk_product'] ?>">&nbsp;</label>
                                                            </label>
                                                        </td>
                                                        <td><?= $prd['product']?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                </table>


                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>


                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
