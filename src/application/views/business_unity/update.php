<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Unidade
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>business_unity/index">Unidade</a></li>
            <li class="active">Editar Unidade</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Unidade
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_unity/update/<?=$this->uri->segment(3,0)?>">
                        <input type="hidden" name="update" id="update" value="true" />
                       
                         <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($units2 as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div id="div_vicence" class="form-group" style="display: block">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência diferenciada</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>
    </div>
</div>
