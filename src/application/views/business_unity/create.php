<!--UNIDADES-->

<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Unidade
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>business_unity/index">Unidade</a></li>
            <li class="active">Nova Unidade</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Nova Unidade
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_unity/create/">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($units2 as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('unity_differentiated_validate') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="unity_differentiated_validate">Vigências Diferenciadas por Unidade</label>
                            <div class="col-lg-1">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" id="unity_differentiated_validate" name="unity_differentiated_validate" class="form-control" value="1"  ><label for="unity_differentiated_validate">&nbsp;</label>
                                </label>
                            </div>
                        </div>

                        <div id="div_vicence" class="form-group" style="display: block">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência diferenciada</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Adicionar</button>
                            </div>
                        </div>

                        <div>
                            <?php if (!$list) : ?>
                                <div class="alert alert-warning fade in">
                                    <button type="button" class="close close-sm" data-dismiss="alert">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                </div>
                            <?php else : ?>
                                <table class="table responsive-data-table data-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Unidade
                                            </th>
                                            <th>
                                                Data inicial
                                            </th>
                                            <th>
                                                Data final
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($list as $item) : ?>
                                            <tr>
                                                <td><?= $item['community_name'] ?></td>
                                                <td><?= date_to_human_date($item['start_date']) ?></td>
                                                <td><?= date_to_human_date($item['end_date']) ?></td>
                                                <td class="hidden-xs">
                                                    <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_business_unity').val(<?= $item['pk_business_unity'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                        </div>

                    </form> 
                </div>                                    
        </div>                                    
    </div>

</div>  

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>business_unity/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_business_unity" id="pk_business_unity" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->


