<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Relatório de Energia</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Relatório de Energia </p>
            <p align="justify">Prezados, encaminhamos em anexo e disponibilizamos na <a href="<?=base_url()?>customer/free_captive" target="_blank">área do cliente</a> o relatório de Energia do Agente <?=$this->Agent_model->_community_name?>, conforme os dados abaixo.</p>
        <br/>
        <br />
        <table style="border: 0px; width: 100%;">
            <tr>
                <td width="300px">Referência</td>
                <td><?= retorna_mes($this->Free_captive_model->_month)?>/<?=$this->Free_captive_model->_year?></td>
            </tr>
        </table>
        <br/>
        <table style="border: 0px; width: 100%;">
            <thead>
                <tr>
                    <th style="text-align: left">Unidade</th>
                    <th style="text-align: left">Economia</th>
                    <th style="text-align: left">Total Cativo </th>
                    <th style="text-align: left">Total Livre</th>
                    <th style="text-align: left">Distribuidora</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($free_captives as $free_captive) : ?>
                <tr>
                    <td><?=$free_captive['unity_name']?></td>
                    <td>R$<?= number_format(($free_captive['captive_total'] - $free_captive['free_total']),2,",",".") ?> (<?=number_format($free_captive['economy'],2,",","")?>%)</td>
                    <td>R$ <?=number_format($free_captive['captive_total'],2,",",".")?></td>
                    <td>R$ <?=number_format($free_captive['free_total'],2,",",".")?></td>
                    <td><?=$free_captive['agent_power_distributor']?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            
        </table>
      <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?=$this->config->item("config_phone")?> e em nosso atendimento on-line, via chat da área do cliente.</p>
        <br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>
    </body>
</html>