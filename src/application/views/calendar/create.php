<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Calendário
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>calendar">Calendário</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Evento
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>calendar/create">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Referência</label>
                            <div class="col-lg-4 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month_reference" name="month_reference" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= $i ?>" <?= set_value('month_reference') == $i  ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year_reference') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year_reference" name="year_reference" class="form-control" value="<?= set_value('year_reference') != "" ? set_value('year_reference') : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div>
                        <div class="form-group <?= form_error('') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_calendar_responsible">Responsável</label>
                            <div class="col-lg-6">
                                <select class='form-control select2' id='fk_calendar_responsible' name='fk_calendar_responsible'>
                                    <option></option>
                                    <?php foreach ($calendar_responsibles as $calendar_responsible) : ?>
                                        <option value='<?= $calendar_responsible['pk_calendar_responsible'] ?>'><?= $calendar_responsible['responsible'] ?></option>>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="event_date">Data do Evento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="event_date" name="event_date" class="form-control" value="<?= set_value('event_date') ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="calendar">Descrição</label>
                            <div class="col-lg-6">
                                <input type="text" placeholder="" id="calendar" name="calendar" class="form-control" value="<?= set_value('calendar') ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
