<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Evento no Calendário</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Evento no Calendário </p>
            <table style="border: 0px; width: 100%">
                <tr>
                    <td width="300px">Evento:</td>
                    <td><?= $this->Calendar_model->_calendar ?></td>
                </tr>
                <tr>
                    <td>Referência:</td>
                    <td><?= $this->Calendar_model->_month_reference ?>/<?= $this->Calendar_model->_year_reference ?></td>
                </tr>
                <tr>
                    <td>Data:</td>
                    <td><?= date_to_human_date($this->Calendar_model->_event_date) ?></td>
                </tr>
            </table>
        <br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>
    </body>
</html>