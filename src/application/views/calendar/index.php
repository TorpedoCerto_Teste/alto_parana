<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Calendário
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Calendário</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Calendário
                    <?php foreach ($calendar_responsibles as $calendar_responsible) : ?>
                        <button class="btn btn-<?= $colors[$calendar_responsible['pk_calendar_responsible']] ?> btn-sm"> <?= $calendar_responsible['responsible'] ?> </button>
                    <?php endforeach; ?>
                    <span class="tools pull-right">
                        <div class="btn-row">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("-1 MONTH", strtotime($this->Calendar_model->_event_date))) ?>"><i class="fa fa-backward"></i></button>
                                    <button class="btn btn-info muda_data active" type="button" date="<?= date("Y-m-01") ?>">Mês Atual</button>
                                    <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("+1 MONTH", strtotime($this->Calendar_model->_event_date))) ?>"><i class="fa fa-forward"></i></button>
                                </div>
                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>calendar/create'">
                                    <i class="fa fa-plus pull-right"></i>
                                    Novo
                                </button>
                            </div>
                        </div>
                    </span>                    
                </header>
                <div class="panel-body">
                    <form id='frm_calendar' role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>calendar" >
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="event_date" id="event_date" value="<?= $this->Calendar_model->_event_date ?>" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>
                    </form>

                    <section class="isolate-tabs">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#calendar">Calendário</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#list">Listagem</a>
                            </li>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="calendar" class="tab-pane active">
                                    <div class="fc-content" style="position: relative; min-height: 1px;">
                                        <?= $calendar ?>
                                    </div>
                                </div>
                                <div id="list" class="tab-pane">
                                    <table class="table data-table dataTable responsive-data-table">
                                        <thead>
                                            <tr>
                                                <th>Data</th>
                                                <th>Responsável</th>
                                                <th>Agente</th>
                                                <th>Descrição</th>
                                                <th>Referência</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($month_events as $month_event) : ?>
                                                <tr>
                                                    <td><?= date_to_human_date($month_event['event_date']) ?></td>
                                                    <td><button class="btn btn-xs btn-<?= $colors[$month_event['fk_calendar_responsible']] ?>"><?= $month_event['responsible'] ?></button></td>
                                                    <td><?= $month_event['agent'] ?></td>
                                                    <td><?= $month_event['calendar'] ?></td>
                                                    <td><?= $month_event['month_reference'] ?>/<?= $month_event['year_reference'] ?></td>
                                                    <td><input type="checkbox" class="chk" id="chk_l_<?=$month_event['pk_calendar']?>" value="<?=$month_event['pk_calendar']?>" <?=$month_event['status'] == 2 ? "checked" : ""?> /></td>
                                                    <td>
                                                        <a class="btn btn-info btn-xs" href="<?=base_url()?>calendar/update/<?= $month_event['pk_calendar'] ?>"><i class="fa fa-pencil"></i></a>
                                                        <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_calendar').val(<?= $month_event['pk_calendar'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                    </td>    
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>                    

                </div>
            </section>
        </div>

    </div>
</div>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>calendar/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_calendar" id="pk_calendar" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

