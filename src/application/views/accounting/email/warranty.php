<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Aporte de Garantia Financeira</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Aporte de Garantia Financeira </p>
        <div>
            <p> 
                <b>Atenção!</b> A Câmara de Comercialização de Energia Elétrica divulgou o relatório de Aporte de Garantias de seus agentes. O aporte deverá ser providenciado junto à sua conta no Banco Bradesco - Agência Trianon, conforme os dados detalhados abaixo:
            </p>
        </div>
        <br/>
        <table style="border:0px solid black; width: 100%">
            <tr>
                <td width="300px">Agente</td>
                <td><?=$accounting["agent"]?></td>
            </tr>
            <tr>
                <td width="300px">Referência</td>
                <td><?=$accounting["month"]?>/<?=$accounting["year"]?></td>
            </tr>
            <tr>
                <td width="300px">Valor</td>
                <td><?=number_format($accounting["value"],2,",","")?></td>
            </tr>
            <tr>
                <td width="300px">Data limite para aporte</td>
                <td><span style="color: red"><?= date_to_human_date($accounting["warranty_limit_date"])?></span></td>
            </tr>
        </table>
        <br/>
        <div>
            <p>É de suma importância que o depósito requerido seja realizado até a data limite. Em caso de não aporte, a CCEE poderá iniciar processo de desligamento do agente, além de aplicação de multas e penalidades.<br/>
                Em caso de qualquer dúvida, pedimos que entre em contato com urgência com nossa equipe pelo telefone <?=$this->config->item("config_phone")?> ou através do chat online.
            </p>

        </div>
        <br/>
        <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
        <div>
            <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
            <a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
        </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>

    </body>
</html>