<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Sumário da contabilização na CCEE</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Sumário da Contabilização na CCEE </p>
        <p align="justify">Prezados, encaminhamos em anexo e disponibilizamos na <a href="<?= base_url() ?>customer/accounting" target="_blank">área do cliente</a> o relatório de contabilização da CCEE, com o detalhamento dos lançamentos, conforme divulgado pela Câmara de Comercialização de Energia Elétrica - CCEE. Estes valores são resultantes das diferenças entre energia registrada e consumida (MCP), resultados retidos de meses anteriores, encargos, ajustes financeiros, entre outras cobranças e rateios realizados pela CCEE.
            <?php if ($accounting["type"] == "DEBITO") : ?>
                <br/><br/><br/><b>Neste mês, para o agente <?= $accounting["agent"] ?>, o resultado da apuração da contabilização foi um DÉBITO. </b><br/><br/><br/>O valor aportado como garantia financeira, e que está mantido em sua conta de custódia da Agência Trianon, do Banco Bradesco, poderá ser utilizado para o pagamento desta contabilização. O valor requerido abaixo deverá estar disponível, <b>integralmente</b>, até a data de <b><?= date_to_human_date($accounting["debit_date"]) ?></b>. <br/><br/><b>Atenção para caso seja necessário complementar o valor que foi aportado anteriormente!</b><br/>
                <?php if ($accounting['total'] > $accounting['value']) : ?>
                <p style="color: red">Importante: notamos que o valor do sumário é maior que o valor solicitado anteriormente para o aporte de garantia. O valor depositado como garantia financeira na data de <?= date_to_human_date($accounting["warranty_limit_date"]) ?> deverá ser complementado.</p>
            <?php endif; ?>
        <?php else : ?>
            <br/><br/><br/><b>Neste mês, para o agente <?= $accounting["agent"] ?>, o resultado da apuração da contabilização foi um CRÉDITO.</b><br/><br/>O valor será transferido para sua conta de custódia da Agência Trianon, do Banco Bradesco, na data de <?= date_to_human_date($accounting["credit_date"]) ?>. <br/>Atenção, os créditos estão sujeitos à inadimplência de mercado, e poderão ser depositados parcialmente pela CCEE.<br/>
        <?php endif; ?>
    </p>

    <h2>Dados Gerais da Contabilização</h2>

    <br/>
    <table style="border:0px; width: 100%">
        <tr>
            <td width="300px">Agente</td>
            <td><?= $accounting["agent"] ?></td>
        </tr>
        <tr>
            <td>Situação</td>
            <td><?= $accounting["type"] ?></td>
        </tr>
        <tr>
            <td>Valor (R$)</td>
            <td><?= number_format($accounting["total"], 2, ".", ",") ?></td>
        </tr>
        <tr>
            <td>Referência</td>
            <td><?= $accounting["month"] ?>/<?= $accounting["year"] ?></td>
        </tr>
        <?php if ($accounting["type"] == "DEBITO") : ?>
            <tr>
                <td>Data Limite em caso de débito</td>
                <td><?= date_to_human_date($accounting["debit_date"]) ?></td>
            </tr>
        <?php else : ?>
            <tr>
                <td>Data Depósito em caso de crédito</td>
                <td><?= date_to_human_date($accounting["credit_date"]) ?></td>
            </tr>
        <?php endif; ?>
    </table>
    <br/>

    <?php /*
    <h2>Dados Detalhados da Contabilização</h2>

    <br/>
    <table style="border:0px; width: 100%">
        <tr>
            <td width="300px">ESS (R$)</td>
            <td><?= number_format($accounting["ess"], 2, ".", ",") ?></td>
        </tr>
        <tr>
            <td>Sumário anterior (R$)</td>
            <td><?= number_format($accounting["previous_summary"], 2, ".", ",") ?></td>
        </tr>
        <tr>
            <td>MCP (R$)</td>
            <td><?= number_format($accounting["mcp"], 2, ".", ",") ?></td>
        </tr>
        <tr>
            <td>Ajustes (R$)</td>
            <td><?= number_format($accounting["adjust"], 2, ".", ",") ?></td>
        </tr>
        <tr>

    </table>
     * 
     */?>
    <br/>   



    <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?= $this->config->item("config_phone") ?> e em nosso atendimento on-line, via chat da área do cliente.</p>
    <br/>
    <br/>
    <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
    <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
        <a href="mailto:contato@altoparanaenergia.com" style="text-decoration: none; color: #999">contato@altoparanaenergia.com</a>
    </div>
    <br/>
    <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
        Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
    </div>
</body>

</html>
