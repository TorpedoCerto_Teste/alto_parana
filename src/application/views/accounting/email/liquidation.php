<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Efetivação da Liquidação Financeira da CCEE</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center">
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999">Efetivação da Liquidação Financeira da CCEE</p>
        <p align="justify">Prezados, encaminhamos em anexo e disponibilizamos na <a href="<?= base_url() ?>customer/accounting" target="_blank">área do cliente</a> o relatório de efetivação da Liquidaçao Financeira da CCEE.

            <br/>
            <br/><b>Importante:</b> O valor final abaixo deverá ser exatamento o recebido na conta de custódia do agente, na Agência Trianon, Banco Bradesco.<br/>
        </p>

        <br/><b>Inadimplência e condições de mercado:</b> Em decorrência da situação atual de judicialização das usinas que estão no MRE (Mecanismo de Realocação de Energia), os agentes credores na CCEE estão recebendo valores consideravelmente inferiores ao devido. Os impasses estão sendo alocados como inadimplência de mercado pela CCEE.<br/>
    </p>

    <br/>
    <table style="border:0px; width: 100%">
        <tr>
            <td width="300px">Agente</td>
            <td><?= $accounting["agent"] ?></td>
        </tr>
        <tr>
            <td>Referência</td>
            <td><?= $accounting["month"] ?>/<?= $accounting["year"] ?></td>
        </tr>
        <!-- Caso o valor da liquidação seja a receber: -->
        <tr>
            <td>Valor a <?= ($accounting["type"] == "CREDITO") ? "receber" : "pagar" ?> (R$)</td>
            <td><?= number_format($accounting['total'], 2, ",", ".") ?></td>
        </tr>
        <tr>
            <td>Valor <?= ($accounting["type"] == "CREDITO") ? "recebido" : "pago" ?> (R$)</td>
            <td><?= number_format($accounting['value_paid'], 2, ",", ".") ?></td>
        </tr>
    </table>
    <br/>
    <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?= $this->config->item("config_phone") ?> e em nosso atendimento on-line, via chat da área do cliente.</p>
    <br/>
    <br/>
    <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
    <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
        <a href="mailto:contato@altoparanaenergia.com" style="text-decoration: none; color: #999">contato@altoparanaenergia.com</a>
    </div>
    <br/>
    <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
        Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis.
    </div>
</body>
</html>
