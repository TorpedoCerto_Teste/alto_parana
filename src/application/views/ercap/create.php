<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Ercap
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ercap">Ercap</a></li>
            <li class="active">Lançamento</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Lançamento Ercap
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ercap/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Agente</label>
                            <div class="col-lg-6">
                                <select id="fk_agent" name="fk_agent" class="form-control select2">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $type => $agent) : ?>
                                            <optgroup label="<?= $type ?>">
                                                <?php foreach ($agent as $agt) : ?>
                                                    <option value="<?= $agt['pk_agent'] ?>" <?= set_value('fk_agent') == $agt['pk_agent'] ? "selected" : "" ?>><?= $agt['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Período</label>
                            <div class="col-lg-2 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= $i ?>" <?= set_value('month') == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year"
                                           data-bts-min="1980"
                                           data-bts-max="2050"
                                           data-bts-step="1"
                                           data-bts-decimal="0"
                                           data-bts-step-interval="1"
                                           data-bts-force-step-divisibility="round"
                                           data-bts-step-interval-delay="500"
                                           data-bts-booster="true"
                                           data-bts-boostat="10"
                                           data-bts-max-boosted-step="false"
                                           data-bts-mousewheel="true"
                                           data-bts-button-down-class="btn btn-default"
                                           data-bts-button-up-class="btn btn-default"
/>
                            </div>
                            <div class="col-lg-2 <?= form_error('value') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Valor</span>
                                <input type="text" id="value" name="value" class="form-control" value="<?= set_value('value') != "" ? set_value('value') : "" ?>" alt="valor2" />
                            </div>
                        </div>

                        <div id="div_unity" class="form-group" style="display: none">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Rateio</label>
                            <div class="col-lg-6">
                                <table class="table responsive-data-table data-table" >
                                    <thead>
                                        <tr>
                                            <th>
                                                <label class="checkbox-custom check-success">
                                                    <input type="checkbox" value="1" name="chk_all" id="chk_all" class="chk-all" /><label for="chk_all"><span class="help-block">Todas unidades</span></label>
                                                </label>
                                                Incluir no rateio
                                            </th>
                                            <th>Início no ML</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_unity">
                                        <tr>
                                            <td>
                                                <label class="checkbox-custom check-success">
                                                    <input type="checkbox" class="chk" value="1" name="chk[]" id="chk_1" /><label for="chk_1">&nbsp;</label>
                                                </label>
                                            </td>
                                            <td>01/01/2017</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
