<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contatos
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent/view/<?= $this->Contact_model->_fk_agent ?>/contatos">Contatos</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Contato
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados.
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>contact/update/<?= $this->Contact_model->_fk_agent ?>/<?= $this->Contact_model->_pk_contact ?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="name">Nome</label>
                            <div class="col-lg-10">
                                <input type="text" id="name" name="name" class="form-control" value="<?= set_value('name') != "" ? set_value('name') : $this->Contact_model->_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('surname') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="surname">Sobrenome</label>
                            <div class="col-lg-10">
                                <input type="text" id="surname" name="surname" class="form-control" value="<?= set_value('surname') != "" ? set_value('surname') : $this->Contact_model->_surname ?>" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('phone') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="phone">Telefone</label>
                            <div class="col-lg-10">
                                <input type="text" id="phone" name="phone" class="form-control" value="<?= set_value('phone') != "" ? set_value('phone') : $this->Contact_model->_phone ?>" alt="telefone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('extension') != "" ? "has-error" : ""; ?>" >
                            <label class="col-lg-2 col-sm-2 control-label" for="extension">Ramal</label>
                            <div class="col-lg-10">
                                <input type="text" id="extension" name="extension" class="form-control" value="<?= set_value('extension') != "" ? set_value('extension') : $this->Contact_model->_extension ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('mobile') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="mobile">Celular</label>
                            <div class="col-lg-10">
                                <input type="text" id="mobile" name="mobile" class="form-control" value="<?= set_value('mobile') != "" ? set_value('mobile') : $this->Contact_model->_mobile ?>" alt="telefone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('email') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="email">E-mail</label>
                            <div class="col-lg-10">
                                <input type="email" id="email" name="email" class="form-control" value="<?= set_value('email') != "" ? set_value('email') : $this->Contact_model->_email ?>">
                                <?php echo form_error('email', '<span class="help-block has-error">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('responsibility') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="responsibility">Cargo/Função</label>
                            <div class="col-lg-10">
                                <input type="text" id="responsibility" name="responsibility" class="form-control" value="<?= set_value('responsibility') != "" ? set_value('responsibility') : $this->Contact_model->_responsibility ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('birthdate') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="birthdate">Data de Nascimento</label>
                            <div class="col-lg-2">
                                <input type="text" id="birthdate" name="birthdate" class="form-control" value="<?= set_value('birthdate') != "" ? set_value(date_to_human_date('birthdate')) : date("d/m/Y", strtotime($this->Contact_model->_birthdate)) ?>" alt="date">
                            </div>
                            <div class="col-lg-8" name="div_birthdate_as_password" id="div_birthdate_as_password">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" id="birthdate_as_password" name="birthdate_as_password" <?= set_value('birthdate_as_password') == 1 ? 'checked' : '' ?>> <label for="birthdate_as_password">Usar a data de nascimento como senha (somente os números)</label>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Permitir Acesso</label>
                            <div class="col-lg-1">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" id="allow_access" name="allow_access" <?= set_value('allow_access') == 1 || $this->Contact_model->_allow_access == 1 ? 'checked' : '' ?>> <label for="allow_access">Permite que este contato acesse o painel de clientes</label>
                                </label>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('password') != "" ? "has-error" : ""; ?>" id="div_password" name="div_password" style="display: <?= set_value('allow_access') == 1 || $this->Contact_model->_allow_access == 1 ? 'block' : 'none' ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="password">Senha</label>
                            <div class="col-md-4" id="div_asterisk">
                                <input type="password" id="password" name="password" class="form-control" value="">
                                <span class="input-group-btn add-on">
                                    <button class="btn btn-default btn-xs mostrar" type="button"><i class="fa fa-eye"></i></button>
                                </span>
                                <span class="help-block"><?= $this->Contact_model->_password == "" ? "Insira uma senha de acesso" : "Deixe em branco se não quiser alterar a senha" ?></span>
                            </div>
                            <div class="col-md-4" id="div_text" style="display:none">
                                <input type="text" name="password_text" id="password_text" class="form-control" value="">
                                <span class="input-group-btn add-on">
                                    <button class="btn btn-default btn-xs mostrar" type="button"><i class="fa fa-eye-slash"></i></button>
                                </span>
                                <span class="help-block"><?= $this->Contact_model->_password == "" ? "Insira uma senha de acesso" : "Deixe em branco se não quiser alterar a senha" ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>agent/view/<?= $this->Contact_model->_fk_agent ?>/contatos'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>

