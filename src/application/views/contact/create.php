<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contatos
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent/view/<?= $this->Contact_model->_fk_agent ?>/contatos">Contatos</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Contato
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados.
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>contact/create/<?= $this->uri->segment(3) ?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Nome</label>
                            <div class="col-lg-10">
                                <input type="text" id="name" name="name" class="form-control" value="<?= set_value('name') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('surname') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Sobrenome</label>
                            <div class="col-lg-10">
                                <input type="text" id="surname" name="surname" class="form-control" value="<?= set_value('surname') ?>" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('phone') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Telefone</label>
                            <div class="col-lg-10">
                                <input type="text" id="phone" name="phone" class="form-control" value="<?= set_value('phone') ?>" alt="telefone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('extension') != "" ? "has-error" : ""; ?>" >
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Ramal</label>
                            <div class="col-lg-10">
                                <input type="text" id="extension" name="extension" class="form-control" value="<?= set_value('extension') ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('mobile') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Celular</label>
                            <div class="col-lg-10">
                                <input type="text" id="mobile" name="mobile" class="form-control" value="<?= set_value('mobile') ?>" alt="telefone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('email') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">E-mail</label>
                            <div class="col-lg-10">
                                <input type="email" id="email" name="email" class="form-control" value="<?= set_value('email') ?>">
                                <?php echo form_error('email', '<span class="help-block has-error">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('responsibility') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Cargo/Função</label>
                            <div class="col-lg-10">
                                <input type="text" id="responsibility" name="responsibility" class="form-control" value="<?= set_value('responsibility') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('birthdate') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="birthdate">Data de Nascimento</label>
                            <div class="col-lg-2">
                                <input type="text" id="birthdate" name="birthdate" class="form-control" value="<?= set_value(date_to_human_date('birthdate')) ?>" alt="date">
                            </div>
                            <div class="col-lg-8" name="div_birthdate_as_password" id="div_birthdate_as_password">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" id="birthdate_as_password" name="birthdate_as_password" <?= set_value('birthdate_as_password') == 1 ? 'checked' : '' ?>> <label for="birthdate_as_password">Usar a data de nascimento como senha (somente os números)</label>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Permitir Acesso</label>
                            <div class="col-lg-1">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" id="allow_access" name="allow_access" <?= set_value('allow_access') == 1 ? 'checked' : '' ?>> <label for="allow_access">Permite que este contato acesse o painel de clientes</label>
                                </label>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('password') != "" ? "has-error" : ""; ?>" id="div_password" name="div_password" style="display: <?= set_value('allow_access') == 1 ? 'block' : 'none' ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="password">Senha</label>
                            <div class="col-lg-10">
                                <input type="password" id="password" name="password" class="form-control" value="">
                                <span class="help-block">Insira uma senha de acesso</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>agent/view/<?= $this->Contact_model->_fk_agent ?>/contatos'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>

