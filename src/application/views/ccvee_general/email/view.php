<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Faturamento de Energia Livre</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Faturamento de Energia Livre </p>

            <p><b>Atenção!</b> Já estão disponíveis, em anexo à este e-mail e na <a href="<?=base_url()?>customer/ccvee_billing">área do cliente</a>, os arquivos referentes ao faturamento de energia no mercado livre, conforme os dados abaixo:</p>

            <table style="border:0px; width: 100%">
                <tr>
                    <td width="300px">Referência:</td>
                    <td><?= $ccvee_billing["_month"] ?>/<?= $ccvee_billing["_year"] ?></td>
                </tr>
                <tr>
                    <td>Agente:</td>
                    <td><?= $ccvee_billing['agente']['_community_name'] ?></td>
                </tr>
                <tr>
                    <td>Unidade:</td>
                    <td><?= $ccvee_billing['unity']['_community_name'] ?></td>
                </tr>
                <tr>
                    <td>Vencimento:</td>
                    <td><?= date_to_human_date($ccvee_billing["_expiration_date"]) ?></td>
                </tr>
                <tr>
                    <td>Total a pagar:</td>
                    <td>R$ <?= number_format($ccvee_billing["_total"],2,",","") ?></td>
                </tr>
            </table>
        <br/>
        <br/>
              <div style="text-align: justify"> Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?=$this->config->item("config_phone")?> e em nosso atendimento on-line, via chat da área do cliente.</div>
		<br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a>
        <br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>

    </body>
</html>