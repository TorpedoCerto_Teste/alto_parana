<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        CCVEE
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">CCVEE</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    CCVEE
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>
                        <?php if ($list) : ?> 
                        <button class="btn btn-info addon-btn m-b-10" name="btn-export" id="btn-export"><i class="fa fa-table pull-right"></i>Exportar Excel</button> 
                        <?php endif; ?>                      
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>ccvee_general/create'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: <?= (is_array($ape_statuses) && !empty($ape_statuses)) ? 'block' : 'none' ?>">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_general" id="frm_ccvee_general" name="frm_ccvee_general">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="export_excel" id="export_excel" value="false" />
                        <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="ape_statuses">Situação com a APE</label>
                                <div class="col-lg-6">
                                    <select class="form-control select2" multiple id="ape_statuses" name="ape_statuses[]" >
                                        <option value="Cliente Ativo"  <?= in_array('Cliente Ativo', $ape_statuses) ? 'selected' : '' ?>>Cliente Ativo</option>
                                        <option value="Cliente Antigo" <?= in_array('Cliente Antigo', $ape_statuses) ? 'selected' : '' ?>>Cliente Antigo</option>
                                        <option value="Em prospecção" <?= in_array('Em prospecção', $ape_statuses) ? 'selected' : '' ?>>Em prospecção</option>
                                        <option value="Em prospecção com Gestão Paralela" <?= in_array('Em prospecção com Gestão Paralela', $ape_statuses) ? 'selected' : '' ?>>Em prospecção com Gestão Paralela</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success" type="submit">Filtrar</button>
                                    <?php if (is_array($ape_statuses) && !empty($ape_statuses)) : ?>
                                        <a href="<?= base_url() ?>ccvee_general" class="btn btn-info">Resetar</a>
                                    <?php endif; ?>
                                </div>
                        </div>
                    </form>
                </div>                  
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Nº Interno</th>
                                <th>Nº Documento</th>
                                <th>Agente</th>
                                <th>Unidade</th>
                                <th>Tipo Energia</th>
                                <th>Contraparte</th>
                                <th>Operação</th>
                                <th>Início Vig.</th>
                                <th>Final Vig.</th>
                                <th>Situação com a APE</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><a href="<?=base_url()?>ccvee_general/view/<?= $item['pk_ccvee_general'] ?>">
                                            <?= substr($item['contract_type'], 0,1) ?>_<?= $item['cplp'] ?>_<?= $item['energy_type']?>.<?= $item['pk_ccvee_general'] ?>
                                        </a>
                                    </td>
                                    <td><a href="<?=base_url()?>ccvee_general/view/<?= $item['pk_ccvee_general'] ?>"><?= $item['document_number'] ?></a></td>
                                    <td><?= $item['agent'] ?></td>
                                    <td><?= $item['unity_name'] ?></td>
                                    <td><?= $item['energy_type']?></td>
                                    <td><?= $item['counterpart'] ?></td>
                                    <td><?= $item['contract_type'] ?></td>
                                    <td><?= date_to_human_date($item['energy_supply_start']) ?></td>
                                    <td><?= date_to_human_date($item['energy_supply_end']) ?></td>
                                    <td><?= $item['unity_ape_status'] ?></td>
                                    <td class="hidden-xs">
                                        <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_ccvee_general').val(<?= $item['pk_ccvee_general'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>ccvee_general/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_ccvee_general" id="pk_ccvee_general" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
