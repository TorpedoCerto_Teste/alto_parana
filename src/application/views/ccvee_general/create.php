<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo CCVEE (etapa 1)
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ccvee_general">CCVEE</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo CCVEE (etapa 1)
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_general/create"  enctype="multipart/form-data">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>unity/get_profile/" disabled=""/>
                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Cliente de Serviço</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($units_client_counterpart as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_submarket') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_submarket">Submercado</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_submarket" id="fk_submarket">
                                    <option></option>
                                    <?php foreach ($submarkets as $submarket) : ?>
                                        <option value="<?= $submarket['pk_submarket'] ?>" <?= $submarket['pk_submarket'] == $this->session->userdata('fk_submarket') ? 'selected' : '' ?>><?= $submarket['submarket'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_user">Cadastrado Por</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_user" id="fk_user">
                                    <option></option>
                                    <?php foreach ($users as $user) : ?>
                                        <option value="<?= $user['pk_user'] ?>" <?= $user['pk_user'] == $this->session->userdata('pk_user') ? 'selected' : '' ?>><?= $user['name'] . " " . $user['surname'] . " - " . $user['email'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('document_number') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="document_number">Número do documento da Contraparte</label>
                            <div class="col-lg-2">
                                <input type="text" id="document_number" name="document_number" class="form-control" value="<?= set_value('document_number') != "" ? set_value('document_number') : "" ?>" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('contract_type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="contract_type">Tipo de Contrato</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="contract_type" id="contract_type">
                                    <option></option>
                                    <option value="COMPRA" <?= set_value('contract_type') == "COMPRA" ? "SELECTED" : "" ?>>COMPRA</option>
                                    <option value="VENDA" <?= set_value('contract_type') == "VENDA" ? "SELECTED" : "" ?>>VENDA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_unity_counterpart') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity_counterpart">Contraparte</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity_counterpart" id="fk_unity_counterpart">
                                    <option></option>
                                    <?php foreach ($units_client_counterpart as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <?php if ($unit['pk_unity'] != $this->Unity_model->_pk_unity) : ?>
                                                    <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('cplp') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="cplp">Prazo</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="cplp" id="cplp">
                                    <option></option>
                                    <option value="CP" <?= set_value('cplp') == "CP" ? "SELECTED" : "" ?>>CURTO PRAZO</option>
                                    <option value="LP" <?= set_value('cplp') == "LP" ? "SELECTED" : "" ?>>LONGO PRAZO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('source') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="source">Origem</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="source" id="source">
                                    <option></option>
                                    <option value="BALCÃO" <?= set_value('source') == "BALCÃO" ? "SELECTED" : "" ?>>BALCÃO</option>
                                    <option value="BBCE" <?= set_value('source') == "BBCE" ? "SELECTED" : "" ?>>BBCE</option>
                                    <option value="RFQ" <?= set_value('source') == "RFQ" ? "SELECTED" : "" ?>>RFQ</option>
                                    <option value="BILATERAL" <?= set_value('source') == "BILATERAL" ? "SELECTED" : "" ?>>BILATERAL</option>
                                    <option value="ANTERIOR" <?= set_value('source') == "ANTERIOR" ? "SELECTED" : "" ?>>ANTERIOR</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('rfq') != "" ? "has-error" : ""; ?>" style="display:<?= set_value('source') == "RFQ" ? "block" : "none" ?>" name="div_rfq" id="div_rfq">
                            <label class="col-lg-2 col-sm-2 control-label" for="rfq">Código RFQ</label>
                            <div class="col-lg-2">
                                <input type="text" id="rfq" name="rfq" class="form-control" value="<?= set_value('rfq') != "" ? set_value('rfq') : "" ?>" alt="cpfcnpj">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_energy_type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_energy_type">Fontes de Energia</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="fk_energy_type" id="fk_energy_type">
                                    <option></option>
                                    <?php foreach ($energy_types as $energy_type) : ?>
                                        <option value="<?= $energy_type['pk_energy_type'] ?>" <?= set_value('pk_energy_type') == $energy_type['pk_energy_type'] ? 'selected' : "" ?>><?= $energy_type['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('retusd') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="retusd">Retusd (R$/MWh)</label>
                            <div class="col-lg-2">
                                <input type="text" id="retusd" name="retusd" class="form-control" value="<?= set_value('retusd') != "" ? set_value('retusd') : "" ?>" alt="valor2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Observações</label>
                            <div class="col-lg-10">
                                <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="18"><?= set_value('memo') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Arquivo</label>
                            <div class="col-lg-6">
                                <input type="file" class="form-control" name="ccvee_general_file" id="ccvee_general_file" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Continuar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>ccvee_general'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>


</div>
