<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Geral / Energia / Pagamento / Unidades / Garantias
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ccvee_general">CCVEE</a></li>
            <li class="active">Visualizar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<div id="app">
    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">

                <!--tab nav start-->
                <section class="panel">
                    <header class="panel-heading tab-dark tab-right ">
                        <ul class="nav nav-tabs pull-right">
                            <li class="<?= $this->uri->segment(4, "") == "" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#geral">
                                    <i class="fa fa-home"></i>
                                    Geral
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "energia" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#energia">
                                    <i class="fa fa-flash"></i>
                                    Energia
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "pagamentos" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#pagamentos">
                                    <i class="fa fa-money"></i>
                                    Pagamentos
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "unidades" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#unidades">
                                    <i class="fa fa-cubes"></i>
                                    Unidades
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "garantias" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#garantias">
                                    <i class="fa fa-key"></i>
                                    Garantias
                                </a>
                            </li>
                            <li class="<?= $this->uri->segment(4, "") == "faturamentos" ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#faturamentos">
                                    <i class="fa fa-sliders"></i>
                                    Faturamentos
                                </a>
                            </li>
                        </ul>
                        <span class="hidden-sm wht-color"><?= substr($this->Ccvee_general_model->_contract_type, 0, 1) ?>_<?= $this->Ccvee_general_model->_cplp ?>_<?= $this->Ccvee_general_model->_fk_energy_type ?>.<?= $this->Ccvee_general_model->_pk_ccvee_general ?> (<?= $this->Ccvee_general_model->_document_number ?>) </span>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <!--GERAL-->
                            <div id="geral" class="tab-pane <?= $this->uri->segment(4, "") == "" ? 'active' : '' ?>">
                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_general/update/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>"  enctype="multipart/form-data">
                                    <input type="hidden" name="update" id="update" value="true" />

                                    <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Cliente de Serviço</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="fk_unity" id="fk_unity" >
                                                <option></option>
                                                <?php foreach ($units as $type => $unity) : ?>
                                                    <optgroup label="<?= $type ?>">
                                                        <?php foreach ($unity as $unit) : ?>
                                                            <option value="<?= $unit['pk_unity'] ?>" <?= $this->Ccvee_general_model->_fk_unity == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('fk_submarket') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_submarket">Submercado</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="fk_submarket" id="fk_submarket">
                                                <option></option>
                                                <?php foreach ($submarkets as $submarket) : ?>
                                                    <option value="<?= $submarket['pk_submarket'] ?>" <?= $submarket['pk_submarket'] == $this->Ccvee_general_model->_fk_submarket ? 'selected' : '' ?>><?= $submarket['submarket'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_user">Cadastrado Por</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="fk_user" id="fk_user">
                                                <option></option>
                                                <?php foreach ($users as $user) : ?>
                                                    <option value="<?= $user['pk_user'] ?>" <?= $this->Ccvee_general_model->_fk_user == $this->session->userdata('pk_user') ? 'selected' : '' ?>><?= $user['name'] . " " . $user['surname'] . " - " . $user['email'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('document_number') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="document_number">Número do documento da Contraparte</label>
                                        <div class="col-lg-2">
                                            <input type="text" id="document_number" name="document_number" class="form-control" value="<?= $this->Ccvee_general_model->_document_number ?>" >
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('contract_type') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="contract_type">Tipo de Contrato</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" name="contract_type" id="contract_type" >
                                                <option></option>
                                                <option value="COMPRA" <?= $this->Ccvee_general_model->_contract_type == "COMPRA" ? "SELECTED" : "" ?>>COMPRA</option>
                                                <option value="VENDA" <?= $this->Ccvee_general_model->_contract_type == "VENDA" ? "SELECTED" : "" ?>>VENDA</option>
                                                <option value="CESSÃO" <?= $this->Ccvee_general_model->_contract_type == "CESSÃO" ? "SELECTED" : "" ?>>CESSÃO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('fk_unity_counterpart') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_unity_counterpart">Contraparte</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="fk_unity_counterpart" id="fk_unity_counterpart" >
                                                <option></option>
                                                <?php foreach ($units as $type => $unity) : ?>
                                                    <optgroup label="<?= $type ?>">
                                                        <?php foreach ($unity as $unit) : ?>
                                                            <?php if ($unit['pk_unity'] != $this->Unity_model->_pk_unity) : ?>
                                                                <option value="<?= $unit['pk_unity'] ?>" <?= $this->Ccvee_general_model->_fk_unity_counterpart == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('cplp') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="cplp">Prazo</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" name="cplp" id="cplp" >
                                                <option></option>
                                                <option value="CP" <?= $this->Ccvee_general_model->_cplp == "CP" ? "SELECTED" : "" ?>>CURTO PRAZO</option>
                                                <option value="LP" <?= $this->Ccvee_general_model->_cplp == "LP" ? "SELECTED" : "" ?>>LONGO PRAZO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('source') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="source">Origem</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" name="source" id="source" >
                                                <option></option>
                                                <option value="BALCÃO" <?= $this->Ccvee_general_model->_source == "BALCÃO" ? "SELECTED" : "" ?>>BALCÃO</option>
                                                <option value="BBCE" <?= $this->Ccvee_general_model->_source == "BBCE" ? "SELECTED" : "" ?>>BBCE</option>
                                                <option value="RFQ" <?= $this->Ccvee_general_model->_source == "RFQ" ? "SELECTED" : "" ?>>RFQ</option>
                                                <option value="BILATERAL" <?= $this->Ccvee_general_model->_source == "BILATERAL" ? "SELECTED" : "" ?>>BILATERAL</option>
                                                <option value="ANTERIOR" <?= $this->Ccvee_general_model->_source == "ANTERIOR" ? "SELECTED" : "" ?>>ANTERIOR</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('rfq') != "" ? "has-error" : ""; ?>" style="display:<?= $this->Ccvee_general_model->_source == "RFQ" ? "block" : "none" ?>" name="div_rfq" id="div_rfq">
                                        <label class="col-lg-2 col-sm-2 control-label" for="rfq">Código RFQ</label>
                                        <div class="col-lg-2">
                                            <input type="text" id="rfq" name="rfq" class="form-control" value="<?= $this->Ccvee_general_model->_rfq ?>" alt="cpfcnpj" >
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('fk_energy_type') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_energy_type">Fontes de Energia</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" name="fk_energy_type" id="fk_energy_type" >
                                                <option></option>
                                                <?php foreach ($energy_types as $energy_type) : ?>
                                                    <option value="<?= $energy_type['pk_energy_type'] ?>" <?= $this->Ccvee_general_model->_fk_energy_type == $energy_type['pk_energy_type'] ? 'selected' : "" ?>><?= $energy_type['name'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group <?= form_error('retusd') != "" ? "has-error" : ""; ?>">
                                        <label class="col-lg-2 col-sm-2 control-label" for="retusd">Retusd (R$/MWh)</label>
                                        <div class="col-lg-2">
                                            <input type="text" id="retusd" name="retusd" class="form-control" value="<?= $this->Ccvee_general_model->_retusd > 0 ? number_format($this->Ccvee_general_model->_retusd, 2, ",", "") : "" ?>" alt="valor2" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Observações</label>
                                        <div class="col-lg-10">
                                            <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="18" ><?= $this->Ccvee_general_model->_memo ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Arquivo</label>
                                        <?php if ($this->Ccvee_general_model->_ccvee_general_file) : ?>
                                            <div class="col-lg-2">
                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_general/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>/<?= $this->Ccvee_general_model->_ccvee_general_file ?>" class="btn btn-info" target="_blank"><?= $this->Ccvee_general_model->_ccvee_general_file ?></a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="ccvee_general_file" id="ccvee_general_file" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" class="btn btn-success addon-btn m-b-10" value="Salvar Alterações">
                                        </div>
                                    </div>
                                </form> 
                            </div>

                            <!--ENERGIA-->
                            <div id="energia" class="tab-pane <?= $this->uri->segment(4, "") == "energia" ? 'active' : '' ?>">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <div class="panel-body">
                                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_energy/upsert_energy/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
                                                    <input type="hidden" name="update_energy" id="update_energy" value="true" />
                                                    <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>
                                                    <input type="hidden" name="pk_ccvee_energy" id="pk_ccvee_energy" value="<?= $this->Ccvee_energy_model->_pk_ccvee_energy > 0 ? $this->Ccvee_energy_model->_pk_ccvee_energy : 0 ?>" disabled="disabled"/>
                                                    <input type="hidden" name="total_months" id="total_months" value="<?= count($merged_supply_period) ?>" />

                                                    <div class="form-group <?= form_error('supply_start') != "" || form_error('supply_end') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label">Período de Fornecimento</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                                                <input type="text" class="form-control default-date-picker" name="supply_start" id="supply_start" value="<?= date_to_human_date($this->Ccvee_energy_model->_supply_start) ?>" alt="date" >
                                                                <span class="input-group-addon">a</span>
                                                                <input type="text" class="form-control default-date-picker" name="supply_end" id="supply_end" value="<?= date_to_human_date($this->Ccvee_energy_model->_supply_end) ?>" alt="date" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('losses_type') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="losses_type">Tipos de Perdas</label>
                                                        <div class="col-lg-2">
                                                            <select name="losses_type" id="losses_type" class="form-control">
                                                                <option value="valor_unico" <?php $this->Ccvee_energy_model->_losses_type == "valor_unico" ? "selected" : "" ?> >Valor Único</option>
                                                                <option value="manual" <?php $this->Ccvee_energy_model->_losses_type == "manual" ? "selected" : "" ?>>Inserção Manual</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div name="div_losses_unique" id="div_losses_unique" class="form-group <?= form_error('losses_unique') != "" ? "has-error" : ""; ?>" style="display:<?= $this->Ccvee_energy_model->_losses_type == "valor_unico" ? "block" : "none" ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="losses_unique">% Perdas</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" class="form-controll" name="losses_unique" id="losses_unique" value="<?= number_format($this->Ccvee_energy_model->_losses, 2, ",", "") ?>" alt="valor2"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('differentiated_volume_period') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="differentiated_volume_period">Volume diferenciado por período</label>
                                                        <div class="col-lg-10">
                                                            <label class="checkbox-custom check-success">
                                                                <input type="checkbox" value="1" name="differentiated_volume_period" id="differentiated_volume_period" <?= $this->Ccvee_energy_model->_differentiated_volume_period == 1 ? "checked" : "" ?>/><label for="differentiated_volume_period">&nbsp;</label>
                                                            </label>

                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('unit_measurement') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="unit_measurement">Unidade de medida</label>
                                                        <div class="col-lg-2">
                                                            <select name="unit_measurement" id="unit_measurement" class="form-control">
                                                                <option value="MWh" <?= $this->Ccvee_energy_model->_unit_measurement == "MWh" ? "selected" : "" ?> >MWh</option>
                                                                <option value="MWm" <?= $this->Ccvee_energy_model->_unit_measurement == "MWm" ? "selected" : "" ?>>MWm</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('volume') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="volume">Volume</label>
                                                        <div class="col-lg-10" id="volumes" name="volumes">
                                                            <table class="table responsive-data-table data-table">
                                                                <thead>
                                                                    <tr>
                                                                        <?php if ($this->Ccvee_energy_model->_differentiated_volume_period == 1) : ?>
                                                                            <th>Período</th>
                                                                            <th>Previsto</th>
                                                                            <th>Sazonalizado</th>
                                                                            <th>Ajustado</th>
                                                                            <?php if ($this->Ccvee_energy_model->_losses_type == "manual") : ?>
                                                                                <th>% Perdas</th>
                                                                                <?php endif; ?>
                                                                            <th>% Atendimento</th>
                                                                            <th>Copiar</th>
                                                                        <?php else : ?>
                                                                            <th>Previsto</th>
                                                                            <th>Sazonalizado</th>
                                                                            <th>Ajustado</th>
                                                                            <th>% Atendimento</th>
                                                                        <?php endif; ?>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if (!empty($merged_supply_period)) : ?>
                                                                        <?php foreach ($merged_supply_period as $k => $sp) : ?>
                                                                            <tr>
                                                                                <?php if ($this->Ccvee_energy_model->_differentiated_volume_period == 1) : ?>
                                                                                    <td>
                                                                                        <input placeholder="" id="periodo<?= $k ?>" name="volume[<?= $k ?>][periodo]" class="form-control" value="<?= $sp['periodo'] ?>" disabled="" type="text">
                                                                                        <input placeholder="" id="period<?= $k ?>" name="volume[<?= $k ?>][period]" class="form-control" value="<?= $sp['periodo'] ?>" type="hidden">
                                                                                    </td>
                                                                                <?php endif; ?>
                                                                                <td>
                                                                                    <input placeholder="" id="forecast<?= $k ?>" name="volume[<?= $k ?>][forecast]" class="form-control" value="<?= number_format($sp['forecast'], 6, ",", "") ?>" alt="valor6" style="text-align: right;" type="text">
                                                                                </td>
                                                                                <td>
                                                                                    <input placeholder="" id="seasonal<?= $k ?>" name="volume[<?= $k ?>][seasonal]" class="form-control" value="<?= number_format($sp['seasonal'], 6, ",", "") ?>" alt="valor6" style="text-align: right;" type="text">
                                                                                </td>
                                                                                <td>
                                                                                    <input placeholder="" id="adjusted<?= $k ?>" name="volume[<?= $k ?>][adjusted]" class="form-control" value="<?= number_format($sp['adjusted'], 6, ",", "") ?>" alt="valor6" style="text-align: right;" type="text">
                                                                                </td>
                                                                                <?php if ($this->Ccvee_energy_model->_losses_type == "manual") : ?>
                                                                                    <td>
                                                                                        <input placeholder="" id="losses<?= $k ?>" name="volume[<?= $k ?>][losses]" class="form-control" value="<?= number_format($sp['losses'], 2, ",", "") ?>" alt="valor2" style="text-align: right;" type="text">
                                                                                    </td>
                                                                                <?php endif; ?>
                                                                                <td>
                                                                                    <input placeholder="" id="attendance<?= $k ?>" name="volume[<?= $k ?>][attendance]" class="form-control" value="<?= number_format($sp['attendance'], 2, ",", "") ?>" alt="valor2" style="text-align: right;" type="text">
                                                                                </td>
                                                                                <?php if ($this->Ccvee_energy_model->_differentiated_volume_period == 1) : ?>
                                                                                    <td>
                                                                                        <button data-toggle="button" class="btn btn-sm btn-default copy" value="copy<?= $k ?>" onclick="copy_values(<?= $k ?>)"><i class="fa fa-copy"></i></button>
                                                                                    </td>
                                                                                <?php endif; ?>
                                                                            </tr>
                                                                        <?php endforeach; ?>
                                                                    <?php else : ?>
                                                                        <tr>
                                                                            <td><input type="text" placeholder="" id="forecast0" name="volume[0][forecast]" class="form-control" value="" alt="valor6" ></td>
                                                                            <td><input type="text" placeholder="" id="seasonal0" name="volume[0][seasonal]" class="form-control" value="" alt="valor6" ></td>
                                                                            <td><input type="text" placeholder="" id="adjusted0" name="volume[0][adjusted]" class="form-control" value="" alt="valor6" ></td>
                                                                            <?php if ($this->Ccvee_energy_model->_losses_type == "manual") : ?>
                                                                                <td><input type="text" placeholder="" id="losses0" name="volume[0][losses]" class="form-control" value="" alt="valor2" ></td>
                                                                            <?php endif; ?>
                                                                            <td><input type="text" placeholder="" id="attendance0" name="volume[0][attendance]" class="form-control" value="" alt="valor2" ></td>
                                                                        </tr>
                                                                    <?php endif; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('discount_proinfa') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="discount_proinfa">Desconta PROINFA</label>
                                                        <div class="col-lg-1">
                                                            <label class="checkbox-custom check-success">
                                                                <input type="checkbox" value="1" name="discount_proinfa" id="discount_proinfa" <?= $this->Ccvee_energy_model->_discount_proinfa == 1 ? "checked" : "" ?>/><label for="discount_proinfa">&nbsp;</label>
                                                            </label>

                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('seasonality') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="seasonality">Sazonalidade</label>
                                                        <div class="col-lg-1">
                                                            <label class="checkbox-custom check-success">
                                                                <input type="checkbox" value="1" name="seasonality" id="seasonality" <?= $this->Ccvee_energy_model->_seasonal == 1 ? "checked" : "" ?>/><label for="seasonality">&nbsp;</label>
                                                            </label>

                                                        </div>
                                                    </div>

                                                    <div id="seasonality_div" name="seasonality_div" style="display: <?= $this->Ccvee_energy_model->_seasonal == 1 ? "block" : "none" ?>">
                                                        <div class="form-group <?= form_error('seasonal_inferior') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="seasonal_inferior">Inferior</label>
                                                            <div class="col-lg-3">
                                                                <input id="seasonal_inferior" type="text" value="<?= $this->Ccvee_energy_model->_seasonal_inferior ?>" name="seasonal_inferior" placeholder="Entre -100% e 0%">
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?= form_error('seasonal_superior') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="seasonal_superior">Superior</label>
                                                            <div class="col-lg-3">
                                                                <input id="seasonal_superior" type="text" value="<?= $this->Ccvee_energy_model->_seasonal_superior ?>" name="seasonal_superior" placeholder="Entre 0% e 100%">
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?= form_error('seasonal_limit') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="limit">Datas Limite</label>
                                                            <div class="col-lg-6" id="seasonal_limit" name="seasonal_limit">
                                                                <table class="table responsive-data-table data-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Ano</th>
                                                                            <th>Data limite para informar sazonalidade</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php if (!empty($merged_seasonal)) : ?>
                                                                            <?php foreach ($merged_seasonal as $k => $ms) : ?>
                                                                                <tr>
                                                                                    <td class="col-lg-2">
                                                                                        <input type="text"  placeholder="" id="seasonal_limit_year_show" name="seasonal_limit_year_show" class="form-control" value="<?= $ms['year'] ?>" disabled="disabled" >
                                                                                        <input type="hidden" placeholder="" id="seasonal_limit_year" name="seasonal_limit[<?= $k ?>][year]" class="form-control" value="<?= $ms['year'] ?>" >
                                                                                    </td>
                                                                                    <td class="col-lg-4">
                                                                                        <input type="text" class="form-control default-date-picker"  id="seasonal_limit_limit" name="seasonal_limit[<?= $k ?>][limit]"  alt="date" value="<?= $ms['limit'] ?>" >
                                                                                    </td>
                                                                                </tr>                                           
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </tbody>
                                                                </table>                                            

                                                                <!--conteúdo por ajax-->
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group <?= form_error('flexibility') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="flexibility">Flexibilidade</label>
                                                        <div class="col-lg-1">

                                                            <label class="checkbox-custom check-success">
                                                                <input type="checkbox" value="1" name="flexibility" id="flexibility" <?= $this->Ccvee_energy_model->_flexibility == 1 ? "checked" : "" ?>/><label for="flexibility">&nbsp;</label>
                                                            </label>


                                                        </div>
                                                    </div>

                                                    <div id="flexibility_div" name="flexibility_div" style="display: <?= $this->Ccvee_energy_model->_flexibility == 1 ? "block" : "none" ?>">
                                                        <div class="form-group <?= form_error('flexibility_inferior') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="flexibility_inferior">Inferior</label>
                                                            <div class="col-lg-3">
                                                                <input id="flexibility_inferior" type="text" value="<?= $this->Ccvee_energy_model->_flexibility_inferior ?>" name="flexibility_inferior" placeholder="Entre -100% e 0%">
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?= form_error('flexibility_superior') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="flexibility_superior">Superior</label>
                                                            <div class="col-lg-3">
                                                                <input id="flexibility_superior" type="text" value="<?= $this->Ccvee_energy_model->_flexibility_superior ?>" name="flexibility_superior" placeholder="Entre 0% e 100%">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('modulation') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="modulation">Modulação</label>
                                                        <div class="col-lg-2">
                                                            <div class="radio-custom radio-success">
                                                                <input type='radio' name='modulation_radio' id='flat' value='flat' <?= $this->Ccvee_energy_model->_modulation == "flat" || $this->Ccvee_energy_model->_modulation == "" ? "checked" : "" ?> />
                                                                <label for="flat"><b>Flat</b></label>
                                                                <input type='radio' name='modulation_radio' id='modulada' value='modulada' <?= $this->Ccvee_energy_model->_modulation == "modulada" ? "checked" : "" ?> /> 
                                                                <label for="modulada"><b>Modulada</b></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">

                                                        </div>
                                                    </div>

                                                    <div id="modulation_div" name="modulation_div" style="display: <?= $this->Ccvee_energy_model->_modulation == "modulada" ? "block" : "none" ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="">&nbsp;</label>
                                                            <div class="col-lg-10">
                                                                <table class="table responsive-data-table data-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Pesado</th>
                                                                            <th>Médio</th>
                                                                            <th>Leve</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?= form_error('modulation_inferior') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="modulation_inferior">Inferior</label>
                                                            <div class="col-lg-10">
                                                                <table class="table responsive-data-table data-table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><input class="form-control" id="modulation_heavy_inferior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_heavy_inferior ?>" name="modulation_heavy_inferior" placeholder="Entre -100% e 0%" alt="negativo"></td>
                                                                            <td><input class="form-control" id="modulation_medium_inferior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_medium_inferior ?>" name="modulation_medium_inferior" placeholder="Entre -100% e 0%" alt="negativo"></td>
                                                                            <td><input class="form-control" id="modulation_light_inferior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_light_inferior ?>" name="modulation_light_inferior" placeholder="Entre -100% e 0%" alt="negativo"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?= form_error('modulation_superior') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="modulation_superior">Superior</label>
                                                            <div class="col-lg-10">
                                                                <table class="table responsive-data-table data-table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><input class="form-control" id="modulation_heavy_superior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_heavy_superior ?>" name="modulation_heavy_superior" placeholder="Entre 0% e 100%" alt="fone"></td>
                                                                            <td><input class="form-control" id="modulation_medium_superior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_medium_superior ?>" name="modulation_medium_superior" placeholder="Entre 0% e 100%" alt="fone"></td>
                                                                            <td><input class="form-control" id="modulation_light_superior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_light_superior ?>" name="modulation_light_superior" placeholder="Entre 0% e 100%" alt="fone"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?= form_error('modulation_type_input') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="modulation_type_input">Tipo de Inserção da Modulação</label>
                                                            <div class="col-lg-2">
                                                                <select name="modulation_type_input" id="modulation_type_input" class="form-control">
                                                                    <option>Selecione</option>
                                                                    <option value="manual" <?= $this->Ccvee_energy_model->_modulation_type_input == "manual" ? "selected" : "" ?>>Manual</option>
                                                                    <option value="medição" <?= $this->Ccvee_energy_model->_modulation_type_input == "medição" ? "selected" : "" ?>>Medição</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <button class="btn btn-success" type="submit">Salvar</button>
                                                            <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>product_category'; return false;">Cancelar</button>
                                                        </div>
                                                    </div>
                                                </form> 

                                            </div>
                                        </section>
                                    </div>

                                </div>                            
                            </div>

                            <!--PAGAMENTOS-->
                            <div id="pagamentos" class="tab-pane <?= $this->uri->segment(4, "") == "pagamentos" ? 'active' : '' ?>">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <div class="panel-body">
                                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_payment/update/<?= $this->Ccvee_payment_model->_fk_ccvee_general ?>">
                                                    <input type="hidden" name="update" id="update" value="true" />
                                                    <input type="hidden" name="base_url" id="base_url" disabled="disabled" value="<?= base_url() ?>" />
                                                    <input type="hidden" name="fk_ccvee_general" id="fk_ccvee_general" disabled="disabled" value="<?= $this->Ccvee_payment_model->_fk_ccvee_general ?>"/>
                                                    <!--<input type="hidden" name="total_months" id="total_months" disabled="disabled" value="<?= $payment_prices ? count($payment_prices) : 0 ?>" />-->

                                                    <div class="form-group <?= form_error('due') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="due">Dia de vencimento</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control" id="due" name="due">
                                                                <option value="dia_util" <?= $this->Ccvee_payment_model->_due == "dia_util" ? "selected" : "" ?>>Dia útil</option>
                                                                <option value="dia_fixo" <?= $this->Ccvee_payment_model->_due == "dia_fixo" ? "selected" : "" ?>>Dia fixo</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <select class="form-control" id="due_day" name="due_day">
                                                                <?php for ($i = 1; $i <= 28; $i++) : ?>
                                                                    <option value="<?= $i ?>" <?= $this->Ccvee_payment_model->_due_day == $i ? "selected" : "" ?>><?= $i ?></option>
                                                                <?php endfor; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('due_period') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="due_period">Mês de vencimento</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control" id="due_period" name="due_period">
                                                                <option value="M" <?= $this->Ccvee_payment_model->_due_period == "M" ? "selected" : "" ?>>Mês Fornecimento</option>
                                                                <option value="MA" <?= $this->Ccvee_payment_model->_due_period == "MA" ? "selected" : "" ?>>Meses Antes</option>
                                                                <option value="MS" <?= $this->Ccvee_payment_model->_due_period == "MS" ? "selected" : "" ?>>Meses Subsequentes</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="due_month" name="due_month" class="form-control" value="<?= set_value('due_month') != "" ? set_value('due_month') : $this->Ccvee_payment_model->_due_month ?>" alt="year">
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('financial_index') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="financial_index">Índice de Reajuste</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control" id="financial_index" name="financial_index">
                                                                <option value="SEM_RE" <?= $this->Ccvee_payment_model->_financial_index == "SEM_RE" ? "selected" : "" ?>>SEM REAJUSTE</option>
                                                                <option value="IGPM" <?= $this->Ccvee_payment_model->_financial_index == "IGPM" ? "selected" : "" ?>>IGP-M</option>
                                                                <option value="IPCA" <?= $this->Ccvee_payment_model->_financial_index == "IPCA" ? "selected" : "" ?>>IPCA</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="div_financial_index" name="div_financial_index" style="display: <?=$this->Ccvee_payment_model->_financial_index == "SEM_RE" ? "none" : "block"?>">
                                                        <div class="form-group <?= form_error('index_readjustment_calculation') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="index_readjustment_calculation">Modo de Cálculo do Índice de Reajuste</label>
                                                            <div class="col-lg-4">
                                                                <select class="form-control" id="index_readjustment_calculation" name="index_readjustment_calculation">
                                                                    <option value="1" <?= $this->Ccvee_payment_model->_index_readjustment_calculation == "1" ? "selected" : "" ?>>Padrão</option>
                                                                    <option value="2" <?= $this->Ccvee_payment_model->_index_readjustment_calculation == "2" ? "selected" : "" ?>>Não realizar o reajuste se o valor ajustado for menor que o valor atual</option>
                                                                    <option value="3" <?= $this->Ccvee_payment_model->_index_readjustment_calculation == "3" ? "selected" : "" ?>>Não considerar índices negativos no cálculo</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?= form_error('database') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="database">Data base</label>
                                                            <div class="col-lg-2">
                                                                <input type="text" placeholder="" id="database" name="database" class="form-control default-date-picker" value="<?= set_value('database') != "" ? set_value('database') : date_to_human_date($this->Ccvee_payment_model->_database) ?>" alt="date">
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?= form_error('first_adjustment') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="first_adjustment">Data do Primeiro Reajuste</label>
                                                            <div class="col-lg-10">
                                                                <div class="radio-custom radio-success">
                                                                    <input type="radio"  name="first_adjustment" id="insert_date" value="0" <?= $this->Ccvee_payment_model->_adjustment_montly_manually != 1 ? "checked" : "" ?>/>
                                                                    <label for="insert_date"> Inserir Data</label>
                                                                    <br/>
                                                                    <input type="radio"  name="first_adjustment" id="insert_manually" value="1" <?= $this->Ccvee_payment_model->_adjustment_montly_manually == 1 ? "checked" : "" ?>/> 
                                                                    <label for="insert_manually"> Apontar Pontos Mensais de Reajuste Manualmente</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="div_date_adjustment" name="div_date_adjustment" class="form-group <?= form_error('date_adjustment') != "" ? "has-error" : ""; ?>" style="display: <?= $this->Ccvee_payment_model->_adjustment_montly_manually != 1 ? "block" : "none" ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="date_adjustment">Inserir Data</label>
                                                            <div class="col-lg-2">
                                                                <input type="text" placeholder="" id="date_adjustment" name="date_adjustment" class="form-control default-date-picker" value="<?= set_value('date_adjustment') != "" ? set_value('date_adjustment') : date_to_human_date($this->Ccvee_payment_model->_date_adjustment) ?>" alt="date">
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <select name="date_adjustment_frequency" id="date_adjustment_frequency" class="form-control">
                                                                    <option value="12em12" <?= $this->Ccvee_payment_model->_date_adjustment_frequency == "12em12" ? "selected" : "" ?>>12 em 12</option>
                                                                    <option value="todoJaneiro" <?= $this->Ccvee_payment_model->_date_adjustment_frequency == "todoJaneiro" ? "selected" : "" ?>>Todo Janeiro</option>
                                                                </select>
                                                            </div>
                                                        </div>                                                    
                                                    </div>



                                                    <div class="form-group <?= form_error('price_flexibility') != "" ? "has-error" : ""; ?>" >
                                                        <label class="col-lg-2 col-sm-2 control-label" for="price_flexibility">Flexibilidade de Preço</label>
                                                        <div class="col-lg-4">
                                                            <select name="price_flexibility" id="price_flexibility" class="form-control">
                                                                <option value="fixo" <?= $this->Ccvee_payment_model->_price_flexibility == "fixo" ? "selected" : "" ?>>Fixo em R$/MWh</option>
                                                                <option value="pld" <?= $this->Ccvee_payment_model->_price_flexibility == "pld" ? "selected" : "" ?>>PLD +/- Spread</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('different_price_period') != "" ? "has-error" : ""; ?>" >
                                                        <label class="col-lg-2 col-sm-2 control-label" for="different_price_period">Preço Diferenciado por Período de Consumo</label>
                                                        <div class="col-lg-10">
                                                            <div class="radio-custom radio-success">
                                                                <input type="radio" name="different_price_period" id="different_price_period1" value="0" <?= $this->Ccvee_payment_model->_different_price_period != 1 ? "checked" : "" ?>/>
                                                                <label for="different_price_period1">  Não</label>
                                                                <br/>
                                                                <input type="radio" name="different_price_period" id="different_price_period" value="1" <?= $this->Ccvee_payment_model->_different_price_period == 1 ? "checked" : "" ?>/>
                                                                <label for="different_price_period">  Sim</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- campos de preços  SE preço não for diferenciado, vai mostrar apenas 1 campo de preço, senão, vários campos -->

                                                    <!-- não diferenciado -->
                                                    <div name="div_not_different_price_period" id="div_not_different_price_period" style="display: <?= $this->Ccvee_payment_model->_different_price_period != 1 ? "block" : "none" ?>">
                                                        <div class="form-group <?= form_error('price_mwh') != "" ? "has-error" : ""; ?>">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="price_mwh">Preço em R$/MWh</label>
                                                            <div class="col-lg-2">
                                                                <input type="text" placeholder="" id="price_mwh" name="price_mwh" class="form-control" value="<?= set_value('price_mwh') != "" ? set_value('price_mwh') : number_format($this->Ccvee_payment_model->_price_mwh, 4, ",", "") ?>" alt="valor4">
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <!--diferenciado-->
                                                    <div name="div_different_price_period" id="div_different_price_period" style="display: <?= $this->Ccvee_payment_model->_different_price_period == 1 ? "block" : "none" ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 col-sm-2 control-label" for="payment_prices">Preço em R$/MWh</label>
                                                            <div class="col-lg-10" name="table_payment_prices" id="table_payment_prices">
                                                                <table class="table responsive-data-table data-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <td>Período</td>
                                                                            <td>Previsto/Base</td>
                                                                            <td>Corrigido/Reajustado</td>
                                                                            <td>Ajustado Manualmente</td>
                                                                            <td>Previsão de Preço Ajustado</td>
                                                                            <td>Apontamento de Mês Reajuste</td>
                                                                            <td>Copiar</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php if ($payment_prices) : ?>
                                                                            <?php foreach ($payment_prices as $k => $payment_price) : ?>
                                                                                <tr>
                                                                                    <td><input type="text" placeholder="" id="period<?= $k ?>" name="payment_prices[<?= $k ?>][period]" class="form-control" value="<?= date("m/Y", strtotime($payment_price['period'])) ?>"></td>
                                                                                    <td><input type="text" placeholder="" id="provided_base<?= $k ?>" name="payment_prices[<?= $k ?>][provided_base]" class="form-control" value="<?= number_format($payment_price['provided_base'], 4, ",", "") ?>" alt="valor4"></td>
                                                                                    <td><input type="text" placeholder="" id="fixed<?= $k ?>" name="payment_prices[<?= $k ?>][fixed]" class="form-control" value="<?= number_format($payment_price['fixed'], 4, ",", "") ?>" alt="valor4"></td>
                                                                                    <td><input type="text" placeholder="" id="manual_fixed<?= $k ?>" name="payment_prices[<?= $k ?>][manual_fixed]" class="form-control" value="<?= number_format($payment_price['manual_fixed'], 4, ",", "") ?>" alt="valor4"></td>
                                                                                    <td><input type="text" placeholder="" id="provided_fixed<?= $k ?>" name="payment_prices[<?= $k ?>][provided_fixed]" class="form-control" value="<?= number_format($payment_price['provided_fixed'], 4, ",", "") ?>" alt="valor4"></td>
                                                                                    <td><input type="checkbox" placeholder="" id="point_monthly<?= $k ?>" name="payment_prices[<?= $k ?>][point_monthly]" class="form-control" value="1" <?= $payment_price['point_monthly'] == 1 ? "checked" : "" ?>></td>
                                                                                    <td><button data-toggle="button" class="btn btn-sm btn-default copy" value="copy<?= $k ?>" onclick="copy_values_payment(<?= $k ?>)"><i class="fa fa-copy"></i></button></td>
                                                                                </tr>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </tbody>
                                                                </table>                                        
                                                            </div>
                                                        </div>

                                                    </div>



                                                    <div class="form-group">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <button class="btn btn-success" type="submit">Salvar</button>
                                                        </div>
                                                    </div>
                                                </form> 
                                            </div>
                                        </section>
                                    </div>

                                </div>                            
                            </div>

                            <!--UNIDADES-->
                            <div id="unidades" class="tab-pane <?= $this->uri->segment(4, "") == "unidades" ? 'active' : '' ?>">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel-body">
                                            <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_unity/create/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
                                                <input type="hidden" name="create" id="create" value="true" />
                                                <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled" />
                                                <input type="hidden" name="pk_ccvee_general" id="pk_ccvee_general" value="<?= $this->Ccvee_general_model->_pk_ccvee_general ?>" disabled="disabled" />

                                                <?php if (!$list_units) : ?>
                                                    <div class="form-group <?= form_error('unity_apportionment') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="unity_apportionment">Rateio</label>
                                                        <div class="col-lg-6">
                                                            <select class="form-control" name="unity_apportionment" id="unity_apportionment">
                                                                <option value="CONSUMO" <?= $this->Ccvee_general_model->_unity_apportionment == "CONSUMO" ? 'selected' : '' ?>>CONSUMO</option>
                                                                <option value="MONTANTE" <?= $this->Ccvee_general_model->_unity_apportionment == "MONTANTE" ? 'selected' : '' ?>>MONTANTE</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('unity_differentiated_validate') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="unity_differentiated_validate">Vigências Diferenciadas por Unidade</label>
                                                        <div class="col-lg-1">
                                                            <label class="checkbox-custom check-success">
                                                                <input type="checkbox" id="unity_differentiated_validate" name="unity_differentiated_validate" class="form-control" value="1" <?= $this->Ccvee_general_model->_unity_differentiated_validate == "1" ? "checked" : "" ?> ><label for="unity_differentiated_validate">&nbsp;</label>
                                                            </label>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>


                                                <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                                                    <div class="col-lg-6">
                                                        <select class="form-control select2" name="fk_unity" id="fk_unity">
                                                            <option></option>
                                                            <?php foreach ($units2 as $type => $unity) : ?>
                                                                <optgroup label="<?= $type ?>">
                                                                    <?php foreach ($unity as $unit) : ?>
                                                                        <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['agent_company_name'] . " - " . $unit['community_name'] ?></option>
                                                                    <?php endforeach; ?>
                                                                </optgroup>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="div_volumn_mwm" class="form-group <?= form_error('volumn_mwm') != "" ? "has-error" : ""; ?>" style="display: <?= $this->Ccvee_general_model->_unity_apportionment == "CONSUMO" ? "none" : "block" ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="volumn_mwm">Volume MW Médio</label>
                                                    <div class="col-lg-2">
                                                        <input type="text" id="volumn_mwm" name="volumn_mwm" class="form-control" value="" alt="valor6" >
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('contract_cliqccee') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="contract_cliqccee">Número Contrato CLIQCCEE</label>
                                                    <div class="col-lg-2">
                                                        <input type="text" id="contract_cliqccee" name="contract_cliqccee" class="form-control" alt="cpfcnpj" >
                                                    </div>
                                                </div>
                                                <div id="div_vicence" class="form-group" style="display: <?= $this->Ccvee_general_model->_unity_differentiated_validate == "0" ? "none" : "block" ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label">Vigência diferenciada</label>
                                                    <div class="col-md-4">
                                                        <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                                            <span class="input-group-addon">a</span>
                                                            <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button class="btn btn-success" type="submit">Adicionar</button>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>                                    
                                    </div>

                                </div>   

                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <?php if (!$list_units) : ?>
                                                <div class="alert alert-warning fade in">
                                                    <button type="button" class="close close-sm" data-dismiss="alert">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    <strong>Atenção!</strong> Nenhuma unidade encontrada. 
                                                </div>
                                            <?php else : ?>
                                                <table class="table responsive-data-table data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Agente/Unidade</th>
                                                            <?php if ($this->Ccvee_general_model->_unity_apportionment == "MONTANTE") : ?>
                                                                <th>Volume MW Médio</th>
                                                            <?php endif; ?>
                                                            <th># Contrato CLIQCCEE</th>
                                                            <?php if ($this->Ccvee_general_model->_unity_differentiated_validate == 1) : ?>
                                                                <th>Data Início</th>
                                                                <th>Data Fim</th>
                                                            <?php endif; ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($list_units as $item) : ?>
                                                            <tr>
                                                                <td><?= $item['agent_community_name'] . " - " . $item['unity_community_name'] ?></td>
                                                                <?php if ($this->Ccvee_general_model->_unity_apportionment == "MONTANTE") : ?>
                                                                    <td><?= number_format($item['volumn_mwm'], 6, ",", "") ?></td>
                                                                <?php endif; ?>
                                                                <td><?= $item['contract_cliqccee'] ?></td>
                                                                <?php if ($this->Ccvee_general_model->_unity_differentiated_validate == 1) : ?>
                                                                    <td><?= date_to_human_date($item['start_date']) ?></td>
                                                                    <td><?= date_to_human_date($item['end_date']) ?></td>
                                                                <?php endif; ?>
                                                                <td class="hidden-xs">
                                                                    <a class="btn btn-danger btn-xs" href='#delete_unity' onclick="$('#pk_ccvee_unity').val(<?= $item['pk_ccvee_unity'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php endif; ?>
                                        </section>
                                    </div>

                                </div>

                            </div>

                            <!--GARANTIAS-->
                            <div id="garantias" class="tab-pane <?= $this->uri->segment(4, "") == "garantias" ? 'active' : '' ?>">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <div class="panel-body">
                                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_warranty/create/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
                                                    <input type="hidden" name="create" id="create" value="true" />


                                                    <div class="form-group <?= form_error('fk_modality_warranty') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_modality_warranty">Modalidade de Garantia</label>
                                                        <div class="col-lg-4">
                                                            <select class="form-control select2" id="fk_modality_warranty" name="fk_modality_warranty">
                                                                <option></option>
                                                                <?php if ($modality_warranties) : ?>
                                                                    <?php foreach ($modality_warranties as $modality_warranty) : ?>
                                                                        <option value="<?= $modality_warranty['pk_modality_warranty'] ?>"><?= $modality_warranty['modality'] ?></option>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('ccvee_warranty') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') ?>" alt="year">
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('deadline_status') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="deadline_status">Status</label>
                                                        <div class="col-lg-4">
                                                            <select class="form-control" id="deadline_status" name="deadline_status">
                                                                <option value="pendente">PENDENTE</option>
                                                                <option value="apresentada">APRESENTADA</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('customer_communication_status') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="customer_communication_status">Status comunicação ao cliente</label>
                                                        <div class="col-lg-4">
                                                            <select class="form-control" id="customer_communication_status" name="customer_communication_status">
                                                                <option value="comunicada">COMUNICADA</option>
                                                                <option value="nao_comunicada">NÃO COMUNICADA</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('start_date') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="start_date">Vigência</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                                                <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="" alt="date" >
                                                                <span class="input-group-addon">a</span>
                                                                <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="" alt="date" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('deadline_contract_presentation') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="deadline_contract_presentation">Data limite apresentação</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="deadline_contract_presentation" name="deadline_contract_presentation" class="form-control default-date-picker" value="<?= set_value('deadline_contract_presentation') ?>" alt="date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('presentation_date') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="presentation_date">Data de apresentação</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="presentation_date" name="presentation_date" class="form-control default-date-picker" value="<?= set_value('presentation_date') ?>" alt="date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('billing_cycles') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="billing_cycles">Ciclos de Pagamento</label>
                                                        <div class="col-lg-2">
                                                            <select id="billing_cycles" name="billing_cycles" class="form-control">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <button class="btn btn-success" type="submit">Salvar</button>
                                                        </div>
                                                    </div>
                                                </form> 


                                                <?php if (!$list_warranties) : ?>
                                                    <div class="alert alert-warning fade in">
                                                        <button type="button" class="close close-sm" data-dismiss="alert">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                                    </div>
                                                <?php else : ?>
                                                    <table class="table responsive-data-table data-table">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    Modalidade
                                                                </th>
                                                                <th>
                                                                    Ano
                                                                </th>
                                                                <th>
                                                                    Status prazo
                                                                </th>
                                                                <th>
                                                                    Status comunicação ao cliente
                                                                </th>
                                                                <th>
                                                                    Data inicial
                                                                </th>
                                                                <th>
                                                                    Data final
                                                                </th>
                                                                <th>
                                                                    Apresentação do contrato
                                                                </th>
                                                                <th>
                                                                    Data de apresentação
                                                                </th>
                                                                <th>
                                                                    Ciclos
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($list_warranties as $item) : ?>
                                                                <tr>
                                                                    <td><?= $item['modality'] ?></td>
                                                                    <td><?= $item['year'] ?></td>
                                                                    <td><?= $item['year'] != "" ? mb_strtoupper($item['deadline_status'], "UTF-8") : "" ?></td>
                                                                    <td><?= $item['year'] != "" ? mb_strtoupper($item['customer_communication_status'], "UTF-8") : ""  ?></td>
                                                                    <td><?= $item['year'] != "" ? date_to_human_date($item['start_date']) : ""  ?></td>
                                                                    <td><?= $item['year'] != "" ? date_to_human_date($item['end_date']) : ""  ?></td>
                                                                    <td><?= $item['year'] != "" ? date_to_human_date($item['deadline_contract_presentation']) : ""  ?></td>
                                                                    <td><?= $item['year'] != "" ? date_to_human_date($item['presentation_date']) : ""  ?></td>
                                                                    <td><?= $item['year'] != "" ? $item['billing_cycles'] : ""  ?></td>
                                                                    <td class="hidden-xs">
                                                                        <a class="btn btn-danger btn-xs" href='#delete_warranty' onclick="$('#pk_ccvee_warranty').val(<?= $item['pk_ccvee_warranty'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>


                                            </div>
                                        </section>


                                    </div>


                                </div>

                            </div>

                            <!--FATURAMENTOS-->
                            <div id="faturamentos" class="tab-pane <?= $this->uri->segment(4, "") == "faturamentos" ? 'active' : '' ?>">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <div class="panel-body">
                                                <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_billing/create/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>" enctype="multipart/form-data">
                                                    <input type="hidden" name="create" id="create" value="true" />
                                                    <input type="hidden" name="fk_agent" id="fk_agent" value="<?= $this->Ccvee_general_model->_fk_agent?>" />
                                                    
                                                    <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= date("Y") ?>" alt="year">
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                                                        <div class="col-lg-2">
                                                            <select id="month" name="month" class="form-control">
                                                                <?php for ($i = 1; $i <=12; $i++) : ?>
                                                                    <option value="<?= str_pad($i, 2, 0, STR_PAD_LEFT)?>" <?= date('m') == $i ? "selected" : "" ?>><?= retorna_mes($i)?></option>
                                                                <?php endfor; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                                                        <div class="col-lg-4">
                                                            <select class="form-control select2" name="fk_unity" id="fk_unity">
                                                                <option></option>
                                                                <?php foreach ($list_units as $unit) : ?>
                                                                    <option value="<?= $unit['fk_unity'] ?>" <?= set_value('pk_unity') == $unit['fk_unity'] ? 'selected' : "" ?>><?= $unit['unity_community_name'] ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Data de vencimento</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control default-date-picker" value="<?= set_value('expiration_date') ?>" alt="date">
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('total') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="total">Total</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="total" name="total" class="form-control" value="<?= set_value('total') ?>" alt="valor2">
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('payment_status') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="payment_status">Status do pagamento</label>
                                                        <div class="col-lg-4">
                                                            <select id="payment_status" name="payment_status" class="form-control">
                                                                <option value="programado" >PROGRAMADO</option>
                                                                <option value="nao_programado" >NÃO PROGRAMADO</option>
                                                                <option value="quitado" >QUITADO</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('payment_date') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="payment_date">Data de pagamento</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" placeholder="" id="payment_date" name="payment_date" class="form-control default-date-picker" value="<?= set_value('payment_date') ?>" alt="date">
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('invoice_type') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="invoice_type">Tipo de fatura</label>
                                                        <div class="col-lg-4">
                                                            <select id="invoice_type" name="invoice_type" class="form-control">
                                                                <option value="debito">DÉBITO</option>
                                                                <option value="credito">CRÉDITO</option>
                                                                <option value="glosa">GLOSA</option>
                                                                <option value="cancelamento">CANCELAMENTO</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('payment_slip') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="payment_slip">Boleto de pagamento</label>
                                                        <div class="col-lg-4">
                                                            <input type="file" placeholder="" id="payment_slip" name="payment_slip" class="form-control" value="<?= set_value('payment_slip') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('invoice') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="invoice">Nota Fiscal</label>
                                                        <div class="col-lg-4">
                                                            <input type="file" placeholder="" id="invoice" name="invoice" class="form-control" value="<?= set_value('invoice') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?= form_error('xml') != "" ? "has-error" : ""; ?>">
                                                        <label class="col-lg-2 col-sm-2 control-label" for="xml">XML</label>
                                                        <div class="col-lg-4">
                                                            <input type="file" placeholder="" id="xml" name="xml" class="form-control" value="<?= set_value('xml') ?>">
                                                        </div>
                                                    </div>                        

                                                    <div class="form-group">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <button class="btn btn-success" type="submit">Salvar</button>
                                                        </div>
                                                    </div>
                                                </form> 
                                            </div>
                                            <div class="wrapper">
                                                <?php if (!$list_billings) : ?>
                                                    <div class="alert alert-warning fade in">
                                                        <button type="button" class="close close-sm" data-dismiss="alert">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                                    </div>
                                                <?php else : ?>
                                                    <table class="table responsive-data-table data-table faturamento">
                                                        <thead>
                                                            <tr>
                                                                <th>Ano</th>
                                                                <th>Mês</th>
                                                                <th>Unidade</th>
                                                                <th>Data de vencimento</th>
                                                                <th>Total</th>
                                                                <th>Status do pagamento</th>
                                                                <th>Data de pagamento</th>
                                                                <th></th>
                                                                <th>Tipo de fatura</th>
                                                                <th>Boleto</th>
                                                                <th>NFe</th>
                                                                <th>XML</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($list_billings) : ?>
                                                                <?php foreach ($list_billings as $item) : ?>
                                                                    <tr>
                                                                        <td><?= $item['year'] ?></td>
                                                                        <td><?= strtoupper(retorna_mes($item['month'])) ?></td>
                                                                        <td><?= $item['unity_community_name'] ?></td>
                                                                        <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                                                        <td><?= number_format($item['total'], 2, ",", "") ?></td>
                                                                        <td><?= strtoupper($item['payment_status']) ?></td>
                                                                        <td><?= date_to_human_date($item['payment_date']) ?></td>
                                                                        <td>
                                                                            <?php if ($item['payment_status'] != "quitado" && preg_replace("/([^\d]*)/", "", $item['expiration_date']) < preg_replace("/([^\d]*)/", "", $item['payment_date'])) : ?>
                                                                                <span class="label label-danger">ATRASADO</span>
                                                                            <?php else: ?>
                                                                                <span class="label label-success">EM DIA</span>
                                                                            <?php endif; ?>
                                                                        </td>
                                                                        <td><?= strtoupper($item['invoice_type']) ?></td>
                                                                        <td>
                                                                            <?php if ($item['payment_slip'] !== "") : ?>
                                                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_billing/<?= $item['pk_ccvee_billing'] ?>/<?= $item['payment_slip'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                                            <?php endif; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if ($item['invoice'] !== "") : ?>
                                                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_billing/<?= $item['pk_ccvee_billing'] ?>/<?= $item['invoice'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                                            <?php endif; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if ($item['xml'] !== "") : ?>
                                                                                <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_billing/<?= $item['pk_ccvee_billing'] ?>/<?= $item['xml'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                                            <?php endif; ?>
                                                                        </td>
                                                                        <td class="hidden-xs">
                                                                            <a class="btn btn-danger btn-xs" href='#delete_billing' onclick="$('#pk_ccvee_billing').val(<?= $item['pk_ccvee_billing'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                                            <a class="btn btn-info btn-xs" href='#sendmail' onclick="$('#pk_ccvee_billing_sendmail').val(<?= $item['pk_ccvee_billing'] ?>)" data-toggle="modal"><i class="fa fa-envelope "></i></a>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--tab nav start-->

            </div>

        </div>
    </div>


    <!-- Modal Deletar Unidade -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_unity" class="modal fade">
        <form method="POST" action="<?= base_url() ?>ccvee_unity/delete/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
            <input type="hidden" name="delete" id="delete" value="true" />
            <input type="hidden" name="pk_ccvee_unity" id="pk_ccvee_unity" value="0" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Deseja deletar este registro?</h4>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-danger" type="submit">Deletar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->


    <!-- Modal  Deletar Garantias -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_warranty" class="modal fade">
        <form method="POST" action="<?= base_url() ?>ccvee_warranty/delete/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
            <input type="hidden" name="delete" id="delete" value="true" />
            <input type="hidden" name="pk_ccvee_warranty" id="pk_ccvee_warranty" value="0" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Deseja deletar este registro?</h4>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-danger" type="submit">Deletar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->


    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_billing" class="modal fade">
        <form method="POST" action="<?= base_url() ?>ccvee_billing/delete/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
            <input type="hidden" name="delete" id="delete" value="true" />
            <input type="hidden" name="pk_ccvee_billing" id="pk_ccvee_billing" value="0" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Deseja deletar este registro?</h4>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-danger" type="submit">Deletar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->

    <!-- Modal SENDMAIL -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sendmail" class="modal fade">
        <form method="POST" action="<?= base_url() ?>ccvee_general/sendmail/<?= $this->Ccvee_general_model->_pk_ccvee_general ?>">
            <input type="hidden" name="sendmail" id="sendmail" value="true" />
            <input type="hidden" name="pk_ccvee_billing_sendmail" id="pk_ccvee_billing_sendmail" value="0" />
            <input type="hidden" name="fk_agent_sendmail" id="fk_agent_sendmail" value="<?= $this->Ccvee_general_model->_fk_agent?>" />
            <input type="hidden" name="pk_ccvee_general_sendmail" id="pk_ccvee_general_sendmail" value="<?= $this->Ccvee_general_model->_pk_ccvee_general?>" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Notificar por email</h4>
                    </div>
                    <div class="modal-body">
                        <label class="checkbox-custom check-success">
                            <input type="checkbox" name="test" id="test" value="test"/>
                            <label for="test">Teste</label>
                        </label>

                        <div class="clearfix"></div>
                        <div class="form-group hidden" id="div_test_email">
                            <label class="col-lg-2 col-sm-2 control-label" for="test_email">Email de teste</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="test_email" id="test_email" />
                            </div>
                        </div>
                        
                        <div id="email_list">
                            <div class="tools pull-right" id="group_selector">
                                <input type="hidden" ref="controller_name" value="<?= $this->router->fetch_class() ?>" />
                                <input type="hidden" ref="fk_agent" value="<?= $this->Ccvee_general_model->_fk_agent?>" />
                                <input type="hidden" ref="base_url" value="<?= base_url() ?>" />
                                
                                <select class="form-control" name="fk_group_controller" ref="selectedGroup" v-model="selectedGroup">
                                    <option v-for="item in groupsList" :key="item.pk_group" :value="item.pk_group">
                                        {{item.group_short_name}}
                                    </option>
                                </select>
                            </div>

                            <table class="table table-responsive table-hover">
                                <thead>
                                    <tr>
                                        <th>TODOS</th>
                                        <th>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="all" name="all" id="all" value=""/><label for="all">&nbsp;</label>
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody v-if="contacts.length > 0">
                                    <tr v-for="contact in contacts" :key="contact.pk_contact">
                                        <td>{{ contact.name }} {{ contact.surname }} <br> {{ contact.email }}</td>
                                        <td>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="chk_contact" :name="'contacts[]'" :id="'contact_' + contact.pk_contact" :value="contact.pk_contact" :checked="contact.selected"/>
                                                <label :for="'contact_' + contact.pk_contact">&nbsp;</label>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody v-else>
                                    <tr>
                                        <td colspan="2">
                                            <div class="alert alert-danger fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Não existem contatos cadastrados para este agente. 
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->
</div>
