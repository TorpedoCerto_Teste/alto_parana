<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Unidades
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Unidades</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Unidades
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>unity/create'">
                            <i class="fa fa-plus pull-right"></i>
                            Novo
                        </button>
                    </span>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>
                                    Unidade
                                </th>
                                <th>
                                    Submercado
                                </th>
                                <th>
                                    Perfil
                                </th>
                                <th>
                                    Tipo Perfil
                                </th>
                                <th>
                                    Agente
                                </th>
                                <th>
                                    Tipo Agente
                                </th>
                                <th>
                                    Opções
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['company_name'] ?></td>
                                    <td><?= $item['submarket'] ?></td>
                                    <td><?= $item['profile'] ?></td>
                                    <td><?= $item['profile_type'] ?></td>
                                    <td><?= $item['agent_company_name'] ?></td>
                                    <td><?= $item['agent_type'] ?></td>
                                    <td class="hidden-xs">
                                        <a data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips" data-original-title="Editar" href = '<?= base_url() ?>unity/update/<?= $item['pk_unity'] ?>'><i class="fa fa-pencil"></i></a>
                                        <a data-placement="top" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" data-original-title="Deletar" href='#delete' onclick="$('#pk_unity').val(<?= $item['pk_unity'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                        <a data-placement="top" data-toggle="tooltip" class="btn btn-primary btn-xs tooltips" data-original-title="Unidades de Medição" href = '<?= base_url() ?>gauge/index/<?= $item['pk_unity'] ?>'><i class="fa fa-clock-o"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>unity/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_unity" id="pk_unity" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
