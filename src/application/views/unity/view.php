<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Unidades / Contatos / Pontos de Medição / Arquivos
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>agent/view/<?= $this->Unity_model->_fk_agent ?>/unidades">Agente</a></li>
            <li class="active">Unidades</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">

            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-dark tab-right ">
                    <ul class="nav nav-tabs pull-right">
                        <li class="<?= $this->uri->segment(5, "") == "" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#unidade">
                                <i class="fa fa-cubes"></i>
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(5, "") == "contatos" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#contatos">
                                <i class="fa fa-users"></i>
                                Contatos
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(5, "") == "pontos_medicao" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#pontos_medicao">
                                <i class="fa fa-tachometer"></i>
                                Pontos de Medição
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(5, "") == "demanda" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#demanda">
                                <i class="fa fa-bolt"></i>
                                Demandas
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(5, "") == "proinfa" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#proinfa">
                                <i class="fa fa-calendar"></i>
                                Proinfa
                            </a>
                        </li>
                        <li class="<?= $this->uri->segment(5, "") == "arquivos" ? 'active' : '' ?>">
                            <a data-toggle="tab" href="#arquivos">
                                <i class="fa fa-file"></i>
                                Arquivos
                            </a>
                        </li>
                    </ul>
                    <span class="hidden-sm wht-color"><a href="<?= base_url() ?>agent/view/<?= $this->Agent_model->_pk_agent ?>/unidades"><?= $this->Agent_model->_community_name ?></a> - <?= $this->Unity_model->_community_name ?> 
                        <?php if ($this->Unity_model->_status == $this->Unity_model->_status_active) : ?>
                            <span class="label label-success">Ativo</span></span>
                    <?php else : ?>
                        <span class="label label-warning">Inativo</span></span>
                    <?php endif; ?>
                </header>
                <div class="panel-body">
                    <div class="tab-content">

                        <!--UNIDADE-->
                        <div id="unidade" class="tab-pane <?= $this->uri->segment(5, "") == "" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Unidade
                                        </header>
                                        <div class="panel-body">
                                            <form role="form" class="form-horizontal" method="POST" action="javascript:void(0)">
                                                <div class="form-group <?= form_error('community_name') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="community_name">Nome da Unidade</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Nome da Unidade" id="community_name" name="community_name" class="form-control" value="<?= set_value('community_name') != "" ? set_value('community_name') : $this->Unity_model->_community_name ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('company_name') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="company_name">Empresa</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Nome da Empresa" id="company_name" name="company_name" class="form-control" value="<?= set_value('company_name') != "" ? set_value('company_name') : $this->Unity_model->_company_name ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('cnpj') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="cnpj">CNPJ</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="CNPJ" id="cnpj" name="cnpj" class="form-control" value="<?= set_value('cnpj') != "" ? set_value('cnpj') : str_pad($this->Unity_model->_cnpj, 14, 0, STR_PAD_LEFT) ?>" alt="cnpj">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('ie') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="ie">Inscrição Estadual</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Inscrição Estadual" id="ie" name="ie" class="form-control" value="<?= set_value('ie') != "" ? set_value('ie') : $this->Unity_model->_ie ?>" >
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('fk_agent_power_distributor') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label">Agente Distribuidor de Energia</label>
                                                    <div class="col-lg-10">
                                                        <select disabled="" class="form-control select2-allow-clear" id="fk_agent_power_distributor" name="fk_agent_power_distributor">
                                                            <option></option>
                                                            <?php if ($agents_distrib) : ?>
                                                                <?php foreach ($agents_distrib as $agent) : ?>
                                                                    <option value="<?= $agent['pk_agent'] ?>" <?= set_value('fk_agent_power_distributor') == $agent['pk_agent'] || $this->Unity_model->_fk_agent_power_distributor == $agent['pk_agent'] ? "selected" : "" ?>><?= $agent['community_name'] ?></option>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('uc_number') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="uc_number">Número UC</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Número UC" id="uc_number" name="uc_number" class="form-control" value="<?= set_value('uc_number') != "" ? set_value('uc_number') : $this->Unity_model->_uc_number ?>" >
                                                    </div>
                                                </div>   
                                                <div class="form-group <?= form_error('ape_status') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label">Situação com a APE</label>
                                                    <div class="col-lg-10">
                                                        <input disabled type="text" id="ape_status" name="ape_status" class="form-control" value="<?= $this->Unity_model->_ape_status ?>" >
                                                    </div>
                                                </div>                                                
                                                <div class="form-group <?= form_error('fk_profile') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label">Perfil</label>
                                                    <div class="col-lg-10">
                                                        <select disabled="" class="form-control select2-allow-clear" id="fk_profile" name="fk_profile">
                                                            <option></option>
                                                            <?php if ($profiles) : ?>
                                                                <?php foreach ($profiles as $profile) : ?>
                                                                    <option value="<?= $profile['pk_profile'] ?>" <?= set_value('fk_profile') == $profile['pk_profile'] || $this->Unity_model->_fk_profile == $profile['pk_profile'] ? "selected" : "" ?>><?= $profile['profile'] . " - " . $profile['type'] . " - " . $profile['submarket'] ?></option>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('communion_acronym') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="communion_acronym">Sigla Comunhão</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Sigla Comunhão" id="communion_acronym" name="communion_acronym" class="form-control" value="<?= set_value('communion_acronym') != "" ? set_value('communion_acronym') : $this->Unity_model->_communion_acronym ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group <?= form_error('siga') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="siga">Código SIGA</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Código SIGA na CCEE" id="siga" name="siga" class="form-control" value="<?= set_value('siga') != "" ? set_value('siga') : $this->Unity_model->_siga ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('postal_code') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="postal_code">CEP</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="CEP" id="postal_code" name="postal_code" class="form-control" value="<?= set_value('postal_code') != "" ? set_value('postal_code') : $this->Unity_model->_postal_code ?>" alt="cep">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('address') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="address">Endereço</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Endereço" id="address" name="address" class="form-control" value="<?= set_value('address') != "" ? set_value('address') : $this->Unity_model->_address ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('number') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="number">Número</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Número" id="number" name="number" class="form-control" value="<?= set_value('number') != "" ? set_value('number') : $this->Unity_model->_number ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('complement') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="complement">Complemento</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Complemento" id="complement" name="complement" class="form-control" value="<?= set_value('complement') != "" ? set_value('complement') : $this->Unity_model->_complement ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('district') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="district">Bairro</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Bairro" id="district" name="district" class="form-control" value="<?= set_value('district') != "" ? set_value('district') : $this->Unity_model->_district ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('city') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="city">Cidade</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="text" placeholder="Cidade" id="city" name="city" class="form-control" value="<?= set_value('city') != "" ? set_value('city') : $this->Unity_model->_city ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                                                    <div class="col-lg-10">
                                                        <select disabled="" id="state" name="state" class="form-control">
                                                            <option
                                                                value="AC" <?= set_value('state') == 'AC' || $this->Unity_model->_state == 'AC' ? 'selected' : '' ?>>
                                                                Acre
                                                            </option>
                                                            <option
                                                                value="AL" <?= set_value('state') == 'AL' || $this->Unity_model->_state == 'AL' ? 'selected' : '' ?>>
                                                                Alagoas
                                                            </option>
                                                            <option
                                                                value="AP" <?= set_value('state') == 'AP' || $this->Unity_model->_state == 'AP' ? 'selected' : '' ?>>
                                                                Amapá
                                                            </option>
                                                            <option
                                                                value="AM" <?= set_value('state') == 'AM' || $this->Unity_model->_state == 'AM' ? 'selected' : '' ?>>
                                                                Amazonas
                                                            </option>
                                                            <option
                                                                value="BA" <?= set_value('state') == 'BA' || $this->Unity_model->_state == 'BA' ? 'selected' : '' ?>>
                                                                Bahia
                                                            </option>
                                                            <option
                                                                value="CE" <?= set_value('state') == 'CE' || $this->Unity_model->_state == 'CE' ? 'selected' : '' ?>>
                                                                Ceará
                                                            </option>
                                                            <option
                                                                value="DF" <?= set_value('state') == 'DF' || $this->Unity_model->_state == 'DF' ? 'selected' : '' ?>>
                                                                Distrito Federal
                                                            </option>
                                                            <option
                                                                value="ES" <?= set_value('state') == 'ES' || $this->Unity_model->_state == 'ES' ? 'selected' : '' ?>>
                                                                Espirito Santo
                                                            </option>
                                                            <option
                                                                value="GO" <?= set_value('state') == 'GO' || $this->Unity_model->_state == 'GO' ? 'selected' : '' ?>>
                                                                Goiás
                                                            </option>
                                                            <option
                                                                value="MA" <?= set_value('state') == 'MA' || $this->Unity_model->_state == 'MA' ? 'selected' : '' ?>>
                                                                Maranhão
                                                            </option>
                                                            <option
                                                                value="MS" <?= set_value('state') == 'MS' || $this->Unity_model->_state == 'MS' ? 'selected' : '' ?>>
                                                                Mato Grosso do Sul
                                                            </option>
                                                            <option
                                                                value="MT" <?= set_value('state') == 'MT' || $this->Unity_model->_state == 'MT' ? 'selected' : '' ?>>
                                                                Mato Grosso
                                                            </option>
                                                            <option
                                                                value="MG" <?= set_value('state') == 'MG' || $this->Unity_model->_state == 'MG' ? 'selected' : '' ?>>
                                                                Minas Gerais
                                                            </option>
                                                            <option
                                                                value="PA" <?= set_value('state') == 'PA' || $this->Unity_model->_state == 'PA' ? 'selected' : '' ?>>
                                                                Pará
                                                            </option>
                                                            <option
                                                                value="PB" <?= set_value('state') == 'PB' || $this->Unity_model->_state == 'PB' ? 'selected' : '' ?>>
                                                                Paraíba
                                                            </option>
                                                            <option
                                                                value="PR" <?= set_value('state') == 'PR' || $this->Unity_model->_state == 'PR' ? 'selected' : '' ?>>
                                                                Paraná
                                                            </option>
                                                            <option
                                                                value="PE" <?= set_value('state') == 'PE' || $this->Unity_model->_state == 'PE' ? 'selected' : '' ?>>
                                                                Pernambuco
                                                            </option>
                                                            <option
                                                                value="PI" <?= set_value('state') == 'PI' || $this->Unity_model->_state == 'PI' ? 'selected' : '' ?>>
                                                                Piauí
                                                            </option>
                                                            <option
                                                                value="RJ" <?= set_value('state') == 'RJ' || $this->Unity_model->_state == 'RJ' ? 'selected' : '' ?>>
                                                                Rio de Janeiro
                                                            </option>
                                                            <option
                                                                value="RN" <?= set_value('state') == 'RN' || $this->Unity_model->_state == 'RN' ? 'selected' : '' ?>>
                                                                Rio Grande do Norte
                                                            </option>
                                                            <option
                                                                value="RS" <?= set_value('state') == 'RS' || $this->Unity_model->_state == 'RS' ? 'selected' : '' ?>>
                                                                Rio Grande do Sul
                                                            </option>
                                                            <option
                                                                value="RO" <?= set_value('state') == 'RO' || $this->Unity_model->_state == 'RO' ? 'selected' : '' ?>>
                                                                Rondônia
                                                            </option>
                                                            <option
                                                                value="RR" <?= set_value('state') == 'RR' || $this->Unity_model->_state == 'RR' ? 'selected' : '' ?>>
                                                                Roraima
                                                            </option>
                                                            <option
                                                                value="SC" <?= set_value('state') == 'SC' || $this->Unity_model->_state == 'SC' ? 'selected' : '' ?>>
                                                                Santa Catarina
                                                            </option>
                                                            <option
                                                                value="SP" <?= set_value('state') == 'SP' || $this->Unity_model->_state == 'SP' ? 'selected' : '' ?>>
                                                                São Paulo
                                                            </option>
                                                            <option
                                                                value="SE" <?= set_value('state') == 'SE' || $this->Unity_model->_state == 'SE' ? 'selected' : '' ?>>
                                                                Sergipe
                                                            </option>
                                                            <option
                                                                value="TO" <?= set_value('state') == 'TO' || $this->Unity_model->_state == 'TO' ? 'selected' : '' ?>>
                                                                Tocantins
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 col-sm-2 control-label">Observações</label>
                                                    <div class="col-lg-10">
                                                        <textarea disabled="" class="wysihtml5 form-control" id="memo" name="memo" rows="18"><?= set_value('memo') != "" ? set_value('memo') : $this->Unity_model->_memo ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 col-sm-2 control-label">Icms</label>
                                                    <div class="col-lg-2">
                                                        <input disabled="" id="icms" type="text" name="icms" value="<?= set_value('icms') != "" ? set_value('icms') : number_format($this->Unity_model->_icms, 2, '.', '') ?>" alt="valor">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input disabled="" placeholder="Observações de ICMS" class="form-control" id="memo_icms" type="text" name="memo_icms" value="<?= set_value('memo_icms') != "" ? set_value('memo_icms') : $this->Unity_model->_memo_icms ?>" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 col-sm-2 control-label">Atividade</label>
                                                    <div class="col-lg-10">
                                                        <input disabled="" type="radio" name="iCheck" class="iCheck-flat-blue" id="activity" value="INDUSTRIAL" <?= set_value('activity') == 'INDUSTRIAL' || $this->Unity_model->_activity == 'INDUSTRIAL' ? 'checked' : '' ?>><label class="control-label">&nbsp;INDUSTRIAL </label><br/>
                                                        <input disabled="" type="radio" name="iCheck" class="iCheck-flat-blue" id="activity" value="COMERCIAL" <?= set_value('activity') == 'COMERCIAL' || $this->Unity_model->_activity == 'COMERCIAL' ? 'checked' : '' ?>><label class="control-label">&nbsp;COMERCIAL</label>
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('credit_icms') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="credit_icms">Laudo ICMS Crédito</label>
                                                    <div class="col-lg-2">
                                                        <input disabled="" type="text"  id="credit_icms" name="credit_icms"  value="<?= set_value('credit_icms') != "" ? set_value('credit_icms') : number_format($this->Unity_model->_credit_icms, 2, '.', '') ?>" alt="valor">
                                                    </div>
                                                </div>
                                                <div class="form-group <?= form_error('free_market_entry') != "" ? "has-error" : ""; ?>">
                                                    <label class="col-lg-2 col-sm-2 control-label" for="free_market_entry">Entrada no Mercado Livre de Energia</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="free_market_entry" name="free_market_entry"  value="<?= date_to_human_date($this->Unity_model->_free_market_entry) ?>" alt="date" disabled="disabled">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button class="btn btn-success" onclick="window.location.href = '<?= base_url() ?>unity/update/<?= $this->Unity_model->_fk_agent ?>/<?= $this->Unity_model->_pk_unity ?>'; return false;">Editar</button>
                                                    </div>
                                                </div>
                                            </form> 

                                        </div>
                                    </section>
                                </div>

                            </div>

                        </div>

                        <!--CONTATOS-->
                        <div id="contatos" class="tab-pane <?= $this->uri->segment(5, "") == "contatos" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Contatos
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>unity_contact/create/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Adicionar Contato à Unidade
                                                </button>
                                            </span>
                                        </header>
                                        <?php if (!$unity_contacts) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Adicionar Contato à Unidade" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Contato</th>
                                                        <th>Telefone</th>
                                                        <th>Ramal</th>
                                                        <th>Celular</th>
                                                        <th>Email</th>
                                                        <th>Cargo</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($unity_contacts as $item) : ?>
                                                        <tr>
                                                            <td><?= $item['name'] ?> <?= $item['surname'] ?></td>
                                                            <td><?= $item['phone'] ?></td>
                                                            <td><?= $item['extension'] ?></td>
                                                            <td><?= $item['mobile'] ?></td>
                                                            <td><?= $item['email'] ?></td>
                                                            <td><?= $item['responsibility'] ?></td>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-danger btn-xs tooltips" href='#delete_unidade_contato' onclick="$('#pk_unity_contact').val(<?= $item['pk_unity_contact'] ?>); " data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Remover este contato para a unidade"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>                            
                        </div>

                        <!--PONTOS DE MEDIÇÃO-->
                        <div id="pontos_medicao" class="tab-pane <?= $this->uri->segment(5, "") == "pontos_medicao" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Pontos de Medição
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>gauge/create/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo
                                                </button>
                                            </span>
                                        </header>
                                        <?php if (!$gauges) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>ID Telemetria</th>
                                                        <th>
                                                            Opções
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($gauges as $item) : ?>
                                                        <tr>
                                                            <td><?= $item['gauge'] ?></td>
                                                            <td><?= $item['telemetry_code'] ?></td>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-success btn-xs" href = '<?= base_url() ?>gauge/update/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/<?= $item['pk_gauge'] ?>'><i class="fa fa-pencil"></i></a>
                                                                <a class="btn btn-danger btn-xs" href='#delete_gauge' onclick="$('#pk_gauge').val(<?= $item['pk_gauge'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>

                        </div>

                        <!--DEMANDA-->
                        <div id="demanda" class="tab-pane <?= $this->uri->segment(5, "") == "demanda" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Demandas Contratadas
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>demand/create/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo
                                                </button>
                                            </span>
                                        </header>

                                        <?php if (!$demands) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Data Inicial</th>
                                                        <th>Data Final</th>
                                                        <th>Tensão</th>
                                                        <th>Mod.Tarifária</th>
                                                        <th>Demanda Ponta</th>
                                                        <th>Dem. Fora Ponta</th>
                                                        <th>Situação</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($demands as $item) : ?>
                                                        <tr>
                                                            <td><?= date_to_human_date($item['start_date']) ?></td>
                                                            <td><?= $item['current'] ? 'Atual' : date_to_human_date($item['end_date']) ?></td>
                                                            <td><?= $item['tension'] ?></td>
                                                            <td><span class="label label-<?= $item['tariff_mode'] == 'VERDE' ? 'success' : 'info' ?>"><?= $item['tariff_mode'] ?></span></td>
                                                            <td><?= $item['end_demand'] ?></td>
                                                            <td><?= $item['demand_tip_out'] ?></td>
                                                            <td>
                                                                <?php if ($item['captive_situation'] == $this->Demand_model->_status_active) : ?>
                                                                    Cativo
                                                                <?php else : ?>
                                                                    Livre
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-info btn-xs tooltips" href = '<?= base_url() ?>demand/update/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/<?= $item['pk_demand'] ?>' data-placement="top" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                                                <!--<a class="btn btn-warning btn-xs tooltips" href = '<?= base_url() ?>demand/status/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/<?= $item['pk_demand'] ?>' data-placement="top" data-toggle="tooltip" data-original-title="<?= $item['status'] == $this->Demand_model->_status_active ? 'Inativar' : 'Ativar' ?>"><i class="fa fa-warning"></i></a>-->
                                                                <a class="btn btn-danger btn-xs tooltips" href='#delete_demanda' onclick="$('#pk_demand').val(<?= $item['pk_demand'] ?>)" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>

                        </div>

                        <!--PROINFA-->
                        <div id="proinfa" class="tab-pane <?= $this->uri->segment(5, "") == "proinfa" ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading ">
                                            Proinfa
                                            <span class="tools pull-right">
                                                <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>proinfa/create/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>'">
                                                    <i class="fa fa-plus pull-right"></i>
                                                    Novo
                                                </button>
                                            </span>
                                        </header>
                                        <?php if (!$proinfas) : ?>
                                            <div class="alert alert-warning fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                                            </div>
                                        <?php else : ?>
                                            <table class="table responsive-data-table data-table">
                                                <thead>
                                                    <tr>
                                                        <th>Ano</th>
                                                        <th>Mês</th>
                                                        <th>MWh</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($proinfas as $item) : ?>
                                                        <tr>
                                                            <td><?= $item['year'] ?></td>
                                                            <td><?= $item['month'] ?></td>
                                                            <td><?= number_format($item['value_mwh'],3,",","") ?></td>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-success btn-xs" href = '<?= base_url() ?>proinfa/update/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/<?= $item['pk_proinfa'] ?>'><i class="fa fa-pencil"></i></a>
                                                                <a class="btn btn-danger btn-xs" href='#delete_proinfa' onclick="$('#pk_proinfa').val(<?= $item['pk_proinfa'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php endif; ?>
                                    </section>
                                </div>

                            </div>

                        </div>


                        <!--ARQUIVOS-->
                        <div id="arquivos" class="tab-pane <?= $this->uri->segment(5, "") == "arquivos" ? 'active' : '' ?>">
                            <div class="panel-body">
                                <form id="my-awesome-dropzone" action="<?= base_url() ?>unity/dropzone/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>" class="dropzone"></form>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <?php if (!$upload) : ?>
                                                <div class="alert alert-warning fade in">
                                                    <button type="button" class="close close-sm" data-dismiss="alert">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    <strong>Atenção!</strong> Nenhum arquivo! Para inserir, arraste e solte na área pontilhada.
                                                </div>
                                            <?php else : ?>
                                                <table class="table responsive-data-table data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Arquivo</th>
                                                            <th>Opções</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($upload as $item) : ?>
                                                            <tr>
                                                                <td><a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/agent/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/<?= $item ?>" target="_blank"><?= $item ?></td>
                                                                <td class="hidden-xs">
                                                                    <a class="btn btn-danger btn-xs tooltips" href='#delete_file' onclick="$('#file_to_delete').val('<?= $item ?>')" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o "></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php endif; ?>

                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--tab nav start-->

        </div>

    </div>
</div>


<!-- Modal delete perfil -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_unidade_contato" class="modal fade">
    <form method="POST" action="<?= base_url() ?>unity_contact/delete/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_unity_contact" id="pk_unity_contact" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal delete file -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_file" class="modal fade">
    <form method="POST" action="<?= base_url() ?>unity/delete_file/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="file_to_delete" id="file_to_delete" value="0" />
        <input type="hidden" name="pk_agent" id="pk_agent" value="<?= $this->Agent_model->_pk_agent ?>" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este arquivo?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal  perfil -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_demanda" class="modal fade">
    <form method="POST" action="<?= base_url() ?>demand/delete/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_demand" id="pk_demand" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal  proinfa -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_proinfa" class="modal fade">
    <form method="POST" action="<?= base_url() ?>proinfa/delete/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_proinfa" id="pk_proinfa" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->

<!-- Modal gauge -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_gauge" class="modal fade">
    <form method="POST" action="<?= base_url() ?>gauge/delete/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_gauge" id="pk_gauge" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
