<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Unidades
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity">Unidades</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Unidade
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <span name="base_url" id="base_url" style="display: none"><?=base_url()?></span>
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>unity/update/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <div class="form-group <?= form_error('community_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="community_name">Nome da Unidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Nome da Unidade" id="community_name" name="community_name" class="form-control" value="<?= set_value('community_name') != "" ? set_value('community_name') : $this->Unity_model->_community_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('company_name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="company_name">Empresa</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Nome da Empresa" id="company_name" name="company_name" class="form-control" value="<?= set_value('company_name') != "" ? set_value('company_name') : $this->Unity_model->_company_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('cnpj') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="cnpj">CNPJ</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CNPJ" id="cnpj" name="cnpj" class="form-control" value="<?= set_value('cnpj') != "" ? set_value('cnpj') : str_pad($this->Unity_model->_cnpj, 14, 0, STR_PAD_LEFT) ?>" alt="cnpj">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ie') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="ie">Inscrição Estadual</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Inscrição Estadual" id="ie" name="ie" class="form-control" value="<?= set_value('ie') != "" ? set_value('ie') : $this->Unity_model->_ie ?>" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_agent_power_distributor') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Agente Distribuidor de Energia</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_agent_power_distributor" name="fk_agent_power_distributor">
                                    <option></option>
                                    <?php if ($agents_distrib) : ?>
                                        <?php foreach ($agents_distrib as $agent) : ?>
                                            <option value="<?= $agent['pk_agent'] ?>" <?= set_value('fk_agent_power_distributor') == $agent['pk_agent']|| $this->Unity_model->_fk_agent_power_distributor == $agent['pk_agent']  ? "selected" : "" ?>><?= $agent['community_name'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('uc_number') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="uc_number">Número UC</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Número UC" id="uc_number" name="uc_number" class="form-control" value="<?= set_value('uc_number') != "" ? set_value('uc_number') : $this->Unity_model->_uc_number ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ape_status') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Situação com a APE</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="ape_status" name="ape_status">
                                    <option></option>
                                    <option value="Cliente Ativo" <?= set_value('ape_status') == "Cliente Ativo" || $this->Unity_model->_ape_status == "Cliente Ativo" ? "selected" : "" ?>>Cliente Ativo</option>
                                    <option value="Cliente Antigo" <?= set_value('ape_status') == "Cliente Antigo" || $this->Unity_model->_ape_status == "Cliente Antigo" ? "selected" : "" ?>>Cliente Antigo</option>
                                    <option value="Em Prospecção" <?= set_value('ape_status') == "Em Prospecção" || $this->Unity_model->_ape_status == "Em Prospecção" ? "selected" : "" ?>>Em Prospecção</option>
                                    <option value="Em Prospecção com Gestão Paralela" <?= set_value('ape_status') == "Em Prospecção com Gestão Paralela" || $this->Unity_model->_ape_status == "Em Prospecção com Gestão Paralela" ? "selected" : "" ?>>Em Prospecção com Gestão Paralela</option>
                                </select>
                            </div>
                        </div>                                                
                        <div class="form-group <?= form_error('fk_profile') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Perfil</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_profile" name="fk_profile">
                                    <option></option>
                                    <?php if ($profiles) : ?>
                                        <?php foreach ($profiles as $profile) : ?>
                                            <option value="<?= $profile['pk_profile'] ?>" <?= set_value('fk_profile') == $profile['pk_profile'] || $this->Unity_model->_fk_profile == $profile['pk_profile'] ? "selected" : "" ?>><?= $profile['profile']." - ".$profile['type']." - ".$profile['submarket'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('communion_acronym') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="communion_acronym">Sigla Comunhão</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Sigla Comunhão" id="communion_acronym" name="communion_acronym" class="form-control" value="<?= set_value('communion_acronym') != "" ? set_value('communion_acronym') : $this->Unity_model->_communion_acronym ?>" />
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('siga') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="siga">Código SIGA</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Código SIGA na CCEE" id="siga" name="siga" class="form-control" value="<?= set_value('siga') != "" ? set_value('siga') : $this->Unity_model->_siga ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('postal_code') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="postal_code">CEP</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="CEP" id="postal_code" name="postal_code" class="form-control" value="<?= set_value('postal_code') != "" ? set_value('postal_code') : $this->Unity_model->_postal_code ?>" alt="cep">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('address') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="address">Endereço</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Endereço" id="address" name="address" class="form-control" value="<?= set_value('address') != "" ? set_value('address') : $this->Unity_model->_address ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('number') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="number">Número</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Número" id="number" name="number" class="form-control" value="<?= set_value('number') != "" ? set_value('number') : $this->Unity_model->_number ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('complement') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="complement">Complemento</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Complemento" id="complement" name="complement" class="form-control" value="<?= set_value('complement') != "" ? set_value('complement') : $this->Unity_model->_complement ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('district') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="district">Bairro</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Bairro" id="district" name="district" class="form-control" value="<?= set_value('district') != "" ? set_value('district') : $this->Unity_model->_district ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('city') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="city">Cidade</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Cidade" id="city" name="city" class="form-control" value="<?= set_value('city') != "" ? set_value('city') : $this->Unity_model->_city ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                            <div class="col-lg-10">
                                <select id="state" name="state" class="form-control">
                                    <option
                                        value="AC" <?= set_value('state') == 'AC' || $this->Unity_model->_state == 'AC' ? 'selected' : '' ?>>
                                        Acre
                                    </option>
                                    <option
                                        value="AL" <?= set_value('state') == 'AL' || $this->Unity_model->_state == 'AL' ? 'selected' : '' ?>>
                                        Alagoas
                                    </option>
                                    <option
                                        value="AP" <?= set_value('state') == 'AP' || $this->Unity_model->_state == 'AP' ? 'selected' : '' ?>>
                                        Amapá
                                    </option>
                                    <option
                                        value="AM" <?= set_value('state') == 'AM' || $this->Unity_model->_state == 'AM' ? 'selected' : '' ?>>
                                        Amazonas
                                    </option>
                                    <option
                                        value="BA" <?= set_value('state') == 'BA' || $this->Unity_model->_state == 'BA' ? 'selected' : '' ?>>
                                        Bahia
                                    </option>
                                    <option
                                        value="CE" <?= set_value('state') == 'CE' || $this->Unity_model->_state == 'CE'  ? 'selected' : '' ?>>
                                        Ceará
                                    </option>
                                    <option
                                        value="DF" <?= set_value('state') == 'DF' || $this->Unity_model->_state == 'DF'  ? 'selected' : '' ?>>
                                        Distrito Federal
                                    </option>
                                    <option
                                        value="ES" <?= set_value('state') == 'ES' || $this->Unity_model->_state == 'ES'  ? 'selected' : '' ?>>
                                        Espirito Santo
                                    </option>
                                    <option
                                        value="GO" <?= set_value('state') == 'GO' || $this->Unity_model->_state == 'GO'  ? 'selected' : '' ?>>
                                        Goiás
                                    </option>
                                    <option
                                        value="MA" <?= set_value('state') == 'MA' || $this->Unity_model->_state == 'MA'  ? 'selected' : '' ?>>
                                        Maranhão
                                    </option>
                                    <option
                                        value="MS" <?= set_value('state') == 'MS' || $this->Unity_model->_state == 'MS'  ? 'selected' : '' ?>>
                                        Mato Grosso do Sul
                                    </option>
                                    <option
                                        value="MT" <?= set_value('state') == 'MT' || $this->Unity_model->_state == 'MT'  ? 'selected' : '' ?>>
                                        Mato Grosso
                                    </option>
                                    <option
                                        value="MG" <?= set_value('state') == 'MG' || $this->Unity_model->_state == 'MG'  ? 'selected' : '' ?>>
                                        Minas Gerais
                                    </option>
                                    <option
                                        value="PA" <?= set_value('state') == 'PA' || $this->Unity_model->_state == 'PA'  ? 'selected' : '' ?>>
                                        Pará
                                    </option>
                                    <option
                                        value="PB" <?= set_value('state') == 'PB' || $this->Unity_model->_state == 'PB'  ? 'selected' : '' ?>>
                                        Paraíba
                                    </option>
                                    <option
                                        value="PR" <?= set_value('state') == 'PR' || $this->Unity_model->_state == 'PR'  ? 'selected' : '' ?>>
                                        Paraná
                                    </option>
                                    <option
                                        value="PE" <?= set_value('state') == 'PE' || $this->Unity_model->_state == 'PE'  ? 'selected' : '' ?>>
                                        Pernambuco
                                    </option>
                                    <option
                                        value="PI" <?= set_value('state') == 'PI' || $this->Unity_model->_state == 'PI'  ? 'selected' : '' ?>>
                                        Piauí
                                    </option>
                                    <option
                                        value="RJ" <?= set_value('state') == 'RJ' || $this->Unity_model->_state == 'RJ'  ? 'selected' : '' ?>>
                                        Rio de Janeiro
                                    </option>
                                    <option
                                        value="RN" <?= set_value('state') == 'RN' || $this->Unity_model->_state == 'RN'  ? 'selected' : '' ?>>
                                        Rio Grande do Norte
                                    </option>
                                    <option
                                        value="RS" <?= set_value('state') == 'RS' || $this->Unity_model->_state == 'RS'  ? 'selected' : '' ?>>
                                        Rio Grande do Sul
                                    </option>
                                    <option
                                        value="RO" <?= set_value('state') == 'RO' || $this->Unity_model->_state == 'RO'  ? 'selected' : '' ?>>
                                        Rondônia
                                    </option>
                                    <option
                                        value="RR" <?= set_value('state') == 'RR' || $this->Unity_model->_state == 'RR'  ? 'selected' : '' ?>>
                                        Roraima
                                    </option>
                                    <option
                                        value="SC" <?= set_value('state') == 'SC' || $this->Unity_model->_state == 'SC'  ? 'selected' : '' ?>>
                                        Santa Catarina
                                    </option>
                                    <option
                                        value="SP" <?= set_value('state') == 'SP' || $this->Unity_model->_state == 'SP'  ? 'selected' : '' ?>>
                                        São Paulo
                                    </option>
                                    <option
                                        value="SE" <?= set_value('state') == 'SE' || $this->Unity_model->_state == 'SE'  ? 'selected' : '' ?>>
                                        Sergipe
                                    </option>
                                    <option
                                        value="TO" <?= set_value('state') == 'TO' || $this->Unity_model->_state == 'TO'  ? 'selected' : '' ?>>
                                        Tocantins
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Observações</label>
                            <div class="col-lg-10">
                                <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="18"><?=set_value('memo') != "" ? set_value('memo') : $this->Unity_model->_memo ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Icms</label>
                            <div class="col-lg-2">
                                <input id="icms" type="text" name="icms" value="<?=set_value('icms') != "" ? set_value('icms') : number_format($this->Unity_model->_icms, 2, '.', '') ?>" alt="valor">
                            </div>
                            <div class="col-lg-8">
                                <input placeholder="Observações de ICMS" class="form-control" id="memo_icms" type="text" name="memo_icms" value="<?=set_value('memo_icms') != "" ? set_value('memo_icms') : $this->Unity_model->_memo_icms ?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Atividade</label>
                            <div class="col-lg-10">
                                <input type="radio" name="iCheck" class="iCheck-flat-blue" id="activity" value="INDUSTRIAL" <?=set_value('activity') == 'INDUSTRIAL' || $this->Unity_model->_activity == 'INDUSTRIAL' ? 'checked' : '' ?>><label class="control-label">&nbsp;INDUSTRIAL </label><br/>
                                <input type="radio" name="iCheck" class="iCheck-flat-blue" id="activity" value="COMERCIAL" <?=set_value('activity') == 'COMERCIAL' || $this->Unity_model->_activity == 'COMERCIAL' ? 'checked' : '' ?>><label class="control-label">&nbsp;COMERCIAL</label>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('credit_icms') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="credit_icms">Laudo ICMS Crédito</label>
                            <div class="col-lg-2">
                                <input type="text"  id="credit_icms" name="credit_icms"  value="<?= set_value('credit_icms') != "" ? set_value('credit_icms') : number_format($this->Unity_model->_credit_icms, 2, '.', '') ?>" alt="valor">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('free_market_entry') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="free_market_entry">Entrada no Mercado Livre de Energia</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control default-date-picker" id="free_market_entry" name="free_market_entry"  value="<?= set_value('free_market_entry') != "" ? set_value('free_market_entry') : date_to_human_date($this->Unity_model->_free_market_entry) ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>unity/view/<?=$this->Unity_model->_fk_agent?>/<?=$this->Unity_model->_pk_unity?>'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
