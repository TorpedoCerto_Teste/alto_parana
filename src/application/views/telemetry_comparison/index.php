<!-- page head start-->
<div class="page-head">
    <h3>
        Comparação de Telemetrias
    </h3>
    <div class="state-information">
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Filtros
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>telemetry_comparison" id="frm_telemetry_comparison" name="frm_telemetry_comparison">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>
                        <input type="hidden" name="export_excel" id="export_excel" value="false" />

                        <div class="form-group">
                            <div class="col-lg-2 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control select2">
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?= date("m", strtotime($date)) == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= date("Y", strtotime($date)) ?>" alt="year"
                                       data-bts-min="2010"
                                       data-bts-max="<?= date("Y") ?>"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                            <div class="col-lg-4">
                                <span class="help-block">Agente</span>
                                <select id="fk_agent" name="fk_agent" class="form-control select2">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $type => $agent) : ?>
                                            <optgroup label="<?= $type ?>">
                                                <?php foreach ($agent as $agt) : ?>
                                                    <option value="<?= $agt['pk_agent'] ?>" <?= set_value('fk_agent') == $agt['pk_agent'] ? "selected" : "" ?>><?= $agt['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <span class="help-block">Medidor</span>
                                <select id="fk_gauge" name="fk_gauge" class="form-control select2">
                                    <option></option>
                                    <?php if ($gauges) : ?>
                                        <?php foreach ($gauges as $gauge) : ?>
                                            <option value="<?= $gauge['pk_gauge'] ?>" <?= set_value('fk_gauge') == $gauge['pk_gauge'] ? "selected" : "" ?>><?= $gauge['internal_name'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div> 

    <?php if (isset($comparison)) : ?>
        <?php if ($comparison) : ?>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Resultados
                            <span class="tools pull-right">
                                <button class="btn btn-info addon-btn" name="btn-export" id="btn-export"><i class="fa fa-table"></i>Exportar Excel</button>
                            </span>
                        </header>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Way2 Valor</th>
                                        <th>Way2 Status</th>
                                        <th>SCDE Valor</th>
                                        <th>SCDE Status</th>
                                        <th>Diferença</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $total_way2 = 0;
                                    $total_scde = 0;
                                    ?>
                                    <?php foreach ($comparison as $dia => $horas) : ?>
                                        <?php foreach ($horas as $hora => $resultado) : ?>
                                                <?php 
                                                $total_way2 += $resultado['way2']['value'];
                                                $total_scde += $resultado['scde']['active_power_consumption'];
                                                ?>
                                            <tr>
                                                <td><?= $dia ?>/<?=$this->input->post("month")?>/<?=$this->input->post("year")?> <?=$hora?>:00:00</td>
                                                <td style="color: <?=$resultado['way2']['quality'] == 0 ? "green" : "red"?>"><?=number_format($resultado['way2']['value'], 2, ",", "")?></td>
                                                <td style="color: <?=$resultado['way2']['quality'] == 0 ? "green" : "red"?>"><?=$resultado['way2']['quality'] == 0 ? "OK" : "ERRO"?></td>
                                                <td style="color: <?=$resultado['scde']['reason_situation'] == "Consistido" ? "green" : "red"?>"><?=number_format($resultado['scde']['active_power_consumption'], 2, ",", "")?></td>
                                                <td style="color: <?=$resultado['scde']['reason_situation'] == "Consistido" ? "green" : "red"?>"><?=$resultado['scde']['reason_situation'] == "Consistido" ? "OK" : "ERRO"?></td>
                                                <td style="color: <?=$resultado['way2']['value'] == $resultado['scde']['active_power_consumption'] ? "green" : "red"?>"><?=$resultado['way2']['value'] == $resultado['scde']['active_power_consumption'] ? "IGUAL" : "DIFERENTE"?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td><?= number_format($total_way2, 2, ",", "")?></td>
                                        <td></td>
                                        <td><?= number_format($total_scde, 2, ",", "")?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>                            
                        </div>
                    </section>
                </div>
            </div>
        <?php else : ?>
            <div class="alert alert-danger fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <strong>Atenção!</strong> Sem dados suficientes para o período selecionado. 
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <!--body wrapper end-->





</div>
<!-- body content end-->

