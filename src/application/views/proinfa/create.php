<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo/Editar Proinfa
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity/view/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>/proinfa">Proinfa</a></li>
            <li class="active">Novo/Editar Proinfa</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo/Editar Proinfa (R$/MWh)
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>proinfa/create/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>proinfa/ajax/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>" disabled=""/>
                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date('Y') ?>" alt='year'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_1') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_1">Janeiro</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_1" name="value_mwh_1" class="form-control" value="<?= set_value('value_mwh_1') != "" ? set_value('value_mwh_1') : number_format($proinfas[1]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_2') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_2">Fevereiro</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_2" name="value_mwh_2" class="form-control" value="<?= set_value('value_mwh_2') != "" ? set_value('value_mwh_2') : number_format($proinfas[2]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_3') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_3">Março</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_3" name="value_mwh_3" class="form-control" value="<?= set_value('value_mwh_3') != "" ? set_value('value_mwh_3') : number_format($proinfas[3]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_4') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_4">Abril</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_4" name="value_mwh_4" class="form-control" value="<?= set_value('value_mwh_4') != "" ? set_value('value_mwh_4') : number_format($proinfas[4]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_5') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_5">Maio</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_5" name="value_mwh_5" class="form-control" value="<?= set_value('value_mwh_5') != "" ? set_value('value_mwh_5') : number_format($proinfas[5]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_6') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_6">Junho</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_6" name="value_mwh_6" class="form-control" value="<?= set_value('value_mwh_6') != "" ? set_value('value_mwh_6') : number_format($proinfas[6]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_7') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_7">Julho</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_7" name="value_mwh_7" class="form-control" value="<?= set_value('value_mwh_7') != "" ? set_value('value_mwh_7') : number_format($proinfas[7]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_8') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_8">Agosto</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_8" name="value_mwh_8" class="form-control" value="<?= set_value('value_mwh_8') != "" ? set_value('value_mwh_8') : number_format($proinfas[8]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_9') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_9">Setembro</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_9" name="value_mwh_9" class="form-control" value="<?= set_value('value_mwh_9') != "" ? set_value('value_mwh_9') : number_format($proinfas[9]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_10') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_10">Outubro</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_10" name="value_mwh_10" class="form-control" value="<?= set_value('value_mwh_10') != "" ? set_value('value_mwh_10') : number_format($proinfas[10]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_11') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_11">Novembro</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_11" name="value_mwh_11" class="form-control" value="<?= set_value('value_mwh_11') != "" ? set_value('value_mwh_11') : number_format($proinfas[11]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh_12') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh_12">Dezembro</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh_12" name="value_mwh_12" class="form-control" value="<?= set_value('value_mwh_12') != "" ? set_value('value_mwh_12') : number_format($proinfas[12]['value_mwh'], 3, ',', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/proinfa'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
