<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Editar Proinfa
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity/view/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>/proinfa">Proinfa</a></li>
            <li class="active">Editar Proinfa</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Proinfa
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>proinfa/update/<?=$this->Agent_model->_pk_agent?>/<?=$this->Unity_model->_pk_unity?>/<?=$this->Proinfa_model->_pk_proinfa?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : $this->Proinfa_model->_year ?>" alt='year'>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-2">
                                <select id="month" name="month" class="form-control">
                                    <option value='01' <?=set_value('month') == '01' || $this->Proinfa_model->_month == '01' ? 'selected' : '' ?>>Janeiro</option>
                                    <option value='02' <?=set_value('month') == '02' || $this->Proinfa_model->_month == '02' ? 'selected' : '' ?>>Fevereiro</option>
                                    <option value='03' <?=set_value('month') == '03' || $this->Proinfa_model->_month == '03' ? 'selected' : '' ?>>Março</option>
                                    <option value='04' <?=set_value('month') == '04' || $this->Proinfa_model->_month == '04' ? 'selected' : '' ?>>Abril</option>
                                    <option value='05' <?=set_value('month') == '05' || $this->Proinfa_model->_month == '05' ? 'selected' : '' ?>>Maio</option>
                                    <option value='06' <?=set_value('month') == '06' || $this->Proinfa_model->_month == '06' ? 'selected' : '' ?>>Junho</option>
                                    <option value='07' <?=set_value('month') == '07' || $this->Proinfa_model->_month == '07' ? 'selected' : '' ?>>Julho</option>
                                    <option value='08' <?=set_value('month') == '08' || $this->Proinfa_model->_month == '08' ? 'selected' : '' ?>>Agosto</option>
                                    <option value='09' <?=set_value('month') == '09' || $this->Proinfa_model->_month == '09' ? 'selected' : '' ?>>Setembro</option>
                                    <option value='10' <?=set_value('month') == '10' || $this->Proinfa_model->_month == '10' ? 'selected' : '' ?>>Outubro</option>
                                    <option value='11' <?=set_value('month') == '11' || $this->Proinfa_model->_month == '11' ? 'selected' : '' ?>>Novembro</option>
                                    <option value='12' <?=set_value('month') == '12' || $this->Proinfa_model->_month == '12' ? 'selected' : '' ?>>Dezembro</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('value_mwh') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value_mwh">R$ mW/h</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value_mwh" name="value_mwh" class="form-control" value="<?= set_value('value_mwh') != "" ? set_value('value_mwh') : number_format($this->Proinfa_model->_value_mwh, 3, '.', '') ?>" alt='valor3'>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/proinfa'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
