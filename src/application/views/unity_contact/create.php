<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Adicionar Contato à Unidade
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity/view/<?=$this->Contact_model->_fk_agent?>/<?=$this->Unity_contact_model->_fk_unity?>/contatos">Unidades</a></li>
            <li class="active">Adicionar Contato à Unidade</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Adicionar Contato à Unidade
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <span name="base_url" id="base_url" style="display: none"><?=base_url()?></span>
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>unity_contact/create/<?=$this->Contact_model->_fk_agent?>/<?=$this->Unity_contact_model->_fk_unity?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('pk_contact') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Contato</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="pk_contact" name="pk_contact">
                                    <option></option>
                                    <?php if ($contacts) : ?>
                                        <?php foreach ($contacts as $contact) : ?>
                                            <option value="<?= $contact['pk_contact'] ?>" <?= set_value('pk_contact') == $contact['pk_contact'] ? "selected" : "" ?>><?= $contact['name']." ".$contact['surname'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>unity/view/<?=$this->Contact_model->_fk_agent?>/<?=$this->Unity_contact_model->_fk_unity?>/contatos'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
