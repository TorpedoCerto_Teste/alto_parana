<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Resultado de apuração de Penalidades na CCEE</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Resultado de apuração de Penalidades na CCEE </p>
            <p align="justify">Prezados, encaminhamos em anexo e disponibilizamos na <a href="<?=base_url()?>customer/pen001" target="_blank">área do cliente</a> o relatório de Apuração de Penalidades na CCEE.</p>
            <?php if ($this->Pen001_model->_realized_penalty > 0) : ?>
                <b>Para este ciclo de cobrança, foram apuradas penalidades a serem pagas pelo agente <?=$this->Agent_model->_community_name?>, <br/> e que podem ser identificadas em detalhes no relatório em anexo </b>.
                <br/>
                <br/>
                <b>Importante:</b> o valor apurado será cobrado em um ciclo futuro de aplicação de penalidades, respeitando os prazos de contestação disponíveis no termo de notificação enviado pela CCEE diretamente ao endereço do agente<br/>
                <br/>
                <table style="border:0px; width: 100%">
                    <tr>
                        <td width="300px">Agente</td>
                        <td><?=$this->Agent_model->_community_name?></td>
                    </tr>
                    <tr>
                        <td>Mês de referência</td>
                        <td><?=$this->Pen001_model->_month?>/<?=$this->Pen001_model->_year?></td>
                    </tr>
                    <tr>
                        <td>Valor (R$)</td>
                        <td><?=number_format($this->Pen001_model->_realized_penalty, 2, ",", "")?></td>
                    </tr>
                    <tr>
                        <td>Provável ciclo de cobrança</td>
                        <td><?=$this->Pen001_model->_provided_cicle_month?>/<?=$this->Pen001_model->_provided_cicle_year?></td>
                    </tr>
                    <tr>
                        <td>Data provável de pagamento</td>
                        <td><?= date_to_human_date($this->Pen001_model->_provided_payment_date)?></td>
                    </tr> 
                </table>
                <br/> 
            <?php else : ?>
                <b>Para o mês de <?= retorna_mes($this->Pen001_model->_month)?> de apuração, não foram verificadas penalidades a serem pagas pelo agente <?=$this->Agent_model->_community_name?> </b>.
            <?php endif; ?>
        <br/>
      <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?=$this->config->item("config_phone")?> e em nosso atendimento on-line, via chat da área do cliente.</p>
        <br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>
    </body>
</html>