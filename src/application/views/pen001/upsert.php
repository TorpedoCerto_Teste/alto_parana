<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Pen001
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Pen001</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<div id="app">
    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading ">
                        Pen001
                    </header>
                    <?php if ($error) : ?>
                        <div class="alert alert-danger fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                        </div>
                    <?php endif; ?>
                    <?php if ($success) : ?>
                        <div class="alert alert-success fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Sucesso!</strong> Dados gravados. 
                        </div>
                    <?php endif; ?>
                    <div class="panel-body">
                        <form id='frm_pen001' role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>pen001/upsert" enctype="multipart/form-data">
                            <input type="hidden" name="upsert" id="upsert" value="true" />
                            <input type="hidden" name="pk_pen001" id="pk_pen001" value="<?=$this->Pen001_model->_pk_pen001?>" />
                            <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                            <div class="form-group <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Agente</label>
                                <div class="col-lg-6">
                                    <select id="fk_agent" name="fk_agent" class="form-control select2">
                                        <option></option>
                                        <?php if ($agents) : ?>
                                            <?php foreach ($agents as $type => $agent) : ?>
                                                <optgroup label="<?= $type ?>">
                                                    <?php foreach ($agent as $agt) : ?>
                                                        <option value="<?= $agt['pk_agent'] ?>" <?= set_value('fk_agent') == $agt['pk_agent'] || $this->Pen001_model->_fk_agent == $agt['pk_agent'] ? "selected" : "" ?>><?= $agt['community_name'] ?></option>
                                                    <?php endforeach; ?>
                                                </optgroup>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Período</label>
                                <div class="col-lg-4 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                    <span class="help-block">Mês</span>
                                    <select id="month" name="month" class="form-control">
                                        <option value="">Selecione</option>
                                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                                            <option value="<?= $i ?>" <?= set_value('month') == $i || $this->Pen001_model->_month == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                                <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                    <span class="help-block">Ano</span>
                                    <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : ($this->Pen001_model->_year != "" ? $this->Pen001_model->_year : date("Y")) ?>" alt="year"
                                        data-bts-min="1980"
                                        data-bts-max="2050"
                                        data-bts-step="1"
                                        data-bts-decimal="0"
                                        data-bts-step-interval="1"
                                        data-bts-force-step-divisibility="round"
                                        data-bts-step-interval-delay="500"
                                        data-bts-booster="true"
                                        data-bts-boostat="10"
                                        data-bts-max-boosted-step="false"
                                        data-bts-mousewheel="true"
                                        data-bts-button-down-class="btn btn-default"
                                        data-bts-button-up-class="btn btn-default"
                                        />
                                </div>
                            </div>

                            <div id="div_unity" class="form-group" style="display: <?= $unities ? "block" : "none" ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Unidades para Rateio</label>
                                <div class="col-lg-6">
                                    <table class="table responsive-data-table data-table" >
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="checkbox-custom check-success">
                                                        <input type="checkbox" value="1" name="chk_all" id="chk_all" class="chk-all" /><label for="chk_all"><span class="help-block">Todas unidades</span></label>
                                                    </label>
                                                    Incluir no rateio
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbl_unity">
                                            <tr>
                                                <?php foreach ($unities as $unity) : ?>
                                                <td>
                                                    <label class="checkbox-custom check-success">
                                                        <input type="checkbox" class="chk" value="<?=$unity['pk_unity']?>" name="chk[]" id="chk_<?=$unity['pk_unity']?>" <?=$unity['checked'] ? "checked" : ""?> /><label for="chk_<?=$unity['pk_unity']?>">&nbsp;<?=$unity['community_name']?></label>
                                                    </label>
                                                </td>
                                                <?php endforeach; ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="">PEN001 - PDF</label>
                                <div class="col-lg-6 <?= form_error('pen001_file') != "" ? "has-error" : ""; ?>">
                                    <?php if ($this->Pen001_model->_pen001_file != "") : ?>
                                        <a class='btn btn-info' href='https://docs.google.com/viewer?embedded=true&url=<?=base_url()?>uploads/pen001/<?=$this->Pen001_model->_pk_pen001?>/<?=$this->Pen001_model->_pen001_file?>' target="_blank"><?=$this->Pen001_model->_pen001_file?></a>
                                    <?php else : ?>
                                        <input type="file" id="pen001_file" name="pen001_file" class="form-control" />
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Termo de Notificação - PDF</label>
                                <div class="col-lg-6 <?= form_error('notification_term_file') != "" ? "has-error" : ""; ?>">
                                    <?php if ($this->Pen001_model->_notification_term_file != "") : ?>
                                        <a class='btn btn-info' href='https://docs.google.com/viewer?embedded=true&url=<?=base_url()?>uploads/pen001/<?=$this->Pen001_model->_pk_pen001?>/<?=$this->Pen001_model->_notification_term_file?>' target="_blank"><?=$this->Pen001_model->_notification_term_file?></a>
                                    <?php else : ?>
                                        <input type="file" id="notification_term_file" name="notification_term_file" class="form-control" />
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Valor de Aplicação de Penalidade</label>

                                <div class="col-lg-3">
                                    <span class="help-block">Valor de Referência (VR) em R$/MWh</span>
                                    <input type="text" id="vr" name="vr" class="form-control" disabled="disabled" alt='valor2' value="<?=$this->Pen001_market_model->_value_reference?>" <?=$this->Pen001_market_model->_value_reference > $this->Pen001_market_model->_PLD_average ? 'style="font-weight: bold;"' : ""?>/>
                                </div>

                                <div class="col-lg-3">
                                    <span class="help-block">PLD Médio Penalidades em R$/MWh</span>
                                    <input type="text" id="pld" name="pld" class="form-control" disabled="disabled" alt='valor2' value="<?=$this->Pen001_market_model->_PLD_average?>" <?=$this->Pen001_market_model->_value_reference < $this->Pen001_market_model->_PLD_average ? 'style="font-weight: bold;"' : ""?>/>
                                </div>

                            </div>

                            <div class='alert-warning' >
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label" for="">Previsto</label>
                                    <div class="col-lg-3">
                                        <span class="help-block">Insuficiência (MWh)</span>
                                        <input type="text" id="provided_insuficiency" name="provided_insuficiency" class="form-control"  value="<?= $this->Pen001_model->_provided_insuficiency?>" alt='valor3' />
                                    </div>
                                    <div class="col-lg-3">
                                        <span class="help-block">Penalidade (R$)</span>
                                        <input type="text" id="provided_penalty" name="provided_penalty" class="form-control" alt='valor2' value="<?= $this->Pen001_model->_provided_penalty?>"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label" for="">Ciclo de Cobrança</label>
                                    <div class="col-lg-2 <?= form_error('provided_cicle_month') != "" ? "has-error" : ""; ?>">
                                        <span class="help-block">Mês</span>
                                        <select id="provided_cicle_month" name="provided_cicle_month" class="form-control">
                                            <option value="">Selecione</option>
                                            <?php for ($i = 1; $i <= 12; $i++) : ?>
                                                <option value="<?= $i ?>" <?= set_value('provided_cicle_month') == $i || $this->Pen001_model->_provided_cicle_month == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 <?= form_error('provided_cicle_year') != "" ? "has-error" : ""; ?>">
                                        <span class="help-block">Ano</span>
                                        <input type="text" id="provided_cicle_year" name="provided_cicle_year" class="form-control" value="<?= $this->Pen001_model->_year != "" ? $this->Pen001_model->_year : date("Y") ?>" alt="year"
                                            data-bts-min="1980"
                                            data-bts-max="2050"
                                            data-bts-step="1"
                                            data-bts-decimal="0"
                                            data-bts-step-interval="1"
                                            data-bts-force-step-divisibility="round"
                                            data-bts-step-interval-delay="500"
                                            data-bts-booster="true"
                                            data-bts-boostat="10"
                                            data-bts-max-boosted-step="false"
                                            data-bts-mousewheel="true"
                                            data-bts-button-down-class="btn btn-default"
                                            data-bts-button-up-class="btn btn-default"
                                            />
                                    </div>

                                    <div class="col-lg-2">
                                        <span class="help-block">Data Pagamento</span>
                                        <input type="text" id="provided_payment_date" name="provided_payment_date" class="form-control" alt='date' value='<?= date_to_human_date($this->Pen001_model->_provided_payment_date) ?>' />
                                    </div>


                                </div>
                            </div>

                            <div class='alert-info'>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label" for="">Realizado</label>
                                    <div class="col-lg-3">
                                        <span class="help-block">Insuficiência (MWh)</span>
                                        <input type="text" id="realized_insuficiency" name="realized_insuficiency" class="form-control" value='<?= $this->Pen001_model->_realized_insuficiency ?>' alt='valor3' />
                                    </div>

                                    <div class="col-lg-3">
                                        <span class="help-block">Penalidade (R$)</span>
                                        <input type="text" id="realized_penalty" name="realized_penalty" class="form-control" alt='valor2' value='<?= $this->Pen001_model->_realized_penalty ?>' />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label" for="">Ciclo de Cobrança</label>
                                    <div class="col-lg-2 <?= form_error('realized_cicle_month') != "" ? "has-error" : ""; ?>">
                                        <span class="help-block">Mês</span>
                                        <select id="realized_cicle_month" name="realized_cicle_month" class="form-control">
                                            <option value="">Selecione</option>
                                            <?php for ($i = 1; $i <= 12; $i++) : ?>
                                                <option value="<?= $i ?>" <?= $this->Pen001_model->_realized_cicle_month == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 <?= form_error('realized_cicle_year') != "" ? "has-error" : ""; ?>">
                                        <span class="help-block">Ano</span>
                                        <input type="text" id="realized_cicle_year" name="realized_cicle_year" class="form-control" value="<?= $this->Pen001_model->_realized_cicle_year != "" ? $this->Pen001_model->_realized_cicle_year : date("Y") ?>" alt="year"
                                            data-bts-min="1980"
                                            data-bts-max="2050"
                                            data-bts-step="1"
                                            data-bts-decimal="0"
                                            data-bts-step-interval="1"
                                            data-bts-force-step-divisibility="round"
                                            data-bts-step-interval-delay="500"
                                            data-bts-booster="true"
                                            data-bts-boostat="10"
                                            data-bts-max-boosted-step="false"
                                            data-bts-mousewheel="true"
                                            data-bts-button-down-class="btn btn-default"
                                            data-bts-button-up-class="btn btn-default"
                                            />
                                    </div>

                                    <div class="col-lg-2">
                                        <span class="help-block">Data Pagamento</span>
                                        <input type="text" id="realized_payment_date" name="realized_payment_date" class="form-control" alt='date' value='<?= date_to_human_date($this->Pen001_model->_realized_payment_date) ?>' />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label" for="">Número do Termo de Notificação</label>
                                    <div class="col-lg-3">
                                        <input type="text" id="realized_notification_term" name="realized_notification_term" class="form-control" value='<?= $this->Pen001_model->_realized_notification_term ?>' />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Observações</label>
                                <div class="col-lg-6">
                                    <textarea class="wysihtml5 form-control" id="memo" name="memo" rows="6"><?= $this->Pen001_model->_memo ?></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">Salvar</button>
                                    <?php if ($this->Pen001_model->_pk_pen001 > 0) : ?>
                                        <a class="btn btn-info" href='#sendmail' data-toggle="modal"><i class="fa fa-envelope "></i> Notificar por email</a>
                                        <a class="btn btn-primary" href='#email_notification' data-toggle="modal"><i class="fa fa-info "></i> Listagem de Notificações</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </form> 
                    </div>
                </section>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sendmail" class="modal fade">
        <form method="POST" action="<?= base_url() ?>pen001/sendmail">
            <input type="hidden" name="sendmail" id="sendmail" value="true" />
            <input type="hidden" name="pk_pen001_sendmail" id="pk_pen001_sendmail" value="<?= $this->Pen001_model->_pk_pen001 ?>" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header btn-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Notificar por email</h4>
                    </div>
                    <div class="modal-body">
                        <label class="checkbox-custom check-success">
                            <input type="checkbox" name="test" id="test" value="test"/>
                            <label for="test">Teste</label>
                        </label>

                        <div class="clearfix"></div>
                        <div class="form-group hidden" id="div_test_email">
                            <label class="col-lg-2 col-sm-2 control-label" for="test_email">Email de teste</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="test_email" id="test_email" />
                            </div>
                        </div>
                        
                        <div id="email_list">
                            <div class="tools pull-right" id="group_selector">
                                <input type="hidden" ref="controller_name" value="<?= $this->router->fetch_class() ?>" />
                                <input type="hidden" ref="fk_agent" value="<?= $this->input->post('fk_agent') ?>" />
                                <input type="hidden" ref="fk_unity" value="0" />
                                <input type="hidden" ref="base_url" value="<?= base_url() ?>" />
                                
                                <select class="form-control" name="fk_group_controller" ref="selectedGroup" v-model="selectedGroup">
                                    <option v-for="item in groupsList" :key="item.pk_group" :value="item.pk_group">
                                        {{item.group_short_name}}
                                    </option>
                                </select>
                            </div>

                            <table class="table table-responsive table-hover">
                                <thead>
                                    <tr>
                                        <th>TODOS</th>
                                        <th>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="all" name="all" id="all" value=""/><label for="all">&nbsp;</label>
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody v-if="contacts.length > 0">
                                    <tr v-for="contact in contacts" :key="contact.pk_contact">
                                        <td>{{ contact.name }} {{ contact.surname }} <br> {{ contact.email }}</td>
                                        <td>
                                            <label class="checkbox-custom check-success">
                                                <input type="checkbox" class="chk_contact" :name="'contacts[]'" :id="'contact_' + contact.pk_contact" :value="contact.pk_contact" :checked="contact.selected"/>
                                                <label :for="'contact_' + contact.pk_contact">&nbsp;</label>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody v-else>
                                    <tr>
                                        <td colspan="2">
                                            <div class="alert alert-danger fade in">
                                                <button type="button" class="close close-sm" data-dismiss="alert">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <strong>Atenção!</strong> Não existem contatos cadastrados para este agente. 
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-info" type="submit">Enviar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="email_notification" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Notificações Enviadas</h4>
                </div>
                <div class="modal-body">
                    <?php if ($email_notifications) : ?>
                        <table class="table table-responsive table-hover">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Contato</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($email_notifications as $email_notification) : ?>
                                    <tr>
                                        <td><?= date("d/m/Y H:i", strtotime($email_notification['created_at'])) ?></td>
                                        <td><?= $email_notification['name'] ?> <?= $email_notification['surname'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else : ?>
                        <div class="alert alert-danger fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Não existem dados registrados. 
                        </div>
                    <?php endif; ?>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->
</div>