<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Editar Tipo de Energia
    </h3>
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Tipo de Energia
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>energy_type/update/<?=$this->Energy_type_model->_pk_energy_type?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        
                        <div class="form-group <?= form_error('source') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="source">Fonte</label>
                            <div class="col-lg-2">
                                <select id="source" name="source" class="form-control">
                                    <option value="CONVENCIONAL" <?= set_value('source') == 'CONVENCIONAL' || $this->Energy_type_model->_source == 'CONVENCIONAL' ? 'SELECTED' : '' ?>>CONVENCIONAL</option>
                                    <option value="INCENTIVADA" <?= set_value('source') == 'INCENTIVADA' || $this->Energy_type_model->_source == 'INCENTIVADA' ? 'SELECTED' : '' ?>>INCENTIVADA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('name') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="name">Nome</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="name" name="name" class="form-control" value="<?= set_value('name') != "" ? set_value('name') : $this->Energy_type_model->_name ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('discount') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="discount">% de Desconto</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="discount" name="discount" class="form-control" value="<?= set_value('discount') != "" ? set_value('discount') : number_format($this->Energy_type_model->_discount, 2, '.', '') ?>" alt="valor">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
