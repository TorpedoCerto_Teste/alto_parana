<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Faturamento 
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>ccvee_billing/index">Faturamento</a></li>
            <li class="active">Editar Faturamento</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Faturamento
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_billing/update/<?= $this->uri->segment(3, 0) ?>">
                        <input type="hidden" name="update" id="update" value="true" />

                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : $this->Ccvee_billing_model->_year ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="month" name="month" class="form-control" value="<?= set_value('month') != "" ? set_value('month') : $this->Ccvee_billing_model->_month ?>" alt="ddd">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Data de validade</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control" value="<?= set_value('expiration_date') != "" ? set_value('expiration_date') : $this->Ccvee_billing_model->_expiration_date ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('total') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="total">Total</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="total" name="total" class="form-control" value="<?= set_value('total') != "" ? set_value('total') : $this->Ccvee_billing_model->_total ?>" alt="decimal">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_status') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_status">Status do pagamento</label>
                            <div class="col-lg-4">
                                <select id="payment_status" name="payment_status" class="form-control">
                                    <option value="programado" >PROGRAMADO</option>
                                    <option value="nao_programado" >NÃO PROGRAMADO</option>
                                    <option value="quitado" >QUITADO</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_date">Data de pagamento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="payment_date" name="payment_date" class="form-control" value="<?= set_value('payment_date') != "" ? set_value('payment_date') : $this->Ccvee_billing_model->_payment_date ?>" alt="date">
                            </div>
                        </div>
                        
                        
                         <div class="form-group <?= form_error('invoice_type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="invoice_type">Tipo de fatura</label>
                            <div class="col-lg-4">
                                <select id="invoice_type" name="invoice_type" class="form-control">
                                    <option value="debito">DÉBITO</option>
                                    <option value="credito">CRÉDITO</option>
                                    <option value="glosa">GLOSA</option>
                                    <option value="cancelamento">CANCELAMENTO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('payment_slip') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="payment_slip">Boleto de pagamento</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="payment_slip" name="payment_slip" class="form-control" value="<?= set_value('payment_slip') != "" ? set_value('payment_slip') : $this->Ccvee_billing_model->_payment_slip?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('invoice') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="invoice">Fatura</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="invoice" name="invoice" class="form-control" value="<?= set_value('invoice') != "" ? set_value('invoice') : $this->Ccvee_billing_model->_invoice?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('xml') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="xml">xml</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="xml" name="xml" class="form-control" value="<?= set_value('xml') != "" ? set_value('xml') : $this->Ccvee_billing_model->_xml?>">
                            </div>
                        </div>                       

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
