<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Grupo para Contato
    </h3>
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Grupo para Contato
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>group_agent_contact/create/<?=$this->Agent_model->_pk_agent?>">
                        <input type="hidden" name="create" id="create" value="true" />
                        
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Agente</label>
                            <div class="col-lg-10">
                                <input type="text" id="agent" name="agent" class="form-control" value="<?= $this->Agent_model->_community_name ?>" disabled="" >
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_group') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_group">Grupo</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_group" name="fk_group">
                                    <option></option>
                                    <?php if ($groups) : ?>
                                        <?php foreach ($groups as $group) : ?>
                                            <option value="<?= $group['pk_group'] ?>" <?= set_value('fk_group') == $group['pk_group'] ? "selected" : "" ?>><?= $group['group'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_contact') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_contact">Contato</label>
                            <div class="col-lg-10">
                                <select class="form-control select2-allow-clear" id="fk_contact" name="fk_contact">
                                    <option></option>
                                    <?php if ($contacts) : ?>
                                        <?php foreach ($contacts as $agent => $contact) : ?>
                                            <optgroup label="<?=$agent?>">
                                                <?php foreach ($contact as $ct) : ?>
                                                    <option value="<?= $ct['pk_contact'] ?>" <?= set_value('fk_contact') == $ct['pk_contact'] ? "selected" : "" ?>><?= $ct['name']." ".$ct['surname'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>agent/view/<?=$this->Agent_model->_pk_agent?>'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
