<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        DEVEC
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">DEVEC</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Selecione um filtro para o relatório.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Agente</th>
                                <th>Unidade</th>
                                <th>Arquivo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['month'] ?>/<?= $item['year'] ?></td>
                                    <td><?= $item['agent_community_name']?></td>
                                    <td><?= $item['unity_community_name']?></td>
                                    <td>
                                        <?php if ($item['devec_file'] != "") : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?=base_url()?>uploads/devec_customer/<?= $item['pk_devec_customer'] ?>/<?= $item['devec_file'] ?>" target="_blank"><?= $item['devec_file'] ?></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
