<!-- page head start-->
<div class="page-head">
    <h3>
        Dashboard
    </h3>
    <div class="state-information">
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Filtros
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>customer/dashboard" id="frm_dashboard" name="frm_dashboard">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled">
                        <input type="hidden" name="date" id="date" value="<?= $date ?>">

                        <div class="form-group">
                            <div class="col-lg-2 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control select2">
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?= date("m", strtotime($date)) == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= date("Y", strtotime($date)) ?>" alt="year"
                                       data-bts-min="2010"
                                       data-bts-max="<?= date("Y") ?>"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                            <div class="col-lg-3">
                                <span class="help-block">CCVEE</span>
                                <select id="pk_ccvee_general" name="pk_ccvee_general" class="form-control select2">
                                    <option></option>
                                    <?php if ($ccvee_generals) : ?>
                                        <?php foreach ($ccvee_generals as $ccvee) : ?>
                                            <option value="<?= $ccvee['pk_ccvee_general'] ?>" <?= set_value('pk_ccvee_general') == $ccvee['pk_ccvee_general'] || count($ccvee_generals) == 1 ? "selected" : "" ?>><?= $ccvee['document_number'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div> 

    <?php if ($ccvee_generals) : ?>
        <!--GRÁFICOS-->
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Atendimento de Contrato
                        <span class="tools pull-right">
                            <div class="btn-row">
                                <div class="btn-toolbar">
                                </div>
                            </div>
                        </span>
                    </header>
                    <div class="panel-body">
                        <?php if (empty($contrato_versus_consumo)) : ?>
                        <?php else : ?>
                        <?php endif;?>
                        <div id="chart_data" style="display: none"><?= $contrato_versus_consumo ?></div>
                        <div id="atendimento_contrato_chart">
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Resumo
                        <span class="tools pull-right">
                            <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body" style="background-color: #f0f0f0">

                        <div class="row">
                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>CONSUMO (MWh)</p>
                                            <hr>
                                            <div class="btn-info" style="text-align:center;">
                                                <h3 style="font-size: 65px;"><?= number_format($consumo, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                            <div class="progress">
                                                <?php $percent = ($consumo * 100) / $contrato; ?>
                                                <div class="progress-bar progress-bar-<?= $percent < 100 ? 'success' : 'danger' ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent < 100 ? ceil($percent) : 100 ?>%;"> <?= ceil($percent) ?>% </div> 
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>PERDAS (MWh)</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;"><?= number_format($perdas, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>PROINFA (MWh)</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;"><?= number_format($proinfa, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                                
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>CONTRATO (MWh)</p>
                                            <hr>
                                            <div class="btn-success" style="text-align:center;">
                                                <h3 style="font-size: 65px;"><?= number_format($contrato, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>FLEXIBILIDADE INFERIOR (MWh)</p>
                                            <hr>
                                            <div class="btn-warning" style="text-align:center;">
                                                <h3 style="font-size: 65px;"><?= number_format($flexibilidade_inferior, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>FLEXIBILIDADE SUPERIOR (MWh)</p>
                                            <hr>
                                            <div class="btn-danger" style="text-align:center;">
                                                <h3 style="font-size: 65px;"><?= number_format($flexibilidade_superior, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                                
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>HORAS MEDIDAS</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;"><?= $horas_medidas ?>/<?= $total_horas_mes?></h3>
                                                &nbsp;
                                            </div>
                                            <div class="progress">
                                                <?php $percent = ($horas_medidas * 100) / $total_horas_mes; ?>
                                                <div class="progress-bar progress-bar-<?= $percent < 100 ? 'success' : 'danger' ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent < 100 ? ceil($percent) : 100 ?>%;"> <?= ceil($percent) ?>% </div> 
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>HORAS FALTANTES</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;"><?= $horas_faltantes ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>SOBRA (MWh)</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;"><?= number_format($sobra, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>        

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>DÉFICIT (MWh)</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;"><?= number_format($deficit, 3, ",", "") ?></h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                                
                        </div>


                    </div>
                </section>
            </div>
        </div>       
    <?php endif; ?>

    <div class="row">
        <div class="col-md-4">
            <section class="panel">
                <header class="panel-heading">
                    Previsão do Tempo
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <img src="http://s3.cptec.inpe.br/admingpt/tempo/mapas/br1.jpg" class='carrossel' width="300px" border="0">
                </div>
            </section>
        </div>

        <div class="col-md-4">
            <section class="panel">
                <header class="panel-heading">
                    Imagem de Satélite
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <img src="http://img0.cptec.inpe.br/~rgrafico/portal_tempo/satelite/img_atu_nova.jpg" width="300px" border="0" alt="Satélite">
                </div>
            </section>
        </div>
        
        <div class="col-md-4">
            <section class="panel">
                <header class="panel-heading">
                    Mapa de Ventos
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <iframe width="300" height="300" src="https://embed.windy.com/embed2.html?lat=-25.958&lon=-53.438&zoom=3&level=surface&overlay=wind&menu=&message=&marker=&forecast=12&calendar=now&location=coordinates&type=map&actualGrid=&metricWind=km%2Fh&metricTemp=%C2%B0C" frameborder="0"></iframe>
                </div>
            </section>
        </div>
    </div>          


</div>
<!--body wrapper end-->





</div>
<!-- body content end-->

