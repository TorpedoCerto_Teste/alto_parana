<div id="app">
    <div class="page-head">
        <h3>
            <img src="<?= base_url() ?>img/APE_4.png">
        </h3>
        <div class="state-information">
            <h3>Dashboard</h3>
        </div>
    </div>

    <div class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        <table class="table">
                            <tr>
                                <td v-if="unities.length > 0">{{ unities[0].agent_company_name }}</td>
                                <td v-if="ccvee_generals.length > 0">{{ ccvee_contract.document_number }} ({{ ccvee_contract.unities }} unidade{{ parseInt(ccvee_contract.unities) > 1 ? 's' : '' }})</td>
                                <td>PERÍODO: {{ filter.month }}/{{ filter.year }}</td>
                                <td><?= date("d/m/Y")?></td>
                            </tr>
                        </table>
                    </header>
                </section>
            </div>
        </div>
        <div class="row" v-if="parseInt(ccvee_contract.unities) > 1">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Resumo por Unidade
                    </header>
                    <div class="panel-body">
                        <section class="panel">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Unidade</th>
                                        <th>Consumo ({{conversion}})</th>
                                        <th>Perdas ({{conversion}}) {{widgets.perdasCcvee}}%</th>
                                        <th>Proinfa ({{conversion}})</th>
                                        <th>Líquido ({{conversion}})</th> 
                                        <th>Horas Medidas</th>
                                        <th>Horas/Mês</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-if="!_.isUndefined(resumeModal) && !_.isEmpty(resumeModal)">
                                        <tr v-for="i of resumeModal">
                                            <th>{{i.unity_community_name}}</th>
                                            <td class="info">{{ (i.resume.consumo).replace(".", ",") }}</td>
                                            <td class="active">{{ (i.resume.perdas).replace(".", ",")}}</td>
                                            <td class="success">{{ (i.resume.proinfa).replace(".", ",") }}</td>
                                            <td class="warning">{{ (i.resume.liquido).replace(".", ",") }}</td>
                                            <td class="danger">{{ (i.resume.hours) }}</td>
                                            <th class="active">{{ (widgets.totalHorasMes) }}</th>
                                        </tr>
                                    </template>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th class="info">{{ (resumeModalTotal.consumo) }}</th>
                                        <th class="active">{{ (resumeModalTotal.perdas) }}</th>
                                        <th class="success">{{ (resumeModalTotal.proinfa) }}</th>
                                        <th class="warning">{{ (resumeModalTotal.liquido) }}</th>
                                        <th class="danger">{{ (resumeModalTotal.hours) }}</th>
                                        <th class="active"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </section>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Atendimento de Contrato
                    </header>
                    <div class="panel-body">
                        <div class="background_loader_chart" style="display: none;">
                            <div id="loading" class="center">
                                <div class="loader"></div>
                            </div>
                        </div>
                        <div style="width: 100%; height: 300px !important;">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div><canvas id="mixedchart" style="display: block; height: 300px; width: 1370px;" width="2740" height="600" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Resumo Energético
                    </header>
                    <div class="panel-body" style="background-color: rgb(240, 240, 240);">
                        <div class="row">
                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0px;">
                                            <p>CONSUMO (MWh)</p>
                                            <hr>
                                            <div class="btn-info" style="text-align:center;">
                                                <h3 style="font-size: 65px;">{{floatToString((widgets.consumoLiquido), conversion == 'MWh' ? 3 : 6)}}</h3>
                                                <p v-if="widgets.consumoLiquidoProjetado >= 0"  style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.consumoLiquidoProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                            </div>
                                            <div class="progress" style="margin-bottom: 0;">
                                                <div v-bind:class="{'progress-bar-success': widgets.consumoversuscontrato < 100, 'progress-bar-danger': widgets.consumoversuscontrato >= 100}"  class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: widgets.consumoversuscontrato + '%' }" > {{widgets.consumoversuscontrato}}% </div> 
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>PERDAS ({{conversion}}) {{widgets.perdasCcvee}}%</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;">{{floatToString(widgets.perdas, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                <p v-if="widgets.perdasProjetado >= 0" style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.perdasProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>  

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>PROINFA ({{conversion}})</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 65px;">{{floatToString(widgets.proinfa, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>  
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>CONTRATO ({{conversion}})</p>
                                            <hr>
                                            <div class="btn-success" style="text-align:center;">
                                                <h3 style="font-size: 55px;">{{floatToString(widgets.contrato, conversion == 'MWh' ? 3 : 6)}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>FLEXIBILIDADE INFERIOR ({{conversion}}) {{widgets.percentFlexInferior}}%</p>
                                            <hr>
                                            <div class="btn-warning" style="text-align:center;">
                                                <h3 style="font-size: 55px;">{{floatToString(widgets.flexInferior, conversion == 'MWh' ? 3 : 6)}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>FLEXIBILIDADE SUPERIOR ({{conversion}}) {{widgets.percentFlexSuperior}}%</p>
                                            <hr>
                                            <div class="btn-danger" style="text-align:center;">
                                                <h3 style="font-size: 55px;">{{floatToString(widgets.flexSuperior, conversion == 'MWh' ? 3 : 6)}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>% ATENDIMENTO</p>
                                            <hr>
                                            <div style="text-align:center; text-align:center;background-color: #f0f0f0;">
                                                <h3 style="font-size: 55px;">{{floatToString(widgets.attendance, 2)}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                                  
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <template v-if="unities.length === 1">
                                                <p>HORAS MEDIDAS</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 65px;">{{widgets.horasMedidas}}/{{widgets.totalHorasMes}}</h3>
                                                    &nbsp;
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: this.widgets.avgHorasMedidas + '%' }" >  {{Math.ceil(this.widgets.avgHorasMedidas)}}% </div> 
                                                </div>
                                            </template>
                                            <template v-else>
                                                <p>HORAS NO MÊS</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 65px;">{{widgets.totalHorasMes}}</h3>
                                                    &nbsp;
                                                </div>
                                            </template>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <template v-if="unities.length === 1">
                                            <div class="value" style="padding-left: 0;">
                                                <p>HORAS FALTANTES</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 65px;">{{widgets.horasFaltantes}}</h3>
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </template>
                                        <template v-else>
                                            <div class="value" style="padding-left: 0;">
                                                <p>SITUAÇÃO MEDIÇÃO</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <template v-if="parseInt(widgets.horasFaltantes) > 0">
                                                        <h3 style="font-size: 25px;">Horas Faltantes</h3>
                                                    </template>
                                                    <template v-else>
                                                        <h3 style="font-size: 25px;">Consistido</h3>
                                                    </template>
                                                    <h3 style="font-size: 40px;">&nbsp;</h3>
                                                </div>
                                            </div>
                                        </template>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>SOBRA ({{conversion}})</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 60px;">{{floatToString(widgets.sobra, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                <p v-if="widgets.sobraProjetado >= 0" style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.sobraProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>        

                            <div class="col-md-3">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>DÉFICIT ({{conversion}})</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0">
                                                <h3 style="font-size: 60px;">{{floatToString(widgets.deficit, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                <p v-if="widgets.deficitProjetado >= 0" style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.deficitProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                                
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Resumo Financeiro por Contrato
                        <span class="tools pull-right">
                        </span>
                    </header>
                    <div class="panel-body" style="background-color: #f0f0f0">
                        <!-- submercado / pld  -->
                        <div class="row">
                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>SUBMERCADO</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0; min-height: 150px">
                                                <h3 style="font-size: 65px;">{{widgets.submarket}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>PLD (R$/MWh)</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0; min-height: 150px">
                                                <h3 style="font-size: 65px;">R$ {{floatToString(widgets.pld.pld_value, 2)}}</h3>
                                                <p style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{widgets.pld.status}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div> 
                            
                            <div class="col-md-4">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>SPREAD EM RELAÇÃO PLD (MÉDIA ESTIMADA)</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0; text-align:center; min-height: 150px">
                                                <h3 style="font-size: 55px;">R$ {{floatToString(widgets.pld.spread, 2)}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                        </div>
                        <!-- /submercado / pld  -->

                        <!-- Se Liquidar / Spread em Relacao PLD / Venda/Compra de Curto Prazo -->
                        <div class="row">
                            <div class="col-md-6">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>LIQUIDAÇÃO PREVISTA EM CASO DE SOBRA</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0; text-align:center; min-height: 180px">
                                                <h3 style="font-size: 55px;">R$ {{floatToString(widgets.pld_if_liquidate, 2)}}</h3>
                                                <p style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{widgets.pld.status}}</p>
                                                <p style="margin: 0; font-size: 20px; padding-bottom: 10px;">DATA LIQUIDAÇÃO: {{widgets.credit_date}}</p>
                                                <p style="margin: 0; font-size: 15px; padding-bottom: 10px;">OBS. Considera o crédito da sobra projetada, caso opte por não vender e liquidar na CCEE.</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>                    

                            <div class="col-md-6">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="value" style="padding-left: 0;">
                                            <p>{{ widgets.short_term_text1 }} DE CURTO PRAZO</p>
                                            <hr>
                                            <div style="text-align:center;background-color: #f0f0f0; text-align:center; min-height: 180px;">
                                                <h3 style="font-size: 55px;"><span style="font-size: 20px">PREÇO R$/MWh:</span> {{floatToString(widgets.short_term_price, 2)}}</h3>
                                                <h3 style="font-size: 55px;"><span style="font-size: 20px">TOTAL R$:</span> {{floatToString(widgets.short_term_total)}}</h3>
                                                <p style="margin: 0; font-size: 15px; padding-bottom: 10px;">OBS. {{ widgets.short_term_text2 }} no 6º dia útil</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>    

                        </div>
                        <!-- /Se Liquidar / Spread em Relacao PLD / Venda/Compra de Curto Prazo -->

                    </div>
                </section>
            </div>
        </div> 
    </div>

</div>