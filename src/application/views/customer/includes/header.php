<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="" />
    <link rel="shortcut icon" href="<?= base_url() ?>img/ico/favicon.ico">
    <title><?php echo $_SERVER['CI_ENV'] != 'production' ? "[STAGE]" : "" ?>Alto Paraná Energia</title>
    <link href="<?= base_url() ?>js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />
    <link href="<?= base_url() ?>css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>css/style-responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="<?= base_url() ?>js/html5shiv.js"></script>
        <script src="<?= base_url() ?>js/respond.min.js"></script>
        <![endif]-->
    <?php if (isset($css_include)) print_r($css_include); ?>
</head>

<body class="sticky-header <?= $this->session->userdata('sidebar_collapsed') ? "sidebar-collapsed" : "" ?>">
    <div id="body_base_url" style="display: none"><?= base_url() ?></div>
    <section>
        <div class="sidebar-left sticky-sidebar">
            <div class="logo dark-logo-bg">
                <a href="<?= base_url() ?>customer">
                    <img src="<?= base_url() ?>img/APE_3.png" alt="">
                </a>
            </div>
            <div class="sidebar-left-info">
                <div class="search-field"></div>
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li class="<?= $this->router->fetch_class() == 'index' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                    <li class="<?= $this->router->fetch_class() == 'dashboard_measurement' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/dashboard_measurement"><i class="fa fa-dashboard"></i> <span>Dashboard de Medição</span></a></li>
                    <li class="menu-list <?php
                                            if (
                                                $this->router->fetch_class() == 'payment_slip_ccee' ||
                                                $this->router->fetch_class() == 'eer' ||
                                                $this->router->fetch_method() == 'gfn001' ||
                                                $this->router->fetch_class() == 'accounting' ||
                                                $this->router->fetch_class() == 'lfpen001' ||
                                                $this->router->fetch_class() == 'pen001'
                                            ) {
                                                echo "nav-active";
                                            }
                                            ?> ">
                        <a href="#"><i class="fa fa-bolt"></i> <span>Operação CCEE</span></a>
                        <ul class="child-list">
                            <li class="<?= $this->router->fetch_class() == 'payment_slip_ccee' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/payment_slip_ccee"> Contribuição Associativa </a></li>
                            <li class="<?= $this->router->fetch_class() == 'eer' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/eer"> Encargo de Energia de Reserva </a></li>
                            <li class="<?= $this->router->fetch_method() == 'gfn001' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/accounting/gfn001"> Aporte Garantia </a></li>
                            <li class="<?= $this->router->fetch_class() == 'accounting' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/accounting"> Contabilização </a></li>
                            <li class="<?= $this->router->fetch_class() == 'lfpen001' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/lfpen001"> Liquidação de Penalidades </a></li>
                            <li class="<?= $this->router->fetch_class() == 'pen001' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/pen001"> Penalidades / Lastro </a></li>
                        </ul>
                    </li>
                    <li class="menu-list <?php
                                            if (
                                                $this->router->fetch_method() == 'free_captive'
                                            ) {
                                                echo "nav-active";
                                            }
                                            ?> ">
                        <a href="#"><i class="fa fa-bolt"></i> <span>Relatórios Gerenciais</span></a>
                        <ul class="child-list">
                            <li class="<?= $this->router->fetch_method() == 'free_captive' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/free_captive"> Relatório de Energia </a></li>
                        </ul>
                    </li>
                    <li class="menu-list <?php
                                            if (
                                                $this->router->fetch_class() == 'devec_customer' ||
                                                $this->router->fetch_class() == 'ccvee_billing' ||
                                                $this->router->fetch_method() == 'billing' ||
                                                $this->router->fetch_class() == 'tusd'
                                            ) {
                                                echo "nav-active";
                                            }
                                            ?> ">
                        <a href="#"><i class="fa fa-dollar"></i> <span>Financeiro</span></a>
                        <ul class="child-list">
                            <li class="<?= $this->router->fetch_class() == 'devec_customer' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/devec_customer"> DEVEC / Subst. Tributária </a></li>
                            <li class="<?= $this->router->fetch_class() == 'ccvee_billing' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/ccvee_billing"> Faturamentos de Energia</a></li>
                            <li class="<?= $this->router->fetch_method() == 'billing' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/business/billing"> Faturamentos de Serviços</a></li>
                            <li class="<?= $this->router->fetch_class() == 'tusd' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/tusd"> Faturamentos de Distribuição (TUSD)</a></li>
                        </ul>
                    </li>
                    <li class="menu-list <?php
                                            if (
                                                $this->router->fetch_class() == 'business' && $this->router->fetch_method() == 'index' ||
                                                $this->router->fetch_class() == 'ccvee_general'
                                            ) {
                                                echo "nav-active";
                                            }
                                            ?> ">
                        <a href="#"><i class="fa fa-file-text-o"></i> <span>Contratos</span></a>
                        <ul class="child-list">
                            <li class="<?= $this->router->fetch_class() == 'business' && $this->router->fetch_method() == 'index' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/business"> Contrato de Serviço Alto Paraná</a></li>
                            <li class="<?= $this->router->fetch_class() == 'ccvee_general' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/ccvee_general"> Contratos de Energia (CCVEE)</a></li>
                        </ul>
                    </li>
                    <li class="menu-list <?php
                                            if (
                                                $this->router->fetch_class() == 'energy_market' ||
                                                $this->router->fetch_class() == 'pld'
                                            ) {
                                                echo "nav-active";
                                            }
                                            ?> ">
                        <a href="#"><i class="fa fa-bar-chart-o"></i> <span>Mercado</span></a>
                        <ul class="child-list">
                            <li class="<?= $this->router->fetch_class() == 'energy_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/energy_market"> Energy Market</a></li>
                            <li class="<?= $this->router->fetch_class() == 'pld' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/pld"> PLD</a></li>
                        </ul>
                    </li>
                    <li class="menu-list <?php
                                            if (
                                                $this->router->fetch_class() == 'financial_index' ||
                                                $this->router->fetch_class() == 'tariff_flags' ||
                                                $this->router->fetch_class() == 'bank_charges' ||
                                                $this->router->fetch_class() == 'devec_market'
                                            ) {
                                                echo "nav-active";
                                            }
                                            ?> ">
                        <a href="#"><i class="fa fa-cubes"></i> <span>Utilidades</span></a>
                        <ul class="child-list">
                            <li class="<?= $this->router->fetch_class() == 'financial_index' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/financial_index"> Calculadora de Reajustes</a></li>
                            <li class="<?= $this->router->fetch_class() == 'tariff_flags' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/tariff_flags"> Bandeiras Tarifárias</a></li>
                            <li class="<?= $this->router->fetch_class() == 'bank_charges' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/bank_charges"> Tarifas</a></li>
                            <li class="<?= $this->router->fetch_class() == 'devec_market' ? 'active' : '' ?>"><a href="<?= base_url() ?>customer/devec_market"> DEVEC por UF</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="sidebar-widget"></div>
            </div>
        </div>
        <div class="body-content">
            <div class="header-section">
                <div class="logo dark-logo-bg hidden-xs hidden-sm">
                    <a href="<?= base_url() ?>customer">
                        <img src="<?= base_url() ?>img/APE_3.png" alt="">
                    </a>
                </div>
                <div class="icon-logo dark-logo-bg hidden-xs hidden-sm">
                    <a href="<?= base_url() ?>customer">
                        <img src="<?= base_url() ?>img/APE_1.png" width="24px">
                    </a>
                </div>
                <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
                <div class="notification-wrap">
                    <div class="left-notification">
                        <ul class="notification-menu">
                            <li>
                                <a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-warning"><?= $this->message_total_unread ?></span>
                                </a>
                                <div class="dropdown-menu dropdown-title">
                                    <div class="title-row">
                                        <h5 class="title yellow">
                                            <?php if ($this->message_total_unread > 0) : ?>
                                                Você tem <?= $this->message_total_unread ?> novas mensagens
                                            <?php else: ?>
                                                Nenhuma mensagem nova!
                                            <?php endif; ?>
                                        </h5>
                                        <a href="<?= base_url() ?>customer/message_agent" class="btn-warning btn-view-all">Ver todas</a>
                                    </div>
                                    <div class="notification-list-scroll sidebar">
                                        <div class="notification-list mail-list not-list">
                                            <?php if ($this->all_message) : ?>
                                                <?php foreach ($this->all_message as $message) : ?>
                                                    <a href="<?= base_url() ?>customer/message_agent/view/<?= $message['pk_message_agent'] ?>" class="single-mail">
                                                        <?php if ($message['priority'] == 1) : ?>
                                                            <span class="icon bg-warning">
                                                                <i class="fa fa-warning"></i>
                                                            </span>
                                                        <?php else: ?>
                                                            <span class="icon bg-info">
                                                                <i class="fa fa-envelope-o"></i>
                                                            </span>
                                                        <?php endif; ?>
                                                        <strong><?= $message['title'] ?></strong>
                                                        <p>
                                                            <small><?= date("d/m/Y H:i", strtotime($message['created_at'])) ?></small>
                                                        </p>
                                                        <?php if ($message['status'] == $this->Message_agent_model->_status_active) : ?>
                                                            <span class="un-read tooltips" data-original-title="Marcar como lida" data-toggle="tooltip" data-placement="left">
                                                                <i class="fa fa-circle-o"></i>
                                                            </span>
                                                        <?php else : ?>
                                                            <span class="read">
                                                                <i class="fa fa-circle"></i>
                                                            </span>
                                                        <?php endif; ?>
                                                    </a>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>customer/calendar" class="btn btn-default dropdown-toggle info-number">
                                    <i class="fa fa-calendar"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="right-notification">
                        <ul class="notification-menu">
                            <li>
                                <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <?= $this->session->userdata('name') ?> (<?= $this->agents_list[$this->fk_agent]['community_name'] ?>)
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu purple pull-right" style="max-height: 300px; overflow-y: auto;">
                                    <?php if (isset($this->agents_list) && sizeof($this->agents_list) > 1) : ?>
                                        <li><a href="<?= base_url()?>customer/agent"><i class="fa fa-bolt pull-right"></i> TROCAR DE AGENTE</a></li>
                                    <?php endif; ?>
                                    <li><a href="<?= base_url() ?>customer/index/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


