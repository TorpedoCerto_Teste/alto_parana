</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url() ?>js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url() ?>js/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url() ?>js/jquery-migrate.js"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/modernizr.min.js"></script>


<script src="<?= base_url() ?>js/scripts.js"></script>
<?php if (isset($js_include)) print_r($js_include); ?>

</body>


</html>
