<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="" />
        <link rel="shortcut icon" href="<?= base_url() ?>img/ico/favicon.ico">

        <title><?php echo $_SERVER['CI_ENV'] != 'production' ? "[STAGE]" : "" ?>Alto Paraná Energia</title>

        <!--jquery-ui-->
        <link href="<?= base_url() ?>js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />

        <!--common style-->
        <link href="<?= base_url() ?>css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/style-responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?= base_url() ?>js/html5shiv.js"></script>
        <script src="<?= base_url() ?>js/respond.min.js"></script>
        <![endif]-->
        <?php if (isset($css_include)) print_r($css_include); ?>

    </head>

    <body>
        <div id="body_base_url" style="display: none"><?= base_url() ?></div>

        <section>
            <!-- body content start-->
            <div class="body-content" style="width: 100%; margin: 0" >
