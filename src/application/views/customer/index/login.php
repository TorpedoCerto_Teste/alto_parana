<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?=base_url()?>img/ico/favicon.ico">
        <title>Login - ALTO PARANÁ ENERGIA</title>

        <!-- Base Styles -->
        <link href="<?=base_url()?>css/style.css" rel="stylesheet">
        <link href="<?=base_url()?>css/style-responsive.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?=base_url()?>js/html5shiv.min.js"></script>
        <script src="<?=base_url()?>js/respond.min.js"></script>
        <![endif]-->


    </head>

    <body class="login-body" style="background-color: #fff; background-image: url('<?=base_url()?>web/img/background<?=rand(1,5)?>.jpg'); background-position: relative; background-repeat: no-repeat; background-size: cover;">

        <div class="login-logo" style="background-color: transparent !important">
            <img src="<?=base_url()?>img/APE_1.png" alt=""/>
        </div>

        <h2 class="form-heading" style='background: #fff none repeat scroll 0 0;color: #191970;font-size: 32px;'><b>SISTEMA ALTO PARANÁ ENERGIA</b></h2>
        <div class="container log-row">
            <form class="form-signin" action="<?= base_url() ?>customer/index/login" method="post">
                <input type="hidden" name="login" id="login" value="true" />
                <div class="login-wrap">
                    <div class="alert alert-block alert-danger fade in" style="display: <?=$error == 1 ? "block": "none"?>">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Confira seu email ou senha! 
                    </div>
                    <input type="email" name="email" id="email" class="form-control <?= form_error('email') != "" ? "error" : ""; ?>" placeholder="E-mail" autofocus required type="email" style="background: whitesmoke none repeat scroll 0 0; color: black">
                    <input type="password" name="password" id="password" class="form-control <?= form_error('password') != "" ? "error" : ""; ?>" placeholder="Password" required style="background: whitesmoke none repeat scroll 0 0; color: black">
                    <button class="btn btn-lg btn-success btn-block" type="submit">LOGIN</button>
                    <label class="checkbox-custom check-success">
                        <a class="pull-right" data-toggle="modal" href="#forgotPass"> Esqueceu a senha?</a>
                    </label>

                </div>

                <!-- Modal -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="forgotPass" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Esqueceu a senha?</h4>
                            </div>
                            <div class="modal-body">
                                <p>Entre com seu e-mail que enviaremos instruções de acesso.</p>
                                <input type="text" name="emailForgot" id="emailForgot" placeholder="E-mail" autocomplete="off" class="form-control placeholder-no-fix <?= form_error('emailForgot') != "" ? "error" : ""; ?>" type="email">

                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                                <button class="btn btn-success" type="button">Recuperar Senha</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->

            </form>
        </div>


        <!--jquery-1.10.2.min-->
        <script src="<?=base_url()?>js/jquery-1.11.1.min.js"></script>
        <!--Bootstrap Js-->
        <script src="<?=base_url()?>js/bootstrap.min.js"></script>
        <!--<script src="js/jrespond..min.html"></script>-->

    </body>
</html>
