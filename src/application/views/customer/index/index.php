<!-- page head start-->
<div class="page-head">
    <h3>
        <?= $this->Agent_model->_community_name ?>
    </h3>
    <span class="sub-title"><?= $this->Agent_model->_company_name ?></span>
    <div class="state-information">
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row state-overview">
        <div class="col-lg-4 col-sm-6">
            <section class="panel blue">
                <div class="symbol">
                    <i class="fa fa-money"></i>
                </div>
                <div class="value white" style='padding-left: 0;'>
                    <h1>
                        Pagamento TUSD
                    </h1>
                    <p>01/12/2016</p>
                </div>
            </section>
        </div>
        <div class="col-lg-4 col-sm-6">
            <section class="panel blue">
                <div class="symbol">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="value white"  style='padding-left: 0;'>
                    <h1>
                        Publicação PLD
                    </h1>
                    <p>02/12/2016</p>
                </div>
            </section>
        </div>
        <div class="col-lg-4 col-sm-6">
            <section class="panel blue">
                <div class="symbol ">
                    <i class="fa fa-dashboard"></i>
                </div>
                <div class="value white"  style='padding-left: 0;'>
                    <h1>
                        Informe de Medição
                    </h1>
                    <p>03/12/2016</p>
                </div>
            </section>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Gráfico de consumo x contrato mensal (Dezembro / 2016)
                    <span class="tools pull-right">
                        <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div id="combine-chart">
                        <div id="combine-chart-container" class="f-c-space">
                        </div>
                    </div>
                    <div class="row earning-chart-info">
                        <div class="col-sm-4 col-xs-6">
                            <h4>1,007</h4>
                            <small class="text-muted"> Consumo</small>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <h4>0,000</h4>
                            <small class="text-muted">Sobra</small>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <h4>0,000</h4>
                            <small class="text-muted">Déficit</small>
                        </div>
                    </div>                
                </div>
            </section>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-4">
            <section class="panel">
                <header class="panel-heading">
                    Previsão do Tempo
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <img src="http://img0.cptec.inpe.br/~rgrafico/portal_tempo/bandas/br1.jpg" class='carrossel' width="300px" border="0">
                </div>
            </section>
        </div>

        <div class="col-md-4">
            <section class="panel">
                <header class="panel-heading">
                    Imagem de Satélite
                    <span class="tools pull-right">
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <img src="http://img0.cptec.inpe.br/~rgrafico/portal_tempo/satelite/img_atu_nova.jpg" width="300px" border="0" alt="Satélite">
                </div>
            </section>
        </div>
    </div>          
    
    <div class="row">
    </div>                
</div>
<!--body wrapper end-->


<!--footer section start-->
<footer>
</footer>
<!--footer section end-->



</div>
<!-- body content end-->
