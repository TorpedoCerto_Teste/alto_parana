<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Encargo de Energia de Reserva (EER)
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Eer</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Selecione um filtro para o relatório.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Valor</th>
                                <th>Data Débito</th>
                                <th>Arquivo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['month'] ?>/<?= $item['year'] ?></td>
                                    <td><?= number_format($item['value'], 2, ",", "") ?></td>
                                    <td><?= date_to_human_date($item['payment_date'])?></td>
                                    <td>
                                        <?php if ($item['eer_file'] != "") : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?=base_url()?>uploads/eer/<?= $item['pk_eer'] ?>/<?= $item['eer_file'] ?>" target="_blank"><?= $item['eer_file'] ?></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
