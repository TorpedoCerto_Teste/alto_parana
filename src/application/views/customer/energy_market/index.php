<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Energy Market
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Energy Market</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Energy Market
                    <span class="tools pull-right">
                    </span>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th style="display:none">Ano/Mês (referencia)</th>
                                <th>Ano/Mês</th>
                                <th>Semana</th>
                                <th>Indicadores</th>
                                <th>Preços</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td style="display:none"><?= $item['year'] . str_pad($item['month'], 2, '0', STR_PAD_LEFT) ?></td>
                                    <td><?= $item['year'] . "/" . retorna_mes($item['month']) ?></td>
                                    <td><?= $item['week'] ?></td>
                                    <td>
                                        <?php if ($item['energy_market_file_indicator']) : ?>
                                            <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/energy_market/<?= $item['pk_energy_market'] ?>/<?= $item['energy_market_file_indicator'] ?>" class="btn btn-info btn-xs" target="_blank"><?= $item['energy_market_file_indicator'] ?></a>
                                        <?php else: ?>
                                            <a href="#" class="btn btn-dark btn-xs">Sem arquivo</a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($item['energy_market_file_price']) : ?>
                                            <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/energy_market/<?= $item['pk_energy_market'] ?>/<?= $item['energy_market_file_price'] ?>" class="btn btn-info btn-xs" target="_blank"><?= $item['energy_market_file_price'] ?></a>
                                        <?php else: ?>
                                            <a href="#" class="btn btn-dark btn-xs">Sem arquivo</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>

