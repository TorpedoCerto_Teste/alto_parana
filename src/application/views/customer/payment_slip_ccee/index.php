<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Boleto CCEE
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Boleto CCEE</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Boleto CCEE
                </header>

                <table class="table colvis-data-table data-table">
                    <thead>
                        <tr>
                            <th>Mês/Ano</th>
                            <th>Valor</th>
                            <th>Data Vencimento</th>
                            <th>PDF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($list) : ?>
                            <?php foreach ($list as $boleto) : ?>
                                <tr>
                                    <td><?= $boleto['month'] ?>/<?= $boleto['year'] ?></td>
                                    <td><?= number_format($boleto['value'], 2, ",", "") ?></td>
                                    <td><?= date_to_human_date($boleto['validate']) ?></td>
                                    <td><a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/payment_slip_ccee/<?= $boleto['pk_payment_slip_ccee'] ?>/<?= $boleto['payment_slip_file'] ?>" target="_blank"><?= $boleto['payment_slip_file'] ?></a></td>
                                </tr>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>

            </section>
        </div>

    </div>
</div>