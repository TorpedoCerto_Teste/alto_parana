<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contabilização
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Contabilização</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Selecione um filtro para o relatório.
                    </div>
                <?php else : ?>
                    <table class="table colvis-data-table data-table">
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Tipo</th>
                                <th>Valor a Liquidar (R$)</th>
                                <th>Data Crédito</th>
                                <th>Data Débito</th>
                                <th>SUM001</th>
                                <th>Valor Liquidado (R$)</th>
                                <th>LFN002</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['month'] ?>/<?= $item['year'] ?></td>
                                    <td><?= $item['type'] ?></td>
                                    <td><?= number_format($item['value'], 2, ",", "") ?></td>
                                    <td><?= date_to_human_date($item['credit_date']) ?></td>
                                    <td><?= date_to_human_date($item['debit_date']) ?></td>
                                    <td>
                                        <?php if ($item['sum001'] != "") : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/accounting/<?= $item['pk_accounting'] ?>/<?= $item['sum001'] ?>" target="_blank"><?= $item['sum001'] ?></a>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= number_format($item['value_paid'], 2, ",", "") ?></td>    
                                    <td>
                                        <?php if ($item['lfn002'] != "") : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/accounting/<?= $item['pk_accounting'] ?>/<?= $item['lfn002'] ?>" target="_blank"><?= $item['lfn002'] ?></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
