<!-- page head start-->
<div class="page-head">
    <h3>
        Dashboard de Medição
    </h3>
    <div class="state-information">
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <?php if ($gauges) : ?>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Filtros
                        <span class="tools pull-right">
                            <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>customer/dashboard_measurement" id="frm_dashboard" name="frm_dashboard">
                            <input type="hidden" name="filter" id="filter" value="true" />
                            <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled">
                            <input type="hidden" name="date" id="date" value="<?= $date ?>">

                            <div class="form-group">
                                <div class="col-lg-2 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                    <span class="help-block">Mês</span>
                                    <select id="month" name="month" class="form-control">
                                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                                            <option value="<?= $i ?>" <?= date("m", strtotime($date)) == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                                <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                    <span class="help-block">Ano</span>
                                    <input type="text" id="year" name="year" class="form-control" value="<?= date("Y", strtotime($date)) ?>" alt="year"
                                           data-bts-min="2010"
                                           data-bts-max="<?= date("Y") ?>"
                                           data-bts-step="1"
                                           data-bts-decimal="0"
                                           data-bts-step-interval="1"
                                           data-bts-force-step-divisibility="round"
                                           data-bts-step-interval-delay="500"
                                           data-bts-booster="true"
                                           data-bts-boostat="10"
                                           data-bts-max-boosted-step="false"
                                           data-bts-mousewheel="true"
                                           data-bts-button-down-class="btn btn-default"
                                           data-bts-button-up-class="btn btn-default"
                                           />
                                </div>
                                <div class="col-lg-3">
                                    <span class="help-block">Unidade</span>
                                    <select id="fk_unity" name="fk_unity" class="form-control select2">
                                        <option></option>
                                        <?php if ($unities) : ?>
                                            <?php foreach ($unities as $unity) : ?>
                                                <option value="<?= $unity['pk_unity'] ?>" <?= set_value('fk_unity') == $unity['pk_unity'] || count($unities) == 1 ? "selected" : "" ?>><?= $unity['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Ponto de Medição</span>
                                    <select id="fk_gauge" name="fk_gauge" class="form-control select2">
                                        <option></option>
                                        <?php if ($gauges) : ?>
                                            <?php foreach ($gauges as $gauge) : ?>
                                                <option value="<?= $gauge['pk_gauge'] ?>"  <?= set_value('fk_gauge') == $gauge['pk_gauge'] || count($gauges) == 1 ? "selected" : "" ?>><?= $gauge['internal_name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                        </form>

                    </div>
                </section>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-4">
                <section class="panel">
                    <div class="panel-body">
                        <div class="value"  style='padding-left: 0;'>
                            <p>Tipo TUSD</p>
                            <hr>
                            <div class="<?= $this->Demand_model->_tariff_mode == "AZUL" ? "btn-info" : "btn-success" ?>" style="text-align:center;">
                                <h3 style="font-size: 65px;">
                                    <?= $this->Demand_model->_tariff_mode ?>
                                </h3>
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel">
                    <div class="panel-body">
                        <div class="value"  style='padding-left: 0;'>
                            <p>Demanda Medida Ponta (kW)</p>
                            <hr>
                            <div style="text-align:center;">
                                <h3 style="font-size: 65px;">
                                    <?= round($demanda_medida_ponta) ?> 
                                </h3>
                                <div class="progress tooltips" style="margin-bottom: 5px;" data-placement="bottom" data-toggle="tooltip" data-original-title="Demanda Contratada: <?= round($this->Demand_model->_end_demand) ?> kW">
                                    <?php $percent = isset($this->Demand_model->_end_demand) && $this->Demand_model->_end_demand > 0 ? ($demanda_medida_ponta * 100) / $this->Demand_model->_end_demand  : 0?>
                                    <div class="progress-bar progress-bar-<?= $percent < 100 ? 'success' : 'danger' ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent < 100 ? ceil($percent) : 100 ?>%;"> <?= ceil($percent) ?>% </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel">
                    <div class="panel-body">
                        <div class="value"  style='padding-left: 0;'>
                            <p>Demanda Medida Fora Ponta (kW)</p>
                            <hr> 
                            <div style="text-align:center;">
                                <h3 style="font-size: 65px;">
                                    <?= round($demanda_medida_fora_ponta) ?>
                                </h3>
                                <div class="progress tooltips" style="margin-bottom: 5px;" data-placement="bottom" data-toggle="tooltip" data-original-title="Demanda Contratada: <?= round($this->Demand_model->_demand_tip_out) ?> kW">
                                    <?php $percent = isset($this->Demand_model->_demand_tip_out) && $this->Demand_model->_demand_tip_out > 0 ? ($demanda_medida_fora_ponta * 100) / $this->Demand_model->_demand_tip_out : 0 ?>
                                    <div class="progress-bar progress-bar-<?= $percent < 100 ? 'success' : 'danger' ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent < 100 ? ceil($percent) : 100 ?>%;"> <?= ceil($percent) ?>% </div> 
                                </div>                            
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <section class="panel">
                    <div class="panel-body">
                        <div class="value"  style='padding-left: 0;'>
                            <p>Consumo Mensal (MWh)
                                <span class="tools pull-right tooltips" data-toggle="tooltip" data-original-title="Informe a meta de consumo">
                                    <a class="fa fa-plus-square box-refresh" href='#unity_consumption_limit_upsert' data-toggle="modal"></a>
                                </span>
                            </p>
                            <hr> 
                            <div style="text-align:center;">
                                <div id="div_consumo_mensal" style="display: none"><?= $consumo_mensal ?></div>
                                <h3 style="font-size: 65px;">
                                    <?= number_format($consumo_mensal, 3, ",", ".") ?> 
                                </h3>
                                <div id="div_limite_consumo">
                                    <?php if ($limite_consumo) : $percent = $consumo_mensal > 0 && $limite_consumo[0]['consumption_limit'] > 0 ? ($consumo_mensal * 100) / $limite_consumo[0]['consumption_limit'] : 0; ?>
                                        <div class="progress tooltips" style="margin-bottom: 5px;" data-placement="bottom" data-toggle="tooltip" data-original-title="Meta de consumo informada: <?= number_format($limite_consumo[0]['consumption_limit'], 3, ",", "") ?> MWh">
                                            <div class="progress-bar progress-bar-<?= $percent < 100 ? 'success' : 'danger' ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent < 100 ? ceil($percent) : 100 ?>%;"> <?= ceil($percent) ?>% </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel">
                    <div class="panel-body">
                        <div class="value"  style='padding-left: 0;'>
                            <p>Fator Carga Ponta</p>
                            <hr>
                            <div style="text-align:center;">
                                <h3 style="font-size: 65px;">
                                    <?= number_format($fator_carga_ponta, 3, ",", "") ?>
                                </h3>
                                <?php if (($this->Demand_model->_tariff_mode == "AZUL" && $fator_carga_ponta > 0.65) || ($this->Demand_model->_tariff_mode == "VERDE" && $fator_carga_ponta < 0.65)) : ?>
                                    <span class="label label-success">OK</span>
                                <?php else : ?>
                                    <span class="label label-warning">Revisar TUSD</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel">
                    <div class="panel-body">
                        <div class="value"  style='padding-left: 0;'>
                            <p>Fator Carga Fora Ponta</p>
                            <hr>
                            <div style="text-align:center;">
                                <h3 style="font-size: 65px;">
                                    <?= number_format($fator_carga_fora_ponta, 3, ",", "") ?>
                                </h3>
                                <?php if (($this->Demand_model->_tariff_mode == "AZUL" && $fator_carga_fora_ponta > 0.65) || ($this->Demand_model->_tariff_mode == "VERDE" && $fator_carga_fora_ponta < 0.65)) : ?>
                                    <span class="label label-success">OK</span>
                                <?php else : ?>
                                    <span class="label label-warning">Revisar TUSD</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div> 

        <!--GRÁFICOS-->
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Consumo Diário no Mês
                        <span class="tools pull-right">
                            <div class="btn-row">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("-1 months", strtotime($date))) ?>"><i class="fa fa-backward"></i></button>
                                        <input type="text" class="btn btn-info active" value="<?= retorna_mes(date("m", strtotime($date))) ?>/<?= date("Y", strtotime($date)) ?>">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("+1 month", strtotime($date))) ?>"><i class="fa fa-forward"></i></button>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="consumo_diario_mes_chart">
                        </div>
                        <div class="row earning-chart-info">
                            <div class="col-sm-3 col-xs-6">
                                <h4><?= number_format($resumo_grafico_consumo_diario_mes['consumo_MWh'], 3, ",", "") ?></h4>
                                <small class="text-muted"> Consumo MWh</small>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <h4><?= number_format($resumo_grafico_consumo_diario_mes['consumo_MWm'], 3, ",", "") ?></h4>
                                <small class="text-muted">Consumo MWm</small>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <h4><?= number_format($resumo_grafico_consumo_diario_mes['horas_medidas'], 3, ",", "") ?></h4>
                                <small class="text-muted">Horas Medidas</small>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <h4><?= number_format($resumo_grafico_consumo_diario_mes['horas_inconsistentes'], 3, ",", "") ?></h4>
                                <small class="text-muted">Horas Inconsist.</small>
                            </div>
                        </div>                
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Consumo Horário no Dia
                        <span class="tools pull-right">
                            <div class="btn-row">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-d", strtotime("-1 days", strtotime($date))) ?>"><i class="fa fa-backward"></i></button>
                                        <input type="text" class="btn btn-info active calendar-date-picker" name="consumo_horario_dia_date" id="consumo_horario_dia_date" value="<?= date("d/m/Y", strtotime($date)) ?>">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-d", strtotime("+1 days", strtotime($date))) ?>"><i class="fa fa-forward"></i></button>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="consumo_horario_dia_chart">
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Demanda Medida Diária
                        <span class="tools pull-right">
                            <div class="btn-row">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-d", strtotime("-1 days", strtotime($date))) ?>"><i class="fa fa-backward"></i></button>
                                        <input type="text" class="btn btn-info active calendar-date-picker" name="consumo_horario_dia_date" id="consumo_horario_dia_date" value="<?= date("d/m/Y", strtotime($date)) ?>">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-d", strtotime("+1 days", strtotime($date))) ?>"><i class="fa fa-forward"></i></button>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="demanda_medida_diaria_chart" style="height: 350px">
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <section class="panel">
                    <header class="panel-heading">
                        Demandas Máximas Diárias No Mês
                        <span class="tools pull-right">
                            <div class="btn-row">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("-1 months", strtotime($date))) ?>"><i class="fa fa-backward"></i></button>
                                        <input type="text" class="btn btn-info active" value="<?= retorna_mes(date("m", strtotime($date))) ?>/<?= date("Y", strtotime($date)) ?>">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("+1 month", strtotime($date))) ?>"><i class="fa fa-forward"></i></button>
                                    </div>
                                </div>
                            </div>
                        </span>

                    </header>
                    <div class="panel-body">
                        <div id="demandas_maximas_diarias_mes_chart" style="height: 350px">
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-md-4">
                <section class="panel">
                    <header class="panel-heading">
                        Consumo por Posto Tarifário
                        <span class="tools pull-right">
                            <div class="btn-row">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("-1 months", strtotime($date))) ?>"><i class="fa fa-backward"></i></button>
                                        <input type="text" class="btn btn-info active" value="<?= retorna_mes(date("m", strtotime($date))) ?>/<?= date("Y", strtotime($date)) ?>">
                                        <button class="btn btn-info muda_data" type="button" date="<?= date("Y-m-01", strtotime("+1 month", strtotime($date))) ?>"><i class="fa fa-forward"></i></button>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="consumo_posto_tarifario_chart" style="height: 350px">
                        </div>
                    </div>
                </section>
            </div>
        </div> 
    <?php else: ?>
        <div class="alert alert-danger fade in">
            <button type="button" class="close close-sm" data-dismiss="alert">
                <i class="fa fa-times"></i>
            </button>
            <strong>Atenção!</strong> Não existe medidor registrado para exibição de dados. 
        </div>
    <?php endif; ?>


    <div class="row">
    </div>                
</div>
<!--body wrapper end-->


<!--footer section start-->
<footer>
</footer>
<!--footer section end-->



</div>
<!-- body content end-->

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="unity_consumption_limit_upsert" class="modal fade">
    <form method="POST" action="<?= base_url() ?>unity_consumption_limit/upsert">
        <input type="hidden" name="upsert" id="upsert" value="true" />
        <input type="hidden" name="unity_consumption_limit_month" id="unity_consumption_limit_month" value="<?= date("m", strtotime($date)) ?>" />
        <input type="hidden" name="unity_consumption_limit_year" id="unity_consumption_limit_year" value="<?= date("Y", strtotime($date)) ?>" />
        <input type="hidden" name="unity_consumption_limit_fk_unity" id="unity_consumption_limit_fk_unity" value="<?= $this->Unity_model->_pk_unity ?>" />
        <input type="hidden" name="pk_unity_consumption_limit" id="pk_unity_consumption_limit" value="<?= $limite_consumo[0]['pk_unity_consumption_limit'] ?>" />
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Informe a Meta de Consumo</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label" for="unity_consumption_limit">Meta (MWh)</label>
                        <div class="col-lg-10">
                            <input type="text" placeholder="" id="consumption_limit" name="consumption_limit" class="form-control" value="<?= number_format($limite_consumo[0]['consumption_limit'], 3, ",", "") ?>" alt="valor3">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-success btn_unity_consumption_limit" type="button">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
