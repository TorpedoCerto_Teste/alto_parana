<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Geral / Energia / Pagamento / Unidades / Garantias
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>customer/ccvee_general">CCVEE</a></li>
            <li class="active">Visualizar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">

            <!--tab nav start-->
            <section class="panel">
                <div class="panel-body">

                    <table class="dataTable table-hover table-responsive">
                        <tbody>
                            <tr>
                                <td colspan="2"><b>Informações Gerais</b></td>
                            </tr>
                            <tr>
                                <td width="200px">Comprador</td>
                                <td><?= $ccvee_general["unity_name"] ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Vendedor</td>
                                <td><?= $ccvee_general["counterpart"] ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Prazo</td>
                                <td><?= $ccvee_general["cplp"] == "CP" ? "CURTO PRAZO" : "LONGO PRAZO" ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Tipo de Energia</td>
                                <td><?= $ccvee_general["energy_source"] ?> <?= $ccvee_general["energy_type"] ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Submercado</td>
                                <td><?= $this->Submarket_model->_submarket ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Retusd (R$/MWh)</td>
                                <td><?= number_format($ccvee_general["retusd"], 2, ",", ".") ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Quantidade e Período de Fornecimento</b></td>
                            </tr>
                            <tr>
                                <td width="200px">Início do Fornecimento</td>
                                <td><?= date_to_human_date($this->Ccvee_energy_model->_supply_start) ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Término do Fornecimento</td>
                                <td><?= date_to_human_date($this->Ccvee_energy_model->_supply_end) ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Perdas (%)</td>
                                <td><?= $this->Ccvee_energy_model->_losses ?></td>
                            </tr>
                            <?php if ($energy_bulks) : ?>
                                <tr>
                                    <td colspan="2">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Data</th>
                                                    <th>Previsto</th>
                                                    <th>Sazonalizado</th>
                                                    <th>Ajustado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($energy_bulks as $energy_bulk) : ?>
                                                    <tr>
                                                        <td width="100px"><?= date_to_human_date($energy_bulk['date']) ?></td>
                                                        <td width="100px"><?= $energy_bulk['forecast'] ?></td>
                                                        <td width="100px"><?= $energy_bulk['seasonal'] ?></td>
                                                        <td width="100px"><?= $energy_bulk['adjusted'] ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td width="200px">Considerar PROINFA</td>
                                <td><?= $this->Ccvee_energy_model->_discount_proinfa == 1 ? "SIM" : "NÃO" ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Sazonalidade Inferior (%)</td>
                                <td><?= $this->Ccvee_energy_model->_seasonal_inferior ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Sazonalidade Superior (%)</td>
                                <td><?= $this->Ccvee_energy_model->_seasonal_superior ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Flexibilidade Inferior (%)</td>
                                <td><?= $this->Ccvee_energy_model->_flexibility_inferior ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Flexibilidade Superior (%)</td>
                                <td><?= $this->Ccvee_energy_model->_flexibility_superior ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Tipo de Modulação</td>
                                <td><?= strtoupper($this->Ccvee_energy_model->_modulation) ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Pagamentos</b></td>
                            </tr>
                            <tr>
                                <td width="200px">Tipo de dia Vencimento</td>
                                <td><?= strtoupper(str_replace("_", " ", $this->Ccvee_payment_model->_due)) ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Dia Vencimento</td>
                                <td><?= $this->Ccvee_payment_model->_due_day ?></td>
                            </tr>
                            <tr>
                                <td width="200px">Data Base</td>
                                <td><?= date_to_human_date($this->Ccvee_payment_model->_database) ?></td>
                            </tr>
                            <?php if ($payment_unities) : ?>
                                <tr>
                                    <td colspan="2">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th width="100px">Período</th>
                                                    <th width="100px">Valor Base</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($payment_unities as $payment_unity) : ?>
                                                    <tr>
                                                        <td><?= date_to_human_date($payment_unity['period']) ?></td>
                                                        <td><?= number_format($payment_unity['provided_base'], 2, ",", ".") ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td colspan="2"><b>Unidades</b></td>
                            </tr>
                            <?php if ($unities) : ?>
                                <?php foreach ($unities as $unity) : ?>
                                    <tr>
                                        <td width="200px"><?= $unity['unity_community_name'] ?></td>
                                        <td><?= $unity['contract_cliqccee'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <tr>
                                <td colspan="2"><b>Garantia Financeira</b></td>
                            </tr>
                            <?php if ($warranties) : ?>
                                <?php foreach ($warranties as $warranty) : ?>
                                    <tr>
                                        <td width="200px">Modalidade</td>
                                        <td><?= $warranty['modality'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="200px">Data Limite Apresentação</td>
                                        <td><?= date_to_human_date($warranty['deadline_contract_presentation']) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <!--tab nav start-->

        </div>

    </div>
</div>
