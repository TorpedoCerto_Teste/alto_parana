<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        CCVEE
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">CCVEE</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    CCVEE
                    <span class="tools pull-right">
                    </span>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Nº Documento</th>
                                <th>Unidade</th>
                                <th>Contraparte</th>
                                <th>Operação</th>
                                <th>Prazo</th>
                                <th>PDF</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><a href="<?= base_url() ?>customer/ccvee_general/view/<?= $item['pk_ccvee_general'] ?>"><?= $item['document_number'] ?></a></td>
                                    <td><?= $item['unity_name'] ?></td>
                                    <td><?= $item['counterpart'] ?></td>
                                    <td><?= $item['contract_type'] ?></td>
                                    <td><?= $item['cplp'] == "CP" ? "Curto Prazo" : "Longo Prazo" ?></td>
                                    <td>
                                        <?php if ($item['ccvee_general_file']) : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_general/<?= $item['pk_ccvee_general'] ?>/<?= $item['ccvee_general_file'] ?>" target="_blank"><?= $item['ccvee_general_file'] ?></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>

