<div id="app">
    <!-- page head start-->
    <div class="page-head">
        <h3>
            Dashboard 
        </h3>
        <div class="state-information">
        </div>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Filtros
                        <span class="tools pull-right">
                            <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal" method="POST" action="" id="frm_dashboard" name="frm_dashboard">
                            <input type="hidden" name="filter" id="filter" value="true" />
                            <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled">

                            <div class="form-group">

                                <div class="col-lg-2 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                    <span class="help-block">Mês</span>
                                    <select id="month" name="month" class="form-control" v-model="filter.month">
                                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                                            <option value="<?= $i ?>"><?= retorna_mes($i) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>

                                <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                    <span class="help-block">Ano</span>
                                    <input type="number" id="year" name="year" class="form-control" v-bind:class="{'error': String(this.filter.year).length !== 4}" value="" alt="year" maxlength="4" v-model="filter.year" />
                                </div>

                                <div class="col-lg-3">
                                    <span class="help-block">CCVEE</span>
                                    <select id="pk_ccvee_general" name="pk_ccvee_general" class="form-control" v-model="filter.ccvee" v-bind:disabled="!ccvee_generals.length">
                                        <option v-if="ccvee_generals.length > 1" v-bind:value="0" v-bind:selected="!filter.ccvee || parseInt(filter.ccvee) == 0">Selecione um contrato</option>
                                        <option v-for="(ccvee_general, index) of ccvee_generals" v-show="checkSupplyEnd(ccvee_general)" v-bind:value="ccvee_general.pk_ccvee_general">{{ccvee_general.document_number}} ({{ccvee_general.unities}} unidade{{parseInt(ccvee_general.unities) > 1 ? 's' : ''}})</option>
                                    </select>
                                </div>

                                <div class="col-lg-2" v-if="parseInt(ccvee_unities) > 1 && !_.isUndefined(resumeModal) && !_.isEmpty(resumeModal)">
                                    <span class="help-block">&nbsp;</span>
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_unities">Ver consumo por unidade</button>
                                </div>

                                <div class="col-lg-2" style="display: none">
                                    <span class="help-block">Origem da Medição</span>
                                    <select id="source" name="source" class="form-control" v-model="filter.source">
                                        <option value="w2">Telemetria</option>
                                        <option value="scde">SCDE MC</option>
                                        <option value="odc">SCDE ODC</option>
                                    </select>
                                </div>

                                <div class="col-lg-1" style="display: none">
                                    <span class="help-block">Hora DST</span>
                                    <select id="hour" name="hour" class="form-control" v-model="filter.hour">
                                        <option value="+1">+1</option> 
                                        <option value="0">0</option>
                                        <option value="-1">-1</option>
                                    </select>
                                </div>

                                <div class="col-lg-1">
                                    <span class="help-block">&nbsp;</span>
                                    <button class="btn btn-success" v-bind:disabled="!validateFilter || parseInt(filter.ccvee) <= 0" @click.prevent.stop="startProcess">Aplicar</button>
                                </div>

                                <div class="col-lg-1" v-show="validateFilter">
                                    <span class="help-block">&nbsp;</span>
                                    <a class="btn btn-info" :href="`<?=base_url()?>customer/dashboard_pdf?month=${filter.month}&year=${filter.year}&ccvee=${filter.ccvee}&agent_token=${agentToken}`" target="_blank">PDF</a>
                                </div>

                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div> 

        <template v-if="!validateFilter || parseInt(filter.ccvee) == 0 || !filtered">
            <div class="alert alert-warning fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <strong>Atenção!</strong> Não há dados suficientes para geração de gráficos. 
            </div>
        </template>
        <template v-else>
            <!--GRÁFICOS-->
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Atendimento de Contrato
                            <template v-if="unities.length === 1">(Unidade: {{unities[0].community_name}})</template>
                            <span class="tools pull-right">
                                <a class="fa fa-arrows-alt" v-on:click="openGraphNewTab()"  href="javascript:;"></a>
                                <div class="btn-row">
                                    <div class="btn-toolbar">
                                    </div>
                                </div>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div v-show="show_loading_chart" class="background_loader_chart">
                                <div id="loading" class="center">
                                    <div class="loader"></div>
                                </div>
                            </div>
                            
                            <div style="width: 100%; height: 300px !important">
                                <canvas id="mixedchart"></canvas>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Resumo Energético
                            <span class="tools pull-right">
                                <select v-model="conversion">
                                    <option value="MWh">MWh</option>
                                    <option value="MWm">MWm</option>
                                </select>
                            </span>
                        </header>
                        <div class="panel-body" style="background-color: #f0f0f0">

                            <div class="row">
                                <div class="col-md-4">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>CONSUMO ({{conversion}})</p>
                                                <hr>
                                                <div class="btn-info" style="text-align:center;">
                                                    <h3 style="font-size: 65px;">{{floatToString((widgets.consumoLiquido), conversion == 'MWh' ? 3 : 6)}}</h3>
                                                    <p v-if="widgets.consumoLiquidoProjetado >= 0"  style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.consumoLiquidoProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                                </div>
                                                <div class="progress" style="margin-bottom: 0;">
                                                    <div v-bind:class="{'progress-bar-success': widgets.consumoversuscontrato < 100, 'progress-bar-danger': widgets.consumoversuscontrato >= 100}"  class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: widgets.consumoversuscontrato + '%' }" > {{widgets.consumoversuscontrato}}% </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-4">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>PERDAS ({{conversion}}) {{widgets.perdasCcvee}}%</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 65px;">{{floatToString(widgets.perdas, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                    <p v-if="widgets.perdasProjetado >= 0" style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.perdasProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-4">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>PROINFA ({{conversion}})</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 65px;">{{floatToString(widgets.proinfa, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                                
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>CONTRATO ({{conversion}})</p>
                                                <hr>
                                                <div class="btn-success" style="text-align:center;">
                                                    <h3 style="font-size: 55px;">{{floatToString(widgets.contrato, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>FLEXIBILIDADE INFERIOR ({{conversion}}) {{widgets.percentFlexInferior}}%</p>
                                                <hr>
                                                <div class="btn-warning" style="text-align:center;">
                                                    <h3 style="font-size: 55px;">{{floatToString(widgets.flexInferior, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>FLEXIBILIDADE SUPERIOR ({{conversion}}) {{widgets.percentFlexSuperior}}%</p>
                                                <hr>
                                                <div class="btn-danger" style="text-align:center;">
                                                    <h3 style="font-size: 55px;">{{floatToString(widgets.flexSuperior, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>    

                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>% ATENDIMENTO</p>
                                                <hr>
                                                <div style="text-align:center; text-align:center;background-color: #f0f0f0;">
                                                    <h3 style="font-size: 55px;">{{floatToString(widgets.attendance, 2)}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                                  
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <template v-if="unities.length === 1">
                                                    <p>HORAS MEDIDAS</p>
                                                    <hr>
                                                    <div style="text-align:center;background-color: #f0f0f0">
                                                        <h3 style="font-size: 65px;">{{widgets.horasMedidas}}/{{widgets.totalHorasMes}}</h3>
                                                        &nbsp;
                                                    </div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: this.widgets.avgHorasMedidas + '%' }" >  {{Math.ceil(this.widgets.avgHorasMedidas)}}% </div> 
                                                    </div>
                                                </template>
                                                <template v-else>
                                                    <p>HORAS NO MÊS</p>
                                                    <hr>
                                                    <div style="text-align:center;background-color: #f0f0f0">
                                                        <h3 style="font-size: 65px;">{{widgets.totalHorasMes}}</h3>
                                                        &nbsp;
                                                    </div>
                                                </template>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <template v-if="unities.length === 1">
                                                <div class="value" style="padding-left: 0;">
                                                    <p>HORAS FALTANTES</p>
                                                    <hr>
                                                    <div style="text-align:center;background-color: #f0f0f0">
                                                        <h3 style="font-size: 65px;">{{widgets.horasFaltantes}}</h3>
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </template>
                                            <template v-else>
                                                <div class="value" style="padding-left: 0;">
                                                    <p>SITUAÇÃO MEDIÇÃO</p>
                                                    <hr>
                                                    <div style="text-align:center;background-color: #f0f0f0">
                                                        <template v-if="parseInt(widgets.horasFaltantes) > 0">
                                                            <h3 style="font-size: 25px;">Horas Faltantes</h3>
                                                        </template>
                                                        <template v-else>
                                                            <h3 style="font-size: 25px;">Consistido</h3>
                                                        </template>
                                                        <h3 style="font-size: 40px;">&nbsp;</h3>
                                                    </div>
                                                </div>
                                            </template>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>SOBRA ({{conversion}})</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 60px;">{{floatToString(widgets.sobra, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                    <p v-if="widgets.sobraProjetado >= 0" style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.sobraProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>        

                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>DÉFICIT ({{conversion}})</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0">
                                                    <h3 style="font-size: 60px;">{{floatToString(widgets.deficit, conversion == 'MWh' ? 3 : 6)}}</h3>
                                                    <p v-if="widgets.deficitProjetado >= 0" style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{floatToString(widgets.deficitProjetado, conversion == 'MWh' ? 3 : 6)}} (projetado)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                                
                            </div>


                        </div>
                    </section>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Resumo Financeiro por Contrato
                            <span class="tools pull-right">
                            </span>
                        </header>
                        <div class="panel-body" style="background-color: #f0f0f0">
                            <!-- submercado / pld  -->
                            <div class="row">
                                <div class="col-md-4">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>SUBMERCADO</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0; min-height: 150px">
                                                    <h3 style="font-size: 65px;">{{widgets.submarket}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-4">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>PLD (R$/MWh)</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0; min-height: 150px">
                                                    <h3 style="font-size: 65px;">R$ {{floatToString(widgets.pld.pld_value, 2)}}</h3>
                                                    <p style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{widgets.pld.status}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div> 
                                
                                <div class="col-md-4">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>SPREAD EM RELAÇÃO PLD (MÉDIA ESTIMADA)</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0; text-align:center; min-height: 150px">
                                                    <h3 style="font-size: 55px;">R$ {{floatToString(widgets.pld.spread, 2)}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                            </div>
                            <!-- /submercado / pld  -->

                            <!-- Se Liquidar / Spread em Relacao PLD / Venda/Compra de Curto Prazo -->
                            <div class="row">
                                <div class="col-md-6">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>LIQUIDAÇÃO PREVISTA EM CASO DE SOBRA</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0; text-align:center; min-height: 180px">
                                                    <h3 style="font-size: 55px;">R$ {{floatToString(widgets.pld_if_liquidate, 2)}}</h3>
                                                    <p style="margin: 0; font-size: 20px; padding-bottom: 10px;">{{widgets.pld.status}}</p>
                                                    <p style="margin: 0; font-size: 20px; padding-bottom: 10px;">DATA LIQUIDAÇÃO: {{widgets.credit_date}}</p>
                                                    <p style="margin: 0; font-size: 15px; padding-bottom: 10px;">OBS. Considera o crédito da sobra projetada, caso opte por não vender e liquidar na CCEE.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                    

                                <div class="col-md-6">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="value" style="padding-left: 0;">
                                                <p>{{ widgets.short_term_text1 }} DE CURTO PRAZO</p>
                                                <hr>
                                                <div style="text-align:center;background-color: #f0f0f0; text-align:center; min-height: 180px;">
                                                    <h3 style="font-size: 55px;"><span style="font-size: 20px">PREÇO R$/MWh:</span> {{floatToString(widgets.short_term_price, 2)}}</h3>
                                                    <h3 style="font-size: 55px;"><span style="font-size: 20px">TOTAL R$:</span> {{floatToString(widgets.short_term_total)}}</h3>
                                                    <p style="margin: 0; font-size: 15px; padding-bottom: 10px;">OBS. {{ widgets.short_term_text2 }} no 6º dia útil</p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>    

                            </div>
                            <!-- /Se Liquidar / Spread em Relacao PLD / Venda/Compra de Curto Prazo -->

                        </div>
                    </section>
                </div>
            </div>       
        </template>

        <div class="row">
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        Mapa de Ventos
                        <span class="tools pull-right">
                            <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <iframe width="100%" height="600px" src="https://embed.windy.com/embed2.html?lat=-25.958&lon=-53.438&zoom=3&level=surface&overlay=wind&menu=&message=&marker=&forecast=12&calendar=now&location=coordinates&type=map&actualGrid=&metricWind=km%2Fh&metricTemp=%C2%B0C" frameborder="0"></iframe>
                    </div>
                </section>
            </div>

            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        Imagem de Satélite
                        <span class="tools pull-right">
                            <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <img src="https://s0.cptec.inpe.br/grafico/Satelite/goes16/realcadas/pagina/g16ch13_cptec.jpg" width="100%" height="600px"  border="0" alt="Satélite">
                    </div>
                </section>
            </div>

        </div>          


    </div>
    <!--body wrapper end-->


    <!-- Modal Consumo das Unidades -->
    <div id="modal_unities" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header btn-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Consumo por unidade</h4>
                </div>
                <div class="modal-body">

                    <section class="panel">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Unidade</th>
                                    <th>Consumo ({{conversion}})</th>
                                    <th>Perdas ({{conversion}}) {{widgets.perdasCcvee}}%</th>
                                    <th>Proinfa ({{conversion}})</th>
                                    <th>Líquido ({{conversion}})</th> 
                                    <th>Horas Medidas</th>
                                    <th>Horas/Mês</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template v-if="!_.isUndefined(resumeModal) && !_.isEmpty(resumeModal)">
                                    <tr v-for="i of resumeModal">
                                        <th>{{i.unity_community_name}}</th>
                                        <td class="info">{{ (i.resume.consumo).replace(".", ",") }}</td>
                                        <td class="active">{{ (i.resume.perdas).replace(".", ",")}}</td>
                                        <td class="success">{{ (i.resume.proinfa).replace(".", ",") }}</td>
                                        <td class="warning">{{ (i.resume.liquido).replace(".", ",") }}</td>
                                        <td class="danger">{{ (i.resume.hours) }}</td>
                                        <th class="active">{{ (widgets.totalHorasMes) }}</th>
                                    </tr>
                                </template>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <th class="info">{{ (resumeModalTotal.consumo) }}</th>
                                    <th class="active">{{ (resumeModalTotal.perdas) }}</th>
                                    <th class="success">{{ (resumeModalTotal.proinfa) }}</th>
                                    <th class="warning">{{ (resumeModalTotal.liquido) }}</th>
                                    <th class="danger">{{ (resumeModalTotal.hours) }}</th>
                                    <th class="active"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </section>                    


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>

        </div>
    </div>


</div>
<!-- body content end-->


