<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="" />
    <link rel="shortcut icon" href="<?= base_url() ?>img/ico/favicon.ico">

    <title>Alto Paraná Energia</title>

    <?php if (isset($css_include)) print_r($css_include); ?>

</head>

<body>
    <div id="app">
        <canvas id="mixedchart" style="display: block; height: 474px; width: 948px;" width="1422" height="711" class="chartjs-render-monitor"></canvas>
    </div>

    <?php if (isset($js_include)) print_r($js_include); ?>
</body>

</html>