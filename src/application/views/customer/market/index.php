<div class="wrapper">

    <div class="row">
        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    ENERGIA ARMAZENADA SE/CO - % DA MÉDIA HISTÓRICA
                    <span class="tools pull-right">
                        <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div id="combine-chart">
                        <div id="combine-chart-container" class="f-c-space">
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    ENERGIA NATURAL AFLUENTE - ENA - % DA MÉDIA HISTÓRICA
                    <span class="tools pull-right">
                        <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close fa fa-times" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="chartJS">
                        <div id="ena" class="f-c-space">
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    CARGA DE ENERGIA SE/CO (MW médio)
                    <span class="tools pull-right">
                        <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close fa fa-times" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="chartJS">
                        <div id="carga" class="f-c-space">
                    </div>
                </div>
            </section>
        </div>

        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    PLD SE/CO (R$ MW HORA)
                    <span class="tools pull-right">
                        <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                        <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close fa fa-times" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="chartJS">
                       <div id="pld" class="f-c-space">
                    </div>
                </div>
            </section>
        </div>

    </div>

</div>
