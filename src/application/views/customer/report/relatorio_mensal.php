<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Relatório Mensal
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Relatório Mensal</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Relatório Mensal
                </header>

                <table class="table colvis-data-table data-table">
                    <thead>
                        <tr>
                            <th>Mês/Ano</th>
                            <th>Percentual de Economia</th>
                            <th>PDF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1; $i<=12; $i++) : ?>
<!--                        <tr>
                            <td><?=str_pad($i, 2,"0", STR_PAD_LEFT)?>/2016</td>
                            <td>9,67%</td>
                            <td>
                                <a class="btn btn-success btn-xs" href = '<?= base_url() ?>pdf/relatorio_mensal/<?=str_pad($i, 2,"0", STR_PAD_LEFT)?>_2016_RELATORIO_CONSOLIDADO.pdf' target="_blank"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>-->
                        <?php endfor; ?>
                    </tbody>
                </table>

            </section>
        </div>

    </div>
</div>