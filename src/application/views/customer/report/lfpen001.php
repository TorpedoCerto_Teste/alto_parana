<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        LFPEN001
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">LFPEN001</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    LFPEN001
                </header>

                <table class="table colvis-data-table data-table">
                    <thead>
                        <tr>
                            <th>Unidade</th>
                            <th>Mês/Ano</th>
                            <th>PDF</th>
                            <th>Valor</th>
                            <th>Data de Pagamento</th>
                            <th>Sem penalidade</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1; $i<=12; $i++) : ?>
<!--                        <tr>
                            <td>PKO DO BRASIL</td>
                            <td><?=str_pad($i, 2,"0", STR_PAD_LEFT)?>/2017</td>
                            <td>
                                <a class="btn btn-success btn-xs" href = '<?= base_url() ?>pdf/lfpen001/devec.pdf' target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                            </td>
                            <td>R$ <?= number_format(rand(1000, 9999), 2, "", ".")?></td>
                            <td></td>
                            <td></td>
                        </tr>-->
                        <?php endfor; ?>
                    </tbody>
                </table>

            </section>
        </div>

    </div>
</div>