<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Faturamentos de Serviços
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Faturamentos de Serviços</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Faturamentos de Serviços
                </header>

                <table class="table responsive-data-table data-table">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>Mês</th>
                            <th>Data de validade</th>
                            <th>Total</th>
                            <th>Número do Contrato</th>
                            <th>Data de pagamento</th>
                            <th>Tipo de fatura</th>
                            <th>Boleto</th>
                            <th>NFe</th>
                            <th>XML</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </section>
        </div>

    </div>
</div>