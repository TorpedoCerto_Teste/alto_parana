<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        TUSD
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">TUSD</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    TUSD
                </header>

                <table class="table colvis-data-table data-table">
                    <thead>
                        <tr>
                            <th>Mês/Ano</th>
                            <th>Valor Total</th>
                            <th>ICMS</th>
                            <th>PIS</th>
                            <th>COFINS</th>
                            <th>PDF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1; $i<=12; $i++) : ?>
<!--                        <tr>
                            <td><?=str_pad($i, 2,"0", STR_PAD_LEFT)?>/2017</td>
                            <td>R$ 246.034,36</td>
                            <td>R$ 24.765,56</td>
                            <td>R$ 1.724,33</td>
                            <td>R$ 7.972,65</td>
                            <td>
                                <a class="btn btn-success btn-xs" href = '<?= base_url() ?>pdf/TUSD/TUSD.pdf' target="_blank"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>-->
                        <?php endfor; ?>
                    </tbody>
                </table>

            </section>
        </div>

    </div>
</div>