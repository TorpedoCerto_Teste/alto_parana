<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Boleto CCEE
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Boleto CCEE</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Boleto CCEE
                </header>

                <table class="table colvis-data-table data-table">
                    <thead>
                        <tr>
                            <th>Mês/Ano</th>
                            <th>Valor</th>
                            <th>Data Vencimento</th>
                            <th>PDF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1; $i<=12; $i++) : ?>
<!--                        <tr>
                            <td><?=str_pad($i, 2,"0", STR_PAD_LEFT)?>/2016</td>
                            <td>R$ 136,39</td>
                            <td>28/<?=str_pad($i, 2,"0", STR_PAD_LEFT)?>/2016</td>
                            <td>
                                <a class="btn btn-success btn-xs" href = '<?= base_url() ?>pdf/boleto_ccee/boleto_ccee.pdf' target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                            </td>
                        </tr>-->
                        <?php endfor; ?>
                    </tbody>
                </table>

            </section>
        </div>

    </div>
</div>