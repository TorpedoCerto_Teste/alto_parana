<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        TUSD
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">TUSD</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    TUSD
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Unidade</th>
                                <th>Mês/Ano</th>
                                <th>Valor de Referência</th>
                                <th>ICMS</th>
                                <th>PIS/COFINS</th>
                                <th>Anexos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['unity'] ?></td>
                                    <td><?= retorna_mes($item['month'])."/".$item['year']?></td>
                                    <td><?= number_format($item['value'], 2, ",", ".")  ?></td>
                                    <td><?= number_format($item['icms'], 2, ",", ".") ?></td>
                                    <td><?= number_format($item['piscofins'], 2, ",", ".")  ?></td>
                                    <td>
                                        <?php if ($files) : ?>
                                            <?php foreach ($files[$item['pk_tusd']] as $file) : ?>
                                                <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/tusd/<?= $item['pk_tusd'] ?>/<?= $file ?>" target="_blank"><?= $file ?></a>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
