<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        PLD
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">PLD</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

<div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    PLD
                </header>
                <div class="panel-body">

                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                    <input id="year"
                                           type="text"
                                           value="<?= set_value('year') != "" ? set_value('year') : $year ?>"
                                           name="year"
                                           data-bts-min="0"
                                           data-bts-max="9999"
                                           data-bts-init-val=""
                                           data-bts-step="1"
                                           data-bts-decimal="0"
                                           data-bts-force-step-divisibility="round"
                                           data-bts-prefix=""
                                           data-bts-postfix=""
                                           data-bts-prefix-extra-class=""
                                           data-bts-postfix-extra-class=""
                                           data-bts-booster="true"
                                           data-bts-boostat="10"
                                           data-bts-max-boosted-step="false"
                                           data-bts-mousewheel="true"
                                           data-bts-button-down-class="btn btn-default"
                                           data-bts-button-up-class="btn btn-default"
                                           alt="year" 
                                            />
                            </div>
                        </div>

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Mês</th>
                                    <th>SE/CO</th>
                                    <th>SUL</th>
                                    <th>NE</th>
                                    <th>NORTE</th>
                                    <th>Spread I5</th>
                                    <th>Spread I1</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($list as $data) : ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="data[<?= $data['month']?>][month]" value="<?= $data['month']?>">
                                            <span><?= $data['month_name']?></span>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" name="data[<?= $data['month']?>][seco]" class="form-control" value="<?= $data['seco']?>" alt="valor" disabled>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" name="data[<?= $data['month']?>][s]" class="form-control" value="<?= $data['s']?>" alt="valor" disabled>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" name="data[<?= $data['month']?>][ne]" class="form-control" value="<?= $data['ne']?>" alt="valor" disabled>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" name="data[<?= $data['month']?>][n]" class="form-control" value="<?= $data['n']?>" alt="valor" disabled>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" name="data[<?= $data['month']?>][spread_i5]" class="form-control" value="<?= $data['spread_i5']?>" alt="valor" disabled>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="" name="data[<?= $data['month']?>][spread_i1]" class="form-control" value="<?= $data['spread_i1']?>" alt="valor" disabled>
                                        </td>
                                        <td>
                                            <select class="form-control" name="data[<?= $data['month']?>][status_pld]" disabled>
                                                <option value="0" <?= $data['status_pld'] == 0 ? "selected" : ""?>>Previsto</option>
                                                <option value="1" <?= $data['status_pld'] == 1 ? "selected" : ""?>>Fechado</option>
                                            </select>
                                        </td>
                                    </tr>

                                <?php endforeach;?>

                            </tbody>
                        </table>

                </div>
            </section>
        </div>

    </div>
</div>

