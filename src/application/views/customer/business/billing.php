<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Negócios
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Negócios</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Negócios
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table colvis-data-table data-table">
                        <thead>
                            <tr>
                                <th>Ano</th>
                                <th>Mês</th>
                                <th>Unidade</th>
                                <th>Vencimento</th>
                                <th>Valor total</th>
                                <th>Data de pagamento</th>
                                <th>Status do pagamento</th>
                                <th>Boleto</th>
                                <th>NFE</th>
                                <th>XML</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['year'] ?></td>
                                    <td><?= retorna_mes($item['month']) ?></td>
                                    <td><?= $item['unity_name'] ?></td>
                                    <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                    <td><?= number_format($item['total'],2,",","") ?></td>
                                    <td><?= date_to_human_date($item['payment_date']) ?></td>
                                    <td>
                                        <?php if ($item['payment_status'] != "quitado" && preg_replace("/([^\d]*)/", "", $item['expiration_date']) < preg_replace("/([^\d]*)/", "", $item['payment_date'])) : ?>
                                            <span class="label label-danger">ATRASADO</span>
                                        <?php else: ?>
                                            <span class="label label-success">EM DIA</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($item['payment_slip'] != "") : ?>
                                            <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['payment_slip'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($item['invoice'] != "") : ?>
                                            <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['invoice'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($item['xml'] != "") : ?>
                                            <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business_billing/<?= $item['pk_business_billing'] ?>/<?= $item['xml'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                        <?php endif; ?>
                                    </td>

                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
