<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Negócios
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Negócios</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Negócios
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Unidade</th>
                                <th>Data Início</th>
                                <th>Data Fim</th>
                                <th>Preço Base (R$)</th>
                                <th>Anexo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['community_name'] ?></td>
                                    <td><?= date_to_human_date($item['start_date'])?></td>
                                    <td><?= date_to_human_date($item['end_date']) ?></td>
                                    <td><?= number_format($item['monthly_value'], 2, ",", ".") ?></td>
                                    <td>
                                        <?php if ($item['business_file']) : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/business/<?= $item['pk_business'] ?>/<?= $item['business_file'] ?>" target="_blank"><?= $item['business_file'] ?></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
