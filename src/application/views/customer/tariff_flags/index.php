<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Bandeiras Tarifárias
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Bandeiras Tarifárias</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Bandeiras Tarifárias
                </header>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>tariff_flags/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>tariff_flags/ajax/" disabled=""/>


                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input id="year"
                                       type="text"
                                       value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>"
                                       name="year"
                                       data-bts-min="0"
                                       data-bts-max="9999"
                                       data-bts-init-val=""
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-prefix=""
                                       data-bts-postfix=""
                                       data-bts-prefix-extra-class=""
                                       data-bts-postfix-extra-class=""
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       alt="year" 
                                       />
                            </div>
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Mês</th>
                                    <th>Bandeira</th>
                                    <th>Acréscimo (R$/MWh)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($tariffs as $month => $tariff) : ?>
                                    <tr id="tr_<?= $month ?>" class="alert-<?= $tariff['color'] ?>">
                                        <td><div id="month_<?= $month ?>"><?= retorna_mes($month) ?></div></td>
                                        <td><div id="flag_<?= $month ?>"><?= $tariff['flag'] ?></div></td>
                                        <td><div id="increase_<?= $month ?>"><?= number_format($tariff['increase'], 2, ',', '') ?></div></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>

<div class="modal fade" id="loading" role="dialog">
    <div class="modal-dialog">
        <img src="<?= base_url() ?>images/loader.gif" />
    </div>
</div>