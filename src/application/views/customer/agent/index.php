<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Contas
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Contas</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Contas
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: <?= (is_array($fk_agent_types) && !empty($fk_agent_types)) ? 'block' : 'none' ?>">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>customer/agent">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Tipo de Agente</label>
                                <div class="col-lg-6">
                                    <select class="form-control select2" multiple id="fk_agent_type" name="fk_agent_type[]" >
                                        <?php foreach ($agent_types as $type) : ?>
                                            <option value="<?= $type['pk_agent_type']?>" <?= in_array($type['pk_agent_type'], $fk_agent_types) ? 'selected' : '' ?>><?= $type['type'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success" type="submit">Filtrar</button>
                                    <?php if (is_array($fk_agent_types) && !empty($fk_agent_types)) : ?>
                                        <a href="<?= base_url() ?>customer/agent" class="btn btn-info">Resetar</a>
                                    <?php endif; ?>
                                </div>
                        </div>
                    </form>
                </div> 

                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table hover">
                        <thead>
                            <tr>
                                <th>
                                    Agente Consumidor
                                </th>
                                <th>
                                    Tipo
                                </th>
                                <th>
                                    Cidade/Estado
                                </th>
                                <th>
                                    Data do cadastro
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr data-href="<?= base_url() ?>agent/view/<?= $item['pk_agent'] ?>" style="cursor: pointer">
                                    <td><a href="<?= base_url() ?>customer/index/change_agent/<?= $item['pk_agent'] ?>"><?= $item['community_name'] ?></a></td>
                                    <td><a href="<?= base_url() ?>customer/index/change_agent/<?= $item['pk_agent'] ?>"><?= $item['type'] ?></a></td>
                                    <td><a href="<?= base_url() ?>customer/index/change_agent/<?= $item['pk_agent'] ?>"><?= $item['city'] ?>/<?= $item['state'] ?></a></td>
                                    <td><a href="<?= base_url() ?>customer/index/change_agent/<?= $item['pk_agent'] ?>"><?= date("d/m/Y", strtotime($item['created_at'])); ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>