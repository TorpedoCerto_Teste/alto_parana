<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Faturamentos de Energia
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Faturamentos de Energia</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Contrato</th>
                                <th>Fornecedor</th>
                                <th>Ano</th>
                                <th>Mês</th>
                                <th>Data de validade</th>
                                <th>Total</th>
                                <th>Status do pagamento</th>
                                <th>Data de pagamento</th>
                                <th></th>
                                <th>Tipo de fatura</th>
                                <th>Boleto</th>
                                <th>NFe</th>
                                <th>XML</th>
                            </tr>
                        </thead>
                        <?php foreach ($list as $ccvee) :?>
                            <?php if ($ccvee['billing']) : ?>
                                <tbody>
                                    <?php foreach ($ccvee['billing'] as $item) : ?>
                                        <tr>
                                            <td><?=$ccvee['document_number']?></td>
                                            <td><?=$ccvee['counterpart']?></td>
                                            <td><?= $item['year'] ?></td>
                                            <td><?= strtoupper(retorna_mes($item['month'])) ?></td>
                                            <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                            <td><?= number_format($item['total'], 2, ",", "") ?></td>
                                            <td><?= strtoupper($item['payment_status']) ?></td>
                                            <td><?= date_to_human_date($item['payment_date']) ?></td>
                                            <td>
                                                <?php if ($item['payment_status'] != "quitado" && preg_replace("/([^\d]*)/", "", $item['expiration_date']) < preg_replace("/([^\d]*)/", "", $item['payment_date'])) : ?>
                                                    <span class="label label-danger">ATRASADO</span>
                                                <?php else: ?>
                                                    <span class="label label-success">EM DIA</span>
                                                <?php endif; ?>
                                            </td>
                                            <td><?= strtoupper($item['invoice_type']) ?></td>
                                            <td>
                                                <?php if ($item['payment_slip'] !== "") : ?>
                                                    <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_billing/<?= $item['pk_ccvee_billing'] ?>/<?= $item['payment_slip'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if ($item['invoice'] !== "") : ?>
                                                    <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_billing/<?= $item['pk_ccvee_billing'] ?>/<?= $item['invoice'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if ($item['xml'] !== "") : ?>
                                                    <a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/ccvee_billing/<?= $item['pk_ccvee_billing'] ?>/<?= $item['xml'] ?>" target="_blank" class="btn btn-success btn-xs">Download</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
