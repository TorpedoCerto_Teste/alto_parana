<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Banco de Tarifas
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Banco de Tarifas</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Banco de Tarifas
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" >
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>customer/bank_charges/" disabled=""/>

                        <div class="form-group <?= form_error('fk_agent_power_distributor') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_agent_power_distributor">Distribuidora</label>
                            <div class="col-lg-4">
                                <select class="form-control select2-allow-clear" id="fk_agent_power_distributor" name="fk_agent_power_distributor">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $agent) : ?>
                                            <option value="<?= $agent['pk_agent'] ?>" <?= set_value('fk_agent_power_distributor') == $agent['pk_agent'] || $this->Bank_charges_model->_fk_agent_power_distributor == $agent['pk_agent'] ? "selected" : "" ?>><?= $agent['community_name'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('resolution') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="resolution">Resolução</label>
                            <div class="col-lg-2">
                                <div class="input-group m-b-10">
                                    <input type="text" placeholder="" id="resolution" name="resolution" class="form-control" value="<?= set_value('resolution') != "" ? set_value('resolution') : $this->Bank_charges_model->_resolution ?>" alt="fone">
                                    <span class="input-group-btn" id="btn_search" name="btn_search" style="display: <?= isset($resolutions) ? 'block' : 'none' ?>">
                                        <button type="button" class="btn btn-white"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('date_resolution') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="date_resolution">Data da Resolução</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="date_resolution" name="date_resolution" class="form-control default-date-picker" value="<?= set_value('date_resolution') != "" ? set_value('date_resolution') : date_to_human_date($this->Bank_charges_model->_date_resolution) ?>" alt="date" disabled>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('beginning_term') != "" || form_error('end_term') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="beginning_term" id="beginning_term" value="<?= set_value('beginning_term') != "" ? set_value('beginning_term') : date_to_human_date($this->Bank_charges_model->_beginning_term) ?>" alt="date" disabled>
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_term" id="end_term" value="<?= set_value('end_term') != "" ? set_value('end_term') : date_to_human_date($this->Bank_charges_model->_end_term) ?>" alt="date" disabled>
                                </div>
                            </div>
                        </div>
                        <?php if ($this->Bank_charges_model->_bank_charges_file != "") : ?>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="bank_charges_files">Arquivo</label>
                            <div class="col-lg-6">
                                <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/bank_charges/<?= $this->Bank_charges_model->_pk_bank_charges ?>/<?= $this->Bank_charges_model->_bank_charges_file ?>" target="_blank"><?= $this->Bank_charges_model->_bank_charges_file ?></a>
                            </div>
                        </div>
                        <?php endif; ?>


                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="3">Subgrupo</th>
                                    <th rowspan="3">Modalidade</th>
                                    <th rowspan="3">Posto</th>
                                    <th colspan="3">Tarifa de Aplicação</th>
                                </tr>
                                <tr>
                                    <th colspan="2">TUSD</th>
                                    <th>TE</th>
                                </tr>
                                <tr>
                                    <th>R$/kW</th>
                                    <th>R$/MWh</th>
                                    <th>Verde R$/MWh</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td rowspan="4">A1 (230kV ou mais)</td>
                                    <td rowspan="2">Azul</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_tusd_kw_p" name="a1_azul_tusd_kw_p" class="form-control" value="<?= set_value('a1_azul_tusd_kw_p') != "" ? set_value('a1_azul_tusd_kw_p') : @number_format($this->Bank_charges_model->_a1_azul_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_tusd_mwh_p" name="a1_azul_tusd_mwh_p" class="form-control" value="<?= set_value('a1_azul_tusd_mwh_p') != "" ? set_value('a1_azul_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a1_azul_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_te_mwh_p_verde" name="a1_azul_te_mwh_p_verde" class="form-control" value="<?= set_value('a1_azul_te_mwh_p_verde') != "" ? set_value('a1_azul_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a1_azul_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2"> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_tusd_kw_fp" name="a1_azul_tusd_kw_fp" class="form-control" value="<?= set_value('a1_azul_tusd_kw_fp') != "" ? set_value('a1_azul_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a1_azul_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_tusd_mwh_fp" name="a1_azul_tusd_mwh_fp" class="form-control" value="<?= set_value('a1_azul_tusd_mwh_fp') != "" ? set_value('a1_azul_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a1_azul_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_te_mwh_fp_verde" name="a1_azul_te_mwh_fp_verde" class="form-control" value="<?= set_value('a1_azul_te_mwh_fp_verde') != "" ? set_value('a1_azul_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a1_azul_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Azul APE</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_ape_tusd_kw_p" name="a1_azul_ape_tusd_kw_p" class="form-control" value="<?= set_value('a1_azul_ape_tusd_kw_p') != "" ? set_value('a1_azul_ape_tusd_kw_p') : @number_format($this->Bank_charges_model->_a1_azul_ape_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_ape_tusd_mwh_p" name="a1_azul_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a1_azul_ape_tusd_mwh_p') != "" ? set_value('a1_azul_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a1_azul_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_ape_te_mwh_p_verde" name="a1_azul_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a1_azul_ape_te_mwh_p_verde') != "" ? set_value('a1_azul_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a1_azul_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_ape_tusd_kw_fp" name="a1_azul_ape_tusd_kw_fp" class="form-control" value="<?= set_value('a1_azul_ape_tusd_kw_fp') != "" ? set_value('a1_azul_ape_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a1_azul_ape_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_ape_tusd_mwh_fp" name="a1_azul_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a1_azul_ape_tusd_mwh_fp') != "" ? set_value('a1_azul_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a1_azul_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a1_azul_ape_te_mwh_fp_verde" name="a1_azul_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a1_azul_ape_te_mwh_fp_verde') != "" ? set_value('a1_azul_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a1_azul_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <!--A2-->
                                <tr>
                                    <td rowspan="4">A2 (88 a 138kV)</td>
                                    <td rowspan="2">Azul</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_tusd_kw_p" name="a2_azul_tusd_kw_p" class="form-control" value="<?= set_value('a2_azul_tusd_kw_p') != "" ? set_value('a2_azul_tusd_kw_p') : @number_format($this->Bank_charges_model->_a2_azul_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_tusd_mwh_p" name="a2_azul_tusd_mwh_p" class="form-control" value="<?= set_value('a2_azul_tusd_mwh_p') != "" ? set_value('a2_azul_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a2_azul_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_te_mwh_p_verde" name="a2_azul_te_mwh_p_verde" class="form-control" value="<?= set_value('a2_azul_te_mwh_p_verde') != "" ? set_value('a2_azul_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a2_azul_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2"> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_tusd_kw_fp" name="a2_azul_tusd_kw_fp" class="form-control" value="<?= set_value('a2_azul_tusd_kw_fp') != "" ? set_value('a2_azul_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a2_azul_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_tusd_mwh_fp" name="a2_azul_tusd_mwh_fp" class="form-control" value="<?= set_value('a2_azul_tusd_mwh_fp') != "" ? set_value('a2_azul_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a2_azul_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_te_mwh_fp_verde" name="a2_azul_te_mwh_fp_verde" class="form-control" value="<?= set_value('a2_azul_te_mwh_fp_verde') != "" ? set_value('a2_azul_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a2_azul_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Azul APE</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_ape_tusd_kw_p" name="a2_azul_ape_tusd_kw_p" class="form-control" value="<?= set_value('a2_azul_ape_tusd_kw_p') != "" ? set_value('a2_azul_ape_tusd_kw_p') : @number_format($this->Bank_charges_model->_a2_azul_ape_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_ape_tusd_mwh_p" name="a2_azul_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a2_azul_ape_tusd_mwh_p') != "" ? set_value('a2_azul_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a2_azul_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_ape_te_mwh_p_verde" name="a2_azul_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a2_azul_ape_te_mwh_p_verde') != "" ? set_value('a2_azul_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a2_azul_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_ape_tusd_kw_fp" name="a2_azul_ape_tusd_kw_fp" class="form-control" value="<?= set_value('a2_azul_ape_tusd_kw_fp') != "" ? set_value('a2_azul_ape_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a2_azul_ape_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_ape_tusd_mwh_fp" name="a2_azul_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a2_azul_ape_tusd_mwh_fp') != "" ? set_value('a2_azul_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a2_azul_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a2_azul_ape_te_mwh_fp_verde" name="a2_azul_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a2_azul_ape_te_mwh_fp_verde') != "" ? set_value('a2_azul_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a2_azul_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <!--A3-->
                                <tr>
                                    <td rowspan="4">A3 (69kV)</td>
                                    <td rowspan="2">Azul</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_tusd_kw_p" name="a3_azul_tusd_kw_p" class="form-control" value="<?= set_value('a3_azul_tusd_kw_p') != "" ? set_value('a3_azul_tusd_kw_p') : @number_format($this->Bank_charges_model->_a3_azul_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_tusd_mwh_p" name="a3_azul_tusd_mwh_p" class="form-control" value="<?= set_value('a3_azul_tusd_mwh_p') != "" ? set_value('a3_azul_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a3_azul_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_te_mwh_p_verde" name="a3_azul_te_mwh_p_verde" class="form-control" value="<?= set_value('a3_azul_te_mwh_p_verde') != "" ? set_value('a3_azul_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a3_azul_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2"> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_tusd_kw_fp" name="a3_azul_tusd_kw_fp" class="form-control" value="<?= set_value('a3_azul_tusd_kw_fp') != "" ? set_value('a3_azul_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a3_azul_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_tusd_mwh_fp" name="a3_azul_tusd_mwh_fp" class="form-control" value="<?= set_value('a3_azul_tusd_mwh_fp') != "" ? set_value('a3_azul_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a3_azul_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_te_mwh_fp_verde" name="a3_azul_te_mwh_fp_verde" class="form-control" value="<?= set_value('a3_azul_te_mwh_fp_verde') != "" ? set_value('a3_azul_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a3_azul_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Azul APE</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_ape_tusd_kw_p" name="a3_azul_ape_tusd_kw_p" class="form-control" value="<?= set_value('a3_azul_ape_tusd_kw_p') != "" ? set_value('a3_azul_ape_tusd_kw_p') : @number_format($this->Bank_charges_model->_a3_azul_ape_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_ape_tusd_mwh_p" name="a3_azul_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a3_azul_ape_tusd_mwh_p') != "" ? set_value('a3_azul_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a3_azul_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_ape_te_mwh_p_verde" name="a3_azul_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a3_azul_ape_te_mwh_p_verde') != "" ? set_value('a3_azul_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a3_azul_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_ape_tusd_kw_fp" name="a3_azul_ape_tusd_kw_fp" class="form-control" value="<?= set_value('a3_azul_ape_tusd_kw_fp') != "" ? set_value('a3_azul_ape_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a3_azul_ape_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_ape_tusd_mwh_fp" name="a3_azul_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a3_azul_ape_tusd_mwh_fp') != "" ? set_value('a3_azul_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a3_azul_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3_azul_ape_te_mwh_fp_verde" name="a3_azul_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a3_azul_ape_te_mwh_fp_verde') != "" ? set_value('a3_azul_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a3_azul_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <!--A3a-->
                                <tr>
                                    <td rowspan="10">A3a (34,5kV)</td>
                                    <td rowspan="2">Azul</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_tusd_kw_p" name="a3a_azul_tusd_kw_p" class="form-control" value="<?= set_value('a3a_azul_tusd_kw_p') != "" ? set_value('a3a_azul_tusd_kw_p') : @number_format($this->Bank_charges_model->_a3a_azul_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_tusd_mwh_p" name="a3a_azul_tusd_mwh_p" class="form-control" value="<?= set_value('a3a_azul_tusd_mwh_p') != "" ? set_value('a3a_azul_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a3a_azul_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_te_mwh_p_verde" name="a3a_azul_te_mwh_p_verde" class="form-control" value="<?= set_value('a3a_azul_te_mwh_p_verde') != "" ? set_value('a3a_azul_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a3a_azul_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2"> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_tusd_kw_fp" name="a3a_azul_tusd_kw_fp" class="form-control" value="<?= set_value('a3a_azul_tusd_kw_fp') != "" ? set_value('a3a_azul_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a3a_azul_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_tusd_mwh_fp" name="a3a_azul_tusd_mwh_fp" class="form-control" value="<?= set_value('a3a_azul_tusd_mwh_fp') != "" ? set_value('a3a_azul_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a3a_azul_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_te_mwh_fp_verde" name="a3a_azul_te_mwh_fp_verde" class="form-control" value="<?= set_value('a3a_azul_te_mwh_fp_verde') != "" ? set_value('a3a_azul_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a3a_azul_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Azul APE</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_ape_tusd_kw_p" name="a3a_azul_ape_tusd_kw_p" class="form-control" value="<?= set_value('a3a_azul_ape_tusd_kw_p') != "" ? set_value('a3a_azul_ape_tusd_kw_p') : @number_format($this->Bank_charges_model->_a3a_azul_ape_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_ape_tusd_mwh_p" name="a3a_azul_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a3a_azul_ape_tusd_mwh_p') != "" ? set_value('a3a_azul_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a3a_azul_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_ape_te_mwh_p_verde" name="a3a_azul_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a3a_azul_ape_te_mwh_p_verde') != "" ? set_value('a3a_azul_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a3a_azul_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_ape_tusd_kw_fp" name="a3a_azul_ape_tusd_kw_fp" class="form-control" value="<?= set_value('a3a_azul_ape_tusd_kw_fp') != "" ? set_value('a3a_azul_ape_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a3a_azul_ape_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_ape_tusd_mwh_fp" name="a3a_azul_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a3a_azul_ape_tusd_mwh_fp') != "" ? set_value('a3a_azul_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a3a_azul_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_azul_ape_te_mwh_fp_verde" name="a3a_azul_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a3a_azul_ape_te_mwh_fp_verde') != "" ? set_value('a3a_azul_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a3a_azul_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td rowspan="3">Verde</td>
                                    <td>NA</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_tusd_kw_na" name="a3a_verde_tusd_kw_na" class="form-control" value="<?= set_value('a3a_verde_tusd_kw_na') != "" ? set_value('a3a_verde_tusd_kw_na') : @number_format($this->Bank_charges_model->_a3a_verde_tusd_kw_na, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>P</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_tusd_mwh_p" name="a3a_verde_tusd_mwh_p" class="form-control" value="<?= set_value('a3a_verde_tusd_mwh_p') != "" ? set_value('a3a_verde_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a3a_verde_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_te_mwh_p_verde" name="a3a_verde_te_mwh_p_verde" class="form-control" value="<?= set_value('a3a_verde_te_mwh_p_verde') != "" ? set_value('a3a_verde_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a3a_verde_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>FP</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_tusd_mwh_fp" name="a3a_verde_tusd_mwh_fp" class="form-control" value="<?= set_value('a3a_verde_tusd_mwh_fp') != "" ? set_value('a3a_verde_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a3a_verde_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_te_mwh_fp_verde" name="a3a_verde_te_mwh_fp_verde" class="form-control" value="<?= set_value('a3a_verde_te_mwh_fp_verde') != "" ? set_value('a3a_verde_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a3a_verde_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td rowspan="3">Verde APE</td>
                                    <td>NA</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_ape_tusd_kw_na" name="a3a_verde_ape_tusd_kw_na" class="form-control" value="<?= set_value('a3a_verde_ape_tusd_kw_na') != "" ? set_value('a3a_verde_ape_tusd_kw_na') : @number_format($this->Bank_charges_model->_a3a_verde_ape_tusd_kw_na, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>P</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_ape_tusd_mwh_p" name="a3a_verde_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a3a_verde_ape_tusd_mwh_p') != "" ? set_value('a3a_verde_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a3a_verde_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_ape_te_mwh_p_verde" name="a3a_verde_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a3a_verde_ape_te_mwh_p_verde') != "" ? set_value('a3a_verde_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a3a_verde_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>FP</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_ape_tusd_mwh_fp" name="a3a_verde_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a3a_verde_ape_tusd_mwh_fp') != "" ? set_value('a3a_verde_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a3a_verde_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a3a_verde_ape_te_mwh_fp_verde" name="a3a_verde_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a3a_verde_ape_te_mwh_fp_verde') != "" ? set_value('a3a_verde_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a3a_verde_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <!--A4-->
                                <tr>
                                    <td rowspan="10">A4 (2,3 a 25kV)</td>
                                    <td rowspan="2">Azul</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_tusd_kw_p" name="a4_azul_tusd_kw_p" class="form-control" value="<?= set_value('a4_azul_tusd_kw_p') != "" ? set_value('a4_azul_tusd_kw_p') : @number_format($this->Bank_charges_model->_a4_azul_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_tusd_mwh_p" name="a4_azul_tusd_mwh_p" class="form-control" value="<?= set_value('a4_azul_tusd_mwh_p') != "" ? set_value('a4_azul_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a4_azul_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_te_mwh_p_verde" name="a4_azul_te_mwh_p_verde" class="form-control" value="<?= set_value('a4_azul_te_mwh_p_verde') != "" ? set_value('a4_azul_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a4_azul_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2"> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_tusd_kw_fp" name="a4_azul_tusd_kw_fp" class="form-control" value="<?= set_value('a4_azul_tusd_kw_fp') != "" ? set_value('a4_azul_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a4_azul_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_tusd_mwh_fp" name="a4_azul_tusd_mwh_fp" class="form-control" value="<?= set_value('a4_azul_tusd_mwh_fp') != "" ? set_value('a4_azul_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a4_azul_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_te_mwh_fp_verde" name="a4_azul_te_mwh_fp_verde" class="form-control" value="<?= set_value('a4_azul_te_mwh_fp_verde') != "" ? set_value('a4_azul_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a4_azul_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Azul APE</td>
                                    <td>P</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_ape_tusd_kw_p" name="a4_azul_ape_tusd_kw_p" class="form-control" value="<?= set_value('a4_azul_ape_tusd_kw_p') != "" ? set_value('a4_azul_ape_tusd_kw_p') : @number_format($this->Bank_charges_model->_a4_azul_ape_tusd_kw_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_ape_tusd_mwh_p" name="a4_azul_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a4_azul_ape_tusd_mwh_p') != "" ? set_value('a4_azul_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a4_azul_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_ape_te_mwh_p_verde" name="a4_azul_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a4_azul_ape_te_mwh_p_verde') != "" ? set_value('a4_azul_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a4_azul_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>FP</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_ape_tusd_kw_fp" name="a4_azul_ape_tusd_kw_fp" class="form-control" value="<?= set_value('a4_azul_ape_tusd_kw_fp') != "" ? set_value('a4_azul_ape_tusd_kw_fp') : @number_format($this->Bank_charges_model->_a4_azul_ape_tusd_kw_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_ape_tusd_mwh_fp" name="a4_azul_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a4_azul_ape_tusd_mwh_fp') != "" ? set_value('a4_azul_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a4_azul_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_azul_ape_te_mwh_fp_verde" name="a4_azul_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a4_azul_ape_te_mwh_fp_verde') != "" ? set_value('a4_azul_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a4_azul_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td rowspan="3">Verde</td>
                                    <td>NA</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_tusd_kw_na" name="a4_verde_tusd_kw_na" class="form-control" value="<?= set_value('a4_verde_tusd_kw_na') != "" ? set_value('a4_verde_tusd_kw_na') : @number_format($this->Bank_charges_model->_a4_verde_tusd_kw_na, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>P</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_tusd_mwh_p" name="a4_verde_tusd_mwh_p" class="form-control" value="<?= set_value('a4_verde_tusd_mwh_p') != "" ? set_value('a4_verde_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a4_verde_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_te_mwh_p_verde" name="a4_verde_te_mwh_p_verde" class="form-control" value="<?= set_value('a4_verde_te_mwh_p_verde') != "" ? set_value('a4_verde_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a4_verde_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>FP</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_tusd_mwh_fp" name="a4_verde_tusd_mwh_fp" class="form-control" value="<?= set_value('a4_verde_tusd_mwh_fp') != "" ? set_value('a4_verde_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a4_verde_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_te_mwh_fp_verde" name="a4_verde_te_mwh_fp_verde" class="form-control" value="<?= set_value('a4_verde_te_mwh_fp_verde') != "" ? set_value('a4_verde_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a4_verde_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td rowspan="3">Verde APE</td>
                                    <td>NA</td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_ape_tusd_kw_na" name="a4_verde_ape_tusd_kw_na" class="form-control" value="<?= set_value('a4_verde_ape_tusd_kw_na') != "" ? set_value('a4_verde_ape_tusd_kw_na') : @number_format($this->Bank_charges_model->_a4_verde_ape_tusd_kw_na, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>P</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_ape_tusd_mwh_p" name="a4_verde_ape_tusd_mwh_p" class="form-control" value="<?= set_value('a4_verde_ape_tusd_mwh_p') != "" ? set_value('a4_verde_ape_tusd_mwh_p') : @number_format($this->Bank_charges_model->_a4_verde_ape_tusd_mwh_p, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_ape_te_mwh_p_verde" name="a4_verde_ape_te_mwh_p_verde" class="form-control" value="<?= set_value('a4_verde_ape_te_mwh_p_verde') != "" ? set_value('a4_verde_ape_te_mwh_p_verde') : @number_format($this->Bank_charges_model->_a4_verde_ape_te_mwh_p_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>FP</td>
                                    <td></td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_ape_tusd_mwh_fp" name="a4_verde_ape_tusd_mwh_fp" class="form-control" value="<?= set_value('a4_verde_ape_tusd_mwh_fp') != "" ? set_value('a4_verde_ape_tusd_mwh_fp') : @number_format($this->Bank_charges_model->_a4_verde_ape_tusd_mwh_fp, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                    <td>
                                        <input type="text" disabled  placeholder="" id="a4_verde_ape_te_mwh_fp_verde" name="a4_verde_ape_te_mwh_fp_verde" class="form-control" value="<?= set_value('a4_verde_ape_te_mwh_fp_verde') != "" ? set_value('a4_verde_ape_te_mwh_fp_verde') : @number_format($this->Bank_charges_model->_a4_verde_ape_te_mwh_fp_verde, 2, ',', '') ?>" alt="valor2">
                                    </td>
                                </tr>                                   
                            </tbody>

                        </table>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="select" class="modal fade">
    <form method="POST" action="javascript:void(0)">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Selecione uma resolução</h4>
                </div>
                <div class="modal-body">
                    <section class="panel">
                        <table class="table table-hover" id="tbl_select" name="tbl_select">
                            <thead>
                                <tr>
                                    <th>Resolução</th>
                                    <th>Data</th>
                                    <th>Início do Termo</th>
                                    <th>Final do Termo</th
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($resolutions)) : ?>
                                    <?php foreach ($resolutions as $resolution) : ?>
                                        <tr>
                                            <td><?= $resolution['resolution'] ?></td>
                                            <td><?= $resolution['date_resolution'] ?></td>
                                            <td><?= $resolution['beginning_term'] ?></td>
                                            <td><?= $resolution['end_term'] ?></td>
                                            <td>
                                                <button class="btn btn-success btn-xs btn_check" value="<?= $resolution['pk_bank_charges'] ?>" onclick="select_resolution(<?= $resolution['pk_bank_charges'] ?>)">
                                                    <i class="fa fa-check"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </section>

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->