<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Relatório Cativo x Livre
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>customer">Home</a></li>
            <li class="active">Relatório Cativo x Livre</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Selecione um filtro para o relatório.
                    </div>
                <?php else : ?>
                    <table class="table colvis-data-table data-table">
                        <thead>
                            <tr>
                                <th>Ano/Mês</th>
                                <th>Economia %</th>
                                <th>Economia (R$)</th>
                                <th>Total Cativo (R$)</th>
                                <th>Total Livre (R$)</th>
                                <th>Distribuidora</th>
                                <th>Relatório</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['year'] ?>/<?= $item['month'] ?></td>
                                    <td><?= number_format($item['economy'], 2, ",", "") ?></td>
                                    <td><?= number_format(($item['captive_total'] - $item['free_total']), 2, ",", "") ?></td>
                                    <td><?= number_format($item['captive_total'], 2, ",", "") ?></td>
                                    <td><?= number_format($item['free_total'], 2, ",", "") ?></td>
                                    <td><?= $item['agent_power_distributor'] ?></td>
                                    <td>
                                        <?php if ($item['report'] != "") : ?>
                                            <a class="btn btn-info btn-xs" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/free_captive/<?= $item['pk_free_captive'] ?>/<?= $item['report'] ?>" target="_blank"><?= $item['report'] ?></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
