<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Cativo Livre
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Cativo Livre</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Cativo Livre
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <?php if ($success) : ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Dados gravados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form id='frm_free_captive' role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>free_captive/upsert" enctype="multipart/form-data">
                        <input type="hidden" name="upsert" id="upsert" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group <?= form_error('fk_agent') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Agente</label>
                            <div class="col-lg-6">
                                <select id="fk_agent" name="fk_agent" class="form-control select2">
                                    <option></option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $type => $agent) : ?>
                                            <optgroup label="<?= $type ?>">
                                                <?php foreach ($agent as $agt) : ?>
                                                    <option value="<?= $agt['pk_agent'] ?>" <?= $this->Free_captive_model->_fk_agent == $agt['pk_agent'] ? "selected" : "" ?>><?= $agt['community_name'] ?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Período</label>
                            <div class="col-lg-4 <?= form_error('month') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                                        <option value="<?= $i ?>" <?= $this->Free_captive_model->_month == $i ? "selected" : "" ?>><?= retorna_mes($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 <?= form_error('year') != "" ? "has-error" : ""; ?>">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= $this->Free_captive_model->_year != "" ? $this->Free_captive_model->_year : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div>

                        <?php if ($unities) : ?>
                            <table class="table table-responsive table-striped">
                                <thead>
                                    <tr>
                                        <th>Unidade</th>
                                        <th>Economia %</th>
                                        <th>Total Cativo (R$)</th>
                                        <th>Total Livre (R$)</th>
                                        <th>Distribuidora</th>
                                        <th>Relatório</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($unities as $pk_unity => $unity) : ?>
                                <input type="hidden" id="pk_free_captive_<?= $pk_unity ?>" name="unity[<?= $pk_unity ?>][pk_free_captive]" value='<?= isset($unity['free_captive']['pk_free_captive']) ? $unity['free_captive']['pk_free_captive'] : 0 ?>'>
                                        <tr>
                                            <td><?= $unity['unity']['community_name'] ?></td>
                                            <td><input type="text" class="form-control economy" pk_unity="<?= $pk_unity ?>" id="economy_<?= $pk_unity ?>" name="unity[<?= $pk_unity ?>][economy]" value="<?= isset($unity['free_captive']['economy']) ? number_format($unity['free_captive']['economy'], 2, ",", "") : 0 ?>" alt="valor2"></td>
                                            <td><input type="text" class="form-control captive_total" pk_unity="<?= $pk_unity ?>" id="captive_total_<?= $pk_unity ?>" name="unity[<?= $pk_unity ?>][captive_total]" value="<?= isset($unity['free_captive']['captive_total']) ? number_format($unity['free_captive']['captive_total'], 2, ",", "") : 0 ?>" alt="valor2"></td>
                                            <td><input type="text" class="form-control free_total" pk_unity="<?= $pk_unity ?>" id="free_total_<?= $pk_unity ?>" name="unity[<?= $pk_unity ?>][free_total]" value="<?= isset($unity['free_captive']['free_total']) ? number_format($unity['free_captive']['free_total'], 2, ",", "") : 0 ?>" alt="valor2"></td>
                                            <td>
                                                <input type="hidden" id="fk_agent_power_distributor_<?= $pk_unity ?>" name="unity[<?= $pk_unity ?>][fk_agent_power_distributor]" value="<?= $unity['unity']['fk_agent_power_distributor'] ?>">
                                                <?= $unity['unity']['power_distributor_name'] ?>
                                            </td>
                                            <td>
                                                <?php if (isset($unity['free_captive']['report'])) : ?>
                                                    <?php if ($unity['free_captive']['report'] != "") : ?>
                                                        <a class="btn btn-info" href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/free_captive/<?= $unity['free_captive']['pk_free_captive']?>/<?= $unity['free_captive']['report'] ?>" target="_blank">Visualizar Arquivo</a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <input type="file" class="form-control" id="report_<?= $pk_unity ?>" name="report[<?= $pk_unity ?>]" >
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">Salvar</button>
                                </div>
                            </div>
                        <?php endif; ?>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>

