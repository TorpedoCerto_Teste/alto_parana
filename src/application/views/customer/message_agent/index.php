<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Mensagens
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Mensagens</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Mensagens
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Mensagem</th>
                                <th>Data</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                            <tr <?=$item['status'] == $this->Message_agent_model->_status_active ? 'style="font-weight: bold"' : ""?>>
                                    <td><a href="<?=base_url() ?>customer/message_agent/view/<?=$item['pk_message_agent']?>"><?= $item['title'] ?></td>
                                    <td><?= substr(strip_tags($item['message']), 0, 100)."..." ?></td>
                                    <td><?= date("d/m/Y H:i", strtotime($item['created_at']))?></td>
                                    <td>
                                        <?php if ($item['priority']) : ?>
                                            <span class="label label-warning">Prioridade</span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>

