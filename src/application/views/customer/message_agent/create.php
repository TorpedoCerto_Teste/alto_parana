<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Nova Mensagem
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>message/">Mensagens</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Nova Mensagem
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>message/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        <div class="form-group <?= form_error('fk_user') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Agentes</label>
                            <div class="col-lg-10">
                                <select id="multiple" class="form-control  select2" name="fk_agent" id="fk_agent">
                                    <option value="0">TODOS</option>
                                    <?php if ($agents) : ?>
                                        <?php foreach ($agents as $agent) : ?>
                                            <option value="<?=$agent['pk_agent']?>"><?=$agent['community_name']?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('title') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="title">Título</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="" id="title" name="title" class="form-control" value="<?= set_value('title') ?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('message') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="message">Mensagem</label>
                            <div class="col-lg-10">
                                <textarea class="input-block-level" id="message" name="message" rows="18"><?= set_value('message') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('priority') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="priority">Prioridade</label>
                            <div class="col-lg-10">
                                <input type="checkbox" id="priority" name="priority" value="1" <?= set_value('priority') == 1 ? "checked" : "" ?> />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>message'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>



