<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        <?= $this->Message_model->_title ?>
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>customer/message_agent">Mensagens</a></li>
            <li class="active">Visualizar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    <?= $this->Message_model->_title ?>
                    <span class="tools pull-right">
                        <button class="btn btn-default addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>customer/message_agent'">
                            <i class="fa fa-backward pull-right"></i>
                            Voltar
                        </button>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <?= $this->Message_model->_message ?>
                    </div>
                </div>
            </section>
        </div>

    </div>
</div>



