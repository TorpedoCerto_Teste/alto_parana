<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Agentes e Contatos
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Agentes e Contatos</li>
        </ol>
    </div>
</div>
<!-- page head end-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">

            <section class="panel">
                <?php if (empty($contacts)) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada.
                    </div>
                <?php else : ?>

                    <header class="panel-heading tab-dark tab-right ">
                        <ul class="nav nav-tabs pull-right">
                            <li class="active">
                                <a data-toggle="tab" href="#agents">
                                    <i class="fa fa-building"></i>
                                    Agentes
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#contacts">
                                    <i class="fa fa-user"></i>
                                    Contatos
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#export">
                                    <i class="fa fa-download"></i>
                                    Exportar
                                </a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <!--AGENTES-->
                            <div id="agents" class="tab-pane active">
                                <table class="table responsive-data-table data-table">
                                    <thead>
                                        <tr>
                                            <th>Agente</th>
                                            <th>Contatos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($agents as $pk_agent => $agent) : ?>
                                            <tr>
                                                <td><?= $agent['agent']['community_name'] ?></td>
                                                <td class="hidden-xs">
                                                    <table>
                                                        <?php foreach ($agent['contacts'] as $pk_contact => $contact) : ?>
                                                            <tr>
                                                                <td><?=$contact['name']. " ".$contact['surname']?></td>
                                                                <td><?=$contact['email']?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                            <!--CONTATOS-->
                            <div id="contacts" class="tab-pane">
                                <table class="table responsive-data-table data-table">
                                    <thead>
                                        <tr>
                                            <th>Contato</th>
                                            <th>Email</th>
                                            <th>Agentes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($contacts as $pk_contact => $contact) : ?>
                                        <tr>
                                            <td><?=$contact['contact']['name']. " ".$contact['contact']['surname']?></td>
                                            <td><?=$contact['contact']['email']?></td>
                                            <td class="hidden-xs">
                                                <table>
                                                    <?php foreach ($contact['agents'] as $pk_agent => $agent) : ?>
                                                        <tr>
                                                            <td><?=$agent['community_name']?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            </td>
                                        </tr>
                                        
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                            <!-- EXPORTAR -->
                            <div id="export" class="tab-pane">

                                <form id='frm_free_captive' role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>agent_contact/export" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label" for="fk_agent">Agente</label>
                                        <div class="col-lg-6">
                                            <select id="fk_agent" name="fk_agent" class="form-control select2">
                                                <option value="0">EXPORTAR TUDO</option>
                                                <?php if ($agents_list) : ?>
                                                    <?php foreach ($agents_list as $pk_agent => $agent) : ?>
                                                        <option value="<?= $pk_agent ?>"><?= $agent ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-success" type="submit">Exportar Agentes e Contatos</button>
                                        </div>
                                    </div>                                    
                                </form>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>
