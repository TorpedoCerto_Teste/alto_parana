<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo ICMS
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo ICMS
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>icms/create">
                        <input type="hidden" name="create" id="create" value="true" />
                        
                        <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                            <div class="col-lg-4">
                                <select id="state" name="state" class="form-control">
                                    <option
                                        value="AC" <?= set_value('state') == 'AC' ? 'selected' : '' ?>>
                                        Acre
                                    </option>
                                    <option
                                        value="AL" <?= set_value('state') == 'AL' ? 'selected' : '' ?>>
                                        Alagoas
                                    </option>
                                    <option
                                        value="AP" <?= set_value('state') == 'AP' ? 'selected' : '' ?>>
                                        Amapá
                                    </option>
                                    <option
                                        value="AM" <?= set_value('state') == 'AM' ? 'selected' : '' ?>>
                                        Amazonas
                                    </option>
                                    <option
                                        value="BA" <?= set_value('state') == 'BA' ? 'selected' : '' ?>>
                                        Bahia
                                    </option>
                                    <option
                                        value="CE" <?= set_value('state') == 'CE' ? 'selected' : '' ?>>
                                        Ceará
                                    </option>
                                    <option
                                        value="DF" <?= set_value('state') == 'DF' ? 'selected' : '' ?>>
                                        Distrito Federal
                                    </option>
                                    <option
                                        value="ES" <?= set_value('state') == 'ES' ? 'selected' : '' ?>>
                                        Espirito Santo
                                    </option>
                                    <option
                                        value="GO" <?= set_value('state') == 'GO' ? 'selected' : '' ?>>
                                        Goiás
                                    </option>
                                    <option
                                        value="MA" <?= set_value('state') == 'MA' ? 'selected' : '' ?>>
                                        Maranhão
                                    </option>
                                    <option
                                        value="MS" <?= set_value('state') == 'MS' ? 'selected' : '' ?>>
                                        Mato Grosso do Sul
                                    </option>
                                    <option
                                        value="MT" <?= set_value('state') == 'MT' ? 'selected' : '' ?>>
                                        Mato Grosso
                                    </option>
                                    <option
                                        value="MG" <?= set_value('state') == 'MG' ? 'selected' : '' ?>>
                                        Minas Gerais
                                    </option>
                                    <option
                                        value="PA" <?= set_value('state') == 'PA' ? 'selected' : '' ?>>
                                        Pará
                                    </option>
                                    <option
                                        value="PB" <?= set_value('state') == 'PB' ? 'selected' : '' ?>>
                                        Paraíba
                                    </option>
                                    <option
                                        value="PR" <?= set_value('state') == 'PR' ? 'selected' : '' ?>>
                                        Paraná
                                    </option>
                                    <option
                                        value="PE" <?= set_value('state') == 'PE' ? 'selected' : '' ?>>
                                        Pernambuco
                                    </option>
                                    <option
                                        value="PI" <?= set_value('state') == 'PI' ? 'selected' : '' ?>>
                                        Piauí
                                    </option>
                                    <option
                                        value="RJ" <?= set_value('state') == 'RJ' ? 'selected' : '' ?>>
                                        Rio de Janeiro
                                    </option>
                                    <option
                                        value="RN" <?= set_value('state') == 'RN' ? 'selected' : '' ?>>
                                        Rio Grande do Norte
                                    </option>
                                    <option
                                        value="RS" <?= set_value('state') == 'RS' ? 'selected' : '' ?>>
                                        Rio Grande do Sul
                                    </option>
                                    <option
                                        value="RO" <?= set_value('state') == 'RO' ? 'selected' : '' ?>>
                                        Rondônia
                                    </option>
                                    <option
                                        value="RR" <?= set_value('state') == 'RR' ? 'selected' : '' ?>>
                                        Roraima
                                    </option>
                                    <option
                                        value="SC" <?= set_value('state') == 'SC' ? 'selected' : '' ?>>
                                        Santa Catarina
                                    </option>
                                    <option
                                        value="SP" <?= set_value('state') == 'SP' ? 'selected' : '' ?>>
                                        São Paulo
                                    </option>
                                    <option
                                        value="SE" <?= set_value('state') == 'SE' ? 'selected' : '' ?>>
                                        Sergipe
                                    </option>
                                    <option
                                        value="TO" <?= set_value('state') == 'TO' ? 'selected' : '' ?>>
                                        Tocantins
                                    </option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('start_date') != "" || form_error('end_term') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="<?= set_value('start_date') ?>" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="<?= set_value('end_date') ?>" alt="date" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('percent_industry') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="percent_industry">% Indústria</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="percent_industry" name="percent_industry" class="form-control" value="<?= set_value('percent_industry') ?>" alt="valor2">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('percent_commercial') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="percent_commercial">% Comércio</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="percent_commercial" name="percent_commercial" class="form-control" value="<?= set_value('percent_commercial') ?>" alt="valor2">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>icms'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
