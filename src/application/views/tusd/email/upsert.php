<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Fatura da Distribuidora</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
			<img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
            <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> Fatura da Distribuidora </p>
            <p>A equipe da Alto Paraná Energia concluiu a conferência de sua Fatura de Distribuição! Ela está em anexo e também disponível na <a href="<?=base_url()?>customer/tusd">área do cliente</a>.</p>
            <table style="border:0px; width: 100%">
                <tr>
                    <td width="300px">Agente:</td>
                    <td><?= $tusd["agente"]["_community_name"] ?></td>
                </tr>
                <tr>
                    <td width="300px">Referência:</td>
                    <td><?= $tusd["_month"] ?>/<?= $tusd["_year"] ?></td>
                </tr>
                <tr>
                    <td width="300px">Distribuidora de Energia da Unidade:</td>
                    <td><?= isset($tusd["agente_distribuidor"]["_community_name"]) ? $tusd["agente_distribuidor"]["_community_name"] : "" ?></td>
                </tr>
                <tr>
                    <td width="300px">Nome da Unidade:</td>
                    <td><?= isset($tusd["unidade"]["_community_name"]) ? $tusd["unidade"]["_community_name"] : "" ?></td>
                </tr>
                <tr>
                    <td width="300px">Número UC:</td>
                    <td><?= isset($tusd["unidade"]["_uc_number"]) ? $tusd["unidade"]["_uc_number"] : "" ?></td>
                </tr>
                <tr>
                    <td>Vencimento:</td>
                    <td><?= date_to_human_date($tusd["_expiration_date"]) ?></td>
                </tr>
                <tr>
                    <td>Valor:</td>
                    <td>R$ <?= number_format($tusd["_value"],2,",","") ?></td>
                </tr>
            </table>
        <br/>
        <br/>
              <div style="text-align: justify"> Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?=$this->config->item("config_phone")?> e em nosso atendimento on-line, via chat da área do cliente.</div>
		<br/>
        <br/>
      <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
      <div>
        <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a>
        <br/>
      	<a href="mailto:comercial@altoparanaenergia.com" style="text-decoration: none; color: #999">comercial@altoparanaenergia.com</a>
      </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>

    </body>
</html>