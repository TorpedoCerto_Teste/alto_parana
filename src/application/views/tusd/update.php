<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        TUSD 
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>tusd/index">TUSD</a></li>
            <li class="active">Editar TUSD</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar TUSD
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>tusd/update/<?=$this->Tusd_model->_pk_tusd?>" enctype="multipart/form-data">
                        <input type="hidden" name="update" id="update" value="true" />

                       <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity" >
                                    <option></option>
                                    <?php foreach ($units2 as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= $this->Tusd_model->_fk_unity == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                                           
                         <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-2">
                                <select id="month" name="month" class="form-control">
                                    <option value="01" <?= $this->Tusd_model->_month == "01" ? "selected" : "" ?>>JANEIRO</option>
                                    <option value="02" <?= $this->Tusd_model->_month == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                    <option value="03" <?= $this->Tusd_model->_month == "03" ? "selected" : "" ?>>MARÇO</option>
                                    <option value="04" <?= $this->Tusd_model->_month == "04" ? "selected" : "" ?>>ABRIL</option>
                                    <option value="05" <?= $this->Tusd_model->_month == "05" ? "selected" : "" ?>>MAIO</option>
                                    <option value="06" <?= $this->Tusd_model->_month == "06" ? "selected" : "" ?>>JUNHO</option>
                                    <option value="07" <?= $this->Tusd_model->_month == "07" ? "selected" : "" ?>>JULHO</option>
                                    <option value="08" <?= $this->Tusd_model->_month == "08" ? "selected" : "" ?>>AGOSTO</option>
                                    <option value="09" <?= $this->Tusd_model->_month == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                    <option value="10" <?= $this->Tusd_model->_month == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                    <option value="11" <?= $this->Tusd_model->_month == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                    <option value="12" <?= $this->Tusd_model->_month == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                </select>
                            </div>
                        </div>   
                        
                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : $this->Tusd_model->_year ?>" alt="year">
                            </div>
                        </div>
                         
                          <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Vencimento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control default-date-picker" value="<?= set_value('expiration_date') != "" ? set_value('expiration_date') : date_to_human_date($this->Tusd_model->_expiration_date) ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('demand_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="demand_end">Demanda Ponta</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="demand_end" name="demand_end" class="form-control" value="<?= set_value('demand_end') != "" ? set_value('demand_end') : $this->Tusd_model->_demand_end ?>">
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('demand_tip_out') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="demand_tip_out">Demanda Fora de ponta</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="demand_tip_out" name="demand_tip_out" class="form-control default-date-picker" value="<?= set_value('demand_tip_out') != "" ? set_value('demand_tip_out') : $this->Tusd_model->_demand_tip_out ?>">
                            </div>
                        </div>

                       <div class="form-group <?= form_error('consumption_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="consumption_end">Consumo Ponta</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="consumption_end" name="consumption_end" class="form-control" value="<?= set_value('consumption_end') != "" ? set_value('consumption_end') : $this->Tusd_model->_consumption_end?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('consumption_tip_out') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="consumption_tip_out">Consumo Fora de ponta</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="consumption_tip_out" name="consumption_tip_out" class="form-control" value="<?= set_value('consumption_tip_out') != "" ? set_value('consumption_tip_out') : $this->Tusd_model->_consumption_tip_out?>">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('reactive_energy_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="reactive_energy_end">Energia Reativa Ponta</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="reactive_energy_end" name="reactive_energy_end" class="form-control" value="<?= set_value('reactive_energy_end') != "" ? set_value('reactive_energy_end') : $this->Tusd_model->_reactive_energy_end?>">
                            </div>
                        </div> 
                        <div class="form-group <?= form_error('reactive_energi_tip_out') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="reactive_energi_tip_out">Energia Reativa Fora de ponta</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="reactive_energi_tip_out" name="reactive_energi_tip_out" class="form-control" value="<?= set_value('reactive_energi_tip_out') != "" ? set_value('reactive_energi_tip_out') : $this->Tusd_model->_reactive_energi_tip_out?>">
                            </div>
                        </div> 
                        <div class="form-group <?= form_error('connection_charge') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="connection_charge">Encargo de Conexão</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="connection_charge" name="connection_charge" class="form-control" value="<?= set_value('connection_charge') != "" ? set_value('connection_charge') : $this->Tusd_model->_connection_charge?>">
                            </div>
                        </div> 
                        <div class="form-group <?= form_error('adjustment') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="adjustment">Devolução/Ajustes</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="adjustment" name="adjustment" class="form-control" value="<?= set_value('adjustment') != "" ? set_value('adjustment') : $this->Tusd_model->_adjustment?>">
                            </div>
                        </div>                         
                        <div class="form-group <?= form_error('discount') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="discount">Desconto TUSD</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="discount" name="discount" class="form-control" value="<?= set_value('discount') != "" ? set_value('discount') : $this->Tusd_model->_discount?>">
                            </div>
                        </div> 
                         <div class="form-group <?= form_error('street_lighting') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="street_lighting">Iluminação Pública</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="street_lighting" name="street_lighting" class="form-control" value="<?= set_value('street_lighting') != "" ? set_value('street_lighting') : $this->Tusd_model->_street_lighting?>">
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
