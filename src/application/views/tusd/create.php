<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo TUSD
    </h3>
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo TUSD
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" id="show_hide_uploader" name="show_hide_uploader">
                            <i class="fa fa-file pull-right"></i>
                            Inserir Arquivos
                        </button>
                    </span>
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">

                    <div class="alert alert-warning fade in" name="alert_temp_files" id="alert_temp_files" style="display: <?=!empty($temp_files) ? "block" : "none"?>">
                        <strong>Atenção!</strong> Existem arquivos que foram inseridos, porém não puderam ser atribuídos a qualquer TUSD.<br/>
                        Você pode mantê-los e eles serão adicionados à próxima TUSD salva, ou, poderá excluí-los.
                        <hr>
                        <table class="table-hover table-responsive table-inbox">
                            <?php if (!empty($temp_files)) : ?>
                                <?php foreach ($temp_files as $key => $file) : ?>
                                    <tr id="tr_<?= $key ?>" name="tr_<?= $key ?>">
                                        <td><?= $file ?></td>
                                        <td><a class="btn btn-danger btn-xs delete_file" href="javascript:void(0)" value="<?= $file ?>" key="<?= $key ?>"><i class="fa fa-close"></i></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </table>
                    </div>
                    

                    <div id="div_upload" name="div_upload" style="display: none">
                        <form id="my-awesome-dropzone" action="<?= base_url() ?>tusd/dropzone" class="dropzone" ></form>
                        <hr>
                    </div>

                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>tusd/create/">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled" />

                        <div class="form-group <?= form_error('fk_unity') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_unity">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($units2 as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" >Distribuidora</label>
                            <div class="col-lg-2">
                                <span id="power_distributor" name="power_distributor"></span>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="form_penalty_interest">Dados da conta</label>
                            <div class="col-lg-2">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control ">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>JANEIRO</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>MARÇO</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>ABRIL</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>MAIO</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>JUNHO</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>JULHO</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>AGOSTO</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Ano</span>
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= date("Y") ?>" alt="year">
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Tensão</span>
                                <input type="text" id="tension" name="tension" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Modalidade Tarifária</span>
                                <input type="text" id="modality" name="modality" class="form-control" disabled="disabled" />
                            </div>
                        </div>    

                        <div class="alert alert-danger fade in" name="error_demand" id="error_demand" style="display: none">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Atenção!</strong> Não existem registros de demanda para o período selecionado. 
                        </div>


                        <div class="form-group <?= form_error('expiration_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="expiration_date">Vencimento</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="expiration_date" name="expiration_date" class="form-control default-date-picker" value="<?= set_value('expiration_date') ?>" alt="date">
                            </div>
                            <div class="col-lg-2">
                                <a class="btn btn-danger">Avisar Ausência de Fatura</a>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Dados da Demanda Contratada</label>
                            <div class="col-lg-2">
                                <span class="help-block">Ponta (kW)</span>
                                <input type="text" placeholder="" id="contracted_demand_end" name="contracted_demand_end" class="form-control" disabled="disabled">
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Fora de Ponta (kW)</span>
                                <input type="text" id="contracted_demand_tip_out" name="contracted_demand_tip_out" class="form-control" disabled="disabled" />
                            </div>
                        </div>     
                        
                            
                        <div class="form-group <?= form_error('value') != "" || form_error('free_captive') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="">Valor / Cativo Livre</label>
                            <div class="col-lg-2">
                                <span class="help-block">Valor</span>
                                <input type="text" placeholder="" id="value" name="value" class="form-control <?= form_error('value') != "" ? "has-error" : ""; ?>" value="<?= set_value('value') ?>" alt="valor2">
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Cativo/Livre</span>
                                <select id="free_captive" name="free_captive" class="form-control <?= form_error('free_captive') != "" ? "has-error" : ""; ?>">
                                    <option value="CATIVO" <?=set_value("free_captive") == "CATIVO" ? "selected" : ""?>>CATIVO</option>
                                    <option value="LIVRE" <?=set_value("free_captive") == "LIVRE" ? "selected" : ""?>>LIVRE</option>
                                </select>
                            </div>
                        </div>  
                        
                        <div style="display: none">
                            <div class="form-group <?= form_error('demand_end') != "" || form_error('demand_tip_out') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Demanda Medida</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Ponta (kW)</span>
                                    <input type="text" placeholder="" id="demand_end" name="demand_end" class="form-control <?= form_error('demand_end')  ? "has-error" : ""; ?>" value="<?= set_value('demand_end') ?>">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Fora de Ponta (kW)</span>
                                    <input type="text" placeholder="" id="demand_tip_out" name="demand_tip_out" class="form-control <?= form_error('demand_tip_out') != "" ? "has-error" : ""; ?>" value="<?= set_value('demand_tip_out') ?>">
                                </div>
                            </div>                        

                            <div class="form-group <?= form_error('consumption_end') != "" || form_error('consumption_tip_out') != "" ? "has-error" : ""; ?> ">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Consumo</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Ponta (kW)</span>
                                    <input type="text" placeholder="" id="consumption_end" name="consumption_end" class="form-control <?= form_error('consumption_end') != "" ? "has-error" : ""; ?>" value="<?= set_value('consumption_end') ?>">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Fora de Ponta (kW)</span>
                                    <input type="text" placeholder="" id="consumption_tip_out" name="consumption_tip_out" class="form-control <?= form_error('consumption_tip_out') != "" ? "has-error" : ""; ?>" value="<?= set_value('consumption_tip_out') ?>">
                                </div>
                            </div>                        

                            <div class="form-group <?= form_error('reactive_energy_end') != "" || form_error('reactive_energi_tip_out') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Energia Reativa</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Ponta (kW)</span>
                                    <input type="text" placeholder="" id="reactive_energy_end" name="reactive_energy_end" class="form-control <?= form_error('reactive_energy_end') != "" ? "has-error" : ""; ?>" value="<?= set_value('reactive_energy_end') ?>">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Fora de Ponta (kW)</span>
                                    <input type="text" placeholder="" id="reactive_energi_tip_out" name="reactive_energi_tip_out" class="form-control <?= form_error('reactive_energi_tip_out') != "" ? "has-error" : ""; ?>" value="<?= set_value('reactive_energi_tip_out') ?>">
                                </div>
                            </div>  
                            
                            <div class="form-group <?= form_error('connection_charge') != "" || form_error('adjustment') != "" || form_error('discount') != "" || form_error('street_lighting') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Outros Dados</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Encargo de Conexão</span>
                                    <input type="text" placeholder="" id="connection_charge" name="connection_charge" class="form-control <?= form_error('connection_charge') != "" ? "has-error" : ""; ?>" value="<?= set_value('connection_charge') ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Devolução/Ajustes</span>
                                    <input type="text" placeholder="" id="adjustment" name="adjustment" class="form-control <?= form_error('adjustment') != "" ? "has-error" : ""; ?>" value="<?= set_value('adjustment') ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Desconto TUSD</span>
                                    <input type="text" placeholder="" id="discount" name="discount" class="form-control <?= form_error('discount') != "" ? "has-error" : ""; ?>" value="<?= set_value('discount') ?>" alt="valor6">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Iluminação Pública</span>
                                    <input type="text" placeholder="" id="street_lighting" name="street_lighting" class="form-control <?= form_error('street_lighting') != "" ? "has-error" : ""; ?>" value="<?= set_value('street_lighting') ?>" alt="valor2">
                                </div>
                            </div>                        

                            <div class="form-group  <?= form_error('icms') != "" || form_error('icms_reduction') != "" || form_error('piscofins') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="">Impostos</label>
                                <div class="col-lg-2">
                                    <span class="help-block">ICMS</span>
                                    <input type="text" placeholder="" id="icms" name="icms" class="form-control <?= form_error('icms') != "" ? "has-error" : ""; ?>" value="<?= set_value('icms') ?>" alt="valor6">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Redução BC ICMS</span>
                                    <input type="text" placeholder="" id="icms_reduction" name="icms_reduction" class="form-control <?= form_error('icms_reduction') != "" ? "has-error" : ""; ?>" value="<?= set_value('icms_reduction') ?>" alt="valor6">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Diferença PIS/COFINS</span>
                                    <input type="text" placeholder="" id="piscofins" name="piscofins" class="form-control <?= form_error('piscofins') != "" ? "has-error" : ""; ?>" value="<?= set_value('piscofins') ?>" alt="valor6">
                                </div>
                            </div>                                  
                        </div>
                  
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 

                </div>
            </section>
        </div>
    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_billing" class="modal fade">
    <form method="POST" action="<?= base_url() ?>tusd/delete/<?= $this->Tusd_model->_pk_tusd ?>">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_tusd" id="pk_tusd" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
