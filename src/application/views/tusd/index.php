<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        TUSD
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">TUSD</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Filtros
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>tusd/create/<?= $this->Tusd_model->_pk_tusd ?>'">
                            <i class="fa fa-plus pull-right"></i>
                            Lançar TUSD
                        </button>
                    </span>
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>">
                        <input type="hidden" name="filter" id="filter" value="true" disabled="disabled" />
                        <div class="form-group ">
                            <div class="col-lg-2">
                                <span class="help-block">Mês</span>
                                <select id="month" name="month" class="form-control ">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>JANEIRO</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>FEVEREIRO</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>MARÇO</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>ABRIL</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>MAIO</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>JUNHO</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>JULHO</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>AGOSTO</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>SETEMBRO</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>OUTUBRO</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>NOVEMBRO</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>DEZEMBRO</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Ano</span>
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= date("Y") ?>" alt="year">
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Status</span>
                                <select class="form-control" id="status" name="status">
                                    <option value="todos">TODOS</option>
                                    <option value="lancados">LANÇADOS</option>
                                    <option value="nao_lancados">NÃO LANÇADOS</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Estado</span>
                                <select id="state" name="state" class="form-control">
                                    <option value=""  <?= set_value('state') == '' ? 'selected' : '' ?>>TODOS</option>
                                    <?php foreach (retorna_estados() as $sigla => $estado) : ?>
                                        <option value="<?= $sigla ?>" <?= set_value('state') == $sigla ? 'selected' : '' ?>><?= $estado ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-lg-4">
                                <span class="help-block">Agente</span>
                                <select class="form-control select2" name="fk_agent" id="fk_agent">
                                    <option></option>
                                    <?php foreach ($agents as $agent) : ?>
                                        <option value="<?= $agent['pk_agent'] ?>" <?= set_value('fk_agent') == $agent['pk_agent'] ? 'selected' : "" ?>><?= $agent['community_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <span class="help-block">Distribuidora</span>
                                <select class="form-control select2" name="fk_unity" id="fk_unity">
                                    <option></option>
                                    <?php foreach ($units2 as $type => $unity) : ?>
                                        <optgroup label="<?= $type ?>">
                                            <?php foreach ($unity as $unit) : ?>
                                                <option value="<?= $unit['pk_unity'] ?>" <?= set_value('pk_unity') == $unit['pk_unity'] ? 'selected' : "" ?>><?= $unit['community_name'] ?></option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <span class="tools pull-right">
                            <button class="btn btn-info addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>tusd/create/<?= $this->Tusd_model->_pk_tusd ?>'">
                                <i class="fa fa-filter pull-right"></i>
                                Filtrar
                            </button>
                        </span>
                        <div style="clear: both"></div>
                    </form>
                </header>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <table class="table colvis-data-table data-table">
                        <thead>
                            <tr>
                                <th>Unidade</th>
                                <th>Agente</th>
                                <th>Distribuidora</th>
                                <th>UF</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr>
                                    <td><?= $item['fk_unity'] ?></td>
                                    <td><?= $item['month'] ?></td>
                                    <td><?= $item['year'] ?></td>
                                    <td><?= date_to_human_date($item['expiration_date']) ?></td>
                                    <td><?= $item['demand_end'] ?></td>
                                    <td class="hidden-xs">
                                        <a class="btn btn-success btn-xs" href = '<?= base_url() ?>tusd/update/<?= $item['pk_tusd'] ?>'><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" href='#delete' onclick="$('#pk_tusd').val(<?= $item['pk_tusd'] ?>)" data-toggle="modal"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete" class="modal fade">
    <form method="POST" action="<?= base_url() ?>tusd/delete">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="pk_tusd" id="pk_tusd" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este registro?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->
