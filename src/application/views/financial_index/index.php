<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Cálculo de Reajuste
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Cálculo de Reajuste</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Cálculo de Reajuste
                    <span class="tools pull-right">
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>financial_index/upsert'">
                            <i class="fa fa-plus pull-right"></i>
                            Índices Financeiros
                        </button>
                    </span>
                </header>

                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="">
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>financial_index/ajax" disabled=""/>

                        <div class="form-group <?= form_error('type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="type">Tipo</label>
                            <div class="col-lg-1">
                                <div class="radio-custom radio-success">
                                    <input type="radio" value="IGPM"  name="type" id="igpm" <?= set_value('type') == "IGPM" ? "checked" : "" ?>>
                                    <label for="igpm">IGPM</label>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="radio-custom radio-info">
                                    <input type="radio" value="IPCA"   name="type" id="ipca" <?= set_value('type') == "IPCA" ? "checked" : "" ?>>
                                    <label for="ipca">IPCA</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('month_base') != "" || form_error('year_base') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month_base">Data base</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="month_base" id="month_base">
                                    <option value="01" <?= set_value('month_base') == "01" ? 'selected' : '' ?>>Janeiro</option>
                                    <option value="02" <?= set_value('month_base') == "02" ? 'selected' : '' ?>>Fevereiro</option>
                                    <option value="03" <?= set_value('month_base') == "03" ? 'selected' : '' ?>>Março</option>
                                    <option value="04" <?= set_value('month_base') == "04" ? 'selected' : '' ?>>Abril</option>
                                    <option value="05" <?= set_value('month_base') == "05" ? 'selected' : '' ?>>Maio</option>
                                    <option value="06" <?= set_value('month_base') == "06" ? 'selected' : '' ?>>Junho</option>
                                    <option value="07" <?= set_value('month_base') == "07" ? 'selected' : '' ?>>Julho</option>
                                    <option value="08" <?= set_value('month_base') == "08" ? 'selected' : '' ?>>Agosto</option>
                                    <option value="09" <?= set_value('month_base') == "09" ? 'selected' : '' ?>>Setembro</option>
                                    <option value="10" <?= set_value('month_base') == "10" ? 'selected' : '' ?>>Outubro</option>
                                    <option value="11" <?= set_value('month_base') == "11" ? 'selected' : '' ?>>Novembro</option>
                                    <option value="12" <?= set_value('month_base') == "12" ? 'selected' : '' ?>>Dezembro</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control" name="year_base" id="year_base">
                                    <?php for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= date("Y", strtotime("-" . $i . " year")) ?>" <?= set_value('year_base') == "01" ? 'selected' : '' ?>><?= date("Y", strtotime("-" . $i . " year")) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <input type="text" disabled="disabled" id="index_base" name="index_base" class="form-control" value="<?= set_value('index_base') ?>" alt="valor3">
                            </div>

                        </div>
                        <div class="form-group <?= form_error('month') != "" || form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Data consulta</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="month" id="month">
                                    <option value="01" <?= set_value('month') == "01" ? 'selected' : '' ?>>Janeiro</option>
                                    <option value="02" <?= set_value('month') == "02" ? 'selected' : '' ?>>Fevereiro</option>
                                    <option value="03" <?= set_value('month') == "03" ? 'selected' : '' ?>>Março</option>
                                    <option value="04" <?= set_value('month') == "04" ? 'selected' : '' ?>>Abril</option>
                                    <option value="05" <?= set_value('month') == "05" ? 'selected' : '' ?>>Maio</option>
                                    <option value="06" <?= set_value('month') == "06" ? 'selected' : '' ?>>Junho</option>
                                    <option value="07" <?= set_value('month') == "07" ? 'selected' : '' ?>>Julho</option>
                                    <option value="08" <?= set_value('month') == "08" ? 'selected' : '' ?>>Agosto</option>
                                    <option value="09" <?= set_value('month') == "09" ? 'selected' : '' ?>>Setembro</option>
                                    <option value="10" <?= set_value('month') == "10" ? 'selected' : '' ?>>Outubro</option>
                                    <option value="11" <?= set_value('month') == "11" ? 'selected' : '' ?>>Novembro</option>
                                    <option value="12" <?= set_value('month') == "12" ? 'selected' : '' ?>>Dezembro</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control" name="year" id="year">
                                    <?php for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= date("Y", strtotime("-" . $i . " year")) ?>" <?= set_value('year') == "01" ? 'selected' : '' ?>><?= date("Y", strtotime("-" . $i . " year")) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <input type="text" disabled="disabled" id="index" name="index" class="form-control" value="<?= set_value('index') ?>" alt="valor3">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('value') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value">Valor</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value" name="value" class="form-control" value="<?= set_value('value') ?>" alt="valor3">
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-success" onclick="calculate(); return false;" name="btn_calculate" id="btn_calculate">Calcular</button>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('variable') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="variable">Variação %</label>
                            <div class="col-lg-2">
                                <input type="text" disabled="disabled" id="variable" name="variable" class="form-control" value="<?= set_value('variable') ?>" alt="valor3">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('value') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="result">Resultado</label>
                            <div class="col-lg-2">
                                <input type="text" disabled="disabled" id="result" name="result" class="form-control" value="<?= set_value('result') ?>" alt="valor3">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <a class="btn btn-info" href = 'http://portalibre.fgv.br/' target="_blank">Fonte IGP-M</a>
                                <a class="btn btn-info" href = 'ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/IPCA/Serie_Historica/ipca_SerieHist.zip' target="_blank" >Fonte IPCA</a>
                            </div>
                        </div>
                    </form>
                </div>

            </section>

        </div>


        <div class="col-sm-12">
            <section class="panel">
                <?php if (!empty($indices)) : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>IPCA</th>
                                <th>IGPM</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($indices as $indice => $value) : ?>

                                <tr>
                                    <td><?= date("m/Y", strtotime($indice)) ?></td>
                                    <td>
                                        <?= number_format($value['IPCA'], 3, ',', '.') ?>
                                    </td>
                                    <td>
                                        <?= number_format($value['IGPM'], 3, ',', '.') ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        </tbody>                        
                    </table>
                <?php endif; ?>
            </section>
        </div>
    </div>

</div>


