<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Editar ICMS
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar ICMS
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>icms/update/<?= $this->Icms_model->_pk_icms ?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        
                        <div class="form-group <?= form_error('state') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="state">Estado</label>
                            <div class="col-lg-4">
                                <select id="state" name="state" class="form-control">
                                    <option
                                        value="AC" <?= set_value('state') == 'AC' || $this->Icms_model->_state == 'AC' ? 'selected' : '' ?>>
                                        Acre
                                    </option>
                                    <option
                                        value="AL" <?= set_value('state') == 'AL' || $this->Icms_model->_state == 'AL' ? 'selected' : '' ?>>
                                        Alagoas
                                    </option>
                                    <option
                                        value="AP" <?= set_value('state') == 'AP' || $this->Icms_model->_state == 'AP' ? 'selected' : '' ?>>
                                        Amapá
                                    </option>
                                    <option
                                        value="AM" <?= set_value('state') == 'AM' || $this->Icms_model->_state == 'AM' ? 'selected' : '' ?>>
                                        Amazonas
                                    </option>
                                    <option
                                        value="BA" <?= set_value('state') == 'BA' || $this->Icms_model->_state == 'BA' ? 'selected' : '' ?>>
                                        Bahia
                                    </option>
                                    <option
                                        value="CE" <?= set_value('state') == 'CE' || $this->Icms_model->_state == 'CE'  ? 'selected' : '' ?>>
                                        Ceará
                                    </option>
                                    <option
                                        value="DF" <?= set_value('state') == 'DF' || $this->Icms_model->_state == 'DF'  ? 'selected' : '' ?>>
                                        Distrito Federal
                                    </option>
                                    <option
                                        value="ES" <?= set_value('state') == 'ES' || $this->Icms_model->_state == 'ES'  ? 'selected' : '' ?>>
                                        Espirito Santo
                                    </option>
                                    <option
                                        value="GO" <?= set_value('state') == 'GO' || $this->Icms_model->_state == 'GO'  ? 'selected' : '' ?>>
                                        Goiás
                                    </option>
                                    <option
                                        value="MA" <?= set_value('state') == 'MA' || $this->Icms_model->_state == 'MA'  ? 'selected' : '' ?>>
                                        Maranhão
                                    </option>
                                    <option
                                        value="MS" <?= set_value('state') == 'MS' || $this->Icms_model->_state == 'MS'  ? 'selected' : '' ?>>
                                        Mato Grosso do Sul
                                    </option>
                                    <option
                                        value="MT" <?= set_value('state') == 'MT' || $this->Icms_model->_state == 'MT'  ? 'selected' : '' ?>>
                                        Mato Grosso
                                    </option>
                                    <option
                                        value="MG" <?= set_value('state') == 'MG' || $this->Icms_model->_state == 'MG'  ? 'selected' : '' ?>>
                                        Minas Gerais
                                    </option>
                                    <option
                                        value="PA" <?= set_value('state') == 'PA' || $this->Icms_model->_state == 'PA'  ? 'selected' : '' ?>>
                                        Pará
                                    </option>
                                    <option
                                        value="PB" <?= set_value('state') == 'PB' || $this->Icms_model->_state == 'PB'  ? 'selected' : '' ?>>
                                        Paraíba
                                    </option>
                                    <option
                                        value="PR" <?= set_value('state') == 'PR' || $this->Icms_model->_state == 'PR'  ? 'selected' : '' ?>>
                                        Paraná
                                    </option>
                                    <option
                                        value="PE" <?= set_value('state') == 'PE' || $this->Icms_model->_state == 'PE'  ? 'selected' : '' ?>>
                                        Pernambuco
                                    </option>
                                    <option
                                        value="PI" <?= set_value('state') == 'PI' || $this->Icms_model->_state == 'PI'  ? 'selected' : '' ?>>
                                        Piauí
                                    </option>
                                    <option
                                        value="RJ" <?= set_value('state') == 'RJ' || $this->Icms_model->_state == 'RJ'  ? 'selected' : '' ?>>
                                        Rio de Janeiro
                                    </option>
                                    <option
                                        value="RN" <?= set_value('state') == 'RN' || $this->Icms_model->_state == 'RN'  ? 'selected' : '' ?>>
                                        Rio Grande do Norte
                                    </option>
                                    <option
                                        value="RS" <?= set_value('state') == 'RS' || $this->Icms_model->_state == 'RS'  ? 'selected' : '' ?>>
                                        Rio Grande do Sul
                                    </option>
                                    <option
                                        value="RO" <?= set_value('state') == 'RO' || $this->Icms_model->_state == 'RO'  ? 'selected' : '' ?>>
                                        Rondônia
                                    </option>
                                    <option
                                        value="RR" <?= set_value('state') == 'RR' || $this->Icms_model->_state == 'RR'  ? 'selected' : '' ?>>
                                        Roraima
                                    </option>
                                    <option
                                        value="SC" <?= set_value('state') == 'SC' || $this->Icms_model->_state == 'SC'  ? 'selected' : '' ?>>
                                        Santa Catarina
                                    </option>
                                    <option
                                        value="SP" <?= set_value('state') == 'SP' || $this->Icms_model->_state == 'SP'  ? 'selected' : '' ?>>
                                        São Paulo
                                    </option>
                                    <option
                                        value="SE" <?= set_value('state') == 'SE' || $this->Icms_model->_state == 'SE'  ? 'selected' : '' ?>>
                                        Sergipe
                                    </option>
                                    <option
                                        value="TO" <?= set_value('state') == 'TO' || $this->Icms_model->_state == 'TO'  ? 'selected' : '' ?>>
                                        Tocantins
                                    </option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('start_date') != "" || form_error('end_term') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Vigência</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="start_date" id="start_date" value="<?= set_value('start_date') != "" ? set_value('start_date') : date_to_human_date($this->Icms_model->_start_date) ?>" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="end_date" id="end_date" value="<?= set_value('end_date') != "" ? set_value('end_date') : date_to_human_date($this->Icms_model->_end_date) ?>" alt="date" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group <?= form_error('percent_industry') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="percent_industry">% Indústria</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="percent_industry" name="percent_industry" class="form-control" value="<?= set_value('percent_industry') != "" ? set_value('percent_industry') : number_format($this->Icms_model->_percent_industry, 2, ',', '') ?>" alt="valor2">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('percent_commercial') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="percent_commercial">% Comércio</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="percent_commercial" name="percent_commercial" class="form-control" value="<?= set_value('percent_commercial') != "" ? set_value('percent_commercial') : number_format($this->Icms_model->_percent_commercial, 2, ',', '') ?>" alt="valor2">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>icms'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
