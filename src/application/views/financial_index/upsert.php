<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Índice Financeiro
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo/Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Índice Financeiro
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <?php if ($success) : ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Informações atualizadas. 
                    </div>
                <?php endif; ?>

                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>financial_index/upsert">
                        <input type="hidden" name="create" id="create" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>financial_index/ajax" disabled=""/>

                        <div class="form-group <?= form_error('month') != "" || form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Data base</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="month" id="month">
                                    <option value="01" <?= set_value('month') == "01" ? 'selected' : '' ?>>Janeiro</option>
                                    <option value="02" <?= set_value('month') == "02" ? 'selected' : '' ?>>Fevereiro</option>
                                    <option value="03" <?= set_value('month') == "03" ? 'selected' : '' ?>>Março</option>
                                    <option value="04" <?= set_value('month') == "04" ? 'selected' : '' ?>>Abril</option>
                                    <option value="05" <?= set_value('month') == "05" ? 'selected' : '' ?>>Maio</option>
                                    <option value="06" <?= set_value('month') == "06" ? 'selected' : '' ?>>Junho</option>
                                    <option value="07" <?= set_value('month') == "07" ? 'selected' : '' ?>>Julho</option>
                                    <option value="08" <?= set_value('month') == "08" ? 'selected' : '' ?>>Agosto</option>
                                    <option value="09" <?= set_value('month') == "09" ? 'selected' : '' ?>>Setembro</option>
                                    <option value="10" <?= set_value('month') == "10" ? 'selected' : '' ?>>Outubro</option>
                                    <option value="11" <?= set_value('month') == "11" ? 'selected' : '' ?>>Novembro</option>
                                    <option value="12" <?= set_value('month') == "12" ? 'selected' : '' ?>>Dezembro</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control" name="year" id="year">
                                    <?php for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= date("Y", strtotime("-" . $i . " year")) ?>" <?= set_value('year') == "01" ? 'selected' : '' ?>><?= date("Y", strtotime("-" . $i . " year")) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="type">Tipo</label>
                            <div class="col-lg-1">
                                <div class="radio-custom radio-success">
                                    <input type="radio" value="IGPM"  name="type" id="igpm" <?= set_value('type') == "IGPM" ? "checked" : "" ?>>
                                    <label for="igpm">IGPM</label>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="radio-custom radio-info">
                                    <input type="radio" value="IPCA"   name="type" id="ipca" <?= set_value('type') == "IPCA" ? "checked" : "" ?>>
                                    <label for="ipca">IPCA</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('value') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value">Valor</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value" name="value" class="form-control" value="<?= set_value('value') ?>" alt="valor3">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <a class="btn btn-info" href = 'http://www.ipeadata.gov.br/ExibeSerie.aspx?serid=37796' target="_blank">IGP-M (IPEA DATA)</a>
                                <a class="btn btn-info" href = 'http://www.ipeadata.gov.br/ExibeSerie.aspx?serid=36482' target="_blank" >IPEA DATA</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>financial_index'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

        <div class="col-sm-12">
            <section class="panel">
                <?php if (!empty($indices)) : ?>
                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>IPCA</th>
                                <th>IGPM</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($indices as $indice => $value) : ?>

                                <tr>
                                    <td><?= date("m/Y", strtotime($indice)) ?></td>
                                    <td>
                                        <?= number_format($value['IPCA'], 3, ',', '.') ?>
                                    </td>
                                    <td>
                                        <?= number_format($value['IGPM'], 3, ',', '.') ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        </tbody>                        
                    </table>
                <?php endif; ?>
            </section>
        </div>
    </div>
</div>
