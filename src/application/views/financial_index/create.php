<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Novo Índice Financeiro
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Novo</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Novo Índice Financeiro
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>financial_index/create">
                        <input type="hidden" name="create" id="create" value="true" />

                        <div class="form-group <?= form_error('month') != "" || form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Data base</label>
                            <div class="col-lg-2">
                                <select class="form-control" name="month" id="month">
                                    <option value="01" <?= set_value('month') == "01" ? 'selected' : '' ?>>Janeiro</option>
                                    <option value="02" <?= set_value('month') == "02" ? 'selected' : '' ?>>Fevereiro</option>
                                    <option value="03" <?= set_value('month') == "03" ? 'selected' : '' ?>>Março</option>
                                    <option value="04" <?= set_value('month') == "04" ? 'selected' : '' ?>>Abril</option>
                                    <option value="05" <?= set_value('month') == "05" ? 'selected' : '' ?>>Maio</option>
                                    <option value="06" <?= set_value('month') == "06" ? 'selected' : '' ?>>Junho</option>
                                    <option value="07" <?= set_value('month') == "07" ? 'selected' : '' ?>>Julho</option>
                                    <option value="08" <?= set_value('month') == "08" ? 'selected' : '' ?>>Agosto</option>
                                    <option value="09" <?= set_value('month') == "09" ? 'selected' : '' ?>>Setembro</option>
                                    <option value="10" <?= set_value('month') == "10" ? 'selected' : '' ?>>Outubro</option>
                                    <option value="11" <?= set_value('month') == "11" ? 'selected' : '' ?>>Novembro</option>
                                    <option value="12" <?= set_value('month') == "12" ? 'selected' : '' ?>>Dezembro</option>
                                </select>
                            </div>
                            <div class="col-lg-1">
                                <select class="form-control" name="year" id="year">
                                    <?php for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= date("Y", strtotime("-" . $i . " year")) ?>" <?= set_value('year') == "01" ? 'selected' : '' ?>><?= date("Y", strtotime("-" . $i . " year")) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="type">Tipo</label>
                            <div class="col-lg-1">
                                <div class="radio-custom radio-success">
                                    <input type="radio" value="IGPM"  name="type" id="igpm" checked="checked">
                                    <label for="igpm">IGPM</label>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="radio-custom radio-info">
                                    <input type="radio" value="IPCA"   name="type" id="ipca">
                                    <label for="ipca">IPCA</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('value') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="value">Valor</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="value" name="value" class="form-control" value="<?= set_value('value') ?>" alt="valor4">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>icms'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
