<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Editar Categoria de Produto
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Categoria de Produto
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>ccvee_energy/upsert/<?= $this->Ccvee_energy_model->_fk_ccvee_general ?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>
                        <input type="hidden" name="pk_ccvee_energy" id="pk_ccvee_energy" value="<?= $this->Ccvee_energy_model->_pk_ccvee_energy > 0 ? $this->Ccvee_energy_model->_pk_ccvee_energy : 0 ?>" disabled="disabled"/>
                        <input type="hidden" name="total_months" id="total_months" value="<?= count($merged_supply_period) ?>" />

                        <div class="form-group <?= form_error('supply_start') != "" || form_error('supply_end') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label">Período de Fornecimento</label>
                            <div class="col-md-4">
                                <div class="input-group input-large custom-date-range" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control default-date-picker" name="supply_start" id="supply_start" value="<?= date_to_human_date($this->Ccvee_energy_model->_supply_start) ?>" alt="date" >
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control default-date-picker" name="supply_end" id="supply_end" value="<?= date_to_human_date($this->Ccvee_energy_model->_supply_end) ?>" alt="date" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button data-toggle="button" class="btn btn-sm btn-success calcular" value="">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('differentiated_volume_period') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="differentiated_volume_period">Volume diferenciado por período</label>
                            <div class="col-lg-10">
                                <input type="checkbox" class="js-switch-green" value="1" name="differentiated_volume_period" id="differentiated_volume_period" <?= $this->Ccvee_energy_model->_differentiated_volume_period == 1 ? "checked" : "" ?>/>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('unit_measurement') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="unit_measurement">Unidade de medida</label>
                            <div class="col-lg-2">
                                <select name="unit_measurement" id="unit_measurement" class="form-control">
                                    <option value="MWh" <?php $this->Ccvee_energy_model->_unit_measurement == "MWh" ? "selected" : "" ?> >MWh</option>
                                    <option value="Mwm" <?php $this->Ccvee_energy_model->_unit_measurement == "MWm" ? "selected" : "" ?>>MWm</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('volume') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="volume">Volume</label>
                            <div class="col-lg-10" id="volumes" name="volumes">
                                <table class="table responsive-data-table data-table">
                                    <thead>
                                        <tr>
                                            <?php if (!empty($merged_supply_period)) : ?>
                                                <th>Período</th>
                                                <th>Previsto</th>
                                                <th>Sazonalizado</th>
                                                <th>Ajustado</th>
                                                <th>Copiar</th>
                                            <?php else : ?>
                                                <th>Previsto</th>
                                                <th>Sazonalizado</th>
                                                <th>Ajustado</th>
                                            <?php endif; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($merged_supply_period)) : ?>
                                            <?php foreach ($merged_supply_period as $k => $sp) : ?>
                                                <tr>
                                                    <td>
                                                        <input placeholder="" id="periodo<?= $k ?>" name="volume[<?= $k ?>][periodo]" class="form-control" value="<?= $sp['periodo'] ?>" disabled="" type="text">
                                                        <input placeholder="" id="period<?= $k ?>" name="volume[<?= $k ?>][period]" class="form-control" value="<?= $sp['periodo'] ?>" type="hidden">
                                                    </td>
                                                    <td>
                                                        <input placeholder="" id="forecast<?= $k ?>" name="volume[<?= $k ?>][forecast]" class="form-control" value="<?= number_format($sp['forecast'], 3, ",", "") ?>" alt="valor3" style="text-align: right;" type="text">
                                                    </td>
                                                    <td>
                                                        <input placeholder="" id="seasonal<?= $k ?>" name="volume[<?= $k ?>][seasonal]" class="form-control" value="<?= number_format($sp['seasonal'], 3, ",", "") ?>" alt="valor3" style="text-align: right;" type="text">
                                                    </td>
                                                    <td>
                                                        <input placeholder="" id="adjusted<?= $k ?>" name="volume[<?= $k ?>][adjusted]" class="form-control" value="<?= number_format($sp['adjusted'], 3, ",", "") ?>" alt="valor3" style="text-align: right;" type="text">
                                                    </td>
                                                    <td>
                                                        <button data-toggle="button" class="btn btn-sm btn-default copy" value="copy<?= $k ?>" onclick="copy_values(<?= $k ?>)"><i class="fa fa-copy"></i></button>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr>
                                                <td><input type="text" placeholder="" id="forecast" name="forecast" class="form-control" value="" alt="valor3" ></td>
                                                <td><input type="text" placeholder="" id="seasonal" name="seasonal" class="form-control" value="" alt="valor3" ></td>
                                                <td><input type="text" placeholder="" id="adjusted" name="adjusted" class="form-control" value="" alt="valor3" ></td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="form-group <?= form_error('seasonality') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="seasonality">Sazonalidade</label>
                            <div class="col-lg-10">
                                <input type="checkbox" class="js-switch-green" value="1" name="seasonality" id="seasonality" <?= $this->Ccvee_energy_model->_seasonal == 1 ? "checked" : "" ?>/>
                            </div>
                        </div>

                        <div id="seasonality_div" name="seasonality_div" style="display: <?= $this->Ccvee_energy_model->_seasonal == 1 ? "block" : "none" ?>">
                            <div class="form-group <?= form_error('seasonal_inferior') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="seasonal_inferior">Inferior</label>
                                <div class="col-lg-3">
                                    <input id="seasonal_inferior" type="text" value="<?= $this->Ccvee_energy_model->_seasonal_inferior ?>" name="seasonal_inferior" placeholder="Entre -100% e 0%">
                                </div>
                            </div>
                            <div class="form-group <?= form_error('seasonal_superior') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="seasonal_superior">Superior</label>
                                <div class="col-lg-3">
                                    <input id="seasonal_superior" type="text" value="<?= $this->Ccvee_energy_model->_seasonal_superior ?>" name="seasonal_superior" placeholder="Entre 0% e 100%">
                                </div>
                            </div>
                            <div class="form-group <?= form_error('seasonal_limit') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="limit">Datas Limite</label>
                                <div class="col-lg-6" id="seasonal_limit" name="seasonal_limit">
                                    <table class="table responsive-data-table data-table">
                                        <thead>
                                            <tr>
                                                <th>Ano</th>
                                                <th>Data limite para informar sazonalidade</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (!empty($merged_seasonal)) : ?>
                                                <?php foreach ($merged_seasonal as $k => $ms) : ?>
                                                    <tr>
                                                        <td class="col-lg-2">
                                                            <input type="text"  placeholder="" id="seasonal_limit_year_show" name="seasonal_limit_year_show" class="form-control" value="<?=$ms['year']?>" disabled="disabled" >
                                                            <input type="hidden" placeholder="" id="seasonal_limit_year" name="seasonal_limit[<?=$k?>][year]" class="form-control" value="<?=$ms['year']?>" >
                                                        </td>
                                                        <td class="col-lg-4">
                                                            <input type="text" class="form-control default-date-picker"  id="seasonal_limit_limit" name="seasonal_limit[<?=$k?>][limit]"  alt="date" value="<?=$ms['limit']?>" >
                                                        </td>
                                                    </tr>                                           
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>                                            

                                    <!--conteúdo por ajax-->
                                </div>
                            </div>

                        </div>

                        <div class="form-group <?= form_error('flexibility') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="flexibility">Flexibilidade</label>
                            <div class="col-lg-10">
                                <input type="checkbox" class="js-switch-green" value="1" name="flexibility" id="flexibility" <?= $this->Ccvee_energy_model->_flexibility == 1 ? "checked" : "" ?>/>
                            </div>
                        </div>

                        <div id="flexibility_div" name="flexibility_div" style="display: <?= $this->Ccvee_energy_model->_flexibility == 1 ? "block" : "none" ?>">
                            <div class="form-group <?= form_error('flexibility_inferior') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="flexibility_inferior">Inferior</label>
                                <div class="col-lg-3">
                                    <input id="flexibility_inferior" type="text" value="<?= $this->Ccvee_energy_model->_flexibility_inferior ?>" name="flexibility_inferior" placeholder="Entre -100% e 0%">
                                </div>
                            </div>
                            <div class="form-group <?= form_error('flexibility_superior') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="flexibility_superior">Superior</label>
                                <div class="col-lg-3">
                                    <input id="flexibility_superior" type="text" value="<?= $this->Ccvee_energy_model->_flexibility_superior ?>" name="flexibility_superior" placeholder="Entre 0% e 100%">
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('modulation') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="modulation">Modulação</label>
                            <div class="col-lg-2">
                                <input type='radio' name='modulation_radio' id='flat' value='flat' <?= $this->Ccvee_energy_model->_modulation == "flat" || $this->Ccvee_energy_model->_modulation == "" ? "checked" : "" ?> /> <b>Flat</b>
                            </div>
                            <div class="col-lg-2">
                                <input type='radio' name='modulation_radio' id='modulada' value='modulada' <?= $this->Ccvee_energy_model->_modulation == "modulada" ? "checked" : "" ?> /> <b>Modulada</b>
                            </div>
                        </div>

                        <div id="modulation_div" name="modulation_div" style="display: <?= $this->Ccvee_energy_model->_modulation == "modulada" ? "block" : "none" ?>">
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label" for="">&nbsp;</label>
                                <div class="col-lg-10">
                                    <table class="table responsive-data-table data-table">
                                        <thead>
                                            <tr>
                                                <th>Pesado</th>
                                                <th>Médio</th>
                                                <th>Leve</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('modulation_inferior') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="modulation_inferior">Inferior</label>
                                <div class="col-lg-10">
                                    <table class="table responsive-data-table data-table">
                                        <tbody>
                                            <tr>
                                                <td><input class="form-control" id="modulation_heavy_inferior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_heavy_inferior ?>" name="modulation_heavy_inferior" placeholder="Entre -100% e 0%" alt="negativo"></td>
                                                <td><input class="form-control" id="modulation_medium_inferior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_medium_inferior ?>" name="modulation_medium_inferior" placeholder="Entre -100% e 0%" alt="negativo"></td>
                                                <td><input class="form-control" id="modulation_light_inferior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_light_inferior ?>" name="modulation_light_inferior" placeholder="Entre -100% e 0%" alt="negativo"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('modulation_superior') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="modulation_superior">Superior</label>
                                <div class="col-lg-10">
                                    <table class="table responsive-data-table data-table">
                                        <tbody>
                                            <tr>
                                                <td><input class="form-control" id="modulation_heavy_superior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_heavy_superior ?>" name="modulation_heavy_superior" placeholder="Entre 0% e 100%" alt="fone"></td>
                                                <td><input class="form-control" id="modulation_medium_superior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_medium_superior ?>" name="modulation_medium_superior" placeholder="Entre 0% e 100%" alt="fone"></td>
                                                <td><input class="form-control" id="modulation_light_superior" type="text" value="<?= $this->Ccvee_energy_model->_modulation_light_superior ?>" name="modulation_light_superior" placeholder="Entre 0% e 100%" alt="fone"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('modulation_type_input') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="modulation_type_input">Tipo de Inserção da Modulação</label>
                                <div class="col-lg-2">
                                    <select name="modulation_type_input" id="modulation_type_input" class="form-control">
                                        <option>Selecione</option>
                                        <option value="manual" <?= $this->Ccvee_energy_model->_modulation_type_input == "manual" ? "selected" : "" ?>>Manual</option>
                                        <option value="medição" <?= $this->Ccvee_energy_model->_modulation_type_input == "medição" ? "selected" : "" ?>>Medição</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>product_category'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
