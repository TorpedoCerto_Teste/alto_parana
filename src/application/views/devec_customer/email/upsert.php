<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>DEVEC</title>
    </head>

    <body>
        <div style="background-color: #CCC; height:50px; text-align: center"> 
            <img src="http://altoparanaenergia.com.br/img/APE_4.png" alt="">
        </div>
        <br/>
        <p style="font-size: 20px; ; font-weight: bold; text-align: center; color: #999"> DEVEC </br> Declaração do Valor de Aquisição da Energia Elétrica em Ambiente de Contratação Livre </p>
        <p align="justify">Encaminhamos em anexo e disponibilizamos na <a href="<?= base_url() ?>customer/devec_customer" target="_blank">área do cliente</a> o espelho das informações que devem ser declaradas, via sistema da receita estadual, para o cumprimento da obrigação acessória da DEVEC. 

            </br></br> Esta declaração é necessária dentro dos estados signatários do convênio 77, de 5 de agosto de 2011, da SEFAZ. Nestes estados, a responsabilidade pelo recolhimento do ICMS sobre a energia elétrica adquirida no Mercado Livre é da distribuidora concessionária local. <br/></br> Resumo da declaração: </p>
        <br/>
        <table style="border:0px; width: 100%">
            <tr>
                <td width="300px">Agente</td>
                <td><?= $this->Agent_model->_community_name ?></td>
            </tr>
            <tr>
                <td>Unidade</td>
                <td><?=$unity_community_name?></td>
            </tr>
            <tr>
                <td>Referência</td>
                <td><?= $month ?>/<?= $year ?></td>
            </tr>

            <tr>
                <td>Data limite declaração estado SP</td>
                <td>Dia 14</td>
            </tr>
            <tr>
                <td>Data limite declaração demais estados</td>
                <td>Dia 12</td>
            </tr>
        </table>
        <br/>
        <h1><b>ATENÇÃO: ASSIM QUE A DEVEC FOR APRESENTADA, FAVOR RESPONDER ESTE E-MAIL COM O COMPROVANTE EM PDF PARA CONTROLE.</b></h1>
        <br/>
        <p>Qualquer dúvida ou eventual esclarecimento, estamos à disposição no telefone +55 <?= $this->config->item("config_phone") ?> e em nosso atendimento on-line, via chat da área do cliente.</p>
        <br/>
        <br/>
        <div style="font-size: 20px; ; font-weight: bold; color: #999"> Equipe Alto Paraná Energia </div>
        <div>
            <a href="https://www.altoparanaenergia.com" style="text-decoration: none; color: #999">www.altoparanaenergia.com</a><br/>
            <a href="mailto:contato@altoparanaenergia.com" style="text-decoration: none; color: #999">contato@altoparanaenergia.com</a>
        </div>
        <br/>
        <div style="background-color: rgb(32,56,100);text-align: justify;color:white;font-size: 8px;padding: 5px;">
            Importante: Esta é uma mensagem gerada pelo sistema. Não responda este email.<br/>Essa mensagem é destinada exclusivamente ao seu destinatário e pode conter informações confidenciais,protegidas por sigilo profissional ou cuja divulgação seja proibida por lei.<br/>O uso não autorizado de tais informações é proibido e está sujeito às penalidades cabíveis. 
        </div>
    </body>

</html>
