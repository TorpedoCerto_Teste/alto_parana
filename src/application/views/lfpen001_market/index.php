<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        LFPEN001 Mercado
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">LFPEN001 Mercado</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    LFPEN001 Mercado
                </header>
                <div class="panel-body">
                    <form id="frm_lfpen001_market" role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>lfpen001_market">
                        <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" disabled="disabled"/>

                        <div class="form-group ">
                            <div class="col-lg-2">
                                <span class="help-block">Ano</span>
                                <input type="text" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year"
                                       data-bts-min="1980"
                                       data-bts-max="2050"
                                       data-bts-step="1"
                                       data-bts-decimal="0"
                                       data-bts-step-interval="1"
                                       data-bts-force-step-divisibility="round"
                                       data-bts-step-interval-delay="500"
                                       data-bts-booster="true"
                                       data-bts-boostat="10"
                                       data-bts-max-boosted-step="false"
                                       data-bts-mousewheel="true"
                                       data-bts-button-down-class="btn btn-default"
                                       data-bts-button-up-class="btn btn-default"
                                       />
                            </div>
                        </div> 
                    </form> 


                    <table class="table responsive-data-table data-table">
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Data do Relatório</th>
                                <th>Data de Pagamento</th>
                            </tr>
                        </thead>
                        <tbody id="tbl_lfpen001">
                            <?php foreach ($list as $key => $item) : ?>
                                <tr>
                                    <td><?= $item['month'] . "/" . $item['year'] ?></td>
                                    <td><input type="text" class="form-control  reporting_date"  month="<?= $key ?>" value="<?= $item['reporting_date'] ?>" alt="date"></td>
                                    <td><input type="text" class="form-control  payment_date"  month="<?= $key ?>"  value="<?= $item['payment_date'] ?>" alt="date"></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>

    </div>
</div>

