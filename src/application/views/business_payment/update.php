<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body">
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>business_payment/update/<?= $this->Business_model->_pk_business ?>">
                        <input type="hidden" name="update" id="update" value="true" />

                        <div class="form-group <?= form_error('remuneration_type') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="remuneration_type">Forma de Remuneração</label>
                            <div class="col-lg-6">
                                <select id="remuneration_type" name="remuneration_type" class="form-control select2">
                                    <option></option>
                                    <option value="mensal_por_unidade" <?= $this->Business_payment_model->_remuneration_type == "mensal_por_unidade" ? "selected" : "" ?>>MENSAL POR UNIDADE</option>
                                    <option value="percentual_da_economia" <?= $this->Business_payment_model->_remuneration_type == "percentual_da_economia" ? "selected" : "" ?>>PERCENTUAL DA ECONOMIA</option>
                                    <option value="valor_sobre_a_energia" <?= $this->Business_payment_model->_remuneration_type == "valor_sobre_a_energia" ? "selected" : "" ?>>VALOR SOBRE A ENERGIA</option>
                                    <option value="valor_fechado" <?= $this->Business_payment_model->_remuneration_type == "valor_fechado" ? "selected" : "" ?>>VALOR FECHADO</option>
                                    <option value="mensal_por_contraparte" <?= $this->Business_payment_model->_remuneration_type == "mensal_por_contraparte" ? "selected" : "" ?>>MENSAL POR CONTRAPARTE</option>
                                    <option value="valor_fixo_mais_percentual_de_economia" <?= $this->Business_payment_model->_remuneration_type == "valor_fixo_mais_percentual_de_economia" ? "selected" : "" ?>>VALOR FIXO MAIS PERCENTUAL DE ECONOMIA</option>
                                </select>
                            </div>
                        </div>

                        <div name="div_mensal_por_unidade" id="div_mensal_por_unidade" style="display: <?= $this->Business_payment_model->_remuneration_type == "mensal_por_unidade" ? "block" : "none" ?>">

                            <div class="form-group <?= form_error('form_mensal_por_unidade') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="form_mensal_por_unidade">Mensal por Unidade</label>
                                <div class="col-lg-6">
                                    <?php if ($unities) : ?>
                                        <table class="table responsive-data-table data-table" name="table_mensal_por_unidade">
                                            <thead>
                                                <tr>
                                                    <th>Unidade</th>
                                                    <th>Valor R$</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <?php foreach ($monthly_unities as $pk_unity => $monthly_unity) : ?>
                                                    <tr>
                                                        <td><?= $monthly_unity['community_name'] ?></td>
                                                        <td>
                                                            <input type="text" placeholder="" id="monthly_value<?= $pk_unity ?>" name="monthly_value[<?= $pk_unity ?>]" class="form-control" value="<?= number_format($monthly_unity['monthly_value'], 2, ".", "") ?>" alt="valor2" />
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    <?php else : ?>
                                        <div class="alert alert-warning fade in">
                                            <button type="button" class="close close-sm" data-dismiss="alert">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Atenção!</strong> Nenhuma unidade foi inserida. Clique em <a href="#" class="btn btn-success">Adicionar Unidades</a> para continuar...
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>

                        <div name="div_percentual_da_economia" id="div_percentual_da_economia" style="display: <?= $this->Business_payment_model->_remuneration_type == "percentual_da_economia" ? "block" : "none" ?>">

                            <div class="form-group <?= form_error('form_percentual_da_economia') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="form_percentual_da_economia">Percentual da Economia</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Percentual %</span>
                                    <input type="text" name="percent_economy_value" id="percent_economy_value" value="<?= $this->Business_payment_model->_percent_economy_value ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Piso R$</span>
                                    <input type="text" name="percent_economy_floor" id="percent_economy_floor" value="<?= $this->Business_payment_model->_percent_economy_floor ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Teto R$</span>
                                    <input type="text" name="percent_economy_roof" id="percent_economy_roof" value="<?= $this->Business_payment_model->_percent_economy_roof ?>" alt="valor2">
                                </div>
                            </div>

                        </div>

                        <div name="div_valor_sobre_a_energia" id="div_valor_sobre_a_energia" style="display: <?= $this->Business_payment_model->_remuneration_type == "valor_sobre_a_energia" ? "block" : "none" ?>">

                            <div class="form-group <?= form_error('form_valor_sobre_a_energia') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="form_valor_sobre_a_energia">Valor Sobre a Energia</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Valor R$/MWh</span>
                                    <input type="text" name="over_energy_value" id="over_energy_value" value="<?= $this->Business_payment_model->_over_energy_value ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Piso R$</span>
                                    <input type="text" name="over_energy_floor" id="over_energy_floor" value="<?= $this->Business_payment_model->_over_energy_floor ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Teto R$</span>
                                    <input type="text" name="over_energy_roof" id="over_energy_roof" value="<?= $this->Business_payment_model->_over_energy_roof ?>" alt="valor2">
                                </div>
                            </div>

                        </div>

                        <div name="div_valor_fechado" id="div_valor_fechado" style="display: <?= $this->Business_payment_model->_remuneration_type == "valor_fechado" ? "block" : "none" ?>">

                            <div class="form-group <?= form_error('form_valor_fechado') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="form_valor_fechado">Valor Fechado</label>
                                <div class="col-lg-2">
                                    <span class="help-block">Valor</span>
                                    <input type="text" name="closed_value" id="closed_value" value="<?= $this->Business_payment_model->_closed_value ?>" alt="valor2">
                                </div>
                                <div class="col-lg-2">
                                    <span class="help-block">Parcelado</span>
                                    <label class="checkbox-custom check-success">
                                        <input type="checkbox" value="1" name="closed_installment" id="closed_installment" <?= $this->Business_payment_model->_closed_installment == 1 ? "checked" : "" ?>/><label for="closed_installment">Sim&nbsp;</label>
                                    </label>
                                </div>
                                <div name="div_closed_installment_quantity" id="div_closed_installment_quantity" class="col-lg-2" style="display: <?= $this->Business_payment_model->_closed_installment == 1 ? "block" : "none" ?>">
                                    <span class="help-block">Quantidade de Parcelas</span>
                                    <input type="text" name="closed_installment_quantity" id="closed_installment_quantity" value="<?= $this->Business_payment_model->_closed_installment_quantity ?>" alt="year">
                                </div>
                            </div>

                        </div>

                        <div name="div_mensal_por_contraparte" id="div_mensal_por_contraparte" style="display: <?= $this->Business_payment_model->_remuneration_type == "mensal_por_contraparte" ? "block" : "none" ?>">

                            <div class="form-group <?= form_error('form_mensal_por_contraparte') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="form_mensal_por_contraparte">Mensal por Contraparte</label>
                                <div class="col-lg-10">
                                    <span class="help-block">Tipo de Valor</span>

                                    <div class="radio-custom radio-success">
                                        <input name="monthly_counterpart_unity_quantity_consumption" id="ratear_quantidade_unidades" type="radio" value="ratear_quantidade_unidades" <?= $this->Business_payment_model->_monthly_counterpart_unity_quantity_consumption == "ratear_quantidade_unidades" ? "checked" : "" ?> >
                                        <label for="ratear_quantidade_unidades">Ratear pela Quantidade de Unidades</label>
                                    </div>
                                    <div class="radio-custom radio-success">
                                        <input name="monthly_counterpart_unity_quantity_consumption" id="ratear_comumo_energia_unidades" type="radio" value="ratear_comumo_energia_unidades" <?= $this->Business_payment_model->_monthly_counterpart_unity_quantity_consumption == "ratear_comumo_energia_unidades" ? "checked" : "" ?> >
                                        <label for="ratear_comumo_energia_unidades">Ratear pelo Consumo de Energia das Unidades</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?= form_error('monthly_counterpart_value') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="monthly_counterpart_value">Valor</label>
                                <div class="col-lg-2">
                                    <input type="text" name="monthly_counterpart_value" id="monthly_counterpart_value" value="<?= $this->Business_payment_model->_monthly_counterpart_value ?>" alt="valor2">
                                </div>
                            </div>
                        </div>

                        <div name="div_valor_fixo_mais_percentual_de_economia" id="div_valor_fixo_mais_percentual_de_economia" style="display: <?= $this->Business_payment_model->_remuneration_type == "valor_fixo_mais_percentual_de_economia" ? "block" : "none" ?>">

                            <div class="form-group <?= form_error('form_valor_fixo_mais_percentual_de_economia') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="form_valor_fixo_mais_percentual_de_economia">Valor Fixo Mais Percentual de Economia</label>
                                <div class="col-lg-10">
                                    <?php if ($unities) : ?>
                                        <table class="table responsive-data-table data-table" name="table_valor_fixo_mais_percentual_de_economia">
                                            <thead>
                                                <tr>
                                                    <th>Unidade</th>
                                                    <th>Valor Fixo R$</th>
                                                    <th>Percentual</th>
                                                    <th>Piso R$</th>
                                                    <th>Teto R$</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <?php foreach ($payment_fixed_value_unities as $pk_unity => $payment_fixed_value_unity) : ?>
                                                    <tr>
                                                        <td>
                                                            <?= $payment_fixed_value_unity['community_name'] ?>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="" id="fixed_value<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][fixed_value]" class="form-control" value="<?= number_format($payment_fixed_value_unity['fixed_value'], 2, ".", "") ?>" alt="valor2" />
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="" id="percent<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][percent]" class="form-control" value="<?= number_format($payment_fixed_value_unity['percent'], 2, ".", "") ?>" alt="valor2" />
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="" id="floor<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][floor]" class="form-control" value="<?= number_format($payment_fixed_value_unity['floor'], 2, ".", "") ?>" alt="valor2" />
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="" id="roof<?= $pk_unity ?>" name="payment_fixed_value_unities[<?= $pk_unity ?>][roof]" class="form-control" value="<?= number_format($payment_fixed_value_unity['roof'], 2, ".", "") ?>" alt="valor2" />                                                    </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    <?php else : ?>
                                        <div class="alert alert-warning fade in">
                                            <button type="button" class="close close-sm" data-dismiss="alert">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Atenção!</strong> Nenhuma unidade foi inserida. Clique em <a href="#" class="btn btn-success">Adicionar Unidades</a> para continuar...
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>

                        <div class=" s-row">
                        </div>

                        <div class="form-group <?= form_error('due') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="due">Dia de vencimento</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="due" name="due">
                                    <option value="dia_util" <?= $this->Business_payment_model->_due == "dia_util" ? "selected" : "" ?>>Dia útil</option>
                                    <option value="dia_fixo" <?= $this->Business_payment_model->_due == "dia_fixo" ? "selected" : "" ?>>Dia fixo</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control" id="due_day" name="due_day">
                                    <?php for ($i = 1; $i <= 28; $i++) : ?>
                                        <option value="<?= $i ?>" <?= $this->Business_payment_model->_due_day == $i ? "selected" : "" ?>><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('due_period') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="due_period">Mês de vencimento</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="due_period" name="due_period">
                                    <option value="M" <?= $this->Business_payment_model->_due_period == "M" ? "selected" : "" ?>>Mês Fornecimento</option>
                                    <option value="MA" <?= $this->Business_payment_model->_due_period == "MA" ? "selected" : "" ?>>Meses Antes</option>
                                    <option value="MS" <?= $this->Business_payment_model->_due_period == "MS" ? "selected" : "" ?>>Meses Subsequentes</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="due_month" name="due_month" class="form-control" value="<?= set_value('due_month') != "" ? set_value('due_month') : $this->Business_payment_model->_due_month ?>" alt="year">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('financial_index') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="financial_index">Índice de Reajuste</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="financial_index" name="financial_index">
                                    <option value="IGPM" <?= $this->Business_payment_model->_financial_index == "IGPM" ? "selected" : "" ?>>IGP-M</option>
                                    <option value="IPCA" <?= $this->Business_payment_model->_financial_index == "IPCA" ? "selected" : "" ?>>IPCA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('index_readjustment_calculation') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="index_readjustment_calculation">Modo de Cálculo do Índice de Reajuste</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="index_readjustment_calculation" name="index_readjustment_calculation">
                                    <option value="1" <?= $this->Business_payment_model->_index_readjustment_calculation == "1" ? "selected" : "" ?>>Padrão</option>
                                    <option value="2" <?= $this->Business_payment_model->_index_readjustment_calculation == "2" ? "selected" : "" ?>>Não realizar o reajuste se o valor ajustado for menor que o valor atual</option>
                                    <option value="3" <?= $this->Business_payment_model->_index_readjustment_calculation == "3" ? "selected" : "" ?>>Não considerar índices negativos no cálculo</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('database') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="database">Data base</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="database" name="database" class="form-control default-date-picker" value="<?= set_value('database') != "" ? set_value('database') : date_to_human_date($this->Business_payment_model->_database) ?>" alt="date">
                            </div>
                        </div>

                        <div class="form-group <?= form_error('adjustment_montly_manually') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="adjustment_montly_manually">Ajustar manualmente a data do primeiro reajuste</label>
                            <div class="col-lg-10">
                                <label class="checkbox-custom check-success">
                                    <input type="checkbox" value="1" name="adjustment_montly_manually" id="adjustment_montly_manually" <?= $this->Business_payment_model->_adjustment_montly_manually == 1 ? "checked" : "" ?>/><label for="adjustment_montly_manually">&nbsp;</label>
                                </label>
                            </div>
                        </div>

                        <div id="div_date_adjustment" name="div_date_adjustment" class="form-group <?= form_error('date_adjustment') != "" ? "has-error" : ""; ?>" style="display: <?= $this->Business_payment_model->_adjustment_montly_manually == 1 ? "block" : "none" ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="date_adjustment">Inserir Data</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="date_adjustment" name="date_adjustment" class="form-control default-date-picker" value="<?= set_value('date_adjustment') != "" ? set_value('date_adjustment') : date_to_human_date($this->Business_payment_model->_date_adjustment) ?>" alt="date">
                            </div>
                            <div class="col-lg-2">
                                <select name="date_adjustment_frequency" id="date_adjustment_frequency" class="form-control">
                                    <option value="12em12" <?= $this->Business_payment_model->_date_adjustment_frequency == "12em12" ? "selected" : "" ?>>12 em 12</option>
                                    <option value="todoJaneiro" <?= $this->Business_payment_model->_date_adjustment_frequency == "todoJaneiro" ? "selected" : "" ?>>Todo Janeiro</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group <?= form_error('form_penalty_interest') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="form_penalty_interest">Multas e Juros</label>
                            <div class="col-lg-2">
                                <span class="help-block">Multa R$</span>
                                <input type="text" name="penalty" id="penalty" value="<?= $this->Business_payment_model->_penalty ?>" alt="valor2">
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Juros %</span>
                                <input type="text" name="interest" id="interest" value="<?= $this->Business_payment_model->_interest ?>" alt="valor2">
                            </div>
                            <div class="col-lg-2">
                                <span class="help-block">Frequência</span>
                                <select name='interest_frequency' id='interest_frequency' class='form-control'>
                                    <option value='diaria' <?= $this->Business_payment_model->_interest_frequency == "diaria" ? "selected" : "" ?>>DIÁRIA</option>
                                    <option value='mensal' <?= $this->Business_payment_model->_interest_frequency == "mensal" ? "selected" : "" ?>>MENSAL</option>
                                    <option value='anual' <?= $this->Business_payment_model->_interest_frequency == "anual" ? "selected" : "" ?>>ANUAL</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
