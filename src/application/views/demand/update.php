<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        Editar Demanda Contratada
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/demanda">Unidades</a></li>
            <li class="active">Editar Demanda</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    Editar Demanda
                </header>
                <?php if ($error) : ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Houve um erro ao salvar os dados. 
                    </div>
                <?php endif; ?>
                <div class="panel-body" >
                    <form role="form" class="form-horizontal" method="POST" action="<?= base_url() ?>demand/update/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/<?= $this->Demand_model->_pk_demand ?>">
                        <input type="hidden" name="update" id="update" value="true" />
                        <div class="form-group <?= form_error('start_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="start_date">Data Inicial</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="Data Inicial" id="start_date" name="start_date" class="form-control default-date-picker" value="<?= set_value('start_date') != "" ? set_value('start_date') : date_to_human_date($this->Demand_model->_start_date) ?>" alt="date">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('end_date') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="end_date">Data Final</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="Data Final" id="end_date" name="end_date" class="form-control default-date-picker" value="<?= set_value('end_date') != "" ? set_value('end_date') : date_to_human_date($this->Demand_model->_end_date) ?>" alt="date" <?= set_value('current') == 1 || $this->Demand_model->_current == 1 ? 'disabled' : '' ?>>
                                <input type="checkbox" name="current" id="current" value="1" <?= set_value('current') == 1 || $this->Demand_model->_current == 1 ? 'checked' : '' ?>>&nbsp;Atual
                            </div>
                        </div>
                        <div class="form-group <?= form_error('tension') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="tension">Tensão</label>
                            <div class="col-lg-2">
                                <select name="tension" id="tension" class="form-control">
                                    <option value="A1" <?= set_value('tension') == 'A1' || $this->Demand_model->_tension == 'A1'  ? 'selected' : '' ?>>A1</option>
                                    <option value="A2" <?= set_value('tension') == 'A2' || $this->Demand_model->_tension == 'A2' ? 'selected' : '' ?>>A2</option>
                                    <option value="A3" <?= set_value('tension') == 'A3' || $this->Demand_model->_tension == 'A3' ? 'selected' : '' ?>>A3</option>
                                    <option value="A3a" <?= set_value('tension') == 'A3a' || $this->Demand_model->_tension == 'A3a' ? 'selected' : '' ?>>A3a</option>
                                    <option value="A4" <?= set_value('tension') == 'A4' || $this->Demand_model->_tension == 'A4' ? 'selected' : '' ?>>A4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('tariff_mode') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="tariff_mode">Modalidade Tarifária</label>
                            <div class="col-lg-2">
                                <select name="tariff_mode" id="tariff_mode" class="form-control">
                                    <option value="AZUL" <?= set_value('tariff_mode') == 'AZUL' || $this->Demand_model->_tariff_mode == 'AZUL' ? 'selected' : '' ?>>AZUL</option>
                                    <option value="VERDE" <?= set_value('tariff_mode') == 'VERDE' || $this->Demand_model->_tariff_mode == 'VERDE' ? 'selected' : '' ?>>VERDE</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('end_demand') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="end_demand">Demanda na Ponta (kW)</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="end_demand" name="end_demand" class="form-control " value="<?= set_value('end_demand') != "" ? set_value('end_demand') : $this->Demand_model->_end_demand ?>" alt="fone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('demand_tip_out') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="demand_tip_out">Demanda Fora de Ponta (kW)</label>
                            <div class="col-lg-2">
                                <input type="text" placeholder="" id="demand_tip_out" name="demand_tip_out" class="form-control " value="<?= set_value('demand_tip_out') != "" ? set_value('demand_tip_out') : $this->Demand_model->_demand_tip_out ?>" alt="fone">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('captive_situation') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="captive_situation">Situação Cativa</label>
                            <div class="col-lg-1" style="width:50px;" name="div_captive_situation" id="div_captive_situation">
                                <input type="checkbox" <?= set_value('captive_situation') == 1 || $this->Demand_model->_captive_situation == 1 ? 'checked' : '' ?> class="js-switch-small-blue" name="captive_situation" id="captive_situation" value="1">
                            </div>
                        </div>
                        <div name="plus_captive_situation" id="plus_captive_situation" style="display: <?= set_value('captive_situation') == 1 || $this->Demand_model->_captive_situation == 1 ? 'block' : 'none' ?>">
                            <div class="form-group <?= form_error('edge_generators') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="edge_generators">Geradores na Ponta</label>
                                <div class="col-lg-1" style="width:50px;" name="div_edge_generators" id="div_edge_generators">
                                    <input type="checkbox" <?= set_value('edge_generators') == 1 || $this->Demand_model->_edge_generators == 1 ? 'checked' : '' ?> class="js-switch-small-green" name="edge_generators" id="edge_generators" value="1">
                                </div>
                            </div>
                            <div class="form-group <?= form_error('price_kwh') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="price_kwh">R$/MWh (gerador)</label>
                                <div class="col-lg-1">
                                    <input type="text" value="<?= set_value('price_kwh') != "" ? set_value('price_kwh') : $this->Demand_model->_price_kwh ?>" class="form-control" name="price_kwh" id="price_kwh" alt="valor">
                                </div>
                            </div>
                            <div class="form-group <?= form_error('not_consumed_at_the_tip') != "" ? "has-error" : ""; ?>">
                                <label class="col-lg-2 col-sm-2 control-label" for="not_consumed_at_the_tip">Não Consumia na Ponta</label>
                                <div class="col-lg-1">
                                    <input type="checkbox" <?= set_value('not_consumed_at_the_tip') == 1 || $this->Demand_model->_not_consumed_at_the_tip == 1 ? 'checked' : '' ?> class="js-switch-small-red" name="not_consumed_at_the_tip" id="not_consumed_at_the_tip" value="1">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Salvar</button>
                                <button class="btn btn-default" onclick="window.location.href = '<?= base_url() ?>unity/view/<?= $this->Agent_model->_pk_agent ?>/<?= $this->Unity_model->_pk_unity ?>/demanda'; return false;">Cancelar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </section>
        </div>

    </div>
</div>
