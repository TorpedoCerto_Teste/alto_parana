<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        ODC - Importar Medições
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="<?= base_url() ?>odc_record">ODC - Medições</a></li>
            <li class="active">Importar Medições</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    ODC - Importar Medições
                    <span class="tools pull-right">
                        <label class="checkbox-custom check-success">
                            <input name="odc_module" id="odc_module" value="true" type="checkbox"><label for="odc_module">&nbsp;Módulo de Análise ODC |</label>
                            <input name="rewrite_way2" id="rewrite_way2" value="true" type="checkbox"><label for="rewrite_way2">&nbsp;Substituir dados da Way2 |</label>
                            <input name="rewrite_scde" id="rewrite_scde" value="true" type="checkbox"><label for="rewrite_scde">&nbsp;Substituir dados do SCDE</label>
                        </label>
                    </span>
                </header>
                <div class="panel-body">
                    <form id="my-awesome-dropzone" action="<?= base_url() ?>odc_record/dropzone" class="dropzone">
                        <input type="hidden" name="odc_module_data" id="odc_module_data" value="false" />
                        <input type="hidden" name="rewrite_way2_data" id="rewrite_way2_data" value="false" />
                        <input type="hidden" name="rewrite_scde_data" id="rewrite_scde_data" value="true" />
                    </form>
                    <div class="row">
                        <div class="col-sm-12">
                            <section class="panel">
                                <?php if (!$upload) : ?>
                                    <div class="alert alert-warning fade in">
                                        <button type="button" class="close close-sm" data-dismiss="alert">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <strong>Atenção!</strong> Nenhum arquivo! Para inserir, arraste e solte na área pontilhada.
                                    </div>
                                <?php else : ?>
                                    <table class="table responsive-data-table data-table">
                                        <thead>
                                            <tr>
                                                <th>Arquivo</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($upload as $dir_name => $directory) : ?>
                                                <?php foreach ($directory as $item) : ?>
                                                    <?php if (!is_array($item)) : ?>
                                                        <tr>
                                                            <td><a href="https://docs.google.com/viewer?embedded=true&url=<?= base_url() ?>uploads/<?= $dir_name ?>/<?= $item ?>" target="_blank"><?= $item ?></td>
                                                            <td class="hidden-xs">
                                                                <a class="btn btn-danger btn-xs tooltips" href='#delete_file' onclick="$('#file_to_delete').val('<?= $item ?>')" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o "></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php endif; ?>

                            </section>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>
</div>


<!-- Modal delete file -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delete_file" class="modal fade">
    <form method="POST" action="<?= base_url() ?>odc_record/delete_file">
        <input type="hidden" name="delete" id="delete" value="true" />
        <input type="hidden" name="file_to_delete" id="file_to_delete" value="0" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deseja deletar este arquivo?</h4>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                    <button class="btn btn-danger" type="submit">Deletar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- modal -->