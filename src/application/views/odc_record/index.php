<!-- page head start-->
<div class="page-head">
    <h3 class="m-b-less">
        ODC - Origem de Dados da Coleta - Medições
    </h3>
    <!--<span class="sub-title">Welcome to Static Table</span>-->
    <div class="state-information">
        <ol class="breadcrumb m-b-less bg-less">
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">ODC - Medições</li>
        </ol>
    </div>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading ">
                    ODC - Medições
                    <span class="tools pull-right">
                        <button class="btn btn-primary addon-btn m-b-10" onclick="$('#panel_filter').toggle('slow')">
                            <i class="fa fa-filter pull-right"></i>
                            Filtrar
                        </button>
                        <button class="btn btn-success addon-btn m-b-10" onclick="window.location.href = '<?= base_url() ?>odc_record/upload/'">
                            <i class="fa fa-plus pull-right"></i>
                            Importar Medições
                        </button>
                    </span>
                </header>
                <div class="panel-body" name="panel_filter" id="panel_filter" style="display: none">
                    <form role="form" class="form-inline" method="POST" action="<?= base_url() ?>odc_record">
                        <input type="hidden" name="filter" id="filter" value="true" />
                        <div class="form-group <?= form_error('year') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="year">Ano</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="" id="year" name="year" class="form-control" value="<?= set_value('year') != "" ? set_value('year') : date("Y") ?>" alt="year">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('month') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="month">Mês</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="month" name="month">
                                    <option value="01" <?= date("m") == "01" ? "selected" : "" ?>>Janeiro</option>
                                    <option value="02" <?= date("m") == "02" ? "selected" : "" ?>>Fevereiro</option>
                                    <option value="03" <?= date("m") == "03" ? "selected" : "" ?>>Março</option>
                                    <option value="04" <?= date("m") == "04" ? "selected" : "" ?>>Abril</option>
                                    <option value="05" <?= date("m") == "05" ? "selected" : "" ?>>Maio</option>
                                    <option value="06" <?= date("m") == "06" ? "selected" : "" ?>>Junho</option>
                                    <option value="07" <?= date("m") == "07" ? "selected" : "" ?>>Julho</option>
                                    <option value="08" <?= date("m") == "08" ? "selected" : "" ?>>Agosto</option>
                                    <option value="09" <?= date("m") == "09" ? "selected" : "" ?>>Setembro</option>
                                    <option value="10" <?= date("m") == "10" ? "selected" : "" ?>>Outubro</option>
                                    <option value="11" <?= date("m") == "11" ? "selected" : "" ?>>Novembro</option>
                                    <option value="12" <?= date("m") == "12" ? "selected" : "" ?>>Dezembro</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group <?= form_error('fk_gauge') != "" ? "has-error" : ""; ?>">
                            <label class="col-lg-2 col-sm-2 control-label" for="fk_gauge">Medidor</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="fk_gauge" name="fk_gauge">
                                    <?php if ($gauges) : ?>
                                        <?php foreach ($gauges as $gauge) : ?>
                                            <optgroup label="<?=$gauge['community_name']?>">
                                                <?php foreach ($gauge['gauges'] as $medidor) : ?>
                                                    <option value="<?=$medidor['pk_gauge']?>"><?=$medidor['gauge']?> - <?=$medidor['internal_name']?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">Filtrar</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <?php if (!$list) : ?>
                    <div class="alert alert-warning fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Atenção!</strong> Nenhuma informação encontrada. Clique em "Novo" para inserir um registro.
                    </div>
                <?php else : ?>
                    <div id="chart_data" style="display: none"><?=$graphic?></div>
                    <div id="columnChart"></div>

                
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Consumo Ativa</th>
                                <th>Qualidade</th>
                                <th>Origem</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) : ?>
                                <tr style="color: <?=$item['quality'] == "Consistido" ? 'green' : 'red'?>">
                                    <td><?= date("d/m/Y H:i", strtotime($item['record_date'])) ?></td>
                                    <td><?= number_format($item['value'],2,",","") ?></td>
                                    <td><?= $item['quality'] ?></td>
                                    <td><?= $item['source'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </section>
        </div>

    </div>
</div>


