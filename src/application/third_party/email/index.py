#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from dotenv import load_dotenv
load_dotenv()
import base64
import json


def send_email():
    try:
        to = sys.argv[1]
        subject = base64.b64decode(sys.argv[2]).decode('utf-8')
        message = base64.b64decode(sys.argv[3]).decode('utf-8')

        server = smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"))
        server.ehlo()
        server.login(os.getenv("SMTP_MAIL"), os.getenv("SMTP_PASS"))

        # create a multipart message object
        message_obj = MIMEMultipart()
        message_obj['To'] = to
        message_obj['From'] = os.getenv("SMTP_MAIL")
        message_obj['Subject'] = subject

        # attach the message body as a plain text MIME object
        text = MIMEText(message, "html")
        message_obj.attach(text)

        # attach the attachment file as an application MIME object
        if len(sys.argv) > 4 and len(sys.argv[4]) > 0:
            attachment_path = sys.argv[4]
            with open(attachment_path, 'rb') as f:
                attachment = MIMEApplication(f.read(), _subtype=os.path.splitext(attachment_path)[-1][1:])
                attachment.add_header('Content-Disposition', 'attachment', filename=os.path.basename(attachment_path))
                message_obj.attach(attachment)

        # send the email
        server.sendmail(os.getenv("SMTP_MAIL"), to, message_obj.as_string())
        server.quit()
        print(json.dumps({"response": 200, "method": "send_email", "message": 0}))
    except Exception as error:
        print(json.dumps({"response": 500, "method": "send_email", "message": str(error)}))
        sys.exit()

#Start script
if __name__ == "__main__":
    send_email()