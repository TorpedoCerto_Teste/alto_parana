<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_payment_price_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_payment_price
  fk_ccvee_payment
  period
  provided_base
  fixed
  manual_fixed
  provided_fixed
  point_monthly

 */
class Ccvee_payment_price_model extends CI_Model {

    var $_pk_ccvee_payment_price = "";
    var $_fk_ccvee_payment       = "";
    var $_period                 = "";
    var $_provided_base          = "";
    var $_fixed                  = "";
    var $_manual_fixed           = "";
    var $_provided_fixed         = "";
    var $_point_monthly          = "";
    var $_status                 = "";
    var $_created_at             = "";
    var $_updated_at             = "";
    var $_status_active          = 1;
    var $_status_inactive        = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data                     = get_array_from_object($this);
        $data['created_at']       = date("Y-m-d H:m:s");
        $data['fk_ccvee_payment'] = $this->_fk_ccvee_payment;
        $this->db->insert('ccvee_payment_price', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_ccvee_payment_price !== "") {
            $this->db->where('pk_ccvee_payment_price', $this->_pk_ccvee_payment_price);
        }
        if ($this->_fk_ccvee_payment !== "") {
            $this->db->where('fk_ccvee_payment', $this->_fk_ccvee_payment);
        }
        $rec = $this->db->get('ccvee_payment_price');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_payment_price', $this->_pk_ccvee_payment_price);
        $this->db->update('ccvee_payment_price', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_payment_price', $this->_pk_ccvee_payment_price);
        $q                  = $this->db->get('ccvee_payment_price');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_payment_price', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_payment_price', $this->_pk_ccvee_payment_price);
            $this->db->update('ccvee_payment_price', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_ccvee_payment !== "") {
            $this->db->where('fk_ccvee_payment', $this->_fk_ccvee_payment);
        }
        else {
            $this->db->where('pk_ccvee_payment_price', $this->_pk_ccvee_payment_price);
        }
        $this->db->update('ccvee_payment_price', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_ccvee_payment_price !== "") {
            $this->db->where('pk_ccvee_payment_price', $this->_pk_ccvee_payment_price);
        }
        if ($this->_fk_ccvee_payment !== "") {
            $this->db->where('fk_ccvee_payment', $this->_fk_ccvee_payment);
        }
        $rec = $this->db->get('ccvee_payment_price');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
