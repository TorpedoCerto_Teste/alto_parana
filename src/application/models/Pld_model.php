<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Pld_model
 *
 * @author rogeriopellarin
 * 
  pk_pld
  year
  month
  seco
  s
  ne
  n
  spread_i5
  spread_i1
  status_pld
 * 
 */
class Pld_model extends CI_Model {
    var $_pk_pld = "";
    var $_year = "";
    var $_month = "";
    var $_seco = "";
    var $_s = "";
    var $_ne = "";
    var $_n = "";
    var $_spread_i5 = "";
    var $_spread_i1 = "";
    var $_status_pld = "";  
    var $_status = "";
    var $_created_at = "";
    var $_updated_at = "";
    var $_status_active = 1;
    var $_status_inactive = 0;
    var $_status_pld_forecast = 0;
    var $_status_pld_closed = 1;
    var $_limit = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('pld', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_pld !== "") {
            $this->db->where('pk_pld', $this->_pk_pld);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_status_pld !== "") {
            $this->db->where('status_pld', $this->_status_pld);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('pld');
        if ((isset($rec->num_rows) && $rec->num_rows >= 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_pld', $this->_pk_pld);
        $this->db->update('pld', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        unset($data['created_at']);
        unset($data['pk_pld']);
        unset($data['status_pld_forecast']);
        unset($data['status_pld_closed']);
        unset($data['limit']);

        $this->db->where('year', $this->_year);
        $this->db->where('month', $this->_month);
        $q = $this->db->get('pld');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('pld', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            $rec = $q->result_array();
            $this->db->where('pk_pld', $rec[0]['pk_pld']);
            $this->db->update('pld', $data);
            return $this->_pk_pld;
        }
    }   

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_pld', $this->_pk_pld);
        $this->db->update('pld', $data);
        return $this->db->affected_rows();
    }    

    function fetch() {
        $this->db->select(array(
            'pld.*'
        ));
        if ($this->_year > 0) {
            $this->db->where('year', $this->_year);
        }
        $this->db->where('status', $this->_status_active);
        $this->db->order_by('pld.year', 'DESC');
        $this->db->order_by('pld.month', 'ASC');
        if ($this->_limit > 0) {
            $this->db->limit($this->_limit);
        }
        $rec = $this->db->get('pld');
        if ($rec->result_id->num_rows > 0) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
