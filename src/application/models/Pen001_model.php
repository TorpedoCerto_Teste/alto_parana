<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of pen001_model
 *
 * @author Rogerio Pellarin
 *
  pk_pen001
  month
  year
  fk_agent
  pen001_file
  notification_term_file
  provided_insuficiency
  provided_penalty
  provided_cicle_month
  provided_cicle_year
  provided_payment_date
  realized_insuficiency
  realized_penalty
  realized_cicle_month
  realized_cicle_year
  realized_payment_date
  realized_notification_term
  memo
  status
  created_at
  updated_at

 */
class Pen001_model extends CI_Model {

    var $_pk_pen001                  = "";
    var $_month                      = "";
    var $_year                       = "";
    var $_fk_agent                   = "";
    var $_pen001_file                = "";
    var $_notification_term_file     = "";
    var $_provided_insuficiency      = "";
    var $_provided_penalty           = "";
    var $_provided_cicle_month       = "";
    var $_provided_cicle_year        = "";
    var $_provided_payment_date      = "";
    var $_realized_insuficiency      = "";
    var $_realized_penalty           = "";
    var $_realized_cicle_month       = "";
    var $_realized_cicle_year        = "";
    var $_realized_payment_date      = "";
    var $_realized_notification_term = "";
    var $_memo                       = "";
    var $_status                     = "";
    var $_created_at                 = "";
    var $_updated_at                 = "";
    var $_status_active              = 1;
    var $_status_inactive            = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('pen001', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_pen001 !== "") {
            $this->db->where('pk_pen001', $this->_pk_pen001);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        $rec = $this->db->get('pen001');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_pen001', $this->_pk_pen001);
        $this->db->update('pen001', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_pen001', $this->_pk_pen001);
        $q = $this->db->get('pen001');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('pen001', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_pen001', $this->_pk_pen001);
            $this->db->update('pen001', $data);
            return $this->_pk_pen001;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_pen001', $this->_pk_pen001);
        $this->db->update('pen001', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $rec = $this->db->get('pen001');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
