<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Contact_model
 *
 * Table columns
 *
 * pk_contact
 * fk_agent
 * name
 * surname
 * phone
 * extension
 * mobile
 * email
 * responsibility
 * status
 * created_at
 * updated_at
 * allow_access
 * password
 * birthdate
 *
 * @authon luancomputacao
 */
class Contact_model extends CI_Model {

    var $_pk_contact      = "";
    var $_fk_agent        = "";
    var $_name            = "";
    var $_surname         = "";
    var $_phone           = "";
    var $_extension       = "";
    var $_mobile          = "";
    var $_email           = "";
    var $_password        = "";
    var $_birthdate       = "";
    var $_responsibility  = "";
    var $_status          = "";
    var $_allow_access    = "";
    var $_allow           = "1";
    var $_deny            = "0";
    var $_status_active   = "1";
    var $_status_inactive = "2";
    var $_status_deleted  = "0";
    var $_created_at      = "";
    var $_updated_at      = "";
    //variavel para query especial
    var $_fk_unity        = "";

    function __construct() {
        parent::__construct();
    }

    function find_by_email_and_password() {
        $rec = $this->db->get_where('contact', array('email' => $this->_email, 'password' => $this->_password, 'status' => $this->_status_active, 'allow_access' => $this->_allow), 1);
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Insert new database record
     *
     * @return integer ID of inserted data
     */
    function create() {
        $data = get_array_from_object($this);
        unset($data['allow']);
        unset($data['deny']);
        $this->db->insert('contact', $data);
        return $this->db->insert_id();
    }

    /**
     * Get one record by Primary Key
     *
     * @return array|bool
     */
    function read() {
        $this->db->where('pk_contact', $this->_pk_contact);
        $rec = $this->db->get('contact');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Caso encontre algum email já cadastrado, retorna false, para o validador
     * não autorizar este mesmo email (unique)
     * @return boolean
     */
    function read_by_email_unique() {
        $this->db->where('email', $this->_email);
        $rec = $this->db->get('contact');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Update by this Object
     *
     * @return integer Number of rows affected
     */
    function update() {
        $data = get_array_from_object($this);
        unset($data['allow']);
        unset($data['deny']);
        unset($data['fk_agent']);
        $this->db->where('pk_contact', $this->_pk_contact);
        $this->db->update('contact', $data);
        return $this->_pk_contact;
    }

    /**
     * Delete record
     *
     * @return mixed
     */
    function delete() {
        $data = array(
            'status' => $this->_status_deleted,
        );
        $this->db->where('pk_contact', $this->_pk_contact);
        $this->db->update('contact', $data);
        return $this->db->affected_rows();
    }

    /**
     * Get multiple records by status
     *
     * @return array|bool
     */
    function fetch() {
        $this->db->where('contact.status != ', $this->_status_deleted);
        if ($this->_status !== "") {
            $this->db->where('contact.status', $this->_status);
        }
        $this->db->order_by('contact.name');
        $rec = $this->db->get('contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    
    function fetch_to() {
        $this->db->where('contact.status != ', $this->_status_deleted);
        if ($this->_status !== "") {
            $this->db->where('contact.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->join('agent_contact', 'agent_contact.fk_contact = contact.pk_contact');
            $this->db->where('agent_contact.fk_agent', $this->_fk_agent);
        }
        $this->db->order_by('contact.name');
        $rec = $this->db->get('contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    

    function fetch_by_agent() {
        $this->db->select(
                array(
                    'contact.*'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('contact.status', $this->_status);
        }
        $this->db->join('agent_contact', 'agent_contact.fk_contact = contact.pk_contact');
        $this->db->where('agent_contact.fk_agent', $this->_fk_agent);
        $this->db->where('contact.status != ', $this->_status_deleted);
        $this->db->where('contact.pk_contact NOT IN (SELECT fk_contact FROM unity_contact WHERE fk_unity = ' . $this->_fk_unity . ' AND status = ' . $this->_status_active . ')', NULL, FALSE);

        $rec = $this->db->get('contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_with_agent() {
        $this->db->select(
                array(
                    'contact.*',
                    'agent.community_name'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('contact.status', $this->_status);
        }
        $this->db->join('agent', 'agent.pk_agent = contact.fk_agent');
        $this->db->order_by('agent.community_name');

        $rec = $this->db->get('contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    function fetch_external_contacts() {
        $this->db->select(
                array(
                    'contact.*'
                )
        );
        $this->db->where('contact.status', $this->_status_active);
        $this->db->where('contact.pk_contact NOT IN (SELECT fk_contact FROM agent_contact WHERE fk_agent = '.$this->_fk_agent.' AND status = '.$this->_status_active.')');
        $this->db->order_by('contact.name');
        $this->db->group_by('contact.pk_contact');
        $rec = $this->db->get('contact');
        
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }    

    /**
     * Set properties of this Object
     *
     * @param array $ret    (array(pk_contact, fk_agent, name, surname, phone, extension, mobile, email, responsibility, status, created_at, updated_at))
     */
    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

    function status() {
        $data = array(
            'status' => $this->_status
        );
        $this->db->where('pk_contact', $this->_pk_contact);
        $this->db->update('contact', $data);
        return $this->db->affected_rows();
    }

}
