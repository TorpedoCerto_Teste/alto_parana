<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of financial_index_model
 *
 * @author Rogerio Pellarin
 * 
  pk_financial_index
  type
  value
  date
  status
  created_at
  updated_at

 */
class financial_index_model extends CI_Model {

    var $_pk_financial_index = "";
    var $_type               = "";
    var $_value              = "";
    var $_date               = "";
    var $_status             = "";
    var $_created_at         = "";
    var $_updated_at         = "";
    var $_status_active      = 1;
    var $_status_inactive    = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('financial_index', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_financial_index', $this->_pk_financial_index);
        $rec = $this->db->get('financial_index');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_financial_index', $this->_pk_financial_index);
        $this->db->update('financial_index', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_financial_index', $this->_pk_financial_index);
        $this->db->update('financial_index', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->order_by('date', 'DESC');
        $rec = $this->db->get('financial_index');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

    function upsert() {
        $data = get_array_from_object($this);
        $ret  = 0;
        $this->db->where('type', $this->_type);
        $this->db->where('date', $this->_date);
        $q    = $this->db->get('financial_index');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('financial_index', $data);
            $ret = $this->db->insert_id();
        }
        else {
            unset($data['created_at']);
            $this->db->where('type', $this->_type);
            $this->db->where('date', $this->_date);
            $this->db->update('financial_index', $data);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function find_by_date_and_type() {
        $this->db->where('date', $this->_date);
        $this->db->where('type', $this->_type);
        $rec = $this->db->get('financial_index');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

}
