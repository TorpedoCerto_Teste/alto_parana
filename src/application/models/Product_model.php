<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of product_model
 *
 * @author Rogerio Pellarin
 * 
  pk_product
  fk_product_category
  product
  status
  created_at
  updated_at
 */
class Product_model extends CI_Model {

    var $_pk_product          = "";
    var $_fk_product_category = "";
    var $_product             = "";
    var $_product_short       = "";
    var $_status              = "";
    var $_created_at          = "";
    var $_updated_at          = "";
    var $_status_active       = 1;
    var $_status_inactive     = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('product', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_product', $this->_pk_product);
        $this->db->where('fk_product_category', $this->_fk_product_category);
        $rec = $this->db->get('product');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_product', $this->_pk_product);
        $this->db->where('fk_product_category', $this->_fk_product_category);
        $this->db->update('product', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_product', $this->_pk_product);
        $this->db->update('product', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'product.*',
            'product_category.category'
        ));
        if ($this->_fk_product_category !== "") {
            $this->db->where('product.fk_product_category', $this->_fk_product_category);
        }
        if ($this->_status !== "") {
            $this->db->where('product.status', $this->_status);
        }
        $this->db->join('product_category', 'product_category.pk_product_category = product.fk_product_category');
        $rec = $this->db->get('product');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
