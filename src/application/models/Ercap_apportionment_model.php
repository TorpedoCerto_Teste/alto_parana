<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ercap_apportionment_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ercap_apportionment
  fk_ercap
  fk_unity

 */
class Ercap_apportionment_model extends CI_Model {

    var $_pk_ercap_apportionment = "";
    var $_fk_ercap               = "";
    var $_fk_unity             = "";
    var $_status               = "";
    var $_created_at           = "";
    var $_updated_at           = "";
    var $_status_active        = 1;
    var $_status_inactive      = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('ercap_apportionment', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ercap_apportionment', $this->_pk_ercap_apportionment);
        $rec = $this->db->get('ercap_apportionment');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_ercap_apportionment', $this->_pk_ercap_apportionment);
        $this->db->update('ercap_apportionment', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_ercap_apportionment', $this->_pk_ercap_apportionment);
        $q = $this->db->get('ercap_apportionment');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ercap_apportionment', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ercap_apportionment', $this->_pk_ercap_apportionment);
            $this->db->update('ercap_apportionment', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_ercap !== "") {
            $this->db->where('fk_ercap', $this->_fk_ercap);
        }
        if ($this->_pk_ercap_apportionment !== "") {
            $this->db->where('pk_ercap_apportionment', $this->_pk_ercap_apportionment);
        }
        $this->db->update('ercap_apportionment', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_ercap !== "") {
            $this->db->where('fk_ercap', $this->_fk_ercap);
        }
        $rec = $this->db->get('ercap_apportionment');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
