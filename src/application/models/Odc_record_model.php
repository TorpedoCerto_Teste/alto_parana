<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of odc_record_model
 *
 * @author Rogerio Pellarin
 * 
    pk_odc_record
    fk_gauge
    record_date
    order
    value
    quality
    source


 */
class Odc_record_model extends CI_Model {

    var $_pk_odc_record   = "";
    var $_fk_gauge        = "";
    var $_record_date     = "";
    var $_order           = "";
    var $_value           = "";
    var $_quality         = "";
    var $_source          = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('odc_record', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_odc_record', $this->_pk_odc_record);
        $rec = $this->db->get('odc_record');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_odc_record', $this->_pk_odc_record);
        $this->db->update('odc_record', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_odc_record', $this->_pk_odc_record);
        $q = $this->db->get('odc_record');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('odc_record', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_odc_record', $this->_pk_odc_record);
            $this->db->update('odc_record', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_odc_record', $this->_pk_odc_record);
        $this->db->update('odc_record', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('odc_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }
    
    function fetch_by_report() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where('fk_gauge', $this->_fk_gauge);
        $this->db->where('record_date >=', $this->_record_date);
        $this->db->where('record_date <=', date("Y-m-t 23:59:59", strtotime($this->_record_date)));
        $this->db->order_by('record_date', 'DESC');
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('odc_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    function read_by_record_date_time() {
        $this->db->where('record_date', $this->_record_date);
        $this->db->where('order', $this->_order);
        $this->db->where('status', $this->_status_active);
        $this->db->where('fk_gauge', $this->_fk_gauge);
        $rec = $this->db->get('odc_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    function delete_by_record_date() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('record_date', $this->_record_date);
        $this->db->where('status', $this->_status_active);
        $this->db->update('odc_record', $data);
        return $this->db->affected_rows();
    }

    function hard_delete_by_record_date() {
        if ($this->_fk_gauge) {
            $this->db->where('fk_gauge', $this->_fk_gauge);
        }
        if ($this->_order) {
            $this->db->where('order', $this->_order);
        }        
        $this->db->where('record_date', $this->_record_date);
        $this->db->where('status', $this->_status_active);
        $this->db->delete('odc_record');
        return $this->db->affected_rows();
    }


}
