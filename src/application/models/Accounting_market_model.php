<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of accounting_market_model
 *
 * @author Rogerio Pellarin
 *
  pk_accounting_market
  month
  year
  reporting_date
  debit_date
  credit_date
  gfn001_date
  warranty_limit_date
  status
  created_at
  updated_at

 */
class Accounting_market_model extends CI_Model {

    var $_pk_accounting_market = "";
    var $_month                = "";
    var $_year                 = "";
    var $_reporting_date       = "";
    var $_debit_date           = "";
    var $_credit_date          = "";
    var $_gfn001_date          = "";
    var $_warranty_limit_date  = "";
    var $_status               = "";
    var $_created_at           = "";
    var $_updated_at           = "";
    var $_status_active        = 1;
    var $_status_inactive      = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('accounting_market', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_pk_accounting_market !== "") {
            $this->db->where('pk_accounting_market', $this->_pk_accounting_market);
        }
        $rec = $this->db->get('accounting_market');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_accounting_market', $this->_pk_accounting_market);
        $this->db->update('accounting_market', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        if ($this->_pk_accounting_market == "") {
            $this->db->insert('accounting_market', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            unset($data['pk_accounting_market']);
            $this->db->where('pk_accounting_market', $this->_pk_accounting_market);
            $this->db->update('accounting_market', $data);
            return $this->_pk_accounting_market;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_accounting_market', $this->_pk_accounting_market);
        $this->db->update('accounting_market', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        $rec = $this->db->get('accounting_market');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
