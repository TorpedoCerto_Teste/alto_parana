<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of controller_group_model
 *
 * @author Rogerio Pellarin
 * 
  pk_controller_group
  controller
  fk_group


 */
class Controller_group_model extends CI_Model {

    var $_pk_controller_group = "";
    var $_controller          = "";
    var $_fk_group            = "";
    var $_status              = "";
    var $_created_at          = "";
    var $_updated_at          = "";
    var $_status_active       = 1;
    var $_status_inactive     = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('controller_group', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_controller !== "") {
            $this->db->where('controller', $this->_controller);
        }
        if ($this->_pk_controller_group !== "") {
            $this->db->where('pk_controller_group', $this->_pk_controller_group);
        }
        $rec = $this->db->get('controller_group');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_controller_group', $this->_pk_controller_group);
        $this->db->update('controller_group', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('controller', $this->_controller);
        $q = $this->db->get('controller_group');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('controller_group', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('controller', $this->_controller);
            $this->db->update('controller_group', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_controller_group', $this->_pk_controller_group);
        $this->db->update('controller_group', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('controller_group');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
