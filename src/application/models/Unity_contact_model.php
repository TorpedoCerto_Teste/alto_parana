<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of unity_contact_model
 *
 * @author Rogerio Pellarin
 * 
  pk_unity_contact
  fk_unity
  fk_contact
  status
  created_at
  updated_at

 */
class Unity_contact_model extends CI_Model {

    var $_pk_unity_contact = "";
    var $_fk_unity         = "";
    var $_fk_contact       = "";
    var $_status           = "";
    var $_created_at       = "";
    var $_updated_at       = "";
    var $_status_active    = 1;
    var $_status_inactive  = 2;
    var $_status_deleted   = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('unity_contact', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_unity_contact', $this->_pk_unity_contact);
        $rec = $this->db->get('unity_contact');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_unity_contact', $this->_pk_unity_contact);
        $this->db->update('unity_contact', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_unity_contact', $this->_pk_unity_contact);
        $this->db->update('unity_contact', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'contact.name AS name',
                    'contact.surname AS surname',
                    'contact.phone AS phone',
                    'contact.extension AS extension',
                    'contact.mobile AS mobile',
                    'contact.email AS email',
                    'contact.responsibility AS responsibility',
                    'unity_contact.*'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('unity_contact.status', $this->_status);
        }
        $this->db->where('unity_contact.status !=', $this->_status_deleted);
        $this->db->where('unity_contact.fk_unity', $this->_fk_unity);
        $this->db->join('contact', 'contact.pk_contact = unity_contact.fk_contact');
        $rec = $this->db->get('unity_contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
