<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_unity_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_unity
  fk_ccvee_general
  fk_unity
  volumn_mwm
  contract_cliqccee
  start_date
  end_date

 */
class Ccvee_unity_model extends CI_Model {

    var $_pk_ccvee_unity    = "";
    var $_fk_ccvee_general  = "";
    var $_fk_unity          = "";
    var $_volumn_mwm        = "";
    var $_contract_cliqccee = "";
    var $_start_date        = "";
    var $_end_date          = "";
    var $_status            = "";
    var $_created_at        = "";
    var $_updated_at        = "";
    var $_status_active     = 1;
    var $_status_inactive   = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_unity', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee_unity', $this->_pk_ccvee_unity);
        $rec = $this->db->get('ccvee_unity');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_unity', $this->_pk_ccvee_unity);
        $this->db->update('ccvee_unity', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data                     = get_array_from_object($this);
        $data['created_at']       = date("Y-m-d H:m:s");
        $data['fk_ccvee_general'] = $this->_fk_ccvee_general;
        $this->db->where('pk_ccvee_unity', $this->_pk_ccvee_unity);
        $q                        = $this->db->get('ccvee_unity');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_unity', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_unity', $this->_pk_ccvee_unity);
            $this->db->update('ccvee_unity', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee_unity', $this->_pk_ccvee_unity);
        $this->db->update('ccvee_unity', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'ccvee_unity.*',
                    'unity.company_name AS unity_company_name',
                    'unity.community_name AS unity_community_name',
                    'agent.company_name AS agent_company_name',
                    'agent.community_name AS agent_community_name'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('ccvee_unity.status', $this->_status);
        }
        $this->db->join('unity', 'unity.pk_unity = ccvee_unity.fk_unity AND unity.status = '.$this->_status_active);
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->where('fk_ccvee_general', $this->_fk_ccvee_general);
        $rec = $this->db->get('ccvee_unity');

//print_r($this->db->last_query()); die;

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
