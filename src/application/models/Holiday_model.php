<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holiday_model extends CI_Model {

    var $_pk_holiday      = "";
    var $_date_holiday    = "";
    var $_holiday         = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;
    var $_year            = "";

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        unset($data['year']);
        $this->db->insert('holiday', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_holiday !== "") {
            $this->db->where('pk_holiday', $this->_pk_holiday);
        }
        if ($this->_date_holiday !== "") {
            $this->db->where('date_holiday', $this->_date_holiday);
        }
        $rec = $this->db->get('holiday');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data = get_array_from_object($this);
        unset($data['year']);
        $this->db->where('pk_holiday', $this->_pk_holiday);
        $this->db->update('holiday', $data);
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        unset($data['year']);

        $this->db->where('pk_holiday', $this->_pk__holiday);
        $q = $this->db->get('holiday');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('holiday', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_holiday', $this->_pk_holiday);
            $this->db->update('holiday', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_holiday', $this->_pk_holiday);
        $this->db->update('holiday', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_year !== "") {
            $this->db->where('year(date_holiday)', $this->_year);
        }
        $rec = $this->db->get('holiday');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
