<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Profile_model
 *
 * @author rogeriopellarin
 * 
  pk_profile
  fk_submarket
  fk_agent
  profile
  status
  created_at
  updated_at
  fk_standard
  fk_category
  code
  memo
  icms
  memo_icms
 *  * 
 *  */
class Profile_model extends CI_Model {

    var $_pk_profile      = "";
    var $_fk_submarket    = "";
    var $_fk_agent        = "";
    var $_profile         = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_fk_standard     = "";
    var $_fk_category     = "";
    var $_code            = "";
    var $_status_active   = 1;
    var $_status_inactive = 2;
    var $_status_deleted  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('profile', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_profile', $this->_pk_profile);
        $this->db->where('fk_agent', $this->_fk_agent);
        $rec = $this->db->get('profile');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_profile', $this->_pk_profile);
        $this->db->update('profile', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_profile', $this->_pk_profile);
        $this->db->update('profile', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'profile.*',
                    'submarket.submarket',
                    'submarket.initials',
                    'standard.standard',
                    'category.category'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('profile.status', $this->_status);
        }
        $this->db->where('profile.status !=', $this->_status_deleted);
        if ($this->_fk_agent !== "") {
            $this->db->where('profile.fk_agent', $this->_fk_agent);
        }
        $this->db->join('submarket', 'submarket.pk_submarket = profile.fk_submarket');
        $this->db->join('standard', 'standard.pk_standard = profile.fk_standard');
        $this->db->join('category', 'category.pk_category = profile.fk_category');
        $rec = $this->db->get('profile');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
