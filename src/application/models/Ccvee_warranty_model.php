<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_warranty_model
 *
 * @author Rogerio Pellarin
 *
  pk_ccvee_warranty
  fk_ccvee_general
  fk_modality_warranty
  year
  deadline_status
  customer_communication_status
  start_date
  end_date
  deadline_contract_presentation
  presentation_date
  billing_cycles
  status
  created_at
  updated_at

 */
class Ccvee_warranty_model extends CI_Model {

    var $_pk_ccvee_warranty              = "";
    var $_fk_ccvee_general               = "";
    var $_fk_modality_warranty           = "";
    var $_year                           = "";
    var $_deadline_status                = "";
    var $_customer_communication_status  = "";
    var $_start_date                     = "";
    var $_end_date                       = "";
    var $_deadline_contract_presentation = "";
    var $_presentation_date              = "";
    var $_billing_cycles                 = "";
    var $_status                         = "";
    var $_created_at                     = "";
    var $_updated_at                     = "";
    var $_status_active                  = 1;
    var $_status_inactive                = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_warranty', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee_warranty', $this->_pk_ccvee_warranty);
        $rec = $this->db->get('ccvee_warranty');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_warranty', $this->_pk_ccvee_warranty);
        $this->db->update('ccvee_warranty', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_warranty', $this->_pk_ccvee_warranty);
        $q                  = $this->db->get('ccvee_warranty');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_warranty', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_warranty', $this->_pk_ccvee_warranty);
            $this->db->update('ccvee_warranty', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee_warranty', $this->_pk_ccvee_warranty);
        $this->db->update('ccvee_warranty', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'ccvee_warranty.*',
                    'modality_warranty.modality'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('ccvee_warranty.status', $this->_status);
        }
        if ($this->_fk_ccvee_general !== "") {
            $this->db->where('ccvee_warranty.fk_ccvee_general', $this->_fk_ccvee_general);
        }
        $this->db->join('modality_warranty', 'ccvee_warranty.fk_modality_warranty = modality_warranty.pk_modality_warranty');
        $rec = $this->db->get('ccvee_warranty');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
