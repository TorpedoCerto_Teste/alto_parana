<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of energy_offer_model
 *
 * @author Rogerio Pellarin
 * 
  pk_energy_offer
  offer_date
  fk_energy_type 
  fk_submarket   
  set_origin     
  fk_agent       
 */
class Energy_offer_model extends CI_Model {

    var $_pk_energy_offer = null;
    var $_offer_date      = null;
    var $_fk_energy_type  = null;
    var $_fk_submarket    = null;
    var $_set_origin      = null;
    var $_fk_agent        = null;
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('energy_offer', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_energy_offer', $this->_pk_energy_offer);
        $rec = $this->db->get('energy_offer');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_energy_offer', $this->_pk_energy_offer);
        $this->db->update('energy_offer', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_energy_offer', $this->_pk_energy_offer);
        $q    = $this->db->get('energy_offer');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('energy_offer', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_energy_offer', $this->_pk_energy_offer);
            $this->db->update('energy_offer', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_energy_offer', $this->_pk_energy_offer);
        $this->db->update('energy_offer', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'energy_offer.*',
                    'energy_type.name AS energy_type_name',
                    'agent.community_name AS agent_community_name',
                    'submarket.submarket', 
                    'submarket.initials', 
                    'energy_offer_year.year',
                    'energy_offer_year.price'                    
                )
        );
        if ($this->_status !== "") {
            $this->db->where('energy_offer.status', $this->_status);
        }
        $this->db->join('energy_type', 'energy_type.pk_energy_type = energy_offer.fk_energy_type');
        $this->db->join('submarket', 'submarket.pk_submarket  = energy_offer.fk_submarket');
        $this->db->join('agent', 'agent.pk_agent = energy_offer.fk_agent', 'left');
        $this->db->join('energy_offer_year', 'energy_offer_year.fk_energy_offer = energy_offer.pk_energy_offer AND energy_offer_year.status = 1');
        $rec = $this->db->get('energy_offer');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
