<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of accounting_model
 *
 * @author Rogerio Pellarin
 *
  pk_accounting
  fk_agent
  month
  year
  value
  rel_gfn001
  mem_gfn001
  sum001
  ess
  mcp
  total
  previous_summary
  adjust
  type
  value_paid
  lfn002
  status
  created_at
  updated_at

 */
class Accounting_model extends CI_Model {

    var $_pk_accounting    = "";
    var $_fk_agent         = "";
    var $_month            = "";
    var $_year             = "";
    var $_value            = "";
    var $_rel_gfn001       = "";
    var $_mem_gfn001       = "";
    var $_sum001           = "";
    var $_ess              = "";
    var $_mcp              = "";
    var $_total            = "";
    var $_previous_summary = "";
    var $_adjust           = "";
    var $_type             = "";
    var $_value_paid       = "";
    var $_lfn002           = "";
    var $_status           = "";
    var $_created_at       = "";
    var $_updated_at       = "";
    var $_status_active    = 1;
    var $_status_inactive  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('accounting', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('accounting.status', $this->_status);
        }
        if ($this->_pk_accounting !== "") {
            $this->db->where('accounting.pk_accounting', $this->_pk_accounting);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('accounting.fk_agent', $this->_fk_agent);
        }
        if ($this->_month !== "") {
            $this->db->where('accounting.month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('accounting.year', $this->_year);
        }
        $rec = $this->db->get('accounting');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_accounting', $this->_pk_accounting);
        $this->db->update('accounting', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_accounting', $this->_pk_accounting);
        $q = $this->db->get('accounting');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('accounting', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_accounting', $this->_pk_accounting);
            $this->db->update('accounting', $data);
            return $this->_pk_accounting;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_accounting', $this->_pk_accounting);
        $this->db->update('accounting', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'accounting.*',
            'agent.community_name AS agent'
        ));
        if ($this->_status !== "") {
            $this->db->where('accounting.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('accounting.fk_agent', $this->_fk_agent);
        }
        $this->db->join('agent', 'agent.pk_agent = accounting.fk_agent');
        $rec = $this->db->get('accounting');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->db->select(array(
            'accounting.*',
            'accounting_market.reporting_date',
            'accounting_market.credit_date',
            'accounting_market.debit_date',
            'accounting_market.warranty_limit_date',
            'agent.community_name AS agent'
        ));
        if ($this->_status !== "") {
            $this->db->where('accounting.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('accounting.fk_agent', $this->_fk_agent);
        }
        if ($this->_pk_accounting !== "") {
            $this->db->where('accounting.pk_accounting', $this->_pk_accounting);
        }
        $this->db->join('accounting_market', 'accounting.month = accounting_market.month AND accounting.year = accounting_market.year AND accounting_market.status = ' . $this->_status_active, 'LEFT');
        $this->db->join('agent', 'agent.pk_agent = accounting.fk_agent');
        $rec = $this->db->get('accounting');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
