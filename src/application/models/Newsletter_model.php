<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Newsletter_model
 *
 * @author Rogerio Pellarin
 * 


 */
class Newsletter_model extends CI_Model {

    var $_pk_newsletter   = "";
    var $_email           = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('newsletter', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_newsletter', $this->_pk_newsletter);
        $rec = $this->db->get('newsletter');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_newsletter', $this->_pk_newsletter);
        $this->db->update('newsletter', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_newsletter', $this->_pk_newsletter);
        $q = $this->db->get('newsletter');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('newsletter', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_newsletter', $this->_pk_newsletter);
            $this->db->update('newsletter', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $this->db->where('pk_newsletter', $this->_pk_newsletter);
        $this->db->delete('newsletter');
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('newsletter');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
