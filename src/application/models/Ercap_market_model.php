<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ercap_market_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ercap_market
  month
  year
  reporting_date
  payment_date
  value

 */
class Ercap_market_model extends CI_Model {

    var $_pk_ercap_market   = "";
    var $_month           = "";
    var $_year            = "";
    var $_reporting_date  = "";
    var $_payment_date    = "";
    var $_value           = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('ercap_market', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_ercap_market !== "") {
            $this->db->where('pk_ercap_market', $this->_pk_ercap_market);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('ercap_market');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_ercap_market', $this->_pk_ercap_market);
        $this->db->update('ercap_market', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_ercap_market', $this->_pk_ercap_market);
        $q = $this->db->get('ercap_market');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ercap_market', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ercap_market', $this->_pk_ercap_market);
            $this->db->update('ercap_market', $data);
            return $this->_pk_ercap_market;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ercap_market', $this->_pk_ercap_market);
        $this->db->update('ercap_market', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        $rec = $this->db->get('ercap_market');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
