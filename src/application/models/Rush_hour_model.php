<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of rush_hour_model
 *
 * @author Rogerio Pellarin
 * 
  pk_rush_hour
  fk_agent_power_distributor
  start_hour
  end_hour
  current

 */
class Rush_hour_model extends CI_Model {

    var $_pk_rush_hour               = "";
    var $_fk_agent_power_distributor = "";
    var $_start_hour                 = "";
    var $_end_hour                   = "";
    var $_current                    = "";
    var $_status                     = "";
    var $_created_at                 = "";
    var $_updated_at                 = "";
    var $_status_active              = 1;
    var $_status_inactive            = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('rush_hour', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('rush_hour.status', $this->_status);
        }
        if ($this->_pk_rush_hour !== "") {
            $this->db->where('pk_rush_hour', $this->_pk_rush_hour);
        }
        if ($this->_fk_agent_power_distributor !== "") {
            $this->db->where('rush_hour.fk_agent_power_distributor', $this->_fk_agent_power_distributor);
        }
        $rec = $this->db->get('rush_hour');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_rush_hour', $this->_pk_rush_hour);
        $this->db->update('rush_hour', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_rush_hour', $this->_pk_rush_hour);
        $q    = $this->db->get('rush_hour');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('rush_hour', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_rush_hour', $this->_pk_rush_hour);
            $this->db->update('rush_hour', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_rush_hour', $this->_pk_rush_hour);
        $this->db->update('rush_hour', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'rush_hour.*',
                    'agent.community_name'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('rush_hour.status', $this->_status);
        }
        if ($this->_fk_agent_power_distributor !== "") {
            $this->db->where('rush_hour.fk_agent_power_distributor', $this->_fk_agent_power_distributor);
        }
        $this->db->join('agent', 'agent.pk_agent = rush_hour.fk_agent_power_distributor');
        $rec = $this->db->get('rush_hour');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
