<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of gauge_record_model
 *
 * @author Rogerio Pellarin
 * 
  pk_gauge_record
  fk_gauge
  record_date
  type_energy
  active_power_generation
  active_power_consumption
  reactive_power_generation
  reactive_power_consumption
  missing_intervals
  measurement_situation
  reason_situation
  order

 */
class Gauge_record_model extends CI_Model {

    var $_pk_gauge_record            = "";
    var $_fk_gauge                   = "";
    var $_record_date                = "";
    var $_type_energy                = "";
    var $_active_power_generation    = "";
    var $_active_power_consumption   = "";
    var $_reactive_power_generation  = "";
    var $_reactive_power_consumption = "";
    var $_missing_intervals          = "";
    var $_measurement_situation      = "";
    var $_reason_situation           = "";
    var $_order                      = "";
    var $_status                     = "";
    var $_created_at                 = "";
    var $_updated_at                 = "";
    var $_status_active              = 1;
    var $_status_inactive            = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('gauge_record', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_gauge_record', $this->_pk_gauge_record);
        $rec = $this->db->get('gauge_record');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_gauge_record', $this->_pk_gauge_record);
        $this->db->update('gauge_record', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_gauge_record', $this->_pk_gauge_record);
        $q = $this->db->get('gauge_record');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('gauge_record', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_gauge_record', $this->_pk_gauge_record);
            $this->db->update('gauge_record', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_gauge_record', $this->_pk_gauge_record);
        $this->db->update('gauge_record', $data);
        return $this->db->affected_rows();
    }

    function delete_by_record_date() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('record_date', $this->_record_date);
        $this->db->where('status', $this->_status_active);
        $this->db->update('gauge_record', $data);
        return $this->db->affected_rows();
    }
    
    function hard_delete() {
        $this->db->where('pk_gauge_record', $this->_pk_gauge_record);
        $this->db->delete('gauge_record');
        return $this->db->affected_rows();
    }

    function hard_delete_by_record_date() {
        if ($this->_fk_gauge) {
            $this->db->where('fk_gauge', $this->_fk_gauge);
        }
        if ($this->_order) {
            $this->db->where('order', $this->_order);
        }        
        $this->db->where('record_date', $this->_record_date);
        $this->db->where('status', $this->_status_active);
        $this->db->delete('gauge_record');
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->order_by('record_date', 'DESC');
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('gauge_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_report() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where_in('fk_gauge', $this->_fk_gauge);
        $this->db->where('record_date >=', date("Y-m-d 00:00:00", strtotime($this->_record_date)));
        $this->db->where('record_date <=', date("Y-m-t 23:59:59", strtotime($this->_record_date)));
        $this->db->order_by('record_date', 'DESC');
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('gauge_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_for_dashboard() {
        $this->db->select(array(
            'fk_gauge',
            'record_date',
            'DAY(record_date) AS day',
            'active_power_consumption AS value',
            'IF (reason_situation = \'Consistido\' , 0, 1) AS error',
            'order'
        ));
        
        
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where_in('fk_gauge', $this->_fk_gauge);
        $this->db->where('record_date >=', $this->_record_date);
        $this->db->where('record_date <=', date("Y-m-t 23:59:59", strtotime($this->_record_date)));
        $this->db->order_by('record_date', 'DESC');
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('gauge_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
