<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of free_captive_model
 *
 * @author Rogerio Pellarin
 *
  pk_free_captive
  month
  year
  fk_agent
  fk_unity
  economy
  captive_total
  free_total
  fk_agent_power_distributor
  report
  email
  status
  created_at
  updated_at

 */
class Free_captive_model extends CI_Model {

    var $_pk_free_captive            = "";
    var $_month                      = "";
    var $_year                       = "";
    var $_fk_agent                   = "";
    var $_fk_unity                   = "";
    var $_economy                    = "";
    var $_captive_total              = "";
    var $_free_total                 = "";
    var $_fk_agent_power_distributor = "";
    var $_report                     = "";
    var $_email                      = "";
    var $_status                     = "";
    var $_created_at                 = "";
    var $_updated_at                 = "";
    var $_status_active              = 1;
    var $_status_inactive            = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('free_captive', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_free_captive', $this->_pk_free_captive);
        $rec = $this->db->get('free_captive');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_free_captive', $this->_pk_free_captive);
        $this->db->update('free_captive', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        try {
            $data = get_array_from_object($this);
            unset($data['report']);

            $this->db->where('pk_free_captive', $this->_pk_free_captive);
            $q = $this->db->get('free_captive');
            
            if ($q->result_id->num_rows == 0) {
                $this->db->insert('free_captive', $data);
                $ret = $this->db->insert_id();
                return $ret;
            }
            else {
                unset($data['created_at']);
                $this->db->where('pk_free_captive', $this->_pk_free_captive);
                $this->db->update('free_captive', $data);
                return $this->_pk_free_captive;
            }
        } catch (Exception $e) {
            throw new Exception($e->getTraceAsString());
        }

    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_free_captive', $this->_pk_free_captive);
        $this->db->update('free_captive', $data);
        return $this->db->affected_rows();
    }
    
    function delete_file() {
        $data = array(
            'report' => null
        );
        $this->db->where('pk_free_captive', $this->_pk_free_captive);
        $this->db->update('free_captive', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'free_captive.*',
            'agent.community_name AS agent_power_distributor',
            'unity.community_name AS unity_name'
        ));
        if ($this->_status !== "") {
            $this->db->where('free_captive.status', $this->_status);
        }
        if ($this->_month !== "") {
            $this->db->where('free_captive.month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('free_captive.year', $this->_year);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('free_captive.fk_agent', $this->_fk_agent);
        }
        if ($this->_email !== "") {
            $this->db->where('free_captive.email', $this->_email);
        }
        $this->db->join('agent', 'free_captive.fk_agent_power_distributor = agent.pk_agent');
        $this->db->join('unity', 'free_captive.fk_unity = unity.pk_unity');
        $rec = $this->db->get('free_captive');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
