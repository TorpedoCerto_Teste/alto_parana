<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of devec_customer_model
 *
 * @author Rogerio Pellarin
 * 


 */
class devec_customer_model extends CI_Model {

    var $_pk_devec_customer = "";
    var $_fk_agent          = "";
    var $_fk_unity          = "";
    var $_year              = "";
    var $_month             = "";
    var $_devec_file        = "";
    var $_status            = "";
    var $_created_at        = "";
    var $_updated_at        = "";
    var $_status_active     = 1;
    var $_status_inactive   = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('devec_customer', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_devec_customer', $this->_pk_devec_customer);
        $rec = $this->db->get('devec_customer');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function read_by_parameters() {
        $this->db->where('fk_agent', $this->_fk_agent);
        $this->db->where('fk_unity', $this->_fk_unity);
        $this->db->where('month', $this->_month);
        $this->db->where('year', $this->_year);
        $rec = $this->db->get('devec_customer');
        
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_devec_customer', $this->_pk_devec_customer);
        $this->db->update('devec_customer', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_devec_customer', $this->_pk_devec_customer);
        $q = $this->db->get('devec_customer');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('devec_customer', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_devec_customer', $this->_pk_devec_customer);
            $this->db->update('devec_customer', $data);
            return $this->_pk_devec_customer;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_devec_customer', $this->_pk_devec_customer);
        $this->db->update('devec_customer', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'devec_customer.*',
            'agent.community_name AS agent_community_name',
            'unity.community_name AS unity_community_name'
        ));
        if ($this->_status !== "") {
            $this->db->where('devec_customer.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('devec_customer.fk_agent', $this->_fk_agent);
        }
        $this->db->join("agent", "agent.pk_agent = devec_customer.fk_agent");
        $this->db->join("unity", "unity.pk_unity = devec_customer.fk_unity");
        $rec = $this->db->get('devec_customer');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_unity() {
        $this->db->select(array(
            'devec_customer.*',
            'unity.community_name AS unity_community_name'
        ));
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_devec_customer !== "") {
            $this->db->where('pk_devec_customer', $this->_pk_devec_customer);
        }
        $this->db->join("unity", "unity.pk_unity = devec_customer.fk_unity");
        $rec = $this->db->get('devec_customer');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
