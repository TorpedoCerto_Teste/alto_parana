<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of gauge_model
 *
 * @author Rogerio Pellarin
 * 
  pk_gauge
  fk_unity
  gauge
  internal_name
  status
  created_at
  updated_at
  way2_point
 */
class Gauge_model extends CI_Model {

    var $_pk_gauge        = "";
    var $_fk_unity        = "";
    var $_gauge           = "";
    var $_internal_name   = "";
    var $_telemetry_code  = "";
    var $_summer_time     = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 2;
    var $_status_deleted  = 0;
    var $_fk_agent        = "";

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        unset($data['fk_agent']);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('gauge', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_gauge !== "") {
            $this->db->where('pk_gauge', $this->_pk_gauge);
        }
        if ($this->_gauge !== "") {
            $this->db->where('gauge', $this->_gauge);
        }
        $this->db->where('status', $this->_status_active);
        $rec = $this->db->get('gauge');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data = get_array_from_object($this);
        unset($data['fk_agent']);
        $data['telemetry_code'] = (int)$data['telemetry_code'];
        $this->db->where('pk_gauge', $this->_pk_gauge);
        $this->db->update('gauge', $data);
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_gauge', $this->_pk_gauge);
        $this->db->update('gauge', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'gauge.*',
            'unity.community_name'
        ));
        if ($this->_status !== "") {
            $this->db->where('gauge.status', $this->_status);
        }
        if ($this->_fk_unity !== "") {
            $this->db->where_in('gauge.fk_unity', $this->_fk_unity);
        }
        $this->db->where('gauge.status !=', $this->_status_deleted);
        $this->db->join('unity', 'unity.pk_unity = gauge.fk_unity');
        $this->db->where('unity.status =', $this->_status_active);
        $rec = $this->db->get('gauge');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_telemetry_code() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_gauge !== "") {
            $this->db->where('pk_gauge', $this->_pk_gauge);
        }
        $this->db->where('status !=', $this->_status_deleted);
        $this->db->where('telemetry_code IS NOT NULL', NULL, FALSE);
        $rec = $this->db->get('gauge');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->db->select(array(
            'gauge.*'
        ));
        $this->db->join('unity', 'unity.pk_unity = gauge.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->where('gauge.status', $this->_status_active);
        $this->db->where('agent.pk_agent', $this->_fk_agent);
        $rec = $this->db->get('gauge');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    function delete_by_unity() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('fk_unity', $this->_fk_unity);
        $this->db->where('status', $this->_status_active);
        $this->db->update('gauge', $data);
        return $this->db->affected_rows();
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
