<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of return_investment_migration_model
 *
 * @author Rogerio Pellarin
 *
  pk_return_investment_migration
  month
  year
  fk_agent
  return_investment_migration_file
  status
  created_at
  updated_at

 */
class Return_investment_migration_model extends CI_Model {

    var $_pk_return_investment_migration   = "";
    var $_month                            = "";
    var $_year                             = "";
    var $_fk_agent                         = "";
    var $_return_investment_migration_file = "";
    var $_status                           = "";
    var $_created_at                       = "";
    var $_updated_at                       = "";
    var $_status_active                    = 1;
    var $_status_inactive                  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('return_investment_migration', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_return_investment_migration', $this->_pk_return_investment_migration);
        $rec = $this->db->get('return_investment_migration');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_return_investment_migration', $this->_pk_return_investment_migration);
        $this->db->update('return_investment_migration', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_return_investment_migration', $this->_pk_return_investment_migration);
        $q = $this->db->get('return_investment_migration');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('return_investment_migration', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_return_investment_migration', $this->_pk_return_investment_migration);
            $this->db->update('return_investment_migration', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_return_investment_migration', $this->_pk_return_investment_migration);
        $this->db->update('return_investment_migration', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('return_investment_migration');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
