<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of unity_model
 *
 * @author Rogerio Pellarin
 * 
  pk_unity
  fk_agent
  fk_agent_power_distributor
  company_name
  community_name
  cnpj
  ie
  uc_number
  ape_status
  siga
  postal_code
  address
  number
  complement
  district
  city
  state
  status
  created_at
  updated_at
  memo
  icms
  memo_icms
  activity
  fk_profile
  communion_acronym
  credit_icms
 */
class Unity_model extends CI_Model {

    var $_pk_unity                   = "";
    var $_fk_agent                   = "";
    var $_fk_profile                 = "";
    var $_fk_agent_power_distributor = "";
    var $_company_name               = "";
    var $_community_name             = "";
    var $_cnpj                       = "";
    var $_ie                         = "";
    var $_uc_number                  = "";
    var $_ape_status                 = "";
    var $_siga                       = "";
    var $_postal_code                = "";
    var $_address                    = "";
    var $_number                     = "";
    var $_complement                 = "";
    var $_district                   = "";
    var $_city                       = "";
    var $_state                      = "";
    var $_activity                   = "";
    var $_memo                       = "";
    var $_icms                       = "";
    var $_memo_icms                  = "";
    var $_communion_acronym          = "";
    var $_credit_icms                = "";
    var $_free_market_entry          = "";
    var $_status                     = "";
    var $_created_at                 = "";
    var $_updated_at                 = "";
    var $_status_active              = 1;
    var $_status_inactive            = 2;
    var $_status_deleted             = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('unity', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_unity !== "") {
            $this->db->where('pk_unity', $this->_pk_unity);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('unity');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        try {
            $this->db->where('pk_unity', $this->_pk_unity);
            $this->db->update('unity', get_array_from_object($this));
            return $this->db->affected_rows();
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_unity', $this->_pk_unity);
        $this->db->update('unity', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'a.community_name AS agent_company_name',
                    'p.community_name AS power_distributor_name',
                    'agent_type.type AS agent_type',
                    'profile.profile AS profile',
                    'profile.type AS profile_type',
                    'submarket.initials AS submarket',
                    'unity.*'
                )
        );
        $this->db->join('agent a', 'a.pk_agent = unity.fk_agent AND a.status = 1', 'LEFT');
        $this->db->join('agent p', 'p.pk_agent = unity.fk_agent_power_distributor', 'LEFT');
        $this->db->join('agent_type', 'agent_type.pk_agent_type = a.fk_agent_type', 'LEFT');
        $this->db->join('profile', 'profile.pk_profile = unity.fk_profile', 'LEFT');
        $this->db->join('submarket', 'submarket.pk_submarket = profile.fk_submarket', 'LEFT');
        if ($this->_fk_agent !== "") {
            $this->db->where('unity.fk_agent', $this->_fk_agent);
        }
        if ($this->_status !== "") {
            $this->db->where('unity.status', $this->_status);
        } else {
            $this->db->where('unity.status', $this->_status_active);
        }
        if ($this->_pk_unity !== "") {
            $this->db->where('unity.pk_unity', $this->_pk_unity);
        }
        $this->db->where('a.pk_agent IS NOT NULL');
        $rec = $this->db->get('unity');

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_unities_from_ccvee() {
        $this->db->select([
            'ccvee_general.pk_ccvee_general'
        ]);
        $this->db->from('ccvee_general');
        $this->db->join('unity', 'unity.pk_unity = ccvee_general.fk_unity AND unity.fk_agent = '.$this->_fk_agent);
        $this->db->where('ccvee_general.status', $this->_status_active);
        $sub_query = $this->db->get_compiled_select();

        $this->db->select(
            array(
                'a.community_name AS agent_company_name',
                'p.community_name AS power_distributor_name',
                'agent_type.type AS agent_type',
                'unity.*'
            )
        );
        $this->db->join('agent a', 'a.pk_agent = unity.fk_agent', 'LEFT');
        $this->db->join('agent p', 'p.pk_agent = unity.fk_agent_power_distributor', 'LEFT');
        $this->db->join('agent_type', 'agent_type.pk_agent_type = a.fk_agent_type', 'LEFT');
        $this->db->join('ccvee_unity', 'ccvee_unity.fk_unity = unity.pk_unity AND ccvee_unity.fk_ccvee_general IN ('.$sub_query.')', 'INNER');
        $this->db->where('unity.status', $this->_status_active);
        $this->db->group_by('unity.pk_unity');
        $rec = $this->db->get('unity');

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
