<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of demand_model
 *
 * @author Rogerio Pellarin
 * 
  pk_demand
  fk_unity
  start_date
  end_date
  tension
  tariff_mode
  end_demand
  demand_tip_out
  captive_situation
  current
  edge_generators
  price_kwh
  not_consumed_at_the_tip
  status
  created_at
  updated_at

 */
class Demand_model extends CI_Model {

    var $_pk_demand               = "";
    var $_fk_unity                = "";
    var $_start_date              = "";
    var $_end_date                = "";
    var $_tension                 = "";
    var $_tariff_mode             = "";
    var $_end_demand              = "";
    var $_demand_tip_out          = "";
    var $_captive_situation       = "";
    var $_current                 = "";
    var $_edge_generators         = "";
    var $_price_kwh               = "";
    var $_not_consumed_at_the_tip = "";
    var $_status                  = "";
    var $_created_at              = "";
    var $_updated_at              = "";
    var $_status_active           = 1;
    var $_status_inactive         = 2;
    var $_status_deleted          = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('demand', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_demand !== "") {
            $this->db->where('pk_demand', $this->_pk_demand);
        }
        if ($this->_start_date != "") {
            $this->db->where("start_date <= '" . $this->_start_date . "' AND (end_date >= '" . $this->_start_date . "' OR (end_date IS NULL AND current = 1))", NULL, FALSE);
        }
        $this->db->where('fk_unity', $this->_fk_unity);
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(1);
        $rec = $this->db->get('demand');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_demand', $this->_pk_demand);
        $this->db->update('demand', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_demand', $this->_pk_demand);
        $this->db->update('demand', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where('status !=', $this->_status_deleted);
        $this->db->where('fk_unity', $this->_fk_unity);
        $rec = $this->db->get('demand');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

    function change_old_current() {
        $sql = "
            UPDATE demand 
            SET 
                current = 0,
                end_date = (SELECT 
                        x.*
                    FROM
                        (SELECT 
                            DATE_SUB(start_date, INTERVAL 1 DAY)
                        FROM
                            demand
                        WHERE
                            current = 1
                        ORDER BY start_date DESC
                        LIMIT 1) AS x)
            WHERE
                current = 1 ORDER BY start_date ASC LIMIT 1;            
                ";
        $this->db->query($sql);
    }

    function read_by_unity_and_date() {
        $this->db->where('status', $this->_status);
        $this->db->where('fk_unity', $this->_fk_unity);
        $this->db->where('start_date <=', $this->_start_date);
        $this->db->where("(end_date >= '" . $this->_end_date . "' OR end_date IS NULL)", NULL, FALSE);
        $rec = $this->db->get('demand');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

}
