<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of business_unity_model
 *
 * @author Rogerio Pellarin
 *
  pk_business_unity
  fk_business
  fk_unity
  start_date
  end_date
  status
  created_at
  updated_at

 */
class Business_unity_model extends CI_Model {

    var $_pk_business_unity = "";
    var $_fk_business       = "";
    var $_fk_unity          = "";
    var $_start_date        = "";
    var $_end_date          = "";
    var $_status            = "";
    var $_created_at        = "";
    var $_updated_at        = "";
    var $_status_active     = 1;
    var $_status_inactive   = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('business_unity', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_business_unity', $this->_pk_business_unity);
        $rec = $this->db->get('business_unity');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_business_unity', $this->_pk_business_unity);
        $this->db->update('business_unity', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_business_unity', $this->_pk_business_unity);
        $q                  = $this->db->get('business_unity');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('business_unity', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_business_unity', $this->_pk_business_unity);
            $this->db->update('business_unity', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_business_unity', $this->_pk_business_unity);
        $this->db->update('business_unity', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'business_unity.*',
            'unity.community_name'
        ));
        if ($this->_status !== "") {
            $this->db->where('business_unity.status', $this->_status);
        }
        if ($this->_fk_business !== "") {
            $this->db->where('business_unity.fk_business', $this->_fk_business);
        }
        $this->db->join('unity', 'unity.pk_unity = business_unity.fk_unity');
        $rec = $this->db->get('business_unity');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
