<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of service_product_model
 *
 * @author Rogerio Pellarin
 * 
  fk_service
  fk_product

 */
class Service_product_model extends CI_Model {

    var $_pk_service_product = "";
    var $_fk_service         = "";
    var $_fk_product         = "";
    var $_status             = "";
    var $_created_at         = "";
    var $_updated_at         = "";
    var $_status_active      = 1;
    var $_status_inactive    = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('service_product', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_fk_service !== "") {
            $this->db->where('fk_service', $this->_fk_service);
        }
        if ($this->_fk_product !== "") {
            $this->db->where('fk_product', $this->_fk_product);
        }
        if ($this->_pk_service_product !== "") {
            $this->db->where('pk_service_product', $this->_pk_service_product);
        }
        $rec = $this->db->get('service_product');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_service_product', $this->_pk_service_product);
        $this->db->update('service_product', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_service_product', $this->_pk_service_product);
        $q    = $this->db->get('service_product');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('service_product', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_service_product', $this->_pk_service_product);
            $this->db->update('service_product', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_service !== "") {
            $this->db->where('fk_service', $this->_fk_service);
        }
        if ($this->_pk_service_product !== "") {
            $this->db->where('pk_service_product', $this->_pk_service_product);
        }
        $this->db->update('service_product', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_service !== "") {
            $this->db->where('fk_service', $this->_fk_service);
        }
        $rec = $this->db->get('service_product');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
