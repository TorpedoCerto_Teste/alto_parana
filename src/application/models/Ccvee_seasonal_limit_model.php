<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_seasonal_limit_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_seasonal_limit
  fk_ccvee_energy
  year
  limit


 */
class Ccvee_seasonal_limit_model extends CI_Model {

    var $_pk_ccvee_seasonal_limit = "";
    var $_fk_ccvee_energy         = "";
    var $_year                    = "";
    var $_limit                   = "";
    var $_status                  = "";
    var $_created_at              = "";
    var $_updated_at              = "";
    var $_status_active           = 1;
    var $_status_inactive         = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_seasonal_limit', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee_seasonal_limit', $this->_pk_ccvee_seasonal_limit);
        $rec = $this->db->get('ccvee_seasonal_limit');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_seasonal_limit', $this->_pk_ccvee_seasonal_limit);
        $this->db->update('ccvee_seasonal_limit', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_seasonal_limit', $this->_pk_ccvee_seasonal_limit);
        $q                  = $this->db->get('ccvee_seasonal_limit');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_seasonal_limit', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_seasonal_limit', $this->_pk_ccvee_seasonal_limit);
            $this->db->update('ccvee_seasonal_limit', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('fk_ccvee_energy', $this->_fk_ccvee_energy);
        $this->db->update('ccvee_seasonal_limit', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_ccvee_seasonal_limit !== "") {
            $this->db->where('pk_ccvee_seasonal_limit', $this->_pk_ccvee_seasonal_limit);
        }
        if ($this->_fk_ccvee_energy !== "") {
            $this->db->where('fk_ccvee_energy', $this->_fk_ccvee_energy);
        }
        $rec = $this->db->get('ccvee_seasonal_limit');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
