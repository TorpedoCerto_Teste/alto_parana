<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Telemetry_record_model extends CI_Model {

    var $_pk_telemetry_record = "";
    var $_fk_gauge            = "";
    var $_greatness           = "";
    var $_record_date         = "";
    var $_value               = "";
    var $_quality             = "";
    var $_status              = "";
    var $_order               = "";
    var $_created_at          = "";
    var $_updated_at          = "";
    var $_status_active       = 1;
    var $_status_inactive     = 0;
    var $_fetch_period        = "month";

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        unset($data['fetch_period']);
        $this->db->insert('telemetry_record', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_telemetry_record', $this->_pk_telemetry_record);
        $rec = $this->db->get('telemetry_record');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data = get_array_from_object($this);
        unset($data['fetch_period']);
        $this->db->where('pk_telemetry_record', $this->_pk_telemetry_record);
        $this->db->update('telemetry_record', $data);
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        unset($data['fetch_period']);
        $this->db->where('pk_telemetry_record', $this->_pk__telemetry_record);
        $q    = $this->db->get('telemetry_record');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('telemetry_record', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_telemetry_record', $this->_pk_telemetry_record);
            $this->db->update('telemetry_record', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_pk_telemetry_record !== "") {
            $this->db->where('pk_telemetry_record', $this->_pk_telemetry_record);
        }
        if ($this->_fk_gauge !== "") {
            $this->db->where('fk_gauge', $this->_fk_gauge);
        }
        if ($this->_greatness !== "") {
            $this->db->where('greatness', $this->_greatness);
        }
        if ($this->_record_date !== "") {
            $this->db->where('record_date', $this->_record_date);
        }
        $this->db->update('telemetry_record', $data);
        return $this->db->affected_rows();
    }

    function hard_delete() {
        if ($this->_pk_telemetry_record !== "") {
            $this->db->where('pk_telemetry_record', $this->_pk_telemetry_record);
        }
        if ($this->_fk_gauge !== "") {
            $this->db->where('fk_gauge', $this->_fk_gauge);
        }
        if ($this->_greatness !== "") {
            $this->db->where('greatness', $this->_greatness);
        }
        if ($this->_record_date !== "") {
            $this->db->where('record_date', $this->_record_date);
        }
        $this->db->delete('telemetry_record');
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_gauge !== "") {
            $this->db->where_in('fk_gauge', $this->_fk_gauge);
        }
        if ($this->_greatness !== "") {
            $this->db->where('greatness', $this->_greatness);
        }
        if ($this->_record_date !== "") {
            if ($this->_fetch_period == "month") {
                $this->db->where('record_date >= ', date("Y-m-01 00:00:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-t 23:59:59", strtotime($this->_record_date)));
            }
            else {
                $this->db->where('record_date >= ', date("Y-m-d 00:00:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-d 23:59:59", strtotime($this->_record_date)));
            }
        }
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('telemetry_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_demat() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_gauge !== "") {
            $this->db->where_in('fk_gauge', $this->_fk_gauge);
        }
        $this->db->where('greatness', "Demat");
        if ($this->_record_date !== "") {
            if ($this->_fetch_period == "month") {
                $this->db->where('record_date >= ', date("Y-m-01 00:15:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-01 00:00:00", strtotime("+1 MONTH", strtotime($this->_record_date))));
            }
            else {
                $this->db->where('record_date >= ', date("Y-m-d 00:15:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-d 00:00:00", strtotime("+1 DAY", strtotime($this->_record_date))));
            }
        }
        $this->db->order_by('record_date', 'DESC');
        $rec = $this->db->get('telemetry_record');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_eneat() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_gauge !== "") {
            $this->db->where_in('fk_gauge', $this->_fk_gauge);
        }
        $this->db->where('greatness', "Eneat");
        if ($this->_record_date !== "") {
            if ($this->_fetch_period == "month") {
//                $this->db->where('record_date >= ', date("Y-m-01 01:00:00", strtotime($this->_record_date)));
//                $this->db->where('record_date <= ', date("Y-m-01 00:00:00", strtotime("+1 MONTH", strtotime($this->_record_date))));
                //MUDADO EM 25/01/2020
                $this->db->where('record_date >= ', date("Y-m-01 00:00:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-t 23:00:00", strtotime($this->_record_date)));
            }
            else {
//                $this->db->where('record_date >= ', date("Y-m-d 01:00:00", strtotime($this->_record_date)));
//                $this->db->where('record_date <= ', date("Y-m-d 00:00:00", strtotime("+1 DAY", strtotime($this->_record_date))));
                //MUDADO EM 25/01/2020
                $this->db->where('record_date >= ', date("Y-m-d 00:00:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-d 23:00:00", strtotime($this->_record_date)));
            }
        }
        $this->db->order_by('record_date', 'DESC');
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('telemetry_record');
        
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    function fetch_for_dashboard() {
        $this->db->select(array(
            'fk_gauge',
            'record_date',
            'DAY(record_date) AS day',
            'value',
            'quality AS error',
            'order'
        ));

        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_gauge !== "") {
            $this->db->where_in('fk_gauge', $this->_fk_gauge);
        }
        $this->db->where('greatness', "Eneat");
        if ($this->_record_date !== "") {
            if ($this->_fetch_period == "month") {
                $this->db->where('record_date >= ', date("Y-m-01 00:00:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-t 23:00:00", strtotime($this->_record_date)));
            }
            else {
                $this->db->where('record_date >= ', date("Y-m-d 00:00:00", strtotime($this->_record_date)));
                $this->db->where('record_date <= ', date("Y-m-d 23:00:00", strtotime($this->_record_date)));
            }
        }
        $this->db->order_by('record_date', 'DESC');
        $this->db->order_by('order', 'ASC');
        $rec = $this->db->get('telemetry_record');
        
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
