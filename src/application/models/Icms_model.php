<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of icms_model_model
 *
 * @author Rogerio Pellarin
 * 
  pk_icms
  state
  start_date
  end_date
  percent_industry
  percent_commercial
  status
  created_at
  updated_at

 */
class Icms_model extends CI_Model {

    var $_pk_icms            = "";
    var $_state              = "";
    var $_start_date         = "";
    var $_end_date           = "";
    var $_percent_industry   = "";
    var $_percent_commercial = "";
    var $_status             = "";
    var $_created_at         = "";
    var $_updated_at         = "";
    var $_status_active      = 1;
    var $_status_inactive    = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('icms', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_icms', $this->_pk_icms);
        $rec = $this->db->get('icms');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_icms', $this->_pk_icms);
        $this->db->update('icms', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_icms', $this->_pk_icms);
        $this->db->update('icms', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where('status != ', $this->_status_inactive);
        $rec = $this->db->get('icms');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
