<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of User_model
 *
 * @author rogeriopellarin
 * 
  pk_user
  name
  surname
  email
  password
  access_level
  status
  updated_at
  created_at
  verification
  token
 */
class User_model extends CI_Model {

    var $_pk_user            = "";
    var $_name               = "";
    var $_surname            = "";
    var $_email              = "";
    var $_password           = "";
    var $_access_level       = "";
    var $_status             = "";
    var $_updated_at         = "";
    var $_created_at         = "";
    var $_verification       = "";
    var $_token              = "";
    var $_status_active      = 1;
    var $_status_inactive    = 0;
    var $_status_app         = 2; //validação por APP
    var $_access_level_admin = 1;
    var $_access_level_user  = 2;

    function __construct() {
        parent::__construct();
    }

    function find_by_email_and_password() {
        $rec = $this->db->get_where('user', array('email' => $this->_email, 'password' => $this->_password, 'status' => $this->_status_active), 1);
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('user', $data);
        $ret  = $this->db->insert_id();
        return $ret;
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('user.status', $this->_status);
        }
        $rec = $this->db->get('user');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
