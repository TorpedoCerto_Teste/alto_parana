<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of energy_market_model
 *
 * @author Rogerio Pellarin
 * 


 */
class Energy_market_model extends CI_Model {

    var $_pk_energy_market             = "";
    var $_year                         = "";
    var $_month                        = "";
    var $_week                         = "";
    var $_energy_market_file_indicator = "";
    var $_energy_market_file_price     = "";
    var $_schedule                     = "";
    var $_status                       = "";
    var $_created_at                   = "";
    var $_updated_at                   = "";
    var $_status_active                = 1;
    var $_status_inactive              = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('energy_market', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_energy_market', $this->_pk_energy_market);
        $rec = $this->db->get('energy_market');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_energy_market', $this->_pk_energy_market);
        $this->db->update('energy_market', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_energy_market', $this->_pk_energy_market);
        $q = $this->db->get('energy_market');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('energy_market', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_energy_market', $this->_pk_energy_market);
            $this->db->update('energy_market', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_energy_market', $this->_pk_energy_market);
        $this->db->update('energy_market', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('energy_market');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_schedule() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where('schedule <= ', date("Y-m-d H:i:s"));
        $rec = $this->db->get('energy_market');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
