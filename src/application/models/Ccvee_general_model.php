<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_general_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_general
  fk_unity
  fk_user
  fk_profile
  fk_submarket
  document_number
  contract_type
  cplp
  source
  rfq
  memo
  fk_energy_type
  retusd
  fk_unity_counterpart
  fk_profile_counterpart
  unity_apportionment
  unity_differentiated_validate
  ccvee_general_file
  status
  created_at
  updated_at
 */
class Ccvee_general_model extends CI_Model
{

    var $_pk_ccvee_general              = "";
    var $_fk_unity                      = "";
    var $_fk_user                       = "";
    var $_fk_submarket                  = "";
    var $_document_number               = "";
    var $_contract_type                 = "";
    var $_cplp                          = "";
    var $_source                        = "";
    var $_rfq                           = "";
    var $_memo                          = "";
    var $_fk_energy_type                = "";
    var $_retusd                        = "";
    var $_fk_unity_counterpart          = "";
    var $_unity_apportionment           = "CONSUMO";
    var $_unity_differentiated_validate = "";
    var $_ccvee_general_file            = "";
    var $_status                        = "";
    var $_created_at                    = "";
    var $_updated_at                    = "";
    var $_status_active                 = 1;
    var $_status_inactive               = 0;
    var $_fk_agent                      = "";
    var $_ape_status                    = "";

    function __construct()
    {
        parent::__construct();
    }

    function create()
    {
        $data               = get_array_from_object($this);
        unset($data['fk_agent']);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_general', $data);
        $this->db->cache_delete('api_vue', 'getCcveeList');
        return $this->db->insert_id();
    }

    function read()
    {
        $this->db->join('unity', 'unity.pk_unity = ccvee_general.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->where('pk_ccvee_general', $this->_pk_ccvee_general);
        if ($this->_status !== "") {
            $this->db->where('ccvee_general.status', $this->_status);
        }
        $rec = $this->db->get('ccvee_general');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function read_by_customer()
    {
        $this->db->select(
            array(
                "agent.community_name AS agent",
                "agent.cnpj AS agent_cnpj",
                "unity.community_name AS unity_name",
                "unity.cnpj AS unity_cnpj",
                "energy_type.name AS energy_type",
                "energy_type.source AS energy_source",
                "unity_counterpart.community_name AS counterpart",
                "submarket.submarket AS submarket",
                "submarket.initials AS submarket_initials",
                "ccvee_general.* "
            )
        );
        $this->db->join('unity', 'unity.pk_unity = ccvee_general.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->join('energy_type', 'energy_type.pk_energy_type = ccvee_general.fk_energy_type');
        $this->db->join('unity unity_counterpart', 'unity_counterpart.pk_unity = ccvee_general.fk_unity_counterpart');
        $this->db->join('submarket', 'submarket.pk_submarket = ccvee_general.fk_submarket', "LEFT");
        $this->db->where('ccvee_general.pk_ccvee_general', $this->_pk_ccvee_general);
        if ($this->_status !== "") {
            $this->db->where('ccvee_general.status', $this->_status);
        }
        $rec = $this->db->get('ccvee_general');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $return = $rec->result_array();
            return $return[0];
        }
        return FALSE;
    }

    function update()
    {
        $data               = get_array_from_object($this);

        $this->read();

        $new_data = [
            "pk_ccvee_general" => isset($data["pk_ccvee_general"]) ? $data["pk_ccvee_general"] : $this->_pk_ccvee_general,
            "fk_unity" => isset($data["fk_unity"]) ? $data["fk_unity"] : $this->_fk_unity,
            "fk_user" => isset($data["fk_user"]) ? $data["fk_user"] : $this->_fk_user,
            "fk_submarket" => isset($data["fk_submarket"]) ? $data["fk_submarket"] : $this->_fk_submarket,
            "document_number" => isset($data["document_number"]) ? $data["document_number"] : $this->_document_number,
            "contract_type" => isset($data["contract_type"]) ? $data["contract_type"] : $this->_contract_type,
            "cplp" => isset($data["cplp"]) ? $data["cplp"] : $this->_cplp,
            "source" => isset($data["source"]) ? $data["source"] : $this->_source,
            "rfq" => isset($data["rfq"]) ? $data["rfq"] : $this->_rfq,
            "memo" => isset($data["memo"]) ? $data["memo"] : $this->_memo,
            "fk_energy_type" => isset($data["fk_energy_type"]) ? $data["fk_energy_type"] : $this->_fk_energy_type,
            "retusd" => isset($data["retusd"]) ? $data["retusd"] : $this->_retusd,
            "fk_unity_counterpart" => isset($data["fk_unity_counterpart"]) ? $data["fk_unity_counterpart"] : $this->_fk_unity_counterpart,
            "unity_apportionment" => isset($data["unity_apportionment"]) ? $data["unity_apportionment"] : $this->_unity_apportionment,
            "unity_differentiated_validate" => isset($data["unity_differentiated_validate"]) ? $data["unity_differentiated_validate"] : $this->_unity_differentiated_validate,
            "ccvee_general_file" => isset($data["ccvee_general_file"]) ? $data["ccvee_general_file"] : $this->_ccvee_general_file,
            "status" => isset($data["status"]) ? $data["status"] : $this->_status
        ];

        $this->db->where('pk_ccvee_general', $this->_pk_ccvee_general);
        $this->db->update('ccvee_general', $new_data);
        return $this->db->affected_rows();
    }

    function delete()
    {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee_general', $this->_pk_ccvee_general);
        $this->db->update('ccvee_general', $data);
        return $this->db->affected_rows();
    }

    function fetch()
    {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('ccvee_general');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent_unity()
    {
        $this->db->select(
            array(
                "agent.community_name AS agent",
                "agent.cnpj AS agent_cnpj",
                "unity.community_name AS unity_name",
                "unity.cnpj AS unity_cnpj",
                "unity.ape_status AS unity_ape_status",
                "energy_type.name AS energy_type",
                "energy_type.source AS energy_source",
                "unity_counterpart.community_name AS counterpart",
                "submarket.submarket AS submarket",
                "submarket.initials AS submarket_initials",
                "ccvee_energy.supply_start AS energy_supply_start",
                "ccvee_energy.supply_end AS energy_supply_end",
                "ccvee_general.* "
            )
        );
        $this->db->join('unity', 'unity.pk_unity = ccvee_general.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->join('energy_type', 'energy_type.pk_energy_type = ccvee_general.fk_energy_type');
        $this->db->join('unity unity_counterpart', 'unity_counterpart.pk_unity = ccvee_general.fk_unity_counterpart');
        $this->db->join('submarket', 'submarket.pk_submarket = ccvee_general.fk_submarket', "LEFT");
        $this->db->join('ccvee_energy', 'ccvee_energy.fk_ccvee_general = ccvee_general.pk_ccvee_general', "LEFT");
        if ($this->_status !== "") {
            $this->db->where('ccvee_general.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('unity.fk_agent', $this->_fk_agent);
        }
        if (is_array($this->_ape_status) && !empty($this->_ape_status)) {
            $this->db->where_in('unity.ape_status', $this->_ape_status);
        } else if ($this->_ape_status !== "") {
            $this->db->where('unity.ape_status', $this->_ape_status);
        }
        $rec = $this->db->get('ccvee_general');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_general_energy_by_unity()
    {
        $this->db->select(array(
            "ccvee_general.*",
            "ccvee_energy.*",
            "COUNT(ccvee_general.pk_ccvee_general) AS unities",
            "submarket.submarket",
            "energy_type.name AS energy_type_name"
        ));
        $this->db->join('ccvee_unity', 'ccvee_general.pk_ccvee_general = ccvee_unity.fk_ccvee_general');
        $this->db->join('ccvee_energy', 'ccvee_energy.fk_ccvee_general = ccvee_general.pk_ccvee_general', "LEFT");
        $this->db->join('submarket', 'ccvee_general.fk_submarket = submarket.pk_submarket');
        $this->db->join('energy_type', 'ccvee_general.fk_energy_type = energy_type.pk_energy_type');

        $this->db->where_in('ccvee_unity.fk_unity', $this->_fk_unity);
        $this->db->where('cplp', 'LP');
        $this->db->where('ccvee_unity.status', '1');

        if ($this->_status !== "") {
            $this->db->where('ccvee_general.status', $this->_status);
            $this->db->where('ccvee_unity.status', $this->_status);
            $this->db->where('ccvee_energy.status', $this->_status);
        }
        $this->db->group_by('ccvee_general.pk_ccvee_general');
        $rec = $this->db->get('ccvee_general');

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret)
    {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }
}
