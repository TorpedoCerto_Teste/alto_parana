<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of lfpen001_model
 *
 * @author Rogerio Pellarin
 *
  pk_lfpen001
  month
  year
  fk_agent
  lfpen001_file
  total
  payment_date
  penalty_not_apply
  status
  created_at
  updated_at

 */
class Lfpen001_model extends CI_Model {

    var $_pk_lfpen001       = "";
    var $_month             = "";
    var $_year              = "";
    var $_fk_agent          = "";
    var $_lfpen001_file     = "";
    var $_total             = "";
    var $_payment_date      = "";
    var $_penalty_not_apply = "";
    var $_status            = "";
    var $_created_at        = "";
    var $_updated_at        = "";
    var $_status_active     = 1;
    var $_status_inactive   = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('lfpen001', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_lfpen001 !== "") {
            $this->db->where('pk_lfpen001', $this->_pk_lfpen001);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        $rec = $this->db->get('lfpen001');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_lfpen001', $this->_pk_lfpen001);
        $this->db->update('lfpen001', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_lfpen001', $this->_pk_lfpen001);
        $q = $this->db->get('lfpen001');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('lfpen001', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_lfpen001', $this->_pk_lfpen001);
            $this->db->update('lfpen001', $data);
            return $this->_pk_lfpen001;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_lfpen001', $this->_pk_lfpen001);
        $this->db->update('lfpen001', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $rec = $this->db->get('lfpen001');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }
}
