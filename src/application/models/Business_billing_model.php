<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of business_billing_model
 *
 * @author Rogerio Pellarin
 *
  pk_business_billing
  fk_business
  year
  month
  fk_unity
  expiration_date
  amount
  discout
  total
  payment_method
  payment_status
  payment_date
  payment_slip
  invoice
  xml
  status
  created_at
  updated_at

 */
class Business_billing_model extends CI_Model {

    var $_pk_business_billing = "";
    var $_fk_business         = "";
    var $_year                = "";
    var $_month               = "";
    var $_fk_unity            = "";
    var $_expiration_date     = "";
    var $_amount              = "";
    var $_discount            = "";
    var $_total               = "";
    var $_payment_method      = "";
    var $_payment_status      = "";
    var $_payment_date        = "";
    var $_payment_slip        = "";
    var $_invoice             = "";
    var $_xml                 = "";
    var $_status              = "";
    var $_created_at          = "";
    var $_updated_at          = "";
    var $_status_active       = 1;
    var $_status_inactive     = 0;
    var $_fk_agent            = "";

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        unset($data['fk_agent']);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('business_billing', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_business_billing', $this->_pk_business_billing);
        $rec = $this->db->get('business_billing');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data               = get_array_from_object($this);
        unset($data['fk_agent']);
        $this->db->where('pk_business_billing', $this->_pk_business_billing);
        $this->db->update('business_billing', $data);
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        unset($data['fk_agent']);
        $data['created_at'] = date("Y-m-d H:m:s");
        $q                  = $this->db->get('business_billing');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('business_billing', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_business_billing', $this->_pk_business_billing);
            $this->db->update('business_billing', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_business_billing', $this->_pk_business_billing);
        $this->db->update('business_billing', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'business_billing.*',
            'unity.community_name AS unity_name',
            'agent.pk_agent AS fk_agent'
        ));
        if ($this->_pk_business_billing !== "") {
            $this->db->where('business_billing.pk_business_billing', $this->_pk_business_billing);
        }
        if ($this->_status !== "") {
            $this->db->where('business_billing.status', $this->_status);
        }
        if ($this->_fk_business !== "") {
            $this->db->where('business_billing.fk_business', $this->_fk_business);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('agent.pk_agent', $this->_fk_agent);
        }
        $this->db->where('business.status', $this->_status_active);
        $this->db->join('business', 'business.pk_business = business_billing.fk_business');
        $this->db->join('unity', 'unity.pk_unity = business_billing.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->order_by("business_billing.created_at", "DESC");
        $rec = $this->db->get('business_billing');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
