<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of accounting_apportionment_model
 *
 * @author Rogerio Pellarin
 *
  pk_accounting_apportionment
  fk_unity
  fk_accounting
  status
  created_at
  updated_at

 */
class Accounting_apportionment_model extends CI_Model {

    var $_pk_accounting_apportionment = "";
    var $_fk_unity                    = "";
    var $_fk_accounting               = "";
    var $_status                      = "";
    var $_created_at                  = "";
    var $_updated_at                  = "";
    var $_status_active               = 1;
    var $_status_inactive             = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('accounting_apportionment', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_accounting_apportionment', $this->_pk_accounting_apportionment);
        $rec = $this->db->get('accounting_apportionment');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_accounting_apportionment', $this->_pk_accounting_apportionment);
        $this->db->update('accounting_apportionment', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_accounting_apportionment', $this->_pk_accounting_apportionment);
        $q = $this->db->get('accounting_apportionment');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('accounting_apportionment', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_accounting_apportionment', $this->_pk_accounting_apportionment);
            $this->db->update('accounting_apportionment', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_accounting !== "") {
            $this->db->where('fk_accounting', $this->_fk_accounting);
        }
        if ($this->_pk_accounting_apportionment !== "") {
            $this->db->where('pk_accounting_apportionment', $this->_pk_accounting_apportionment);
        }
        $this->db->update('accounting_apportionment', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_accounting !== "") {
            $this->db->where('fk_accounting', $this->_fk_accounting);
        }
        $rec = $this->db->get('accounting_apportionment');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
