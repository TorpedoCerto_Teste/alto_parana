<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of business_payment_model
 *
 * @author Rogerio Pellarin
 * 
  pk_business_payment
  fk_business
  remuneration_type
  percent_economy_value
  percent_economy_floor
  percent_economy_roof
  over_energy_value
  over_energy_floor
  over_energy_roof
  closed_value
  closed_installment
  closed_installment_quantity
  monthly_counterpart_unity_quantity_consumption
  monthly_counterpart_value
  due
  due_day
  due_period
  due_month
  financial_index
  index_readjustment_calculation
  database
  adjustment_montly_manually
  date_adjustment
  date_adjustment_frequency
  penalty
  interest
  interest_frequency

 */
class Business_payment_model extends CI_Model {

    var $_pk_business_payment                            = "";
    var $_fk_business                                    = "";
    var $_remuneration_type                              = "";
    var $_percent_economy_value                          = "";
    var $_percent_economy_floor                          = "";
    var $_percent_economy_roof                           = "";
    var $_over_energy_value                              = "";
    var $_over_energy_floor                              = "";
    var $_over_energy_roof                               = "";
    var $_closed_value                                   = "";
    var $_closed_installment                             = "";
    var $_closed_installment_quantity                    = "";
    var $_monthly_counterpart_unity_quantity_consumption = "";
    var $_monthly_counterpart_value                      = "";
    var $_due                                            = "";
    var $_due_day                                        = "";
    var $_due_period                                     = "";
    var $_due_month                                      = "";
    var $_financial_index                                = "";
    var $_index_readjustment_calculation                 = "";
    var $_database                                       = "";
    var $_adjustment_montly_manually                     = "";
    var $_date_adjustment                                = "";
    var $_date_adjustment_frequency                      = "";
    var $_penalty                                        = "";
    var $_interest                                       = "";
    var $_interest_frequency                             = "";
    var $_status                                         = "";
    var $_created_at                                     = "";
    var $_updated_at                                     = "";
    var $_status_active                                  = 1;
    var $_status_inactive                                = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('business_payment', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_business_payment !== "") {
            $this->db->where('pk_business_payment', $this->_pk_business_payment);
        }
        if ($this->_fk_business !== "") {
            $this->db->where('fk_business', $this->_fk_business);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('business_payment');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_business_payment', $this->_pk_business_payment);
        $this->db->update('business_payment', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_business_payment', $this->_pk_business_payment);
        $q                  = $this->db->get('business_payment');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('business_payment', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_business_payment', $this->_pk_business_payment);
            $this->db->update('business_payment', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_business_payment', $this->_pk_business_payment);
        $this->db->update('business_payment', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('business_payment');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
