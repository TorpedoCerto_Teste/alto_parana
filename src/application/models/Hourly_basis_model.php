<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of hourly_basis_model
 *
 * @author Rogerio Pellarin
 * 
  pk_hourly_basis
  week
  date
  landing
  hour_type
  pld_south
  pld_southeast
  pld_north
  pld_northeast
  week_ccee
  status
  created_at
  updated_at

 */
class Hourly_basis_model extends CI_Model {

    var $_pk_hourly_basis = "";
    var $_week            = "";
    var $_date            = "";
    var $_landing         = "";
    var $_hour_type       = "";
    var $_pld_south       = "";
    var $_pld_southeast   = "";
    var $_pld_north       = "";
    var $_pld_northeast   = "";
    var $_week_ccee       = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;
    var $_date_start      = "";
    var $_date_end        = "";
    var $_week_start      = "";
    var $_week_end        = "";

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        unset($data['date_start']);
        unset($data['date_end']);
        unset($data['week_start']);
        unset($data['week_end']);
        $this->db->insert('hourly_basis', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_hourly_basis', $this->_pk_hourly_basis);
        $rec = $this->db->get('hourly_basis');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data = get_array_from_object($this);
        unset($data['date_start']);
        unset($data['date_end']);
        unset($data['week_start']);
        unset($data['week_end']);
        $this->db->where('pk_hourly_basis', $this->_pk_hourly_basis);
        $this->db->update('hourly_basis', $data);
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_hourly_basis', $this->_pk_hourly_basis);
        $this->db->update('hourly_basis', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_week !== "") {
            $this->db->where('week', $this->_week);
        }
        if ($this->_date_start !== "" && $this->_date_end !== "") {
            $this->db->where('date >=', $this->_date_start);
            $this->db->where('date <=', $this->_date_end);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('hourly_basis');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_start_end_date() {
        $this->db->where('date >=', $this->_date_start);
        $this->db->where('date <=', $this->_date_end);
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('hourly_basis');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_start_end_week() {
        $this->db->select(
                array(
                    'week',
                    '(SUM(pld_south) / COUNT(week)) AS avg_south',
                    '(SUM(pld_southeast) / COUNT(week)) AS avg_southeast',
                    '(SUM(pld_north) / COUNT(week)) AS avg_north',
                    '(SUM(pld_northeast) / COUNT(week)) AS avg_northeast'
                )
        );
        $this->db->where('week >=', $this->_week_start);
        $this->db->where('week <=', $this->_week_end);
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->group_by('week');
        $rec = $this->db->get('hourly_basis');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
