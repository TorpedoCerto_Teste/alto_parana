<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee
  fk_unity
  fk_user
  status
  created_at
  updated_at

 */
class Ccvee_model extends CI_Model {

    var $_pk_ccvee        = "";
    var $_fk_unity        = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $data['fk_unity']   = $this->_fk_unity;
        $data['fk_user']    = $this->_fk_user;
        $this->db->insert('ccvee', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee', $this->_pk_ccvee);
        $this->db->where('fk_unity', $this->_fk_unity);
        $rec = $this->db->get('ccvee');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {       
        $this->db->where('pk_ccvee', $this->_pk_ccvee);
        $this->db->update('ccvee', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee', $this->_pk_ccvee);
        $this->db->where('fk_unity', $this->_fk_unity);
        $this->db->update('ccvee', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_unity !== "") {
            $this->db->where('fk_unity', $this->_fk_unity);
        }
        $rec = $this->db->get('ccvee');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_unity_agent_submarket() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_unity !== "") {
            $this->db->where('fk_unity', $this->_fk_unity);
        }
        $rec = $this->db->get('ccvee');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
