<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of message_agent_model
 *
 * @author Rogerio Pellarin
 * 
  pk_message_agent
  fk_message
  fk_agent

 */
class Message_agent_model extends CI_Model {

    var $_pk_message_agent = "";
    var $_fk_message       = "";
    var $_fk_agent         = "";
    var $_status           = "";
    var $_created_at       = "";
    var $_updated_at       = "";
    var $_status_active    = 1;
    var $_status_inactive  = 0;
    var $_status_read      = 2;
    var $_limit            = 4;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        unset($data['status_read']);
        unset($data['limit']);
        $this->db->insert('message_agent', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $this->db->where('pk_message_agent', $this->_pk_message_agent);
        $rec = $this->db->get('message_agent');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data = get_array_from_object($this);
        unset($data['status_read']);
        unset($data['limit']);
        $this->db->where('pk_message_agent', $this->_pk_message_agent);
        $this->db->update('message_agent', $data);
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        unset($data['status_read']);
        unset($data['limit']);
        $this->db->where('pk_message_agent', $this->_pk_message_agent);
        $q    = $this->db->get('message_agent');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('message_agent', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_message_agent', $this->_pk_message_agent);
            $this->db->update('message_agent', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_message_agent', $this->_pk_message_agent);
        $this->db->update('message_agent', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_message !== "") {
            $this->db->where('fk_message', $this->_fk_message);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $rec = $this->db->get('message_agent');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->db->select(
                array(
                    'message_agent.*',
                    'message.title',
                    'message.message',
                    'message.priority'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('message_agent.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('message_agent.fk_agent', $this->_fk_agent);
        }
        $this->db->join('message', 'message.pk_message = message_agent.fk_message');
        $this->db->order_by('message_agent.created_at', 'DESC');
        $this->db->order_by('message_agent.status', 'ASC');
        $this->db->order_by('message.priority', 'DESC');
        if ($this->_limit !== "") {
            $this->db->limit($this->_limit);
        }
        $rec = $this->db->get('message_agent');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function get_total() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_message !== "") {
            $this->db->where('fk_message', $this->_fk_message);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $rec = $this->db->get('message_agent');
        return isset($rec->num_rows) ? $rec->num_rows : $rec->result_id->num_rows;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
