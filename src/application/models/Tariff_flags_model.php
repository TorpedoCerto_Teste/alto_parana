<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of tariff_flags_model
 *
 * @author Rogerio Pellarin
 * 
  pk_tariff_flags
  year
  month
  flag
  increase
  status
  created_at
  updated_at

 */
class Tariff_flags_model extends CI_Model {

    var $_pk_tariff_flags = "";
    var $_year            = "";
    var $_month           = "";
    var $_flag            = "";
    var $_increase        = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('tariff_flags', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_tariff_flags', $this->_pk_tariff_flags);
        $rec = $this->db->get('tariff_flags');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_tariff_flags', $this->_pk_tariff_flags);
        $this->db->update('tariff_flags', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_tariff_flags', $this->_pk_tariff_flags);
        $this->db->update('tariff_flags', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where('year', $this->_year);
        $rec = $this->db->get('tariff_flags');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('year', $this->_year);
        $this->db->where('month', $this->_month);
        $q    = $this->db->get('tariff_flags');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('tariff_flags', $data);
            $ret = $this->db->insert_id();
        }
        else {
            unset($data['created_at']);
            $this->db->where('year', $this->_year);
            $this->db->where('month', $this->_month);
            $this->db->update('tariff_flags', $data);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
