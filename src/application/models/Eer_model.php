<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of eer_model
 *
 * @author Rogerio Pellarin
 * 
  pk_eer
  fk_agent
  month
  year
  value
  eer_file
 */
class Eer_model extends CI_Model {

    var $_pk_eer          = "";
    var $_fk_agent        = "";
    var $_month           = "";
    var $_year            = "";
    var $_value           = "";
    var $_eer_file        = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;
    var $_state           = ""; //utilizado no join do fetch para agent

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('eer', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_eer !== "") {
            $this->db->where('pk_eer', $this->_pk_eer);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('eer');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_eer', $this->_pk_eer);
        $this->db->update('eer', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function update_in() {
        $this->db->where_in('pk_eer', explode(",", $this->_pk_eer));
        $this->db->update('eer', array('value' => 0));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_eer', $this->_pk_eer);
        $q = $this->db->get('eer');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('eer', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            unset($data['pk_eer']);
            $this->db->where('pk_eer', $this->_pk_eer);
            $this->db->update('eer', $data);
            return $this->_pk_eer;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_eer', $this->_pk_eer);
        $this->db->update('eer', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'eer.*',
            'agent.community_name',
            'eer_market.reporting_date',
            'eer_market.payment_date',
            'eer_market.value AS eer_market_value',
        ));
        if ($this->_fk_agent !== "") {
            $this->db->where('eer.fk_agent', $this->_fk_agent);
        }
        if ($this->_month !== "") {
            $this->db->where('eer.month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('eer.year', $this->_year);
        }
        if ($this->_state !== "" && $this->_state !== NULL) {
            $this->db->where('agent.state', $this->_state);
        }
        if ($this->_status !== "") {
            $this->db->where('eer.status', $this->_status);
        }
        $this->db->join('agent', 'agent.pk_agent = eer.fk_agent');
        $this->db->join('eer_market', 'eer_market.month = eer.month AND eer_market.year =  eer.year', 'left');
        $rec = $this->db->get('eer');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
