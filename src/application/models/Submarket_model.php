<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Submarket_model
 *
 * @author rogeriopellarin
 * 
  pk_submarket
  submarket
  initials
  status
  created_at
  updated_at
 */
class Submarket_model extends CI_Model {

    var $_pk_submarket    = "";
    var $_submarket       = "";
    var $_initials        = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('submarket', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_submarket', $this->_pk_submarket);
        $rec = $this->db->get('submarket');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_submarket', $this->_pk_submarket);
        $this->db->update('submarket', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_submarket', $this->_pk_submarket);
        $this->db->update('submarket', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('submarket');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
