<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ercap_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ercap
  fk_agent
  month
  year
  value
  ercap_file
 */
class Ercap_model extends CI_Model {

    var $_pk_ercap          = "";
    var $_fk_agent        = "";
    var $_month           = "";
    var $_year            = "";
    var $_value           = "";
    var $_ercap_file        = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;
    var $_state           = ""; //utilizado no join do fetch para agent

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('ercap', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_ercap !== "") {
            $this->db->where('pk_ercap', $this->_pk_ercap);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('ercap');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_ercap', $this->_pk_ercap);
        $this->db->update('ercap', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function update_in() {
        $this->db->where_in('pk_ercap', explode(",", $this->_pk_ercap));
        $this->db->update('ercap', array('value' => 0));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_ercap', $this->_pk_ercap);
        $q = $this->db->get('ercap');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ercap', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            unset($data['pk_ercap']);
            $this->db->where('pk_ercap', $this->_pk_ercap);
            $this->db->update('ercap', $data);
            return $this->_pk_ercap;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ercap', $this->_pk_ercap);
        $this->db->update('ercap', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'ercap.*',
            'agent.community_name',
            'ercap_market.reporting_date',
            'ercap_market.payment_date',
            'ercap_market.value AS ercap_market_value',
        ));
        if ($this->_fk_agent !== "") {
            $this->db->where('ercap.fk_agent', $this->_fk_agent);
        }
        if ($this->_month !== "") {
            $this->db->where('ercap.month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('ercap.year', $this->_year);
        }
        if ($this->_state !== "" && $this->_state !== NULL) {
            $this->db->where('agent.state', $this->_state);
        }
        if ($this->_status !== "") {
            $this->db->where('ercap.status', $this->_status);
        }
        $this->db->join('agent', 'agent.pk_agent = ercap.fk_agent');
        $this->db->join('ercap_market', 'ercap_market.month = ercap.month AND ercap_market.year =  ercap.year', 'left');
        $rec = $this->db->get('ercap');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return [];
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
