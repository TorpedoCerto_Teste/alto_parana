<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of tusd_model
 *
 * @author Rogerio Pellarin
 *
  pk_tusd
  fk_agent
  fk_unity
  month
  year
  expiration_date
  demand_end
  demand_tip_out
  consumption_end
  consumption_tip_out
  reactive_energy_end
  reactive_energi_tip_out
  connection_charge
  adjustment
  discount
  street_lighting
  icms
  icms_reduction
  piscofins
  status
  created_at
  updated_at
 * value
 * free_captive
 tusd_file

 */
class Tusd_model extends CI_Model {

    var $_pk_tusd                 = "";
    var $_fk_unity                = "";
    var $_fk_agent                = "";
    var $_month                   = "";
    var $_year                    = "";
    var $_expiration_date         = "";
    var $_demand_end              = "";
    var $_demand_tip_out          = "";
    var $_consumption_end         = "";
    var $_consumption_tip_out     = "";
    var $_reactive_energy_end     = "";
    var $_reactive_energi_tip_out = "";
    var $_connection_charge       = "";
    var $_adjustment              = "";
    var $_discount                = "";
    var $_street_lighting         = "";
    var $_icms                    = "";
    var $_icms_reduction          = "";
    var $_piscofins               = "";
    var $_status                  = "";
    var $_created_at              = "";
    var $_updated_at              = "";
    var $_status_active           = 1;
    var $_status_inactive         = 0;
    var $_value                   = "";
    var $_free_captive            = "";
    var $_tusd_file               = "";

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('tusd', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_fk_unity !== "") {
            $this->db->where('fk_unity', $this->_fk_unity);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_pk_tusd !== "") {
            $this->db->where('pk_tusd', $this->_pk_tusd);
        }
        
        $rec = $this->db->get('tusd');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_tusd', $this->_pk_tusd);
        $this->db->update('tusd', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_tusd', $this->_pk_tusd);
        $q = $this->db->get('tusd');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('tusd', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_tusd', $this->_pk_tusd);
            $this->db->update('tusd', $data);
            return $this->_pk_tusd;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_tusd', $this->_pk_tusd);
        $this->db->update('tusd', $data);
        return $this->db->affected_rows();
    }

    function delete_file() {
        $data = array(
            'tusd_file' => null
        );
        $this->db->where('pk_tusd', $this->_pk_tusd);
        $this->db->update('tusd', $data);
        return $this->db->affected_rows();
    }    

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('tusd');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->db->select(array(
            'tusd.*',
            'unity.community_name AS unity',
            'agent_power_distributor.community_name as power_distributor'
        ));
        $this->db->join("unity", "unity.pk_unity = tusd.fk_unity");
        $this->db->join("agent AS agent_power_distributor", "agent_power_distributor.pk_agent = unity.fk_agent_power_distributor");
        $this->db->join("agent", "agent.pk_agent = unity.fk_agent");
        $this->db->where("tusd.status", $this->Tusd_model->_status_active);
        $this->db->where("agent.pk_agent", $this->session->userdata("fk_agent"));
        if ($this->_status !== "") {
            $this->db->where('tusd.status', $this->_status);
        }
        $rec = $this->db->get('tusd');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }
    
    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
