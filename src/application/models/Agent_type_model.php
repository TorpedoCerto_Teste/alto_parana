<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of agent_type
 *
 * @author rogeriopellarin
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of agent_type_model
 *
 * @author rogeriopellarin
 * 
 * pk_agent_type
 * type
 * status
 * created_at
 * updated_at
 */
class Agent_type_model extends CI_Model {

    var $_pk_agent_type   = "";
    var $_type            = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('agent_type', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_agent_type', $this->_pk_agent_type);
        $rec = $this->db->get('agent_type');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_agent_type', $this->_pk_agent_type);
        $this->db->update('agent_type', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_agent_type', $this->_pk_agent_type);
        $this->db->update('agent_type', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->order_by('type', 'ASC');
        $rec = $this->db->get('agent_type');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
