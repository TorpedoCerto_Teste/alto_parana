<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of group_model
 *
 * @author Rogerio Pellarin
 * 
  pk_group
  group
  group_short_name

 */
class Group_model extends CI_Model {

    var $_pk_group         = "";
    var $_group            = "";
    var $_group_short_name = "";
    var $_status           = "";
    var $_created_at       = "";
    var $_updated_at       = "";
    var $_status_active    = 1;
    var $_status_inactive  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('group', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_group', $this->_pk_group);
        $rec = $this->db->get('group');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_group', $this->_pk_group);
        $this->db->update('group', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_group', $this->_pk_group);
        $q                  = $this->db->get('group');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('group', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_group', $this->_pk_group);
            $this->db->update('group', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_group', $this->_pk_group);
        $this->db->update('group', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('group');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
