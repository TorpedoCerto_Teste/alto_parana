<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of energy_offer_year_model
 *
 * @author Rogerio Pellarin
 * 
  pk_energy_offer_year
  fk_energy_offer
  year
  price
 */
class Energy_offer_year_model extends CI_Model {

    var $_pk_energy_offer_year = null;
    var $_fk_energy_offer      = null;
    var $_year                 = null;
    var $_price                = null;
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('energy_offer_year', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_energy_offer_year', $this->_pk_energy_offer_year);
        $rec = $this->db->get('energy_offer_year');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_energy_offer_year', $this->_pk_energy_offer_year);
        $this->db->update('energy_offer_year', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_energy_offer_year', $this->_pk_energy_offer_year);
        $q    = $this->db->get('energy_offer_year');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('energy_offer_year', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_energy_offer_year', $this->_pk_energy_offer_year);
            $this->db->update('energy_offer_year', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_energy_offer_year', $this->_pk_energy_offer_year);
        $this->db->update('energy_offer_year', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'energy_offer_year.*',
                    'COUNT(energy_offer_year_agent.pk_energy_offer_year_agent) AS total_sent'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('energy_offer_year.status', $this->_status);
        }
        if ($this->_pk_energy_offer_year !== "") {
            $this->db->where('energy_offer_year.pk_energy_offer_year', $this->_pk_energy_offer_year);
        }
        $this->db->join('energy_offer_year_agent', 'energy_offer_year_agent.fk_energy_offer_year = energy_offer_year.pk_energy_offer_year');
        $this->db->group_by('energy_offer_year.pk_energy_offer_year');
        $rec = $this->db->get('energy_offer_year');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
