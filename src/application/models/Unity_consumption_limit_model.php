<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unity_consumption_limit_model extends CI_Model {

    var $_pk_unity_consumption_limit = "";
    var $_fk_unity                   = "";
    var $_month                      = "";
    var $_year                       = "";
    var $_consumption_limit          = "";
    var $_status                     = "";
    var $_created_at                 = "";
    var $_updated_at                 = "";
    var $_status_active              = 1;
    var $_status_inactive            = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('unity_consumption_limit', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_unity_consumption_limit', $this->_pk_unity_consumption_limit);
        $rec = $this->db->get('unity_consumption_limit');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_unity_consumption_limit', $this->_pk_unity_consumption_limit);
        $this->db->update('unity_consumption_limit', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_unity_consumption_limit', $this->_pk_unity_consumption_limit);
        $q = $this->db->get('unity_consumption_limit');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('unity_consumption_limit', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_unity_consumption_limit', $this->_pk_unity_consumption_limit);
            $this->db->update('unity_consumption_limit', $data);
            return $this->_pk_unity_consumption_limit;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_unity_consumption_limit', $this->_pk_unity_consumption_limit);
        $this->db->update('unity_consumption_limit', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_unity !== "") {
            $this->db->where('fk_unity', $this->_fk_unity);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        $rec = $this->db->get('unity_consumption_limit');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
