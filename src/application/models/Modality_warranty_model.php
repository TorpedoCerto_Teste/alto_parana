<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of modality_warranty_model
 *
 * @author Rogerio Pellarin
 *
  pk_modality_warranty
  modality
  status
  created_at
  updated_at

 */
class Modality_warranty_model extends CI_Model {

    var $_pk_modality_warranty = "";
    var $_modality             = "";
    var $_status               = "";
    var $_created_at           = "";
    var $_updated_at           = "";
    var $_status_active        = 1;
    var $_status_inactive      = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('modality_warranty', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_modality_warranty', $this->_pk_modality_warranty);
        $rec = $this->db->get('modality_warranty');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_modality_warranty', $this->_pk_modality_warranty);
        $this->db->update('modality_warranty', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_modality_warranty', $this->_pk_modality_warranty);
        $q    = $this->db->get('modality_warranty');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('modality_warranty', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_modality_warranty', $this->_pk_modality_warranty);
            $this->db->update('modality_warranty', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_modality_warranty', $this->_pk_modality_warranty);
        $this->db->update('modality_warranty', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->order_by('modality', 'ASC');
        $rec = $this->db->get('modality_warranty');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
