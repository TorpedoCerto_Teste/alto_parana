<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_energy_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_energy
  fk_ccvee_general
  supply_start
  supply_end
  losses_type
  losses
  differentiated_volume_period
  unit_measurement
  discount_proinfa
  seasonal
  seasonal_inferior
  seasonal_superior
  flexibility
  flexibility_inferior
  flexibility_superior
  modulation
  modulation_heavy_inferior
  modulation_heavy_superior
  modulation_medium_inferior
  modulation_medium_superior
  modulation_light_inferior
  modulation_light_superior
  modulation_type_input
  status
  created_at
  updated_at

 */
class Ccvee_energy_model extends CI_Model {

    var $_pk_ccvee_energy              = "";
    var $_fk_ccvee_general             = "";
    var $_supply_start                 = "";
    var $_supply_end                   = "";
    var $_losses_type                  = "";
    var $_losses                       = "";
    var $_differentiated_volume_period = "";
    var $_unit_measurement             = "";
    var $_discount_proinfa             = "";
    var $_seasonal                     = "";
    var $_seasonal_inferior            = "";
    var $_seasonal_superior            = "";
    var $_flexibility                  = "";
    var $_flexibility_inferior         = "";
    var $_flexibility_superior         = "";
    var $_modulation                   = "";
    var $_modulation_heavy_inferior    = "";
    var $_modulation_heavy_superior    = "";
    var $_modulation_medium_inferior   = "";
    var $_modulation_medium_superior   = "";
    var $_modulation_light_inferior    = "";
    var $_modulation_light_superior    = "";
    var $_modulation_type_input        = "";
    var $_status                       = "";
    var $_created_at                   = "";
    var $_updated_at                   = "";
    var $_status_active                = 1;
    var $_status_inactive              = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_energy', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_ccvee_energy !== "") {
            $this->db->where('pk_ccvee_energy', $this->_pk_ccvee_energy);
        }
        if ($this->_fk_ccvee_general !== "") {
            $this->db->where('fk_ccvee_general', $this->_fk_ccvee_general);
        }
        $rec = $this->db->get('ccvee_energy');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_energy', $this->_pk_ccvee_energy);
        $this->db->update('ccvee_energy', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee_energy', $this->_pk_ccvee_energy);
        $this->db->update('ccvee_energy', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_ccvee_energy !== "") {
            $this->db->where('pk_ccvee_energy', $this->_pk_ccvee_energy);
        }
        $rec = $this->db->get('ccvee_energy');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

    function upsert() {
        $ret                = false;
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_energy', $this->_pk_ccvee_energy);
        $q                  = $this->db->get('ccvee_energy');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_energy', $data);
            $ret = $this->db->insert_id();
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_energy', $this->_pk_ccvee_energy);
            $this->db->update('ccvee_energy', $data);
            $rec = $q->result_array();
            $ret = $rec[0]['pk_ccvee_energy'];
        }
        return $ret;
    }

    function _proccess_supply($start_date, $end_date, $pk_ccvee_energy) {
        $return       = array();
        $supply_start = new DateTime($start_date);
        $supply_end   = new DateTime($end_date);

        $total_months = ($supply_start->diff($supply_end)->m + ($supply_start->diff($supply_end)->y * 12)) + 1;

        if ($total_months) {
            $count = 0;
            for ($i = 0; $i < $total_months; $i++) {
                $new_date              = $supply_start;
                $new_date->add(new DateInterval('P' . $count . 'M'));
                $return[$i]['periodo'] = $new_date->format('m/Y');
                $count                 = 1;
            }
            if ($pk_ccvee_energy > 0) {
                $return = $this->_return_merged_supply($return, $pk_ccvee_energy);
            }
        }
        return $return;
    }

    function _return_merged_supply($supply_period, $pk_ccvee_energy) {
        //pega os dados de volume diferenciado por período
        $this->load->model("Ccvee_energy_bulk_model");
        $this->Ccvee_energy_bulk_model->_fk_ccvee_energy = $pk_ccvee_energy;
        $this->Ccvee_energy_bulk_model->_status          = $this->Ccvee_energy_bulk_model->_status_active;
        $ccvee_energy_bulk                               = $this->Ccvee_energy_bulk_model->fetch();

        return $this->Ccvee_energy_model->_merge_supply_period($supply_period, $ccvee_energy_bulk);
    }

    function _merge_supply_period($period, $data) {
        foreach ($period as $k => $p) {
            $period[$k]['forecast'] = 0;
            $period[$k]['seasonal'] = 0;
            $period[$k]['adjusted'] = 0;
            $period[$k]['attendance'] = 0;
            $period[$k]['losses']   = 0;
        }

        if ($data) {
            foreach ($period as $k => $p) {
                foreach ($data as $d) {
                    if ($p['periodo'] == date("m/Y", strtotime($d['date']))) {
                        $period[$k]['forecast'] = $d['forecast'];
                        $period[$k]['seasonal'] = $d['seasonal'];
                        $period[$k]['adjusted'] = $d['adjusted'];
                        $period[$k]['attendance'] = $d['attendance'];
                        $period[$k]['losses']   = $d['losses'];
                    }
                }
            }
        }
        return $period;
    }

    function _proccess_seasonal($start_date, $end_date, $pk_ccvee_energy) {
        $return       = array();
        $supply_start = new DateTime($start_date);
        $supply_end   = new DateTime($end_date);

        $total_years = ($supply_start->diff($supply_end)->y) + 1;

        if ($total_years > 1) {
            $count = 0;
            for ($i = 0; $i < $total_years; $i++) {
                $new_date            = $supply_start;
                $new_date->add(new DateInterval('P' . $count . 'Y'));
                $return[$i]['year']  = $new_date->format('Y');
                $return[$i]['limit'] = "";
                $count               = 1;
            }
        }
        else {
            $return[0]['year']  = $supply_start->format('Y');
            $return[0]['limit'] = "";
        }

        if ($pk_ccvee_energy > 0) {
            $return = $this->_merge_seasonal($return, $pk_ccvee_energy);
        }
        return $return;
    }

    function _merge_seasonal($seasonal, $pk_ccvee_energy) {
        $this->load->model("Ccvee_seasonal_limit_model");
        $this->Ccvee_seasonal_limit_model->_fk_ccvee_energy = $pk_ccvee_energy;
        $this->Ccvee_seasonal_limit_model->_status          = $this->Ccvee_seasonal_limit_model->_status_active;
        $limit                                              = $this->Ccvee_seasonal_limit_model->fetch();
        if ($limit) {
            foreach ($seasonal as $k => $s) {
                foreach ($limit as $l) {
                    if ($s['year'] == $l['year']) {
                        $seasonal[$k]['limit'] = date_to_human_date($l['limit']);
                    }
                }
            }
        }
        return $seasonal;
    }

}
