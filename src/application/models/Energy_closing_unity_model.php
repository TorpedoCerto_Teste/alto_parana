<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of energy_closing_unity_model
 *
 * @author Rogerio Pellarin
 *
  pk_energy_closing_unity
  fk_energy_closing
  fk_unity
  consumption
  loosing
  proinfa
  total
  acl_average
  energy_closing_unity_file
  status
  created_at
  updated_at

 */
class Energy_closing_unity_model extends CI_Model {

    var $_pk_energy_closing_unity   = "";
    var $_fk_energy_closing         = "";
    var $_fk_unity                  = "";
    var $_consumption               = "";
    var $_loosing                   = "";
    var $_proinfa                   = "";
    var $_total                     = "";
    var $_acl_average               = "";
    var $_energy_closing_unity_file = "";
    var $_status                    = "";
    var $_created_at                = "";
    var $_updated_at                = "";
    var $_status_active             = 1;
    var $_status_inactive           = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('energy_closing_unity', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_energy_closing_unity', $this->_pk_energy_closing_unity);
        $rec = $this->db->get('energy_closing_unity');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_energy_closing_unity', $this->_pk_energy_closing_unity);
        $this->db->update('energy_closing_unity', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_energy_closing_unity', $this->_pk_energy_closing_unity);
        $q = $this->db->get('energy_closing_unity');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('energy_closing_unity', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_energy_closing_unity', $this->_pk_energy_closing_unity);
            $this->db->update('energy_closing_unity', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_energy_closing_unity', $this->_pk_energy_closing_unity);
        $this->db->update('energy_closing_unity', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('energy_closing_unity');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
