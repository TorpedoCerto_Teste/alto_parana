<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of log_model
 *
 * @author Rogerio Pellarin
 * 
    pk_log
    key
    date
    data

 */
class Log_model extends CI_Model {

    var $_pk_log = "";
    var $_key = "";
    var $_date = "";
    var $_data = "";
    var $_created_at = "";

    function __construct() {
        parent::__construct();
    }

    function save() {
        $this->db->insert('log', get_array_from_object($this));
        return $this->db->insert_id();
    }
}