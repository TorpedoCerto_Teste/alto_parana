<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_billing_model
 *
 * @author Rogerio Pellarin
 *
  pk_ccvee_billing
  fk_ccvee_general
  fk_unity
  year
  month
  expiration_date
  total
  payment_status
  payment_date
  invoice_type
  payment_slip
  invoice
  xml
  status
  created_at
  updated_at

 */
class Ccvee_billing_model extends CI_Model {

    var $_pk_ccvee_billing = "";
    var $_fk_ccvee_general = "";
    var $_fk_unity         = "";
    var $_year             = "";
    var $_month            = "";
    var $_expiration_date  = "";
    var $_total            = "";
    var $_payment_status   = "";
    var $_payment_date     = "";
    var $_invoice_type     = "";
    var $_payment_slip     = "";
    var $_invoice          = "";
    var $_xml              = "";
    var $_status           = "";
    var $_created_at       = "";
    var $_updated_at       = "";
    var $_status_active    = 1;
    var $_status_inactive  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_billing', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee_billing', $this->_pk_ccvee_billing);
        $rec = $this->db->get('ccvee_billing');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {       
        $this->db->where('pk_ccvee_billing', $this->_pk_ccvee_billing);
        $this->db->update('ccvee_billing', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_billing', $this->_pk_ccvee_billing);
        $q                  = $this->db->get('ccvee_billing');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_billing', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_billing', $this->_pk_ccvee_billing);
            $this->db->update('ccvee_billing', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee_billing', $this->_pk_ccvee_billing);
        $this->db->update('ccvee_billing', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select([
            'ccvee_billing.*',
            'unity.community_name AS unity_community_name'
        ]);
        if ($this->_status !== "") {
            $this->db->where('ccvee_billing.status', $this->_status);
        }
        if ($this->_fk_ccvee_general !== "") {
            $this->db->where('ccvee_billing.fk_ccvee_general', $this->_fk_ccvee_general);
        }
        $this->db->join('ccvee_general', 'ccvee_general.pk_ccvee_general = ccvee_billing.fk_ccvee_general', 'LEFT');
        $this->db->join('unity', 'unity.pk_unity = ccvee_billing.fk_unity', 'LEFT');
        $rec = $this->db->get('ccvee_billing');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
