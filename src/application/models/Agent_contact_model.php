<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of agent_contact_model
 *
 * @author Rogerio Pellarin
 * 
  pk_agent_contact
  fk_agent
  fk_contact
  fk_group

 */
class Agent_contact_model extends CI_Model {

    var $_pk_agent_contact = "";
    var $_fk_agent               = "";
    var $_fk_contact             = "";
    var $_status                 = "";
    var $_created_at             = "";
    var $_updated_at             = "";
    var $_status_active          = 1;
    var $_status_inactive        = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('agent_contact', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_agent_contact', $this->_pk_agent_contact);
        $rec = $this->db->get('agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_agent_contact', $this->_pk_agent_contact);
        $this->db->update('agent_contact', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_agent_contact', $this->_pk_agent_contact);
        $q                  = $this->db->get('agent_contact');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('agent_contact', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_agent_contact', $this->_pk_agent_contact);
            $this->db->update('agent_contact', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_fk_contact !== "") {
            $this->db->where('fk_contact', $this->_fk_contact);
        }
        if ($this->_pk_agent_contact !== "") {
            $this->db->where('pk_agent_contact', $this->_pk_agent_contact);
        }
        $this->db->update('agent_contact', $data);
        return $this->db->affected_rows();
    }

    function deattach() {
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_fk_contact !== "") {
            $this->db->where('fk_contact', $this->_fk_contact);
        }
        if ($this->_pk_agent_contact !== "") {
            $this->db->where('pk_agent_contact', $this->_pk_agent_contact);
        }
        $this->db->limit(1);
        return $this->db->delete('agent_contact'); 
    }
    
    function fetch() {
        $this->load->model('Contact_model');
        if ($this->_status !== "") {
            $this->db->where('agent_contact.status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('agent_contact.fk_agent', $this->_fk_agent);
        }
        if ($this->_fk_contact !== "") {
            $this->db->where('agent_contact.fk_contact', $this->_fk_contact);
        }
        $this->db->join('agent', 'agent.pk_agent = agent_contact.fk_agent AND agent.status = '.$this->_status_active);
        $this->db->join('contact', 'contact.pk_contact = agent_contact.fk_contact AND contact.status = '.$this->Contact_model->_status_active);
        $rec = $this->db->get('agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->load->model('Contact_model');
        $this->db->select(
                array(
                    'agent_contact.*',
                    'contact.*'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('agent_contact.status', $this->_status);
        }
        $this->db->where('agent_contact.fk_agent', $this->_fk_agent);
        $this->db->join('agent', 'agent.pk_agent = agent_contact.fk_agent');
        $this->db->join('contact', 'contact.pk_contact = agent_contact.fk_contact AND contact.status != '.$this->Contact_model->_status_deleted);
        $this->db->order_by('contact.name');
        $rec = $this->db->get('agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_external_contacts() {
        $this->load->model('Contact_model');
        $this->db->select(
                array(
                    'agent_contact.*',
                    'contact.*'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('agent_contact.status', $this->_status);
        }
        $this->db->where('agent_contact.fk_agent !=', $this->_fk_agent);
        $this->db->join('agent', 'agent.pk_agent = agent_contact.fk_agent');
        $this->db->join('contact', 'contact.pk_contact = agent_contact.fk_contact AND contact.status != '.$this->Contact_model->_status_deleted);
        $this->db->where('agent_contact.fk_contact NOT IN (SELECT fk_contact FROM agent_contact WHERE fk_agent = '.$this->_fk_agent.')');
        $this->db->order_by('contact.name');
        $rec = $this->db->get('agent_contact');
        
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function export() {
        $this->db->select('c.name, c.surname, c.email, a.community_name as agent_name, g.group, g.group_short_name, u.community_name as unity_name');
        $this->db->from('agent_contact ac');
        $this->db->join('contact c', 'c.pk_contact = ac.fk_contact');
        $this->db->join('agent a', 'a.pk_agent = ac.fk_agent');
        $this->db->join('group_agent_contact gac', 'gac.fk_contact = c.pk_contact AND gac.fk_agent = a.pk_agent', 'left');
        $this->db->join('group g', 'g.pk_group = gac.fk_group', 'left');
        $this->db->join('unity_contact uc', 'uc.fk_contact = c.pk_contact AND uc.status = 1', 'left');
        $this->db->join('unity u', 'u.pk_unity = uc.fk_unity', 'left');
        if ($this->_fk_agent > 0) {
            $this->db->where('ac.fk_agent', $this->_fk_agent);
        }        
        $this->db->where('ac.status', 1);
        $this->db->order_by('a.community_name');
        $this->db->order_by('c.name');
        $rec = $this->db->get();
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;      
    }
    
    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
