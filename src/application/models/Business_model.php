<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of business_model
 *
 * @author Rogerio Pellarin
 *
  pk_business
  fk_unity
  fk_user
  fk_agent
  start_date
  end_date
  automatic_renovation
  memo
  unity_differentiated_duration
  business_file
  status
  created_at
  updated_at


 */
class Business_model extends CI_Model {

    var $_pk_business                   = "";
    var $_fk_unity                      = "";
    var $_fk_user                       = "";
    var $_fk_agent                      = "";
    var $_start_date                    = "";
    var $_end_date                      = "";
    var $_automatic_renovation          = "";
    var $_memo                          = "";
    var $_unity_differentiated_duration = "";
    var $_business_file                 = "";
    var $_status                        = "";
    var $_created_at                    = "";
    var $_updated_at                    = "";
    var $_status_active                 = 1;
    var $_status_inactive               = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('business', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_business', $this->_pk_business);
        $rec = $this->db->get('business');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_business', $this->_pk_business);
        $this->db->update('business', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_business', $this->_pk_business);
        $q                  = $this->db->get('business');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('business', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_business', $this->_pk_business);
            $this->db->update('business', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_business', $this->_pk_business);
        $this->db->update('business', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'business.*',
            'unity.community_name AS unity',
            'agent.community_name AS agent'
        ));
        if ($this->_status !== "") {
            $this->db->where('business.status', $this->_status);
        }
        if ($this->_fk_unity !== "") {
            $this->db->where('business.fk_unity', $this->_fk_unity);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('business.fk_agent', $this->_fk_agent);
        }
        $this->db->join('unity', 'unity.pk_unity = business.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = business.fk_agent', 'LEFT');
        $rec = $this->db->get('business');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->db->select(array(
            'business.*',
            'unity.community_name',
            'business_payment.remuneration_type',
            'business_payment_monthly_unity.monthly_value'
        ));
        if ($this->_status !== "") {
            $this->db->where('agent.pk_agent', $this->_fk_agent);
        }
        $this->db->where('business.status', $this->_status);
        $this->db->join('unity', 'unity.pk_unity = business.fk_unity');
        $this->db->join('agent', 'agent.pk_agent = unity.fk_agent');
        $this->db->join('business_payment', 'business_payment.fk_business = business.pk_business');
        $this->db->join('business_payment_monthly_unity', 'business_payment_monthly_unity.fk_business_payment = business_payment.pk_business_payment', 'LEFT');
        $rec = $this->db->get('business');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
