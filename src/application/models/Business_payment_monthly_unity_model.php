<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of business_payment_monthly_unity_model
 *
 * @author Rogerio Pellarin
 * 
  pk_business_payment_monthly_unity
  fk_business_payment
  fk_unity
  monthly_value

 */
class Business_payment_monthly_unity_model extends CI_Model {

    var $_pk_business_payment_monthly_unity = "";
    var $_fk_business_payment               = "";
    var $_fk_unity                          = "";
    var $_monthly_value                     = "";
    var $_status                            = "";
    var $_created_at                        = "";
    var $_updated_at                        = "";
    var $_status_active                     = 1;
    var $_status_inactive                   = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('business_payment_monthly_unity', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_business_payment_monthly_unity', $this->_pk_business_payment_monthly_unity);
        $rec = $this->db->get('business_payment_monthly_unity');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_business_payment_monthly_unity', $this->_pk_business_payment_monthly_unity);
        $this->db->update('business_payment_monthly_unity', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_business_payment_monthly_unity', $this->_pk_business_payment_monthly_unity);
        $q    = $this->db->get('business_payment_monthly_unity');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('business_payment_monthly_unity', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_business_payment_monthly_unity', $this->_pk_business_payment_monthly_unity);
            $this->db->update('business_payment_monthly_unity', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_business_payment !== "") {
            $this->db->where('fk_business_payment', $this->_fk_business_payment);
        }
        if ($this->_pk_business_payment_monthly_unity !== "") {
            $this->db->where('pk_business_payment_monthly_unity', $this->_pk_business_payment_monthly_unity);
        }
        $this->db->update('business_payment_monthly_unity', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_business_payment !== "") {
            $this->db->where('fk_business_payment', $this->_fk_business_payment);
        }
        $rec = $this->db->get('business_payment_monthly_unity');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
