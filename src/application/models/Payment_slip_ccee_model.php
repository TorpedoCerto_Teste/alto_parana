<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of payment_slip_ccee_model
 *
 * @author Rogerio Pellarin
 *
  pk_payment_slip_ccee
  month
  year
  fk_agent
  payment_slip_file
  value
  validate
  status
  created_at
  updated_at

 */
class Payment_slip_ccee_model extends CI_Model {

    var $_pk_payment_slip_ccee = "";
    var $_month                = "";
    var $_year                 = "";
    var $_fk_agent             = "";
    var $_payment_slip_file    = "";
    var $_value                = "";
    var $_validate             = "";
    var $_status               = "";
    var $_created_at           = "";
    var $_updated_at           = "";
    var $_status_active        = 1;
    var $_status_inactive      = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('payment_slip_ccee', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_pk_payment_slip_ccee !== "") {
            $this->db->where('pk_payment_slip_ccee', $this->_pk_payment_slip_ccee);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $rec = $this->db->get('payment_slip_ccee');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_payment_slip_ccee', $this->_pk_payment_slip_ccee);
        $this->db->update('payment_slip_ccee', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_payment_slip_ccee', $this->_pk_payment_slip_ccee);
        $q = $this->db->get('payment_slip_ccee');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('payment_slip_ccee', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_payment_slip_ccee', $this->_pk_payment_slip_ccee);
            $this->db->update('payment_slip_ccee', $data);
            return $this->_pk_payment_slip_ccee;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_payment_slip_ccee', $this->_pk_payment_slip_ccee);
        $this->db->update('payment_slip_ccee', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $rec = $this->db->get('payment_slip_ccee');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
