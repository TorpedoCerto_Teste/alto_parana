<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of devec_unity_contract_model
 *
 * @author Rogerio Pellarin
 *
  pk_devec_unity_contract
  fk_devec_unity
  cliqCCEE
  contract_mhw
  contract_value
  status
  created_at
  updated_at

 */
class Devec_unity_contract_model extends CI_Model {

    var $_pk_devec_unity_contract = "";
    var $_fk_devec_unity          = "";
    var $_cliqCCEE                = "";
    var $_contract_mhw            = "";
    var $_contract_value          = "";
    var $_status                  = "";
    var $_created_at              = "";
    var $_updated_at              = "";
    var $_status_active           = 1;
    var $_status_inactive         = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('devec_unity_contract', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_devec_unity_contract', $this->_pk_devec_unity_contract);
        $rec = $this->db->get('devec_unity_contract');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_devec_unity_contract', $this->_pk_devec_unity_contract);
        $this->db->update('devec_unity_contract', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_devec_unity_contract', $this->_pk_devec_unity_contract);
        $q = $this->db->get('devec_unity_contract');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('devec_unity_contract', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_devec_unity_contract', $this->_pk_devec_unity_contract);
            $this->db->update('devec_unity_contract', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_devec_unity_contract', $this->_pk_devec_unity_contract);
        $this->db->update('devec_unity_contract', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('devec_unity_contract');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
