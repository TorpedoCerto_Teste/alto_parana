<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of group_agent_contact_model
 *
 * @author Rogerio Pellarin
 * 
  pk_group_agent_contact
  fk_agent
  fk_contact
  fk_group

 */
class Group_agent_contact_model extends CI_Model {

    var $_pk_group_agent_contact = "";
    var $_fk_agent               = "";
    var $_fk_contact             = "";
    var $_fk_group               = "";
    var $_status                 = "";
    var $_created_at             = "";
    var $_updated_at             = "";
    var $_status_active          = 1;
    var $_status_inactive        = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('group_agent_contact', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_group_agent_contact', $this->_pk_group_agent_contact);
        $rec = $this->db->get('group_agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_group_agent_contact', $this->_pk_group_agent_contact);
        $this->db->update('group_agent_contact', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_group_agent_contact', $this->_pk_group_agent_contact);
        $q                  = $this->db->get('group_agent_contact');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('group_agent_contact', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_group_agent_contact', $this->_pk_group_agent_contact);
            $this->db->update('group_agent_contact', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_fk_contact !== "") {
            $this->db->where('fk_contact', $this->_fk_contact);
        }
        if ($this->_pk_group_agent_contact !== "") {
            $this->db->where('pk_group_agent_contact', $this->_pk_group_agent_contact);
        }
        $this->db->update('group_agent_contact', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        if ($this->_fk_contact !== "") {
            $this->db->where('fk_contact', $this->_fk_contact);
        }
        $rec = $this->db->get('group_agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_agent() {
        $this->db->select(
                array(
                    'group_agent_contact.*',
                    'agent.community_name',
                    'contact.name',
                    'contact.surname',
                    'group.group'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('group_agent_contact.status', $this->_status);
        }
        $this->db->where('group_agent_contact.fk_agent', $this->_fk_agent);
        $this->db->join('agent', 'agent.pk_agent = group_agent_contact.fk_agent');
        $this->db->join('contact', 'contact.pk_contact = group_agent_contact.fk_contact');
        $this->db->join('group', 'group.pk_group = group_agent_contact.fk_group');
        $rec = $this->db->get('group_agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_group() {
        $this->db->select(
                array(
                    'contact.name',
                    'contact.surname',
                    'contact.email',
                    'group_agent_contact.fk_agent'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('group_agent_contact.status', $this->_status);
        }
        if ($this->_fk_group !== "") {
            $this->db->where('group_agent_contact.fk_group', $this->_fk_group);
        }
        $this->db->join('contact', 'contact.pk_contact = group_agent_contact.fk_contact AND contact.status = '.$this->_status_active);
        $rec = $this->db->get('group_agent_contact');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function get_contacts_by_group_and_agent($fk_group, $fk_agent, $fk_unity = 0) {
        $this->db->select([
            'contact.pk_contact'
        ]);
        $this->db->from('group_agent_contact');
        $this->db->join('contact', 'group_agent_contact.fk_contact = contact.pk_contact  AND contact.status = '.$this->_status_active);
        if ($fk_unity) {
            $this->db->join('unity_contact', 'unity_contact.fk_contact = contact.pk_contact AND unity_contact.fk_unity = '.$fk_unity.' AND unity_contact.status = '.$this->_status_active);
        }
        $this->db->where('group_agent_contact.fk_group', $fk_group);
        $this->db->where('group_agent_contact.fk_agent', $fk_agent);
        $this->db->where('group_agent_contact.status', $this->_status_active);
        $rec = $this->db->get();

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
