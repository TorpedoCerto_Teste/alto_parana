<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of business_service_model
 *
 * @author Rogerio Pellarin
 * 
  pk_business_service
  fk_business
  fk_service
  personalized

 */
class Business_service_model extends CI_Model {

    var $_pk_business_service = "";
    var $_fk_business         = "";
    var $_fk_service          = "";
    var $_personalized        = "";
    var $_status              = "";
    var $_created_at          = "";
    var $_updated_at          = "";
    var $_status_active       = 1;
    var $_status_inactive     = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('business_service', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_pk_business_service !== "") {
            $this->db->where('pk_business_service', $this->_pk_business_service);
        }
        if ($this->_fk_business !== "") {
            $this->db->where('fk_business', $this->_fk_business);
        }
        $rec = $this->db->get('business_service');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_business_service', $this->_pk_business_service);
        $this->db->update('business_service', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");

        $this->db->where('fk_business', $this->_fk_business);
        $q = $this->db->get('business_service');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('business_service', $data);
            return $this->db->insert_id();
        }
        else {
            $this->_set($q->result_array());
            unset($data['created_at']);
            $this->db->where('fk_business', $this->_fk_business);
            $this->db->update('business_service', $data);
            return $this->_pk_business_service;
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_business_service', $this->_pk_business_service);
        $this->db->update('business_service', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'business_service.*',
            'service.service'
        ));
        if ($this->_status !== "") {
            $this->db->where('business_service.status', $this->_status);
        }
        $this->db->join('service', 'service.pk_service = business_service.fk_service');
        $rec = $this->db->get('business_service');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
