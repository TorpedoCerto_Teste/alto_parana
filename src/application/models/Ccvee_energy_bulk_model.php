<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Ccvee_energy_bulk_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_energy_bulk
  fk_ccvee_energy
  date
  forecast
  seasonal
  adjusted
  attendance
  losses

 */
class Ccvee_energy_bulk_model extends CI_Model {

    var $_pk_ccvee_energy_bulk = "";
    var $_fk_ccvee_energy      = "";
    var $_date                 = "";
    var $_forecast             = "";
    var $_seasonal             = "";
    var $_adjusted             = "";
    var $_attendance           = "";
    var $_losses               = "";
    var $_status               = "";
    var $_created_at           = "";
    var $_updated_at           = "";
    var $_status_active        = 1;
    var $_status_inactive      = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_energy_bulk', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee_energy_bulk', $this->_pk_ccvee_energy_bulk);
        $rec = $this->db->get('ccvee_energy_bulk');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_energy_bulk', $this->_pk_ccvee_energy_bulk);
        $this->db->update('ccvee_energy_bulk', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_energy_bulk', $this->_pk_ccvee_energy_bulk);
        $q                  = $this->db->get('ccvee_energy_bulk');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_energy_bulk', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_energy_bulk', $this->_pk_ccvee_energy_bulk);
            $this->db->update('ccvee_energy_bulk', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('fk_ccvee_energy', $this->_fk_ccvee_energy);
        $this->db->update('ccvee_energy_bulk', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_fk_ccvee_energy !== "") {
            $this->db->where('fk_ccvee_energy', $this->_fk_ccvee_energy);
        }
        if ($this->_pk_ccvee_energy_bulk !== "") {
            $this->db->where('pk_ccvee_energy_bulk', $this->_pk_ccvee_energy_bulk);
        }
        if ($this->_date !== "") {
            $this->db->where('date', $this->_date);
        }
        $rec = $this->db->get('ccvee_energy_bulk');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
