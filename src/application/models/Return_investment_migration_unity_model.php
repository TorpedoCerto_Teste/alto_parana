<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of return_investment_migration_unity_model
 *
 * @author Rogerio Pellarin
 *
  pk_return_investment_migration_unity
  fk_return_investment_migration
  fk_unity
  total_invested
  month_return
  month_return_percent
  cumulative_return
  cumulative_return_percent
  status
  created_at
  updated_at

 */
class Return_investment_migration_unity_model extends CI_Model {

    var $_pk_return_investment_migration_unity = "";
    var $_fk_return_investment_migration       = "";
    var $_fk_unity                             = "";
    var $_total_invested                       = "";
    var $_month_return                         = "";
    var $_month_return_percent                 = "";
    var $_cumulative_return                    = "";
    var $_cumulative_return_percent            = "";
    var $_status                               = "";
    var $_created_at                           = "";
    var $_updated_at                           = "";
    var $_status_active                        = 1;
    var $_status_inactive                      = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('return_investment_migration_unity', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_return_investment_migration_unity', $this->_pk_return_investment_migration_unity);
        $rec = $this->db->get('return_investment_migration_unity');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_return_investment_migration_unity', $this->_pk_return_investment_migration_unity);
        $this->db->update('return_investment_migration_unity', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_return_investment_migration_unity', $this->_pk_return_investment_migration_unity);
        $q = $this->db->get('return_investment_migration_unity');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('return_investment_migration_unity', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_return_investment_migration_unity', $this->_pk_return_investment_migration_unity);
            $this->db->update('return_investment_migration_unity', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_return_investment_migration_unity', $this->_pk_return_investment_migration_unity);
        $this->db->update('return_investment_migration_unity', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('return_investment_migration_unity');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
