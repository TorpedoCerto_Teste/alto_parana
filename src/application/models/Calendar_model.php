<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar_model extends CI_Model {

    var $_pk_calendar             = "";
    var $_calendar                = "";
    var $_year_reference          = "";
    var $_month_reference         = "";
    var $_fk_calendar_responsible = "";
    var $_event_date              = "";
    var $_fk_agent                = "";
    var $_status                  = "";
    var $_created_at              = "";
    var $_updated_at              = "";
    var $_status_active           = 1;
    var $_status_inactive         = 0;
    var $_status_completed        = 2;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        unset($data['status_completed']);
        $this->db->insert('calendar', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_calendar', $this->_pk_calendar);
        $rec = $this->db->get('calendar');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $data = get_array_from_object($this);
        unset($data['status_completed']);
        $this->db->where('pk_calendar', $this->_pk_calendar);
        $this->db->update('calendar', $data);
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        unset($data['status_completed']);

        $this->db->where('pk_calendar', $this->_pk__calendar);
        $q = $this->db->get('calendar');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('calendar', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_calendar', $this->_pk_calendar);
            $this->db->update('calendar', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_calendar', $this->_pk_calendar);
        $this->db->update('calendar', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(array(
            'calendar.*',
            'calendar_responsible.responsible AS responsible',
            'agent.community_name AS agent'
        ));
        if ($this->_status !== "") {
            $this->db->where('calendar.status', $this->_status);
        }
        if ($this->_event_date !== "") {
            $this->db->where('calendar.event_date', $this->_event_date);
        }
        if ($this->_fk_calendar_responsible !== "") {
            $this->db->where('calendar.fk_calendar_responsible', $this->_fk_calendar_responsible);
        }
        $this->db->where('calendar.status !=', $this->_status_inactive);
        $this->db->join("calendar_responsible", "calendar_responsible.pk_calendar_responsible = calendar.fk_calendar_responsible");
        $this->db->join("agent", "agent.pk_agent = calendar.fk_agent", "LEFT");
        $this->db->group_by(["year_reference", "month_reference", "fk_calendar_responsible", "fk_agent", "event_date"]);
        $rec = $this->db->get('calendar');

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_month_events() {
        $this->db->select(array(
            'calendar.*',
            'calendar_responsible.responsible AS responsible',
            'agent.community_name AS agent'
        ));
        if ($this->_status !== "") {
            $this->db->where('calendar.status', $this->_status);
        }
        $this->db->where('calendar.status !=', $this->_status_inactive);
        $this->db->where('month(calendar.event_date)', date("m", strtotime($this->_event_date)));
        $this->db->where('year(calendar.event_date)', date("Y", strtotime($this->_event_date)));
        $this->db->order_by('calendar.event_date');
        $this->db->join("calendar_responsible", "calendar_responsible.pk_calendar_responsible = calendar.fk_calendar_responsible");
        $this->db->join("agent", "agent.pk_agent = calendar.fk_agent", 'LEFT');
        $rec = $this->db->get('calendar');

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_month_events_by_agent() {
        $this->db->select(array(
            'calendar.*',
            'calendar_responsible.responsible AS responsible',
            'calendar_agent.`status` as c_status'
        ));
        if ($this->_status !== "") {
            $this->db->where('calendar.status', $this->_status);
        }
        $this->db->where('calendar.status !=', $this->_status_inactive);
        $this->db->where('month(calendar.event_date)', date("m", strtotime($this->_event_date)));
        $this->db->where('year(calendar.event_date)', date("Y", strtotime($this->_event_date)));
        $this->db->where('(calendar.fk_agent IS NULL OR calendar.fk_agent = '.$this->session->userdata("fk_agent").")");
        $this->db->order_by('calendar.event_date');
        $this->db->join("calendar_responsible", "calendar_responsible.pk_calendar_responsible = calendar.fk_calendar_responsible");
        $this->db->join("calendar_agent", "calendar_agent.fk_calendar = calendar.pk_calendar and calendar_agent.fk_agent = " . $this->session->userdata("fk_agent"), "LEFT");
        $rec = $this->db->get('calendar');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_by_day() {
        $this->db->select(array(
            'calendar.*',
            'calendar_responsible.responsible AS responsible'
        ));
        if ($this->_status !== "") {
            $this->db->where('calendar.status', $this->_status);
        }
        $this->db->where('calendar.status !=', $this->_status_inactive);
        $this->db->where('day(calendar.event_date)', date("d", strtotime($this->_event_date)));
        $this->db->where('month(calendar.event_date)', date("m", strtotime($this->_event_date)));
        $this->db->where('year(calendar.event_date)', date("Y", strtotime($this->_event_date)));
        $this->db->order_by('calendar.event_date');
        $this->db->join("calendar_responsible", "calendar_responsible.pk_calendar_responsible = calendar.fk_calendar_responsible");
        $rec = $this->db->get('calendar');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
