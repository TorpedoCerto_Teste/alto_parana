<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar_agent_model extends CI_Model {

    var $_pk_calendar_agent = "";
    var $_fk_calendar       = "";
    var $_fk_agent          = "";
    var $_status            = "";
    var $_created_at        = "";
    var $_updated_at        = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('calendar_agent', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_calendar_agent', $this->_pk_calendar_agent);
        $rec = $this->db->get('calendar_agent');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_calendar_agent', $this->_pk_calendar_agent);
        $this->db->update('calendar_agent', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('fk_agent', $this->_fk_agent);
        $this->db->where('fk_calendar', $this->_fk_calendar);
        $q = $this->db->get('calendar_agent');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('calendar_agent', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            // $data['event_date'] = 
            $this->db->where('fk_agent', $this->_fk_agent);
            $this->db->where('fk_calendar', $this->_fk_calendar);
            $this->db->update('calendar_agent', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_calendar_agent', $this->_pk_calendar_agent);
        $this->db->update('calendar_agent', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('calendar_agent');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
