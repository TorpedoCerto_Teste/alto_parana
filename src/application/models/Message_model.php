<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of message_model
 *
 * @author Rogerio Pellarin
 * 
  pk_message
  title
  message
  priority

 */
class Message_model extends CI_Model {

    var $_pk_message      = "";
    var $_title           = "";
    var $_message         = "";
    var $_priority        = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('message', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_message', $this->_pk_message);
        $rec = $this->db->get('message');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_message', $this->_pk_message);
        $this->db->update('message', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);
        $this->db->where('pk_message', $this->_pk_message);
        $q    = $this->db->get('message');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('message', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_message', $this->_pk_message);
            $this->db->update('message', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_message', $this->_pk_message);
        $this->db->update('message', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'message.*',
                    'COUNT(message_agent.pk_message_agent) AS total_sent'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('message.status', $this->_status);
        }
        if ($this->_pk_message !== "") {
            $this->db->where('message.pk_message', $this->_pk_message);
        }
        $this->db->join('message_agent', 'message_agent.fk_message = message.pk_message');
        $this->db->group_by('message.pk_message');
        $rec = $this->db->get('message');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
