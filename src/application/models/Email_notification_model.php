<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of email_notification_model
 *
 * @author Rogerio Pellarin
 * 
  pk_email_notification
  reference
  fk_agent
  fk_contact
  controller


 */
class Email_notification_model extends CI_Model {

    var $_pk_email_notification = "";
    var $_reference             = "";
    var $_fk_agent              = "";
    var $_fk_contact            = "";
    var $_controller            = "";
    var $_status                = "";
    var $_created_at            = "";
    var $_updated_at            = "";
    var $_status_active         = 1;
    var $_status_inactive       = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('email_notification', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_email_notification', $this->_pk_email_notification);
        $rec = $this->db->get('email_notification');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_email_notification', $this->_pk_email_notification);
        $this->db->update('email_notification', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('pk_email_notification', $this->_pk_email_notification);
        $q = $this->db->get('email_notification');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('email_notification', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_email_notification', $this->_pk_email_notification);
            $this->db->update('email_notification', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_email_notification', $this->_pk_email_notification);
        $this->db->update('email_notification', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
            array(
                'email_notification.*',
                'contact.name',
                'contact.surname'
            )
        );
        if ($this->_status !== "") {
            $this->db->where('email_notification.status', $this->_status);
        }
        if ($this->_reference !== "") {
            $this->db->where('email_notification.reference', $this->_reference);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('email_notification.fk_agent', $this->_fk_agent);
        }
        if ($this->_controller !== "") {
            $this->db->where('email_notification.controller', $this->_controller);
        }
        $this->db->join("contact", "contact.pk_contact = email_notification.fk_contact");
        $this->db->order_by("created_at", "DESC");
        $rec = $this->db->get('email_notification');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
