<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of agent_model
 *
 * @author rogeriopellarin
 * 
  pk_agent
  fk_agent_type
  type
  company_name
  community_name
  cnpj
  ie
  postal_code
  address
  number
  complement
  district
  city
  state
  memo
  status
  created_at
  updated_at
 */
class Agent_model extends CI_Model {

    var $_pk_agent        = "";
    var $_fk_agent_type   = "";
    var $_company_name    = "";
    var $_community_name  = "";
    var $_cnpj            = "";
    var $_ie              = "";
    var $_postal_code     = "";
    var $_address         = "";
    var $_number          = "";
    var $_complement      = "";
    var $_district        = "";
    var $_city            = "";
    var $_state           = "";
    var $_memo            = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_fk_user         = "";
    var $_fk_sector       = "";
    var $_status_active   = 1;
    var $_status_inactive = 2;
    var $_status_deleted  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('agent', $data);
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('agent.status', $this->_status);
        }
        $this->db->where('pk_agent', $this->_pk_agent);
        $rec = $this->db->get('agent');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_agent', $this->_pk_agent);
        $this->db->update('agent', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_agent', $this->_pk_agent);
        $this->db->update('agent', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        $this->db->select(
                array(
                    'agent_type.type',
                    'agent.*'
                )
        );
        if ($this->_status !== "") {
            $this->db->where('agent.status', $this->_status);
        }
        $this->db->where('agent.status != ', $this->_status_deleted);
        if (is_array($this->_fk_agent_type) && !empty($this->_fk_agent_type)) {
            $this->db->where_in('agent.fk_agent_type', $this->_fk_agent_type);
        } else if ($this->_fk_agent_type !== "") {
            $this->db->where('agent.fk_agent_type', $this->_fk_agent_type);
        }
        $this->db->join('agent_type', 'agent_type.pk_agent_type = agent.fk_agent_type');
        $rec = $this->db->get('agent');

        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
