<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ccvee_payment_model
 *
 * @author Rogerio Pellarin
 * 
  pk_ccvee_payment
  fk_ccvee_general
  due
  due_day
  due_period
  due_month
  financial_index
  index_readjustment_calculation
  database
  date_adjustment
  date_adjustment_frequency
  adjustment_montly_manually
  price_flexibility
  different_price_period
  price_mwh

 */
class Ccvee_payment_model extends CI_Model {

    var $_pk_ccvee_payment               = "";
    var $_fk_ccvee_general               = "";
    var $_due                            = "";
    var $_due_day                        = "";
    var $_due_period                     = "";
    var $_due_month                      = "";
    var $_financial_index                = "";
    var $_index_readjustment_calculation = "";
    var $_database                       = "";
    var $_date_adjustment                = "";
    var $_date_adjustment_frequency      = "";
    var $_adjustment_montly_manually     = "";
    var $_price_flexibility              = "";
    var $_different_price_period         = "";
    var $_price_mwh                      = "";
    var $_status                         = "";
    var $_created_at                     = "";
    var $_updated_at                     = "";
    var $_status_active                  = 1;
    var $_status_inactive                = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('ccvee_payment', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_ccvee_payment', $this->_pk_ccvee_payment);
        $rec = $this->db->get('ccvee_payment');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function read_by_general() {
        $this->db->where('fk_ccvee_general', $this->_fk_ccvee_general);
        $rec = $this->db->get('ccvee_payment');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {        
        $this->db->where('pk_ccvee_payment', $this->_pk_ccvee_payment);
        $this->db->update('ccvee_payment', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('pk_ccvee_payment', $this->_pk_ccvee_payment);
        $q                  = $this->db->get('ccvee_payment');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('ccvee_payment', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            unset($data['created_at']);
            $this->db->where('pk_ccvee_payment', $this->_pk_ccvee_payment);
            $this->db->update('ccvee_payment', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_ccvee_payment', $this->_pk_ccvee_payment);
        $this->db->update('ccvee_payment', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('ccvee_payment');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
