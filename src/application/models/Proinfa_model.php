<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of proinfa_model
 *
 * @author Rogerio Pellarin
 * 
  pk_proinfa
  fk_unity
  year
  month
  value_mwh
  status
  created_at
  updated_at

 */
class Proinfa_model extends CI_Model {

    var $_pk_proinfa      = "";
    var $_fk_unity        = "";
    var $_year            = "";
    var $_month           = "";
    var $_value_mwh       = "";
    var $_status          = "";
    var $_created_at      = "";
    var $_updated_at      = "";
    var $_status_active   = 1;
    var $_status_inactive = 2;
    var $_status_deleted  = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data = get_array_from_object($this);
        $this->db->insert('proinfa', $data);
        return $this->db->insert_id();
    }

    function read() {
        $this->db->where('pk_proinfa', $this->_pk_proinfa);
        $this->db->where('fk_unity', $this->_fk_unity);
        $rec = $this->db->get('proinfa');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_proinfa', $this->_pk_proinfa);
        $this->db->update('proinfa', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function delete() {
        $data = array(
            'status' => $this->_status_deleted
        );
        $this->db->where('pk_proinfa', $this->_pk_proinfa);
        $this->db->update('proinfa', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        $this->db->where('status !=', $this->_status_deleted);
        $this->db->where_in('fk_unity', $this->_fk_unity);
        $this->db->order_by('year', 'ASC');
        $this->db->order_by('month', 'ASC');
        $rec = $this->db->get('proinfa');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function upsert() {
        $data = get_array_from_object($this);
        $ret  = 0;
        $this->db->where('fk_unity', $this->_fk_unity);
        $this->db->where('year', $this->_year);
        $this->db->where('month', $this->_month);
        $q    = $this->db->get('proinfa');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('proinfa', $data);
            $ret = $this->db->insert_id();
        }
        else {
            unset($data['created_at']);
            $this->db->where('fk_unity', $this->_fk_unity);
            $this->db->where('year', $this->_year);
            $this->db->where('month', $this->_month);
            $this->db->update('proinfa', $data);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
