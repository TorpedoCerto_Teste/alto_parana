<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of energy_closing_model
 *
 * @author Rogerio Pellarin
 *
  pk_energy_closing
  month
  year
  fk_agent
  energy_closing_file
  status
  created_at
  updated_at

 */
class Energy_closing_model extends CI_Model {

    var $_pk_energy_closing   = "";
    var $_month               = "";
    var $_year                = "";
    var $_fk_agent            = "";
    var $_energy_closing_file = "";
    var $_status              = "";
    var $_created_at          = "";
    var $_updated_at          = "";
    var $_status_active       = 1;
    var $_status_inactive     = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $this->db->insert('energy_closing', get_array_from_object($this));
        return $this->db->insert_id();
    }

    function read() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        if ($this->_month !== "") {
            $this->db->where('month', $this->_month);
        }
        if ($this->_year !== "") {
            $this->db->where('year', $this->_year);
        }
        if ($this->_fk_agent !== "") {
            $this->db->where('fk_agent', $this->_fk_agent);
        }
        $this->db->where('pk_energy_closing', $this->_pk_energy_closing);
        $rec = $this->db->get('energy_closing');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            $this->_set($rec->result_array());
            return TRUE;
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_energy_closing', $this->_pk_energy_closing);
        $this->db->update('energy_closing', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data = get_array_from_object($this);

        $this->db->where('month', $this->_month);
        $this->db->where('year', $this->_year);
        $this->db->where('fk_agent', $this->_fk_agent);
        $q = $this->db->get('energy_closing');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('energy_closing', $data);
            $ret = $this->db->insert_id();
            return $ret;
        }
        else {
            $res = $q->result_array();
            $this->_pk_energy_closing = $res[0]['pk_energy_closing'];
            unset($data['created_at']);
            $this->db->where('pk_energy_closing', $this->_pk_energy_closing);
            $this->db->update('energy_closing', $data);
            return $this->db->affected_rows();
        }
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_energy_closing', $this->_pk_energy_closing);
        $this->db->update('energy_closing', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $rec = $this->db->get('energy_closing');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
