<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of bank_charges_model
 *
 * @author Rogerio Pellarin
 * 
  pk_bank_charges
  fk_agent_power_distributor
  date_adjustment
  resolution
  date_resolution
  beginning_term
  end_term
  bank_charges_file
  a1_azul_tusd_kw_p
  a1_azul_tusd_mwh_p
  a1_azul_te_mwh_p_verde
  a1_azul_tusd_kw_fp
  a1_azul_tusd_mwh_fp
  a1_azul_te_mwh_fp_verde
  a1_azul_ape_tusd_kw_p
  a1_azul_ape_tusd_mwh_p
  a1_azul_ape_te_mwh_p_verde
  a1_azul_ape_tusd_kw_fp
  a1_azul_ape_tusd_mwh_fp
  a1_azul_ape_te_mwh_fp_verde
  a2_azul_tusd_kw_p
  a2_azul_tusd_mwh_p
  a2_azul_te_mwh_p_verde
  a2_azul_tusd_kw_fp
  a2_azul_tusd_mwh_fp
  a2_azul_te_mwh_fp_verde
  a2_azul_ape_tusd_kw_p
  a2_azul_ape_tusd_mwh_p
  a2_azul_ape_te_mwh_p_verde
  a2_azul_ape_tusd_kw_fp
  a2_azul_ape_tusd_mwh_fp
  a2_azul_ape_te_mwh_fp_verde
  a3_azul_tusd_kw_p
  a3_azul_tusd_mwh_p
  a3_azul_te_mwh_p_verde
  a3_azul_tusd_kw_fp
  a3_azul_tusd_mwh_fp
  a3_azul_te_mwh_fp_verde
  a3_azul_ape_tusd_kw_p
  a3_azul_ape_tusd_mwh_p
  a3_azul_ape_te_mwh_p_verde
  a3_azul_ape_tusd_kw_fp
  a3_azul_ape_tusd_mwh_fp
  a3_azul_ape_te_mwh_fp_verde
  a3a_azul_tusd_kw_p
  a3a_azul_tusd_mwh_p
  a3a_azul_te_mwh_p_verde
  a3a_azul_tusd_kw_fp
  a3a_azul_tusd_mwh_fp
  a3a_azul_te_mwh_fp_verde
  a3a_azul_ape_tusd_kw_p
  a3a_azul_ape_tusd_mwh_p
  a3a_azul_ape_te_mwh_p_verde
  a3a_azul_ape_tusd_kw_fp
  a3a_azul_ape_tusd_mwh_fp
  a3a_azul_ape_te_mwh_fp_verde
  a3a_verde_tusd_kw_na
  a3a_verde_tusd_mwh_p
  a3a_verde_te_mwh_p_verde
  a3a_verde_tusd_mwh_fp
  a3a_verde_te_mwh_fp_verde
  a3a_verde_ape_tusd_kw_na
  a3a_verde_ape_tusd_mwh_p
  a3a_verde_ape_te_mwh_p_verde
  a3a_verde_ape_tusd_mwh_fp
  a3a_verde_ape_te_mwh_fp_verde
  a4_azul_tusd_kw_p
  a4_azul_tusd_mwh_p
  a4_azul_te_mwh_p_verde
  a4_azul_tusd_kw_fp
  a4_azul_tusd_mwh_fp
  a4_azul_te_mwh_fp_verde
  a4_azul_ape_tusd_kw_p
  a4_azul_ape_tusd_mwh_p
  a4_azul_ape_te_mwh_p_verde
  a4_azul_ape_tusd_kw_fp
  a4_azul_ape_tusd_mwh_fp
  a4_azul_ape_te_mwh_fp_verde
  a4_verde_tusd_kw_na
  a4_verde_tusd_mwh_p
  a4_verde_te_mwh_p_verde
  a4_verde_tusd_mwh_fp
  a4_verde_te_mwh_fp_verde
  a4_verde_ape_tusd_kw_na
  a4_verde_ape_tusd_mwh_p
  a4_verde_ape_te_mwh_p_verde
  a4_verde_ape_tusd_mwh_fp
  a4_verde_ape_te_mwh_fp_verde
  status
  created_at
  updated_at
 */
class Bank_charges_model extends CI_Model {

    var $_pk_bank_charges               = "";
    var $_fk_agent_power_distributor    = "";
    var $_date_adjustment               = "";
    var $_resolution                    = "";
    var $_date_resolution               = "";
    var $_beginning_term                = "";
    var $_end_term                      = "";
    var $_bank_charges_file             = "";
    var $_a1_azul_tusd_kw_p             = "";
    var $_a1_azul_tusd_mwh_p            = "";
    var $_a1_azul_te_mwh_p_verde        = "";
    var $_a1_azul_tusd_kw_fp            = "";
    var $_a1_azul_tusd_mwh_fp           = "";
    var $_a1_azul_te_mwh_fp_verde       = "";
    var $_a1_azul_ape_tusd_kw_p         = "";
    var $_a1_azul_ape_tusd_mwh_p        = "";
    var $_a1_azul_ape_te_mwh_p_verde    = "";
    var $_a1_azul_ape_tusd_kw_fp        = "";
    var $_a1_azul_ape_tusd_mwh_fp       = "";
    var $_a1_azul_ape_te_mwh_fp_verde   = "";
    var $_a2_azul_tusd_kw_p             = "";
    var $_a2_azul_tusd_mwh_p            = "";
    var $_a2_azul_te_mwh_p_verde        = "";
    var $_a2_azul_tusd_kw_fp            = "";
    var $_a2_azul_tusd_mwh_fp           = "";
    var $_a2_azul_te_mwh_fp_verde       = "";
    var $_a2_azul_ape_tusd_kw_p         = "";
    var $_a2_azul_ape_tusd_mwh_p        = "";
    var $_a2_azul_ape_te_mwh_p_verde    = "";
    var $_a2_azul_ape_tusd_kw_fp        = "";
    var $_a2_azul_ape_tusd_mwh_fp       = "";
    var $_a2_azul_ape_te_mwh_fp_verde   = "";
    var $_a3_azul_tusd_kw_p             = "";
    var $_a3_azul_tusd_mwh_p            = "";
    var $_a3_azul_te_mwh_p_verde        = "";
    var $_a3_azul_tusd_kw_fp            = "";
    var $_a3_azul_tusd_mwh_fp           = "";
    var $_a3_azul_te_mwh_fp_verde       = "";
    var $_a3_azul_ape_tusd_kw_p         = "";
    var $_a3_azul_ape_tusd_mwh_p        = "";
    var $_a3_azul_ape_te_mwh_p_verde    = "";
    var $_a3_azul_ape_tusd_kw_fp        = "";
    var $_a3_azul_ape_tusd_mwh_fp       = "";
    var $_a3_azul_ape_te_mwh_fp_verde   = "";
    var $_a3a_azul_tusd_kw_p            = "";
    var $_a3a_azul_tusd_mwh_p           = "";
    var $_a3a_azul_te_mwh_p_verde       = "";
    var $_a3a_azul_tusd_kw_fp           = "";
    var $_a3a_azul_tusd_mwh_fp          = "";
    var $_a3a_azul_te_mwh_fp_verde      = "";
    var $_a3a_azul_ape_tusd_kw_p        = "";
    var $_a3a_azul_ape_tusd_mwh_p       = "";
    var $_a3a_azul_ape_te_mwh_p_verde   = "";
    var $_a3a_azul_ape_tusd_kw_fp       = "";
    var $_a3a_azul_ape_tusd_mwh_fp      = "";
    var $_a3a_azul_ape_te_mwh_fp_verde  = "";
    var $_a3a_verde_tusd_kw_na          = "";
    var $_a3a_verde_tusd_mwh_p          = "";
    var $_a3a_verde_te_mwh_p_verde      = "";
    var $_a3a_verde_tusd_mwh_fp         = "";
    var $_a3a_verde_te_mwh_fp_verde     = "";
    var $_a3a_verde_ape_tusd_kw_na      = "";
    var $_a3a_verde_ape_tusd_mwh_p      = "";
    var $_a3a_verde_ape_te_mwh_p_verde  = "";
    var $_a3a_verde_ape_tusd_mwh_fp     = "";
    var $_a3a_verde_ape_te_mwh_fp_verde = "";
    var $_a4_azul_tusd_kw_p             = "";
    var $_a4_azul_tusd_mwh_p            = "";
    var $_a4_azul_te_mwh_p_verde        = "";
    var $_a4_azul_tusd_kw_fp            = "";
    var $_a4_azul_tusd_mwh_fp           = "";
    var $_a4_azul_te_mwh_fp_verde       = "";
    var $_a4_azul_ape_tusd_kw_p         = "";
    var $_a4_azul_ape_tusd_mwh_p        = "";
    var $_a4_azul_ape_te_mwh_p_verde    = "";
    var $_a4_azul_ape_tusd_kw_fp        = "";
    var $_a4_azul_ape_tusd_mwh_fp       = "";
    var $_a4_azul_ape_te_mwh_fp_verde   = "";
    var $_a4_verde_tusd_kw_na           = "";
    var $_a4_verde_tusd_mwh_p           = "";
    var $_a4_verde_te_mwh_p_verde       = "";
    var $_a4_verde_tusd_mwh_fp          = "";
    var $_a4_verde_te_mwh_fp_verde      = "";
    var $_a4_verde_ape_tusd_mwh_p       = "";
    var $_a4_verde_ape_tusd_kw_na       = "";
    var $_a4_verde_ape_te_mwh_p_verde   = "";
    var $_a4_verde_ape_tusd_mwh_fp      = "";
    var $_a4_verde_ape_te_mwh_fp_verde  = "";
    var $_status                        = "";
    var $_created_at                    = "";
    var $_updated_at                    = "";
    var $_status_active                 = 1;
    var $_status_inactive               = 0;

    function __construct() {
        parent::__construct();
    }

    function create() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->insert('bank_charges', $data);
        return $this->db->insert_id();
    }

    function read($return_array = 0) {
        $this->db->where('pk_bank_charges', $this->_pk_bank_charges);
        $rec = $this->db->get('bank_charges');
        if ((isset($rec->num_rows) && $rec->num_rows == 1) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows == 1)) {
            if (!$return_array) {
                $this->_set($rec->result_array());
                return TRUE;
            }
            else {
                $ret = $rec->result_array();
                return $ret[0];
            }
        }
        return FALSE;
    }

    function update() {
        $this->db->where('pk_bank_charges', $this->_pk_bank_charges);
        $this->db->update('bank_charges', get_array_from_object($this));
        return $this->db->affected_rows();
    }

    function upsert() {
        $data               = get_array_from_object($this);
        $data['created_at'] = date("Y-m-d H:m:s");
        $this->db->where('resolution', $this->_resolution);
        $q                  = $this->db->get('bank_charges');

        if ($q->result_id->num_rows == 0) {
            $this->db->insert('bank_charges', $data);
            $ret = $this->db->insert_id();
        }
        else {
            unset($data['created_at']);
            $this->db->where('fk_agent_power_distributor', $this->_fk_agent_power_distributor);
            $this->db->where('resolution', $this->_resolution);
            $this->db->update('bank_charges', $data);
            $rec = $q->result_array();
            $ret = $rec[0]['pk_bank_charges'];
        }
        return $ret;
    }

    function delete() {
        $data = array(
            'status' => $this->_status_inactive
        );
        $this->db->where('pk_bank_charges', $this->_pk_bank_charges);
        $this->db->update('bank_charges', $data);
        return $this->db->affected_rows();
    }

    function fetch() {
        if ($this->_status !== "") {
            $this->db->where('status', $this->_status);
        }
        $this->db->where('fk_agent_power_distributor', $this->_fk_agent_power_distributor);
        $rec = $this->db->get('bank_charges');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    function fetch_resolution() {
        $this->db->select(
                array(
                    'pk_bank_charges',
                    'resolution',
                    "DATE_FORMAT(date_resolution, '%d/%m/%Y') as date_resolution",
                    "DATE_FORMAT(beginning_term, '%d/%m/%Y') as beginning_term",
                    "DATE_FORMAT(end_term, '%d/%m/%Y') as end_term"
                )
        );
        $this->db->where('fk_agent_power_distributor', $this->_fk_agent_power_distributor);
        $rec = $this->db->get('bank_charges');
        if ((isset($rec->num_rows) && $rec->num_rows > 0) || (isset($rec->result_id->num_rows) && $rec->result_id->num_rows >= 1)) {
            return $rec->result_array();
        }
        return FALSE;
    }

    private function _set($ret) {
        foreach ($ret[0] as $key => $value) {
            if ($value !== "") {
                $propertyName = '_' . $key;
                $this->{$propertyName} = $value;
            }
        }
    }

}
