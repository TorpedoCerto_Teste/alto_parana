<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Icms extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Icms_model');
    }

    function index() {
        $this->Icms_model->_status = $this->Icms_model->_status_active;
        $data['list']              = $this->Icms_model->fetch();
        $data['js_include']        = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']       = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']      = 'icms/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Icms_model->create();
                if ($create) {
                    redirect('icms');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/icms/create.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >';
        $data['main_content'] = 'icms/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('icms/index');
        }
        $this->Icms_model->_pk_icms = $this->uri->segment(3);
        $read                       = $this->Icms_model->read();
        if (!$read) {
            redirect('icms/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Icms_model->update();
                redirect('icms/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/icms/create.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >';
        $data['main_content'] = 'icms/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_icms') > 0) {
            $this->Icms_model->_pk_icms = $this->input->post('pk_icms');
            $this->Icms_model->delete();
        }
        redirect('icms/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('start_date', 'start_date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'end_date', 'trim');
        $this->form_validation->set_rules('percent_industry', 'percent_industry', 'trim|required');
        $this->form_validation->set_rules('percent_commercial', 'percent_commercial', 'trim|required');
    }

    private function _fill_model() {
        $this->Icms_model->_state      = mb_strtoupper($this->input->post('state'), 'UTF-8');
        $this->Icms_model->_start_date = human_date_to_date($this->input->post('start_date'));
        if (trim($this->input->post('end_date')) != "") {
            $this->Icms_model->_end_date = human_date_to_date($this->input->post('end_date'));
        }
        $this->Icms_model->_percent_industry   = number_format(str_replace(',', '.', $this->input->post('percent_industry')), 2, '.', '');
        $this->Icms_model->_percent_commercial = number_format(str_replace(',', '.', $this->input->post('percent_commercial')), 2, '.', '');
        $this->Icms_model->_status             = $this->Icms_model->_status_active;
    }

}
