<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_payment extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        if ($this->uri->segment(3, 0) > 0) {
            $this->load->model('Ccvee_general_model');
            $this->Ccvee_general_model->_pk_ccvee_general = $this->uri->segment(3);
            $read                                         = $this->Ccvee_general_model->read();
            if (!$read) {
                redirect('ccvee_general');
            }
        }
        else {
            redirect('ccvee_general');
        }

        $this->load->model('Ccvee_payment_model');
    }

    function index() {
        $this->Ccvee_payment_model->_status = $this->Ccvee_payment_model->_status_active;
        $data['list']                       = $this->Ccvee_payment_model->fetch();
        $data['js_include']                 = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']                = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']               = 'ccvee_payment/index';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $this->load->model("Ccvee_payment_price_model");
        $this->Ccvee_payment_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $read                                         = $this->Ccvee_payment_model->read_by_general();

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('due', 'due', 'trim');
            $this->form_validation->set_rules('due_day', 'due_day', 'trim');
            $this->form_validation->set_rules('due_period', 'due_period', 'trim');
            $this->form_validation->set_rules('due_month', 'due_month', 'trim');
            $this->form_validation->set_rules('financial_index', 'financial_index', 'trim');
            $this->form_validation->set_rules('index_readjustment_calculation', 'index_readjustment_calculation', 'trim');
            $this->form_validation->set_rules('database', 'database', 'trim');
            $this->form_validation->set_rules('date_adjustment', 'date_adjustment', 'trim');
            $this->form_validation->set_rules('date_adjustment_frequency', 'date_adjustment_frequency', 'trim');
            $this->form_validation->set_rules('price_flexibility', 'price_flexibility', 'trim');
            $this->form_validation->set_rules('different_price_period', 'different_price_period', 'trim');
            if ($this->form_validation->run() == TRUE) {
                $this->Ccvee_payment_model->_due                            = mb_strtolower($this->input->post('due'), 'UTF-8');
                $this->Ccvee_payment_model->_due_day                        = $this->input->post('due_day');
                $this->Ccvee_payment_model->_due_period                     = mb_strtoupper($this->input->post('due_period'), 'UTF-8');
                $this->Ccvee_payment_model->_due_month                      = $this->input->post('due_month');
                $this->Ccvee_payment_model->_financial_index                = mb_strtoupper($this->input->post('financial_index'), 'UTF-8');
                $this->Ccvee_payment_model->_index_readjustment_calculation = $this->input->post('index_readjustment_calculation');
                $this->Ccvee_payment_model->_database                       = human_date_to_date($this->input->post('database'));
                $this->Ccvee_payment_model->_adjustment_montly_manually     = $this->input->post('first_adjustment');
                $this->Ccvee_payment_model->_date_adjustment                = human_date_to_date($this->input->post('date_adjustment'));
                $this->Ccvee_payment_model->_date_adjustment_frequency      = $this->input->post('date_adjustment_frequency');
                $this->Ccvee_payment_model->_price_flexibility              = $this->input->post('price_flexibility');
                $this->Ccvee_payment_model->_different_price_period         = $this->input->post('different_price_period') == 1 ? 1 : 0;
                $this->Ccvee_payment_model->_price_mwh                      = $this->input->post('different_price_period') == 0 ? number_format((str_replace(",", ".", $this->input->post('price_mwh'))), 4, ".", "") : 0;
                $update                                                     = $this->Ccvee_payment_model->update();

                //atualizar os preços
                if ($this->input->post('different_price_period') == 1) {
                    //inativar todos os preços existentes em ccvee_payment_prices
                    $this->Ccvee_payment_price_model->_fk_ccvee_payment = $this->Ccvee_payment_model->_pk_ccvee_payment;
                    $this->Ccvee_payment_price_model->delete();
                    //executar o loop dos preços para inserir na base
                    $payment_prices                                     = $this->input->post("payment_prices");
                    foreach ($payment_prices as $price) {
                        $this->Ccvee_payment_price_model->_period         = human_date_to_date("01/" . $price['period']);
                        $this->Ccvee_payment_price_model->_provided_base  = number_format((str_replace(",", ".", $price['provided_base'])), 4, ".", "");
                        $this->Ccvee_payment_price_model->_fixed          = number_format((str_replace(",", ".", $price['fixed'])), 4, ".", "");
                        $this->Ccvee_payment_price_model->_manual_fixed   = number_format((str_replace(",", ".", $price['manual_fixed'])), 4, ".", "");
                        $this->Ccvee_payment_price_model->_provided_fixed = number_format((str_replace(",", ".", $price['provided_fixed'])), 4, ".", "");
                        $this->Ccvee_payment_price_model->_point_monthly  = isset($price['point_monthly']) ? 1 : 0;
                        $this->Ccvee_payment_price_model->_status         = $this->Ccvee_payment_price_model->_status_active;
                        $this->Ccvee_payment_price_model->create();
                    }
                }
            }
        }
        redirect('ccvee_general/view/' . $this->Ccvee_general_model->_pk_ccvee_general . '/pagamentos');
    }

    function delete() {
        if ($this->input->post('pk_ccvee_payment') > 0) {
            $this->Ccvee_payment_model->_pk_ccvee_payment = $this->input->post('pk_ccvee_payment');
            $this->Ccvee_payment_model->delete();
        }
        redirect('ccvee_payment/index');
    }

    //retorna a quantidade de meses que haverão fornecimento de energia, para 
    //calcular os preços diferenciados de pagamentos
    function ajax_get_supply_period() {
        $total_months                                = 0;
        $return                                      = array();
        $this->load->model("Ccvee_energy_model");
        $this->Ccvee_energy_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $read                                        = $this->Ccvee_energy_model->read();
        if ($read) {

            $supply_start = new DateTime($this->Ccvee_energy_model->_supply_start);
            $supply_end   = new DateTime($this->Ccvee_energy_model->_supply_end);

            $total_months = (int) ($supply_start->diff($supply_end)->m + ($supply_start->diff($supply_end)->y * 12)) + 1;

            $count = 0;
            for ($i = 0; $i < $total_months; $i++) {
                $new_date              = $supply_start;
                $new_date->add(new DateInterval('P' . $count . 'M'));
                $return[$i]['periodo'] = $new_date->format('m/Y');
                $return[$i]['checked'] = $this->_adjustment_checkboxes($new_date->format('m'));
                $count                 = 1;
            }
        }

        print_r(json_encode($return));
    }

    private function _adjustment_checkboxes($month) {
        //pega as variáveis do POST para fazer o tratamento de checkboxes na tabela
        $first_adjustment          = (int) $this->input->post('first_adjustment'); //0 = inserir data, 1 = apontar manualmente (este não necessita de intervenção para marcar os checkboxes)
        $date_adjustment           = human_date_to_date($this->input->post('date_adjustment'));
        $date_adjustment_frequency = $this->input->post('date_adjustment_frequency'); //12em12 ou todoJaneiro

        if ($first_adjustment == 0) {
            $reference_month = 0;
            if ($date_adjustment_frequency == "12em12") {
                $reference_month = date("m", strtotime($date_adjustment));
            }
            elseif ($date_adjustment_frequency == "todoJaneiro") {
                $reference_month = 1;
            }

            if ($reference_month > 0) {
                if ($reference_month * 1 == $month * 1) {
                    return 1;
                }
            }
        }
        return 0;
    }

    
    
}
