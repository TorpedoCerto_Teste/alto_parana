<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Calendar_model');
    }

    function index() {
        $config['day_type'] = 'long';
        $config['template'] = '
            {table_open}<table class="calendar">{/table_open}
            {week_day_cell}<th class="day_header">{week_day}</th>{/week_day_cell}
            {cal_cell_content}<span class="day_listing">{day}</span>{content}&nbsp;{/cal_cell_content}
            {cal_cell_content_today}<div class="today"><span class="day_listing">{day}</span>&bull; {content}</div>{/cal_cell_content_today}
            {cal_cell_no_content}<span class="day_listing">{day}</span>&nbsp;{/cal_cell_no_content}
            {cal_cell_no_content_today}<div class="today"><span class="day_listing">{day}</span></div>{/cal_cell_no_content_today}
        ';
        $this->load->library('calendar', $config);

        //pega os responsáveis
        $data['calendar_responsibles'] = $this->_process_calendar_responsible();
        $data['colors']                = $this->config->item("colors");

        //pega os eventos
        $this->Calendar_model->_event_date = $this->input->post('event_date') != "" ? $this->input->post('event_date') : date("Y-m-d");
        $data['month_events']              = $this->Calendar_model->fetch_month_events();

        $calendar_events  = $this->_process_calendar_events($data['month_events']);
        $data['calendar'] = $this->calendar->generate(date("Y", strtotime($this->Calendar_model->_event_date)), date("m", strtotime($this->Calendar_model->_event_date)), $calendar_events);

        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/data-table-init.js"></script>
            <script src="' . base_url() . 'web/js/calendar/index.js"></script> ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'web/css/calendar/index.css" rel="stylesheet">
        ';
        $data['main_content'] = 'calendar/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Calendar_model->_created_at = date("Y-m-d H:i:s");
                $create                            = $this->Calendar_model->create();
                $create                            = true;
                if ($create) {
                    $this->_send_mail_create();
                    redirect('calendar/index');
                }
            }
            $data['error'] = true;
        }

        //pega os responsáveis
        $data['calendar_responsibles'] = $this->_process_calendar_responsible();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'web/js/calendar/create.js"></script>';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'calendar/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('calendar/index');
        }
        $this->Calendar_model->_pk_calendar = $this->uri->segment(3);
        $read                               = $this->Calendar_model->read();
        if (!$read) {
            redirect('calendar/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Calendar_model->update();
                redirect('calendar/index');
            }
            $data['error'] = true;
        }
        //pega os responsáveis
        $data['calendar_responsibles'] = $this->_process_calendar_responsible();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'web/js/calendar/create.js"></script>';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'calendar/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('calendar/index');
        }
        $this->Calendar_model->_pk_calendar = $this->uri->segment(3);
        $read                               = $this->Calendar_model->read();
        if (!$read) {
            redirect('calendar/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Calendar_model->upsert();
                redirect('calendar/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar/upsert';
        $this->load->view('template/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_calendar') > 0) {
            $this->Calendar_model->_pk_calendar = $this->input->post('pk_calendar');
            $this->Calendar_model->delete();
        }
        redirect('calendar');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('calendar', 'calendar', 'trim|required');
        $this->form_validation->set_rules('year_reference', 'year_reference', 'trim|required');
        $this->form_validation->set_rules('month_reference', 'month_reference', 'trim|required');
        $this->form_validation->set_rules('fk_calendar_responsible', 'fk_calendar_responsible', 'trim|required');
        $this->form_validation->set_rules('event_date', 'event_date', 'trim|required');
    }

    private function _fill_model() {
        $this->Calendar_model->_calendar                = mb_strtoupper($this->input->post('calendar'), 'UTF-8');
        $this->Calendar_model->_year_reference          = preg_replace("([^\d]*)", "", $this->input->post('year_reference'));
        $this->Calendar_model->_month_reference         = preg_replace("([^\d]*)", "", $this->input->post('month_reference'));
        $this->Calendar_model->_fk_calendar_responsible = preg_replace("([^\d]*)", "", $this->input->post('fk_calendar_responsible'));
        $this->Calendar_model->_event_date              = human_date_to_date($this->input->post('event_date'));
    }

    private function _process_calendar_responsible() {
        $this->load->model("Calendar_responsible_model");
        $this->Calendar_responsible_model->_status = $this->Calendar_responsible_model->_status_active;
        return $this->Calendar_responsible_model->fetch();
    }

    private function _process_calendar_events($month_events) {
        $colors = $this->config->item("colors");
        $return = array();
        if ($month_events) {
            foreach ($month_events as $event) {
                $return[date("j", strtotime($event['event_date']))] = "";
            }
            foreach ($month_events as $event) {
                $status                                             = $event['status'] == 2 ? "checked" : "";
                $agent = $event['agent'] ? '<b>Agente: </b>' . $event['agent'] . '<br/>' : ""; 
                $return[date("j", strtotime($event['event_date']))] .= '
                    <a  class="btn btn-' . $colors[$event['fk_calendar_responsible']] . ' btn-sm popovers" 
                        data-original-title="' . date_to_human_date($event['event_date']) . '" 
                        data-content="<b>Evento: </b>' . $event['calendar'] . '<br/>
                        '.$agent.'
                                      <b>Ref: </b>' . $event['month_reference'] . '/' . $event['year_reference'] . '" 
                        data-placement="top" 
                        data-trigger="hover" 
                        data-html="true">
                        <input  type="checkbox" 
                                class="chk" 
                                id="chk_c_' . $event['pk_calendar'] . '" 
                                value="' . $event['pk_calendar'] . '" ' . $status . ' />
                    </a>&nbsp;';
            }
        }
        return $return;
    }

    public function ajax_set_done() {
        $this->disableLayout                = TRUE;
        $this->output->set_content_type('application/json');
        $this->Calendar_model->_pk_calendar = $this->input->post("pk_calendar");
        $this->Calendar_model->read();
        $this->Calendar_model->_status      = $this->input->post("check") == 1 ? $this->Calendar_model->_status_completed : $this->Calendar_model->_status_active;
        $this->Calendar_model->update();
    }

    private function _send_mail_create() {
        $contacts = false;
        switch ($this->Calendar_model->_fk_calendar_responsible) {
            case 1: //CLIENTE
                //buscar os dados dos contatos do agente
                $this->load->model("Contact_model");
                $this->Contact_model->_status = $this->Contact_model->_status_active;
                $contacts                     = $this->Contact_model->fetch();
                break;
            case 2: //APE
                $contacts[]['email']          = 'comercial@altoparanaenergia.com.br';
                $contacts[]['email']          = 'comercial@altoparanaenergia.com';
                $contacts[]['email']          = 'rogerio@torpedocerto.com.br';
                break;
            case 3: //CCEE
                break;
        }


        if ($contacts) {
            $this->load->config('email_config');
            foreach ($contacts as $contact) {
                $post['emails'] = !$this->config->item('email_debug') && $contact['email'] != "" ? $contact['email'] : join(",",$this->config->item('to'));
                $post['subject'] = $this->Calendar_model->_calendar;
                $this->load->clear_vars();
                $msg = $this->load->view('calendar/email/create', array(), true);
                $post['message'] = base64_encode($msg);
        
                $pymail = pymail($post);
            }
        }
        
    }

}
