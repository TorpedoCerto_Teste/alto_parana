<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Odc_record extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Odc_record_model');
    }

    function index() {
        //lista de medidores
        $data['gauges'] = $this->_process_gauge();
        $data['list']   = false;

        if ($this->input->post("filter") == "true") {
            $this->_validate_filter();
            if ($this->form_validation->run() == TRUE) {
                $this->Odc_record_model->_status      = $this->Odc_record_model->_status_active;
                $this->Odc_record_model->_record_date = $this->input->post('year') . "-" . $this->input->post('month') . "-01 00:00:00";
                $this->Odc_record_model->_fk_gauge    = $this->input->post('fk_gauge');
                $data['list']                           = $this->Odc_record_model->fetch_by_report();
                //gráfico
                $data['graphic']                        = $this->_process_graphic($data['list']);
            }
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script src="' . base_url() . 'web/js/odc_record/index.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'odc_record/index';
        $this->load->view('includes/template', $data);

    }
    
    function upload() {
        $this->load->helper('directory');
        $directory      = './uploads/odc_record/';
        $data['upload']['odc_record'] = directory_map($directory);
        $directory      = './uploads/ccee_odc/';
        $data['upload']['ccee_odc'] = directory_map($directory);

        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'js/dropzone.js"></script>
            <script src="' . base_url() . 'web/js/odc_record/upload.js?'. microtime().'"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dropzone.css" rel="stylesheet">    
            ';
        $data['main_content'] = 'odc_record/upload';
        $this->load->view('includes/template', $data);
    }

    function dropzone() {
        $config['upload_path']   = './uploads/odc_record/';
        $config['allowed_types'] = '*';
        $config['max_size']      = 800000;
        $config['file_name']     = date("YmdHis");
        $config['file_name']     .= $this->input->post('rewrite_way2_data') == "true" ? "_way2" : "";
        $config['file_name']     .= $this->input->post('rewrite_scde_data') == "true" ? "_scde" : "";

        //odc_module_data
        if ($this->input->post('odc_module_data') !== null && $this->input->post('odc_module_data') == "true") {
            $config['upload_path']   = './uploads/ccee_odc/';
            $config['file_name']     .= "_ccee_odc";
        }
        
        check_dir_exists($config['upload_path']);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $return = array(
                'error'   => true,
                'message' => strip_tags($this->upload->display_errors()));
        }
        else {
            $return = array(
                'error'   => false,
                'message' => $this->upload->data());
        }
        print json_encode($return);
    }

    function delete_file() {
        if ($this->input->post('delete') == 'true') {
            if (stristr($this->input->post('file_to_delete'), 'ccee_odc')) {
                unlink('./uploads/ccee_odc/' . $this->input->post('file_to_delete'));   
            } else {
                unlink('./uploads/odc_record/' . $this->input->post('file_to_delete'));
            }
        }
        redirect('odc_record/upload/');
    }    
    
    private function _process_gauge() {
        $return                     = false;
        $this->load->model("Gauge_model");
        $this->Gauge_model->_status = $this->Gauge_model->_status_active;
        $gauges                     = $this->Gauge_model->fetch();
        if ($gauges) {
            foreach ($gauges as $gauge) {
                $return[$gauge['fk_unity']]['community_name'] = $gauge['community_name'];
                $return[$gauge['fk_unity']]['gauges'][]       = $gauge;
            }
        }
        return $return;
    }

    private function _process_graphic($records) {
        $return = false;
        if ($records) {

            //colunas
            $col1            = array();
            $col1["id"]      = "";
            $col1["label"]   = "Dia";
            $col1["pattern"] = "";
            $col1["type"]    = "string";

            $col2            = array();
            $col2["id"]      = "";
            $col2["label"]   = "Consumo";
            $col2["pattern"] = "";
            $col2["type"]    = "number";

            $cols = array($col1, $col2);

            $rows = array();
            for ($i = 1; $i <= date("t", strtotime($this->Odc_record_model->_record_date)); $i++) {
                $cel0["v"] = $i;
                $cel1["v"] = 0;
                $row0["c"] = array($cel0, $cel1);
                $rows[$i]  = $row0;
            }

            foreach ($records as $record) {
                $rows[date("j", strtotime($record['record_date']))]["c"][1]["v"] += $record['value'];
            }
            
            $rows = array_values($rows);

            $return = array("cols" => $cols, "rows" => $rows);
        }
        return (json_encode($return));
    }    

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('odc_record/index');
        }
        $this->Odc_record_model->_pk_odc_record = $this->uri->segment(3);
        $read                                   = $this->Odc_record_model->read();
        if (!$read) {
            redirect('odc_record/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Odc_record_model->upsert();
                redirect('odc_record/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'odc_record/update';
        $this->load->view('includes/template', $data);
    }

    private function _validate_filter() {
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('fk_gauge', 'fk_gauge', 'trim|required');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('', '', 'trim|required');
    }

    private function _fill_model() {
        $this->Odc_record_model->_       = $this->input->post('');
        $this->Odc_record_model->_status = $this->Odc_record_model->_status_active;
    }

}
