<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Energy_closing_unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Energy_closing_unity_model');
    }

    function index() {
        $this->Energy_closing_unity_model->_status = $this->Energy_closing_unity_model->_status_active;
        $data['list']                              = $this->Energy_closing_unity_model->fetch();
        $data['js_include']                        = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                       = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                      = 'energy_closing_unity/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Energy_closing_unity_model->_created_at = date("Y-m-d H:i:s");
                $create                                        = $this->Energy_closing_unity_model->create();
                if ($create) {
                    redirect('energy_closing_unity/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_closing_unity/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('energy_closing_unity/index');
        }
        $this->Energy_closing_unity_model->_pk_energy_closing_unity = $this->uri->segment(3);
        $read                                                       = $this->Energy_closing_unity_model->read();
        if (!$read) {
            redirect('energy_closing_unity/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Energy_closing_unity_model->update();
                redirect('energy_closing_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_closing_unity/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('energy_closing_unity/index');
        }
        $this->Energy_closing_unity_model->_pk_energy_closing_unity = $this->uri->segment(3);
        $read                                                       = $this->Energy_closing_unity_model->read();
        if (!$read) {
            redirect('energy_closing_unity/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Energy_closing_unity_model->upsert();
                redirect('energy_closing_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_closing_unity/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_energy_closing_unity') > 0) {
            $this->Energy_closing_unity_model->_pk_energy_closing_unity = $this->input->post('pk_energy_closing_unity');
            $this->Energy_closing_unity_model->delete();
        }
        redirect('energy_closing_unity/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_energy_closing', 'fk_energy_closing', 'trim|required');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('consumption', 'consumption', 'trim|required');
        $this->form_validation->set_rules('loosing', 'loosing', 'trim|required');
        $this->form_validation->set_rules('proinfa', 'proinfa', 'trim|required');
        $this->form_validation->set_rules('total', 'total', 'trim|required');
        $this->form_validation->set_rules('acl_average', 'acl_average', 'trim|required');
        $this->form_validation->set_rules('energy_closing_unity_file', 'energy_closing_unity_file', 'trim|required');
    }

    private function _fill_model() {
        $this->Energy_closing_unity_model->_fk_energy_closing         = $this->input->post('fk_energy_closing');
        $this->Energy_closing_unity_model->_fk_unity                  = $this->input->post('fk_unity');
        $this->Energy_closing_unity_model->_consumption               = $this->input->post('consumption');
        $this->Energy_closing_unity_model->_loosing                   = $this->input->post('loosing');
        $this->Energy_closing_unity_model->_proinfa                   = $this->input->post('proinfa');
        $this->Energy_closing_unity_model->_total                     = $this->input->post('total');
        $this->Energy_closing_unity_model->_acl_average               = $this->input->post('acl_average');
        $this->Energy_closing_unity_model->_energy_closing_unity_file = $this->input->post('energy_closing_unity_file');
        $this->Energy_closing_unity_model->_status                    = $this->Energy_closing_unity_model->_status_active;
    }

}
