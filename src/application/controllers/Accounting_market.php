<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting_market extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Accounting_market_model');
    }

    function index() {
        $data['list'] = $this->_process_accounting_market();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/accounting_market/index.js"></script>            
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">            
            ';
        $data['main_content'] = 'accounting_market/index';
        $this->load->view('includes/template', $data);
    }

    private function _validate_form() {
        $this->form_validation->set_rules('year', 'year', 'trim|required');
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_accounting_market() {
        $this->Accounting_market_model->_year = $this->input->post('year') ? $this->input->post('year') : date("Y");
        //gera os meses
        $return                               = array();
        for ($i = 1; $i <= 12; $i++) {
            $return[$i]['pk_accounting_market'] = 0;
            $return[$i]['month']                = $i;
            $return[$i]['year']                 = $this->Accounting_market_model->_year;
            $return[$i]['reporting_date']       = "";
            $return[$i]['debit_date']           = "";
            $return[$i]['credit_date']          = "";
            $return[$i]['gfn001_date']          = "";
            $return[$i]['warranty_limit_date']  = "";
        }

        $this->Accounting_market_model->_status = $this->Accounting_market_model->_status_active;
        $accounting_market_list                 = $this->Accounting_market_model->fetch();
        if ($accounting_market_list) {
            foreach ($accounting_market_list as $list) {
                $return[$list['month']]['pk_accounting_market'] = $list['pk_accounting_market'];
                $return[$list['month']]['reporting_date']       = $list['reporting_date'];
                $return[$list['month']]['debit_date']           = $list['debit_date'];
                $return[$list['month']]['credit_date']          = $list['credit_date'];
                $return[$list['month']]['gfn001_date']          = $list['gfn001_date'];
                $return[$list['month']]['warranty_limit_date']  = $list['warranty_limit_date'];
            }
        }
        return $return;
    }

    function ajax_upsert() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        $this->Accounting_market_model->_month  = $this->input->post('month');
        $this->Accounting_market_model->_year   = $this->input->post('year');
        $this->Accounting_market_model->_status = $this->Accounting_market_model->_status_active;
        $read                                   = $this->Accounting_market_model->read();
        if (!$read) {
            $this->Accounting_market_model->_created_at = date("Y-m-d H:i:s");
        }

        switch ($this->input->post('field')) {
            case 'reporting_date':
                $this->Accounting_market_model->_reporting_date      = human_date_to_date($this->input->post('val'), 1);
                break;
            case 'debit_date':
                $this->Accounting_market_model->_debit_date          = human_date_to_date($this->input->post('val'), 1);
                break;
            case 'credit_date':
                $this->Accounting_market_model->_credit_date         = human_date_to_date($this->input->post('val'), 1);
                break;
            case 'gfn001_date':
                $this->Accounting_market_model->_gfn001_date         = human_date_to_date($this->input->post('val'), 1);
                break;
            case 'warranty_limit_date':
                $this->Accounting_market_model->_warranty_limit_date = human_date_to_date($this->input->post('val'), 1);
                break;
        }

        $upsert = $this->Accounting_market_model->upsert();
        print_r(json_encode($upsert));
    }

}
