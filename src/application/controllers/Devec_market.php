<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Devec_market extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Devec_market_model');
    }

    function index() {
        $data['devecs'] = $this->_process_devec();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/devec_market/index.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'devec_market/index';
        $this->load->view('includes/template', $data);
    }

    private function _process_devec() {
        $return = false;
        $states = retorna_estados();
        if ($states) {
            foreach ($states as $sigla => $estado) {
                $return[$sigla]['devec']      = 0;
                $return[$sigla]['start_date'] = null;
            }
        }

        $this->Devec_market_model->_status = $this->Devec_market_model->_status_active;
        $devecs                            = $this->Devec_market_model->fetch();
        if ($devecs) {
            foreach ($devecs as $devec) {
                $return[$devec['uf']]['devec']      = $devec['devec'];
                $return[$devec['uf']]['start_date'] = $devec['start_date'];
            }
        }
        return $return;
    }

    private function _validate_form() {
        $this->form_validation->set_rules('uf', 'uf', 'trim|required');
        $this->form_validation->set_rules('devec', 'devec', 'trim|required');
        $this->form_validation->set_rules('start_date', 'start_date', 'trim|required');
    }

    private function _fill_model() {
        $this->Devec_market_model->_uf         = $this->input->post('uf');
        $this->Devec_market_model->_devec      = $this->input->post('devec');
        $this->Devec_market_model->_start_date = human_date_to_date($this->input->post('start_date'), 1);
        $this->Devec_market_model->_status     = $this->Devec_market_model->_status_active;
        $this->Devec_market_model->_created_at = date("Y-m-d H:i:s");
    }

    public function ajax_upsert() {
        $this->_fill_model();
        $this->Devec_market_model->upsert();
    }

}
