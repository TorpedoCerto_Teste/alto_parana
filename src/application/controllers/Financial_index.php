<?php

/**
  pk_financial_index
  type
  value
  date
  status
  created_at
  updated_at
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Financial_index extends Public_Controller {

    function __construct() {
        parent::__construct();


        $this->load->model('Financial_index_model');
    }

    function index() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $data['indices'] = $this->_process_index();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'web/js/financial_index/index.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'financial_index/index';


        $this->load->view('includes/template', $data);
    }

    function upsert() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        $data['error']   = false;
        $data['success'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('type', 'type', 'trim|required');
            $this->form_validation->set_rules('value', 'value', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Financial_index_model->_date   = $this->input->post('year') . "-" . $this->input->post('month') . "-01";
                $this->Financial_index_model->_type   = mb_strtoupper($this->input->post('type'), 'UTF-8');
                $this->Financial_index_model->_value  = number_format(str_replace(',', '.', $this->input->post('value')), 4, '.', '');
                $this->Financial_index_model->_status = $this->Financial_index_model->_status_active;
                $create                               = $this->Financial_index_model->upsert();
                if ($create) {
                    $data['success'] = true;
                }
            }
            else {
                $data['error'] = true;
            }
        }

        $data['indices'] = $this->_process_index();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/financial_index/upsert.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'financial_index/upsert';
        $this->load->view('includes/template', $data);
    }

    function ajax() {
        $this->Financial_index_model->_date = $this->uri->segment(4) . "-" . $this->uri->segment(3) . "-01";
        $this->Financial_index_model->_type = $this->uri->segment(5);
        if ($this->Financial_index_model->find_by_date_and_type()) {
            print_r(number_format($this->Financial_index_model->_value, 3, ',', ''));
        }
    }

    private function _process_index() {
        $return                               = array();
        $this->Financial_index_model->_status = $this->Financial_index_model->_status_active;
        $data['list']                         = $this->Financial_index_model->fetch();

        if ($data['list']) {
            foreach ($data['list'] as $value) {
                $return[$value['date']]['IPCA'] = 0;
                $return[$value['date']]['IGPM'] = 0;
            }
        }

        if ($data['list']) {
            foreach ($data['list'] as $value) {
                $return[$value['date']][$value['type']] = $value['value'];
            }
        }

        return $return;
    }

}
