<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ccvee_unity_model');
    }

    function index() {
        $this->Ccvee_unity_model->_status = $this->Ccvee_unity_model->_status_active;
        $data['list']                     = $this->Ccvee_unity_model->fetch();
        $data['js_include']               = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']              = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']             = 'ccvee_unity/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Ccvee_unity_model->create();
            }
        }
        redirect('ccvee_general/view/' . $this->uri->segment(3) . '/unidades');
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('ccvee_unity/index');
        }
        $this->Ccvee_unity_model->_pk_ccvee_unity = $this->uri->segment(3);
        $read                                     = $this->Ccvee_unity_model->read();
        if (!$read) {
            redirect('ccvee_unity/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Ccvee_unity_model->update();
                redirect('ccvee_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'ccvee_unity/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('ccvee_unity/index');
        }
        $this->Ccvee_unity_model->_pk_ccvee_unity = $this->uri->segment(3);
        $read                                     = $this->Ccvee_unity_model->read();
        if (!$read) {
            redirect('ccvee_unity/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Ccvee_unity_model->upsert();
                redirect('ccvee_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'ccvee_unity/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_ccvee_unity') > 0) {
            $this->Ccvee_unity_model->_pk_ccvee_unity = $this->input->post('pk_ccvee_unity');
            $this->Ccvee_unity_model->delete();
        }
        redirect('ccvee_general/view/' . $this->uri->segment(3) . '/unidades');
    }

    private function _process_agent_unity($list_units) {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();

        //retirar as unidades que já foram selecionadas
        if ($units && $list_units) {
            $units = $this->_clean_units($units, $list_units);
        }

        $option = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_company_name']][$unity['pk_unity']] = $unity;
            }
        }

        return $option;
    }

    private function _clean_units($units, $list_units) {
        foreach ($units as $k => $unit) {
            foreach ($list_units as $list_unit) {
                if ($unit['pk_unity'] == $list_unit['fk_unity']) {
                    unset($units[$k]);
                }
            }
        }
        return $units;
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('volumn_mwm', 'volumn_mwm', 'trim');
        $this->form_validation->set_rules('contract_cliqccee', 'contract_cliqccee', 'trim');
        $this->form_validation->set_rules('start_date', 'start_date', 'trim');
        $this->form_validation->set_rules('end_date', 'end_date', 'trim');
    }

    private function _fill_model() {
        $this->Ccvee_unity_model->_fk_ccvee_general  = $this->uri->segment(3);
        $this->Ccvee_unity_model->_fk_unity          = $this->input->post('fk_unity');
        $this->Ccvee_unity_model->_volumn_mwm        = number_format(str_replace(",", ".", $this->input->post('volumn_mwm')), 6, '.', '');
        $this->Ccvee_unity_model->_contract_cliqccee = preg_replace("/([^\d]*)/", "", $this->input->post('contract_cliqccee'));
        $this->Ccvee_unity_model->_start_date        = human_date_to_date($this->input->post('start_date'));
        $this->Ccvee_unity_model->_end_date          = human_date_to_date($this->input->post('end_date'));
        $this->Ccvee_unity_model->_status            = $this->Ccvee_unity_model->_status_active;
    }

}
