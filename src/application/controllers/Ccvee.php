<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ccvee_model');
    }

    function index() {
        $this->Ccvee_model->_status = $this->Ccvee_model->_status_active;
        $data['list']               = $this->Ccvee_model->fetch();
        $data['js_include']         = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']        = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';




        $data['main_content'] = 'ccvee/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Ccvee_model->create();
                if ($create) {
                    redirect('ccvee_general/create/' . $this->Ccvee_model->_fk_unity . '/' . $create);
                }
            }
            $data['error'] = true;
        }

        //pega a lista de unidades (cliente de serviço)
        $data['units'] = $this->_process_unity();
        //pega a lista de usuários (cadastrado por)
        $data['users'] = $this->_process_user();

        $data['js_include']  = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>';
        $data['css_include'] = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';

        $data['main_content'] = 'ccvee/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('ccvee/index');
        }
        $this->Ccvee_model->_pk_ccvee = $this->uri->segment(3);
        $read                         = $this->Ccvee_model->read();
        if (!$read) {
            redirect('ccvee/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Ccvee_model->update();
                redirect('ccvee/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'ccvee/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_ccvee') > 0) {
            $this->Ccvee_model->_pk_ccvee = $this->input->post('pk_ccvee');
            $this->Ccvee_model->delete();
        }
        redirect('ccvee/index');
    }

    private function _process_unity() {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();

        $option = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_type']][$unity['pk_unity']] = $unity;
            }
        }

        return $option;
    }

    private function _process_user() {
        $this->load->model('User_model');
        $this->User_model->_status = $this->User_model->_status_active;
        return $this->User_model->fetch();
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('fk_user', 'fk_user', 'trim|required');
    }

    private function _fill_model() {
        $this->Ccvee_model->_fk_unity = $this->input->post('fk_unity');
        $this->Ccvee_model->_fk_user  = $this->input->post('fk_user');
        $this->Ccvee_model->_status   = $this->Ccvee_model->_status_active;
    }

}
