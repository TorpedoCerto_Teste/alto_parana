<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message_agent extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Message_agent_model');
    }

    function index() {
        $this->Message_agent_model->_status = $this->Message_agent_model->_status_active;
        $data['list']                       = $this->Message_agent_model->fetch();
        $data['js_include']                 = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']                = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']               = 'message_agent/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Message_agent_model->_       = mb_strtoupper($this->input->post(''), 'UTF-8');
                $this->Message_agent_model->_status = $this->Message_agent_model->_status_active;
                $create                             = $this->Message_agent_model->create();
                if ($create) {
                    redirect('message_agent/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message_agent/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message_agent/index');
        }
        $this->Message_agent_model->_pk_message_agent = $this->uri->segment(3);
        $read                                         = $this->Message_agent_model->read();
        if (!$read) {
            redirect('message_agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Message_agent_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $update                       = $this->Message_agent_model->update();
                redirect('message_agent/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message_agent/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message_agent/index');
        }
        $this->Message_agent_model->_pk_message_agent = $this->uri->segment(3);
        $read                                         = $this->Message_agent_model->read();
        if (!$read) {
            redirect('message_agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Message_agent_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $upsert                       = $this->Message_agent_model->upsert();
                redirect('message_agent/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message_agent/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_message_agent') > 0) {
            $this->Message_agent_model->_pk_message_agent = $this->input->post('pk_message_agent');
            $this->Message_agent_model->delete();
        }
        redirect('message_agent/index');
    }

}
