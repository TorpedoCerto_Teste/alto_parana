<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_charges extends Public_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Bank_charges_model');
    }

    function index() {
        redirect('bank_charges/create');
    }

    function create() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();

                $this->Bank_charges_model->_pk_bank_charges = $this->Bank_charges_model->upsert();
                $this->_process_files();
                redirect('bank_charges/create/' . $this->Bank_charges_model->_pk_bank_charges);
            }
            $data['error'] = true;
        }

        //pega todas as distribuidoras
        $this->load->model('Agent_model');
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $data['agents']                    = $this->Agent_model->fetch();

        //se o pk_bank_charges for informado, selecionar a tarifa selecionada
        $this->Bank_charges_model->_pk_bank_charges = $this->uri->segment(3, $this->Bank_charges_model->_pk_bank_charges);
        $read_bank_charges                          = $this->Bank_charges_model->read();

        if ($read_bank_charges) {
            $data['resolutions'] = $this->Bank_charges_model->fetch_resolution();
        }


        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'web/js/bank_charges/create.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'bank_charges/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('bank_charges/index');
        }
        $this->Bank_charges_model->_pk_bank_charges = $this->uri->segment(3);
        $read                                       = $this->Bank_charges_model->read();
        if (!$read) {
            redirect('bank_charges/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Bank_charges_model->update();
                redirect('bank_charges/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'bank_charges/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        if ($this->input->post('pk_bank_charges') > 0) {
            $this->Bank_charges_model->_pk_bank_charges = $this->input->post('pk_bank_charges');
            $this->Bank_charges_model->delete();
        }
        redirect('bank_charges/index');
    }

    /**
     * Método ajax para pegar as resoluções
     */
    function resolution() {
        $this->Bank_charges_model->_fk_agent_power_distributor = $this->uri->segment(3, 0);
        $resolutions                                           = $this->Bank_charges_model->fetch_resolution();
        print(json_encode($resolutions));
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_agent_power_distributor', 'fk_agent_power_distributor', 'trim|required');
        $this->form_validation->set_rules('date_adjustment', 'date_adjustment', 'trim');
        $this->form_validation->set_rules('resolution', 'resolution', 'trim|required');
        $this->form_validation->set_rules('date_resolution', 'date_resolution', 'trim|required');
        $this->form_validation->set_rules('beginning_term', 'beginning_term', 'trim|required');
        $this->form_validation->set_rules('end_term', 'end_term', 'trim|required');
        $this->form_validation->set_rules('a1_azul_tusd_kw_p', 'a1_azul_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a1_azul_tusd_mwh_p', 'a1_azul_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a1_azul_te_mwh_p_verde', 'a1_azul_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a1_azul_tusd_kw_fp', 'a1_azul_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a1_azul_tusd_mwh_fp', 'a1_azul_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a1_azul_te_mwh_fp_verde', 'a1_azul_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a1_azul_ape_tusd_kw_p', 'a1_azul_ape_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a1_azul_ape_tusd_mwh_p', 'a1_azul_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a1_azul_ape_te_mwh_p_verde', 'a1_azul_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a1_azul_ape_tusd_kw_fp', 'a1_azul_ape_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a1_azul_ape_tusd_mwh_fp', 'a1_azul_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a1_azul_ape_te_mwh_fp_verde', 'a1_azul_ape_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a2_azul_tusd_kw_p', 'a2_azul_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a2_azul_tusd_mwh_p', 'a2_azul_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a2_azul_te_mwh_p_verde', 'a2_azul_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a2_azul_tusd_kw_fp', 'a2_azul_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a2_azul_tusd_mwh_fp', 'a2_azul_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a2_azul_te_mwh_fp_verde', 'a2_azul_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a2_azul_ape_tusd_kw_p', 'a2_azul_ape_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a2_azul_ape_tusd_mwh_p', 'a2_azul_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a2_azul_ape_te_mwh_p_verde', 'a2_azul_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a2_azul_ape_tusd_kw_fp', 'a2_azul_ape_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a2_azul_ape_tusd_mwh_fp', 'a2_azul_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a2_azul_ape_te_mwh_fp_verde', 'a2_azul_ape_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a3_azul_tusd_kw_p', 'a3_azul_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a3_azul_tusd_mwh_p', 'a3_azul_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a3_azul_te_mwh_p_verde', 'a3_azul_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a3_azul_tusd_kw_fp', 'a3_azul_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a3_azul_tusd_mwh_fp', 'a3_azul_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a3_azul_te_mwh_fp_verde', 'a3_azul_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a3_azul_ape_tusd_kw_p', 'a3_azul_ape_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a3_azul_ape_tusd_mwh_p', 'a3_azul_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a3_azul_ape_te_mwh_p_verde', 'a3_azul_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a3_azul_ape_tusd_kw_fp', 'a3_azul_ape_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a3_azul_ape_tusd_mwh_fp', 'a3_azul_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a3_azul_ape_te_mwh_fp_verde', 'a3_azul_ape_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a3a_azul_tusd_kw_p', 'a3a_azul_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a3a_azul_tusd_mwh_p', 'a3a_azul_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a3a_azul_te_mwh_p_verde', 'a3a_azul_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a3a_azul_tusd_kw_fp', 'a3a_azul_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a3a_azul_tusd_mwh_fp', 'a3a_azul_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a3a_azul_te_mwh_fp_verde', 'a3a_azul_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a3a_azul_ape_tusd_kw_p', 'a3a_azul_ape_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a3a_azul_ape_tusd_mwh_p', 'a3a_azul_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a3a_azul_ape_te_mwh_p_verde', 'a3a_azul_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a3a_azul_ape_tusd_kw_fp', 'a3a_azul_ape_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a3a_azul_ape_tusd_mwh_fp', 'a3a_azul_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a3a_azul_ape_te_mwh_fp_verde', 'a3a_azul_ape_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a3a_verde_tusd_kw_na', 'a3a_verde_tusd_kw_na', 'trim');
        $this->form_validation->set_rules('a3a_verde_tusd_mwh_p', 'a3a_verde_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a3a_verde_te_mwh_p_verde', 'a3a_verde_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a3a_verde_tusd_mwh_fp', 'a3a_verde_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a3a_verde_te_mwh_fp_verde', 'a3a_verde_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a3a_verde_ape_tusd_kw_na', 'a3a_verde_ape_tusd_kw_na', 'trim');
        $this->form_validation->set_rules('a3a_verde_ape_tusd_mwh_p', 'a3a_verde_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a3a_verde_ape_te_mwh_p_verde', 'a3a_verde_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a3a_verde_ape_tusd_mwh_fp', 'a3a_verde_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a3a_verde_ape_te_mwh_fp_verde', 'a3a_verde_ape_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a4_azul_tusd_kw_p', 'a4_azul_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a4_azul_tusd_mwh_p', 'a4_azul_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a4_azul_te_mwh_p_verde', 'a4_azul_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a4_azul_tusd_kw_fp', 'a4_azul_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a4_azul_tusd_mwh_fp', 'a4_azul_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a4_azul_te_mwh_fp_verde', 'a4_azul_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a4_azul_ape_tusd_kw_p', 'a4_azul_ape_tusd_kw_p', 'trim');
        $this->form_validation->set_rules('a4_azul_ape_tusd_mwh_p', 'a4_azul_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a4_azul_ape_te_mwh_p_verde', 'a4_azul_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a4_azul_ape_tusd_kw_fp', 'a4_azul_ape_tusd_kw_fp', 'trim');
        $this->form_validation->set_rules('a4_azul_ape_tusd_mwh_fp', 'a4_azul_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a4_azul_ape_te_mwh_fp_verde', 'a4_azul_ape_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a4_verde_tusd_kw_na', 'a4_verde_tusd_kw_na', 'trim');
        $this->form_validation->set_rules('a4_verde_ape_tusd_kw_na', 'a4_verde_ape_tusd_kw_na', 'trim');
        $this->form_validation->set_rules('a4_verde_tusd_mwh_p', 'a4_verde_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a4_verde_te_mwh_p_verde', 'a4_verde_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a4_verde_tusd_mwh_fp', 'a4_verde_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a4_verde_te_mwh_fp_verde', 'a4_verde_te_mwh_fp_verde', 'trim');
        $this->form_validation->set_rules('a4_verde_ape_tusd_mwh_p', 'a4_verde_ape_tusd_mwh_p', 'trim');
        $this->form_validation->set_rules('a4_verde_ape_te_mwh_p_verde', 'a4_verde_ape_te_mwh_p_verde', 'trim');
        $this->form_validation->set_rules('a4_verde_ape_tusd_mwh_fp', 'a4_verde_ape_tusd_mwh_fp', 'trim');
        $this->form_validation->set_rules('a4_verde_ape_te_mwh_fp_verde', 'a4_verde_ape_te_mwh_fp_verde', 'trim');
    }

    private function _fill_model() {
        $this->Bank_charges_model->_fk_agent_power_distributor    = $this->input->post('fk_agent_power_distributor');
        $this->Bank_charges_model->_date_adjustment               = human_date_to_date($this->input->post('date_adjustment'));
        $this->Bank_charges_model->_resolution                    = $this->input->post('resolution');
        $this->Bank_charges_model->_date_resolution               = human_date_to_date($this->input->post('date_resolution'));
        $this->Bank_charges_model->_beginning_term                = human_date_to_date($this->input->post('beginning_term'));
        $this->Bank_charges_model->_end_term                      = human_date_to_date($this->input->post('end_term'));
        $this->Bank_charges_model->_a1_azul_tusd_kw_p             = number_format(str_replace(',', '.', $this->input->post('a1_azul_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_tusd_mwh_p            = number_format(str_replace(',', '.', $this->input->post('a1_azul_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_te_mwh_p_verde        = number_format(str_replace(',', '.', $this->input->post('a1_azul_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_tusd_kw_fp            = number_format(str_replace(',', '.', $this->input->post('a1_azul_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_tusd_mwh_fp           = number_format(str_replace(',', '.', $this->input->post('a1_azul_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_te_mwh_fp_verde       = number_format(str_replace(',', '.', $this->input->post('a1_azul_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_ape_tusd_kw_p         = number_format(str_replace(',', '.', $this->input->post('a1_azul_ape_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_ape_tusd_mwh_p        = number_format(str_replace(',', '.', $this->input->post('a1_azul_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_ape_te_mwh_p_verde    = number_format(str_replace(',', '.', $this->input->post('a1_azul_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_ape_tusd_kw_fp        = number_format(str_replace(',', '.', $this->input->post('a1_azul_ape_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_ape_tusd_mwh_fp       = number_format(str_replace(',', '.', $this->input->post('a1_azul_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a1_azul_ape_te_mwh_fp_verde   = number_format(str_replace(',', '.', $this->input->post('a1_azul_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_tusd_kw_p             = number_format(str_replace(',', '.', $this->input->post('a2_azul_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_tusd_mwh_p            = number_format(str_replace(',', '.', $this->input->post('a2_azul_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_te_mwh_p_verde        = number_format(str_replace(',', '.', $this->input->post('a2_azul_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_tusd_kw_fp            = number_format(str_replace(',', '.', $this->input->post('a2_azul_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_tusd_mwh_fp           = number_format(str_replace(',', '.', $this->input->post('a2_azul_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_te_mwh_fp_verde       = number_format(str_replace(',', '.', $this->input->post('a2_azul_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_ape_tusd_kw_p         = number_format(str_replace(',', '.', $this->input->post('a2_azul_ape_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_ape_tusd_mwh_p        = number_format(str_replace(',', '.', $this->input->post('a2_azul_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_ape_te_mwh_p_verde    = number_format(str_replace(',', '.', $this->input->post('a2_azul_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_ape_tusd_kw_fp        = number_format(str_replace(',', '.', $this->input->post('a2_azul_ape_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_ape_tusd_mwh_fp       = number_format(str_replace(',', '.', $this->input->post('a2_azul_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a2_azul_ape_te_mwh_fp_verde   = number_format(str_replace(',', '.', $this->input->post('a2_azul_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_tusd_kw_p             = number_format(str_replace(',', '.', $this->input->post('a3_azul_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_tusd_mwh_p            = number_format(str_replace(',', '.', $this->input->post('a3_azul_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_te_mwh_p_verde        = number_format(str_replace(',', '.', $this->input->post('a3_azul_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_tusd_kw_fp            = number_format(str_replace(',', '.', $this->input->post('a3_azul_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_tusd_mwh_fp           = number_format(str_replace(',', '.', $this->input->post('a3_azul_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_te_mwh_fp_verde       = number_format(str_replace(',', '.', $this->input->post('a3_azul_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_ape_tusd_kw_p         = number_format(str_replace(',', '.', $this->input->post('a3_azul_ape_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_ape_tusd_mwh_p        = number_format(str_replace(',', '.', $this->input->post('a3_azul_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_ape_te_mwh_p_verde    = number_format(str_replace(',', '.', $this->input->post('a3_azul_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_ape_tusd_kw_fp        = number_format(str_replace(',', '.', $this->input->post('a3_azul_ape_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_ape_tusd_mwh_fp       = number_format(str_replace(',', '.', $this->input->post('a3_azul_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3_azul_ape_te_mwh_fp_verde   = number_format(str_replace(',', '.', $this->input->post('a3_azul_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_tusd_kw_p            = number_format(str_replace(',', '.', $this->input->post('a3a_azul_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_tusd_mwh_p           = number_format(str_replace(',', '.', $this->input->post('a3a_azul_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_te_mwh_p_verde       = number_format(str_replace(',', '.', $this->input->post('a3a_azul_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_tusd_kw_fp           = number_format(str_replace(',', '.', $this->input->post('a3a_azul_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_tusd_mwh_fp          = number_format(str_replace(',', '.', $this->input->post('a3a_azul_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_te_mwh_fp_verde      = number_format(str_replace(',', '.', $this->input->post('a3a_azul_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_ape_tusd_kw_p        = number_format(str_replace(',', '.', $this->input->post('a3a_azul_ape_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_ape_tusd_mwh_p       = number_format(str_replace(',', '.', $this->input->post('a3a_azul_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_ape_te_mwh_p_verde   = number_format(str_replace(',', '.', $this->input->post('a3a_azul_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_ape_tusd_kw_fp       = number_format(str_replace(',', '.', $this->input->post('a3a_azul_ape_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_ape_tusd_mwh_fp      = number_format(str_replace(',', '.', $this->input->post('a3a_azul_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3a_azul_ape_te_mwh_fp_verde  = number_format(str_replace(',', '.', $this->input->post('a3a_azul_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_tusd_kw_na          = number_format(str_replace(',', '.', $this->input->post('a3a_verde_tusd_kw_na')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_tusd_mwh_p          = number_format(str_replace(',', '.', $this->input->post('a3a_verde_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_te_mwh_p_verde      = number_format(str_replace(',', '.', $this->input->post('a3a_verde_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_tusd_mwh_fp         = number_format(str_replace(',', '.', $this->input->post('a3a_verde_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_te_mwh_fp_verde     = number_format(str_replace(',', '.', $this->input->post('a3a_verde_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_ape_tusd_kw_na      = number_format(str_replace(',', '.', $this->input->post('a3a_verde_ape_tusd_kw_na')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_ape_tusd_mwh_p      = number_format(str_replace(',', '.', $this->input->post('a3a_verde_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_ape_te_mwh_p_verde  = number_format(str_replace(',', '.', $this->input->post('a3a_verde_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_ape_tusd_mwh_fp     = number_format(str_replace(',', '.', $this->input->post('a3a_verde_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a3a_verde_ape_te_mwh_fp_verde = number_format(str_replace(',', '.', $this->input->post('a3a_verde_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_tusd_kw_p             = number_format(str_replace(',', '.', $this->input->post('a4_azul_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_tusd_mwh_p            = number_format(str_replace(',', '.', $this->input->post('a4_azul_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_te_mwh_p_verde        = number_format(str_replace(',', '.', $this->input->post('a4_azul_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_tusd_kw_fp            = number_format(str_replace(',', '.', $this->input->post('a4_azul_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_tusd_mwh_fp           = number_format(str_replace(',', '.', $this->input->post('a4_azul_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_te_mwh_fp_verde       = number_format(str_replace(',', '.', $this->input->post('a4_azul_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_ape_tusd_kw_p         = number_format(str_replace(',', '.', $this->input->post('a4_azul_ape_tusd_kw_p')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_ape_tusd_mwh_p        = number_format(str_replace(',', '.', $this->input->post('a4_azul_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_ape_te_mwh_p_verde    = number_format(str_replace(',', '.', $this->input->post('a4_azul_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_ape_tusd_kw_fp        = number_format(str_replace(',', '.', $this->input->post('a4_azul_ape_tusd_kw_fp')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_ape_tusd_mwh_fp       = number_format(str_replace(',', '.', $this->input->post('a4_azul_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a4_azul_ape_te_mwh_fp_verde   = number_format(str_replace(',', '.', $this->input->post('a4_azul_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_tusd_kw_na           = number_format(str_replace(',', '.', $this->input->post('a4_verde_tusd_kw_na')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_tusd_mwh_p           = number_format(str_replace(',', '.', $this->input->post('a4_verde_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_te_mwh_p_verde       = number_format(str_replace(',', '.', $this->input->post('a4_verde_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_tusd_mwh_fp          = number_format(str_replace(',', '.', $this->input->post('a4_verde_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_te_mwh_fp_verde      = number_format(str_replace(',', '.', $this->input->post('a4_verde_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_ape_tusd_mwh_p       = number_format(str_replace(',', '.', $this->input->post('a4_verde_ape_tusd_mwh_p')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_ape_tusd_kw_na       = number_format(str_replace(',', '.', $this->input->post('a4_verde_ape_tusd_kw_na')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_ape_te_mwh_p_verde   = number_format(str_replace(',', '.', $this->input->post('a4_verde_ape_te_mwh_p_verde')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_ape_tusd_mwh_fp      = number_format(str_replace(',', '.', $this->input->post('a4_verde_ape_tusd_mwh_fp')), 3, '.', '');
        $this->Bank_charges_model->_a4_verde_ape_te_mwh_fp_verde  = number_format(str_replace(',', '.', $this->input->post('a4_verde_ape_te_mwh_fp_verde')), 3, '.', '');
        $this->Bank_charges_model->_status                        = $this->Bank_charges_model->_status_active;
    }
    
    private function _process_files() {
        $config['upload_path']   = './uploads/bank_charges/' . $this->Bank_charges_model->_pk_bank_charges;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('bank_charges_file')) {
            $this->Bank_charges_model->_bank_charges_file = str_replace(" ", "_", $_FILES['bank_charges_file']['name']);
            $this->Bank_charges_model->update();
        }
    }

    

}
