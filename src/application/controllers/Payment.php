<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment extends Public_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Payment_model');
    }

    function index() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        
        $this->Payment_model->_status = $this->Payment_model->_status_active;
        $data['list']                 = $this->Payment_model->fetch();
        $data['js_include']                 = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            ';
        $data['css_include']                = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']               = 'payment/index';
        $this->load->view('includes/template', $data);
    }

    function create2() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Payment_model->_created_at = date("Y-m-d H:i:s");
                if ($this->Payment_model->_month <= date("m") && $this->Payment_model->_year <= date("Y") && $this->Payment_model->_value > 0) {
                    $create                           = $this->Payment_model->create();
                    if ($create) {
                        redirect('payment/index');
                    }
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'payment/create';
        $this->load->view('includes/template', $data);
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('value', 'value', 'trim|required');
    }

    private function _fill_model() {
        $this->Payment_model->_month = preg_replace("([^\d]*)", "", $this->input->post('month'));
        $this->Payment_model->_year  = preg_replace("([^\d]*)", "", $this->input->post('year'));
        $this->Payment_model->_value = number_format(str_replace(",", ".", $this->input->post('value')), 2, '.', '');
    }

}
