<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Message_model');
    }

    function index() {
        $this->Message_model->_status = $this->Message_model->_status_active;
        $data['list']                 = $this->Message_model->fetch();
        $data['js_include']           = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']          = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']         = 'message/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        //pega a lista de agentes
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $data['agents']             = $this->Agent_model->fetch();

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Message_model->create();
                if ($create) {
                    //gera o message_agent para atribuir aos agentes específicos
                    $this->load->model('Message_agent_model');
                    $this->Message_agent_model->_status     = $this->Message_agent_model->_status_active;
                    $this->Message_agent_model->_fk_message = $create;
                    if ($this->input->post('fk_agent') <= 0) {
                        foreach ($data['agents'] as $agent) {
                            $this->Message_agent_model->_fk_agent = $agent['pk_agent'];
                            $this->Message_agent_model->create();
                            //enviar email com o conteúdo da mensagem para os agentes selecionados
                            $this->_send_mail_create();
                        }
                    }
                    else {
                        $this->Message_agent_model->_fk_agent = $this->input->post('fk_agent');
                        $this->Message_agent_model->create();
                        //enviar email com o conteúdo da mensagem para os agentes selecionados
                        $this->_send_mail_create();
                    }

                    redirect('message/index');
                }
            }
            $data['error'] = true;
        }


        $data['js_include']   = '
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'web/js/message/create.js"></script>';
        $data['css_include']  = '    
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" /> 
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'message/create';
        $this->load->view('includes/template', $data);
    }

    function view() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message/index');
        }
        $this->Message_model->_pk_message = $this->uri->segment(3);
        $read                             = $this->Message_model->read();
        if (!$read) {
            redirect('message/index');
        }

        $this->load->model('Message_agent_model');
        //conta as mensagens lidas e não lidas
        $this->Message_agent_model->_fk_message = $this->Message_model->_pk_message;
        $this->Message_agent_model->_status     = $this->Message_agent_model->_status_active;
        $data['unread']                         = $this->Message_agent_model->get_total();
        $this->Message_agent_model->_status     = $this->Message_agent_model->_status_read;
        $data['read']                           = $this->Message_agent_model->get_total();

        $data['total_percent'] = $data['read'] > 0 ? round((($data['read'] * 100) / ($data['read'] + $data['unread'])), 0) : 0;

        $data['js_include']   = '
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'web/js/message/create.js"></script>';
        $data['css_include']  = '    
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" /> ';
        $data['main_content'] = 'message/view';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message/index');
        }
        $this->Message_model->_pk_message = $this->uri->segment(3);
        $read                             = $this->Message_model->read();
        if (!$read) {
            redirect('message/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Message_model->update();
                redirect('message/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message/index');
        }
        $this->Message_model->_pk_message = $this->uri->segment(3);
        $read                             = $this->Message_model->read();
        if (!$read) {
            redirect('message/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Message_model->upsert();
                redirect('message/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_message') > 0) {
            $this->Message_model->_pk_message = $this->input->post('pk_message');
            $this->Message_model->delete();
        }
        redirect('message/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('message', 'message', 'trim|required');
    }

    private function _fill_model() {
        $this->Message_model->_title      = $this->input->post('title');
        $this->Message_model->_message    = $this->input->post('message');
        $this->Message_model->_priority   = $this->input->post('priority') == 1 ? 1 : 0;
        $this->Message_model->_status     = $this->Message_model->_status_active;
        $this->Message_model->_created_at = date("Y-m-d H:i:s");
    }

    private function _send_mail_create() {
        //buscar os dados dos contatos do agente
        $this->load->model("Contact_model");
        $this->Contact_model->_fk_agent = $this->Message_agent_model->_fk_agent;
        $this->Contact_model->_status   = $this->Contact_model->_status_active;
        $contacts                       = $this->Contact_model->fetch_to();
        if ($contacts) {
            foreach ($contacts as $contact) {
                $this->load->config('email_config');
                $post['emails'] = !$this->config->item('email_debug') && $contact['email'] != "" ? $contact['email'] : join(",", $this->config->item('to'));
                $post['subject'] = $this->Message_model->_title;
                $msg = $this->load->view('message/email/create', array(), true);
                $post['message'] = base64_encode($msg);
        
                $pymail = pymail($post);
            }
        }
    }

}
