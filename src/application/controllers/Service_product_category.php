<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_product_category extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Service_product_category_model');
    }

    function index() {
        $this->Service_product_category_model->_status = $this->Service_product_category_model->_status_active;
        $data['list']                                  = $this->Service_product_category_model->fetch();
        $data['js_include']                            = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                           = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                          = 'service_product_category/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Service_product_category_model->create();
                if ($create) {
                    redirect('service_product_category/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'service_product_category/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('service_product_category/index');
        }
        $this->Service_product_category_model->_pk_service_product_category = $this->uri->segment(3);
        $read                                                               = $this->Service_product_category_model->read();
        if (!$read) {
            redirect('service_product_category/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Service_product_category_model->update();
                redirect('service_product_category/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'service_product_category/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('service_product_category/index');
        }
        $this->Service_product_category_model->_pk_service_product_category = $this->uri->segment(3);
        $read                                                               = $this->Service_product_category_model->read();
        if (!$read) {
            redirect('service_product_category/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Service_product_category_model->upsert();
                redirect('service_product_category/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'service_product_category/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_service_product_category') > 0) {
            $this->Service_product_category_model->_pk_service_product_category = $this->input->post('pk_service_product_category');
            $this->Service_product_category_model->delete();
        }
        redirect('service_product_category/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_service', 'fk_service', 'trim|required');
        $this->form_validation->set_rules('fk_product_category', 'fk_product_category', 'trim|required');
    }

    private function _fill_model() {
        $this->Service_product_category_model->_fk_service          = mb_strtoupper($this->input->post('fk_service'), 'UTF-8');
        $this->Service_product_category_model->_fk_product_category = mb_strtoupper($this->input->post('fk_product_category'), 'UTF-8');
        $this->Service_product_category_model->_status              = $this->Service_product_category_model->_status_active;
    }

}
