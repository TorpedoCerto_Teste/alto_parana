<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Controller_group extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Controller_group_model');
    }

    function index() {
        $this->Controller_group_model->_status = $this->Controller_group_model->_status_active;
        $data['list']                          = $this->Controller_group_model->fetch();
        $data['js_include']                    = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                   = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                  = 'controller_group/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Controller_group_model->_created_at = date("Y-m-d H:i:s");
                $create                                    = $this->Controller_group_model->create();
                if ($create) {
                    redirect('controller_group/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'controller_group/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('controller_group/index');
        }
        $this->Controller_group_model->_pk_controller_group = $this->uri->segment(3);
        $read                                               = $this->Controller_group_model->read();
        if (!$read) {
            redirect('controller_group/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Controller_group_model->update();
                redirect('controller_group/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'controller_group/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $this->_validate_form();
        if ($this->form_validation->run() == TRUE) {
            $this->_fill_model();
            $upsert = $this->Controller_group_model->upsert();
            redirect($this->input->post('url_return'));
        }
    }

    function delete() {
        if ($this->input->post('pk_controller_group') > 0) {
            $this->Controller_group_model->_pk_controller_group = $this->input->post('pk_controller_group');
            $this->Controller_group_model->delete();
        }
        redirect('controller_group/index');
    }

    function ajax_get_contact_by_group() {
        $fk_group_controller = $this->input->post("fk_group_controller");
        $fk_agent = isset($_POST['fk_agent']) ? $_POST['fk_agent'] : null;
        $fk_unity = isset($_POST['fk_unity']) ? $_POST['fk_unity'] : null;

        if (!$fk_agent && $fk_unity) {
            $fk_agent = $this->get_agent_by_unity($fk_unity);
        }
        $contacts_by_agent = $this->get_contact_by_agent($fk_agent);
        if (is_array($contacts_by_agent) && !empty($contacts_by_agent)) {
            $contacts_by_agent_and_group = $this->get_contact_by_agent_and_group($fk_group_controller, $fk_agent, $fk_unity);

            foreach ($contacts_by_agent as $k => $contacts) {
                $contacts_by_agent[$k]['selected'] = 0;
                if (is_array($contacts_by_agent_and_group) && !empty($contacts_by_agent_and_group)) {
                    foreach ($contacts_by_agent_and_group  as $group) {
                        if ($contacts['fk_contact'] === $group['pk_contact']) {
                            $contacts_by_agent[$k]['selected'] = 1;
                        }
                    }
                }
            }
        }

        header('Content-Type: application/json');
        echo json_encode(['contacts' => $contacts_by_agent]);
    } 

    function ajax_get_groups() {
        $return                     = array();
        

        $this->load->model("Group_model");
        $this->Group_model->_status = $this->Group_model->_status_active;
        $groups                     = $this->Group_model->fetch();

        if ($groups) {
            $controller_name = $this->input->post("controller_name");

            $this->load->model("Controller_group_model");
            $this->Controller_group_model->_status     = $this->Controller_group_model->_status_active;
            $this->Controller_group_model->_controller = $controller_name;
            $read                                      = $this->Controller_group_model->read();

            foreach ($groups as $key => $group) {
                $return[$key]             = $group;
                $return[$key]['selected'] = $group['pk_group'] == $this->Controller_group_model->_fk_group ? 1 : 0;
            }
        }
        header('Content-Type: application/json');
        echo json_encode($return);        
    }    

    private function _validate_form() {
        $this->form_validation->set_rules('fk_group_controller', 'fk_group_controller', 'trim|required');
        $this->form_validation->set_rules('controller', 'controller', 'trim|required');
    }

    private function _fill_model() {
        $this->Controller_group_model->_fk_group   = $this->input->post('fk_group_controller');
        $this->Controller_group_model->_controller = $this->input->post('controller');
        $this->Controller_group_model->_created_at = date("Y-m-d H:i:s");
        $this->Controller_group_model->_status     = $this->Controller_group_model->_status_active;
    }

    private function get_contact_by_agent($fk_agent) {
        $return = [];
        $this->load->model("Agent_contact_model");
        $this->Agent_contact_model->_fk_agent = $fk_agent;
        $this->Agent_contact_model->_status = $this->Agent_contact_model->_status_active;

        $return = $this->Agent_contact_model->fetch_by_agent();
        return $return;
    }

    private function get_contact_by_agent_and_group($fk_group_controller, $fk_agent, $fk_unity) {
        $return = [];
        $this->load->model('Group_agent_contact_model');
        $return = $this->Group_agent_contact_model->get_contacts_by_group_and_agent($fk_group_controller, $fk_agent, $fk_unity);
        return $return;
    }

    private function get_agent_by_unity($fk_unity) {
        $return = 0;
        $this->load->model('Unity_model');
        $this->Unity_model->_pk_unity = $fk_unity;
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $read = $this->Unity_model->read();
        if ($read) {
            $return = $this->Unity_model->_fk_agent;
        }
        return $return;
    }

}
