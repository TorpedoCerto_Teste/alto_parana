<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Telemetry_record extends Public_Controller {

    function __construct() {
        parent::__construct();


        $this->load->model('Telemetry_record_model');
    }

    function index() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        $data['gauges'] = $this->_process_gauge();
        $data['list']   = false;
        $data['date']   = date("Y-m-d 00:00:00");

        if ($this->input->post("filter") == "true") {
            $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
            $this->Telemetry_record_model->_record_date = $this->input->post('year') . "-" . $this->input->post('month') . "-01 00:00:00";
            $this->Telemetry_record_model->_fk_gauge    = $this->input->post('fk_gauge');
            $this->Telemetry_record_model->_greatness   = $this->input->post('greatness');

            $data['list'] = $this->Telemetry_record_model->_greatness == "Demat" ? $this->Telemetry_record_model->fetch_demat() : $this->Telemetry_record_model->fetch_eneat();
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'web/js/telemetry_record/index.js?' . date("YmdHis") . '"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'telemetry_record/index';
        $this->load->view('includes/template', $data);
    }

    function api() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        
        if ($this->input->post("filter") == "true") {
            $this->load->model("Gauge_model");
            $this->Gauge_model->_status = $this->Gauge_model->_status_active;
            if ($this->input->post("fk_gauge") > 0) {
                $this->Gauge_model->_pk_gauge = $this->input->post("fk_gauge");
            }
            $telemetry_codes            = $this->Gauge_model->fetch_telemetry_code();
            if ($telemetry_codes) {
                $start_date = human_date_to_date($this->input->post('start_date'));
                $end_date   = human_date_to_date($this->input->post('end_date'));
                $this->_call_way2($telemetry_codes, $start_date, $end_date);
            }
        }
        $data['gauges'] = $this->_process_gauge();

        $data['js_include']  = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>    
            ';
        $data['css_include'] = '
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            ';

        $data['main_content'] = 'telemetry_record/api';
        $this->load->view('includes/template', $data);
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_gauge', 'fk_gauge', 'trim|required');
        $this->form_validation->set_rules('greatness', 'greatness', 'trim|required');
        $this->form_validation->set_rules('record_date', 'record_date', 'trim|required');
        $this->form_validation->set_rules('value', 'value', 'trim|required');
        $this->form_validation->set_rules('quality', 'quality', 'trim|required');
    }

    private function _fill_model() {
        $this->Telemetry_record_model->_fk_gauge    = $this->input->post('fk_gauge');
        $this->Telemetry_record_model->_greatness   = $this->input->post('greatness');
        $this->Telemetry_record_model->_record_date = mb_strtoupper($this->input->post('record_date'), 'UTF-8');
        $this->Telemetry_record_model->_value       = number_format($this->input->post('value'), 2, '.', '');
        $this->Telemetry_record_model->_quality     = $this->input->post('quality');
        $this->Telemetry_record_model->_created_at  = date("Y-m-d H:i:s");
    }

    private function _process_gauge() {
        $return                     = false;
        $this->load->model("Gauge_model");
        $this->Gauge_model->_status = $this->Gauge_model->_status_active;
        $gauges                     = $this->Gauge_model->fetch();
        if ($gauges) {
            foreach ($gauges as $gauge) {
                $return[$gauge['fk_unity']]['community_name'] = $gauge['community_name'];
                $return[$gauge['fk_unity']]['gauges'][]       = $gauge;
            }
        }
        return $return;
    }

    private function _call_way2($teleletry_codes, $start_date = null, $end_date = null) {
        $pim_auth      = $this->config->item("way2_token");
        $start         = !is_null($start_date) ? $start_date."T00:00:00" : date("Y-m-d\TH", strtotime("-1 DAYS")) . ":00:00";
        $end           = !is_null($end_date) ? date("Y-m-d", strtotime("+1 DAYS", strtotime($end_date)))."T00:00:00" : date("Y-m-d\TH") . ":00:00";
        $context       = 1;
        $type_interval = array(
            'Eneat' => 60, //60 minutos
            'Enere' => 60,
            'Demat' => 15,
            'Demre' => 15
//            ,
//            'Demre'      => 15,
//            'DemConPta'  => 15,
//            'DemConfpta' => 15
        );

        foreach ($teleletry_codes as $teleletry_code) {
            foreach ($type_interval as $type => $interval) {
                $summer_time = $teleletry_code['summer_time'] == 1 ? "true" : "false";
                $url         = 'http://sx.way2.com.br:81/api/pontos/' . $teleletry_code['telemetry_code'] . '/dados-de-medicao/' . $type . '?dataInicio=' . $start . '&dataFim=' . $end . '&contextoDasDatas=' . $context . '&intervaloEmMinutos=' . $interval . '&aplicarHorarioDeVerao=' . $summer_time;
                $ch          = curl_init($url);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Pim-auth: ' . $pim_auth));
                $result      = curl_exec($ch);
                $this->_process_way2_response($teleletry_code, $result);
            }
        }
    }

    private function _process_way2_response($telemetry_code, $result) {
        $response = json_decode($result);
        if (isset($response->Dados)) {
            $this->Telemetry_record_model->_fk_gauge  = $telemetry_code['pk_gauge'];
            $this->Telemetry_record_model->_greatness = $response->Dados[0]->NomeGrandeza;
            foreach ($response->Dados[0]->Valores as $values) {
                $this->Telemetry_record_model->_record_date = str_replace("T", " ", $values->Data);
                $this->Telemetry_record_model->_value       = isset($values->Valor) ? number_format($values->Valor, 3, ".", "") : 0.000;
                $this->Telemetry_record_model->_quality     = $values->Qualidade;
                $this->Telemetry_record_model->delete();
                $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
                $this->Telemetry_record_model->_created_at  = date("Y-m-d H:i:s");
                $this->Telemetry_record_model->create();
            }
        }
    }
    
    
    //INFORMAÇÕES PARA GRÁFICOS

    /**
     * Retorna valores para geração do gráfico de consumo diario por mês
     * Parâmetros por post: fk_gauge, fk_unity, date
     */
    public function ajax_consumo_diario_mes() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        //com a informação da unidade, é possivel encontrar o agente e assim, continuar coletando os dados
        $this->_process_unity();
        //pegando os horários de ponta
        $this->_process_rush_hour();
        //pega os dias de feriados no ano
        $data['holiday'] = $this->_process_holiday_by_year($this->input->post('date'));

        //pega as telemetrias
        $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_fk_gauge    = $this->input->post('fk_gauge');
        $this->Telemetry_record_model->_record_date = $this->input->post('date');
        $this->Telemetry_record_model->_greatness   = "Demat";
        $telemetry_records                          = $this->Telemetry_record_model->fetch_demat();
        //colunas
        $col1                                       = array();
        $col1["id"]                                 = "";
        $col1["label"]                              = "Dia";
        $col1["pattern"]                            = "";
        $col1["type"]                               = "string";

        $col2          = array();
        $col2["id"]    = "";
        $col2["label"] = "Fora Ponta";
        $col2["type"]  = "number";

        $col3          = array();
        $col3["id"]    = "";
        $col3["label"] = "Ponta";
        $col3["type"]  = "number";

        $cols = array($col1, $col2, $col3);

        $rows = array();
        for ($i = 1; $i <= date("t", strtotime($this->input->post('date'))); $i++) {
            $cel0["v"] = $i;
            $cel1["v"] = 0;
            $cel2["v"] = 0;
            $row0["c"] = array($cel0, $cel1, $cel2);
            array_push($rows, $row0);
        }

        if ($telemetry_records) {
            foreach ($telemetry_records as $consumo) {
                $consumo['value'] = $consumo['value'] > 0 ? ($consumo['value'] / 4) / 1000 : 0;
                foreach ($rows as $k => $row) {
                    if ($row['c'][0]['v'] == date("j", strtotime($consumo['record_date']))) {
                        if (date("N", strtotime($consumo['record_date'])) < 6 && $this->_is_holiday($data['holiday'], date("Y-m-d", strtotime($consumo['record_date']))) == false) {
                            if ((date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) >= $this->Rush_hour_model->_start_hour && date("H:i:s", strtotime($consumo['record_date'])) <= $this->Rush_hour_model->_end_hour)) {
                                $rows[$k]['c'][2]['v'] += $consumo['value'];
                            }
                            else {
                                $rows[$k]['c'][1]['v'] += $consumo['value'];
                            }
                        }
                        else {
                            $rows[$k]['c'][1]['v'] += $consumo['value'];
                        }
                    }
                }
            }
        }
        $data = array("cols" => $cols, "rows" => $rows);
        print_r(json_encode($data));
    }

    public function ajax_consumo_horario_dia() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        //pega as telemetrias
        $this->Telemetry_record_model->_status       = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_fk_gauge     = $this->input->post('fk_gauge');
        $this->Telemetry_record_model->_record_date  = $this->input->post('date');
        $this->Telemetry_record_model->_greatness    = "Eneat";
        $this->Telemetry_record_model->_fetch_period = "day";
        $telemetry_records                           = $this->Telemetry_record_model->fetch_eneat();

        //colunas
        $col1            = array();
        $col1["id"]      = "";
        $col1["label"]   = "Hora";
        $col1["pattern"] = "";
        $col1["type"]    = "string";

        $col2            = array();
        $col2["id"]      = "";
        $col2["label"]   = "Consumo";
        $col2["pattern"] = "";
        $col2["type"]    = "number";

        $cols = array($col1, $col2);

        $rows = array();
        for ($i = 1; $i <= 24; $i++) {
            $cel0["v"] = $i;
            $cel1["v"] = 0;
            $row0["c"] = array($cel0, $cel1);
            array_push($rows, $row0);
        }

        if ($telemetry_records) {
            foreach ($telemetry_records as $consumo) {
                foreach ($rows as $k => $row) {
                    if ($row['c'][0]['v'] == $consumo['order']) {
                        $rows[$k]['c'][1]['v'] += $consumo['value'];
                    }
                }
            }
        }
        $data = array("cols" => $cols, "rows" => $rows);
        print_r(json_encode($data));
    }

    public function ajax_demanda_medida_diaria() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        //com a informação da unidade, é possivel encontrar o agente e assim, continuar coletando os dados
        $this->_process_unity();
        //pegando os horários de ponta
        $this->_process_rush_hour();
        //demanda contratada
        $this->_process_demand($this->input->post('date'));
        //pega os dias de feriados no ano
        $data['holiday'] = $this->_process_holiday_by_year($this->input->post('date'));

        //pega as telemetrias
        $this->Telemetry_record_model->_status       = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_fk_gauge     = $this->input->post('fk_gauge');
        $this->Telemetry_record_model->_record_date  = $this->input->post('date');
        $this->Telemetry_record_model->_greatness    = "Demat";
        $this->Telemetry_record_model->_fetch_period = "day";
        $telemetry_records                           = $this->Telemetry_record_model->fetch_demat();

        //colunas
        $col1            = array();
        $col1["id"]      = "";
        $col1["label"]   = "Hora";
        $col1["pattern"] = "";
        $col1["type"]    = "string";

        $col2            = array();
        $col2["id"]      = "";
        $col2["label"]   = "Fora Ponta";
        $col2["pattern"] = "";
        $col2["type"]    = "number";

        $col3            = array();
        $col3["id"]      = "";
        $col3["label"]   = "Ponta";
        $col3["pattern"] = "";
        $col3["type"]    = "number";

        $col4            = array();
        $col4["id"]      = "";
        $col4["label"]   = "Contratada";
        $col4["pattern"] = "";
        $col4["type"]    = "number";

        $col5            = array();
        $col5["id"]      = "";
        $col5["label"]   = "Limite";
        $col5["pattern"] = "";
        $col5["type"]    = "number";

        $cols = array($col1, $col2, $col3, $col4, $col5);

        $rows = array();
        for ($i = 1; $i <= 24; $i++) {
            $x = $i == 24 ? 0 : $i;
            for ($j = 0; $j <= 45; $j += 15) {
                $cel0["v"] = str_pad($x, 2, "0", STR_PAD_LEFT) . ":" . str_pad($j, 2, "0", STR_PAD_LEFT);
                $cel1["v"] = 0;
                $cel2["v"] = 0;
                $cel3["v"] = 0;
                $cel4["v"] = 0;
                $row0["c"] = array($cel0, $cel1, $cel2, $cel3, $cel4);
                array_push($rows, $row0);
            }
        }

        //insere os valores de demanda contratada na ponta e fora de ponta
        foreach ($rows as $k => $row) {
            if ((date("N", strtotime($row['c'][0]['v'])) < 6 && $this->_is_holiday($data['holiday'], date("Y-m-d", strtotime($row['c'][0]['v']))) == false)) {
                if ($row['c'][0]['v'] >= date("H:i", strtotime($this->Rush_hour_model->_start_hour)) && $row['c'][0]['v'] <= $this->Rush_hour_model->_end_hour) {
                    $rows[$k]['c'][3]['v'] = (int) $this->Demand_model->_end_demand;
                    $rows[$k]['c'][4]['v'] = (int) $this->Demand_model->_end_demand + ((int)$this->Demand_model->_end_demand * 0.05);
                }
                else {
                    $rows[$k]['c'][3]['v'] = (int) $this->Demand_model->_demand_tip_out;
                    $rows[$k]['c'][4]['v'] = (int) $this->Demand_model->_demand_tip_out + ((int)$this->Demand_model->_demand_tip_out * 0.05);
                }
            }
            else {
                $rows[$k]['c'][3]['v'] = (int) $this->Demand_model->_demand_tip_out;
                $rows[$k]['c'][4]['v'] = (int) $this->Demand_model->_demand_tip_out + ((int)$this->Demand_model->_demand_tip_out * 0.05);
            }
        }

        //insere os dados de telemetria
        if ($telemetry_records) {
            foreach ($rows as $k => $row) {
                foreach ($telemetry_records as $consumo) {
                    if ($row['c'][0]['v'] == date("H:i", strtotime($consumo['record_date']))) {
                        if ((date("N", strtotime($consumo['record_date'])) < 6 && $this->_is_holiday($data['holiday'], date("Y-m-d", strtotime($consumo['record_date']))) == false)) {
                            if ((date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) >= $this->Rush_hour_model->_start_hour && date("H:i:s", strtotime($consumo['record_date'])) <= $this->Rush_hour_model->_end_hour)) {
                                $rows[$k]['c'][2]['v'] = (int) number_format($consumo['value'], 3, ".", "");
                            }
                            else {
                                $rows[$k]['c'][1]['v'] = (int) number_format($consumo['value'], 3, ".", "");
                            }
                        }
                        else {
                            $rows[$k]['c'][1]['v'] = (int) number_format($consumo['value'], 3, ".", "");
                        }
                    }
                }
            }
        }
        $data = array("cols" => $cols, "rows" => $rows);
        print_r(json_encode($data));
    }

    public function ajax_demandas_maximas_diarias_mes() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        //com a informação da unidade, é possivel encontrar o agente e assim, continuar coletando os dados
        $this->_process_unity();
        //pegando os horários de ponta
        $this->_process_rush_hour();
        //demanda contratada
        $this->_process_demand($this->input->post('date'));
        //pega os dias de feriados no ano
        $data['holiday'] = $this->_process_holiday_by_year($this->input->post('date'));

        //pega as telemetrias
        $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_fk_gauge    = $this->input->post('fk_gauge');
        $this->Telemetry_record_model->_record_date = $this->input->post('date');
        $this->Telemetry_record_model->_greatness   = "Demat";
        $telemetry_records                          = $this->Telemetry_record_model->fetch_demat();

        //colunas
        $col1            = array();
        $col1["id"]      = "";
        $col1["label"]   = "Dia";
        $col1["pattern"] = "";
        $col1["type"]    = "string";

        $col2            = array();
        $col2["id"]      = "";
        $col2["label"]   = "Fora Ponta";
        $col2["pattern"] = "";
        $col2["type"]    = "number";

        $col3            = array();
        $col3["id"]      = "";
        $col3["label"]   = "Ponta";
        $col3["pattern"] = "";
        $col3["type"]    = "number";

        $col4            = array();
        $col4["id"]      = "";
        $col4["label"]   = "Contratada Fora Ponta";
        $col4["pattern"] = "";
        $col4["type"]    = "number";

        $col5            = array();
        $col5["id"]      = "";
        $col5["label"]   = "Contratada Ponta";
        $col5["pattern"] = "";
        $col5["type"]    = "number";

        $cols = array($col1, $col2, $col3, $col4, $col5);

        $rows = array();
        for ($i = 1; $i <= date("t", strtotime($this->input->post('date'))); $i++) {
            $cel0["v"] = $i;
            $cel1["v"] = 0;
            $cel2["v"] = 0;
            $cel3["v"] = (int) $this->Demand_model->_demand_tip_out;
            $cel4["v"] = (int) $this->Demand_model->_end_demand;
            $row0["c"] = array($cel0, $cel1, $cel2, $cel3, $cel4);
            array_push($rows, $row0);
        }


        //insere os dados de telemetria
        if ($telemetry_records) {
            foreach ($telemetry_records as $consumo) {
                foreach ($rows as $k => $row) {
                    if ($row['c'][0]['v'] == date("d", strtotime($consumo['record_date']))) {
                        if ((date("H:i:s", strtotime($consumo['record_date'])) >= $this->Rush_hour_model->_start_hour && date("H:i:s", strtotime($consumo['record_date'])) <= $this->Rush_hour_model->_end_hour) && (date("N", strtotime($consumo['record_date'])) < 6 && $this->_is_holiday($data['holiday'], date("Y-m-d", strtotime($consumo['record_date']))) == false)) {
                            $rows[$k]['c'][2]['v'] = $consumo['value'] > $rows[$k]['c'][2]['v'] ? $consumo['value'] : $rows[$k]['c'][2]['v'];
                        }
                        else {
                            $rows[$k]['c'][1]['v'] = $consumo['value'] > $rows[$k]['c'][1]['v'] ? $consumo['value'] : $rows[$k]['c'][1]['v'];
                        }
                    }
                }
            }
        }
        $data = array("cols" => $cols, "rows" => $rows);
        print_r(json_encode($data));
    }

    public function ajax_consumo_posto_tarifario() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        //com a informação da unidade, é possivel encontrar o agente e assim, continuar coletando os dados
        $this->_process_unity();
        //pegando os horários de ponta
        $this->_process_rush_hour();
        //pega os dias de feriados no ano
        $data['holiday'] = $this->_process_holiday_by_year($this->input->post('date'));

        //pega as telemetrias
        $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_fk_gauge    = $this->input->post('fk_gauge');
        $this->Telemetry_record_model->_record_date = $this->input->post('date');
        $this->Telemetry_record_model->_greatness   = "Eneat";
        $telemetry_records                          = $this->Telemetry_record_model->fetch_eneat();

        //colunas
        $col1            = array();
        $col1["id"]      = "";
        $col1["label"]   = "Tipo";
        $col1["pattern"] = "";
        $col1["type"]    = "string";
        //
        $col2            = array();
        $col2["id"]      = "";
        $col2["label"]   = "Total";
        $col2["pattern"] = "";
        $col2["type"]    = "number";
        //
        $cols            = array($col1, $col2);

        $rows      = array();
        $cel0["v"] = "Fora Ponta";
        $cel1["v"] = 0;
        $row0["c"] = array($cel0, $cel1);
        array_push($rows, $row0);

        $cel0["v"] = "Ponta";
        $cel1["v"] = 0;
        $row0["c"] = array($cel0, $cel1);
        array_push($rows, $row0);


        //insere os dados de telemetria
        if ($telemetry_records) {
            foreach ($telemetry_records as $consumo) {
                if ((date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) >= $this->Rush_hour_model->_start_hour && date("H:i:s", strtotime($consumo['record_date'])) <= $this->Rush_hour_model->_end_hour) && (date("N", strtotime($consumo['record_date'])) < 6 && $this->_is_holiday($data['holiday'], date("Y-m-d", strtotime($consumo['record_date']))) == false)) {
                    $rows[1]['c'][1]['v'] ++;
                }
                else {
                    $rows[0]['c'][1]['v'] ++;
                }
            }
            $rows[1]['c'][1]['v'] = ($rows[1]['c'][1]['v'] * 100) / count($telemetry_records);
            $rows[0]['c'][1]['v'] = ($rows[0]['c'][1]['v'] * 100) / count($telemetry_records);
        }

        $data = array("cols" => $cols, "rows" => $rows);
        print_r(json_encode($data));
    }

    private function _process_unity() {
        $this->load->model("Unity_model");
        $this->Unity_model->_pk_unity = $this->input->post('fk_unity');
        return $this->Unity_model->read();
    }

    private function _process_rush_hour() {
        $this->load->model("Rush_hour_model");
        $this->Rush_hour_model->_status                     = $this->Rush_hour_model->_status_active;
        $this->Rush_hour_model->_fk_agent_power_distributor = $this->Unity_model->_fk_agent_power_distributor;
        return $this->Rush_hour_model->read();
    }

    private function _process_demand($date) {
        $this->load->model("Demand_model");
        $this->Demand_model->_status     = $this->Demand_model->_status_active;
        $this->Demand_model->_start_date = $date;
        $this->Demand_model->_fk_unity   = $this->input->post('fk_unity');
        $this->Demand_model->read();
    }

    private function _process_holiday($date) {
        $this->load->model("Holiday_model");
        $this->Holiday_model->_date_holiday = $date;
        $this->Holiday_model->_status       = $this->Holiday_model->_status_active;
        return $this->Holiday_model->read();
    }

    private function _process_holiday_by_year($date) {
        $return                       = array();
        $this->load->model("Holiday_model");
        $this->Holiday_model->_year   = date("Y", strtotime($date));
        $this->Holiday_model->_status = $this->Holiday_model->_status_active;
        $holiday                      = $this->Holiday_model->fetch();
        if ($holiday) {
            foreach ($holiday as $feriado) {
                $return[] = $feriado['date_holiday'];
            }
        }
        return $return;
    }

    private function _is_holiday($holiday, $date) {
        return in_array($date, $holiday);
    }

}
