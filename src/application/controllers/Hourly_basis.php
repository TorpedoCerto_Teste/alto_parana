<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hourly_basis extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Hourly_basis_model');
    }

    function index() {
        $data['previous_week'] = getWeeks(date("Y-m-d", strtotime('-1 week')), "saturday");

        if ($this->input->post('filter') == "true") {
            $this->Hourly_basis_model->_week = $this->input->post('year') . $this->input->post('month') . $this->input->post('week');
        }
        else {
            $this->Hourly_basis_model->_week = date("Ym", strtotime('-1 week')) . $data['previous_week'];
        }
        $this->Hourly_basis_model->_status = $this->Hourly_basis_model->_status_active;
        $data['list']                      = $this->Hourly_basis_model->fetch();
        $data['js_include']                = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            ';
        $data['css_include']               = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']              = 'hourly_basis/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $fetches = $this->Hourly_basis_model->fetch();
                if ($fetches) {
                    foreach ($fetches as $fetch) {
                        $this->_upsert($fetch);
                    }
                    redirect('hourly_basis');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/hourly_basis/create.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'hourly_basis/create';
        $this->load->view('includes/template', $data);
    }

    private function _upsert($data) {
        $this->Hourly_basis_model->_week            = $data['week'];
        $this->Hourly_basis_model->_date            = $data['date'];
        $this->Hourly_basis_model->_landing         = $data['landing'];
        $this->Hourly_basis_model->_hour_type       = $data['hour_type'];
        $this->Hourly_basis_model->_pk_hourly_basis = $data['pk_hourly_basis'];

        if ($data['landing'] == 'LEVE') {
            $this->Hourly_basis_model->_pld_south     = $this->input->post('south_soft');
            $this->Hourly_basis_model->_pld_southeast = $this->input->post('southeast_soft');
            $this->Hourly_basis_model->_pld_north     = $this->input->post('north_soft');
            $this->Hourly_basis_model->_pld_northeast = $this->input->post('northeast_soft');
        }
        elseif ($data['landing'] == 'MEDIO') {
            $this->Hourly_basis_model->_pld_south     = $this->input->post('south_medium');
            $this->Hourly_basis_model->_pld_southeast = $this->input->post('southeast_medium');
            $this->Hourly_basis_model->_pld_north     = $this->input->post('north_medium');
            $this->Hourly_basis_model->_pld_northeast = $this->input->post('northeast_medium');
        }
        elseif ($data['landing'] == 'PESADO') {
            $this->Hourly_basis_model->_pld_south     = $this->input->post('south_heavy');
            $this->Hourly_basis_model->_pld_southeast = $this->input->post('southeast_heavy');
            $this->Hourly_basis_model->_pld_north     = $this->input->post('north_heavy');
            $this->Hourly_basis_model->_pld_northeast = $this->input->post('northeast_heavy');
        }

        if ($data['pld_south'] <= 0) {
            $this->Hourly_basis_model->update();
        }
        elseif ($data['pld_south'] != $this->Hourly_basis_model->_pld_south || $data['pld_southeast'] != $this->Hourly_basis_model->_pld_southeast_soft || $data['pld_north'] != $this->Hourly_basis_model->_pld_north || $data['pld_northeast'] != $this->Hourly_basis_model->_pld_northeast) {
            //inativa o registro ativo
            $this->Hourly_basis_model->delete();
            $this->Hourly_basis_model->_status = $this->Hourly_basis_model->_status_active;
            $this->Hourly_basis_model->create();
        }
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('hourly_basis/index');
        }
        $this->Hourly_basis_model->_pk_hourly_basis = $this->uri->segment(3);
        $read                                       = $this->Hourly_basis_model->read();
        if (!$read) {
            redirect('hourly_basis/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Hourly_basis_model->update();
                redirect('hourly_basis/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'hourly_basis/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_hourly_basis') > 0) {
            $this->Hourly_basis_model->_pk_hourly_basis = $this->input->post('pk_hourly_basis');
            $this->Hourly_basis_model->delete();
        }
        redirect('hourly_basis/index');
    }

    function upload() {
        $data['error'] = false;

        $data['js_include']   = '<script src="' . base_url() . 'js/dropzone.js"></script>
                <script src="' . base_url() . 'web/js/hourly_basis/dropzone.js"></script>';
        $data['css_include']  = '<link href="' . base_url() . 'css/dropzone.css" rel="stylesheet">';
        $data['main_content'] = 'hourly_basis/upload';
        $this->load->view('includes/template', $data);
    }

    function dropzone() {
        $config['upload_path']   = './uploads/';
        $config['allowed_types'] = 'CSV|csv';
        $config['max_size']      = 10000;
        $config['file_name']     = "pld_" . date("YmdHis") . ".csv";

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $return = array(
                'error'   => true,
                'message' => strip_tags($this->upload->display_errors()));
        }
        else {
            $return = array(
                'error'   => false,
                'message' => $this->upload->data());
        }
        print json_encode($return);
    }

    function average_weekly() {
        $this->Hourly_basis_model->_week_start = date('Ym1');
        $this->Hourly_basis_model->_week_end   = date('Ym5');

        if ($this->input->post('filter')) {
            $this->form_validation->set_rules('year_start', 'Ano Inicial', 'trim|required|exact_length[4]');
            $this->form_validation->set_rules('month_start', 'Mês Inicial', 'trim|required|exact_length[2]');
            $this->form_validation->set_rules('week_start', 'Semana Inicial', 'trim|required|exact_length[1]');
            $this->form_validation->set_rules('year_end', 'Ano Final', 'trim|required|exact_length[4]');
            $this->form_validation->set_rules('month_end', 'Mês Final', 'trim|required|exact_length[2]');
            $this->form_validation->set_rules('week_end', 'Semana Final', 'trim|required|exact_length[1]');

            if ($this->form_validation->run() == TRUE) {
                $this->Hourly_basis_model->_week_start = $this->input->post('year_start') . $this->input->post('month_start') . $this->input->post('week_start');
                $this->Hourly_basis_model->_week_end   = $this->input->post('year_end') . $this->input->post('month_end') . $this->input->post('week_end');
            }
        }

        $data['list'] = $this->Hourly_basis_model->fetch_by_start_end_week();
        if ($data['list']) {
            foreach ($data['list'] as $item) {
                //south
                $data['graph']['S'][$item['week']]  = round($item['avg_south']);
                //southeast
                $data['graph']['SE'][$item['week']] = round($item['avg_southeast']);
                //north
                $data['graph']['N'][$item['week']]  = round($item['avg_north']);
                //northeast
                $data['graph']['NE'][$item['week']] = round($item['avg_northeast']);
            }
            $i    = 1;
            $text = "[";
            foreach ($data['graph'] as $region => $avgs) {
                $text .= '{ data: [';
                foreach ($avgs as $week => $avg) {
                    $arr[] = '[' . $week . ', ' . $avg . ']';
                }
                $text .= join(",", $arr);
                $text .= ' ], label: \'' . $region . '\' }';
                if (count($data['graph']) != $i) {
                    $text .= ',';
                }
                ++$i;
            }
            $data['graph'] = $text;
//            foreach($data['graph'] as $regiao) {
//                $text .= '[{';
//                foreach
//                $text .= 'data: [ ['.$item['week'].', '.round($item['avg_south']).'] ],';
//                $text .= 'label: \'S\' },';
//                $text .= '{data: [ ['.$item['week'].', '.round($item['avg_southeast']).'] ],';
//                $text .= 'label: \'SE\' },';
//                $text .= '{data: [ ['.$item['week'].', '.round($item['avg_north']).'] ],';
//                $text .= 'label: \'N\' },';
//                $text .= '{data: [ ['.$item['week'].', '.round($item['avg_northeast']).'] ],';
//                $text .= 'label: \'NE\'';
//                $text .= '}],';
//            }
//            $data['graph'] = $text;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.resize.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.tooltip.min.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.pie.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.selection.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.stack.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.crosshair.js"></script>
            <script src="' . base_url() . 'web/js/hourly_basis/average_weekly.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'hourly_basis/average_weekly';
        $this->load->view('includes/template', $data);
    }

    private function _validate_form() {
        $this->form_validation->set_rules('from', 'Data Inicial', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('to', 'Data Final', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('south_heavy', 'Sul Pesado', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('south_medium', 'Sul Médio', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('south_soft', 'Sul Leve', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('southeast_heavy', 'Sul Pesado', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('southeast_medium', 'Sul Médio', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('southeast_soft', 'Sul Leve', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('north_heavy', 'Sul Pesado', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('north_medium', 'Sul Médio', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('north_soft', 'Sul Leve', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('northeast_heavy', 'Sul Pesado', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('northeast_medium', 'Sul Médio', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('northeast_soft', 'Sul Leve', 'trim|required|greater_than[0]');
    }

    private function _fill_model() {
        $this->Hourly_basis_model->_date_start = date("Y-m-d 00:00:00", strtotime(human_date_to_date($this->input->post('from'))));
        $this->Hourly_basis_model->_date_end   = date("Y-m-d 23:59:59", strtotime(human_date_to_date($this->input->post('to'))));
        $this->Hourly_basis_model->_status     = $this->Hourly_basis_model->_status_active;
    }

}
