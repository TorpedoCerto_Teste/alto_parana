<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Return_investment_migration_unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Return_investment_migration_unity_model');
    }

    function index() {
        $this->Return_investment_migration_unity_model->_status = $this->Return_investment_migration_unity_model->_status_active;
        $data['list']                                           = $this->Return_investment_migration_unity_model->fetch();
        $data['js_include']                                     = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                                    = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                                   = 'return_investment_migration_unity/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Return_investment_migration_unity_model->_created_at = date("Y-m-d H:i:s");
                $create                                                     = $this->Return_investment_migration_unity_model->create();
                if ($create) {
                    redirect('return_investment_migration_unity/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'return_investment_migration_unity/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('return_investment_migration_unity/index');
        }
        $this->Return_investment_migration_unity_model->_pk_return_investment_migration_unity = $this->uri->segment(3);
        $read                                                                                 = $this->Return_investment_migration_unity_model->read();
        if (!$read) {
            redirect('return_investment_migration_unity/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Return_investment_migration_unity_model->update();
                redirect('return_investment_migration_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'return_investment_migration_unity/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('return_investment_migration_unity/index');
        }
        $this->Return_investment_migration_unity_model->_pk_return_investment_migration_unity = $this->uri->segment(3);
        $read                                                                                 = $this->Return_investment_migration_unity_model->read();
        if (!$read) {
            redirect('return_investment_migration_unity/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Return_investment_migration_unity_model->upsert();
                redirect('return_investment_migration_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'return_investment_migration_unity/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_return_investment_migration_unity') > 0) {
            $this->Return_investment_migration_unity_model->_pk_return_investment_migration_unity = $this->input->post('pk_return_investment_migration_unity');
            $this->Return_investment_migration_unity_model->delete();
        }
        redirect('return_investment_migration_unity/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_return_investment_migration', 'fk_return_investment_migration', 'trim|required');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('total_invested', 'total_invested', 'trim|required');
        $this->form_validation->set_rules('month_return', 'month_return', 'trim|required');
        $this->form_validation->set_rules('month_return_percent', 'month_return_percent', 'trim|required');
        $this->form_validation->set_rules('cumulative_return', 'cumulative_return', 'trim|required');
        $this->form_validation->set_rules('cumulative_return_percent', 'cumulative_return_percent', 'trim|required');
    }

    private function _fill_model() {
        $this->Return_investment_migration_unity_model->_fk_return_investment_migration = $this->input->post('fk_return_investment_migration');
        $this->Return_investment_migration_unity_model->_fk_unity                       = $this->input->post('fk_unity');
        $this->Return_investment_migration_unity_model->_total_invested                 = $this->input->post('total_invested');
        $this->Return_investment_migration_unity_model->_month_return                   = $this->input->post('month_return');
        $this->Return_investment_migration_unity_model->_month_return_percent           = $this->input->post('month_return_percent');
        $this->Return_investment_migration_unity_model->_cumulative_return              = $this->input->post('cumulative_return');
        $this->Return_investment_migration_unity_model->_cumulative_return_percent      = $this->input->post('cumulative_return_percent');
        $this->Return_investment_migration_unity_model->_status                         = $this->Return_investment_migration_unity_model->_status_active;
    }

}
