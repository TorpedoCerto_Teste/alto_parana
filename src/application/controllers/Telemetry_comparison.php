<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Telemetry_comparison extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
    }

    function index() {
        $data['date'] = date("Y-m-d");

        if ($this->input->post('filter') == 'true') {
            if ($this->input->post('month') && $this->input->post('year')) {
                $data['date'] = $this->input->post('year')."-".$this->input->post('month')."-01";
            }

            $data['gauges']     = $this->_fetch_gauge_by_fk_agent();
            $data['comparison'] = $this->_process_comparison();
            if ($this->input->post("export_excel") == 'true') {
                $this->_export_excel($data['comparison']);
                $return              = false;
                $this->disableLayout = TRUE;
                $this->output->set_content_type('application/json');
            }
        }
        $data['js_include']  = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'web/js/telemetry_comparison/index.js"></script>    
            ';
        $data['css_include'] = '
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            ';

        //pega a lista de agentes
        $data['agents'] = $this->_process_agents();


        $data['main_content'] = 'telemetry_comparison/index';
        $this->load->view('includes/template', $data);
    }

    function ajax_fetch_gauge_by_fk_agent() {
        $return              = false;
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $return              = $this->_fetch_gauge_by_fk_agent();
        print_r(json_encode($return));
    }

    private function _fetch_gauge_by_fk_agent() {
        $this->load->model('Gauge_model');
        $this->Gauge_model->_fk_agent = $this->input->post("fk_agent");
        return $this->Gauge_model->fetch_by_agent();
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_comparison() {
        $telemetry_records = $this->_process_telemetry_records();
        if ($telemetry_records) {
            $gauge_records = $this->_process_gauge_records();
            
            if ($gauge_records) {
                return $this->_compare($telemetry_records, $gauge_records);
            }
        }
        return false;
    }

    private function _process_telemetry_records() {
        $this->load->model("Telemetry_record_model");
        $this->Telemetry_record_model->_fk_gauge    = $this->input->post("fk_gauge");
        $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_record_date = $this->input->post("year") . "-" . $this->input->post("month") . "-01";
        
        return $this->Telemetry_record_model->fetch_eneat();
    }

    private function _process_gauge_records() {
        $this->load->model("Gauge_record_model");
        $this->Gauge_record_model->_status      = $this->Gauge_record_model->_status_active;
        $this->Gauge_record_model->_fk_gauge    = $this->input->post("fk_gauge");
        $this->Gauge_record_model->_record_date = $this->input->post("year") . "-" . $this->input->post("month") . "-01";
        return $this->Gauge_record_model->fetch_by_report();
    }

    private function _compare($telemetry_records, $gauge_records) {
        $return = $this->_generate_records();

        foreach ($telemetry_records as $telemetry_record) {
            $return[date("j", strtotime($telemetry_record['record_date']))][date("G", strtotime($telemetry_record['record_date']))]['way2']['value']   = $telemetry_record['value'];
            $return[date("j", strtotime($telemetry_record['record_date']))][date("G", strtotime($telemetry_record['record_date']))]['way2']['quality'] = $telemetry_record['quality'];
        }
        foreach ($gauge_records as $gauge_record) {
            $return[date("j", strtotime($gauge_record['record_date']))][date("G", strtotime($gauge_record['record_date']))]['scde']['active_power_consumption'] = $gauge_record['active_power_consumption'];
            $return[date("j", strtotime($gauge_record['record_date']))][date("G", strtotime($gauge_record['record_date']))]['scde']['reason_situation']         = $gauge_record['reason_situation'];
        }
        return $return;
    }

    private function _generate_records() {
        $return = array();
        for ($dia = 1; $dia <= date("t", strtotime($this->input->post("year") . "-" . $this->input->post("month") . "-01")); $dia++) {
            for ($hora = 0; $hora <= 23; $hora++) {
                $return[$dia][$hora]['way2']['value']                    = 0;
                $return[$dia][$hora]['way2']['quality']                  = 1;
                $return[$dia][$hora]['scde']['active_power_consumption'] = 0;
                $return[$dia][$hora]['scde']['reason_situation']         = "";
            }
        }
        return $return;
    }
    
    private function _export_excel($comparison) {
        $file = "Comparacao_Telemetrias.xls";
        
        $html = '';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>Data</td>';
        $html .= '<td>Way2 Valor</td>';
        $html .= '<td>SCDE Valor</td>';
        $html .= '</tr>';
        if ($comparison) {
            foreach ($comparison as $dia => $horas) {
                foreach ($horas as $hora => $resultado) {
                    $html .= '<tr>';
                    $html .= '<td>'.$dia.'/'.$this->input->post("month").'/'.$this->input->post("year").' '.$hora.':00:00</td>';
                    $html .= '<td>'.number_format($resultado['way2']['value'], 2, ".", "").'</td>';
                    $html .= '<td>'.number_format($resultado['scde']['active_power_consumption'], 2, ".", "").'</td>';
                    $html .= '</tr>';
                }
            }
        }
        $html .= '</table>';
        // Configurações header para forçar o download
        header ("Expires: Mon, 10 Jan 2997 05:00:00 GMT");
        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-type: application/x-msexcel");
        header ("Content-Disposition: attachment; filename=\"{$file}\"" );
        header ("Content-Description: Alto Parana Energia" );
        // Envia o conteúdo do arquivo
        echo $html;
        exit;        
    }

}
