<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proinfa extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->load->model("Unity_model");
        $this->Unity_model->_pk_unity = $this->uri->segment(4, 0);
        $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
        $read                         = $this->Unity_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->load->model('Proinfa_model');
    }

    function index() {
        $this->Proinfa_model->fk_unity = $this->Unity_model->_pk_unity;
        $data['proinfas']              = $this->Proinfa_model->fetch();
        $data['js_include']            = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']           = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']          = 'proinfa/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error']                  = false;
        $this->Proinfa_model->_fk_unity = $this->Unity_model->_pk_unity;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('year', 'year', 'trim|required|exact_length[4]');
            if ($this->form_validation->run() == TRUE) {
                $this->Proinfa_model->_year = $this->input->post('year');
                for ($i = 1; $i < 13; $i++) {
                    $value_mwh = str_replace(',', '.', $this->input->post('value_mwh_' . $i));

                    if ($value_mwh > 0) {
                        $this->Proinfa_model->_month     = $i;
                        $this->Proinfa_model->_value_mwh = $value_mwh;
                        $this->Proinfa_model->_status    = $this->Proinfa_model->_status_active;
                        $this->Proinfa_model->upsert();
                    }
                }
                redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/proinfa');
            }
            $data['error'] = true;
        }

        //pega os proinfas já cadastrados
        $this->Proinfa_model->_year = date('Y');
        $data['proinfas']           = $this->_get_proinfas();

        $data['js_include']   = ' 
            <script src="'.base_url().'js/mascara.js"></script>
            <script src="'.base_url().'web/js/proinfa/create.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'proinfa/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;

        $this->Proinfa_model->_fk_unity   = $this->Unity_model->_pk_unity;
        $this->Proinfa_model->_pk_proinfa = $this->uri->segment(5, 0);
        $read                             = $this->Proinfa_model->read();
        if (!$read) {
            redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/proinfa');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('year', 'year', 'trim|required');
            $this->form_validation->set_rules('month', 'month', 'trim');
            $this->form_validation->set_rules('value_mwh', 'value_mwh', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Proinfa_model->_year      = $this->input->post('year');
                $this->Proinfa_model->_month     = $this->input->post('month');
                $this->Proinfa_model->_value_mwh = str_replace(',', '.', $this->input->post('value_mwh'));
                $update                          = $this->Proinfa_model->update();
                redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/proinfa');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '<script src="'.base_url().'js/mascara.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'proinfa/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_proinfa') > 0) {
            $this->Proinfa_model->_pk_proinfa = $this->input->post('pk_proinfa');
            $this->Proinfa_model->delete();
        }
        redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/proinfa');
    }

    function ajax() {
        //pega os proinfas já cadastrados
        $this->Proinfa_model->_fk_unity = $this->Unity_model->_pk_unity;
        $this->Proinfa_model->_year     = $this->uri->segment(5, 0);
        $proinfas                       = $this->_get_proinfas(1);
        
        print_r(json_encode($proinfas));
    }

    private function _get_proinfas($return_comma = 0) {
        $return = array();
        for ($i = 1; $i < 13; $i++) {
            $return[$i]['value_mwh'] = 0.00;
        }
        $proinfas = $this->Proinfa_model->fetch();
        if ($proinfas) {
            foreach ($proinfas as $proinfa) {
                if ($return_comma) {
                    $return[$proinfa['month']]['value_mwh'] = number_format($proinfa['value_mwh'], 3, ',', '');
                }
                else {
                    $return[$proinfa['month']]['value_mwh'] = number_format($proinfa['value_mwh'], 3, '.', '');
                }
            }
        }
        return $return;
    }

}
