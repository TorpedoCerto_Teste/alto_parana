<?php

/**
  pk_group_agent_contact
  fk_agent
  fk_contact
  fk_group

 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_contact extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Agent_contact_model');
    }

    function index() {
        
        //pega os dados
        $this->load->model("Agent_contact_model");
        $this->Agent_contact_model->_status = $this->Agent_contact_model->_status_active;
        $fetch                              = $this->Agent_contact_model->fetch();
        if (!$fetch) {
            redirect("agent");
        }
        
        //separar em agentes com contatos e contatos que pertencem a agentes
        $agents = array();
        $contacts = array();
        $agents_list = [];

        foreach ($fetch as $agentscontacts) {
            $agents_list[$agentscontacts['pk_agent']] = $agentscontacts['community_name'];
            $agents[$agentscontacts['pk_agent']]['agent']                                   = array(
                'community_name' => $agentscontacts['community_name']
            );
            $agents[$agentscontacts['pk_agent']]['contacts'][$agentscontacts['pk_contact']] = array(
                'name'    => $agentscontacts['name'],
                'surname' => $agentscontacts['surname'],
                'email'   => $agentscontacts['email']
            );

            $contacts[$agentscontacts['pk_contact']]['contact']                             = array(
                'name'    => $agentscontacts['name'],
                'surname' => $agentscontacts['surname'],
                'email'   => $agentscontacts['email']
            );
            $contacts[$agentscontacts['pk_contact']]['agents'][$agentscontacts['pk_agent']] = array(
                'community_name' => $agentscontacts['community_name']
            );
        }

        $data['agents']   = $agents;
        $data['contacts'] = $contacts;
        asort($agents_list);
        $data['agents_list'] = $agents_list;
        
        $data['js_include']                         = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']                        = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">                                
            ';
        $data['main_content']                       = 'agent_contact/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        //pega os dados do agente em questão
        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect("agent");
        }

        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Agent_contact_model->create();
                if ($create) {
                    redirect('agent/view/' . $this->Agent_model->_pk_agent . '/grupos');
                }
            }
            $data['error'] = true;
        }

        //pega a lista de grupos
        $this->load->model("Group_model");
        $this->Group_model->_status = $this->Group_model->_status_active;
        $data['groups']             = $this->Group_model->fetch();


        //pega a lista de contatos
        $this->load->model("Contact_model");
        $this->Contact_model->_status = $this->Contact_model->_status_active;
        $data['contacts']             = array();
        $contacts                     = $this->Contact_model->fetch_with_agent();
        if ($contacts) {
            foreach ($contacts as $contact) {
                $data['contacts'][$contact['community_name']][] = $contact;
            }
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'group_agent_contact/create';
        $this->load->view('includes/template', $data);
    }
    
    function create_multi() {
        $data['error'] = false;

        if ($this->input->post('fk_agent', 0) > 0 && $this->input->post('contacts', false)) {
            $this->Agent_contact_model->_fk_agent = $this->input->post('fk_agent');
            $contacts = $this->input->post('contacts');
            foreach ($contacts as $fk_contact) {
                $this->Agent_contact_model->_fk_contact = $fk_contact;
                $this->Agent_contact_model->_status = $this->Agent_contact_model->_status_active;
                $this->Agent_contact_model->create();
            }
            redirect('agent/view/' . $this->Agent_contact_model->_fk_agent . '/contatos');
        }
        else {
            redirect('agent');
        }
    }
    

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('group_agent_contact/index');
        }
        $this->Agent_contact_model->_pk_group_agent_contact = $this->uri->segment(3);
        $read                                                     = $this->Agent_contact_model->read();
        if (!$read) {
            redirect('index');
        }

        if ($this->input->post('fk_contact') > 0) {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Agent_contact_model->update();
                redirect('group_agent_contact/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'group_agent_contact/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->Agent_contact_model->_fk_agent   = $this->uri->segment(3);
            $this->Agent_contact_model->_fk_contact = $this->input->post('fk_contact');
            $this->Agent_contact_model->delete();

            $groups = $this->input->post('groups');
            if (!empty($groups)) {
                foreach ($groups as $fk_group) {
                    $this->Agent_contact_model->_fk_group = $fk_group;
                    $this->Agent_contact_model->create();
                }
            }
        }
        redirect('agent/view/' . $this->Agent_contact_model->_fk_agent . '/grupos');
    }

    function delete() {
        if ($this->input->post('pk_group_agent_contact') > 0) {
            $this->Agent_contact_model->_pk_group_agent_contact = $this->input->post('pk_group_agent_contact');
            $this->Agent_contact_model->delete();
        }
        redirect('agent/view/' . $this->uri->segment(3, 0) . '/grupos');
    }
    
    function deattach() {
        if ($this->input->post('deattach', false) == 'true') {
            $this->Agent_contact_model->_fk_agent   = $this->input->post('fk_agent_deattach');
            $this->Agent_contact_model->_fk_contact = $this->input->post('fk_contact_deattach');
            $this->Agent_contact_model->deattach();
            redirect('agent/view/' . $this->Agent_contact_model->_fk_agent . '/contatos');
        }
    }

    function export() {
        $return = [];

        $this->Agent_contact_model->_fk_agent   = $this->input->post('fk_agent'); 
        $data_export = $this->Agent_contact_model->export();

        if ($data_export && !empty($data_export)) {
            foreach ($data_export as $data) {
                $contact_key = md5($data['email']);
                $return[$contact_key]['name'] = $data['name'];
                $return[$contact_key]['surname'] = $data['surname']; 
                $return[$contact_key]['email'] = $data['email'];
                $return[$contact_key]['agent'] = $data['agent_name'];

                $group_key = md5($data['group_short_name']);
                $group = $data['group'] && $data['group_short_name'] ? 
                    $data['group']." (".$data['group_short_name'].")" :
                    "";
                $return[$contact_key]['groups'][$group_key] = $group;

                $unity_key = md5($data['unity_name']);
                $return[$contact_key]['unities'][$unity_key] = $data['unity_name'];
            }

            sort($return);
            $return = array_map(function($entry) {
                sort($entry['groups']);
                sort($entry['unities']);
                return $entry;
            }, $return); 
            
            //processar e transformar em csv
            $csv = [];
            foreach ($return as $k => $data) {
                $csv[$k] = [
                    "name" => $data['name'],
                    "surname" => $data['surname'],
                    "email" => $data['email'],
                    "agent" => $data['agent'],
                    "groups" => null,
                    "unities" => null,

                ]; 
                $limit = count($data['groups']) > count($data['unities']) ? count($data['groups']) : count($data['unities']);
                for ($i=0; $i<$limit; $i++) {
                    $csv[$k]['groups_unities'][] = [
                        null,
                        null,
                        null,
                        null,
                        $data['groups'][$i] ?? null,
                        $data['unities'][$i] ?? null
                    ];
                }
            }

            header('Content-Type: text/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename=data_export.csv');
    
            // Open output stream for writing
            $output = fopen('php://output', 'w');
    
            // Write the header row
            fputcsv($output, ['Name', 'Surname', 'Email', 'Agent', 'Groups', 'Unities']);
    
            // Write data rows
            foreach ($csv as $row) {
   
                // Write the row to CSV
                fputcsv($output, [
                    $row['name'],
                    mb_strtoupper($row['surname'], "UTF-8"),
                    $row['email'],
                    $row['agent'],
                    $row['groups'],
                    $row['unities']
                ]);

                foreach ($row['groups_unities'] as $groups_unities) {
                    fputcsv($output, $groups_unities);
                }
            }
    
            // Close the output stream
            fclose($output);
            exit;

        }   


    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_group', 'fk_group', 'trim|required');
        $this->form_validation->set_rules('fk_contact', 'fk_contact', 'trim|required');
    }

    private function _fill_model() {
        $this->Agent_contact_model->_fk_group   = $this->input->post('fk_group');
        $this->Agent_contact_model->_fk_contact = $this->input->post('fk_contact');
        $this->Agent_contact_model->_fk_agent   = $this->Agent_model->_pk_agent;
        $this->Agent_contact_model->_status     = $this->Agent_contact_model->_status_active;
    }

    function ajax_fetch_by_contact() {
        $this->Agent_contact_model->_status     = $this->Agent_contact_model->_status_active;
        $this->Agent_contact_model->_fk_contact = $this->input->post('fk_contact');
        $group_agent_contacts                         = $this->Agent_contact_model->fetch();
        print_r(json_encode($group_agent_contacts));
    }

}
