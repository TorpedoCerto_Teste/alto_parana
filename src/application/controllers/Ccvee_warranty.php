<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_warranty extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ccvee_warranty_model');
    }

    function create() {
        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('fk_modality_warranty', 'fk_modality_warranty', 'trim|required');
            $this->form_validation->set_rules('year', 'year', 'trim');
            $this->form_validation->set_rules('deadline_status', 'deadline_status', 'trim');
            $this->form_validation->set_rules('customer_communication_status', 'customer_communication_status', 'trim');
            $this->form_validation->set_rules('start_date', 'start_date', 'trim');
            $this->form_validation->set_rules('end_date', 'end_date', 'trim');
            $this->form_validation->set_rules('deadline_contract_presentation', 'deadline_contract_presentation', 'trim');
            $this->form_validation->set_rules('presentation_date', 'presentation_date', 'trim');
            $this->form_validation->set_rules('billing_cycles', 'billing_cycles', 'trim');

            if ($this->form_validation->run() == TRUE) {

                $this->Ccvee_warranty_model->_fk_ccvee_general               = $this->uri->segment(3);
                $this->Ccvee_warranty_model->_fk_modality_warranty           = $this->input->post('fk_modality_warranty');
                $this->Ccvee_warranty_model->_year                           = $this->input->post('year');
                $this->Ccvee_warranty_model->_deadline_status                = $this->input->post('deadline_status');
                $this->Ccvee_warranty_model->_customer_communication_status  = $this->input->post('customer_communication_status');
                $this->Ccvee_warranty_model->_start_date                     = human_date_to_date($this->input->post('start_date'));
                $this->Ccvee_warranty_model->_end_date                       = human_date_to_date($this->input->post('end_date'));
                $this->Ccvee_warranty_model->_deadline_contract_presentation = human_date_to_date($this->input->post('deadline_contract_presentation'));
                $this->Ccvee_warranty_model->_presentation_date              = human_date_to_date($this->input->post('presentation_date'));
                $this->Ccvee_warranty_model->_billing_cycles                 = $this->input->post('billing_cycles');
                $this->Ccvee_warranty_model->_status                         = $this->Ccvee_warranty_model->_status_active;
                $create                                                      = $this->Ccvee_warranty_model->create();
            }
            redirect('ccvee_general/view/' . $this->Ccvee_warranty_model->_fk_ccvee_general . '/garantias');
        }
    }

    function delete() {
        if ($this->input->post('pk_ccvee_warranty') > 0) {
            $this->Ccvee_warranty_model->_pk_ccvee_warranty = $this->input->post('pk_ccvee_warranty');
            $this->Ccvee_warranty_model->delete();
        }
        redirect('ccvee_general/view/' . $this->uri->segment(3) . '/garantias');
    }

}
