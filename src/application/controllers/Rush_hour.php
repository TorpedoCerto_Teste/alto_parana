<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rush_hour extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Rush_hour_model');
    }

    function index() {
        $this->Rush_hour_model->_status = $this->Rush_hour_model->_status_active;
        $data['list']                   = $this->Rush_hour_model->fetch();
        $data['js_include']             = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']            = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']           = 'rush_hour/index';
        $this->load->view('includes/template', $data);
    }

    /**
     * 
     */
    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Rush_hour_model->create();
                if ($create) {
                    redirect('rush_hour/index');
                }
            }
            $data['error'] = true;
        }

        $this->load->model('Agent_model');
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $data['power_distributors']        = $this->Agent_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/rush_hour/create.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'rush_hour/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('rush_hour/index');
        }

        $this->Rush_hour_model->_pk_rush_hour = $this->uri->segment(3);
        $read                                 = $this->Rush_hour_model->read();
        if (!$read) {
            redirect('rush_hour/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Rush_hour_model->update();
                redirect('rush_hour/index');
            }
            $data['error'] = true;
        }

        $this->load->model('Agent_model');
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $data['power_distributors']        = $this->Agent_model->fetch();


        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/rush_hour/create.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'rush_hour/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('rush_hour/index');
        }
        $this->Rush_hour_model->_pk_rush_hour = $this->uri->segment(3);
        $read                                 = $this->Rush_hour_model->read();
        if (!$read) {
            redirect('rush_hour/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Rush_hour_model->upsert();
                redirect('rush_hour/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'rush_hour/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_rush_hour') > 0) {
            $this->Rush_hour_model->_pk_rush_hour = $this->input->post('pk_rush_hour');
            $this->Rush_hour_model->delete();
        }
        redirect('rush_hour/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_agent_power_distributor', 'fk_agent_power_distributor', 'trim|required');
        $this->form_validation->set_rules('start_hour', 'start_hour', 'trim|required');
        $this->form_validation->set_rules('end_hour', 'end_hour', 'trim');
        $this->form_validation->set_rules('current', 'current', 'trim');
    }

    private function _fill_model() {
        $this->Rush_hour_model->_fk_agent_power_distributor = $this->input->post('fk_agent_power_distributor');
        $this->Rush_hour_model->_start_hour                 = $this->input->post('start_hour');
        $this->Rush_hour_model->_end_hour                   = $this->input->post('current') == "1" ? null : $this->input->post('end_hour');
        $this->Rush_hour_model->_current                    = $this->input->post('current') == "1" ? 1 : 0;
        $this->Rush_hour_model->_status                     = $this->Rush_hour_model->_status_active;
    }

}
