<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Group extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Group_model');
    }

    function index() {
        $this->Group_model->_status = $this->Group_model->_status_active;
        $data['list']               = $this->Group_model->fetch();
        $data['js_include']         = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']        = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']       = 'group/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Group_model->create();
                if ($create) {
                    if ($this->uri->segment(3,0) > 0) {
                        redirect('agent/view/'.$this->uri->segment(3).'/grupos');
                    }
                    redirect('group/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'group/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('group/index');
        }
        $this->Group_model->_pk_group = $this->uri->segment(3);
        $read                         = $this->Group_model->read();
        if (!$read) {
            redirect('group/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Group_model->update();
                redirect('group/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'group/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('group/index');
        }
        $this->Group_model->_pk_group = $this->uri->segment(3);
        $read                         = $this->Group_model->read();
        if (!$read) {
            redirect('group/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Group_model->upsert();
                redirect('group/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'group/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_group') > 0) {
            $this->Group_model->_pk_group = $this->input->post('pk_group');
            $this->Group_model->delete();
        }
        redirect('group/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('group_short_name', 'group_short_name', 'trim|required');
        $this->form_validation->set_rules('group', 'group', 'trim|required');
    }

    private function _fill_model() {
        $this->Group_model->_group            = mb_strtoupper($this->input->post('group'), 'UTF-8');
        $this->Group_model->_group_short_name = mb_strtoupper($this->input->post('group_short_name'), 'UTF-8');
        $this->Group_model->_status           = $this->Group_model->_status_active;
    }

}
