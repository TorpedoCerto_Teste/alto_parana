<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar_agent extends Public_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Calendar_agent_model');
    }

    function index() {
        $this->Calendar_agent_model->_status = $this->Calendar_agent_model->_status_active;
        $data['list']                        = $this->Calendar_agent_model->fetch();
        $data['js_include']                  = '
            <script type="text/javascript" src="' . base_url() . 'assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
            ';
        $data['css_include']                 = '
            <link rel="stylesheet" href="' . base_url() . 'assets/plugins/DataTables/media/css/DT_bootstrap.css" />
            ';
        $data['main_content']                = 'calendar_agent/index';
        $this->load->view('template/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Calendar_agent_model->_created_at = date("Y-m-d H:i:s");
                $create                                  = $this->Calendar_agent_model->create();
                if ($create) {
                    redirect('calendar_agent/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar_agent/create';
        $this->load->view('template/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('calendar_agent/index');
        }
        $this->Calendar_agent_model->_pk_calendar_agent = $this->uri->segment(3);
        $read                                           = $this->Calendar_agent_model->read();
        if (!$read) {
            redirect('calendar_agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Calendar_agent_model->update();
                redirect('calendar_agent/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar_agent/update';
        $this->load->view('template/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('calendar_agent/index');
        }
        $this->Calendar_agent_model->_pk_calendar_agent = $this->uri->segment(3);
        $read                                           = $this->Calendar_agent_model->read();
        if (!$read) {
            redirect('calendar_agent/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Calendar_agent_model->upsert();
                redirect('calendar_agent/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar_agent/upsert';
        $this->load->view('template/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_calendar_agent') > 0) {
            $this->Calendar_agent_model->_pk_calendar_agent = $this->input->post('pk_calendar_agent');
            $this->Calendar_agent_model->delete();
        }
        redirect('calendar_agent');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_calendar', 'fk_calendar', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
    }

    private function _fill_model() {
        $this->Calendar_agent_model->_fk_calendar = preg_replace("([^\d]*)", "", $this->input->post('fk_calendar'));
        $this->Calendar_agent_model->_fk_agent    = preg_replace("([^\d]*)", "", $this->input->post('fk_agent'));
    }

    public function ajax_set_done() {
        $this->disableLayout                      = TRUE;
        $this->output->set_content_type('application/json');
        $this->Calendar_agent_model->_fk_calendar = $this->input->post("pk_calendar");
        $this->Calendar_agent_model->_fk_agent    = $this->session->userdata("fk_agent");
        $this->Calendar_agent_model->_status      = $this->input->post("check") == 1 ? $this->Calendar_agent_model->_status_active : $this->Calendar_agent_model->_status_inactive;
        $this->Calendar_agent_model->_created_at  = date("Y-m-d H:i:s");
        $this->Calendar_agent_model->upsert();
    }

}
