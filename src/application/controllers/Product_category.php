<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_category extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Product_category_model');
    }

    function index() {
        $this->Product_category_model->_status = $this->Product_category_model->_status_active;
        $data['list']                          = $this->Product_category_model->fetch();
        $data['js_include']                    = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']                   = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                  = 'product_category/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error']  = false;
        $data['exists'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('category', 'category', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                //VERIFICAR SE CATEGORIA JÁ EXISTE, CASO EXISTA, MOSTRAR MENSAGEM DE ERRO
                $this->Product_category_model->_category = mb_strtoupper($this->input->post('category'), 'UTF-8');
                $this->Product_category_model->_status   = $this->Product_category_model->_status_active;
                if ($this->Product_category_model->find_by_category()) {
                    $data['exists'] = true;
                } else {
                    $create = $this->Product_category_model->create();
                    if ($create) {
                        if ($this->input->post('insert_product')) {
                            redirect('product/create/' . $create);
                        }
                        redirect('product_category/index');
                    }
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'product_category/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        $data['exists'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('product_category/index');
        }
        $this->Product_category_model->_pk_product_category = $this->uri->segment(3);
        $read                                               = $this->Product_category_model->read();
        if (!$read) {
            redirect('product_category/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('category', 'category', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $old_category = $this->Product_category_model->_category;
                //VERIFICAR SE CATEGORIA JÁ EXISTE, CASO EXISTA, MOSTRAR MENSAGEM DE ERRO
                $this->Product_category_model->_category = mb_strtoupper($this->input->post('category'), 'UTF-8');
                $this->Product_category_model->_status   = $this->Product_category_model->_status_active;
                if ($old_category != $this->Product_category_model->_category && $this->Product_category_model->find_by_category()) {
                    $data['exists'] = true;
                } else {
                    $update = $this->Product_category_model->update();
                    redirect('product_category/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'product_category/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_product_category') > 0) {
            $this->Product_category_model->_pk_product_category = $this->input->post('pk_product_category');
            $this->Product_category_model->delete();
        }
        redirect('product_category/index');
    }

}
