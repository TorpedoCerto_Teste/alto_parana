<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Category_model');
    }

    function index() {
        $this->Category_model->_status = $this->Category_model->_status_active;
        $data['list']                  = $this->Category_model->fetch();
        $data['js_include']            = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']           = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']          = 'category/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('category', 'category', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Category_model->_category = mb_strtoupper($this->input->post('category'), 'UTF-8');
                $this->Category_model->_status   = $this->Category_model->_status_active;
                $create                          = $this->Category_model->create();
                if ($create) {
                    redirect('category/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'category/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('category/index');
        }
        $this->Category_model->_pk_category = $this->uri->segment(3);
        $read                               = $this->Category_model->read();
        if (!$read) {
            redirect('category/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('category', 'category', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Category_model->_category = mb_strtoupper($this->input->post('category'), 'UTF-8');
                $update                          = $this->Category_model->update();
                redirect('category/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'category/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_category') > 0) {
            $this->Category_model->_pk_category = $this->input->post('pk_category');
            $this->Category_model->delete();
        }
        redirect('category/index');
    }

}
