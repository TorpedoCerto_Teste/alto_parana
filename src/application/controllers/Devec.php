<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Devec extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Devec_model');
    }

    function index() {
        $this->Devec_model->_status = $this->Devec_model->_status_active;
        $data['list']               = $this->Devec_model->fetch();
        $data['js_include']         = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']        = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']       = 'devec/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Devec_model->_created_at = date("Y-m-d H:i:s");
                $create                         = $this->Devec_model->create();
                if ($create) {
                    redirect('devec/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'devec/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('devec/index');
        }
        $this->Devec_model->_pk_devec = $this->uri->segment(3);
        $read                         = $this->Devec_model->read();
        if (!$read) {
            redirect('devec/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Devec_model->update();
                redirect('devec/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'devec/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('devec/index');
        }
        $this->Devec_model->_pk_devec = $this->uri->segment(3);
        $read                         = $this->Devec_model->read();
        if (!$read) {
            redirect('devec/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Devec_model->upsert();
                redirect('devec/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'devec/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_devec') > 0) {
            $this->Devec_model->_pk_devec = $this->input->post('pk_devec');
            $this->Devec_model->delete();
        }
        redirect('devec/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
    }

    private function _fill_model() {
        $this->Devec_model->_month    = $this->input->post('month');
        $this->Devec_model->_year     = $this->input->post('year');
        $this->Devec_model->_fk_agent = $this->input->post('fk_agent');
        $this->Devec_model->_status   = $this->Devec_model->_status_active;
    }

}
