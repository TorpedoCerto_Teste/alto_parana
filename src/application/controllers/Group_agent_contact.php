<?php

/**
  pk_group_agent_contact
  fk_agent
  fk_contact
  fk_group

 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Group_agent_contact extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Group_agent_contact_model');
    }

    function index() {
        //pega os dados do agente em questão
        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect("agent");
        }

        $this->Group_agent_contact_model->_status   = $this->Group_agent_contact_model->_status_active;
        $this->Group_agent_contact_model->_fk_agent = $this->Agent_model->_pk_agent;
        $data['group_agent_contacts']               = $this->Group_agent_contact_model->fetch_by_agent();
        $data['js_include']                         = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']                        = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                       = 'group_agent_contact/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        //pega os dados do agente em questão
        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect("agent");
        }

        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Group_agent_contact_model->create();
                if ($create) {
                    redirect('agent/view/' . $this->Agent_model->_pk_agent . '/grupos');
                }
            }
            $data['error'] = true;
        }

        //pega a lista de grupos
        $this->load->model("Group_model");
        $this->Group_model->_status = $this->Group_model->_status_active;
        $data['groups']             = $this->Group_model->fetch();


        //pega a lista de contatos
        $this->load->model("Contact_model");
        $this->Contact_model->_status = $this->Contact_model->_status_active;
        $data['contacts']             = array();
        $contacts                     = $this->Contact_model->fetch_with_agent();
        if ($contacts) {
            foreach ($contacts as $contact) {
                $data['contacts'][$contact['community_name']][] = $contact;
            }
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'group_agent_contact/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('group_agent_contact/index');
        }
        $this->Group_agent_contact_model->_pk_group_agent_contact = $this->uri->segment(3);
        $read                                                     = $this->Group_agent_contact_model->read();
        if (!$read) {
            redirect('index');
        }

        if ($this->input->post('fk_contact') > 0) {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Group_agent_contact_model->update();
                redirect('group_agent_contact/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'group_agent_contact/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->Group_agent_contact_model->_fk_agent   = $this->uri->segment(3);
            $this->Group_agent_contact_model->_fk_contact = $this->input->post('fk_contact');
            $this->Group_agent_contact_model->delete();

            $groups = $this->input->post('groups');

            if (!empty($groups)) {
                foreach ($groups as $fk_group) {
                    $this->Group_agent_contact_model->_fk_group = $fk_group;
                    $this->Group_agent_contact_model->create();
                }
            }
        }
        redirect('agent/view/' . $this->Group_agent_contact_model->_fk_agent . '/grupos');
    }

    function delete() {
        if ($this->input->post('pk_group_agent_contact') > 0) {
            $this->Group_agent_contact_model->_pk_group_agent_contact = $this->input->post('pk_group_agent_contact');
            $this->Group_agent_contact_model->delete();
        }
        redirect('agent/view/' . $this->uri->segment(3, 0) . '/grupos');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_group', 'fk_group', 'trim|required');
        $this->form_validation->set_rules('fk_contact', 'fk_contact', 'trim|required');
    }

    private function _fill_model() {
        $this->Group_agent_contact_model->_fk_group   = $this->input->post('fk_group');
        $this->Group_agent_contact_model->_fk_contact = $this->input->post('fk_contact');
        $this->Group_agent_contact_model->_fk_agent   = $this->Agent_model->_pk_agent;
        $this->Group_agent_contact_model->_status     = $this->Group_agent_contact_model->_status_active;
    }

    function ajax_fetch_by_contact() {
        $this->Group_agent_contact_model->_status     = $this->Group_agent_contact_model->_status_active;
        $this->Group_agent_contact_model->_fk_contact = $this->input->post('fk_contact');
        $this->Group_agent_contact_model->_fk_agent   = $this->input->post('fk_agent');
        $group_agent_contacts                         = $this->Group_agent_contact_model->fetch();
        print_r(json_encode($group_agent_contacts));
    }

}
