<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent extends Public_Controller {

    function __construct() {
        parent::__construct();
        
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Agent_model');
    }

    function index() {
        $data['fk_agent_types'] = [];
        if ($this->input->post('filter') == 'true') {
            $data['fk_agent_types'] = $this->input->post('fk_agent_type');
        }

        if (is_array($data['fk_agent_types']) && !empty($data['fk_agent_types'])) {
           $this->Agent_model->_fk_agent_type = $data['fk_agent_types'];
        } 
        $data['list']         = $this->Agent_model->fetch();

        $this->load->model("Agent_type_model");
        $this->Agent_type_model->_status = $this->Agent_type_model->_status_active;
        $data['agent_types'] = $this->Agent_type_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">            
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'agent/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Agent_model->create();
                if ($create) {
                    if ($this->input->post('create_unity') == "1") {
                        $this->_create_unity($create);
                    }
                    redirect('agent/view/' . $create);
                }
            }
            $data['error'] = true;
        }
        $this->load->model('Agent_type_model');
        $this->Agent_type_model->_status = $this->Agent_type_model->_status_active;
        $data['agent_types']             = $this->Agent_type_model->fetch();

        $this->load->model('User_model');
        $this->User_model->_status = $this->User_model->_status_active;
        $data['users']             = $this->User_model->fetch();

        $this->load->model('Sector_model');
        $this->Sector_model->_status = $this->Sector_model->_status_active;
        $data['sectors']             = $this->Sector_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/agent/create.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'agent/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('agent/index');
        }
        $this->Agent_model->_pk_agent = $this->uri->segment(3);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Agent_model->update();
            }
        }
        redirect('agent/view/' . $this->Agent_model->_pk_agent);
    }

    function view() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('agent/index');
        }
        $this->Agent_model->_pk_agent = $this->uri->segment(3);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->load->model('Agent_type_model');
        $this->Agent_type_model->_status = $this->Agent_type_model->_status_active;
        $data['agent_types']             = $this->Agent_type_model->fetch();

        $this->load->model('User_model');
        $this->User_model->_status = $this->User_model->_status_active;
        $data['users']             = $this->User_model->fetch();

        $this->load->model('Sector_model');
        $this->Sector_model->_status = $this->Sector_model->_status_active;
        $data['sectors']             = $this->Sector_model->fetch();

        //Perfis
        $this->load->model('Profile_model');
        $this->Profile_model->_fk_agent = $this->Agent_model->_pk_agent;
        $data['profiles']               = $this->Profile_model->fetch();

        //Unidades
        $this->load->model('Unity_model');
        $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
        $data['units']                = $this->Unity_model->fetch();

        //Contatos (Por agente)
        $this->load->model('Agent_contact_model');
        $this->Agent_contact_model->_fk_agent = $this->Agent_model->_pk_agent;
        $data['contacts']               = $this->Agent_contact_model->fetch_by_agent();
        
        //Contatos que não pertencem ao agente
        $this->load->model('Contact_model');
        $this->Contact_model->_fk_agent = $this->Agent_model->_pk_agent;
        $data['external_contacts']      = $this->Contact_model->fetch_external_contacts();

        //Grupos
        $this->load->model('Group_model');
        $this->Group_model->_status = $this->Group_model->_status_active;
        $data['groups']             = $this->Group_model->fetch();


        //Uploads
        $this->load->helper('directory');
        $directory      = './uploads/agent/' . $this->Agent_model->_pk_agent;
        $data['upload'] = directory_map($directory);

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'js/dropzone.js"></script>
            <script src="' . base_url() . 'web/js/agent/dropzone.js"></script>
            <script src="' . base_url() . 'web/js/agent/view.js?'. microtime().'"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />        
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dropzone.css" rel="stylesheet">';
        $data['main_content'] = 'agent/view';
        $this->load->view('includes/template', $data);
    }

    function status() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('agent/index');
        }
        $this->Agent_model->_pk_agent = $this->uri->segment(3);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        if ($this->input->post('status') == 'true') {
            $this->Agent_model->_status = $this->Agent_model->_status == $this->Agent_model->_status_active ? $this->Agent_model->_status_inactive : $this->Agent_model->_status_active;
            $update                     = $this->Agent_model->update();
            redirect('agent/view/' . $this->Agent_model->_pk_agent);
        }
    }

    function delete() {
        if ($this->input->post('pk_agent') > 0) {
            $this->Agent_model->_pk_agent = $this->input->post('pk_agent');
            $this->Agent_model->delete();
        }
        redirect('agent/index');
    }

    function dropzone() {
        $config['upload_path']   = './uploads/agent/' . $this->uri->segment(3);
        $config['allowed_types'] = '*';
        $config['max_size']      = 800000;
        check_dir_exists($config['upload_path']);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $return = array(
                'error'   => true,
                'message' => strip_tags($this->upload->display_errors()));
        }
        else {
            $return = array(
                'error'   => false,
                'message' => $this->upload->data());
        }
        print json_encode($return);
    }

    function delete_file() {
        if ($this->input->post('delete') == 'true') {
            unlink('./uploads/agent/' . $this->input->post('pk_agent') . '/' . $this->input->post('file_to_delete'));
        }
        redirect('agent/view/' . $this->input->post('pk_agent') . '/arquivos');
    }

    private function _create_unity($pk_agent) {
        $this->load->model("Unity_model");
        $this->Unity_model->_fk_agent                   = $pk_agent;
        $this->Unity_model->_fk_agent_power_distributor = $pk_agent;
        $this->Unity_model->_company_name               = $this->Agent_model->_company_name;
        $this->Unity_model->_community_name             = $this->Agent_model->_community_name;
        $this->Unity_model->_cnpj                       = $this->Agent_model->_cnpj;
        $this->Unity_model->_ie                         = $this->Agent_model->_ie;
        $this->Unity_model->_postal_code                = $this->Agent_model->_postal_code;
        $this->Unity_model->_address                    = $this->Agent_model->_address;
        $this->Unity_model->_number                     = $this->Agent_model->_number;
        $this->Unity_model->_complement                 = $this->Agent_model->_complement;
        $this->Unity_model->_district                   = $this->Agent_model->_district;
        $this->Unity_model->_city                       = $this->Agent_model->_city;
        $this->Unity_model->_state                      = $this->Agent_model->_state;
        $this->Unity_model->_status                     = $this->Unity_model->_status_active;
        $this->Unity_model->create();
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_agent_type', 'fk_agent_type', 'trim|required');
        $this->form_validation->set_rules('company_name', 'company_name', 'trim|required');
        $this->form_validation->set_rules('community_name', 'community_name', 'trim|required');
        $this->form_validation->set_rules('cnpj', 'cnpj', 'trim');
        $this->form_validation->set_rules('ie', 'ie', 'trim');
        $this->form_validation->set_rules('postal_code', 'postal_code', 'trim');
        $this->form_validation->set_rules('address', 'address', 'trim');
        $this->form_validation->set_rules('number', 'number', 'trim');
        $this->form_validation->set_rules('complement', 'complement', 'trim');
        $this->form_validation->set_rules('district', 'district', 'trim');
        $this->form_validation->set_rules('city', 'city', 'trim');
        $this->form_validation->set_rules('fk_user', 'fk_user', 'trim|required');
        $this->form_validation->set_rules('fk_sector', 'fk_sector', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim');
        $this->form_validation->set_rules('memo', 'memo', 'trim');
    }

    private function _fill_model() {
        $this->Agent_model->_fk_agent_type  = $this->input->post('fk_agent_type');
        $this->Agent_model->_company_name   = mb_strtoupper($this->input->post('company_name'), 'UTF-8');
        $this->Agent_model->_community_name = mb_strtoupper($this->input->post('community_name'), 'UTF-8');
        $this->Agent_model->_cnpj           = preg_replace("/([^\d]*)/", "", $this->input->post('cnpj'));
        $this->Agent_model->_ie             = mb_strtoupper($this->input->post('ie'), 'UTF-8');
        $this->Agent_model->_postal_code    = preg_replace("/([^\d]*)/", "", $this->input->post('postal_code'));
        $this->Agent_model->_address        = mb_strtoupper($this->input->post('address'), 'UTF-8');
        $this->Agent_model->_number         = mb_strtoupper($this->input->post('number'), 'UTF-8');
        $this->Agent_model->_complement     = mb_strtoupper($this->input->post('complement'), 'UTF-8');
        $this->Agent_model->_district       = mb_strtoupper($this->input->post('district'), 'UTF-8');
        $this->Agent_model->_city           = mb_strtoupper($this->input->post('city'), 'UTF-8');
        $this->Agent_model->_state          = mb_strtoupper($this->input->post('state'), 'UTF-8');
        $this->Agent_model->_status         = $this->Agent_model->_status_active;
        $this->Agent_model->_fk_user        = $this->input->post('fk_user');
        $this->Agent_model->_fk_sector      = $this->input->post('fk_sector');
        $this->Agent_model->_memo           = $this->input->post('memo');
    }

}
