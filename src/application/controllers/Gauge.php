<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gauge extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->load->model("Unity_model");
        $this->Unity_model->_pk_unity = $this->uri->segment(4, 0);
        $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
        $read                         = $this->Unity_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->load->model('Gauge_model');
    }

    function index() {
        $this->Gauge_model->_fk_unity = $this->Unity_model->_pk_unity;
        $data['gauges']               = $this->Gauge_model->fetch();
        $data['js_include']           = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']          = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']         = 'gauge/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Gauge_model->create();
                if ($create) {
                    redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/pontos_medicao');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'gauge/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        
        
        $data['error']                = false;
        $this->Gauge_model->_pk_gauge = $this->uri->segment(5, 0);
        $read                         = $this->Gauge_model->read();
        
        if (!$read) {
            redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/pontos_medicao');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Gauge_model->update();
                redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/pontos_medicao');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'gauge/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_gauge') > 0) {
            $this->Gauge_model->_pk_gauge = $this->input->post('pk_gauge');
            $this->Gauge_model->delete();
        }
        redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/pontos_medicao');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('gauge', 'Gauge', 'trim|required');
        $this->form_validation->set_rules('internal_name', 'Nome Interno', 'trim|required');
        $this->form_validation->set_rules('telemetry_code', 'telemetry_code', 'trim');
    }

    private function _fill_model() {
        $this->Gauge_model->_fk_unity       = $this->Unity_model->_pk_unity;
        $this->Gauge_model->_gauge          = mb_strtoupper($this->input->post('gauge'), 'UTF-8');
        $this->Gauge_model->_internal_name  = mb_strtoupper($this->input->post('internal_name'), 'UTF-8');
        $this->Gauge_model->_telemetry_code = preg_replace("([^\d]*)", "", $this->input->post('telemetry_code'));
        $this->Gauge_model->_summer_time    = $this->input->post('summer_time') == 1 ? 1 : 0;
        $this->Gauge_model->_status         = $this->Gauge_model->_status_active;
    }

}
