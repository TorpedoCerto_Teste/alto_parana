<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chatbot extends CI_Controller {

    var $_bot_token    = "EAAHh3DTyFdoBAIypF6UK0EXDUwFWDvc3BhCdkWYVypBJxvyZAqaPtibSCFqWiUmPsHJvWmFc08k0PUZCAtEdNf7Sq0SRfrM96WdWb4xmYHISdJkZBpMUaTyIdQitnpFkmly83dZB6MK19KyoTtbYRZATH7PbZAMZB9E6jg8y4ZAtUQZDZD";
    var $_verify_token = "chatbot_ape";

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $hub_verify_token = null;

        if (isset($_REQUEST['hub_challenge'])) {
            $challenge        = $_REQUEST['hub_challenge'];
            $hub_verify_token = $_REQUEST['hub_verify_token'];
        }
        if ($hub_verify_token === $this->_verify_token) {
            echo $challenge;
        }

        $update_response = file_get_contents("php://input");
        $update          = json_decode($update_response, true);
        if (isset($update['entry'][0]['messaging'][0])) {
            $this->_processMessage($update['entry'][0]['messaging'][0]);
        }
    }

    private function _processMessage($message) {
        // processa a mensagem recebida

        $sender = $message['sender']['id']; //id do emissor
        $text   = $message['message']['text']; //texto recebido na mensagem

        if (isset($text)) {
            $this->_sendMessage();
        }
    }

    private function _sendMessage($parameters) {
        $data = array(
            'recipient' => array(
                'id' => $sender
            ), 
            'message' => array(
                'text' => 'Olá! Eu sou um bot que informa os resultados das loterias da Caixa. Será que você ganhou dessa vez? Para começar, digite o nome do jogo para o qual deseja ver o resultado'
            )
        );
        $url = "https://graph.facebook.com/v2.6/me/messages?access_token=";
        $result = $this->_callFacebook($data, $url);
        return $result;
    }

    private function _setWelcomeMessage() {
        $data   = array(
            "setting_type" => "greeting",
            "greeting"     => array(
                "text" => "Olá {{user_first_name}}, eu sou o Robot esta fan page e irei te ajudar com suas dúvidas."
            )
        );
        $url = "https://graph.facebook.com/v2.6/me/thread_settings?access_token=";
        $result = $this->_callFacebook($data, $url);
        return $result;
    }

    private function _setPersistentMenu() {
        $data   = array(
            "persistent_menu" => array(
                array(
                    "locale"                  => "default",
                    "composer_input_disabled" => false,
                    "call_to_actions"         => array(
                        //botões principais do menu
                        array(
                            "title"           => "Minha conta",
                            "type"            => "nested",
                            "call_to_actions" => array(
                                //botões de submenu
                                array(
                                    "title"   => "Pagar conta",
                                    "type"    => "postback",
                                    "payload" => "PAYBILL_PAYLOAD"
                                ),
                                array(
                                    "title"   => "History",
                                    "type"    => "postback",
                                    "payload" => "HISTORY_PAYLOAD"
                                )
                            )
                        ),
                        array(
                            "title"   => "Outro botão",
                            "type"    => "postback",
                            "payload" => "OTHER_PAYLOAD"
                        )
                    )
                )
            )
        );
        $url = "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=";
        $result = $this->_callFacebook($data, $url);
        return $result;
    }

    private function _setGetStarted() {
        $data   = array(
            "setting_type"    => "call_to_actions",
            "thread_state"    => "new_thread",
            "call_to_actions" => array(
                array(
                    "payload" => "GETSTARTED_PAYLOAD"
                )
            )
        );
        $url = "https://graph.facebook.com/v2.6/me/thread_settings?access_token=";
        $result = $this->_callFacebook($data, $url);
        return $result;
    }

    private function _callFacebook($data, $url) {
        $ch     = curl_init($url . $this->_bot_token);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        return $result;
    }
}
