<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tariff_flags extends Public_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Tariff_flags_model');
    }

    function index() {
        redirect('tariff_flags/create');
        $this->Tariff_flags_model->_status = $this->Tariff_flags_model->_status_active;
        $data['list']                      = $this->Tariff_flags_model->fetch();
        $data['js_include']                = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']               = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']              = 'tariff_flags/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('year', 'year', 'trim|exact_length[4]|required|numeric');
            if ($this->form_validation->run() == TRUE) {
                for ($i = 1; $i <= 12; $i++) {
                    $this->Tariff_flags_model->_year       = $this->input->post('year');
                    $this->Tariff_flags_model->_month      = $i;
                    $this->Tariff_flags_model->_flag       = mb_strtoupper($this->input->post('flag_' . $i), 'UTF-8');
                    $this->Tariff_flags_model->_increase   = number_format(str_replace(',', '.', $this->input->post('increase_' . $i)), 2, '.', '');
                    $this->Tariff_flags_model->_status     = $this->Tariff_flags_model->_status_active;
                    $this->Tariff_flags_model->_created_at = date("Y-m-d H:i:s");
                    $this->Tariff_flags_model->upsert();
                }
            }
            else {
                $data['error'] = true;
            }
        }

        $this->Tariff_flags_model->_year = $this->input->post('year') ? $this->input->post('year') : date("Y");
        $data['tariffs']                 = $this->_get_tariffs();
        $data['js_include']              = '<script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/tariff_flags/create.js"></script>';
        $data['css_include']             = '<link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">';
        $data['main_content']            = 'tariff_flags/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('tariff_flags/index');
        }
        $this->Tariff_flags_model->_pk_tariff_flags = $this->uri->segment(3);
        $read                                       = $this->Tariff_flags_model->read();
        if (!$read) {
            redirect('tariff_flags/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Tariff_flags_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $update                      = $this->Tariff_flags_model->update();
                redirect('tariff_flags/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'tariff_flags/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        if ($this->input->post('pk_tariff_flags') > 0) {
            $this->Tariff_flags_model->_pk_tariff_flags = $this->input->post('pk_tariff_flags');
            $this->Tariff_flags_model->delete();
        }
        redirect('tariff_flags/index');
    }

    function ajax() {
        $this->Tariff_flags_model->_year = $this->uri->segment(3, 2016);
        $tariffs                         = $this->_get_tariffs();
        print(json_encode($tariffs));
    }

    private function _get_tariffs() {
        for ($i = 1; $i <= 12; $i ++) {
            $return[$i]['year']     = $this->Tariff_flags_model->_year;
            $return[$i]['flag']     = "VERDE";
            $return[$i]['increase'] = 0;
            $return[$i]['color']    = 'success';
        }
        $tariffs = $this->Tariff_flags_model->fetch();
        if ($tariffs) {
            foreach ($tariffs as $tariff) {
                $return[$tariff['month']]['flag']     = $tariff['flag'];
                $return[$tariff['month']]['increase'] = $tariff['increase'];
                if ($tariff['flag'] == 'AMARELA') {
                    $return[$tariff['month']]['color'] = 'warning';
                }
                elseif ($tariff['flag'] == 'VERMELHA' || $tariff['flag'] == 'VERMELHA II') {
                    $return[$tariff['month']]['color'] = 'danger';
                }
            }
        }
        return $return;
    }

}
