<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unity_contact extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Unity_contact_model');
    }

    function index() {
        $this->Unity_contact_model->_status = $this->Unity_contact_model->_status_active;
        $data['list']                       = $this->Unity_contact_model->fetch();
        $data['js_include']                 = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']                = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']               = 'unity_contact/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        //carrega a lista de contatos
        $this->load->model("Contact_model");
        $this->Contact_model->_fk_agent = $this->uri->segment(3, 0);
        $this->Unity_contact_model->_fk_unity = $this->uri->segment(4, 0);

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('pk_contact', 'pk_contact', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Unity_contact_model->_fk_contact       = mb_strtoupper($this->input->post('pk_contact'), 'UTF-8');
                $this->Unity_contact_model->_status = $this->Unity_contact_model->_status_active;
                $create                             = $this->Unity_contact_model->create();
                if ($create) {
                    redirect('unity/view/'.$this->Contact_model->_fk_agent.'/'.$this->Unity_contact_model->_fk_unity.'/contatos');
                }
            }
            $data['error'] = true;
        }

        $this->Contact_model->_status   = $this->Contact_model->_status_active;
        $this->Contact_model->_fk_unity = $this->Unity_contact_model->_fk_unity;
        $data['contacts']               = $this->Contact_model->fetch_by_agent();

        
        $data['js_include']   = '
            <script src="'.base_url().'js/select2.js"></script>
            <script src="'.base_url().'js/select2-init.js"></script>';
        $data['css_include']  = '    
            <link href="'.base_url().'css/select2.css" rel="stylesheet">
            <link href="'.base_url().'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'unity_contact/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('unity_contact/index');
        }
        $this->Unity_contact_model->_pk_unity_contact = $this->uri->segment(3);
        $read                                         = $this->Unity_contact_model->read();
        if (!$read) {
            redirect('unity_contact/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Unity_contact_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $update                       = $this->Unity_contact_model->update();
                redirect('unity_contact/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'unity_contact/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('unity_contact/index');
        }
        $this->Unity_contact_model->_pk_unity_contact = $this->uri->segment(3);
        $read                                         = $this->Unity_contact_model->read();
        if (!$read) {
            redirect('unity_contact/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Unity_contact_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $upsert                       = $this->Unity_contact_model->upsert();
                redirect('unity_contact/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'unity_contact/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_unity_contact') > 0) {
            $this->Unity_contact_model->_pk_unity_contact = $this->input->post('pk_unity_contact');
            $this->Unity_contact_model->delete();
        }
        redirect('unity/view/' . $this->uri->segment(3, 0) . '/' . $this->uri->segment(4, 0).'/contatos');
    }

}
