<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Devec_customer extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Devec_customer_model');
    }

    function index() {
        redirect("devec_customer/upsert");
        $this->Devec_customer_model->_status = $this->Devec_customer_model->_status_active;
        $data['list']                        = $this->Devec_customer_model->fetch();
        $data['js_include']                  = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']                 = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                = 'devec_customer/index';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error']   = false;
        $data['success'] = false;
        $data['unities'] = false;
        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                if ($this->input->post('pk_devec_customer') > 0) {
                    $this->Devec_customer_model->_pk_devec_customer = $this->input->post('pk_devec_customer');
                }
                $this->Devec_customer_model->_pk_devec_customer = $this->Devec_customer_model->upsert();
                if ($this->Devec_customer_model->_pk_devec_customer) {
                    $this->_process_files();
                }
                $data['success'] = true;
            }
            else {
                $data['error'] = true;
            }
        }
        elseif ($this->input->post('upsert') == 'false') {
            $this->_fill_model();
            $this->Devec_customer_model->read_by_parameters();
        }
        elseif ($this->uri->segment(3,0) > 0) {
            $this->Devec_customer_model->_pk_devec_customer = $this->uri->segment(3);
            $this->Devec_customer_model->read();
        }
        
        if ($this->Devec_customer_model->_fk_agent !== "") {
            $data['unities'] = $this->_process_unities();            
        }
        
        //pega a lista de notificações enviadas
        $data['email_notifications'] = $this->_fetch_email_notification($this->Devec_customer_model->_year, $this->Devec_customer_model->_month, $this->Devec_customer_model->_fk_agent);

        //pega a lista de agentes
        $data['agents'] = $this->_process_agents_group();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>            
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>    
                <script src="' . base_url() . 'web/js/devec_customer/upsert.js"></script>';
        $data['css_include']  = '
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'devec_customer/upsert';
        $this->load->view('includes/template', $data);
    }
    
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Devec_customer_model->_pk_devec_customer = $this->input->post("pk_devec_customer_sendmail");
            $devec_customer                             = $this->Devec_customer_model->fetch_by_unity();
            if ($devec_customer) {
                //pega o agente
                $agent = $this->_get_agent($devec_customer[0]['fk_agent']);
                if ($agent) {
                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($devec_customer, $destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $devec_customer[0]['fk_agent']);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($devec_customer[0]['year'], $devec_customer[0]['month']);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($devec_customer, $contact_list);
                            }

                        }
                    }
                }
            }
        }
        redirect("devec_customer/upsert/".$devec_customer[0]['pk_devec_customer']);
    }
    
    private function _post_mail($devec_customer, $to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = 'DEVEC';

        $this->load->clear_vars();
        $msg             = $this->load->view('devec_customer/email/upsert', $devec_customer[0], true);
        $post['message'] = base64_encode($msg);

        if ($devec_customer[0]['devec_file'] != "") {
            $post['files'] = 'uploads/devec_customer/' . $devec_customer[0]['pk_devec_customer'] . '/' . $devec_customer[0]['devec_file'];
        }

        $pymail = pymail($post);
    }


    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_fk_agent = $this->Devec_customer_model->_fk_agent;
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        return $this->Unity_model->fetch();
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
    }

    private function _fill_model() {
        $this->Devec_customer_model->_fk_agent   = $this->input->post('fk_agent');
        $this->Devec_customer_model->_fk_unity   = $this->input->post('fk_unity');
        $this->Devec_customer_model->_month      = $this->input->post('month');
        $this->Devec_customer_model->_year       = $this->input->post('year');
        $this->Devec_customer_model->_devec_file = $this->input->post('devec_file');
        $this->Devec_customer_model->_status     = $this->Devec_customer_model->_status_active;
    }
    
    private function _process_files() {
        $config['upload_path']   = './uploads/devec_customer/' . $this->Devec_customer_model->_pk_devec_customer;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('devec_file')) {
            $this->Devec_customer_model->_devec_file = str_replace(" ", "_", $_FILES['devec_file']['name']);
            $this->Devec_customer_model->update();
        }
    }
    
}
