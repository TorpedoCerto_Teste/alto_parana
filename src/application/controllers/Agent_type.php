<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_type extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Agent_type_model');
    }

    function index() {
        $this->Agent_type_model->_status = $this->Agent_type_model->_status_active;
        $data['list']                    = $this->Agent_type_model->fetch();
        $data['js_include']              = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']             = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']            = 'agent_type/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('type', 'Tipo', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Agent_type_model->_type   = mb_strtoupper($this->input->post('type'), 'UTF-8');
                $this->Agent_type_model->_status = $this->Agent_type_model->_status_active;
                $create                          = $this->Agent_type_model->create();
                if ($create) {
                    redirect('agent_type/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'agent_type/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('agent_type/index');
        }
        $this->Agent_type_model->_pk_agent_type = $this->uri->segment(3);
        $read                                   = $this->Agent_type_model->read();
        if (!$read) {
            redirect('agent_type/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('type', 'Tipo', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Agent_type_model->_type = mb_strtoupper($this->input->post('type'), 'UTF-8');
                $update                        = $this->Agent_type_model->update();
                redirect('agent_type/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'agent_type/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_agent_type') > 0) {
            $this->Agent_type_model->_pk_agent_type = $this->input->post('pk_agent_type');
            $this->Agent_type_model->delete();
        }
        redirect('agent_type/index');
    }

}
