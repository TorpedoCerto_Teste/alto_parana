<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Product_category_model');
        $this->Product_category_model->_pk_product_category = $this->uri->segment(3, 0);
        $this->Product_category_model->_status              = $this->Product_category_model->_status_active;
        if (!$this->Product_category_model->read()) {
            redirect('product_category');
        }

        $this->load->model('Product_model');
    }

    function index() {
        $this->Product_model->_fk_product_category = $this->Product_category_model->_pk_product_category;
        $this->Product_model->_status              = $this->Product_model->_status_active;
        $data['list']                              = $this->Product_model->fetch();
        $data['js_include']                        = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']                       = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                      = 'product/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Product_model->create();
                if ($create) {
                    redirect('product/index/' . $this->Product_category_model->_pk_product_category);
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'product/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(4, 0) <= 0) {
            redirect('product/index');
        }
        $this->Product_model->_pk_product          = $this->uri->segment(4);
        $this->Product_model->_fk_product_category = $this->Product_category_model->_pk_product_category;
        $read                                      = $this->Product_model->read();
        if (!$read) {
            redirect('product/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Product_model->update();
                redirect('product/index/' . $this->Product_model->_fk_product_category);
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'product/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_product') > 0) {
            $this->Product_model->_pk_product = $this->input->post('pk_product');
            $this->Product_model->delete();
        }
        redirect('product/index/' . $this->Product_category_model->_pk_product_category);
    }

    private function _validate_form() {
        $this->form_validation->set_rules('product', 'product', 'trim');
        $this->form_validation->set_rules('product_short', 'product_short', 'trim|required');
    }

    private function _fill_model() {
        $this->Product_model->_product             = mb_strtoupper($this->input->post('product'), 'UTF-8');
        $this->Product_model->_product_short       = mb_strtoupper($this->input->post('product_short'), 'UTF-8');
        $this->Product_model->_fk_product_category = $this->Product_category_model->_pk_product_category;
        $this->Product_model->_status              = $this->Product_model->_status_active;
    }

}
