<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Energy_market extends Public_Controller {

    function __construct() {

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Energy_market_model');
    }

    function index() {
        $this->Energy_market_model->_status = $this->Energy_market_model->_status_active;
        $data['list']                       = $this->Energy_market_model->fetch();
        $data['js_include']                 = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/energy_market/index.js?'. microtime().'"></script>
            ';
        $data['css_include']                = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']               = 'energy_market/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Energy_market_model->_created_at       = date("Y-m-d H:i:s");
                $this->Energy_market_model->_pk_energy_market = $this->Energy_market_model->create();
                if ($this->Energy_market_model->_pk_energy_market) {
                    $errors = $this->_process_files();
                    if (empty($errors)) {
                        redirect('energy_market/index');
                    }
                    else {
                        $data['errors'] = $errors;
                    }
                }
            }
           $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'web/js/energy_market/create.js"></script>
        ';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
        ';
        $data['main_content'] = 'energy_market/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('energy_market/index');
        }
        $this->Energy_market_model->_pk_energy_market = $this->uri->segment(3);
        $read                                         = $this->Energy_market_model->read();
        if (!$read) {
            redirect('energy_market/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Energy_market_model->update();

                $errors = $this->_process_files();
                if (empty($errors)) {
                    redirect('energy_market/index');
                }
                else {
                    $data['errors'] = $errors;
                }
            }
            $data['error'] = true;
        }

//        //pega a lista de contatos e seus grupos 
//        if (isset($this->Controller_group_model->_fk_group)) {
//            $data['group_contacts'] = $this->_process_group_contact($this->Free_captive_model->_fk_agent);
//        }
//
//        //pega a lista de notificações enviadas
//        $data['email_notifications'] = $this->_fetch_email_notification($this->Free_captive_model->_year, $this->Free_captive_model->_month, $this->Free_captive_model->_fk_agent);
//        
        //pega a lista de agentes
        $data['agents'] = $this->_process_agents();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/vue"></script>
            <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
            <script src="' . base_url() . 'web/js/energy_market/create.js?' . microtime() . '"></script>
        ';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
                <class>
                .modal-dialog{
                    overflow-y: initial !important
                }
                .modal-body{
                    height: 250px;
                    overflow-y: auto;
                }
                </class>    
        ';
        $data['main_content'] = 'energy_market/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('energy_market/index');
        }
        $this->Energy_market_model->_pk_energy_market = $this->uri->segment(3);
        $read                                         = $this->Energy_market_model->read();
        if (!$read) {
            redirect('energy_market/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Energy_market_model->upsert();
                redirect('energy_market/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_market/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_energy_market') > 0) {
            $this->Energy_market_model->_pk_energy_market = $this->input->post('pk_energy_market');
            $this->Energy_market_model->delete();
        }
        redirect('energy_market/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('week', 'week', 'trim|required');
    }

    private function _fill_model() {
        $this->Energy_market_model->_month      = $this->input->post('month');
        $this->Energy_market_model->_year       = $this->input->post('year');
        $this->Energy_market_model->_week       = $this->input->post('week');
        $this->Energy_market_model->_status     = $this->Energy_market_model->_status_active;
        $this->Energy_market_model->_created_at = date("Y-m-d H:i:s");
    }

    private function _process_files() {
        $errors = [];
        $config['upload_path']   = './uploads/energy_market/' . $this->Energy_market_model->_pk_energy_market;
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = 30000;
        $config['overwrite'] = true;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        $return = array(
            'energy_market_file_indicator' => "",
            'energy_market_file_price'     => ""
        );

        foreach ($return as $key => $value) {
            $uploaded = $this->upload->do_upload($key);
            if ($uploaded) {
                $uploaded_data = $this->upload->data();

                $return[$key] = $uploaded_data['file_name'];
            } else {
                $errors[$_FILES[$key]['name']] = $this->upload->display_errors();
            }
        }

        foreach ($return as $key => $value) {
            if ($value != "") {
                $this->Energy_market_model->{"_" . $key} = $value;
            }
        }
        $this->Energy_market_model->update();
        return $errors;
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    function schedule() {
        $this->Energy_market_model->_pk_energy_market = $this->input->post('pk_energy_market_schedule');
        $this->Energy_market_model->_schedule         = human_date_to_date($this->input->post('date')) . " " . $this->input->post('time');
        $this->Energy_market_model->update();
        redirect('energy_market');
    }
    
    function delete_schedule() {
        $this->Energy_market_model->_pk_energy_market = $this->uri->segment(3);
        $this->Energy_market_model->_schedule = null;
        $this->Energy_market_model->update();
        redirect('energy_market');
    }

}
