<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Business_billing extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }
        if ($this->uri->segment(3, 0) > 0) {
            $this->load->model("Business_model");
            $this->Business_model->_pk_business = $this->uri->segment(3);
            $read                               = $this->Business_model->read();
            if (!$read) {
                redirect("business");
            }
        }
        else {
            redirect("business");
        }

        $this->load->model('Business_billing_model');
    }

    function create() {
        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Business_billing_model->_pk_business_billing = $this->Business_billing_model->create();
                if ($this->Business_billing_model->_pk_business_billing) {
                    //fazer o puload dos arquivos enviados pelo POST
                    $this->_upload();
                    
                    //inserir evento no calendário
                    $this->create_calendar_event();
                }
            }
        }
        redirect("business/view/" . $this->Business_model->_pk_business . "/faturamentos");
    }

    function delete() {
        if ($this->input->post('pk_business_billing') > 0) {
            $this->Business_billing_model->_pk_business_billing = $this->input->post('pk_business_billing');
            $this->Business_billing_model->delete();
        }
        redirect("business/view/" . $this->Business_model->_pk_business . "/faturamentos");
    }
    
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Business_billing_model->_pk_business_billing = $this->input->post("pk_business_billing_sendmail");
            $business_billing                                    = $this->Business_billing_model->fetch();

            if ($business_billing) {
            
                if ($this->input->post('test')) {
                    $destination = $this->input->post('test_email');
                    $this->_post_mail($business_billing[0], $destination);
                }
                else {
                    $contacts = $this->input->post("contacts");
                    $contact_list = [];
                    if (!empty($contacts)) {
                        foreach ($contacts as $contact) {
                            //pega o contato
                            $get_contact = $this->_get_contact($contact, $business_billing[0]['fk_agent']);
                            if ($get_contact) {
                                $contact_list[] = $this->Contact_model->_email;
                            }
                        }
                    }
                    //envia o email
                    if (!empty($contact_list)) {
                        $contact_string = join(", ", $contact_list);
                        $this->_post_mail($business_billing[0], $contact_string);

                    }
                }
            }
        }
        redirect("business/view/" . $this->Business_model->_pk_business . "/faturamentos");
    }

    private function _upload() {
        $config['upload_path']   = './uploads/business_billing/' . $this->Business_billing_model->_pk_business_billing;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        //upload para cada arquivo
        $this->upload->do_upload('payment_slip');
        $this->upload->do_upload('invoice');
        $this->upload->do_upload('xml');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_business', 'fk_business', 'trim');
        $this->form_validation->set_rules('year', 'year', 'trim');
        $this->form_validation->set_rules('month', 'month', 'trim');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim');
        $this->form_validation->set_rules('expiration_date', 'expiration_date', 'trim');
        $this->form_validation->set_rules('amount', 'amount', 'trim');
        $this->form_validation->set_rules('discount', 'discount', 'trim');
        $this->form_validation->set_rules('total', 'total', 'trim');
        $this->form_validation->set_rules('payment_method', 'payment_method', 'trim');
        $this->form_validation->set_rules('payment_status', 'payment_status', 'trim');
        $this->form_validation->set_rules('payment_date', 'payment_date', 'trim');
        $this->form_validation->set_rules('payment_slip', 'payment_slip', 'trim');
        $this->form_validation->set_rules('invoice', 'invoice', 'trim');
        $this->form_validation->set_rules('xml', 'xml', 'trim');
    }

    private function _fill_model() {
        $this->Business_billing_model->_fk_business     = $this->Business_model->_pk_business;
        $this->Business_billing_model->_year            = preg_replace("/([^\d]*)/", "", $this->input->post('year'));
        $this->Business_billing_model->_month           = preg_replace("/([^\d]*)/", "", $this->input->post('month'));
        $this->Business_billing_model->_fk_unity        = $this->input->post('fk_unity');
        $this->Business_billing_model->_expiration_date = human_date_to_date($this->input->post('expiration_date'));
        $this->Business_billing_model->_amount          = number_format(str_replace(",", ".", (str_replace(".", "", $this->input->post('amount')))),2,".","");
        $this->Business_billing_model->_discount        = number_format(str_replace(",", ".", (str_replace(".", "", $this->input->post('discount')))),2,".","");
        $this->Business_billing_model->_total           = number_format(str_replace(",", ".", (str_replace(".", "", $this->input->post('total')))),2,".","");
        $this->Business_billing_model->_payment_method  = $this->input->post('payment_method');
        $this->Business_billing_model->_payment_status  = $this->input->post('payment_status');
        $this->Business_billing_model->_payment_date    = human_date_to_date($this->input->post('payment_date'), 1);
        $this->Business_billing_model->_payment_slip    = str_replace(" ", "_", $_FILES['payment_slip']['name']);
        $this->Business_billing_model->_invoice         = str_replace(" ", "_", $_FILES['invoice']['name']);
        $this->Business_billing_model->_xml             = str_replace(" ", "_", $_FILES['xml']['name']);
        $this->Business_billing_model->_status          = $this->Business_billing_model->_status_active;
    }

    private function _post_mail($business_billing, $to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Faturamento de Serviços";
        $msg = $this->load->view('business_billing/email/sendmail', $business_billing, true);
        $post['message'] = base64_encode($msg);

        $file = [];
        if ($business_billing['invoice'] != "") {
            $file[] = 'uploads/business_billing/' . $business_billing['pk_business_billing'] . '/' . $business_billing['invoice'];
        }
        if ($business_billing['xml'] != "") {
            $file[] = 'uploads/business_billing/' . $business_billing['pk_business_billing'] . '/' . $business_billing['xml'];
        }
        if ($business_billing['payment_slip'] != "") {
            $file[] = 'uploads/business_billing/' . $business_billing['pk_business_billing'] . '/' . $business_billing['payment_slip'];
        }

        if (!empty($file)) {
            $post['files'] = join(",", $file);
        }

        $pymail = pymail($post);
    }
    
    function create_calendar_event() {
        $this->load->model('Calendar_model');
        $this->Calendar_model->_calendar                = "Faturamento de Serviço";
        $this->Calendar_model->_year_reference          = $this->Business_billing_model->_year;
        $this->Calendar_model->_month_reference         = $this->Business_billing_model->_month;
        $this->Calendar_model->_fk_calendar_responsible = 1; //CLIENTE
        $this->Calendar_model->_event_date              = $this->Business_billing_model->_expiration_date;
        $this->Calendar_model->_fk_agent                = $this->Business_model->_fk_agent;
        $this->Calendar_model->_status                  = $this->Calendar_model->_status_active;
        $this->Calendar_model->_created_at              = date("Y-m-d H:i:s");
        return $this->Calendar_model->create();
    }
    
}
