<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ercap extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ercap_model');
    }

    function index() {
        //pega a lista e gera uma tabela
        $data['list'] = false;
        //resultados dos filtros
        if ($this->input->post('filter') == 'true') {
            $this->form_validation->set_rules('month', 'month', 'trim|required');
            $this->form_validation->set_rules('year', 'year', 'trim|required');
            $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim');
            $this->form_validation->set_rules('fk_agent_power_distributor', 'fk_agent_power_distributor', 'trim');
            $this->form_validation->set_rules('state', 'state', 'trim');
            if ($this->form_validation->run() == TRUE) {
                $this->Ercap_model->_month    = $this->input->post('month');
                $this->Ercap_model->_year     = $this->input->post('year');
                $this->Ercap_model->_fk_agent = $this->input->post('fk_agent');
                $this->Ercap_model->_state    = $this->input->post('state');
                $this->Ercap_model->_status   = $this->Ercap_model->_status_active;
                $data['list']               = $this->Ercap_model->fetch();
            }
        }
        else {
            $this->Ercap_model->_status   = $this->Ercap_model->_status_active;
            $data['list'] = array_reverse($this->Ercap_model->fetch());
        }
        //pega a lista de agentes
        $data['agents'] = $this->_process_agents();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/ercap/index.js?'. uniqid().'"></script>
            ';
            // <script src="' . base_url() . 'web/js/index_index.js"></script>
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'ercap/index';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error']   = false;
        $data['success'] = $this->uri->segment(3, 0) == 1 ? true : false;

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Ercap_model->_created_at = date("Y-m-d H:i:s");
                $this->Ercap_model->_pk_ercap                       = $this->Ercap_model->upsert();
                if ($this->Ercap_model->_pk_ercap && !empty($this->input->post('chk'))) {
                    $this->_process_ercap_apportionment($this->Ercap_model->_pk_ercap);
                    $uploads = $this->_process_files();
                }
                redirect(current_url() . "/1");
            }
            else {
                $data['error'] = true;
            }
        }
        elseif ($this->input->post('upsert') == 'false') {
            $this->Ercap_model->_fk_agent = $this->input->post("fk_agent");
            $this->Ercap_model->_month    = $this->input->post("month");
            $this->Ercap_model->_year     = $this->input->post("year");
            $this->Ercap_model->read();
            
            //pega a lista de notificações enviadas
            $data['email_notifications'] = $this->_fetch_email_notification($this->input->post('year'), $this->input->post('month'), $this->input->post('fk_agent'));
        }
        
        //pega a lista de agentes
        $data['agents'] = $this->_process_agents();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'web/js/ercap/upsert.js?'. uniqid().'"></script>';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'ercap/upsert';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_ercap') > 0) {
            $this->Ercap_model->_pk_ercap = $this->input->post('pk_ercap');
            $this->Ercap_model->delete();
        }
        redirect('ercap/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('value', 'value', 'trim');
    }

    private function _fill_model() {
        $this->Ercap_model->_pk_ercap   = $this->input->post('pk_ercap');
        $this->Ercap_model->_fk_agent = $this->input->post('fk_agent');
        $this->Ercap_model->_month    = $this->input->post('month');
        $this->Ercap_model->_year     = $this->input->post('year');
        $this->Ercap_model->_value    = str_replace(",", ".", $this->input->post('value'));
        $this->Ercap_model->_status   = $this->Ercap_model->_status_active;
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_ercap_apportionment($pk_ercap) {
        $this->load->model("Ercap_apportionment_model");
        $this->Ercap_apportionment_model->_fk_ercap = $pk_ercap;
        $this->Ercap_apportionment_model->delete();

        $this->Ercap_apportionment_model->_status     = $this->Ercap_apportionment_model->_status_active;
        $this->Ercap_apportionment_model->_created_at = date("Y-m-d H:i:s");
        foreach ($this->input->post('chk') as $fk_unity) {
            $this->Ercap_apportionment_model->_fk_unity = (int) $fk_unity;
            $this->Ercap_apportionment_model->create();
        }
    }

    function validate_value() {
        if (str_replace(",", ".", $this->input->post("value")) <= 0.00) {
            return false;
        }
        return true;
    }             

    function ajax_find() {
        $return                     = false;
        $this->disableLayout        = TRUE;
        $this->output->set_content_type('application/json');
        $this->Ercap_model->_fk_agent = $this->input->post('fk_agent');
        $this->Ercap_model->_month    = $this->input->post('month');
        $this->Ercap_model->_year     = $this->input->post('year');
        $this->Ercap_model->_status   = $this->Ercap_model->_status_active;
        $read                       = $this->Ercap_model->read();
        if ($read) {
            $return['ercap']                          = $this->Ercap_model;
            //pegar os ids das unidades que estão marcadas para fazer rateio
            $this->load->model("Ercap_apportionment_model");
            $this->Ercap_apportionment_model->_fk_ercap = $this->Ercap_model->_pk_ercap;
            $this->Ercap_apportionment_model->_status = $this->Ercap_apportionment_model->_status_active;
            $return['ercap_apportionment']            = $this->Ercap_apportionment_model->fetch();
        }
        print_r(json_encode($return));
    }

    function ajax_update() {
        $return                   = false;
        $this->disableLayout      = TRUE;
        $this->output->set_content_type('application/json');
        $this->Ercap_model->_pk_ercap = $this->input->post('pk_ercap');
        $this->Ercap_model->_value  = str_replace(",", ".", $this->input->post('value'));
        $update                   = $this->Ercap_model->update();
        print_r(json_encode($update));
    }
    
    function ajax_empty_value() {
        $return                   = false;
        $this->disableLayout      = TRUE;
        $this->output->set_content_type('application/json');
        $this->Ercap_model->_pk_ercap = $this->input->post('pk_ercap');
        $this->Ercap_model->_value  = 0;
        $update                   = $this->Ercap_model->update_in();
        print_r(json_encode($update));
    }
    

    public function clear_field_data() {
        $this->_field_data = array();
        return $this;
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/ercap/' . $this->Ercap_model->_pk_ercap;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('ercap_file')) {
            $this->Ercap_model->_ercap_file = str_replace(" ", "_", $_FILES['ercap_file']['name']);
            $this->Ercap_model->update();
        }
    }
    
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Ercap_model->_pk_ercap = $this->input->post("pk_ercap_sendmail");
            $ercap                                    = $this->Ercap_model->read();

            if ($ercap) {
                //carrega o EER Market
                $this->load->model("Ercap_market_model");
                $this->Ercap_market_model->_month  = $this->Ercap_model->_month;
                $this->Ercap_market_model->_year   = $this->Ercap_model->_year;
                $this->Ercap_market_model->_status = $this->Ercap_market_model->_status_active;
                $this->Ercap_market_model->read();

                //pega o agente
                $agent = $this->_get_agent($this->Ercap_model->_fk_agent);

                if ($agent) {
                    
                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->Ercap_model->_fk_agent);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($this->Ercap_model->_year, $this->Ercap_model->_month);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($contact_list);
                            }

                        }
                        
                    }
            
                }
            }
        }
        redirect("ercap/upsert");
    }

    private function _post_mail($to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Encargo de Energia de Reserva - EER";
        $post['message'] = base64_encode($this->load->view('ercap/email/upsert', array(), true));
        if ($this->Ercap_model->_ercap_file != "") {
            $post['files'] = 'uploads/ercap/' . $this->Ercap_model->_pk_ercap . '/' . $this->Ercap_model->_ercap_file;
        }

        $pymail = pymail($post);
    }

}
