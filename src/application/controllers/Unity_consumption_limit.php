<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unity_consumption_limit extends Public_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Unity_consumption_limit_model');
    }

    function index() {
        $this->Unity_consumption_limit_model->_status = $this->Unity_consumption_limit_model->_status_active;
        $data['list']                                 = $this->Unity_consumption_limit_model->fetch();
        $data['js_include']                           = '
            <script type="text/javascript" src="' . base_url() . 'assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
            ';
        $data['css_include']                          = '
            <link rel="stylesheet" href="' . base_url() . 'assets/plugins/DataTables/media/css/DT_bootstrap.css" />
            ';
        $data['main_content']                         = 'unity_consumption_limit/index';
        $this->load->view('template/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Unity_consumption_limit_model->_created_at = date("Y-m-d H:i:s");
                $create                                           = $this->Unity_consumption_limit_model->create();
                if ($create) {
                    redirect('unity_consumption_limit/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'unity_consumption_limit/create';
        $this->load->view('template/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('unity_consumption_limit/index');
        }
        $this->Unity_consumption_limit_model->_pk_unity_consumption_limit = $this->uri->segment(3);
        $read                                                             = $this->Unity_consumption_limit_model->read();
        if (!$read) {
            redirect('unity_consumption_limit/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Unity_consumption_limit_model->update();
                redirect('unity_consumption_limit/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'unity_consumption_limit/update';
        $this->load->view('template/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('unity_consumption_limit/index');
        }
        $this->Unity_consumption_limit_model->_pk_unity_consumption_limit = $this->uri->segment(3);
        $read                                                             = $this->Unity_consumption_limit_model->read();
        if (!$read) {
            redirect('unity_consumption_limit/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Unity_consumption_limit_model->upsert();
                redirect('unity_consumption_limit/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="' . base_url() . 'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'unity_consumption_limit/upsert';
        $this->load->view('template/template', $data);
    }

    public function ajax_upsert() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $upsert              = 0;

        $this->Unity_consumption_limit_model->_pk_unity_consumption_limit = $this->input->post("pk_unity_consumption_limit");
        $this->_fill_model();
        $upsert                                                           = $this->Unity_consumption_limit_model->upsert();
        print(json_encode($upsert));
    }

    function delete() {
        if ($this->input->post('pk_unity_consumption_limit') > 0) {
            $this->Unity_consumption_limit_model->_pk_unity_consumption_limit = $this->input->post('pk_unity_consumption_limit');
            $this->Unity_consumption_limit_model->delete();
        }
        redirect('unity_consumption_limit');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('consumption_limit', 'consumption_limit', 'trim|required');
    }

    private function _fill_model() {
        $this->Unity_consumption_limit_model->_fk_unity          = preg_replace("([^\d]*)", "", $this->input->post('fk_unity'));
        $this->Unity_consumption_limit_model->_month             = preg_replace("([^\d]*)", "", $this->input->post('month'));
        $this->Unity_consumption_limit_model->_year              = preg_replace("([^\d]*)", "", $this->input->post('year'));
        $this->Unity_consumption_limit_model->_consumption_limit = str_replace(",", ".", $this->input->post('consumption_limit'));
    }

}
