<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_billing extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ccvee_billing_model');
    }

    function create() {
        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Ccvee_billing_model->create();
                if ($create) {
                    //fazer o puload dos arquivos enviados pelo POST
                    $upload = $this->_upload($create);
                    //adicionar evento no calendário
                    foreach ($upload as $key => $value) {
                        if ($value != "") {
                            $this->Ccvee_billing_model->{"_" . $key} = $value;
                        }
                    }
                    $this->Ccvee_billing_model->_pk_ccvee_billing = $create;
                    $this->Ccvee_billing_model->update();

                    $this->create_calendar_event();
                }
            }
        }

        redirect('ccvee_general/view/' . $this->uri->segment(3) . '/faturamentos');
    }

    private function _upload($pk_ccvee_billing) {
        $config['upload_path']   = './uploads/ccvee_billing/' . $pk_ccvee_billing;
        $config['allowed_types'] = '*';
        $config['max_size']      = 80000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        $return = array(
            'payment_slip' => "",
            'invoice' => "",
            'xml'     => "",
        );

        foreach ($return as $key => $value) {
            if ($this->upload->do_upload($key)) {
                $return[$key] = str_replace(" ", "_", $_FILES[$key]['name']);
            }
        }
        return $return;        

    }

    function create_calendar_event() {
        $this->load->model('Calendar_model');
        $this->Calendar_model->_calendar = "Faturamento de Energia ACL";
        $this->Calendar_model->_year_reference = $this->Ccvee_billing_model->_year;
        $this->Calendar_model->_month_reference = $this->Ccvee_billing_model->_month;
        $this->Calendar_model->_fk_calendar_responsible = 1; //CLIENTE
        $this->Calendar_model->_event_date = $this->Ccvee_billing_model->_expiration_date;
        $this->Calendar_model->_fk_agent = $this->input->post('fk_agent');
        $this->Calendar_model->_status = $this->Calendar_model->_status_active;
        $this->Calendar_model->_created_at = date("Y-m-d H:i:s");
        return $this->Calendar_model->create();
    }

    function delete() {
        if ($this->input->post('pk_ccvee_billing') > 0) {
            $this->Ccvee_billing_model->_pk_ccvee_billing = $this->input->post('pk_ccvee_billing');
            $this->Ccvee_billing_model->delete();
        }
        redirect('ccvee_general/view/' . $this->uri->segment(3) . '/faturamentos');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim');
        $this->form_validation->set_rules('month', 'month', 'trim');
        $this->form_validation->set_rules('expiration_date', 'expiration_date', 'trim');
        $this->form_validation->set_rules('total', 'total', 'trim');
        $this->form_validation->set_rules('payment_status', 'payment_status', 'trim');
        $this->form_validation->set_rules('payment_date', 'payment_date', 'trim');
        $this->form_validation->set_rules('invoice_type', 'invoice_type', 'trim');
        $this->form_validation->set_rules('payment_slip', 'payment_slip', 'trim');
        $this->form_validation->set_rules('invoice', 'invoice', 'trim');
        $this->form_validation->set_rules('xml', 'xml', 'trim');
    }

    private function _fill_model() {
        $this->Ccvee_billing_model->_fk_ccvee_general = $this->uri->segment(3);
        $this->Ccvee_billing_model->_fk_unity         = $this->input->post('fk_unity');
        $this->Ccvee_billing_model->_year             = preg_replace("/([^\d]*)/", "", $this->input->post('year'));
        $this->Ccvee_billing_model->_month            = preg_replace("/([^\d]*)/", "", $this->input->post('month'));
        $this->Ccvee_billing_model->_expiration_date  = human_date_to_date($this->input->post('expiration_date'));
        $this->Ccvee_billing_model->_total            = str_replace(",", ".", $this->input->post('total'));
        $this->Ccvee_billing_model->_payment_status   = $this->input->post('payment_status');
        $this->Ccvee_billing_model->_payment_date     = human_date_to_date($this->input->post('payment_date'));
        $this->Ccvee_billing_model->_invoice_type     = $this->input->post('invoice_type');
        $this->Ccvee_billing_model->_payment_slip     = $_FILES['payment_slip']['name'];
        $this->Ccvee_billing_model->_invoice          = $_FILES['invoice']['name'];
        $this->Ccvee_billing_model->_xml              = $_FILES['xml']['name'];
        $this->Ccvee_billing_model->_status           = $this->Ccvee_billing_model->_status_active;
    }

}
