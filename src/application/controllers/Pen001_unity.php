<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pen001_unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Pen001_unity_model');
    }

    function index() {
        $this->Pen001_unity_model->_status = $this->Pen001_unity_model->_status_active;
        $data['list']                      = $this->Pen001_unity_model->fetch();
        $data['js_include']                = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']               = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']              = 'pen001_unity/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Pen001_unity_model->_created_at = date("Y-m-d H:i:s");
                $create                                = $this->Pen001_unity_model->create();
                if ($create) {
                    redirect('pen001_unity/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'pen001_unity/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('pen001_unity/index');
        }
        $this->Pen001_unity_model->_pk_pen001_unity = $this->uri->segment(3);
        $read                                       = $this->Pen001_unity_model->read();
        if (!$read) {
            redirect('pen001_unity/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Pen001_unity_model->update();
                redirect('pen001_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'pen001_unity/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('pen001_unity/index');
        }
        $this->Pen001_unity_model->_pk_pen001_unity = $this->uri->segment(3);
        $read                                       = $this->Pen001_unity_model->read();
        if (!$read) {
            redirect('pen001_unity/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Pen001_unity_model->upsert();
                redirect('pen001_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'pen001_unity/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_pen001_unity') > 0) {
            $this->Pen001_unity_model->_pk_pen001_unity = $this->input->post('pk_pen001_unity');
            $this->Pen001_unity_model->delete();
        }
        redirect('pen001_unity/index');
    }

    private function _validate_form() {

        $this->form_validation->set_rules('fk_pen001', 'fk_pen001', 'trim|required');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
    }

    private function _fill_model() {

        $this->Pen001_unity_model->_fk_pen001 = $this->input->post('fk_pen001');
        $this->Pen001_unity_model->_fk_unity  = $this->input->post('fk_unity');
        $this->Pen001_unity_model->_status    = $this->Pen001_unity_model->_status_active;
    }

}
