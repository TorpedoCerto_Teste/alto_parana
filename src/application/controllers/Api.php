<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Retorna todos os resultados de acesso a API em formato json
 *
 * @author rogeriopellarin
 */
class Api extends Public_Controller
{

    function __construct()
    {
        set_time_limit(0);
        parent::__construct();
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        @header('Access-Control-Allow-Origin: *');
        @header('Access-Control-Allow-Methods: GET, POST');
    }

    function process_pld_csv()
    {
        $this->load->helper('directory');
        $directory = './uploads/';
        $map       = directory_map($directory);
        if (!empty($map)) {
            foreach ($map as $file) {
                if (!is_array($file)) {
                    if (is_file($directory . $file)) {
                        if (strstr($file, 'pld')) {
                            $this->load->library('csvreader');
                            $result = $this->csvreader->parse_file($directory . $file);
                            if (!empty($result)) {
                                $this->_process_hourly_basis($result);
                            }
                            //move o arquivo para a pasta uploads/backup
                            directory_copy('./uploads/' . $file, './uploads/backup/' . $file . '.bkp', 1);

                            //envia email de processamento de arquivo
                            $this->_post_mail("Importação de Arquivo PLD", "PLD", $file);
                        }
                    }
                }
            }
        }
    }

    function process_telemetry()
    {
        $this->load->model("Gauge_model");
        $this->Gauge_model->_status = $this->Gauge_model->_status_active;
        $telemetry_codes            = $this->Gauge_model->fetch_telemetry_code();
        if ($telemetry_codes) {
            $this->_call_way2($telemetry_codes);
        }
    }

    //ALERTAS DO CALENDÁRIO
    public function calendar()
    {
        $this->load->model("Calendar_model");
        $this->Calendar_model->_status     = $this->Calendar_model->_status_active;
        $this->Calendar_model->_event_date = date("Y-m-d");
        $data['today']                     = $this->Calendar_model->fetch();
        $this->Calendar_model->_event_date = date("Y-m-d", strtotime("+1 DAY"));
        $data['tomorrow']                  = $this->Calendar_model->fetch();

        if (!empty($data['today']) || !empty($data['tomorrow'])) {
            $this->load->config('email_config');
            $post['emails'] = is_array($this->config->item('to')) ? join(",", $this->config->item('to')) : $this->config->item('to');
            $post['subject'] = "Eventos do Calendário (" . date("d/m/Y") . ")";
            $this->load->clear_vars();
            $msg = $this->load->view('api/email/calendar', $data, true);
            $post['message'] = base64_encode($msg);
            $pymail = pymail($post);
        }
    }

    //ALERTAS DO CALENDÁRIO PARA OS CLIENTES (AGENTES)
    public function agent_calendar()
    {
        $this->load->model("Calendar_model");
        $this->Calendar_model->_status     = $this->Calendar_model->_status_active;
        $this->Calendar_model->_fk_calendar_responsible = 1; //1 = Cliente
        $this->Calendar_model->_event_date = date("Y-m-d");
        $data['today']                     = $this->Calendar_model->fetch();
        $this->Calendar_model->_event_date = date("Y-m-d", strtotime("+1 DAY"));
        $data['tomorrow']                  = $this->Calendar_model->fetch();

        $calendar_events_by_agent = $this->_process_calendar_events_by_agent($data);

        if ($calendar_events_by_agent) {
            foreach ($calendar_events_by_agent as $k => $agent_events) {
                if (isset($agent_events['calendar']) && ((isset($agent_events['calendar']['today']) && !empty($agent_events['calendar']['today'])) || (isset($agent_events['calendar']['tomorrow']) && !empty($agent_events['calendar']['tomorrow'])))) {
                    $post['emails'] = join(",", $agent_events['email']);
                    $post['subject'] = "Eventos do Calendário (" . date("d/m/Y") . ")";
                    $this->load->clear_vars();
                    $msg = $this->load->view('api/email/calendar', $agent_events['calendar'], true);
                    $post['message'] = base64_encode($msg);
                    $pymail = pymail($post);
                }
            }
        }
    }

    function process_scde()
    {
        $this->load->helper('directory');
        $directory = './uploads/gauge_record/';
        $map       = directory_map($directory);
        if (!empty($map)) {
            foreach ($map as $file) {
                if (!is_array($file)) {
                    if (is_file($directory . $file)) {
                        $result  = file($directory . $file);
                        $rewrite = strstr($file, '_rewrite') ? true : false;
                        //move o arquivo para a pasta uploads/backup
                        check_dir_exists($directory . 'backup/');
                        directory_copy($directory . $file, $directory . 'backup/' . $file . '.bkp', 1);
                        //inicia o processamento
                        if (!empty($result)) {
                            $this->_process_gauge_record($result, $rewrite);
                        }
                        //envia email de processamento de arquivo
                        $this->_post_mail("Importação de Arquivo SCDE", "SCDE", $file);
                    }
                }
            }
        }
    }

    /**
     * php index.php api process_odc
     */
    function process_odc()
    {
        $this->load->helper('directory');
        $directory = './uploads/odc_record/';
        $map       = directory_map($directory);
        if (isset($map['backup/'])) {
            unset($map['backup/']);
        }
        if (!empty($map)) {
            foreach ($map as $file) {
                if (!is_array($file)) {
                    if (is_file($directory . $file)) {
                        $result = file($directory . $file);
                        $scde   = strstr($file, '_scde') ? true : false;
                        $way2   = strstr($file, '_way2') ? true : false;
                        //move o arquivo para a pasta uploads/backup
                        check_dir_exists($directory . 'backup/');
                        directory_copy($directory . $file, $directory . 'backup/' . $file . '.bkp', 1);
                        //inicia o processamento

                        $key = preg_replace("/([^\d]*)/", "", microtime());

                        $this->register_log([
                            "key" => $key,
                            "date" => date("Y-m-d H:i:s"),
                            "data" => [
                                "status" => "start",
                                "replace_scde" => $scde,
                                "replace_way2" => $way2,
                                "file_name" => $file,
                                "total_lines" => count($result)
                            ]
                        ]);

                        if (!empty($result)) {
                            $this->_process_odc_record($result, $scde, $way2, $key);
                        }

                        //envia email de processamento de arquivo
                        $this->_post_mail("Importação de Arquivo ODC", "ODC", $file);
                    }
                }
            }
        }
    }

    /**
     * Importador de arquivos .csv exportados a partir do site da CCEE com a seguinte característica:
     * 
     * Tipo de Relatório: Origem de Dados da Coleta.;;;;;;;;;
     * Tipo de Agente: Conectante | Nome:  METALURGICA STORI;;;;;;;;;
     * Periodo Solicitado: 10/2024;;;;;;;;;
     * Ponto de Medição;Data;Hora;Tipo de Energia;Ativa Geração (kWh);Ativa Consumo (kWh);Reativa Geração (kVArh);Reativa Consumo (kVArh);Origem da Coleta;Notificação de Coleta
     * PRTOANENTR101;01/10/2024;1;Liquida;0;70,348;0;0;Hora Ajustada;
     * 
     * Versão em: 25/11/2024 
     * 
     */
    function process_ccee_odc()
    {
        try {
            $this->load->helper('directory');
            $this->load->helper('file');

            $baseFolder = './uploads/ccee_odc/';
            $tempFolder = $baseFolder . 'temp/';
            $doneFolder = $baseFolder . 'done/';
            $errorFolder = $baseFolder . 'error/';

            // Check if base folder exists
            if (!is_dir($baseFolder)) {
                throw new Exception("Base folder does not exist.");
                return;
            }

            // Create folders if they don't exist
            foreach ([$tempFolder, $doneFolder, $errorFolder] as $folder) {
                if (!is_dir($folder)) {
                    mkdir($folder, 0777, true);
                }
            }

            // Get CSV files
            $csvFiles = glob($baseFolder . '*.csv');

            if (!empty($csvFiles)) {

                $successFiles = [];
                $errorFiles = [];

                // Process each file
                foreach ($csvFiles as $filePath) {
                    $fileName = basename($filePath);
                    $tempFilePath = $tempFolder . $fileName;

                    $scde   = strstr($fileName, '_scde') ? true : false;
                    $way2   = strstr($fileName, '_way2') ? true : false;

                    try {
                        // Move the file to temp folder
                        if (!rename($filePath, $tempFilePath)) {
                            throw new Exception("Failed to move file to temporary folder.");
                        }

                        // Read and process file content
                        $data = $this->_prepare_ccee_odc_data($tempFilePath);
                        if (is_array($data) && !empty($data)) {
                            $report = $this->_process_ccee_odc($data, $way2, $scde);
                        }

                        // Move file to done folder
                        if (!rename($tempFilePath, $doneFolder . $fileName)) {
                            throw new Exception("Failed to move file to done folder.");
                        }

                        $successFiles[$fileName] = $report;
                    } catch (Exception $ex) {
                        // Move file to error folder
                        if (file_exists($tempFilePath)) {
                            rename($tempFilePath, $errorFolder . $fileName);
                        } else {
                            rename($filePath, $errorFolder . $fileName);
                        }

                        // Log error
                        $errorFiles[$fileName] = $ex->getMessage();
                    }
                }

                $this->_send_email_resume_ccee_odc([$successFiles, $errorFiles]);

                echo "Processing completed.";
            }
        } catch (Exception $e) {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($e->getMessage()));
        }
    }

    function process_energy_market()
    {
        $this->load->model("Energy_market_model");
        $this->Energy_market_model->_status = $this->Energy_market_model->_status_active;
        $energy_markets                     = $this->Energy_market_model->fetch_by_schedule();
        if ($energy_markets) {
            $this->load->model("Newsletter_model");
            $this->Newsletter_model->_status = $this->Newsletter_model->_status_active;
            $newsletters                     = $this->Newsletter_model->fetch();
            if ($newsletters) {
                foreach ($energy_markets as $energy_market) {
                    //retira a data de disparo (schedule)
                    $this->Energy_market_model->_pk_energy_market = $energy_market['pk_energy_market'];
                    $this->Energy_market_model->_schedule         = null;
                    $this->Energy_market_model->update();

                    //envia o email para cada um dos contatos
                    foreach ($newsletters as $newsletter) {
                        $this->_post_newsletter("Relatório de Mercado de Energia", $newsletter['email'], $energy_market);
                    }
                }
            }
        }
    }

    function test_energy_market()
    {
        try {
            if ($this->input->post("pk_energy_market") > 0) {
                $this->load->model("Energy_market_model");
                $this->Energy_market_model->_status = $this->Energy_market_model->_status_active;
                $this->Energy_market_model->_pk_energy_market = $this->input->post("pk_energy_market");
                $energy_markets                     = $this->Energy_market_model->read();
                if ($energy_markets) {
                    $email = $this->input->post("email");
                    $energy_market = [
                        "pk_energy_market" => $this->Energy_market_model->_pk_energy_market,
                        "year" => $this->Energy_market_model->_year,
                        "month" => $this->Energy_market_model->_month,
                        "week" => $this->Energy_market_model->_week,
                        "energy_market_file_indicator" => $this->Energy_market_model->_energy_market_file_indicator,
                        "energy_market_file_price" => $this->Energy_market_model->_energy_market_file_price
                    ];
                    $this->_post_newsletter("Relatório de Mercado de Energia", $email, $energy_market);
                }
                print_r(json_encode([
                    "status" => "success"
                ]));
            } else {
                print_r(json_encode([
                    "status" => "error"
                ]));
            }
        } catch (\Throwable $th) {
            print_r($th);
        }
    }

    private function _process_hourly_basis($result)
    {
        foreach ($result as $k => $pld) {
            if ((isset($pld['data incio']) || isset($pld['data início']) || isset($pld['data inicio'])) && isset($pld['data fim'])) {
                $values                                = array_values($pld);
                $this->load->model("Hourly_basis_model");
                $this->Hourly_basis_model->_date_start = date("Y-m-d 00:00:00", strtotime(human_date_to_date($values[3])));
                $this->Hourly_basis_model->_date_end   = date("Y-m-d 23:59:59", strtotime(human_date_to_date($values[4])));
                $this->Hourly_basis_model->_status     = $this->Hourly_basis_model->_status_active;
                $fetches                               = $this->Hourly_basis_model->fetch_by_start_end_date();
                if ($fetches) {
                    foreach ($fetches as $fetch) {
                        $this->_upsert_hourly_basis($fetch, $values);
                    }
                }
            } else {
                //enviar mensagem de error por email
            }
        }
    }

    private function _upsert_hourly_basis($data, $values)
    {
        $this->Hourly_basis_model->_week            = $data['week'];
        $this->Hourly_basis_model->_date            = $data['date'];
        $this->Hourly_basis_model->_landing         = $data['landing'];
        $this->Hourly_basis_model->_hour_type       = $data['hour_type'];
        $this->Hourly_basis_model->_pk_hourly_basis = $data['pk_hourly_basis'];
        $this->Hourly_basis_model->_week_ccee       = $values[0] . str_pad($values[1], 2, 0, STR_PAD_LEFT) . $values[2];

        if ($data['landing'] == 'LEVE') {
            $this->Hourly_basis_model->_pld_south     = str_replace(",", ".", $values[10]);
            $this->Hourly_basis_model->_pld_southeast = str_replace(",", ".", $values[7]);
            $this->Hourly_basis_model->_pld_north     = str_replace(",", ".", $values[16]);
            $this->Hourly_basis_model->_pld_northeast = str_replace(",", ".", $values[13]);
        } elseif ($data['landing'] == 'MEDIO') {
            $this->Hourly_basis_model->_pld_south     = str_replace(",", ".", $values[9]);
            $this->Hourly_basis_model->_pld_southeast = str_replace(",", ".", $values[6]);
            $this->Hourly_basis_model->_pld_north     = str_replace(",", ".", $values[15]);
            $this->Hourly_basis_model->_pld_northeast = str_replace(",", ".", $values[12]);
        } elseif ($data['landing'] == 'PESADO') {
            $this->Hourly_basis_model->_pld_south     = str_replace(",", ".", $values[8]);
            $this->Hourly_basis_model->_pld_southeast = str_replace(",", ".", $values[5]);
            $this->Hourly_basis_model->_pld_north     = str_replace(",", ".", $values[14]);
            $this->Hourly_basis_model->_pld_northeast = str_replace(",", ".", $values[11]);
        }
        if ($data['pld_south'] <= 0.0) {
            $this->Hourly_basis_model->update();
        } elseif ($data['pld_south'] != $this->Hourly_basis_model->_pld_south || $data['pld_southeast'] != $this->Hourly_basis_model->_pld_southeast || $data['pld_north'] != $this->Hourly_basis_model->_pld_north || $data['pld_northeast'] != $this->Hourly_basis_model->_pld_northeast) {
            //inativa o registro ativo
            $this->Hourly_basis_model->delete();
            $this->Hourly_basis_model->_pk_hourly_basis = NULL;
            $this->Hourly_basis_model->_status          = $this->Hourly_basis_model->_status_active;
            $this->Hourly_basis_model->create();
        }
    }

    /**
     * Token de Autenticação
     * Para que seja possível consultar os dados da API, é necessário utilizar 
     * um token de autenticação. Uma vez que o token tenha sido gerado, o mesmo 
     * deve ser inserido na tag pim-auth que será enviada no header da 
     * requisição de cada requisição enviada à API.
     * 
     * Url de Consulta
     * Uma vez que o método de consulta aos dados do PIM trata-se de uma API 
     * REST, todas as requisições à mesma serão feitas através de uma URL 
     * montada com o caminho da API e com os parâmetros da consulta. 
     * Nesse contexto, a seguinte URL deve ser utilizada para consultar os 
     * dados de um determinado ponto de medição:
     * 
     * http://sx.way2.com.br:81/api/pontos/635/dados-de-medicao/Eneat?dataInicio=2017-07-27T00:00:00&dataFim=2017-07-28T00:00:00&contextoDasDatas=2&intervaloEmMinutos=60&aplicarHorarioDeVerao=false
     * 
     * Onde:
     * ·  Eneat: Energia ativa;
     * ·  Enere: Energia reativa;
     * ·  Demat: Demanda ativa;
     * ·  Demre: Demanda reativa
     * · DemConPta: Demanda contratada Ponta;
     * · DemConfpta: Demanda contratada Fora Ponta.
     * 
     * Uma vez que a URL base estiver formada, os seguintes parâmetros devem ser
     *  adicionados a mesma para que a consulta possa ser efetuada.
     * 
     * ·  dataInicio: Timestamp determinando o início do período de dados que 
     * devem ser buscados para a grandeza e medidor especificados 
     * formato: YYYY-MM-DDThh:mm:ss.
     * 
     * ·  contextoDasDatas:  
     * o Considerar Tudo (1): será considerado o início e fim da forma como foi
     *  solicitada. Por exemplo, de 26/11/2015 15:05:00 à 26/11/2015 16:00:00, 
     * com intervalo de 5 minutos, serão retornados 12 registros;
     * o Considerar Dia Cheio (2): serão ignorados as horas e os minutos das 
     * datas de início e fim. Serão retornados 288 registros 
     * (de 26/11/2015 00:05:00 à 27/11/2015 00:00:00);
     * o Considerar Mês Cheio (3): serão ignorados os dias, horas e minutos das 
     * datas de início e fim. Serão retornados 8.640 registros 
     * (de 01/11/2015 00:05:00 à 01/12/2015 00:00:00); 
     * 
     * ·  intervaloEmMinutos: Define o formato de integralização dos dados a 
     * serem consultados. 
     * o Possíveis valores (5, 10, 15, 30 e 60, 1440)
     * 
     * ·  aplicarHorarioDeVerao: Define se os dados devem ser extraídos com ou 
     * sem a aplicação de horário de verão: 
     * o Possíveis valores (True ou False) 
     * 
     * @param type $teleletry_codes
     */
    private function _call_way2($teleletry_codes)
    {
        $pim_auth      = $this->config->item("way2_token");
        $start         = date("Y-m-d\TH", strtotime("-1 DAYS")) . ":00:00";
        $end           = date("Y-m-d\TH") . ":00:00";
        $context       = 1;
        $type_interval = array(
            'Eneat' => 60, //60 minutos
            'Enere' => 60,
            'Demat' => 15,
            'Demre' => 15
            //            ,
            //            'Demre'      => 15,
            //            'DemConPta'  => 15,
            //            'DemConfpta' => 15
        );

        foreach ($teleletry_codes as $teleletry_code) {
            foreach ($type_interval as $type => $interval) {
                //                $summer_time = $teleletry_code['summer_time'] == 1 ? "true" : "false";
                $summer_time = "false";
                $url         = 'http://sx.way2.com.br:81/api/pontos/' . $teleletry_code['telemetry_code'] . '/dados-de-medicao/' . $type . '?dataInicio=' . $start . '&dataFim=' . $end . '&contextoDasDatas=' . $context . '&intervaloEmMinutos=' . $interval . '&aplicarHorarioDeVerao=' . $summer_time;
                $ch          = curl_init($url);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Pim-auth: ' . $pim_auth));
                $result      = curl_exec($ch);
                $response    = json_decode($result);
                if (isset($response->Dados)) {
                    $this->_process_way2_response($teleletry_code, $response);
                }
                //                else {
                //                    //envia email de processamento de arquivo
                //                    $this->_post_mail("Erro de Acesso a API Way2", "API WAY2 entre ".$start. " e ".$end." " ,"API");
                //                }
            }
        }
    }

    private function _process_way2_response($telemetry_code, $response)
    {
        $this->load->model("Telemetry_record_model");
        $this->Telemetry_record_model->_fk_gauge  = $telemetry_code['pk_gauge'];
        $this->Telemetry_record_model->_greatness = $response->Dados[0]->NomeGrandeza;
        foreach ($response->Dados[0]->Valores as $values) {
            $this->Telemetry_record_model->_record_date = str_replace("T", " ", $values->Data);
            $order                                      = date("H", strtotime($this->Telemetry_record_model->_record_date));
            $this->Telemetry_record_model->_order       = $order > 0 ? $order : 24;
            $this->Telemetry_record_model->_value       = isset($values->Valor) ? number_format($values->Valor, 3, ".", "") : 0.000;
            $this->Telemetry_record_model->_quality     = $values->Qualidade;
            $this->Telemetry_record_model->hard_delete();
            $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
            $this->Telemetry_record_model->_created_at  = date("Y-m-d H:i:s");
            $this->Telemetry_record_model->create();
        }
    }

    private function _process_gauge_record($gauge_records, $rewrite)
    {
        try {
            $this->load->model("Gauge_model");
            $this->Gauge_model->_status = $this->Gauge_model->_status_active;

            $this->load->model("Gauge_record_model");

            $gauge_code = "";
            foreach ($gauge_records as $k => $gauge_record) {
                $records = explode(";", $gauge_record);
                array_map("TRIM", $records);


                if ($records[0] != $gauge_code && strlen($records[0]) == 13) {
                    $this->Gauge_model->_pk_gauge        = "";
                    $this->Gauge_record_model->_fk_gauge = "";
                    $gauge_code                          = $records[0];
                    $this->Gauge_model->_gauge           = $records[0];
                    $read                                = $this->Gauge_model->read();
                    if ($read) {
                        $this->Gauge_record_model->_fk_gauge = $this->Gauge_model->_pk_gauge;
                    } else {
                        throw new Exception('nao leu ' . $records[0]);
                    }
                }

                if ((int) $this->Gauge_record_model->_fk_gauge > 0) {
                    $this->Gauge_record_model->_record_date = $this->_get_record_date($records);
                    $order                                  = date("H", strtotime($this->Gauge_record_model->_record_date));
                    $this->Gauge_record_model->_order       = $records[2];


                    //deletar o registro caso exista
                    $this->Gauge_record_model->hard_delete_by_record_date();

                    $this->Gauge_record_model->_type_energy                = $records[3];
                    $this->Gauge_record_model->_active_power_generation    = str_replace(",", ".", $records[4]);
                    $this->Gauge_record_model->_active_power_consumption   = str_replace(",", ".", $records[5]);
                    $this->Gauge_record_model->_reactive_power_generation  = str_replace(",", ".", $records[6]);
                    $this->Gauge_record_model->_reactive_power_consumption = str_replace(",", ".", $records[7]);
                    $this->Gauge_record_model->_missing_intervals          = $records[8];
                    $this->Gauge_record_model->_measurement_situation      = preg_replace('/[^A-Za-z0-9\-]/', '', $records[9]);
                    $this->Gauge_record_model->_reason_situation           = preg_replace('/[^A-Za-z0-9\-]/', '', $records[10]);
                    $this->Gauge_record_model->_status                     = $this->Gauge_record_model->_status_active;
                    $this->Gauge_record_model->_created_at                 = date("Y-m-d H:i:s");
                    $create                                                = $this->Gauge_record_model->create();

                    if ($rewrite) {
                        //insere como dados de telemetria (desligar quando testes estiverem ok)
                        $this->_process_telemetry();
                    }
                }
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    private function _process_odc_record($odc_records, $scde, $way2, $key)
    {
        try {
            $this->load->model("Gauge_model");
            $this->Gauge_model->_status = $this->Gauge_model->_status_active;

            if ($scde || $way2) {
                $this->load->model("Gauge_record_model");
            }

            $this->load->model("Odc_record_model");

            $gauge_code = "";
            $line_count = 0;
            $total_success = 0;
            $total_error = 0;
            foreach ($odc_records as $odc_record) {
                $records = explode(";", $odc_record);
                $records = array_map("TRIM", $records);

                //Novo padrão de processamento de arquivo ODC
                if ($line_count++ >= 4) {
                    if (strlen($records[0]) == 13) {
                        //Buscar os dados do medidor
                        if ($gauge_code != $records[0]) {
                            $gauge_code                   = $records[0];
                            $this->Gauge_model->_pk_gauge = "";
                            $this->Gauge_model->_gauge    = $gauge_code;
                            $this->Gauge_model->_status   = $this->Gauge_model->_status_active;
                            $read                         = $this->Gauge_model->read();

                            $this->Odc_record_model->_fk_gauge = 0;

                            if ($read) {
                                $this->Odc_record_model->_fk_gauge = $this->Gauge_model->_pk_gauge;

                                $this->register_log([
                                    "key" => $key,
                                    "date" => date("Y-m-d H:i:s"),
                                    "data" => [
                                        "status" => "success",
                                        "gauge" => $gauge_code,
                                        "pk_gauge" => $this->Odc_record_model->_fk_gauge,
                                        "line" => $line_count,
                                        "total" => [
                                            "success" => $total_success,
                                            "error" => $total_error
                                        ]
                                    ]
                                ]);
                            } else {
                                $this->register_log([
                                    "key" => $key,
                                    "date" => date("Y-m-d H:i:s"),
                                    "data" => [
                                        "status" => "error",
                                        "gauge" => $gauge_code,
                                        "message" => "gauge not found",
                                        "line" => $line_count
                                    ]
                                ]);
                            }
                        }

                        if ((int) $this->Odc_record_model->_fk_gauge > 0) {
                            $this->Odc_record_model->_record_date = $this->_get_record_date_odc($records[1], $records[2]);
                            $order                                = date("H", strtotime($this->Odc_record_model->_record_date));
                            $this->Odc_record_model->_order       = $order > 0 ? $order : 24;

                            $this->Odc_record_model->_value = 0;

                            //com valor
                            if ($records[5] != "") {
                                //deletar o registro caso exista
                                $this->Odc_record_model->hard_delete_by_record_date();

                                $this->Odc_record_model->_value = str_replace(".", "", $records[5]);
                                $this->Odc_record_model->_value = str_replace(",", ".", $this->Odc_record_model->_value);
                                $this->Odc_record_model->_quality    = "Consistido";
                                $this->Odc_record_model->_status     = $this->Odc_record_model->_status_active;
                                $this->Odc_record_model->_created_at = date("Y-m-d H:i:s");
                                $this->Odc_record_model->create();
                            } else {
                                //valor zerado
                                // 1 - verificar se já existe linha no DB, se existir, não mexa, senão, insere também
                                $current_odc = $this->Odc_record_model->read_by_record_date_time();
                                if (!$current_odc) {
                                    $this->Odc_record_model->_quality    = "HoraFaltante";
                                    $this->Odc_record_model->_status     = $this->Odc_record_model->_status_active;
                                    $this->Odc_record_model->_created_at = date("Y-m-d H:i:s");
                                    $this->Odc_record_model->create();
                                }
                            }

                            if ($scde || $way2) {
                                //preenche o objeto Gauge (scde)
                                $this->Gauge_record_model->_fk_gauge                   = $this->Odc_record_model->_fk_gauge;
                                $this->Gauge_record_model->_record_date                = $this->Odc_record_model->_record_date;
                                $this->Gauge_record_model->_order                      = $this->Odc_record_model->_order;
                                $this->Gauge_record_model->_active_power_consumption   = $this->Odc_record_model->_value;
                                $this->Gauge_record_model->_reason_situation           = $this->Odc_record_model->_quality;
                                $this->Gauge_record_model->_type_energy                = 'Liquida';
                                $this->Gauge_record_model->_active_power_generation    = 0.00;
                                $this->Gauge_record_model->_reactive_power_generation  = 0.00;
                                $this->Gauge_record_model->_reactive_power_consumption = 0.00;
                                $this->Gauge_record_model->_missing_intervals          = 0;

                                if ($way2) {
                                    //insere como dados de telemetria (desligar quando testes estiverem ok)
                                    $this->_process_telemetry();
                                }
                                if ($scde) {
                                    //deletar o registro caso exista
                                    $this->Gauge_record_model->hard_delete_by_record_date();

                                    //salva o novo registro
                                    $this->Gauge_record_model->create();
                                }
                            }

                            $total_success++;
                        }
                    } elseif (isset($records[1]) && strstr($records[1], " (L)")) {
                        $records[1] = str_replace(" (L)", "", $records[1]);

                        if ($records[1] != $gauge_code && strlen($records[1]) == 13) {
                            $gauge_code                = $records[1];
                            $this->Gauge_model->_gauge = $records[1];
                            $read                      = $this->Gauge_model->read();
                            if ($read) {
                                $this->Odc_record_model->_fk_gauge = $this->Gauge_model->_pk_gauge;
                            }
                        }

                        if ((int) $this->Odc_record_model->_fk_gauge > 0) {
                            $this->Odc_record_model->_record_date = $this->_get_record_date_odc($records[2], $records[3]);
                            $order                                = date("H", strtotime($this->Odc_record_model->_record_date));
                            $this->Odc_record_model->_order       = $order > 0 ? $order : 24;


                            //deletar o registro caso exista
                            $this->Odc_record_model->hard_delete_by_record_date();

                            $this->Odc_record_model->_value = 0;
                            if ($records[4] != "") {
                                $this->Odc_record_model->_value = str_replace(".", "", $records[4]);
                                $this->Odc_record_model->_value = str_replace(",", ".", $this->Odc_record_model->_value);
                            }
                            $this->Odc_record_model->_quality    = $records[5];
                            $this->Odc_record_model->_source     = $records[6];
                            $this->Odc_record_model->_status     = $this->Odc_record_model->_status_active;
                            $this->Odc_record_model->_created_at = date("Y-m-d H:i:s");
                            $this->Odc_record_model->create();

                            if ($scde || $way2) {
                                //preenche o objeto Gauge (scde)
                                $this->Gauge_record_model->_fk_gauge                   = $this->Odc_record_model->_fk_gauge;
                                $this->Gauge_record_model->_record_date                = $this->Odc_record_model->_record_date;
                                $this->Gauge_record_model->_order                      = $this->Odc_record_model->_order;
                                $this->Gauge_record_model->_active_power_consumption   = $this->Odc_record_model->_value;
                                $this->Gauge_record_model->_measurement_situation      = utf8_encode($this->Odc_record_model->_source);
                                $this->Gauge_record_model->_reason_situation           = strtolower($this->Odc_record_model->_quality) == 'completo' ? 'Consistido' : $this->Odc_record_model->_quality;
                                $this->Gauge_record_model->_type_energy                = 'Liquida';
                                $this->Gauge_record_model->_active_power_generation    = 0.00;
                                $this->Gauge_record_model->_reactive_power_generation  = 0.00;
                                $this->Gauge_record_model->_reactive_power_consumption = 0.00;
                                $this->Gauge_record_model->_missing_intervals          = 0;

                                if ($way2) {
                                    //insere como dados de telemetria (desligar quando testes estiverem ok)
                                    $this->_process_telemetry();
                                }
                                if ($scde) {
                                    //deletar o registro caso exista
                                    $this->Gauge_record_model->hard_delete_by_record_date();

                                    //salva o novo registro
                                    $this->Gauge_record_model->create();
                                }
                            }
                        }
                    }
                }
            }

            $this->register_log([
                "key" => $key,
                "date" => date("Y-m-d H:i:s"),
                "data" => [
                    "status" => "finish",
                    "line" => $line_count,
                    "total" => [
                        "success" => $total_success,
                        "error" => $total_error
                    ]
                ]
            ]);
        } catch (\Throwable $th) {
            $this->register_log([
                "key" => $key,
                "date" => date("Y-m-d H:i:s"),
                "data" => [
                    "status" => "error",
                    "message" => $th->getTraceAsString(),
                    "line" => $line_count,
                    "total" => [
                        "success" => $total_success,
                        "error" => $total_error
                    ]
                ]
            ]);

            return false;
        }
    }

    private function _get_record_date($records)
    {
        $date = human_date_to_date($records[1], 1);
        if ($date) {
            $time = $records[2] == 24 ? ' 00:00:00' : ' ' . $records[2] . ':00:00';
            return $date . $time;
        }
        return false;
    }

    private function _get_record_date_odc($date, $time)
    {
        $date = human_date_to_date($date, 1);
        if ($date) {
            $time = $time == 24 ? ' 00:00:00' : ' ' . $time . ':00:00';
            return $date . $time;
        }
        return false;
    }

    private function _process_telemetry()
    {
        $this->load->model("Telemetry_record_model");

        //inativa os ENEAT
        $this->Telemetry_record_model->_record_date = $this->Gauge_record_model->_record_date;
        $this->Telemetry_record_model->_order       = $this->Gauge_record_model->_order;
        $this->Telemetry_record_model->_fk_gauge    = $this->Gauge_record_model->_fk_gauge;
        $this->Telemetry_record_model->_greatness   = "Eneat";
        $this->Telemetry_record_model->hard_delete();

        //insere novo registro ENEAT
        $this->Telemetry_record_model->_status     = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_value      = $this->Gauge_record_model->_active_power_consumption;
        $this->Telemetry_record_model->_quality    = $this->Gauge_record_model->_reason_situation == 'Consistido' ? 0 : 1;
        $this->Telemetry_record_model->_created_at = date("Y-m-d H:i:s");
        $this->Telemetry_record_model->create();


        //DEMAT
        $this->Telemetry_record_model->_greatness = "Demat";

        //-1 dia, hora: 00:00:00
        $this->Telemetry_record_model->_record_date = date("Y-m-d H:i:s", strtotime("-60 MINUTES", strtotime($this->Gauge_record_model->_record_date)));

        //inativa os DEMAT
        $this->Telemetry_record_model->_fk_gauge = $this->Gauge_record_model->_fk_gauge;
        $this->Telemetry_record_model->hard_delete();

        //insere novo registro DEMAT
        $this->Telemetry_record_model->_status     = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_value      = $this->Gauge_record_model->_active_power_consumption;
        $this->Telemetry_record_model->_quality    = $this->Gauge_record_model->_reason_situation == 'Consistido' ? 0 : 1;
        $this->Telemetry_record_model->_created_at = date("Y-m-d H:i:s");
        $this->Telemetry_record_model->create();

        //-1 dia, hora: 00:15:00
        $this->Telemetry_record_model->_record_date = date("Y-m-d H:i:s", strtotime("-45 MINUTES", strtotime($this->Gauge_record_model->_record_date)));

        //inativa os DEMAT
        $this->Telemetry_record_model->_fk_gauge = $this->Gauge_record_model->_fk_gauge;
        $this->Telemetry_record_model->hard_delete();

        //insere novo registro DEMAT
        $this->Telemetry_record_model->_status     = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_value      = $this->Gauge_record_model->_active_power_consumption;
        $this->Telemetry_record_model->_quality    = $this->Gauge_record_model->_reason_situation == 'Consistido' ? 0 : 1;
        $this->Telemetry_record_model->_created_at = date("Y-m-d H:i:s");
        $this->Telemetry_record_model->create();

        //-1 dia, hora: 00:30:00
        $this->Telemetry_record_model->_record_date = date("Y-m-d H:i:s", strtotime("-30 MINUTES", strtotime($this->Gauge_record_model->_record_date)));

        //inativa os DEMAT
        $this->Telemetry_record_model->_fk_gauge = $this->Gauge_record_model->_fk_gauge;
        $this->Telemetry_record_model->hard_delete();

        //insere novo registro DEMAT
        $this->Telemetry_record_model->_status     = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_value      = $this->Gauge_record_model->_active_power_consumption;
        $this->Telemetry_record_model->_quality    = $this->Gauge_record_model->_reason_situation == 'Consistido' ? 0 : 1;
        $this->Telemetry_record_model->_created_at = date("Y-m-d H:i:s");
        $this->Telemetry_record_model->create();

        //-1 dia, hora: 00:45:00
        $this->Telemetry_record_model->_record_date = date("Y-m-d H:i:s", strtotime("-15 MINUTES", strtotime($this->Gauge_record_model->_record_date)));

        //inativa os DEMAT
        $this->Telemetry_record_model->_fk_gauge = $this->Gauge_record_model->_fk_gauge;
        $this->Telemetry_record_model->hard_delete();

        //insere novo registro DEMAT
        $this->Telemetry_record_model->_status     = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_value      = $this->Gauge_record_model->_active_power_consumption;
        $this->Telemetry_record_model->_quality    = $this->Gauge_record_model->_reason_situation == 'Consistido' ? 0 : 1;
        $this->Telemetry_record_model->_created_at = date("Y-m-d H:i:s");
        $this->Telemetry_record_model->create();
    }

    private function _post_mail($subject, $function, $file)
    {
        $this->load->config('email_config');
        $post['emails'] = is_array($this->config->item('to')) ? join(",", $this->config->item('to')) : $this->config->item('to');
        $post['subject'] = $subject;
        $data['file']     = $file;
        $data['function'] = $function;
        $this->load->clear_vars();
        $msg              = $this->load->view('api/email/api', $data, true);
        $post['message'] = base64_encode($msg);
        $pymail = pymail($post);
    }

    private function _post_newsletter($subject, $email, $energy_market)
    {
        $post['emails'] = $email;
        $post['subject'] = $subject;
        $data['energy_market'] = $energy_market;
        $this->load->clear_vars();
        $msg                   = $this->load->view('api/email/newsletter', $data, true);
        $post['message'] = base64_encode($msg);

        $file = [];
        if ($energy_market["energy_market_file_indicator"] != "") {
            $file[] = 'uploads/energy_market/' . $energy_market["pk_energy_market"] . '/' . $energy_market["energy_market_file_indicator"];
        }
        if ($energy_market["energy_market_file_price"] != "") {
            $file[] = 'uploads/energy_market/' . $energy_market["pk_energy_market"] . '/' . $energy_market["energy_market_file_price"];
        }

        $post['files'] = join(",", $file);

        $pymail = pymail($post);
    }

    private function _prepare_ccee_odc_data($tempFilePath) {
        try {
            $return = [];
            // Read and process file content
            $fileContent = file_get_contents($tempFilePath);
            $rows = explode(PHP_EOL, $fileContent);

            $i = 0;

            foreach ($rows as $row) {
                $i++;
                $data = [];
                if (!empty($row) && $i > 4) {
                    $columns = explode(';', $row);

                    if (isset($columns[1]) && strstr($columns[1], "(L)")) { 
                        $data["gauge"] = str_replace([" (L)", '"'], ["", ""], $columns[1]);
                        $data["record_date"] = $columns[2];
                        $data["order"] = $columns[3];
                        $data["value"] = trim($columns[4]) ? str_replace(",", ".", str_replace(".", "", trim($columns[4]))) : 0;
                        $data["quality"] = $data['value'] > 0 ? "Consistido" : "HoraFaltante";
                        $return[] = $data;   
                    }
                }
            }
            return $return;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function _process_ccee_odc($data, $way2, $scde) {
        try {
            $return = [];
            $i = 0;
            $current_gauge = 0;
            $pk_gauge = 0;
            $internal_name = "";

            foreach ($data as $k => $record) {
                if ($current_gauge !== $record["gauge"]) {
                    if ($current_gauge !== 0 && $i > 0) { 
                        $gauge_complete_name = $internal_name." ".$current_gauge;
                        $return["success"][$gauge_complete_name] = $i; //report
                    }
                    
                    $current_gauge = $record["gauge"];
                    $i = 0;
                    $gauge = $this->_get_gauge_data($record["gauge"]);
                    $pk_gauge = $gauge["pk_gauge"];
                    $internal_name = $gauge["internal_name"];
                }

                if ((int)$pk_gauge > 0) {
                    $i++;
                    $record['pk_gauge'] = $pk_gauge;
                    //Apagar o registro atual
                    $this->_delete_odc_record_by_date($record);
                    //Inserir o novo registro
                    $this->_create_odc_record($record);

                    if ($way2 || $scde) {
                        $this->_delete_gauge_record_by_date($record);
                        $this->_create_gauge_record($record);
                    }
                    
                }
                else {
                    $return["error"][$record["gauge"]] = 0; 
                }
            }
            return $return;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function _get_gauge_data($gauge) {
        try {
            $return = null;
            $this->load->model("Gauge_model");
            $this->Gauge_model->_pk_gauge = "";
            $this->Gauge_model->_status = $this->Gauge_model->_status_active;
            $this->Gauge_model->_gauge = $gauge;
            $read = $this->Gauge_model->read();            
            if ($read) {
                $return['pk_gauge'] = $this->Gauge_model->_pk_gauge;
                $return['internal_name'] = $this->Gauge_model->_internal_name;
            }
            return $return;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function _delete_gauge_record_by_date($record) {
        try {
            $this->load->model("Gauge_record_model");
            $this->Gauge_record_model->_fk_gauge = $record['pk_gauge'];
            $this->Gauge_record_model->_record_date = $this->_get_record_date_odc($record['record_date'], $record['order']);
            $this->Gauge_record_model->_order = $record['order'];
            $this->Gauge_record_model->hard_delete_by_record_date();            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }            
    }

    private function _create_gauge_record($record) {
        try {
            $this->load->model("Gauge_record_model");
            $this->Gauge_record_model->_fk_gauge                   = $record['pk_gauge'];
            $this->Gauge_record_model->_record_date                = $this->_get_record_date_odc($record['record_date'], $record['order']);
            $this->Gauge_record_model->_order                      = $record['order'];
            $this->Gauge_record_model->_active_power_consumption   = $record['value']; 
            $this->Gauge_record_model->_reason_situation           = $record['quality'];   
            $this->Gauge_record_model->_type_energy                = 'Liquida';
            $this->Gauge_record_model->_active_power_generation    = 0.00;
            $this->Gauge_record_model->_reactive_power_generation  = 0.00;
            $this->Gauge_record_model->_reactive_power_consumption = 0.00;
            $this->Gauge_record_model->_missing_intervals          = 0;
            $this->Gauge_record_model->create();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function _delete_odc_record_by_date($record) {
        try {
            $this->load->model("Odc_record_model");
            $this->Odc_record_model->_fk_gauge = $record['pk_gauge'];
            $this->Odc_record_model->_record_date = $this->_get_record_date_odc($record['record_date'], $record['order']);
            $this->Odc_record_model->_order = $record['order'];
            $this->Odc_record_model->hard_delete_by_record_date();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function _create_odc_record($record) {
        try {
            $this->load->model("Odc_record_model");
            $this->Odc_record_model->_fk_gauge = $record['pk_gauge'];
            $this->Odc_record_model->_record_date = $this->_get_record_date_odc($record['record_date'], $record['order']);
            $this->Odc_record_model->_order = $record['order'];            
            $this->Odc_record_model->_value = $record['value'];            
            $this->Odc_record_model->_quality = $record['quality'];      
            $this->Odc_record_model->_status = $this->Odc_record_model->_status_active;   
            $this->Odc_record_model->create();   
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function _send_email_resume_ccee_odc($resume) {
        try {
            $this->load->config('email_config');
            $post['emails'] = is_array($this->config->item('to')) ? join(",", $this->config->item('to')) : $this->config->item('to');
            $post['subject'] = "Resumo de processamento de arquivo de Módulo de Análise ODC";
            $data['resume'] = $resume;
            $this->load->clear_vars();
            $msg              = $this->load->view('api/email/ccee_odc', $data, true);
            $post['message'] = base64_encode($msg);
            $pymail = pymail($post);            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * FUNÇÕES CONTROLADAS PELO VUEJS
     * 
     */
    public function agents()
    {
        $agents = $this->_process_agents();
        if ($agents) {
            print json_encode($agents);
        }
    }

    public function contacts()
    {
        $return   = false;
        $pk_agent = $this->uri->segment(3, 0);
        if ($pk_agent > 0) {
            $contacts = $this->_process_contacts($pk_agent);
            if ($contacts) {
                $return = $contacts;
            }
        }
        print json_encode($return);
    }

    private function _process_agents()
    {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][$agent['pk_agent']] = $agent['community_name'];
            }
        }
        return $option;
    }

    private function _process_contacts($fk_agent)
    {
        $this->load->model("Contact_model");
        $this->Contact_model->_status   = $this->Contact_model->_status_active;
        $this->Contact_model->_fk_agent = $fk_agent;
        $contacts                       = $this->Contact_model->fetch();
        $option                         = array();
        if ($contacts) {
            foreach ($contacts as $contact) {
                $option[$contact['pk_contact']]              = $contact;
                $option[$contact['pk_contact']]['isChecked'] = false;
            }
        }
        return $option;
    }

    private function _process_calendar_events_by_agent($data)
    {
        $agent_contacts = [];
        if ($data['today'] || $data['tomorrow']) {
            $agent_contacts = $this->_fetch_group_agent_contact();

            if ($agent_contacts) {
                foreach ($agent_contacts as $key => $agent_contact) {
                    if ($data['today']) {
                        foreach ($data['today'] as $today) {
                            if ($agent_contact['fk_agent'] == $today['fk_agent'] || !trim($today['fk_agent'])) {
                                $agent_contacts[$key]['calendar']['today'][] = $today;
                            }
                        }
                    } else {
                        $agent_contacts[$key]['calendar']['today'] = [];
                    }
                    if ($data['tomorrow']) {
                        foreach ($data['tomorrow'] as $tomorrow) {
                            if ($agent_contact['fk_agent'] == $tomorrow['fk_agent'] || !trim($tomorrow['fk_agent'])) {
                                $agent_contacts[$key]['calendar']['tomorrow'][] = $tomorrow;
                            }
                        }
                    } else {
                        $agent_contacts[$key]['calendar']['tomorrow'] = [];
                    }
                }
            }
        }
        return $agent_contacts;
    }

    private function _fetch_group_agent_contact()
    {
        $this->load->model("Group_agent_contact_model");
        $this->Group_agent_contact_model->_status = $this->Group_agent_contact_model->_status_active;
        $this->Group_agent_contact_model->_fk_group = 9; //grupo CALENDÁRIO
        return $this->Group_agent_contact_model->fetch_by_group();
    }

    private function register_log($log)
    {
        $this->load->model("Log_model");
        $this->Log_model->_key = $log["key"];
        $this->Log_model->_date = $log["date"];
        $this->Log_model->_data = json_encode($log["data"]);
        $this->Log_model->save();
    }

    function test()
    {
        $this->output->set_content_type('text/html');
        $this->load->view('api/test', array());
    }

    function test2()
    {
        $this->output->set_content_type('text/html');
        $this->load->view('api/test2', array());
    }
}
