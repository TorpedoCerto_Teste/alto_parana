<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Standard extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Standard_model');
    }

    function index() {
        $this->Standard_model->_status = $this->Standard_model->_status_active;
        $data['list']                  = $this->Standard_model->fetch();
        $data['js_include']            = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']           = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']          = 'standard/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('standard', 'standard', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Standard_model->_standard = mb_strtoupper($this->input->post('standard'), 'UTF-8');
                $this->Standard_model->_status   = $this->Standard_model->_status_active;
                $create                          = $this->Standard_model->create();
                if ($create) {
                    redirect('standard/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'Standard/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('standard/index');
        }
        $this->Standard_model->_pk_standard = $this->uri->segment(3);
        $read                               = $this->Standard_model->read();
        if (!$read) {
            redirect('standard/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('standard', 'standard', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Standard_model->_standard = mb_strtoupper($this->input->post('standard'), 'UTF-8');
                $update                          = $this->Standard_model->update();
                redirect('standard/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'Standard/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_standard') > 0) {
            $this->Standard_model->_pk_standard = $this->input->post('pk_standard');
            $this->Standard_model->delete();
        }
        redirect('standard/index');
    }

}
