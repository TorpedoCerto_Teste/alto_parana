<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Free_captive extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Free_captive_model');
    }

    function index() {
        redirect('free_captive/upsert');
        $this->Free_captive_model->_status = $this->Free_captive_model->_status_active;
        $data['list']                      = $this->Free_captive_model->fetch();
        $data['js_include']                = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']               = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']              = 'free_captive/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Free_captive_model->_created_at = date("Y-m-d H:i:s");
                $create                                = $this->Free_captive_model->create();
                if ($create) {
                    redirect('free_captive/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'free_captive/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('free_captive/index');
        }
        $this->Free_captive_model->_pk_free_captive = $this->uri->segment(3);
        $read                                       = $this->Free_captive_model->read();
        if (!$read) {
            redirect('free_captive/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Free_captive_model->update();
                redirect('free_captive/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'free_captive/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        try {
            $data['error']         = false;
            $data['success']       = false;
            $data['unities']       = false;
            $data['free_captives'] = false;
            $data['email_notifications'] = false;

            if ($this->input->post('upsert') == 'true') {
                $this->_validate_form();
                if ($this->form_validation->run() == TRUE) {
                    $unities                             = $this->input->post("unity");
                    $this->Free_captive_model->_month    = $this->input->post('month');
                    $this->Free_captive_model->_year     = $this->input->post('year');
                    $this->Free_captive_model->_fk_agent = $this->input->post('fk_agent');
                    foreach ($unities as $pk_unity => $unity) {
                        $this->Free_captive_model->_pk_free_captive            = $unity['pk_free_captive'];
                        $this->Free_captive_model->_fk_unity                   = $pk_unity;
                        $this->Free_captive_model->_economy                    = str_replace(",", ".", $unity['economy']);
                        $this->Free_captive_model->_captive_total              = str_replace(",", ".", $unity['captive_total']);
                        $this->Free_captive_model->_free_total                 = str_replace(",", ".", $unity['free_total']);
                        $this->Free_captive_model->_fk_agent_power_distributor = str_replace(",", ".", $unity['fk_agent_power_distributor']);
                        $this->Free_captive_model->_status                     = $this->Free_captive_model->_status_active;
                        $this->Free_captive_model->_pk_free_captive            = $this->Free_captive_model->upsert();
                        if ($this->Free_captive_model->_pk_free_captive) {
                            $this->_process_files();
                        }
                    }
                    $data['success']       = true;
                    $data["free_captives"] = $this->Free_captive_model->fetch();
                    $data['unities']       = $this->_process_unities($data["free_captives"]);
                }
                else {
                    $data['error'] = true;
                }
            }
            elseif ($this->input->post('upsert') == 'false') {
                $this->Free_captive_model->_fk_agent = $this->input->post("fk_agent");
                $this->Free_captive_model->_month    = $this->input->post("month");
                $this->Free_captive_model->_year     = $this->input->post("year");
                $data["free_captives"]               = $this->Free_captive_model->fetch();
                $data['unities']                     = $this->_process_unities($data["free_captives"]);

                //pega a lista de notificações enviadas
                $data['email_notifications'] = $this->_fetch_email_notification($this->Free_captive_model->_year, $this->Free_captive_model->_month, $this->Free_captive_model->_fk_agent);

            }

            //pega a lista de agentes
            $data['agents'] = $this->_process_agents();

            $data['js_include']   = '
                    <script src="' . base_url() . 'js/axios.js"></script>
                    <script src="' . base_url() . 'js/vue.js"></script>
                    <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>              
                    <script src="' . base_url() . 'js/touchspin.js"></script>
                    <script src="' . base_url() . 'js/mascara.js"></script>
                    <script src="' . base_url() . 'js/select2.js"></script>
                    <script src="' . base_url() . 'js/select2-init.js"></script>
                    <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                    <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>    
                    <script src="' . base_url() . 'web/js/free_captive/upsert.js?'. microtime().'"></script>';
            $data['css_include']  = '
                    <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
                    <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                    <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                    <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
            $data['main_content'] = 'free_captive/upsert';
            $this->load->view('includes/template', $data);
            
        } catch (Exception $e) {
            easyPrint($e->getTraceAsString(), 1);
        }

    }

    function delete_file() {
        if ($this->input->post("delete") == "true") {
            $this->Free_captive_model->_pk_free_captive = $this->input->post("file_pk_free_captive");
            $this->Free_captive_model->delete_file();
        }
        redirect('tusd/upsert');
    }
    
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Free_captive_model->_month    = $this->input->post("month_sendmail");
            $this->Free_captive_model->_year     = $this->input->post("year_sendmail");
            $this->Free_captive_model->_fk_agent = $this->input->post("fk_agent_sendmail");
            $this->Free_captive_model->_email    = $this->Free_captive_model->_status_active;
            $free_captives                       = $this->Free_captive_model->fetch();

            if ($free_captives) {
                //pega o agente
                $agent = $this->_get_agent($this->Free_captive_model->_fk_agent);
                if ($agent) {
                    
                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($free_captives, $destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->Free_captive_model->_fk_agent);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($this->Free_captive_model->_year, $this->Free_captive_model->_month);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($free_captives, $contact_list);
                            }

                        }

                    }
                    
                }
            }
        }
        redirect("free_captive");
    }
    
    private function _post_mail($free_captives, $to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Relatório de Energia";
        $data['free_captives'] = $free_captives;
        $msg = $this->load->view('free_captive/email/upsert', $data, true);
        $post['message'] = base64_encode($msg);

        $files = [];
        foreach ($free_captives as $free_captive) {
            if ($free_captive['report'] != "") {
                $files[] = 'uploads/free_captive/' . $free_captive['pk_free_captive'] . '/' . $free_captive['report'];
            }

            $post['files'] = join(",", $files);            
        }

        $pymail = pymail($post);
    }    
    
    function delete() {
        if ($this->input->post('pk_free_captive') > 0) {
            $this->Free_captive_model->_pk_free_captive = $this->input->post('pk_free_captive');
            $this->Free_captive_model->delete();
        }
        redirect('free_captive/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
    }

    private function _fill_model() {
        $this->Free_captive_model->_month                      = $this->input->post('month');
        $this->Free_captive_model->_year                       = $this->input->post('year');
        $this->Free_captive_model->_fk_agent                   = $this->input->post('fk_agent');
        $this->Free_captive_model->_fk_unity                   = $this->input->post('fk_unity');
        $this->Free_captive_model->_economy                    = $this->input->post('economy');
        $this->Free_captive_model->_captive_total              = $this->input->post('captive_total');
        $this->Free_captive_model->_free_total                 = $this->input->post('free_total');
        $this->Free_captive_model->_fk_agent_power_distributor = $this->input->post('fk_agent_power_distributor');
        $this->Free_captive_model->_status                     = $this->Free_captive_model->_status_active;
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_unities($free_captives) {
        $return                       = false;
        $this->load->model("Unity_model");
        $this->Unity_model->_fk_agent = $this->Free_captive_model->_fk_agent;
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $unities                      = $this->Unity_model->fetch();

        if ($unities) {
            foreach ($unities as $k => $unity) {
                $return[$unity['pk_unity']]['unity']        = $unity;
                $return[$unity['pk_unity']]['free_captive'] = false;
            }
            $return = $this->_process_free_captive($return, $free_captives);
        }
        return $return;
    }

    private function _process_free_captive($return, $free_captives) {
        if ($free_captives) {
            foreach ($return as $k => $unity) {
                foreach ($free_captives as $fc) {
                    if ($k == $fc['fk_unity']) {
                        $return[$k]["free_captive"] = $fc;
                    }
                }
            }
        }
        return $return;
    }

    private function _process_files() {
        try {
            $this->load->library('upload');
            $files                        = $_FILES;
            
            if (isset($files['report']['name'][$this->Free_captive_model->_fk_unity]) && !empty($files['report']['name'][$this->Free_captive_model->_fk_unity])) {
                $_FILES['report']['name']     = $files['report']['name'][$this->Free_captive_model->_fk_unity];
                $_FILES['report']['type']     = $files['report']['type'][$this->Free_captive_model->_fk_unity];
                $_FILES['report']['tmp_name'] = $files['report']['tmp_name'][$this->Free_captive_model->_fk_unity];
                $_FILES['report']['error']    = $files['report']['error'][$this->Free_captive_model->_fk_unity];
                $_FILES['report']['size']     = $files['report']['size'][$this->Free_captive_model->_fk_unity];

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('report')) {
                    $this->Free_captive_model->_report = str_replace(" ", "_", $_FILES['report']['name']);
                    $this->Free_captive_model->update();
                }
            }
        } catch (Exception $e) {
            easyPrint($e->getMessage(), 1);
        }

    }

    private function set_upload_options() {
        $config['upload_path']   = './uploads/free_captive/' . $this->Free_captive_model->_pk_free_captive;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        $config['overwrite']     = FALSE;
        check_dir_exists($config['upload_path']);
        return $config;
    }

}
