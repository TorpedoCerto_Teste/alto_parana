<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Unity_model');
    }

    function index() {
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $data['list']               = $this->Unity_model->fetch();
        $data['js_include']         = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']        = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']       = 'unity/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $this->load->model('Agent_model');
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $agent                        = $this->Agent_model->read();
        if (!$agent) {
            redirect('agent/index');
        }

        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Unity_model->create();
                if ($create) {
                    redirect('agent/view/' . $this->Unity_model->_fk_agent . '/unidades');
                }
            }
            $data['error'] = true;
        }
        //envia um json formatado para que complete o endereço do agente quando clicar no botão
        $data['agent_address']             = json_encode($this->Agent_model);
        //lista as distribuidoras
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $data['agents_distrib']            = $this->Agent_model->fetch();
        //lista os perfis
        $this->load->model('Profile_model');
        $this->Profile_model->_fk_agent    = $this->Agent_model->_pk_agent;
        $this->Profile_model->_status      = $this->Profile_model->_status_active;
        $data['profiles']                  = $this->Profile_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/icheck/skins/icheck.min.js"></script>
            <script src="' . base_url() . 'web/js/unity/create.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            <link href="' . base_url() . 'js/icheck/skins/all.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />';
        $data['main_content'] = 'unity/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $this->load->model('Agent_model');
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $agent                        = $this->Agent_model->read();
        if (!$agent) {
            redirect('agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Unity_model->_pk_unity = $this->uri->segment(4, 0);
                $update = $this->Unity_model->update();
                redirect('unity/view/'.$this->Agent_model->_pk_agent.'/'.$this->Unity_model->_pk_unity);
            }
            $data['error'] = true;
        }

        $this->Unity_model->_pk_unity = $this->uri->segment(4, 0);
        $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
        $read                         = $this->Unity_model->read();

        if (!$read) {
            redirect('agent/view/' . $this->Agent_model->_pk_agent . '/unidades');
        }
        $data['error'] = false;

        //lista as distribuidoras
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $data['agents_distrib']            = $this->Agent_model->fetch();
        //lista os perfis
        $this->load->model('Profile_model');
        $this->Profile_model->_fk_agent    = $this->Agent_model->_pk_agent;
        $this->Profile_model->_status      = $this->Profile_model->_status_active;
        $data['profiles']                  = $this->Profile_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/icheck/skins/icheck.min.js"></script>
            <script src="' . base_url() . 'web/js/unity/create.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            <link href="' . base_url() . 'js/icheck/skins/all.css" rel="stylesheet">
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />';
        $data['main_content'] = 'unity/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_unity') > 0) {
            $this->Unity_model->_pk_unity = $this->input->post('pk_unity');
            $this->Unity_model->delete();
        }
        redirect('unity/index');
    }
    
    function delete_by_agent() {
        if ($this->input->post('delete')) {
            $this->load->model("Agent_model"); 
            $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
            $read                         = $this->Agent_model->read();
            if (!$read) {
                redirect('agent/index');
            }
            
            $this->Unity_model->_pk_unity = $this->input->post('fk_unity');
            $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
            $read                         = $this->Unity_model->read();
            if (!$read) {
                redirect('agent/index');
            }

            $delete_unity = $this->Unity_model->delete();
            if ($delete_unity) {
                $this->load->model("Gauge_model"); 
                $this->Gauge_model->_fk_unity = $this->Unity_model->_pk_unity;
                $this->Gauge_model->delete_by_unity();
            }
            
            redirect('agent/view/'.$this->Unity_model->_fk_agent.'#unidades');
        }
        
        
    }
    
    function view() {
        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        $read                         = $this->Agent_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->Unity_model->_pk_unity = $this->uri->segment(4, 0);
        $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
        $read                         = $this->Unity_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        //lista as distribuidoras
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $data['agents_distrib']            = $this->Agent_model->fetch();
        //lista os perfis
        $this->load->model('Profile_model');
        $this->Profile_model->_fk_agent    = $this->Agent_model->_pk_agent;
        $this->Profile_model->_status      = $this->Profile_model->_status_active;
        $data['profiles']                  = $this->Profile_model->fetch();

        //contatos (contacts e unity_contacts)
        $this->load->model('Unity_contact_model');
        $this->Unity_contact_model->_fk_unity = $this->Unity_model->_pk_unity;
        $data['unity_contacts']               = $this->Unity_contact_model->fetch();


        //demandas
        $this->load->model('Demand_model');
        $this->Demand_model->_fk_unity = $this->Unity_model->_pk_unity;
        $data['demands']               = $this->Demand_model->fetch();

        //proinfas
        $this->load->model('Proinfa_model');
        $this->Proinfa_model->_fk_unity = $this->Unity_model->_pk_unity;
        $data['proinfas']               = $this->Proinfa_model->fetch();

        //medidores
        $this->load->model('Gauge_model');
        $this->Gauge_model->_fk_unity = $this->Unity_model->_pk_unity;
        $data['gauges']               = $this->Gauge_model->fetch();


        //Uploads
        $this->load->helper('directory');
        $directory      = './uploads/agent/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity;
        $data['upload'] = directory_map($directory);


        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'web/js/unity/create.js"></script>
            <script src="' . base_url() . 'js/dropzone.js"></script>
            <script src="' . base_url() . 'web/js/agent/dropzone.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/icheck/skins/icheck.min.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dropzone.css" rel="stylesheet">
            <link href="' . base_url() . 'js/icheck/skins/all.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />';
        $data['main_content'] = 'unity/view';
        $this->load->view('includes/template', $data);
    }

    //dropzone (arrastar e soltar arquivos)
    function dropzone() {
        $config['upload_path']   = './uploads/agent/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $return = array(
                'error'   => true,
                'message' => strip_tags($this->upload->display_errors()));
        }
        else {
            $return = array(
                'error'   => false,
                'message' => $this->upload->data());
        }
        print json_encode($return);
    }

    function delete_file() {
        if ($this->input->post('delete') == 'true') {
            unlink('./uploads/agent/' . $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->input->post('file_to_delete'));
        }
        redirect('unity/view/' . $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/arquivos');
    }

    function get_profile() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $this->Unity_model->_pk_unity = $this->uri->segment(3, 0);
        if ($this->Unity_model->read()) {
            print_r($this->Unity_model->_fk_profile);
            return true;
        }
        return false;
    }

    function ajax_find_unity() {
        $this->disableLayout          = TRUE;
        $this->output->set_content_type('application/json');
        $this->Unity_model->_pk_unity = $this->input->post('pk_unity');
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $fetch                        = $this->Unity_model->fetch();
        print_r(json_encode($fetch));
    }

    function ajax_fetch_unity_from_agent() {
        $this->disableLayout          = TRUE;
        $this->output->set_content_type('application/json');
        $this->Unity_model->_fk_agent = $this->input->post('fk_agent');
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $fetch                        = $this->Unity_model->fetch();
        print_r(json_encode($fetch));
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_profile', 'fk_profile', 'trim');
        $this->form_validation->set_rules('fk_agent_power_distributor', 'fk_agent_power_distributor', 'trim|required');
        $this->form_validation->set_rules('company_name', 'company_name', 'trim|required');
        $this->form_validation->set_rules('community_name', 'community_name', 'trim|required');
        $this->form_validation->set_rules('cnpj', 'cnpj', 'trim');
        $this->form_validation->set_rules('ie', 'ie', 'trim');
        $this->form_validation->set_rules('uc_number', 'uc_number', 'trim');
        $this->form_validation->set_rules('ape_status', 'ape_status', 'trim|required');
        $this->form_validation->set_rules('siga', 'siga', 'trim');
        $this->form_validation->set_rules('postal_code', 'postal_code', 'trim');
        $this->form_validation->set_rules('address', 'address', 'trim');
        $this->form_validation->set_rules('number', 'number', 'trim');
        $this->form_validation->set_rules('complement', 'complement', 'trim');
        $this->form_validation->set_rules('district', 'district', 'trim');
        $this->form_validation->set_rules('city', 'city', 'trim');
        $this->form_validation->set_rules('state', 'state', 'trim');
        $this->form_validation->set_rules('memo', 'memo', 'trim');
        $this->form_validation->set_rules('icms', 'icms', 'trim');
        $this->form_validation->set_rules('memo_icms', 'memo_icms', 'trim');
        $this->form_validation->set_rules('iCheck', 'activity', 'trim');
        $this->form_validation->set_rules('communion_acronym', 'communion_acronym', 'trim');
        $this->form_validation->set_rules('credit_icms', 'credit_icms', 'trim');
        $this->form_validation->set_rules('free_market_entry', 'free_market_entry', 'trim');
    }

    private function _fill_model() {
        $this->Unity_model->_fk_agent                   = $this->Agent_model->_pk_agent;
        $this->Unity_model->_fk_profile                 = $this->input->post('fk_profile');
        $this->Unity_model->_fk_agent_power_distributor = $this->input->post('fk_agent_power_distributor');
        $this->Unity_model->_company_name               = mb_strtoupper($this->input->post('company_name'), 'UTF-8');
        $this->Unity_model->_community_name             = mb_strtoupper($this->input->post('community_name'), 'UTF-8');
        $this->Unity_model->_cnpj                       = preg_replace("/([^\d]*)/", "", $this->input->post('cnpj'));
        $this->Unity_model->_ie                         = mb_strtoupper($this->input->post('ie'), 'UTF-8');
        $this->Unity_model->_uc_number                  = mb_strtoupper($this->input->post('uc_number'), 'UTF-8');
        $this->Unity_model->_ape_status                 = $this->input->post('ape_status');
        $this->Unity_model->_siga                       = mb_strtoupper($this->input->post('siga'), 'UTF-8');
        $this->Unity_model->_postal_code                = preg_replace("/([^\d]*)/", "", $this->input->post('postal_code'));
        $this->Unity_model->_address                    = mb_strtoupper($this->input->post('address'), 'UTF-8');
        $this->Unity_model->_number                     = mb_strtoupper($this->input->post('number'), 'UTF-8');
        $this->Unity_model->_complement                 = mb_strtoupper($this->input->post('complement'), 'UTF-8');
        $this->Unity_model->_district                   = mb_strtoupper($this->input->post('district'), 'UTF-8');
        $this->Unity_model->_city                       = mb_strtoupper($this->input->post('city'), 'UTF-8');
        $this->Unity_model->_state                      = mb_strtoupper($this->input->post('state'), 'UTF-8');
        $this->Unity_model->_status                     = $this->Unity_model->_status_active;
        $this->Unity_model->_memo                       = $this->input->post('memo');
        $this->Unity_model->_icms                       = number_format($this->input->post('icms'), 2, '.', '');
        $this->Unity_model->_memo_icms                  = $this->input->post('memo_icms');
        $this->Unity_model->_activity                   = mb_strtoupper($this->input->post('iCheck'), 'UTF-8');
        $this->Unity_model->_communion_acronym          = mb_strtoupper($this->input->post('communion_acronym'), 'UTF-8');
        $this->Unity_model->_credit_icms                = number_format($this->input->post('credit_icms'), 2, '.', '');
        $this->Unity_model->_free_market_entry          = human_date_to_date($this->input->post('free_market_entry'), 1);
    }

}
