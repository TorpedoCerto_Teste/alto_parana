<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pld extends Public_Controller
{

    function __construct()
    {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Pld_model');
    }

    function index()
    {
        redirect('pld/upsert');
    }

    function upsert()
    {
        $data['year'] = preg_replace('/([^\d*])/', '', $this->uri->segment(3, date('Y')));

        if ($this->input->post('upsert') == 'true') {
            $form_data = $this->input->post('data');
            if ($form_data) {
                foreach ($form_data as $item) {
                    $item['year'] = $this->input->post('year');
                    $this->_fill_model($item);
                    $this->Pld_model->upsert();
                }
            }
        }

        $this->Pld_model->_status = $this->Pld_model->_status_active;
        $this->Pld_model->_year = strlen($data['year']) == 4 ? $data['year'] : date('Y');
        $list                   = $this->Pld_model->fetch();

        $data['list'] = $this->_prepare_data($list);


        $data['js_include']   = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/pld/upsert.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">';
        $data['main_content'] = 'pld/upsert';
        $this->load->view('includes/template', $data);
    }

    private function _fill_model($data)
    {
        $this->Pld_model->_month  = $data['month'];
        $this->Pld_model->_year = $data['year'];
        $this->Pld_model->_seco   = $data['seco'];
        $this->Pld_model->_s   = $data['s'];
        $this->Pld_model->_ne   = $data['ne'];
        $this->Pld_model->_n   = $data['n'];
        $this->Pld_model->_spread_i5   = $data['spread_i5'];
        $this->Pld_model->_spread_i1   = $data['spread_i1'];
        $this->Pld_model->_status_pld   = $data['status_pld'];
        $this->Pld_model->_status   = $this->Pld_model->_status_active;
    }

    private function _prepare_data($list)
    {
        $return = [];
        for ($i = 0; $i < 12; $i++) {
            $return[$i] = [
                "month_name" => retorna_mes($i + 1),
                "month" => $i + 1,
                "seco" => 0.0,
                "s" => 0.0,
                "ne" => 0.0,
                "n" => 0.0,
                "spread_i5" => 0.0,
                "spread_i1" => 0.0,
                "status_pld" => 0
            ];
        }

        if ($list) {
            foreach ($list as $k => $item) {
                $return[$k]['seco'] = number_format($item['seco'], 2);
                $return[$k]['s'] = number_format($item['s'], 2);
                $return[$k]['ne'] = number_format($item['ne'], 2);
                $return[$k]['n'] = number_format($item['n'], 2);
                $return[$k]['spread_i5'] = number_format($item['spread_i5'], 2);
                $return[$k]['spread_i1'] = number_format($item['spread_i1'], 2);
                $return[$k]['status_pld'] = $item['status_pld'];
            }
        }

        return $return;
    }
}
