<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Contact
 *
 * @author luancomputacao
 */
class Contact extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Contact_model');
    }

    function index() {
        $this->Contact_model->_fk_agent = $this->uri->segment(3);
        $data['list']                   = $this->Contact_model->fetch();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
                <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
                <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
                <script src="' . base_url() . 'web/js/index_index.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">';
        $data['main_content'] = 'contact/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            $this->form_validation->set_rules('password', 'password', 'trim|callback_check_allow_access'); //regra se aplica somente para criar uma senha nova

            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $fk_contact = $this->Contact_model->create();
                if ($fk_contact) {
                    //verificar se foi informado o fk_agent e inserir em agent_contact
                    $fk_agent = $this->uri->segment(3, 0);
                    if ($fk_agent) {
                        $this->_create_agent_contact($fk_agent, $fk_contact);
                        redirect('agent/view/' . $fk_agent . '/contatos');
                    }
                    redirect('agent');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/contact/create.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'contact/create';
        $this->load->view('includes/template', $data);
    }
    
    function update() {
        $data['error']                    = false;
        $this->Contact_model->_fk_agent   = $this->uri->segment(3);
        $this->Contact_model->_pk_contact = $this->uri->segment(4);
        if (is_numeric($this->Contact_model->_fk_agent) && is_numeric($this->Contact_model->_pk_contact)) {
            if (!$this->Contact_model->read()) {
                redirect('contact/index');
            }
        }

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->Contact_model->_allow_access == 0) {
                $this->form_validation->set_rules('password', 'password', 'trim|callback_check_allow_access'); //regra se aplica somente para criar uma senha nova
            }
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Contact_model->update();
                if ($update) {
                    redirect('agent/view/' . $this->Contact_model->_fk_agent . '/contatos');
                }
            }
            $data['error'] = true;
        }
        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/contact/create.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'contact/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_contact') > 0) {
            $this->Contact_model->_pk_contact = $this->input->post('pk_contact');
            $this->Contact_model->delete();
        }
        redirect('agent/view/'.$this->uri->segment(3).'/contatos');
    }

    function status() {
        if ($this->uri->segment(3, 0) <= 0 || $this->uri->segment(4, 0) <= 0) {
            redirect('agent/index');
        }
        $this->Contact_model->_fk_agent   = $this->uri->segment(3);
        $this->Contact_model->_pk_contact = $this->uri->segment(4);
        $read                             = $this->Contact_model->read();
        if (!$read) {
            redirect('agent/index');
        }
        $this->Contact_model->_status = $this->Contact_model->_status == $this->Contact_model->_status_active ? $this->Contact_model->_status_inactive : $this->Contact_model->_status_active;
        $update                       = $this->Contact_model->update();
        redirect('agent/view/' . $this->Contact_model->_fk_agent . '/contatos');
    }

    public function check_allow_access() {
        if ($this->input->post('allow_access') == 1) {
            if (trim($this->input->post('password')) == "") {
                return false;
            }
        }
        return true;
    }

    public function check_email() {
        if ($this->Contact_model->_email != $this->input->post("email")) {
            $this->Contact_model->_email = $this->input->post("email");
            return $this->Contact_model->read_by_email_unique();
        }
        return true;
    }
    
    private function _validate_form() {
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('surname', 'surname', 'trim');
        $this->form_validation->set_rules('phone', 'phone', 'trim');
        $this->form_validation->set_rules('extension', 'extension', 'trim');
        $this->form_validation->set_rules('mobile', 'mobile', 'trim');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|callback_check_email',  array('check_email' => 'Este email já está sendo usado por outro cadastro.'));
        $this->form_validation->set_rules('responsibility', 'responsibility', 'trim');
        $this->form_validation->set_rules('allow_access', 'allow_access', 'trim');
    }

    private function _fill_model() {
        $this->Contact_model->_name           = mb_strtoupper($this->input->post('name'), 'UTF-8');
        $this->Contact_model->_surname        = mb_strtoupper($this->input->post('surname'), 'UTF-8');
        $this->Contact_model->_phone          = preg_replace("/([^\d]*)/", "", $this->input->post('phone'));
        $this->Contact_model->_extension      = preg_replace("/([^\d]*)/", "", $this->input->post('extension'));
        $this->Contact_model->_mobile         = preg_replace("/([^\d]*)/", "", $this->input->post('mobile'));
        $this->Contact_model->_email          = $this->input->post('email');
        $this->Contact_model->_birthdate      = human_date_to_date($this->input->post('birthdate'));
        $this->Contact_model->_allow_access   = $this->input->post('allow_access') == 1 ? 1 : 0;
        $this->Contact_model->_password       = $this->input->post('allow_access') == 1 && trim($this->input->post('password')) != "" ? md5($this->input->post('password')) : "";
        $this->Contact_model->_responsibility = mb_strtoupper($this->input->post('responsibility'), 'UTF-8');
        $this->Contact_model->_status         = $this->Contact_model->_status_active;
    }

    private function _create_agent_contact($fk_agent, $fk_contact) {
        $this->load->model('Agent_contact_model');
        $this->Agent_contact_model->_fk_contact = $fk_contact;
        $this->Agent_contact_model->_fk_agent   = $fk_agent;
        $this->Agent_contact_model->_status     = $this->Agent_contact_model->_status_active;
        return $this->Agent_contact_model->create();
    }

    
}
