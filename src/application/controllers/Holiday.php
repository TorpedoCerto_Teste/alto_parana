<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holiday extends Public_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Holiday_model');
    }

    function index() {
        $this->Holiday_model->_date_holiday = date("Y");
        
        if ($this->input->post('year')) {
            $this->Holiday_model->_year = $this->input->post('year');
        }
        $this->Holiday_model->_status = $this->Holiday_model->_status_active;
        $data['list']                 = $this->Holiday_model->fetch();
        $data['js_include']          = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'web/js/holiday/index.js"></script>
            ';
        $data['css_include']         = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            ';
        $data['main_content']        = 'holiday/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Holiday_model->_created_at = date("Y-m-d H:i:s");
                $create                           = $this->Holiday_model->create();
                if ($create) {
                    redirect('holiday/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
                <script src="' . base_url() . 'js/mascara.js"></script>
        ';
        $data['css_include']  = '
        ';
        
        $data['main_content'] = 'holiday/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('holiday/index');
        }
        $this->Holiday_model->_pk_holiday = $this->uri->segment(3);
        $read                             = $this->Holiday_model->read();
        if (!$read) {
            redirect('holiday/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Holiday_model->update();
                redirect('holiday/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'holiday/update';
        $this->load->view('template/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_holiday') > 0) {
            $this->Holiday_model->_pk_holiday = $this->input->post('pk_holiday');
            $this->Holiday_model->delete();
        }
        redirect('holiday');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('date_holiday', 'date_holiday', 'trim|required');
        $this->form_validation->set_rules('holiday', 'holiday', 'trim');
    }

    private function _fill_model() {
        $this->Holiday_model->_date_holiday = human_date_to_date($this->input->post('date_holiday'));
        $this->Holiday_model->_holiday      = mb_strtoupper($this->input->post('holiday'), 'UTF-8');
    }

}
