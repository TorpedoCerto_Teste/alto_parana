<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Retorna todos os resultados de acesso a API em formato json
 *
 * @author rogeriopellarin
 */
class Api_vue extends Public_Controller {

    function __construct() {
        parent::__construct();
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        @header('Access-Control-Allow-Origin: *');
        @header('Access-Control-Allow-Methods: GET, POST');

        //verifica se há sessão, caso não haja, devolve negativa
        if (!$this->session->userdata('token')) {
            $this->_response('error', 'Sessão expirou');
        }
    }

    public function getAgentToken() {
        $data = [
            'token' => $this->session->userdata('token')
        ];
        $this->_response('success', '', $data);
    }

    public function getCcveeList() {
        $token     = $this->session->userdata('token');
        $api_token = $this->input->get('token');
        if ($token == $api_token) {

            $month = $this->input->get('month');
            $year  = $this->input->get('year');
            $date  = $year . "-" . $month . "-01";

            $data = $this->_processCcveeList($date);
            $this->_response('success', '', $data);
        } else {
            $this->_response('error', 'Token inválido');
        }
    }
    
    public function filter() {
        $filter = json_decode($this->input->get('filter'), true);

        $date = $filter['year']."-".str_pad($filter['month'],2,'0', STR_PAD_LEFT)."-01";
        $fDate = "01/".str_pad($filter['month'],2,'0', STR_PAD_LEFT)."/".$filter['year'];
        
        //pega a lista de unidades atendidas pelo contrato
        $data['unities']        = $this->_get_unities_from_ccvee($filter['ccvee']);
        $data['fk_unities']     = $this->_get_fk_unities($data['unities']);
        $data['gauges']         = $this->_process_gauge($data['fk_unities']);
        $data['fk_gauges']      = $this->_get_fk_gauges($data['gauges']);
        $data['hours']          = hours_of_month($fDate);
        $data['energy']         = $this->_process_ccvee_energy($filter['ccvee']);
        $data['energy_bulk']    = $this->_process_ccvee_energy_bulk($date, $filter['ccvee'], $data['energy'][0]['differentiated_volume_period']);
        $proinfa                = $this->_process_proinfa_by_unity($date, $data['fk_unities']);
        $data['proinfa']        = $proinfa['total'];
        $data['proinfa_unity']  = $proinfa['unity'];
        $data['daysInMonth']    = date("t", strtotime($date));
        switch ($filter['source']) {
            case 'w2':
                $data['records'] = $this->_process_telemetry_record($date, $data['fk_gauges']);

                if (!$data['records']) {
                    $data['records'] = $this->_process_gauge_record_for_dashboard($date, $data['fk_gauges']);
                }
                break;
            case 'scde':
                $data['records'] = $this->_process_gauge_record_for_dashboard($date, $data['fk_gauges']);
                break;
            case 'odc':
                break;
        }
        
        
        $this->_response('success', '', $data);
    }
    
    private function _get_fk_gauges($gauges) {
        $gauges_list = array();
        foreach ($gauges as $gauge) {
            $gauges_list[] = $gauge['pk_gauge'];
        }
        return $gauges_list;
    }
    
    private function _get_fk_unities($unities) {
        $unity_list = array();
        foreach ($unities as $unity) {
            $unity_list[] = $unity['fk_unity'];
        }
        return $unity_list;    
    }
    
    private function _process_gauge($fk_unities) {
        $this->load->model("Gauge_model");
        $this->Gauge_model->_status   = $this->Gauge_model->_status_active;
        $this->Gauge_model->_fk_unity = $fk_unities;
        return $this->Gauge_model->fetch();
    }

    private function _process_telemetry_record($date, $gauges_list) {
        $this->load->model("Telemetry_record_model");
        $this->Telemetry_record_model->_fetch_period = "month";
        $this->Telemetry_record_model->_record_date  = $date;
        $this->Telemetry_record_model->_fk_gauge     = $gauges_list;
        $this->Telemetry_record_model->_status       = $this->Telemetry_record_model->_status_active;
        return $this->Telemetry_record_model->fetch_for_dashboard();
    }

    private function _process_gauge_record_for_dashboard($date, $gauges_list) {
        try {
            $return = false;
            $report = $this->_process_gauge_record($date, $gauges_list);
            if ($report) {
                foreach ($report as $data) {
                    $return[] = [
                        "fk_gauge" => $data["fk_gauge"],
                        "record_date" => $data["record_date"],
                        "day" => date('d', strtotime($data["record_date"])),
                        "value" => $data["active_power_consumption"],
                        "order" => $data["order"],
                        "error" => "0"
                    ];
                }
            }
            return $return;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    private function _process_gauge_record($date, $gauges_list) {
        $this->load->model("Gauge_record_model");
        $this->Gauge_record_model->_record_date  = $date;
        $this->Gauge_record_model->_fk_gauge     = $gauges_list;
        $this->Gauge_record_model->_status       = $this->Gauge_record_model->_status_active;
        return $this->Gauge_record_model->fetch_by_report();
    }
    
    private function _get_unities_from_ccvee($fk_ccvee_general) {
        $this->load->model("Ccvee_unity_model");
        $this->Ccvee_unity_model->_fk_ccvee_general = $fk_ccvee_general;
        $this->Ccvee_unity_model->_status = $this->Ccvee_unity_model->_status_active;
        return $this->Ccvee_unity_model->fetch();
    }

    private function _processCcveeList($date) {
        $data['unities'] = $this->_process_unities2();

        if ($data['unities']) {
            $data['ccvee_generals'] = $this->_process_ccvee_general($date, $data['unities']);
        }

        $data['pld'] = $this->_process_pld($data['ccvee_generals']);

        return $data;
    }

    private function _process_unities2() {
        $this->load->model("Unity_model");
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $this->Unity_model->_fk_agent = $this->session->userdata('fk_agent');
        return $this->Unity_model->fetch();
    }

    private function _process_ccvee_general($date, $unities) {
        $unity_list = array();
        foreach ($unities as $unity) {
            $unity_list[] = $unity['pk_unity'];
        }

        $general_energy = $this->_process_general_energy($date, $unity_list);
        
        return $general_energy;
    }

    private function _process_general_energy($date, $unity_list) {
        $this->load->model("Ccvee_general_model");
        $this->Ccvee_general_model->_fk_unity   = $unity_list;
        $this->Ccvee_general_model->_created_at = $date;
        return $this->Ccvee_general_model->fetch_general_energy_by_unity();
    }
    
    private function _process_ccvee_energy($fk_ccvee_energy) {
        $this->load->model("Ccvee_energy_model");
        $this->Ccvee_energy_model->_status          = $this->Ccvee_energy_model->_status_active;
        $this->Ccvee_energy_model->_pk_ccvee_energy = $fk_ccvee_energy;
        return $this->Ccvee_energy_model->fetch();
    }

    private function _process_ccvee_energy_bulk($date, $fk_ccvee_energy, $differentiated_volume_period) {
        $this->load->model("Ccvee_energy_bulk_model");
        $this->Ccvee_energy_bulk_model->_status          = $this->Ccvee_energy_bulk_model->_status_active;
        if ($differentiated_volume_period) {
            $this->Ccvee_energy_bulk_model->_date            = date("Y-m-01", strtotime($date));
        }
        $this->Ccvee_energy_bulk_model->_fk_ccvee_energy = $fk_ccvee_energy;
        return $this->Ccvee_energy_bulk_model->fetch();
    }

    private function _process_proinfa_by_unity($date, $unity_list) {
        $return = array(
            'total' => 0,
            'unity' => array()
        );
        $proinfas = $this->_process_proinfa($date, $unity_list);
        if ($proinfas) {
            foreach ($proinfas as $proinfa) {
                $return['total'] += $proinfa['value_mwh'];
                $return['unity'][$proinfa['fk_unity']] = $proinfa['value_mwh'];
            }
        }
        return $return;
    }
    
    private function _process_proinfa($date, $fk_unity) {
        $this->load->model("Proinfa_model");
        $this->Proinfa_model->_fk_unity = $fk_unity;
        $this->Proinfa_model->_month    = date("m", strtotime($date));
        $this->Proinfa_model->_year     = date("Y", strtotime($date));
        $this->Proinfa_model->_status   = $this->Proinfa_model->_status_active;
        return $this->Proinfa_model->fetch();
    }

    private function _process_pld($ccvee_generals) {
        $return = [
            "pld_value" => 0,
            "spread" => 0,
            "status" => 0,
            "credit_date" => null
        ];
        $status = [
            "0" => "PREVISTO",
            "1" => "FECHADO"
        ];

        $this->load->model("Pld_model");
        $this->Pld_model->_year = $this->input->get('year');
        $this->Pld_model->_month = $this->input->get('month');
        $this->Pld_model->_status = $this->Pld_model->_status_active;
        $pld = $this->Pld_model->read();

        $this->load->model("Accounting_market_model");
        $this->Accounting_market_model->_year = $this->input->get('year');
        $this->Accounting_market_model->_month = $this->input->get('month');
        $this->Accounting_market_model->_status = $this->Accounting_market_model->_status_active;
        $accounting_market = $this->Accounting_market_model->read();

        if ($pld) {
            $return["pld_value"] = floatval($this->_get_pld_value($ccvee_generals, $this->Pld_model)); 
            $return["spread"] = floatval($this->_get_spread($ccvee_generals, $this->Pld_model)); 
            $return["status"] = $status[$this->Pld_model->_status_pld]; 
            $return["credit_date"] = $this->Accounting_market_model->_credit_date !== "" ? date_to_human_date($this->Accounting_market_model->_credit_date) : "Indefinida"; 
        }

        return $return;
    }

    private function _get_pld_value($ccvee_generals, $pld_model) {
        $submarkets = [
            1 => "s",
            2 => "seco",
            3 => "ne",
            4 => "n"
        ];
        $varname = $submarkets[$ccvee_generals[0]["fk_submarket"]];
        return $pld_model->{"_" . $varname};
    }

    private function _get_spread($ccvee_generals, $pld_model) {
        switch ($ccvee_generals[0]['energy_type_name']) {
            case 'I5':
                return $pld_model->_spread_i5;
                break;
            
            case 'I1':
                return $pld_model->_spread_i1;
                break;
            
            default:
                return 0;
                break;
        }
    }

    private function _response($status, $message, $data = [], $url = '') {
        $response = [
            'status'  => $status,
            'message' => $message,
            'data'    => $data,
            'url'     => $url
        ];
        print json_encode($response);
        exit;
    }

}
