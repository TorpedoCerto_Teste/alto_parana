<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar_responsible extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Calendar_responsible_model');
        
    }

    function index() {
        $this->Calendar_responsible_model->_status = $this->Calendar_responsible_model->_status_active;
        $data['list']              = $this->Calendar_responsible_model->fetch();
        $data['js_include']        = '
            <script type="text/javascript" src="'.base_url().'assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="'.base_url().'assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
            ';
        $data['css_include']       = '
            <link rel="stylesheet" href="'.base_url().'assets/plugins/DataTables/media/css/DT_bootstrap.css" />
            ';
        
        $data['main_content']      = 'calendar_responsible/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Calendar_responsible_model->_created_at = date("Y-m-d H:i:s");
                $create                        = $this->Calendar_responsible_model->create();
                if ($create) {
                    redirect('calendar_responsible/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="'.base_url().'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar_responsible/create';
        $this->load->view('template/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('calendar_responsible/index');
        }
        $this->Calendar_responsible_model->_pk_calendar_responsible = $this->uri->segment(3);
        $read                       = $this->Calendar_responsible_model->read();
        if (!$read) {
            redirect('calendar_responsible/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Calendar_responsible_model->update();
                redirect('calendar_responsible/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="'.base_url().'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar_responsible/update';
        $this->load->view('template/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('calendar_responsible/index');
        }
        $this->Calendar_responsible_model->_pk_calendar_responsible = $this->uri->segment(3);
        $read                       = $this->Calendar_responsible_model->read();
        if (!$read) {
            redirect('calendar_responsible/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Calendar_responsible_model->upsert();
                redirect('calendar_responsible/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
            <script type="text/javascript" src="'.base_url().'assets/js/mascara.js"></script>
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'calendar_responsible/upsert';
        $this->load->view('template/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_calendar_responsible') > 0) {
            $this->Calendar_responsible_model->_pk_calendar_responsible = $this->input->post('pk_calendar_responsible');
            $this->Calendar_responsible_model->delete();
        }
        redirect('calendar_responsible');
    }

    private function _validate_form() {
                $this->form_validation->set_rules('responsible', 'responsible', 'trim|required');

    }

    private function _fill_model() {
                $this->Calendar_responsible_model->_responsible       = mb_strtoupper($this->input->post('responsible'), 'UTF-8');

    }

}
