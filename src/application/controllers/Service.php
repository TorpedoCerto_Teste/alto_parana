<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Service_model');
    }

    function index() {
        redirect('service/update');
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('service', 'service', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Service_model->_service = mb_strtoupper($this->input->post('service'), 'UTF-8');
                $this->Service_model->_status  = $this->Service_model->_status_active;
                $pk_service                    = $this->Service_model->create();
                if ($pk_service) {
                    $products = $this->input->post('product');
                    if (!empty($products)) {
                        $this->_create_service_product($products, $pk_service);
                    }
                    redirect('service/update');
                }
            }
            $data['error'] = true;
        }

        //pega a lista de product_category e product
        $data['products'] = $this->_process_product();
//        easyPrint($data['products'], 1);

        $data['js_include']   = '
            <script src="' . base_url() . 'web/js/service/create.js"></script>
            ';
        $data['css_include']  = '';
        $data['main_content'] = 'service/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        $data['success'] = false;

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('pk_service', 'pk_service', 'trim|required');
            $this->form_validation->set_rules('service', 'service', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                //busca pelo serviço selecionado
                $this->Service_model->_pk_service = $this->input->post('pk_service');
                $read                             = $this->Service_model->read();
                if ($read) {
                    $this->Service_model->_service = mb_strtoupper($this->input->post('service'), 'UTF-8');
                    $this->Service_model->update();

                    $products = $this->input->post('product');
                    if (!empty($products)) {
                        $this->_create_service_product($products, $this->Service_model->_pk_service);
                    }
                    $data['success'] = true;
                }
            }
            else {
                $data['error'] = true;
            }
        }

        //pega a lista de 
        $this->Service_model->_status = $this->Service_model->_status_active;
        $data['services']             = $this->Service_model->fetch();

        //pega a lista de product_category e product
        $data['products'] = $this->_process_product();


        $data['js_include']   = '
            <script src="' . base_url() . 'web/js/service/create.js"></script>
            ';
        $data['css_include']  = '';
        $data['main_content'] = 'service/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_service') > 0) {
            $this->Service_model->_pk_service = $this->input->post('pk_service');
            $this->Service_model->delete();
        }
        redirect('service/index');
    }

    private function _process_product() {
        $this->load->model("Product_model");
        $this->Product_model->_status = $this->Product_model->_status_active;
        $products                     = $this->Product_model->fetch();

        $return = array();
        if ($products) {
            foreach ($products as $product) {
                $return[$product['category']][$product['fk_product_category']][] = $product;
            }
        }
        return $return;
    }

    private function _create_service_product($products, $pk_service) {
        $this->load->model("Service_product_model");
        //caso de update, inativar todos os serviços e criar novamente
        $this->Service_product_model->_fk_service = $pk_service;
        $this->Service_product_model->delete();


        foreach ($products as $prd) {
            $this->Service_product_model->_fk_product = $prd;
            $this->Service_product_model->_status     = $this->Service_product_model->_status_active;
            $this->Service_product_model->create();
        }
    }

}
