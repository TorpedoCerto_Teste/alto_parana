<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Eer_market extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Eer_market_model');
    }

    function index() {
        $data['list'] = $this->_process_eer_market();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/eer_market/index.js"></script>            
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            ';
        $data['main_content'] = 'eer_market/index';
        $this->load->view('includes/template', $data);
    }

    private function _process_eer_market() {
        $this->Eer_market_model->_year = date("Y");
        if ($this->input->post('filter')) {
            $this->Eer_market_model->_year = $this->input->post('year');
        }
        $this->Eer_market_model->_status = $this->Eer_market_model->_status_active;
        $eer_market_list                 = $this->Eer_market_model->fetch();

        //gera os meses
        $return = array();
        for ($i = 1; $i <= 12; $i++) {
            $return[$i]['pk_eer_market']  = 0;
            $return[$i]['month']          = $i;
            $return[$i]['year']           = $this->Eer_market_model->_year;
            $return[$i]['reporting_date'] = "";
            $return[$i]['payment_date']   = "";
            $return[$i]['value']          = 0;
        }

        if ($eer_market_list) {
            foreach ($eer_market_list as $list) {
                $return[$list['month']]['pk_eer_market']  = $list['pk_eer_market'];
                $return[$list['month']]['reporting_date'] = $list['reporting_date'];
                $return[$list['month']]['payment_date']   = $list['payment_date'];
                $return[$list['month']]['value']          = $list['value'];
            }
        }
        return $return;
    }

    function ajax_upsert() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $read                = false;

        $this->Eer_market_model->_month  = $this->input->post('month');
        $this->Eer_market_model->_year   = $this->input->post('year');
        $this->Eer_market_model->_status = $this->Eer_market_model->_status_active;
        $read                            = $this->Eer_market_model->read();
        if (!$read) {
            $this->Eer_market_model->_created_at = date("Y-m-d H:i:s");
        }

        switch ($this->input->post('field')) {
            case 'reporting_date':
                $this->Eer_market_model->_reporting_date = human_date_to_date($this->input->post('val'), 1);
                break;
            case 'payment_date':
                $this->Eer_market_model->_payment_date   = human_date_to_date($this->input->post('val'), 1);
                break;
            case 'value':
                $this->Eer_market_model->_value          = str_replace(",", ".", $this->input->post('val'));
                break;
        }

        $upsert = $this->Eer_market_model->upsert();
        print_r(json_encode($upsert));
    }

}
