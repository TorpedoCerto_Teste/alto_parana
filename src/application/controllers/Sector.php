<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sector extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Sector_model');
    }

    function index() {
        $this->Sector_model->_status = $this->Sector_model->_status_active;
        $data['list']                = $this->Sector_model->fetch();
        $data['js_include']          = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']         = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']        = 'sector/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('sector', 'sector', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Sector_model->_sector = mb_strtoupper($this->input->post('sector'), 'UTF-8');
                $this->Sector_model->_status = $this->Sector_model->_status_active;
                $create                      = $this->Sector_model->create();
                if ($create) {
                    redirect('sector/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'sector/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('sector/index');
        }
        $this->Sector_model->_pk_sector = $this->uri->segment(3);
        $read                           = $this->Sector_model->read();
        if (!$read) {
            redirect('sector/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('sector', 'sector', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Sector_model->_sector = mb_strtoupper($this->input->post('sector'), 'UTF-8');
                $update                = $this->Sector_model->update();
                redirect('sector/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'sector/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_sector') > 0) {
            $this->Sector_model->_pk_sector = $this->input->post('pk_sector');
            $this->Sector_model->delete();
        }
        redirect('sector/index');
    }

}
