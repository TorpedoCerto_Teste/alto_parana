<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_slip_ccee extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Payment_slip_ccee_model');
    }

    function index() {
        redirect("payment_slip_ccee/upsert");
    }

    function upsert() {
        $this->load->model('Payment_slip_ccee_market_model');
        $data['unities'] = false;
        $data['error']   = false;
        $data['success'] = false;

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Payment_slip_ccee_model->_pk_payment_slip_ccee = $this->Payment_slip_ccee_model->upsert();
                if ($this->Payment_slip_ccee_model->_pk_payment_slip_ccee) {
                    $this->_create_payment_slip_ccee_unity();
                    $uploads = $this->_process_files();
                    //registra o evento no calendario
                    $this->_process_calendar();
                }
                $data['success'] = true;
            }
            else {
                $data['error'] = true;
            }
        }

        if ($this->input->post('upsert')) {
            $this->_process_payment_slip_ccee();
            $data['unities'] = $this->_process_unities();
            $this->_process_payment_slip_ccee_market();

            //pega a lista de contatos e seus grupos 
            if (isset($this->Controller_group_model->_fk_group)) {
                $data['group_contacts'] = $this->_process_group_contact($this->input->post('fk_agent'));
            }

            //pega a lista de notificações enviadas
            $data['email_notifications'] = $this->_fetch_email_notification($this->input->post('year'), $this->input->post('month'), $this->input->post('fk_agent'));
        }


        //pega a lista de agentes
        $data['agents'] = $this->_process_agents();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>    
                <script src="' . base_url() . 'web/js/payment_slip_ccee/upsert.js"></script>';
        $data['css_include']  = '
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'payment_slip_ccee/upsert';
        $this->load->view('includes/template', $data);
    }

    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Payment_slip_ccee_model->_pk_payment_slip_ccee = $this->input->post("pk_payment_slip_ccee_sendmail");
            $payment_slip_ccee                                    = $this->Payment_slip_ccee_model->read();

            if ($payment_slip_ccee) {
                //pega o agente
                $agent = $this->_get_agent($this->Payment_slip_ccee_model->_fk_agent);

                if ($agent) {
                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->Payment_slip_ccee_model->_fk_agent);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($this->Payment_slip_ccee_model->_year, $this->Payment_slip_ccee_model->_month);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($contact_list);
                            }
                        }
                        
                    }
                    
                }
            }
        }
        redirect("payment_slip_ccee/upsert");
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('value', 'value', 'trim|required');
        $this->form_validation->set_rules('validate', 'validate', 'trim|required');
    }

    private function _fill_model() {
        $this->Payment_slip_ccee_model->_pk_payment_slip_ccee = $this->input->post('pk_payment_slip_ccee');
        $this->Payment_slip_ccee_model->_month                = $this->input->post('month');
        $this->Payment_slip_ccee_model->_year                 = $this->input->post('year');
        $this->Payment_slip_ccee_model->_fk_agent             = $this->input->post('fk_agent');
        $this->Payment_slip_ccee_model->_value                = str_replace(",", ".", $this->input->post('value'));
        $this->Payment_slip_ccee_model->_validate             = human_date_to_date($this->input->post('validate'));
        $this->Payment_slip_ccee_model->_status               = $this->Payment_slip_ccee_model->_status_active;
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_fk_agent = $this->Payment_slip_ccee_model->_fk_agent;
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $unities                      = $this->Unity_model->fetch();

        if ($unities) {
            foreach ($unities as $k => $unity) {
                $unities[$k]["checked"] = false;
            }
            $unities = $this->_process_payment_slip_ccee_unity($unities);
        }
        return $unities;
    }

    private function _process_payment_slip_ccee_unity($unities) {
        $this->load->model("Payment_slip_ccee_unity_model");
        $this->Payment_slip_ccee_unity_model->_fk_payment_slip_ccee = $this->Payment_slip_ccee_model->_pk_payment_slip_ccee;
        $this->Payment_slip_ccee_unity_model->_status               = $this->Payment_slip_ccee_unity_model->_status_active;
        $payment_slip_ccee_unity                                    = $this->Payment_slip_ccee_unity_model->fetch();

        if ($payment_slip_ccee_unity) {
            foreach ($unities as $k => $unity) {
                foreach ($payment_slip_ccee_unity as $pu) {
                    if ($unity['pk_unity'] == $pu['fk_unity']) {
                        $unities[$k]["checked"] = true;
                    }
                }
            }
        }
        return $unities;
    }

    private function _process_payment_slip_ccee_market() {
        $this->Payment_slip_ccee_market_model->_month  = $this->Payment_slip_ccee_model->_month;
        $this->Payment_slip_ccee_market_model->_year   = $this->Payment_slip_ccee_model->_year;
        $this->Payment_slip_ccee_market_model->_status = $this->Payment_slip_ccee_market_model->_status_active;
        $this->Payment_slip_ccee_market_model->read();
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/payment_slip_ccee/' . $this->Payment_slip_ccee_model->_pk_payment_slip_ccee;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('payment_slip_file')) {
            $this->Payment_slip_ccee_model->_payment_slip_file = str_replace(" ", "_", $_FILES['payment_slip_file']['name']);
            $this->Payment_slip_ccee_model->update();
        }
    }

    private function _create_payment_slip_ccee_unity() {
        $unities = $this->input->post('chk');
        if ($unities) {
            $this->load->model("Payment_slip_ccee_unity_model");
            $this->Payment_slip_ccee_unity_model->_fk_payment_slip_ccee = $this->Payment_slip_ccee_model->_pk_payment_slip_ccee;
            $this->Payment_slip_ccee_unity_model->delete();

            $this->Payment_slip_ccee_unity_model->_status     = $this->Payment_slip_ccee_unity_model->_status_active;
            $this->Payment_slip_ccee_unity_model->_created_at = date("Y-m-d H:i:s");
            foreach ($unities as $unity) {
                $this->Payment_slip_ccee_unity_model->_fk_unity = $unity;
                $this->Payment_slip_ccee_unity_model->create();
            }
        }
    }

    private function _process_payment_slip_ccee() {
        $this->Payment_slip_ccee_model->_fk_agent = $this->input->post("fk_agent");
        $this->Payment_slip_ccee_model->_month    = $this->input->post("month");
        $this->Payment_slip_ccee_model->_year     = $this->input->post("year");
        $this->Payment_slip_ccee_model->read();
    }


    private function _post_mail($to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Boleto CCEE";
        $this->load->clear_vars();
        $msg = $this->load->view('payment_slip_ccee/email/upsert', array(), true);
        $post['message'] = base64_encode($msg);

        if ($this->Payment_slip_ccee_model->_payment_slip_file != "") {
            $post['files'] =  'uploads/payment_slip_ccee/' . $this->Payment_slip_ccee_model->_pk_payment_slip_ccee . '/' . $this->Payment_slip_ccee_model->_payment_slip_file;
        }

        $pymail = pymail($post);
    }

    private function _process_calendar() {
        $this->load->model("Calendar_model");
        $this->Calendar_model->_calendar                = "BOLETO CCEE";
        $this->Calendar_model->_year_reference          = $this->Payment_slip_ccee_model->_year;
        $this->Calendar_model->_month_reference         = $this->Payment_slip_ccee_model->_month;
        $this->Calendar_model->_fk_calendar_responsible = 1; //pk_calendar_responsible para CLIENTE
        $this->Calendar_model->_event_date              = $this->Payment_slip_ccee_model->_validate;
        $this->Calendar_model->_fk_agent                = $this->Payment_slip_ccee_model->_fk_agent;
        $this->Calendar_model->_created_at              = date("Y-m-d H:i:s");
        $this->Calendar_model->create();
    }

}
