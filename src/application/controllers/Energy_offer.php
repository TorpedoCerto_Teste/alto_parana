<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Energy_offer extends Group_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Energy_offer_model');
    }


    function index()
    {
        $data['list'] = $this->process_energy_offer();

        $data['js_include']           = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/energy_offer/index.js?'. microtime().'"></script>
            ';
        $data['css_include']          = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']         = 'energy_offer/index';
        $this->load->view('includes/template', $data);
    }


    function create()
    {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();

                $this->Energy_offer_model->_pk_energy_offer = $this->Energy_offer_model->create();
                if ($this->Energy_offer_model->_pk_energy_offer) {
                    $this->process_year_price($this->Energy_offer_model->_pk_energy_offer);
                }
            }
            else {
                $data['error'] = true;
            }
            redirect('energy_offer/index');
        }

        //Energy type
        $this->load->model("Energy_type_model");
        $this->Energy_type_model->_status = $this->Energy_type_model->_status_active;
        $data["energy_types"] = $this->Energy_type_model->fetch();

        //Submarket
        $this->load->model("Submarket_model");
        $this->Submarket_model->_status = $this->Submarket_model->_status_active;
        $data["submarkets"] = $this->Submarket_model->fetch();

        //Agent
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $data["agents"] = $this->Agent_model->fetch();  
        
        $data['js_include']   = '
            <script src="' . base_url() . 'js/vue.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'web/js/energy_offer/create.js?'. microtime().'"></script>
        ';
        $data['css_include']      = '
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >        
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
        '; 

        $data['main_content'] = 'energy_offer/create';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_energy_offer') > 0) {
            $this->Energy_offer_model->_pk_energy_offer = $this->input->post('pk_energy_offer');
            $this->Energy_offer_model->delete();
        }
        redirect('energy_offer/index');
    }   
    
    function export_csv() {
        $list = $this->process_energy_offer();
        if ($list) {
            //available years:
            $available_years = []; 
            foreach ($list as $i => $data) {
                foreach ($data['energy_offer_price'] as $price) {
                    $available_years[$price['year']] = 0;
                }
            }
            ksort($available_years);

            foreach ($list as $i => $data) {
                $list[$i]['years'] = $available_years;
                foreach ($data['energy_offer_price'] as $price) {
                    if (isset($list[$i]['years'][$price['year']])) {
                        $list[$i]['years'][$price['year']] = $price['price'];
                    }
                }
            }
            $filename = 'data.csv';
            $this->send_csv_headers($filename);
    
            $csv_data = $this->array_to_csv($list, $available_years);
            echo $csv_data;        
        }


        // redirect('energy_offer/index');
    }

    private function array_to_csv($array, $available_years) {
        ob_start();
        $file = fopen('php://output', 'w');
        $title = ['Data', 'Tipo de Energia', 'Submercado', 'Agente'];
        foreach ($available_years as $k => $v) {
            $title[] = $k;
        }
        fputcsv($file, $title);

        foreach ($array as $row) {
            $data = [
                $row['offer_date'],
                $row['energy_type_name'],
                $row['submarket'],
                $row['agent_community_name']
            ];
            foreach ($row['years'] as $k => $v) {
                $data[] = "R$ ".number_format($v, 2, ',', '.');
            }
            fputcsv($file, $data);
        }
        fclose($file);
        return ob_get_clean();
    }    

    private function send_csv_headers($filename) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename);
    }    

    private function process_energy_offer() {
        $return = [];
        $this->Energy_offer_model->_status = $this->Energy_offer_model->_status_active;
        $energy_offers = $this->Energy_offer_model->fetch();
        if ($energy_offers) {
            foreach ($energy_offers as $i => $energy_offer) {
                $return[$energy_offer['pk_energy_offer']] = [
                    "pk_energy_offer" => $energy_offer['pk_energy_offer'],
                    "fk_energy_type" => $energy_offer['fk_energy_type'],
                    "fk_submarket" => $energy_offer['fk_submarket'],
                    "set_origin" => $energy_offer['set_origin'],
                    "fk_agent" => $energy_offer['fk_agent'],
                    "submarket" => $energy_offer['submarket'],
                    "created_at" => $energy_offer['created_at'],
                    "offer_date" => $energy_offer['offer_date'],
                    "energy_type_name" => $energy_offer['energy_type_name'],
                    "agent_community_name" => $energy_offer['agent_community_name'],
                    "energy_offer_price" => []
                ];
            }

            foreach ($energy_offers as $i => $energy_offer) {
                $return[$energy_offer['pk_energy_offer']]['energy_offer_price'][$i] = [
                    "year" => $energy_offer['year'],
                    "price" => $energy_offer['price']
                ];
            }
        }
        return $return;
    }

    private function process_year_price($fk_energy_offer) {
        $this->load->model("Energy_offer_year_model");
        $data = $this->input->post();
        if (isset($data['year']) && isset($data['value'])) {
            foreach ($data['year'] as $i => $y) {
                $value = number_format(str_replace(",", ".", (str_replace(".", "", $data['value'][$i]))), 2, ".", "");
                if ($value > 0) {
                    $this->Energy_offer_year_model->_fk_energy_offer = $fk_energy_offer;
                    $this->Energy_offer_year_model->_year = $y;
                    $this->Energy_offer_year_model->_price = $value;
                    $this->Energy_offer_year_model->_status = $this->Energy_offer_year_model->_status_active;
                    $this->Energy_offer_year_model->create();
                }                
            }
        }
    }

    private function _validate_form()
    {
        $this->form_validation->set_rules('offer_date', 'offer_date', 'trim|required');
        $this->form_validation->set_rules('fk_energy_type', 'fk_energy_type', 'trim|required');
        $this->form_validation->set_rules('fk_submarket', 'fk_submarket', 'trim|required');
    }

    private function _fill_model()
    {
        $this->Energy_offer_model->_offer_date      = human_date_to_date($this->input->post('offer_date'));
        $this->Energy_offer_model->_fk_energy_type  = $this->input->post('fk_energy_type');
        $this->Energy_offer_model->_fk_submarket    = $this->input->post('fk_submarket');
        $this->Energy_offer_model->_set_origin      = $this->input->post('set_origin') ? true : false;

        if ($this->Energy_offer_model->_set_origin) {
            $this->Energy_offer_model->_fk_agent        = $this->input->post('fk_agent');
        }
        $this->Energy_offer_model->_status          = $this->Energy_offer_model->_status_active;
    }

}
