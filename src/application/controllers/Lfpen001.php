<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lfpen001 extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Lfpen001_model');
    }

    function index() {
        redirect('lfpen001/upsert');
        $this->Lfpen001_model->_status = $this->Lfpen001_model->_status_active;
        $data['list']                  = $this->Lfpen001_model->fetch();
        $data['js_include']            = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']           = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']          = 'lfpen001/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Lfpen001_model->_created_at = date("Y-m-d H:i:s");
                $create                            = $this->Lfpen001_model->create();
                if ($create) {
                    redirect('lfpen001/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'lfpen001/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('lfpen001/index');
        }
        $this->Lfpen001_model->_pk_lfpen001 = $this->uri->segment(3);
        $read                               = $this->Lfpen001_model->read();
        if (!$read) {
            redirect('lfpen001/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Lfpen001_model->update();
                redirect('lfpen001/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'lfpen001/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error']   = false;
        $data['success'] = false;

        $this->load->model("Lfpen001_market_model");

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Lfpen001_model->_pk_lfpen001 = $this->Lfpen001_model->upsert();
                if ($this->Lfpen001_model->_pk_lfpen001) {
                    $uploads = $this->_process_files();
                }
                $data['success'] = true;
            }
            else {
                $data['error'] = true;
            }
        }
        elseif ($this->input->post('upsert') == 'false') {
            $this->Lfpen001_model->_fk_agent = $this->input->post("fk_agent");
            $this->Lfpen001_model->_month    = $this->input->post("month");
            $this->Lfpen001_model->_year     = $this->input->post("year");
            $this->Lfpen001_model->read();

            $this->_process_lfpen001_market();
            
            
        }

        //pega a lista de notificações enviadas
        $data['email_notifications'] = $this->_fetch_email_notification($this->input->post('year'), $this->input->post('month'), $this->input->post('fk_agent'));
        
        //pega a lista de agentes
        $data['agents'] = $this->_process_agents_group();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>          
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>    
                <script src="' . base_url() . 'web/js/lfpen001/upsert.js?'. microtime().'"></script>';
        $data['css_include']  = '
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'lfpen001/upsert';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_lfpen001') > 0) {
            $this->Lfpen001_model->_pk_lfpen001 = $this->input->post('pk_lfpen001');
            $this->Lfpen001_model->delete();
        }
        redirect('lfpen001/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('total', 'total', 'trim');
        $this->form_validation->set_rules('payment_date', 'payment_date', 'trim');
        $this->form_validation->set_rules('penalty_not_apply', 'penalty_not_apply', 'trim');
    }

    private function _fill_model() {
        $this->Lfpen001_model->_pk_lfpen001       = preg_replace("([^\d]*)", "", $this->input->post('pk_lfpen001'));
        $this->Lfpen001_model->_month             = preg_replace("([^\d]*)", "", $this->input->post('month'));
        $this->Lfpen001_model->_year              = preg_replace("([^\d]*)", "", $this->input->post('year'));
        $this->Lfpen001_model->_fk_agent          = preg_replace("([^\d]*)", "", $this->input->post('fk_agent'));
        $this->Lfpen001_model->_total             = str_replace(",", ".", $this->input->post('total'));
        $this->Lfpen001_model->_payment_date      = human_date_to_date($this->input->post('payment_date'));
        $this->Lfpen001_model->_penalty_not_apply = $this->input->post('penalty_not_apply') ? 1 : 0;
        $this->Lfpen001_model->_status            = $this->Lfpen001_model->_status_active;
        $this->Lfpen001_model->_created_at        = date("Y-m-d H:i:s");
    }

    private function _process_lfpen001_market() {
        $this->Lfpen001_market_model->_month  = $this->Lfpen001_model->_month;
        $this->Lfpen001_market_model->_year   = $this->Lfpen001_model->_year;
        $this->Lfpen001_market_model->_status = $this->Lfpen001_market_model->_status_active;
        $this->Lfpen001_market_model->read();
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/lfpen001/' . $this->Lfpen001_model->_pk_lfpen001;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload('lfpen001_file')) {
            $this->Lfpen001_model->_lfpen001_file = $_FILES['lfpen001_file']['name'];
            $this->Lfpen001_model->update();
        }
    }
    
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Lfpen001_model->_pk_lfpen001 = $this->input->post("pk_lfpen001_sendmail");
            $lfpen001                                    = $this->Lfpen001_model->read();
            if ($lfpen001) {
                //pega o agente
                $agent = $this->_get_agent($this->Lfpen001_model->_fk_agent);

                if ($agent) {
                    
                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->Lfpen001_model->_fk_agent);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($this->Lfpen001_model->_year, $this->Lfpen001_model->_month);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($contact_list);
                            }

                        }
                        
                    }
                    
                }
            }
        }
        redirect("lfpen001/upsert");
    }

    private function _post_mail($to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Liquidação de Penalidades  - LFPEN001";
        $msg = $this->load->view('lfpen001/email/upsert', array(), true);
        $post['message'] = base64_encode($msg);

        if ($this->Lfpen001_model->_lfpen001_file != "") {
            $post['files'] =  'uploads/lfpen001/' . $this->Lfpen001_model->_pk_lfpen001 . '/' . $this->Lfpen001_model->_lfpen001_file;
        }

        $pymail = pymail($post);
    }
        

}
