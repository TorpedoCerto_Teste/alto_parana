<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_energy extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ccvee_energy_model');
        $this->load->model('Ccvee_general_model');
    }

    function upsert_energy() {
        $this->Ccvee_energy_model->_fk_ccvee_general = $this->uri->segment(3);
        $this->Ccvee_energy_model->read();

        if ($this->input->post('update_energy') == 'true') {

            //tabela ccvee_energy
            $this->Ccvee_energy_model->_supply_start                 = human_date_to_date($this->input->post("supply_start"));
            $this->Ccvee_energy_model->_supply_end                   = human_date_to_date($this->input->post("supply_end"));
            $this->Ccvee_energy_model->_losses_type                  = $this->input->post("losses_type");
            $this->Ccvee_energy_model->_losses                       = $this->input->post("losses_unique") != "" ? str_replace(",", ".", $this->input->post("losses_unique")) : 0;
            $this->Ccvee_energy_model->_differentiated_volume_period = $this->input->post("differentiated_volume_period") == 1 ? 1 : 0;
            $this->Ccvee_energy_model->_unit_measurement             = $this->input->post("unit_measurement");
            $this->Ccvee_energy_model->_discount_proinfa             = $this->input->post("discount_proinfa") == 1 ? 1 : 0;
            $this->Ccvee_energy_model->_seasonal                     = $this->input->post("seasonality") == 1 ? 1 : 0;
            $this->Ccvee_energy_model->_seasonal_inferior            = $this->input->post("seasonal_inferior") < 0 ? $this->input->post("seasonal_inferior") : 0;
            $this->Ccvee_energy_model->_seasonal_superior            = $this->input->post("seasonal_superior") > 0 ? $this->input->post("seasonal_superior") : 0;
            $this->Ccvee_energy_model->_flexibility                  = $this->input->post("flexibility") == 1 ? 1 : 0;
            $this->Ccvee_energy_model->_flexibility_inferior         = $this->input->post("flexibility_inferior") < 0 ? $this->input->post("flexibility_inferior") : 0;
            $this->Ccvee_energy_model->_flexibility_superior         = $this->input->post("flexibility_superior") > 0 ? $this->input->post("flexibility_superior") : 0;
            $this->Ccvee_energy_model->_modulation                   = mb_strtolower($this->input->post('modulation_radio'), 'UTF-8');
            $this->Ccvee_energy_model->_modulation_heavy_inferior    = $this->input->post("modulation_heavy_inferior") < 0 ? $this->input->post("modulation_heavy_inferior") : 0;
            $this->Ccvee_energy_model->_modulation_medium_inferior   = $this->input->post("modulation_medium_inferior") < 0 ? $this->input->post("modulation_medium_inferior") : 0;
            $this->Ccvee_energy_model->_modulation_light_inferior    = $this->input->post("modulation_light_inferior") < 0 ? $this->input->post("modulation_light_inferior") : 0;
            $this->Ccvee_energy_model->_modulation_heavy_superior    = $this->input->post("modulation_heavy_superior") > 0 ? $this->input->post("modulation_heavy_superior") : 0;
            $this->Ccvee_energy_model->_modulation_medium_superior   = $this->input->post("modulation_medium_superior") > 0 ? $this->input->post("modulation_medium_superior") : 0;
            $this->Ccvee_energy_model->_modulation_light_superior    = $this->input->post("modulation_light_superior") > 0 ? $this->input->post("modulation_light_superior") : 0;
            $this->Ccvee_energy_model->_modulation_type_input        = mb_strtolower($this->input->post('modulation_type_input'), 'UTF-8');
            $upsert                                                  = $this->Ccvee_energy_model->upsert();

            //processa os volumes diferenciados por período
            //inativar os registros no bulk já inseridos
            $this->load->model("Ccvee_energy_bulk_model");
            $this->Ccvee_energy_bulk_model->_fk_ccvee_energy = $upsert;
            $this->Ccvee_energy_bulk_model->delete();
            $this->Ccvee_energy_bulk_model->_status          = $this->Ccvee_energy_bulk_model->_status_active;

            //inserir os novos registros
            $volumes = $this->input->post("volume");
            if ($volumes) {
                foreach ($volumes as $k => $vol) {
                    if ($this->Ccvee_energy_model->_differentiated_volume_period == 1) {
                        $this->Ccvee_energy_bulk_model->_date = join("-", array_reverse(explode("/", $vol['period']))) . "-01";
                    }
                    else {
                        $this->Ccvee_energy_bulk_model->_date = $this->Ccvee_energy_model->_supply_start;
                    }
                    $this->Ccvee_energy_bulk_model->_seasonal = number_format(str_replace(",", ".", $vol['seasonal']), 6, ".", "");
                    $this->Ccvee_energy_bulk_model->_forecast = number_format(str_replace(",", ".", $vol['forecast']), 6, ".", "");
                    $this->Ccvee_energy_bulk_model->_adjusted = number_format(str_replace(",", ".", $vol['adjusted']), 6, ".", "");
                    if (isset($vol['losses'])) {
                        $this->Ccvee_energy_bulk_model->_losses = str_replace(",", ".", $vol['losses']);
                    }
                    $this->Ccvee_energy_bulk_model->_attendance = number_format(str_replace(",", ".", $vol['attendance']), 2, ".", "");
                    $this->Ccvee_energy_bulk_model->create();
                }
            }
            else {
                $this->Ccvee_energy_bulk_model->_date = $this->Ccvee_energy_model->_supply_start;
                $this->Ccvee_energy_bulk_model->_seasonal = number_format(str_replace(",", ".", $this->input->post('seasonal')), 6, ".", "");
                $this->Ccvee_energy_bulk_model->_forecast = number_format(str_replace(",", ".", $this->input->post('forecast')), 6, ".", "");
                $this->Ccvee_energy_bulk_model->_adjusted = number_format(str_replace(",", ".", $this->input->post('adjusted')), 6, ".", "");
                $this->Ccvee_energy_bulk_model->_attendance = number_format(str_replace(",", ".", $this->input->post('attendance')), 2, ".", "");
                $this->Ccvee_energy_bulk_model->create();
            }


            //processa os limites seasonais
            //inativar os registros antigos
            $this->load->model("Ccvee_seasonal_limit_model");
            $this->Ccvee_seasonal_limit_model->_fk_ccvee_energy = $upsert;
            $this->Ccvee_seasonal_limit_model->delete();

            //inserir os registros novos
            if ($this->Ccvee_energy_model->_seasonal == 1) {
                $seasonal_limit                            = $this->input->post("seasonal_limit");
                $this->Ccvee_seasonal_limit_model->_status = $this->Ccvee_seasonal_limit_model->_status_active;

                if (!empty($seasonal_limit)) {
                    foreach ($seasonal_limit as $s) {
                        $this->Ccvee_seasonal_limit_model->_year  = $s['year'];
                        $this->Ccvee_seasonal_limit_model->_limit = human_date_to_date($s['limit']);
                        $this->Ccvee_seasonal_limit_model->create();
                    }
                }
            }

            redirect('ccvee_general/view/' . $upsert . '/energia');
        }
    }

    function ajax_proccess_supply() {
        $start_date      = human_date_to_date($this->input->post('supply_start'));
        $end_date        = human_date_to_date($this->input->post('supply_end'));
        $pk_ccvee_energy = $this->input->post('pk_ccvee_energy');
        $supply          = $this->Ccvee_energy_model->_proccess_supply($start_date, $end_date, $pk_ccvee_energy);
        print_r(json_encode($supply));
    }

    function ajax_proccess_seasonal() {
        $start_date      = human_date_to_date($this->input->post('supply_start'));
        $end_date        = human_date_to_date($this->input->post('supply_end'));
        $pk_ccvee_energy = $this->input->post('pk_ccvee_energy');
        $return          = $this->Ccvee_energy_model->_proccess_seasonal($start_date, $end_date, $pk_ccvee_energy);
        print_r(json_encode($return));
    }

    function ajax_proccess_unit_measurement() {
        $return       = array();
        $supply_start = new DateTime(human_date_to_date($this->input->post('supply_start')));
        $supply_end   = new DateTime(human_date_to_date($this->input->post('supply_end')));

        $total_months = ($supply_start->diff($supply_end)->m + ($supply_start->diff($supply_end)->y * 12)) + 1;

        $mes_inicial = human_date_to_date($this->input->post('supply_start'));
        if ($total_months > 1) {
            for ($i = 0; $i < $total_months; $i++) {
                $return[$i]['period'] = date("m/Y", strtotime($mes_inicial . " +" . $i . " MONTHS"));
                $return[$i]['total']  = hours_of_month(date("d/m/Y", strtotime($mes_inicial . " +" . $i . " MONTHS")));
            }
        }
        else {
            $return[0]['period'] = date("m/Y", strtotime($mes_inicial));
            $return[0]['total']  = hours_of_month(date("d/m/Y", strtotime($mes_inicial)));
        }
        print_r(json_encode($return));
    }

}
