<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Profile_model');
    }

    function index() {
        $this->load->model('Agent_model');
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        if ((int) $this->Agent_model->_pk_agent <= 0) {
            redirect('agent/index');
        }
        $agent = $this->Agent_model->read();
        if (!$agent) {
            redirect('agent/index');
        }

        $this->Profile_model->_status   = $this->Profile_model->_status_active;
        $this->Profile_model->_fk_agent = $this->Agent_model->_pk_agent;
        $data['list']                   = $this->Profile_model->fetch();
        $data['js_include']             = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']            = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']           = 'profile/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $this->load->model('Agent_model');
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        if ((int) $this->Agent_model->_pk_agent <= 0) {
            redirect('agent/index');
        }
        $agent = $this->Agent_model->read();
        if (!$agent) {
            redirect('agent/index');
        }

        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Profile_model->create();
                if ($create) {
                    redirect('agent/view/' . $this->Agent_model->_pk_agent . '/perfis');
                }
            }
            $data['error'] = true;
        }
        $this->load->model('Submarket_model');
        $this->Submarket_model->_status = $this->Submarket_model->_status_active;
        $data['submarkets']             = $this->Submarket_model->fetch();

        $this->load->model('Standard_model');
        $this->Standard_model->_status = $this->Standard_model->_status_active;
        $data['standards']             = $this->Standard_model->fetch();

        $this->load->model('Category_model');
        $this->Category_model->_status = $this->Category_model->_status_active;
        $data['categories']            = $this->Category_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'profile/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $this->load->model('Agent_model');
        $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
        if ((int) $this->Agent_model->_pk_agent <= 0) {
            redirect('agent/index');
        }
        $agent = $this->Agent_model->read();
        if (!$agent) {
            redirect('agent/index');
        }

        $this->Profile_model->_pk_profile = $this->uri->segment(4, 0);
        if ((int) $this->Profile_model->_pk_profile <= 0) {
            redirect('agent/index');
        }
        $this->Profile_model->_fk_agent = $this->Agent_model->_pk_agent;
        $read                           = $this->Profile_model->read();
        if (!$read) {
            redirect('profile/index');
        }

        $data['error'] = false;

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Profile_model->update();
                redirect('agent/view/' . $this->Agent_model->_pk_agent . '/perfis');
            }
            $data['error'] = true;
        }
        $this->load->model('Submarket_model');
        $this->Submarket_model->_status = $this->Submarket_model->_status_active;
        $data['submarkets']             = $this->Submarket_model->fetch();

        $this->load->model('Standard_model');
        $this->Standard_model->_status = $this->Standard_model->_status_active;
        $data['standards']             = $this->Standard_model->fetch();

        $this->load->model('Category_model');
        $this->Category_model->_status = $this->Category_model->_status_active;
        $data['categories']            = $this->Category_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'profile/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {       
        if ($this->input->post('pk_profile') > 0) {
            $this->Profile_model->_pk_profile = $this->input->post('pk_profile');            
            $this->Profile_model->delete();
        }
        redirect('agent/view/'.$this->uri->segment(3).'/perfis');
    }

    function fetch_by_fk_agent() {
        $this->disableLayout            = TRUE;
        $this->output->set_content_type('application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        $this->Profile_model->_fk_agent = $this->uri->segment(3, 0);
        $this->Profile_model->_status   = $this->Profile_model->_status_active;
        $profiles                       = $this->Profile_model->fetch();
        print json_encode($profiles);
    }

    function status() {
        if ($this->uri->segment(3, 0) <= 0 || $this->uri->segment(4, 0) <= 0) {
            redirect('agent/index');
        }
        $this->Profile_model->_fk_agent   = $this->uri->segment(3);
        $this->Profile_model->_pk_profile = $this->uri->segment(4);
        $read                             = $this->Profile_model->read();
        if (!$read) {
            redirect('agent/index');
        }
        $this->Profile_model->_status = $this->Profile_model->_status == $this->Profile_model->_status_active ? $this->Profile_model->_status_inactive : $this->Profile_model->_status_active;
        $this->Profile_model->update();
        redirect('agent/view/' . $this->Profile_model->_fk_agent . '/perfis');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_submarket', 'Submercado', 'trim|required');
        $this->form_validation->set_rules('fk_standard', 'Classe', 'trim|required');
        $this->form_validation->set_rules('fk_category', 'Categoria', 'trim|required');
        $this->form_validation->set_rules('profile', 'Perfil', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
    }

    private function _fill_model() {
        $this->Profile_model->_fk_submarket = $this->input->post('fk_submarket');
        $this->Profile_model->_fk_agent     = $this->Agent_model->_pk_agent;
        $this->Profile_model->_fk_standard  = $this->input->post('fk_standard');
        $this->Profile_model->_fk_category  = $this->input->post('fk_category');
        $this->Profile_model->_profile      = mb_strtoupper($this->input->post('profile'), 'UTF-8');
        $this->Profile_model->_code         = preg_replace('/([^\d]*)/', '', $this->input->post('code'));
        $this->Profile_model->_status       = $this->Profile_model->_status_active;
    }

}
