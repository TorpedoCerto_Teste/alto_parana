<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tusd extends Group_Controller {

    var $_upload_temp_dir = './uploads/tusd/temp/';

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Tusd_model');
    }

    function index() {
        redirect("tusd/upsert");
    }

    function index_inactive() {
        $data['list']              = array();
        $this->Tusd_model->_status = $this->Tusd_model->_status_active;
        if ($this->input->post('filter') == 'true') {

            $this->Tusd_model->_month = $this->input->post('month');
            $this->Tusd_model->_year  = $this->input->post('year') != "" ? $this->input->post('year') : date("Y");

        }

        //listagem de agentes para preencher os filtros
        $this->load->model("Agent_model");
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $data['agents']                    = $this->Agent_model->fetch();



        $data['list']         = $this->Tusd_model->fetch();
        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/data-table-init.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">            ';
        $data['main_content'] = 'tusd/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $this->Tusd_model->_status = $this->Tusd_model->_status_active;
        $data['list']              = $this->Tusd_model->fetch();
        $data['error']             = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $pk_tusd = $this->Tusd_model->create();
                if ($pk_tusd) {
                    //mover arquivos da pasta TEMP para a pasta do 
                    $this->_move_temp_files_to_tusd($pk_tusd);
                    redirect('tusd/index');
                }
            }
            $data['error'] = true;
        }

        //pega a lista de unidades, separado por agentes
        $data['units2'] = $this->_process_tusd_unity();

        //verificar se há arquivos temporários não salvos
        $data['temp_files'] = $this->_process_temporary_files();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/spinner-init.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'js/dropzone.js"></script>    
                <script src="' . base_url() . 'web/js/tusd/create.js"></script>
            ';
        $data['css_include']  = '<link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
                <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />    
                <link href="' . base_url() . 'css/dropzone.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'tusd/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $this->Tusd_model->_status = $this->Tusd_model->_status_active;
        $data['list']              = $this->Tusd_model->fetch();
        $data['error']             = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('tusd/index');
        }
        $this->Tusd_model->_pk_tusd = $this->uri->segment(3);
        $read                       = $this->Tusd_model->read();
        if (!$read) {
            redirect('tusd/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->Tusd_model->_fk_unity                = $this->input->post('fk_unity');
                $this->Tusd_model->_month                   = preg_replace("/([^\d]*)/", "", $this->input->post('month'));
                $this->Tusd_model->_year                    = preg_replace("/([^\d]*)/", "", $this->input->post('year'));
                $this->Tusd_model->_expiration_date         = human_date_to_date($this->input->post('expiration_date'));
                $this->Tusd_model->_demand_end              = preg_replace("/([^\d]*)/", "", $this->input->post('demand_end'));
                $this->Tusd_model->_demand_tip_out          = preg_replace("/([^\d]*)/", "", $this->input->post('demand_tip_out'));
                $this->Tusd_model->_consumption_end         = preg_replace("/([^\d]*)/", "", $this->input->post('consumption_end'));
                $this->Tusd_model->_consumption_tip_out     = preg_replace("/([^\d]*)/", "", $this->input->post('consumption_tip_out'));
                $this->Tusd_model->_reactive_energy_end     = preg_replace("/([^\d]*)/", "", $this->input->post('reactive_energy_end'));
                $this->Tusd_model->_reactive_energi_tip_out = preg_replace("/([^\d]*)/", "", $this->input->post('reactive_energi_tip_out'));
                $this->Tusd_model->_connection_charge       = preg_replace("/([^\d]*)/", "", $this->input->post('connection_charge'));
                $this->Tusd_model->_adjustment              = preg_replace("/([^\d]*)/", "", $this->input->post('adjustment'));
                $this->Tusd_model->_discount                = preg_replace("/([^\d]*)/", "", $this->input->post('discount'));
                $this->Tusd_model->_street_lighting         = preg_replace("/([^\d]*)/", "", $this->input->post('street_lighting'));
                $update                                     = $this->Tusd_model->update();
                redirect('tusd/index');
            }
            $data['error'] = true;
        }


        //pega a lista de unidades, separado por agentes
        $data['units2'] = $this->_process_tusd_unity();

        //<fim unidades>

        $data['js_include']   = '
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/spinner-init.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'web/js/tusd/create.js"></script>
            ';
        $data['css_include']  = '<link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
                <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />    
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'tusd/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['unities'] = false;
        $data['error']   = false;
        $data['success'] = false;

        if ($this->input->post('upsert') == 'true') {

            $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
            $this->form_validation->set_rules('month', 'month', 'trim|required');
            $this->form_validation->set_rules('year', 'year', 'trim|required');
            $this->form_validation->set_rules('value', 'value', 'trim|required');
            $this->form_validation->set_rules('icms', 'icms', 'trim|required');
            $this->form_validation->set_rules('piscofins', 'piscofins', 'trim|required');
            $this->form_validation->set_rules('expiration_date', 'expiration_date', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Tusd_model->_pk_tusd         = $this->input->post('pk_tusd');
                $this->Tusd_model->_fk_agent        = $this->input->post('fk_agent');
                $this->Tusd_model->_fk_unity        = $this->input->post('fk_unity');
                $this->Tusd_model->_expiration_date = human_date_to_date($this->input->post('expiration_date'));
                $this->Tusd_model->_month           = preg_replace("/([^\d]*)/", "", $this->input->post('month'));
                $this->Tusd_model->_year            = preg_replace("/([^\d]*)/", "", $this->input->post('year'));
                $this->Tusd_model->_value           = str_replace(",", ".", $this->input->post('value'));
                $this->Tusd_model->_icms            = str_replace(",", ".", $this->input->post('icms'));
                $this->Tusd_model->_piscofins       = str_replace(",", ".", $this->input->post('piscofins'));
                $this->Tusd_model->_status          = $this->Tusd_model->_status_active;
                $this->Tusd_model->_created_at      = date("Y-m-d H:i:s");
                $this->Tusd_model->_pk_tusd         = $this->Tusd_model->upsert();

                if ($this->Tusd_model->_pk_tusd) {
                    $uploads = $this->_process_files();
                    //Criar evento no calendário
                    $this->create_calendar_event();
                }

                $data['success'] = true;
            } else {
                $data['error'] = true;
            }
        } elseif ($this->input->post('upsert') == 'false') {
            $this->Tusd_model->_fk_agent = $this->input->post('fk_agent');
            $this->Tusd_model->_fk_unity = $this->input->post('fk_unity');
            $this->Tusd_model->_month    = preg_replace("/([^\d]*)/", "", $this->input->post('month'));
            $this->Tusd_model->_year     = preg_replace("/([^\d]*)/", "", $this->input->post('year'));
            $this->Tusd_model->_status   = $this->Tusd_model->_status_active;
            $read                        = $this->Tusd_model->read();

            $this->load->model("Unity_model");
            $this->Unity_model->_fk_agent = $this->Tusd_model->_fk_agent;
            $this->Unity_model->_status   = $this->Unity_model->_status_active;
            $data['unities']              = $this->Unity_model->fetch();
        }

        //pega a lista de agentes
        $data['agents'] = $this->_process_agents_group();

        //pega a lista de notificações enviadas
        $data['email_notifications'] = $this->_fetch_email_notification($this->Tusd_model->_year, $this->Tusd_model->_month, $this->Tusd_model->_fk_agent);


        $data['js_include']  = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>        
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>    
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'web/js/tusd/upsert.js?' . microtime() . '"></script>
            ';
        $data['css_include'] = '
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';

        $data['main_content'] = 'tusd/upsert';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_tusd') > 0) {
            $this->Tusd_model->_pk_tusd = $this->input->post('pk_tusd');
            $this->Tusd_model->delete();
        }
        redirect('tusd/index');
    }

    private function _process_tusd_unity() {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();

        $option = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_company_name']][$unity['pk_unity']] = $unity;
            }
        }

        return $option;
    }

    private function _clean_units($units, $list_units) {
        foreach ($units as $k => $unit) {
            foreach ($list_units as $list_unit) {
                if ($unit['pk_unity'] == $list_unit['fk_unity']) {
                    unset($units[$k]);
                }
            }
        }
        return $units;
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('expiration_date', 'expiration_date', 'trim|required');
        $this->form_validation->set_rules('value', 'value', 'trim|required');
        $this->form_validation->set_rules('free_captive', 'free_captive', 'trim|required');
    }

    private function _fill_model() {
        $this->Tusd_model->_fk_unity        = $this->input->post('fk_unity');
        $this->Tusd_model->_month           = preg_replace("/([^\d]*)/", "", $this->input->post('month'));
        $this->Tusd_model->_year            = preg_replace("/([^\d]*)/", "", $this->input->post('year'));
        $this->Tusd_model->_expiration_date = human_date_to_date($this->input->post('expiration_date'));

        $this->Tusd_model->_status          = $this->Tusd_model->_status_active;
        $this->Tusd_model->_value           = str_replace(",", ".", $this->input->post('value'));
        $this->Tusd_model->_free_captive    = $this->input->post('free_captive');
    }

    /**
     * Varre pelo diretório temporário de arquivos feitos o upload e verifica se
     *  há arquivos que não foram transportados para os TUSD respectivo
     */
    private function _process_temporary_files() {
        $this->load->helper('directory');
        return directory_map($this->_upload_temp_dir);
    }

    private function _move_temp_files_to_tusd($pk_tusd) {
        $temp_files = $this->_process_temporary_files();
        if (!empty($temp_files)) {
            $tusd_dir = './uploads/tusd/' . $pk_tusd;
            check_dir_exists($tusd_dir);
            foreach ($temp_files as $file) {
                directory_copy($this->_upload_temp_dir . $file, $tusd_dir . '/' . $file, 1);
            }
        }
    }

    //dropzone (arrastar e soltar arquivos)
    function dropzone() {
        $config['upload_path']   = $this->_upload_temp_dir;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $return = array(
                'error'   => true,
                'message' => strip_tags($this->upload->display_errors()));
        }
        else {
            $return = array(
                'error'   => false,
                'message' => $this->upload->data());
        }
        print json_encode($return);
    }

    function ajax_delete_file() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        unlink($this->_upload_temp_dir . $this->input->post("file_name"));
        print(json_encode(count($this->_process_temporary_files())));
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/tusd/' . $this->Tusd_model->_pk_tusd;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('tusd_file')) {
            $this->Tusd_model->_tusd_file = str_replace(" ", "_", $_FILES['tusd_file']['name']);
            $this->Tusd_model->update();
        }
    }
    
    function create_calendar_event() {
        $this->load->model('Calendar_model');
        $this->Calendar_model->_calendar                = "Venc. Fatura Distr. (TUSD)";
        $this->Calendar_model->_year_reference          = $this->Tusd_model->_year;
        $this->Calendar_model->_month_reference         = $this->Tusd_model->_month;
        $this->Calendar_model->_fk_calendar_responsible = 1; //CLIENTE
        $this->Calendar_model->_event_date              = $this->Tusd_model->_expiration_date;
        $this->Calendar_model->_fk_agent                = $this->Tusd_model->_fk_agent;
        $this->Calendar_model->_status                  = $this->Calendar_model->_status_active;
        $this->Calendar_model->_created_at              = date("Y-m-d H:i:s");
        return $this->Calendar_model->create();
    }

    function delete_file() {
        if ($this->input->post("delete") == "true") {
            $this->Tusd_model->_pk_tusd = $this->input->post("file_pk_tusd");
            $this->Tusd_model->delete_file();
        }
        redirect('tusd/upsert');
    }
        
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Tusd_model->_pk_tusd    = $this->input->post("pk_tusd_sendmail");
            $tusd                       = $this->Tusd_model->read();

            if ($tusd) {
                //pega o agente
                $agent = $this->_get_agent($this->Tusd_model->_fk_agent);
                $unity = $this->_get_unity($this->Tusd_model->_fk_unity);

                if ($agent && $unity) {
                    $tusd = json_decode(json_encode($this->Tusd_model), true);
                    $tusd['agente'] = json_decode(json_encode($this->Agent_model), true);
                    $tusd['unidade'] = json_decode(json_encode($this->Unity_model), true);

                    $tusd['agente_distribuidor'] = [];
                    if ($tusd['unidade']['_fk_agent_power_distributor'] > 0) {
                        $agent_distrubutor = $this->_get_agent($tusd['unidade']['_fk_agent_power_distributor']);
                        $tusd['agente_distribuidor'] = json_decode(json_encode($this->Agent_model), true);
                    }

                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($tusd, $destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->Tusd_model->_fk_agent);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($this->Tusd_model->_year, $this->Tusd_model->_month);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($tusd, $contact_list);
                            }

                        }
                        
                    }
                    
                }
            }
        }
        redirect("tusd");
    }
    
    private function _post_mail($tusd, $to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Fatura da Distribuidora";
        $data['tusd'] = $tusd;
        $msg = $this->load->view('tusd/email/upsert', $data, true);

        $post['message'] = base64_encode($msg);

        if (isset($tusd['_tusd_file']) && $tusd['_tusd_file'] != "") {
            $post['files'] = 'uploads/tusd/' . $tusd['_pk_tusd'] . '/' . $tusd['_tusd_file'];
        }

        $pymail = pymail($post);
    }    
}
