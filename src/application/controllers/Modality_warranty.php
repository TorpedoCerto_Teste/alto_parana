<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Modality_warranty extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Modality_warranty_model');
    }

    function index() {
        $this->Modality_warranty_model->_status = $this->Modality_warranty_model->_status_active;
        $data['list']                           = $this->Modality_warranty_model->fetch();
        $data['js_include']                     = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                    = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                   = 'modality_warranty/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('modality', 'modality', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Modality_warranty_model->_modality = mb_strtoupper($this->input->post('modality'), 'UTF-8');
                $this->Modality_warranty_model->_status   = $this->Modality_warranty_model->_status_active;
                $create                                   = $this->Modality_warranty_model->create();
                if ($create) {
                    redirect('modality_warranty/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'modality_warranty/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('modality_warranty/index');
        }
        $this->Modality_warranty_model->_pk_modality_warranty = $this->uri->segment(3);
        $read                                                 = $this->Modality_warranty_model->read();
        if (!$read) {
            redirect('modality_warranty/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('modality', 'modality', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Modality_warranty_model->_modality = mb_strtoupper($this->input->post('modality'), 'UTF-8');
                $update                                   = $this->Modality_warranty_model->update();
                redirect('modality_warranty/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'modality_warranty/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('modality_warranty/index');
        }
        $this->Modality_warranty_model->_pk_modality_warranty = $this->uri->segment(3);
        $read                                                 = $this->Modality_warranty_model->read();
        if (!$read) {
            redirect('modality_warranty/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('modality', 'modality', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Modality_warranty_model->_modality = mb_strtoupper($this->input->post('modality'), 'UTF-8');
                $upsert                                   = $this->Modality_warranty_model->upsert();
                redirect('modality_warranty/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'modality_warranty/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_modality_warranty') > 0) {
            $this->Modality_warranty_model->_pk_modality_warranty = $this->input->post('pk_modality_warranty');
            $this->Modality_warranty_model->delete();
        }
        redirect('modality_warranty/index');
    }

}
