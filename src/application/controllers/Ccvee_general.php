<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  pk_ccvee_general
  fk_ccvee
  fk_profile
  fk_submarket
  document_number
  contract_type
  cplp
  source
  rfq
  memo
  fk_energy_type
  retusd
  fk_unity_counterpart
  fk_profile_counterpart
  status
  created_at
  updated_at

 */

class Ccvee_general extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Ccvee_general_model');
    }

    function index() {
        $data['ape_statuses'] = [];
        if ($this->input->post('filter') == 'true') {
            $data['ape_statuses'] = $this->input->post('ape_statuses');

            if (is_array($data['ape_statuses']) && !empty($data['ape_statuses'])) {
               $this->Ccvee_general_model->_ape_status = $data['ape_statuses'];
            } 
        }

        $this->Ccvee_general_model->_status = $this->Ccvee_general_model->_status_active;
        $data['list']                       = $this->Ccvee_general_model->fetch_by_agent_unity();

        if ($this->input->post("export_excel") == 'true') {
            $this->_export_excel($data['list']);
            $this->disableLayout = TRUE;
            $this->output->set_content_type('application/json');
        }


        $data['js_include']                 = '
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>        
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'web/js/ccvee_general/index.js"></script>
            ';
        $data['css_include']                = '
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">           
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';

        $data['main_content']               = 'ccvee_general/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;
        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Ccvee_general_model->_pk_ccvee_general = $this->Ccvee_general_model->create();
                if ($this->Ccvee_general_model->_pk_ccvee_general) {
                    //criar os dados primários para energy
                    $this->load->model("Ccvee_energy_model");
                    $this->Ccvee_energy_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
                    $this->Ccvee_energy_model->create();

                    //criar os dados primários para payment
                    $this->load->model("Ccvee_payment_model");
                    $this->Ccvee_payment_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
                    $this->Ccvee_payment_model->create();

                    //salva o arquivo
                    $this->_process_files();

                    redirect('ccvee_general/view/' . $this->Ccvee_general_model->_pk_ccvee_general . '/energia');
                }
            }
            $data['error'] = true;
        }

        //pega a lista de tipos de energia
        $data['energy_types']             = $this->_process_energy_type();
        //pega a lista de usuários (cadastrado por)
        $data['users']                    = $this->_process_user();
        //pega a lista de unidades (cliente)
        $data['units_client_counterpart'] = $this->_process_unity();
        //pega a lista de submercados
        $data['submarkets']               = $this->_process_submarket();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="' . base_url() . 'web/js/ccvee_general/create.js"></script>';
        $data['css_include']  = '
            <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />  
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'ccvee_general/create';
        $this->load->view('includes/template', $data);
    }

    function view() {
        $this->Ccvee_general_model->_pk_ccvee_general = $this->uri->segment(3, 0);
        $read                                         = $this->Ccvee_general_model->read();

        if (!$read) {
            redirect('ccvee_general/index');
        }

        //pega a lista de tipos de energia
        $data['energy_types'] = $this->_process_energy_type();
        //pega a lista de usuários (cadastrado por)
        $data['users']        = $this->_process_user();
        //pega a lista de unidades (contraparte)
        $data['units']        = $this->_process_unity();
        //pega a lista de submercados
        $data['submarkets']   = $this->_process_submarket();

        //<início energia> recupera os dados da tab: Energia
        $this->load->model('Ccvee_energy_model');
        $this->Ccvee_energy_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $read_energy                                 = $this->Ccvee_energy_model->read();

        $data['merged_supply_period'] = array();
        $data['merged_seasonal']      = array();

        // apresenta os dados
        if ($read_energy) {

            //período diferenciado
            if ($this->Ccvee_energy_model->_differentiated_volume_period == 1) {
                //gera a os valores de volume conforme a data inicial e final
                $data['merged_supply_period'] = $this->Ccvee_energy_model->_proccess_supply($this->Ccvee_energy_model->_supply_start, $this->Ccvee_energy_model->_supply_end, $this->Ccvee_energy_model->_pk_ccvee_energy);
            }
            else {
                $data['merged_supply_period'] = $this->Ccvee_energy_model->_proccess_supply($this->Ccvee_energy_model->_supply_start, $this->Ccvee_energy_model->_supply_start, $this->Ccvee_energy_model->_pk_ccvee_energy);
            }

            //sazo
            if ($this->Ccvee_energy_model->_seasonal == 1) {
                $data['merged_seasonal'] = $this->Ccvee_energy_model->_proccess_seasonal($this->Ccvee_energy_model->_supply_start, $this->Ccvee_energy_model->_supply_end, $this->Ccvee_energy_model->_pk_ccvee_energy);
            }
        }

        //<fim energia>
        //<início pagamentos> recupera os dados da tab: Pagamentos
        $this->load->model('Ccvee_payment_model');
        $this->Ccvee_payment_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $read_payment                                 = $this->Ccvee_payment_model->read_by_general();

        if ($read_payment) {
            $data['payment_prices'] = false;
            if ($this->Ccvee_payment_model->_different_price_period == 1) {
                $this->load->model("Ccvee_payment_price_model");
                $this->Ccvee_payment_price_model->_fk_ccvee_payment = $this->Ccvee_payment_model->_pk_ccvee_payment;
                $this->Ccvee_payment_price_model->_status           = $this->Ccvee_payment_price_model->_status_active;
                $data['payment_prices']                             = $this->Ccvee_payment_price_model->fetch();
            }
        }
        //<fim pagamentos>
        //<início unidades>
        //pega as unidades deste contrato
        $this->load->model('Ccvee_unity_model');
        $this->Ccvee_unity_model->_status           = $this->Ccvee_unity_model->_status_active;
        $this->Ccvee_unity_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $data['list_units']                         = $this->Ccvee_unity_model->fetch();

        //pega a lista de unidades, separado por agentes
        $data['units2'] = $this->_process_agent_unity($data['list_units']);

        //<fim unidades>
        //<início garantias>
        $this->load->model("Modality_warranty_model");
        $this->Modality_warranty_model->_status = $this->Modality_warranty_model->_status_active;
        $data['modality_warranties']            = $this->Modality_warranty_model->fetch();

        $this->load->model('Ccvee_warranty_model');
        $this->Ccvee_warranty_model->_status           = $this->Ccvee_warranty_model->_status_active;
        $this->Ccvee_warranty_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $data['list_warranties']                       = $this->Ccvee_warranty_model->fetch();
        //<fim garantias>
        //<início faturamentos>
        $this->load->model('Ccvee_billing_model');
        $this->Ccvee_billing_model->_status            = $this->Ccvee_billing_model->_status_active;
        $this->Ccvee_billing_model->_fk_ccvee_general  = $this->Ccvee_general_model->_pk_ccvee_general;
        $data['list_billings']                         = $this->Ccvee_billing_model->fetch();


        //<fim faturamentos>

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/spinner-init.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
                <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
                <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
                <script src="' . base_url() . 'web/js/ccvee_general/view.js"></script>';
        $data['css_include']  = '<link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
                <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />    
                <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'ccvee_general/view';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('ccvee_general/index');
        }
        $this->Ccvee_general_model->_pk_ccvee_general = $this->uri->segment(3);
        $read                                         = $this->Ccvee_general_model->read();
        if (!$read) {
            redirect('ccvee_general/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Ccvee_general_model->update();
                $this->_process_files();
            }
            $data['error'] = true;
        }
        redirect('ccvee_general/view/' . $this->Ccvee_general_model->_pk_ccvee_general);
    }

    function delete() {
        if ($this->input->post('pk_ccvee_general') > 0) {
            $this->Ccvee_general_model->_pk_ccvee_general = $this->input->post('pk_ccvee_general');
            $this->Ccvee_general_model->delete();
        }
        redirect('ccvee_general');
    }

    private function _process_profile() {
        $this->load->model('Profile_model');
        $this->Profile_model->_status = $this->Profile_model->_status_active;
        return $this->Profile_model->fetch();
    }

    private function _process_energy_type() {
        $this->load->model('Energy_type_model');
        $this->Energy_type_model->_status = $this->Energy_type_model->_status_active;
        return $this->Energy_type_model->fetch();
    }

    private function _process_unity() {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();

        $option = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_type']][$unity['pk_unity']] = $unity;
            }
        }

        return $option;
    }

    private function _process_user() {
        $this->load->model('User_model');
        $this->User_model->_status = $this->User_model->_status_active;
        return $this->User_model->fetch();
    }

    private function _export_excel($data) {
        $file = "Contratos_de_Energia.xls";
        
        $html = '';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>Nº Interno</td>';
        $html .= '<td>Nº Documento</td>';
        $html .= '<td>Agente</td>';
        $html .= '<td>Unidade</td>';
        $html .= '<td>Tipo Energia</td>';
        $html .= '<td>Contraparte</td>';
        $html .= '<td>Operação</td>';
        $html .= '<td>Início Vig.</td>';
        $html .= '<td>Final Vig.</td>';
        $html .= '<td>Situação com a APE</td>';
        $html .= '</tr>';

        if ($data) {
            foreach ($data as $item) {
                $html .= '<tr>';
                $html .= '<td>'.substr($item['contract_type'], 0,1)."_".$item['cplp']."_".$item['energy_type'].".".$item['pk_ccvee_general'].'</td>';
                $html .= '<td>'.$item['document_number'].'</td>';
                $html .= '<td>'.$item['agent'].'</td>';
                $html .= '<td>'.$item['unity_name'].'</td>';
                $html .= '<td>'.$item['energy_type'].'</td>';
                $html .= '<td>'.$item['counterpart'].'</td>';
                $html .= '<td>'.$item['contract_type'].'</td>';
                $html .= '<td>'.date_to_human_date($item['energy_supply_start']).'</td>';
                $html .= '<td>'.date_to_human_date($item['energy_supply_end']).'</td>';
                $html .= '<td>'.$item['unity_ape_status'].'</td>';
                $html .= '</tr>';
            }
        }
        $html .= '</table>';
        // Configurações header para forçar o download
        header ("Expires: Mon, 10 Jan 2997 05:00:00 GMT");
        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-type: application/x-msexcel");
        header ("Content-Disposition: attachment; filename=\"{$file}\"" );
        header ("Content-Description: Alto Parana Energia" );
        // Envia o conteúdo do arquivo
        echo $html;
        exit;        
    }

    /*
     * TODO: VERIFICAR SE AINDA É USADO O/S METODOS ABAIXO
     */

    private function _process_agent_unity($list_units) {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();

        //retirar as unidades que já foram selecionadas
        if ($units && $list_units) {
            $units = $this->_clean_units($units, $list_units);
        }

        $option = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_company_name']][$unity['pk_unity']] = $unity;
            }
        }

        return $option;
    }

    private function _clean_units($units, $list_units) {
        foreach ($units as $k => $unit) {
            foreach ($list_units as $list_unit) {
                if ($unit['pk_unity'] == $list_unit['fk_unity']) {
                    unset($units[$k]);
                }
            }
        }
        return $units;
    }

    private function _process_submarket() {
        $this->load->model("Submarket_model");
        $this->Submarket_model->_status = $this->Submarket_model->_status_active;
        return $this->Submarket_model->fetch();
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/ccvee_general/' . $this->Ccvee_general_model->_pk_ccvee_general;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('ccvee_general_file')) {
            $this->Ccvee_general_model->_ccvee_general_file = str_replace(" ", "_", $_FILES['ccvee_general_file']['name']);
            $this->Ccvee_general_model->update();
        }
    }

    function ajax_change_unity_apportionment() {
        $this->Ccvee_general_model->_pk_ccvee_general = $this->input->post('pk_ccvee_general');
        $read                                         = $this->Ccvee_general_model->read();
        if ($read) {
            $this->Ccvee_general_model->_unity_apportionment = $this->input->post('unity_apportionment');
            $this->Ccvee_general_model->update();
        }
    }

    function ajax_change_unity_differentiated_validate() {
        $this->Ccvee_general_model->_pk_ccvee_general = $this->input->post('pk_ccvee_general');
        $read                                         = $this->Ccvee_general_model->read();
        if ($read) {
            $this->Ccvee_general_model->_unity_differentiated_validate = $this->input->post('unity_differentiated_validate');
            $this->Ccvee_general_model->update();
        }
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('fk_user', 'fk_user', 'trim|required');
        $this->form_validation->set_rules('fk_submarket', 'fk_submarket', 'trim|required');
        $this->form_validation->set_rules('document_number', 'document_number', 'trim');
        $this->form_validation->set_rules('contract_type', 'contract_type', 'trim');
        $this->form_validation->set_rules('cplp', 'cplp', 'trim');
        $this->form_validation->set_rules('source', 'source', 'trim');
        $this->form_validation->set_rules('rfq', 'rfq', 'trim');
        $this->form_validation->set_rules('memo', 'memo', 'trim');
        $this->form_validation->set_rules('fk_energy_type', 'fk_energy_type', 'trim|required');
        $this->form_validation->set_rules('retusd', 'retusd', 'trim');
        $this->form_validation->set_rules('fk_unity_counterpart', 'fk_unity_counterpart', 'trim|required');
    }

    private function _fill_model() {
        $this->Ccvee_general_model->_fk_unity             = $this->input->post('fk_unity');
        $this->Ccvee_general_model->_fk_user              = $this->input->post('fk_user');
        $this->Ccvee_general_model->_fk_submarket         = $this->input->post('fk_submarket');
        $this->Ccvee_general_model->_document_number      = mb_strtoupper($this->input->post('document_number'), 'UTF-8');
        $this->Ccvee_general_model->_contract_type        = $this->input->post('contract_type');
        $this->Ccvee_general_model->_cplp                 = $this->input->post('cplp');
        $this->Ccvee_general_model->_source               = $this->input->post('source');
        $this->Ccvee_general_model->_rfq                  = $this->input->post('rfq');
        $this->Ccvee_general_model->_memo                 = $this->input->post('memo');
        $this->Ccvee_general_model->_fk_energy_type       = $this->input->post('fk_energy_type');
        $this->Ccvee_general_model->_retusd               = number_format(str_replace(',', '.', $this->input->post('retusd')), 2, ".", "");
        $this->Ccvee_general_model->_fk_unity_counterpart = $this->input->post('fk_unity_counterpart');
        $this->Ccvee_general_model->_status               = $this->Ccvee_general_model->_status_active;
    }

    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->load->model('Ccvee_billing_model');
           $this->Ccvee_billing_model->_pk_ccvee_billing    = $this->input->post("pk_ccvee_billing_sendmail");
            $ccvee_billing                       = $this->Ccvee_billing_model->read();  

            if ($ccvee_billing) {
                //pega o agente
                $agent = $this->_get_agent($this->input->post('fk_agent_sendmail'));
    
                if ($agent) {
                    $ccvee_billing = json_decode(json_encode($this->Ccvee_billing_model), true);
                    $ccvee_billing['agente'] = json_decode(json_encode($this->Agent_model), true);
                    //pega a unidade
                    $unity = $this->_get_unity($ccvee_billing['_fk_unity']);
                    if ($unity) {
                        $ccvee_billing['unity'] = json_decode(json_encode($this->Unity_model), true);
                    }

                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($ccvee_billing, $destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->input->post('fk_agent_sendmail'));
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($ccvee_billing, $contact_list);
                            }
                        }                        
                    }

                }
            }

        }
        redirect("ccvee_general/view/".$this->input->post('pk_ccvee_general_sendmail')."#faturamentos");
    }
    
    private function _post_mail($ccvee_billing, $to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Faturamento de Energia Livre";
        $data['ccvee_billing'] = $ccvee_billing;

        $msg = $this->load->view('ccvee_general/email/view', $data, true);

        $post['message'] = base64_encode($msg);

        $file = [];
        if ($ccvee_billing['_invoice'] != "") {
            $file[] = 'uploads/ccvee_billing/' . $ccvee_billing['_pk_ccvee_billing'] . '/' . str_replace(' ', '_', $ccvee_billing['_invoice']);
        }
        if ($ccvee_billing['_xml'] != "") {
            $file[] = 'uploads/ccvee_billing/' . $ccvee_billing['_pk_ccvee_billing'] . '/' . str_replace(' ', '_', $ccvee_billing['_xml']);
        }
        if ($ccvee_billing['_payment_slip'] != "") {
            $file[] = 'uploads/ccvee_billing/' . $ccvee_billing['_pk_ccvee_billing'] . '/' . str_replace(' ', '_', $ccvee_billing['_payment_slip']);
        }

        if (!empty($file)) {
            $post['files'] = join(",", $file);
        }

        $pymail = pymail($post);
    }        

}
