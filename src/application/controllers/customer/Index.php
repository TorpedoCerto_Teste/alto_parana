<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Index
 *
 * @author rogeriopellarin
 */
class Index extends Customer_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        redirect("customer/dashboard");
        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->session->userdata('fk_agent');
        $this->Agent_model->read();


        $data['js_include']   = '
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.resize.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.tooltip.min.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.selection.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.stack.js"></script>
            <script src="' . base_url() . 'js/flot-chart/jquery.flot.crosshair.js"></script>
            <script src="' . base_url() . 'web/js/customer/index/index.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'index/index';
        $this->load->view('customer/includes/template', $data);
    }

    function login() {
        $data['error'] = 0;

        if ($this->input->post('login') == 'true') {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->load->model('Contact_model');
                $this->Contact_model->_email    = $this->input->post('email');
                $this->Contact_model->_password = md5($this->input->post('password'));
                $find                           = $this->Contact_model->find_by_email_and_password();
                if ($find) {
                    //Recuperar a lista de agentes disponíveis para este contato
                    $agents_list = $this->_get_agents_list();
                    if (!empty($agents_list)) {
                        $this->_register_session($agents_list);
                        if ($this->session->has_userdata('url_return_customer')) {
                            redirect($this->session->userdata('url_return_customer'));
                        }
                        redirect('customer');
                    }
                }
                $data['error'] = 1;
            }
        }

        $this->load->view('customer/index/login', $data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('customer/index/login');
    }
    
    function change_agent() {
        $fk_agent = $this->uri->segment(4, 0);
        if ($fk_agent > 0) {
            $login_session['fk_agent'] = $fk_agent;
            $this->session->set_userdata($login_session);            
        }
        
        redirect('customer');
    }

    private function _get_agents_list() {
        $return                                 = array();
        $this->load->model('Agent_contact_model');
        $this->Agent_contact_model->_fk_contact = $this->Contact_model->_pk_contact;
        $this->Agent_contact_model->_status     = $this->Agent_contact_model->_status_active;
        $agents_list                            = $this->Agent_contact_model->fetch();
        if (!empty($agents_list)) {
            foreach ($agents_list as $agent) {
                $return[$agent['fk_agent']] = array(
                    'fk_agent'       => $agent['fk_agent'],
                    'community_name' => $agent['community_name']
                );
            }
        }
        return $return;
    }

    private function _register_session($agents_list) {
        $login_session['pk_contact']  = $this->Contact_model->_pk_contact;
        $login_session['name']        = $this->Contact_model->_name;
        $login_session['email']       = $this->Contact_model->_email;
        $login_session['fk_agent']    = $agents_list[array_keys($agents_list)[0]]['fk_agent'];
        $login_session['agents_list'] = $agents_list;
        $login_session['token']       = md5(date('YmdHis') . $this->Contact_model->_fk_agent);
        $this->session->set_userdata($login_session);
    }

    private function _send_email_forgot_password($new_password) {
        $this->load->library('email');
        $this->load->config('email_config');
        $this->load->clear_vars();
        $this->email->from($this->config->item('smtp_user'), $this->config->item('remetente'));
        $destinatarios   = $this->config->item('to');
        $destinatarios[] = $this->input->post('input_enter_user');
        $this->email->to($destinatarios);
        $this->email->subject('Recuperar password');

        $data['new_password'] = $new_password;
        $msg                  = $this->load->view('emails/customer/forgot_password', $data, TRUE);
        $this->email->message($msg);

        return $this->email->send();
    }

}
