<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_billing extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Ccvee_billing_model');
    }

    function index() {
        $data['list'] = $this->_process_ccvee();
        if ($data['list']) {
            foreach ($data['list'] as $key => $ccvee) {
                $data['list'][$key]['billing'] = $this->_process_ccvee_billing($ccvee['pk_ccvee_general']);
            }
        }
        $data['js_include']   = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'js/data-table-init.js"></script>
            ';
        $data['css_include']  = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">';
        $data['main_content']                 = 'ccvee_billing/index';
        $this->load->view('customer/includes/template', $data);
    }


    private function _process_ccvee() {
        $this->load->model('Ccvee_general_model');
        $this->Ccvee_general_model->_status   = $this->Ccvee_general_model->_status_active;
        $this->Ccvee_general_model->_fk_agent = $this->session->userdata('fk_agent');
        return $this->Ccvee_general_model->fetch_by_agent_unity();
    }
    
    private function _process_ccvee_billing($fk_ccvee_general) {
        $this->Ccvee_billing_model->_status = $this->Ccvee_billing_model->_status_active;
        $this->Ccvee_billing_model->_fk_ccvee_general = $fk_ccvee_general;
        return $this->Ccvee_billing_model->fetch();
    }


}
