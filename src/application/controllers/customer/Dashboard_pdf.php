<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_pdf extends CI_Controller {

    function __construct() {
        set_time_limit(0);
        parent::__construct();
        @header('Access-Control-Allow-Origin: *');
        @header('Access-Control-Allow-Methods: GET');
    }

    function index() {
        $data['js_include']       = '
                <script src="' . base_url() . 'js/Chart.min.js"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/underscore.js"></script>
                <script src="' . base_url() . 'web/js/customer/dashboard_new/index.vue.js?'. microtime().'"></script>
            ';
        $data['css_include']      = '
                <link href="' . base_url() . 'web/css/dashboard_new/index.css" rel="stylesheet">
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">'; 

        $data['main_content'] = 'dashboard_pdf/index';
        $this->load->view('customer/includes/simple/template', $data); 
    }

}