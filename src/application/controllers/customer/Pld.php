<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pld extends Customer_Controller {

    function __construct() {
        parent::__construct();
        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }        

        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->session->userdata('fk_agent');
        $this->Agent_model->read();

        $this->load->model('Pld_model');
    }

    function index()
    {
        $data['year'] = preg_replace('/([^\d*])/', '', $this->uri->segment(4, date('Y')));

        $this->Pld_model->_status = $this->Pld_model->_status_active;
        $this->Pld_model->_year = strlen($data['year']) == 4 ? $data['year'] : date('Y');
        $list                   = $this->Pld_model->fetch();

        $data['list'] = $this->_prepare_data($list);

        $data['js_include']   = '
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/customer/pld/index.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">';
        $data['main_content'] = 'pld/index';
        $this->load->view('customer/includes/template', $data);
    }

    private function _prepare_data($list)
    {
        $return = [];
        for ($i = 0; $i < 12; $i++) {
            $return[$i] = [
                "month_name" => retorna_mes($i + 1),
                "month" => $i + 1,
                "seco" => 0.0,
                "s" => 0.0,
                "ne" => 0.0,
                "n" => 0.0,
                "spread_i5" => 0.0,
                "spread_i1" => 0.0,
                "status_pld" => 0
            ];
        }

        if ($list) {
            foreach ($list as $k => $item) {
                $return[$k]['seco'] = number_format($item['seco'], 2);
                $return[$k]['s'] = number_format($item['s'], 2);
                $return[$k]['ne'] = number_format($item['ne'], 2);
                $return[$k]['n'] = number_format($item['n'], 2);
                $return[$k]['spread_i5'] = number_format($item['spread_i5'], 2);
                $return[$k]['spread_i1'] = number_format($item['spread_i1'], 2);
                $return[$k]['status_pld'] = $item['status_pld'];
            }
        }

        return $return;
    }
}
