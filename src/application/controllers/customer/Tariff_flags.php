<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tariff_flags extends Customer_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Tariff_flags_model');
    }

    function index() {
        $data['error'] = false;
        $this->Tariff_flags_model->_year = $this->input->post('year') ? $this->input->post('year') : date("Y");
        $data['tariffs']                 = $this->_get_tariffs();
        $data['js_include']              = '<script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/customer/tariff_flags/index.js"></script>';
        $data['css_include']             = '<link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">';
        $data['main_content']            = 'tariff_flags/index';
        $this->load->view('customer/includes/template', $data);
    }

    function ajax() {
        $this->Tariff_flags_model->_year = $this->uri->segment(3, 2016);
        $tariffs                         = $this->_get_tariffs();
        print(json_encode($tariffs));
    }

    private function _get_tariffs() {
        for ($i = 1; $i <= 12; $i ++) {
            $return[$i]['year']     = $this->Tariff_flags_model->_year;
            $return[$i]['flag']     = "VERDE";
            $return[$i]['increase'] = 0;
            $return[$i]['color']    = 'success';
        }
        $tariffs = $this->Tariff_flags_model->fetch();
        if ($tariffs) {
            foreach ($tariffs as $tariff) {
                $return[$tariff['month']]['flag']     = $tariff['flag'];
                $return[$tariff['month']]['increase'] = $tariff['increase'];
                if ($tariff['flag'] == 'AMARELA') {
                    $return[$tariff['month']]['color'] = 'warning';
                }
                elseif ($tariff['flag'] == 'VERMELHA' || $tariff['flag'] == 'VERMELHA II') {
                    $return[$tariff['month']]['color'] = 'danger';
                }
            }
        }
        return $return;
    }

}
