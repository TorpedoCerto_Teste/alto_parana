<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }

        $this->load->model('Calendar_model');
    }

    function index() {
        $config['day_type'] = 'long';
        $config['template'] = '
            {table_open}<table class="calendar">{/table_open}
            {week_day_cell}<th class="day_header">{week_day}</th>{/week_day_cell}
            {cal_cell_content}<span class="day_listing">{day}</span>{content}&nbsp;{/cal_cell_content}
            {cal_cell_content_today}<div class="today"><span class="day_listing">{day}</span>&bull; {content}</div>{/cal_cell_content_today}
            {cal_cell_no_content}<span class="day_listing">{day}</span>&nbsp;{/cal_cell_no_content}
            {cal_cell_no_content_today}<div class="today"><span class="day_listing">{day}</span></div>{/cal_cell_no_content_today}
        ';
        $this->load->library('calendar', $config);

        //pega os responsáveis
        $data['calendar_responsibles'] = $this->_process_calendar_responsible();
        $data['colors']                = $this->config->item("colors");

        //pega os eventos
        $data['month_events'] = $this->_process_events();

        $calendar_events  = $this->_process_calendar_events($data['month_events']);
        $data['calendar'] = $this->calendar->generate(date("Y", strtotime($this->Calendar_model->_event_date)), date("m", strtotime($this->Calendar_model->_event_date)), $calendar_events);

        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/data-table-init.js"></script>
            <script src="' . base_url() . 'web/js/customer/calendar/index.js"></script> ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            <link href="' . base_url() . 'web/css/calendar/index.css" rel="stylesheet">
        ';
        $data['main_content'] = 'calendar/index';
        $this->load->view('customer/includes/template', $data);
    }

    public function _process_events() {
        $this->Calendar_model->_event_date = $this->input->post('event_date') != "" ? $this->input->post('event_date') : date("Y-m-d");
        $month_events                      = $this->Calendar_model->fetch_month_events_by_agent();
        if ($month_events) {
            foreach ($month_events as $k => $event) {
                if ($event['c_status'] == 1) {
                    $month_events[$k]['status'] = 2;
                }
            }
        }
        return $month_events;
    }

    private function _process_calendar_responsible() {
        $this->load->model("Calendar_responsible_model");
        $this->Calendar_responsible_model->_status = $this->Calendar_responsible_model->_status_active;
        return $this->Calendar_responsible_model->fetch();
    }

    private function _process_calendar_events($month_events) {
        $colors = $this->config->item("colors");
        $return = array();
        if ($month_events) {
            foreach ($month_events as $event) {
                $return[date("j", strtotime($event['event_date']))] = "";
            }
            foreach ($month_events as $event) {
                $status                                             = $event['status'] == 2 ? "checked" : "";
                $enabled                                            = $event['responsible'] != "CLIENTE" ? "disabled" : "";
                $return[date("j", strtotime($event['event_date']))] .= '<a class="btn btn-' . $colors[$event['fk_calendar_responsible']] . ' btn-sm popovers" data-original-title="' . date_to_human_date($event['event_date']) . '" data-content="<b>Evento: </b>' . $event['calendar'] . '<br/><b>Ref: </b>' . $event['month_reference'] . '/' . $event['year_reference'] . '" data-placement="top" data-trigger="hover" data-html="true"><input type="checkbox" class="chk" id="chk_c_' . $event['pk_calendar'] . '" value="' . $event['pk_calendar'] . '" ' . $status . ' ' . $enabled . ' /></a>&nbsp;';
            }
        }
        return $return;
    }


}
