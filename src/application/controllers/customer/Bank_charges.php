<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_charges extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Bank_charges_model');
    }

    function index() {
        $data['error'] = false;

        //pega todas as distribuidoras
        $this->load->model('Agent_model');
        $this->Agent_model->_fk_agent_type = $this->config->item('pk_agent_type_distribuidora');
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $data['agents']                    = $this->Agent_model->fetch();

        //se o pk_bank_charges for informado, selecionar a tarifa selecionada
        $this->Bank_charges_model->_pk_bank_charges = $this->uri->segment(4, '');
        $read_bank_charges                          = $this->Bank_charges_model->read();

        if ($read_bank_charges) {
            $data['resolutions'] = $this->Bank_charges_model->fetch_resolution();
        }


        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/select2.js"></script>
            <script src="' . base_url() . 'js/select2-init.js"></script>
            <script src="' . base_url() . 'web/js/customer/bank_charges/index.js"></script>';
        $data['css_include']  = '    
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'bank_charges/index';
        $this->load->view('customer/includes/template', $data);
    }

    /**
     * Método ajax para pegar as resoluções
     */
    function resolution() {
        $this->Bank_charges_model->_fk_agent_power_distributor = $this->uri->segment(4, 0);
        $resolutions                                           = $this->Bank_charges_model->fetch_resolution();
        print(json_encode($resolutions));
    }

}
