<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Index
 *
 * @author rogeriopellarin
 */
class Market extends Customer_Controller {

    function __construct() {
        parent::__construct();
        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }        

        $this->load->model("Agent_model");
        $this->Agent_model->_pk_agent = $this->session->userdata('fk_agent');
        $this->Agent_model->read();
    }

    function index() {
        $data['js_include']   = '
            <script src="'.base_url().'js/flot-chart/jquery.flot.js"></script>
            <script src="'.base_url().'web/js/customer/market/index.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'market/index';
        $this->load->view('customer/includes/template', $data);
    }

}