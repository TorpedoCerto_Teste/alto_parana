<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Devec_market extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Devec_market_model');
    }

    function index() {
        $data['devecs'] = $this->_process_devec();

        $data['js_include']   = '
        ';
        $data['css_include']  = '';
        $data['main_content'] = 'devec_market/index';
        $this->load->view('customer/includes/template', $data);
    }

    private function _process_devec() {
        $return = false;
        $states = retorna_estados();
        if ($states) {
            foreach ($states as $sigla => $estado) {
                $return[$sigla]['devec']      = 0;
                $return[$sigla]['start_date'] = null;
            }
        }

        $this->Devec_market_model->_status = $this->Devec_market_model->_status_active;
        $devecs                            = $this->Devec_market_model->fetch();
        if ($devecs) {
            foreach ($devecs as $devec) {
                $return[$devec['uf']]['devec']      = $devec['devec'];
                $return[$devec['uf']]['start_date'] = $devec['start_date'];
            }
        }
        return $return;
    }


}
