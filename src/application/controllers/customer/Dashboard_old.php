<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Customer_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
    }

    public function index() {
        $data['date']             = date("Y-m-d");
        $data['pk_ccvee_general'] = '';

        if ($this->input->post('date')) {
            $data['date'] = $this->input->post('date');
        }

        //pega a lista de unidades
        $data['unities'] = $this->_process_unities();
        
        if ($data['unities']) {
            //pega todos os dados referentes ao CCVEE
            $data['ccvee_generals']            = $this->_process_ccvee_general($data['date'], $data['unities']);
            $data['ccvee_general_energy_bulk'] = $this->_process_ccvee_general_energy_bulk($data['date'], $data['ccvee_generals']);

            //pega todas as leituras de telemetria da unidade
            $data['telemetry_records'] = $this->_process_telemetry_records($data['date'], $data['ccvee_generals']);

            //gera os dados para o gráfico de contrato x consumo
            $data['contrato_versus_consumo'] = $this->_process_contrato_versus_consumo($data['date'], $data['ccvee_general_energy_bulk'], $data['telemetry_records']);

            //widgets (resumo)
            $consumo                        = $this->_process_consumo($data['telemetry_records']);
            $data['perdas']                 = ($consumo * $data['ccvee_generals'][0]['losses']) / 100;
//            $data['proinfa']                = $this->_process_consumo_proinfa($consumo, $data['ccvee_general_energy_bulk']);
            $data['proinfa']                = $this->_process_proinfa_by_unity($data['date'], $data['unities']);
            $data['consumo']                = $consumo + $data['perdas'] - $data['proinfa'];
            $data['contrato']               = $this->_get_energy_contract($data['date'], $data['ccvee_general_energy_bulk']);
            $data['flexibilidade_inferior'] = $data['contrato'] + (($data['contrato'] * $data['ccvee_general_energy_bulk'][0]['flexibility_inferior']) / 100);
            $data['flexibilidade_superior'] = $data['contrato'] + (($data['contrato'] * $data['ccvee_general_energy_bulk'][0]['flexibility_superior']) / 100);
            $data['horas_medidas']          = $this->_process_horas_medidas($data['telemetry_records']);
            $data['horas_faltantes']        = $this->_process_horas_faltantes($data['telemetry_records']);
            $data['sobra']                  = $data['consumo'] < $data['flexibilidade_inferior'] ? $data['flexibilidade_inferior'] - $data['consumo'] : 0;
            $data['deficit']                = $data['consumo'] > $data['flexibilidade_superior'] ? $data['consumo'] - $data['flexibilidade_superior'] : 0;
            $data['total_horas_mes']        = hours_of_month(date("d/m/Y", strtotime($data['date'])));
        }

        $data['js_include']       = '
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>    
                <script src="' . base_url() . 'web/js/customer/dashboard/index.js?'. microtime().'"></script> 
            ';
        $data['css_include']      = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        
        
        $data['main_content'] = 'dashboard/index';
        $this->load->view('customer/includes/template', $data);
    }
    
    public function dash() {
        $data['date']             = date("Y-m-d");
        $data['js_include']       = '
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>    
                <script src="' . base_url() . 'web/js/customer/dashboard/index.js?'. microtime().'"></script> 
            ';
        $data['css_include']      = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        
        
        $data['main_content'] = 'dashboard/dash';
        $this->load->view('customer/includes/template', $data);
        
    }

    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $this->Unity_model->_fk_agent = $this->session->userdata('fk_agent');
        return $this->Unity_model->fetch();
    }

    private function _process_ccvee_general($date, $unities) {
        $unity_list = array();
        foreach ($unities as $unity) {
            $unity_list[] = $unity['pk_unity'];
        }
        
        $general_energy = $this->_process_general_energy($date, $unity_list);

        return $general_energy;
    }

    private function _process_ccvee_general_energy_bulk($date, $general_energy) {
        if ($general_energy) {
            foreach ($general_energy as $k => $dados_contrato) {
                //se a opção "discount_proinfa" estiver marcada, trazer o proinfa de cada unidade
                $general_energy[$k]['proinfa'] = false;
                if ($dados_contrato['discount_proinfa'] == 1) {
                    $general_energy[$k]['proinfa'] = $this->_process_proinfa($date, $dados_contrato['fk_unity']);
                }

                //volume de energia
                $date_bulk                               = $dados_contrato['differentiated_volume_period'] == 1 ? $date : $dados_contrato['supply_start'];
                $general_energy[$k]['ccvee_energy_bulk'] = $this->_process_ccvee_energy_bulk($date_bulk, $dados_contrato['pk_ccvee_energy']);
            }
        }
        return $general_energy;
    }

    private function _process_general_energy($date, $unity_list) {
        $this->load->model("Ccvee_general_model");
        $this->Ccvee_general_model->_fk_unity   = $unity_list;
        $this->Ccvee_general_model->_created_at = $date;
        return $this->Ccvee_general_model->fetch_general_energy_by_unity();
    }

    private function _process_proinfa($date, $fk_unity) {
        $this->load->model("Proinfa_model");
        $this->Proinfa_model->_fk_unity = $fk_unity;
        $this->Proinfa_model->_month    = date("m", strtotime($date));
        $this->Proinfa_model->_year     = date("Y", strtotime($date));
        $this->Proinfa_model->_status   = $this->Proinfa_model->_status_active;
        return $this->Proinfa_model->fetch();
    }

    private function _process_proinfa_by_unity($date, $unities) {
        $return = 0;
        $unity_list = array();
        foreach ($unities as $unity) {
            $unity_list[] = $unity['pk_unity'];
        }

        $proinfas = $this->_process_proinfa($date, $unity_list);
        if ($proinfas) {
            foreach ($proinfas as $proinfa) {
                $return += $proinfa['value_mwh'];
            }
        }
        return $return;
    }

    private function _process_ccvee_energy_bulk($date, $fk_ccvee_energy) {
        $this->load->model("Ccvee_energy_bulk_model");
        $this->Ccvee_energy_bulk_model->_status          = $this->Ccvee_energy_bulk_model->_status_active;
        $this->Ccvee_energy_bulk_model->_date            = date("Y-m-01", strtotime($date));
        $this->Ccvee_energy_bulk_model->_fk_ccvee_energy = $fk_ccvee_energy;
        return $this->Ccvee_energy_bulk_model->fetch();
    }

    private function _process_telemetry_records($date, $ccvee_generals) {
        $return = array();
        if ($ccvee_generals) {
            foreach ($ccvee_generals as $contrato) {
                //descobre o medidor (gauge) pela unidade
                $gauges = $this->_process_gauge($contrato['fk_unity']);
                if ($gauges) {
                    $gauges_list = array();
                    foreach ($gauges as $gauge) {
                        $gauges_list[] = $gauge['pk_gauge'];
                    }
                    $return = $this->_process_telemetry($date, $gauges_list);
                }
            }
        }
        return $return;
    }

    private function _process_gauge($fk_unity) {
        $this->load->model("Gauge_model");
        $this->Gauge_model->_status   = $this->Gauge_model->_status_active;
        $this->Gauge_model->_fk_unity = $fk_unity;
        return $this->Gauge_model->fetch();
    }

    private function _process_telemetry($date, $gauges_list) {
        $this->load->model("Telemetry_record_model");
        $this->Telemetry_record_model->_fetch_period = "month";
        $this->Telemetry_record_model->_greatness    = "Eneat";
        $this->Telemetry_record_model->_record_date  = $date;
        $this->Telemetry_record_model->_fk_gauge     = $gauges_list;
        $this->Telemetry_record_model->_status       = $this->Telemetry_record_model->_status_active;
        return $this->Telemetry_record_model->fetch();
    }

    private function _process_contrato_versus_consumo($date, $ccvee_general_energy_bulk, $telemetry_records) {
        $energy_contact = $this->_get_energy_contract($date, $ccvee_general_energy_bulk)/date("t");
        $superior_limit = $energy_contact + (($energy_contact * $ccvee_general_energy_bulk[0]['flexibility_superior']) / 100);
        $inferior_limit = $energy_contact + (($energy_contact * $ccvee_general_energy_bulk[0]['flexibility_inferior']) / 100);

        //gera os dias vazios
        $records = array();
        for ($i = 1; $i <= date("t", strtotime($date)); $i++) {
            $records[$i]['dia']      = $i; // "['1/08', 684, 600, 800, 400],\n";
            $records[$i]['consumo']  = 0;
            $records[$i]['contrato'] = $energy_contact;
            $records[$i]['flex_sup'] = $superior_limit;
            $records[$i]['flex_inf'] = $inferior_limit;
        }

        if ($telemetry_records) {
            foreach ($telemetry_records as $telemetry_record) {
                $records[date("j", strtotime($telemetry_record['record_date']))]['consumo'] += ($telemetry_record['value'] / 1000);
            }
        }

        return $this->_transform_records_to_chart_data($records);
    }

    private function _get_energy_contract($date, $ccvee_energy_bulk) {
        $return = 0;
        if ($ccvee_energy_bulk[0]['ccvee_energy_bulk'][0]['seasonal'] > 0) {
            $return = $ccvee_energy_bulk[0]['ccvee_energy_bulk'][0]['seasonal'];
        }
        else {
            $return = $ccvee_energy_bulk[0]['ccvee_energy_bulk'][0]['forecast'];
        }

        if ($ccvee_energy_bulk[0]['unit_measurement'] == "MWm") {
            return $return * (hours_of_month(date_to_human_date($date)));
        }
        return $return;
    }

    private function _transform_records_to_chart_data($records) {
        //colunas
        $col1            = array();
        $col1["id"]      = "";
        $col1["label"]   = "Dia";
        $col1["pattern"] = "";
        $col1["type"]    = "string";

        $col2            = array();
        $col2["id"]      = "";
        $col2["label"]   = "Consumo";
        $col2["pattern"] = "";
        $col2["type"]    = "number";

        $col3            = array();
        $col3["id"]      = "";
        $col3["label"]   = "Contrato";
        $col3["pattern"] = "";
        $col3["type"]    = "number";

        $col4            = array();
        $col4["id"]      = "";
        $col4["label"]   = "Flexibilidade Superior";
        $col4["pattern"] = "";
        $col4["type"]    = "number";

        $col5            = array();
        $col5["id"]      = "";
        $col5["label"]   = "Flexibilidade Inferior";
        $col5["pattern"] = "";
        $col5["type"]    = "number";

        $cols = array($col1, $col2, $col3, $col4, $col5);

        $rows = array();
        foreach ($records as $record) {
            $cel0["v"] = $record['dia'];
            $cel1["v"] = $record['consumo'];
            $cel2["v"] = $record['contrato'];
            $cel3["v"] = $record['flex_sup'];
            $cel4["v"] = $record['flex_inf'];
            $row0["c"] = array($cel0, $cel1, $cel2, $cel3, $cel4);
            array_push($rows, $row0);
        }

        $data = array("cols" => $cols, "rows" => $rows);
        return (json_encode($data));
    }

    private function _process_consumo($telemetry_records) {
        $return = 0;
        if ($telemetry_records) {
            foreach ($telemetry_records as $telemetry_record) {
                $return += ($telemetry_record['value']);
            }
            return $return / 1000;
        }
        return $return;
    }

    private function _process_consumo_proinfa($consumo, $ccvee_general_energy_bulk) {
        if ($ccvee_general_energy_bulk[0]['proinfa']) {
            return ($consumo * $ccvee_general_energy_bulk[0]['proinfa'][0]['value_mwh']) / 100;
        }
        return 0;
    }

    private function _process_horas_medidas($telemetry_records) {
        $return = 0;
        if ($telemetry_records) {
            foreach ($telemetry_records as $telemetry_record) {
                if ($telemetry_record['quality'] == 0) {
                    $return++;
                }
            }
        }
        return $return;
    }

    private function _process_horas_faltantes($telemetry_records) {
        $return = 0;
        if ($telemetry_records) {
            foreach ($telemetry_records as $telemetry_record) {
                if ($telemetry_record['quality'] == 1) {
                    $return++;
                }
            }
        }
        return $return;
    }

}
