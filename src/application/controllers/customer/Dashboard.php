<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Customer_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
    }

    function index() {
        $data['date'] = date("Y-m-d");
        
        $data['js_include']       = '
                <script src="' . base_url() . 'js/Chart.min.js"></script>
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/underscore.js"></script>
                <script src="' . base_url() . 'web/js/customer/dashboard_new/index.vue.js?'. microtime().'"></script>
            ';
        $data['css_include']      = '
                <link href="' . base_url() . 'web/css/dashboard_new/index.css" rel="stylesheet">
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';        
        $data['main_content'] = 'dashboard_new/index';
        $this->load->view('customer/includes/template', $data); 
    }
    
    function graph() {
        $data['js_include']       = '
                <script src="' . base_url() . 'js/Chart.min.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'web/js/customer/dashboard_new/graph.vue.js?'. microtime().'"></script>
            ';
        $data['css_include']      = '';        
        $this->load->view('customer/dashboard_new/graph', $data); 
    }
    
    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $this->Unity_model->_fk_agent = $this->session->userdata('fk_agent');
        return $this->Unity_model->fetch();
    }
    
    private function _process_ccvee_general($date, $unities) {
        $unity_list = array();
        foreach ($unities as $unity) {
            $unity_list[] = $unity['pk_unity'];
        }

        $general_energy = $this->_process_general_energy($date, $unity_list);

        return $general_energy;
    }

    private function _process_general_energy($date, $unity_list) {
        $this->load->model("Ccvee_general_model");
        $this->Ccvee_general_model->_fk_unity   = $unity_list;
        $this->Ccvee_general_model->_created_at = $date;
        return $this->Ccvee_general_model->fetch_general_energy_by_unity();
    }

}