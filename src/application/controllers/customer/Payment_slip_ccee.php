<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_slip_ccee extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Payment_slip_ccee_model');
    }

    function index() {
        $this->Payment_slip_ccee_model->_fk_agent = $this->session->userdata('fk_agent');
        $this->Payment_slip_ccee_model->_status   = $this->Payment_slip_ccee_model->_status_active;
        $data['list']                             = $this->Payment_slip_ccee_model->fetch();
        $data['js_include']                       = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                      = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                     = 'payment_slip_ccee/index';
        $this->load->view('customer/includes/template', $data);
    }

}
