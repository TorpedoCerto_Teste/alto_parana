<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_measurement extends Customer_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
    }

    function index() {
        $data['date']        = date("Y-m-d");
        $data['js_include']  = '
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>    
                <script src="' . base_url() . 'web/js/customer/dashboard_measurement/index.js"></script> 
            ';
        $data['css_include'] = '
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';

        //pega a lista de unidades
        $data['unities'] = $this->_process_unities();
        if (count($data['unities']) == 1) {
            $this->Unity_model->_pk_unity = $data['unities'][0]['pk_unity'];
            $data['gauges']               = $this->_process_gauges();
        }
        else {
            $data['gauges'] = false;
        }

        if (count($data['gauges']) == 1) {
            $this->Gauge_model->_pk_gauge = $data['gauges'][0]['pk_gauge'];
        }

        if ($this->input->post('filter') == 'true') {
            $this->Unity_model->_pk_unity = $this->input->post('fk_unity');
            $this->Gauge_model->_pk_gauge = $this->input->post('fk_gauge');
            $data['date']                 = $this->input->post('date');
        }

        //coleta dos dados caso unidade e gauge estiverem selecionados
        if ($this->Unity_model->_pk_unity != "" && $this->Gauge_model->_pk_gauge != "") {
            //pega o tipo de TUSD (demand)
            $this->_process_demand($data['date']);

            $data['telemetry_records'] = $this->_process_telemetry_records($data['date']);
            $data['current_unity']     = $this->_get_current_unity($data['unities']);
            $data['rush_hour']         = $this->_process_rush_hour($data['current_unity']);
            $data['holiday']           = $this->_process_holiday($data['date']);

            //widgets
            //
            $data['demanda_medida_ponta']              = $this->_process_demanda_medida_ponta($data['telemetry_records']['Demat']['records'], $data['rush_hour'],$data['holiday']);
            $data['demanda_medida_fora_ponta']         = $this->_process_demanda_medida_fora_ponta($data['telemetry_records']['Demat']['records'], $data['rush_hour'],$data['holiday']);
            $data['consumo_mensal']                    = $data['telemetry_records']['Demat']['total']['success']['value'] > 0 ? ($data['telemetry_records']['Demat']['total']['success']['value'] / 1000)/4 : 0;
            $data['limite_consumo']                    = $this->_process_limite_consumo($data['date']);
            $data['fator_carga_ponta']                 = $this->_process_fator_carga_ponta($data['telemetry_records']['Eneat']['records'], $data['rush_hour'],$data['holiday']);
            $data['fator_carga_fora_ponta']            = $this->_process_fator_carga_fora_ponta($data['telemetry_records']['Demat']['records'], $data['rush_hour'],$data['holiday']);
            $data['resumo_grafico_consumo_diario_mes'] = $this->_process_resumo_grafico_consumo_diario_mes($data['telemetry_records']['Demat']['records']);
        }


        $data['main_content'] = 'dashboard_measurement/index';
        $this->load->view('customer/includes/template', $data);
    }

    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $this->Unity_model->_fk_agent = $this->session->userdata('fk_agent');
        return $this->Unity_model->fetch();
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status   = $this->Agent_model->_status_active;
        $this->Agent_model->_pk_agent = $this->session->userdata('fk_agent');
        $agents                       = $this->Agent_model->fetch();
        $option                       = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_gauges() {
        $this->load->model("Gauge_model");
        $this->Gauge_model->_fk_unity = $this->Unity_model->_pk_unity;
        $this->Gauge_model->_status   = $this->Gauge_model->_status_active;
        return $this->Gauge_model->fetch();
    }

    private function _process_demand($date) {
        $this->load->model("Demand_model");
        $this->Demand_model->_status     = $this->Demand_model->_status_active;
        $this->Demand_model->_start_date = $date;
        $this->Demand_model->_fk_unity   = $this->Unity_model->_pk_unity;
        $this->Demand_model->read();
    }

    private function _get_current_unity($unities) {
        $return = false;
        foreach ($unities as $unity) {
            if ($unity['pk_unity'] == $this->Unity_model->_pk_unity) {
                $return = $unity;
            }
        }
        return $return;
    }

    private function _process_rush_hour($current_unity) {
        $return = false;
        if ($current_unity) {
            $this->load->model("Rush_hour_model");
            $this->Rush_hour_model->_status                     = $this->Rush_hour_model->_status_active;
            $this->Rush_hour_model->_fk_agent_power_distributor = $current_unity['fk_agent_power_distributor'];
            $return                                             = $this->Rush_hour_model->fetch();
        }
        return $return;
    }

    private function _process_telemetry_records($date) {
        $this->load->model("Telemetry_record_model");
        $this->Telemetry_record_model->_status      = $this->Telemetry_record_model->_status_active;
        $this->Telemetry_record_model->_fk_gauge    = $this->Gauge_model->_pk_gauge;
        $this->Telemetry_record_model->_record_date = $date;
        $telemetry_records                          = $this->Telemetry_record_model->fetch();

        $return = array();

        $energy_types = array(
            'Eneat',
            'Enere',
            'Demat',
            'Demre',
            'DemConPta',
            'DemConfpta'
        );

        foreach ($energy_types as $type) {
            $return[$type]['records']                      = array();
            $return[$type]['total']['success']['value']    = 0;
            $return[$type]['total']['success']['quantity'] = 0;
            $return[$type]['total']['error']['value']      = 0;
            $return[$type]['total']['error']['quantity']   = 0;
        }

        if ($telemetry_records) {
            foreach ($telemetry_records as $telemetry_record) {
                foreach ($energy_types as $type) {
                    if ($telemetry_record['greatness'] == $type) {
                        $return[$type]['records'][]                 = $telemetry_record;
                        $controll                                   = $telemetry_record['quality'] == 0 ? 'success' : 'error';
                        $return[$type]['total'][$controll]['value'] += $telemetry_record['value'];
                        $return[$type]['total'][$controll]['quantity'] ++;
                    }
                }
            }
        }
        return $return;
    }

    //widgets
    private function _process_demanda_medida_ponta($demat, $rush_hour, $holiday) {
        $return = 0;
        foreach ($demat as $consumo) {
            if (date("N", strtotime($consumo['record_date'])) < 6 && $holiday == 0) {
                if (date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) >= $rush_hour[0]['start_hour'] && date("H:i:s", strtotime($consumo['record_date'])) <= $rush_hour[0]['end_hour']) {
                    $return = $consumo['value'] > $return ? $consumo['value'] : $return;
                }
            }
        }
        return $return;
    }

    private function _process_demanda_medida_fora_ponta($demat, $rush_hour, $holiday) {
        $return = 0;
        foreach ($demat as $consumo) {
            if ((date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) < $rush_hour[0]['start_hour'] || date("H:i:s", strtotime($consumo['record_date'])) > $rush_hour[0]['end_hour']) || (date("N", strtotime($consumo['record_date'])) >=6) || $holiday == 1) {
                $return = $consumo['value'] > $return ? $consumo['value'] : $return;
            }
        }
        return $return;
    }

    private function _process_limite_consumo($date) {
        $this->load->model("Unity_consumption_limit_model");
        $this->Unity_consumption_limit_model->_status   = $this->Unity_consumption_limit_model->_status_active;
        $this->Unity_consumption_limit_model->_fk_unity = $this->Unity_model->_pk_unity;
        $this->Unity_consumption_limit_model->_month    = date("m", strtotime($date));
        $this->Unity_consumption_limit_model->_year     = date("Y", strtotime($date));
        return $this->Unity_consumption_limit_model->fetch();
    }

    private function _process_fator_carga_ponta($demat, $rush_hour, $holiday) {
        $fator_carga_ponta = 0;
        $count             = 0;
        $return            = 0;
        foreach ($demat as $consumo) {
            if (date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) >= $rush_hour[0]['start_hour'] && date("H:i:s", strtotime($consumo['record_date'])) <= $rush_hour[0]['end_hour']) {
                if ($consumo['quality'] == 0) {
                    $fator_carga_ponta += $consumo['value'];
                    $count++;
                }
            }
        }

        if ($count > 0 && $fator_carga_ponta > 0 && $this->Demand_model->_end_demand > 0) {
            $return = ($fator_carga_ponta / $count ) / (($this->Demand_model->_end_demand ));
        }
        return $return;
    }

    private function _process_fator_carga_fora_ponta($demat, $rush_hour, $holiday) {
        $fator_carga_fora_ponta = 0;
        $count                  = 0;
        $return                 = 0;
        foreach ($demat as $consumo) {
            if (date("H:i:s", strtotime("+15 MINUTES", strtotime($consumo['record_date']))) < $rush_hour[0]['start_hour'] || date("H:i:s", strtotime($consumo['record_date'])) > $rush_hour[0]['end_hour']) {
                if ($consumo['quality'] == 0) {
                    $fator_carga_fora_ponta += $consumo['value'];
                    $count++;
                }
            }
        }
        if ($count > 0 && $fator_carga_fora_ponta > 0 && $this->Demand_model->_demand_tip_out > 0) {
            $return = ($fator_carga_fora_ponta / $count) / (($this->Demand_model->_demand_tip_out));
        }
        return $return;
    }

    private function _process_resumo_grafico_consumo_diario_mes($demat) {
        $consumo_MWh          = 0;
        $consumo_MWm          = 0;
        $horas_medidas        = 0;
        $horas_inconsistentes = 0;
        
        foreach ($demat as $consumo) {
            $consumo_milhar = $consumo['value'] > 0 ? ($consumo['value']/4) /1000 : 0;
                $consumo_MWh += $consumo_milhar;
                if ($consumo['quality'] == 0) {
                    $horas_medidas++;
                }
                else {
                    $horas_inconsistentes++;
                }
        }
        if ($consumo_MWh > 0 && $horas_medidas > 0) {
            $consumo_MWm = $consumo_MWh / ($horas_medidas/4);
        }
        return array(
            'consumo_MWh'          => $consumo_MWh,
            'consumo_MWm'          => $consumo_MWm,
            'horas_medidas'        => $horas_medidas/4,
            'horas_inconsistentes' => $horas_inconsistentes/4
        );
    }

    private function _process_holiday($date) {
        $this->load->model("Holiday_model");
        $this->Holiday_model->_date_holiday = $date;
        $this->Holiday_model->_status       = $this->Holiday_model->_status_active;
        return $this->Holiday_model->read();
    }

    //GRÁFICOS FORNECIDOS POR API
}
