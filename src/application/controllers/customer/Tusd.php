<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tusd extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Tusd_model');
    }

    function index() {
        $this->Tusd_model->_status = $this->Tusd_model->_status_active;
        $data['list']              = $this->Tusd_model->fetch_by_agent();
        $data['files']             = $this->_process_files($data['list']);
        $data['js_include']        = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']       = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']      = 'tusd/index';
        $this->load->view('customer/includes/template', $data);
    }

    private function _process_files($tusds) {
        $return = array();
        if ($tusds) {
            foreach ($tusds as $tusd) {
                $return[$tusd['pk_tusd']] = $this->_list_files_from_directory($tusd['pk_tusd']);
            }
        }
        return $return;
    }

    private function _list_files_from_directory($dir) {
        $return    = array();
        $this->load->helper('directory');
        $directory = './uploads/tusd/' . $dir . '/';
        $map       = directory_map($directory);
        if (!empty($map)) {
            foreach ($map as $file) {
                if (!is_array($file)) {
                    if (is_file($directory . $file)) {
                        $return[] = $file;
                    }
                }
            }
        }
        return $return;
    }

}
