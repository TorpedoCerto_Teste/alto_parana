<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccvee_general extends Customer_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Ccvee_general_model');
    }

    function index() {
        $this->Ccvee_general_model->_status   = $this->Ccvee_general_model->_status_active;
        $this->Ccvee_general_model->_fk_agent = $this->session->userdata('fk_agent');
        $data['list']                         = $this->Ccvee_general_model->fetch_by_agent_unity();
        $data['js_include']                   = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                  = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                 = 'ccvee_general/index';
        $this->load->view('customer/includes/template', $data);
    }

    function view() {
        $this->Ccvee_general_model->_pk_ccvee_general = $this->uri->segment(4, 0);
        $data['ccvee_general']                        = $this->Ccvee_general_model->read_by_customer();
        if (!$data['ccvee_general']) {
            redirect('customer/ccvee_general/index');
        }

        $this->_process_energy();
        $data['energy_bulks']    = $this->_process_energy_bulk();
        $this->_process_payment();
        $data['payment_unities'] = $this->_process_payment_unity();
        //pega a lista de unidades do contrato
        $data['unities']         = $this->_process_unity();
        $data['warranties']      = $this->_process_warranty();
        $this->_process_submarket($data['ccvee_general']['fk_submarket']);

        $data['main_content'] = 'ccvee_general/view';
        $this->load->view('customer/includes/template', $data);
    }

    private function _process_unity() {
        $this->load->model('Ccvee_unity_model');
        $this->Ccvee_unity_model->_status           = $this->Ccvee_unity_model->_status_active;
        $this->Ccvee_unity_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        return $this->Ccvee_unity_model->fetch();
    }

    private function _process_energy() {
        $this->load->model('Ccvee_energy_model');
        $this->Ccvee_energy_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $read_energy                                 = $this->Ccvee_energy_model->read();
    }

    private function _process_submarket($fk_submarket) {
        $this->load->model('Submarket_model');
        $this->Submarket_model->_pk_submarket = $fk_submarket;
        $this->Submarket_model->read();
    }

    private function _process_energy_bulk() {
        $return = false;
        if ($this->Ccvee_energy_model->_pk_ccvee_energy) {
            $this->load->model('Ccvee_energy_bulk_model');
            $this->Ccvee_energy_bulk_model->_status          = $this->Ccvee_energy_bulk_model->_status_active;
            $this->Ccvee_energy_bulk_model->_fk_ccvee_energy = $this->Ccvee_energy_model->_pk_ccvee_energy;
            return $this->Ccvee_energy_bulk_model->fetch();
        }
        return $return;
    }

    private function _process_payment() {
        $this->load->model('Ccvee_payment_model');
        $this->Ccvee_payment_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        $read_payment                                 = $this->Ccvee_payment_model->read_by_general();
    }

    private function _process_payment_unity() {
        $return = false;
        if ($this->Ccvee_payment_model->_pk_ccvee_payment) {
            $this->load->model('Ccvee_payment_price_model');
            $this->Ccvee_payment_price_model->_status           = $this->Ccvee_payment_price_model->_status_active;
            $this->Ccvee_payment_price_model->_fk_ccvee_payment = $this->Ccvee_payment_model->_pk_ccvee_payment;
            return $this->Ccvee_payment_price_model->fetch();
        }
        return $return;
    }

    private function _process_warranty() {
        $this->load->model('Ccvee_warranty_model');
        $this->Ccvee_warranty_model->_status           = $this->Ccvee_warranty_model->_status_active;
        $this->Ccvee_warranty_model->_fk_ccvee_general = $this->Ccvee_general_model->_pk_ccvee_general;
        return $this->Ccvee_warranty_model->fetch();
    }

}
