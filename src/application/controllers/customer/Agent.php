<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent extends Customer_Controller {

    function __construct() {
        parent::__construct();
        
        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }
        $this->load->model('Agent_model');        
    }

    function index() {
        $data['fk_agent_types'] = [];
        if ($this->input->post('filter') == 'true') {
            $data['fk_agent_types'] = $this->input->post('fk_agent_type');

            if (is_array($data['fk_agent_types']) && !empty($data['fk_agent_types'])) {
                $this->Agent_model->_fk_agent_type = $data['fk_agent_types'];
             } 
        }

        $this->load->model("Agent_type_model");
        $this->Agent_type_model->_status = $this->Agent_type_model->_status_active;
        $data['agent_types'] = $this->Agent_type_model->fetch();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>        
                <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
                <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
                <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
                <script src="' . base_url() . 'web/js/index_index.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
            <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">           
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">';

            
        $data['list']         = $this->_process_agent_list();  
        $data['main_content'] = 'agent/index';
        $this->load->view('customer/includes/template', $data);
    }

    private function _process_agent_list() {
        $return = [];
        $all_agents = $this->Agent_model->fetch(); 
        if ($all_agents && $this->agents_list) {
            foreach ($all_agents as $agent) {
                foreach ($this->agents_list as $ag) {
                    if ($ag['fk_agent'] === $agent['pk_agent']) {
                        $return[] = $agent;
                    }
                }
            }
        }
        return $return;
    }

}
