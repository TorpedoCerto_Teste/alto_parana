<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message_agent extends Customer_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }        

        $this->load->model('Message_agent_model');
    }

    function index() {
        $this->Message_agent_model->_limit = 0;
        $data['list']                      = $this->Message_agent_model->fetch_by_agent();
        
        $data['js_include']                = '
            <script src="'.base_url().'js/jquery.dataTables.min.js"></script>
            <script src="'.base_url().'js/dataTables.tableTools.min.js"></script>
            <script src="'.base_url().'js/bootstrap-dataTable.js"></script>
            <script src="'.base_url().'js/dataTables.colVis.min.js"></script>
            <script src="'.base_url().'js/dataTables.responsive.min.js"></script>
            <script src="'.base_url().'js/dataTables.scroller.min.js"></script>
            <script src="'.base_url().'web/js/index_index.js"></script>
            ';
        $data['css_include']               = '
            <link href="'.base_url().'css/jquery.dataTables.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.responsive.css" rel="stylesheet">
            <link href="'.base_url().'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']              = 'message_agent/index';
        $this->load->view('customer/includes/template', $data);
    }
    
    function view() {
        if ($this->uri->segment(4, 0) <= 0) {
            redirect('customer/message_agent');
        }
        $this->Message_agent_model->_pk_message_agent = $this->uri->segment(4);
        $this->Message_agent_model->_fk_agent         = $this->session->userdata('fk_agent');
        $read                                         = $this->Message_agent_model->read();
        if (!$read) {
            redirect('customer/message_agent');
        }
        
        //marca a mensagem como lida
        if ($this->Message_agent_model->_status != $this->Message_agent_model->_status_read) {
            $this->Message_agent_model->_status = $this->Message_agent_model->_status_read;
            $this->Message_agent_model->update();
        }
        
        //pega a mensagem
        $this->load->model('Message_model');
        $this->Message_model->_pk_message = $this->Message_agent_model->_fk_message;
        $this->Message_model->read();
        
        $data['js_include']   = '
            <script type="text/javascript" src="'.base_url().'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
            <script type="text/javascript" src="'.base_url().'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
            <script src="'.base_url().'web/js/message/create.js"></script>';
        $data['css_include']  = '    
            <link rel="stylesheet" type="text/css" href="'.base_url().'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />';
        $data['main_content']         = 'message_agent/view';
        $this->load->view('customer/includes/template', $data);
        
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Message_agent_model->_       = mb_strtoupper($this->input->post(''), 'UTF-8');
                $this->Message_agent_model->_status = $this->Message_agent_model->_status_active;
                $create                             = $this->Message_agent_model->create();
                if ($create) {
                    redirect('message_agent/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message_agent/create';
        $this->load->view('customer/includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message_agent/index');
        }
        $this->Message_agent_model->_pk_message_agent = $this->uri->segment(3);
        $read                                         = $this->Message_agent_model->read();
        if (!$read) {
            redirect('message_agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Message_agent_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $update                       = $this->Message_agent_model->update();
                redirect('message_agent/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message_agent/update';
        $this->load->view('customer/includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('message_agent/index');
        }
        $this->Message_agent_model->_pk_message_agent = $this->uri->segment(3);
        $read                                         = $this->Message_agent_model->read();
        if (!$read) {
            redirect('message_agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Message_agent_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $upsert                       = $this->Message_agent_model->upsert();
                redirect('message_agent/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'message_agent/update';
        $this->load->view('customer/includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_message_agent') > 0) {
            $this->Message_agent_model->_pk_message_agent = $this->input->post('pk_message_agent');
            $this->Message_agent_model->delete();
        }
        redirect('message_agent/index');
    }

}
