<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting extends Customer_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }

        $this->load->model('Accounting_model');
    }

    function index() {
        $this->Accounting_model->_status = $this->Accounting_model->_status_active;
        $this->Accounting_model->_fk_agent = $this->session->userdata('fk_agent');
        $data['list']                    = $this->Accounting_model->fetch_by_agent();
        $data['js_include']            = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/data-table-init.js"></script>
            ';
        $data['css_include']           = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']            = 'accounting/index';
        $this->load->view('customer/includes/template', $data);
    }
    
    function gfn001() {
        $this->Accounting_model->_status = $this->Accounting_model->_status_active;
        $this->Accounting_model->_fk_agent = $this->session->userdata('fk_agent');
        $data['list']                    = $this->Accounting_model->fetch_by_agent();
        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/data-table-init.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']            = 'accounting/gfn001';
        $this->load->view('customer/includes/template', $data);
        
    }


}
