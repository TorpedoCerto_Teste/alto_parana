<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lfpen001 extends Customer_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }

        $this->load->model('Lfpen001_model');
    }

    function index() {
        $this->Lfpen001_model->_status   = $this->Lfpen001_model->_status_active;
        $this->Lfpen001_model->_fk_agent = $this->session->userdata('fk_agent');
        $data['list']                    = $this->Lfpen001_model->fetch();
        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'js/data-table-init.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']            = 'lfpen001/index';
        $this->load->view('customer/includes/template', $data);
    }

}
