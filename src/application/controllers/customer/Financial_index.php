<?php

/**
  pk_financial_index
  type
  value
  date
  status
  created_at
  updated_at
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Financial_index extends Customer_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_contact')) {
            $this->session->set_userdata(array('url_return_customer' => $this->uri->uri_string()));
            redirect('customer/index/login');
        }

        $this->load->model('Financial_index_model');
    }

    function index() {

        $data['indices'] = $this->_process_index();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            <script src="' . base_url() . 'web/js/financial_index/index.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'financial_index/index';


        $this->load->view('customer/includes/template', $data);
    }

    function ajax() {
        $this->Financial_index_model->_date = $this->uri->segment(4) . "-" . $this->uri->segment(3) . "-01";
        $this->Financial_index_model->_type = $this->uri->segment(5);
        if ($this->Financial_index_model->find_by_date_and_type()) {
            print_r(number_format($this->Financial_index_model->_value, 3, ',', ''));
        }
    }

    private function _process_index() {
        $return                               = array();
        $this->Financial_index_model->_status = $this->Financial_index_model->_status_active;
        $data['list']                         = $this->Financial_index_model->fetch();

        if ($data['list']) {
            foreach ($data['list'] as $value) {
                $return[$value['date']]['IPCA'] = 0;
                $return[$value['date']]['IGPM'] = 0;
            }
        }

        if ($data['list']) {
            foreach ($data['list'] as $value) {
                $return[$value['date']][$value['type']] = $value['value'];
            }
        }

        return $return;
    }

}
