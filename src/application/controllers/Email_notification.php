<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_notification extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Email_notification_model');
    }

    function index() {
        $this->Email_notification_model->_status = $this->Email_notification_model->_status_active;
        $data['list']                            = $this->Email_notification_model->fetch();
        $data['js_include']                      = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                     = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                    = 'email_notification/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Email_notification_model->_created_at = date("Y-m-d H:i:s");
                $create                                      = $this->Email_notification_model->create();
                if ($create) {
                    redirect('email_notification/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'email_notification/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('email_notification/index');
        }
        $this->Email_notification_model->_pk_email_notification = $this->uri->segment(3);
        $read                                                   = $this->Email_notification_model->read();
        if (!$read) {
            redirect('email_notification/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Email_notification_model->update();
                redirect('email_notification/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'email_notification/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('email_notification/index');
        }
        $this->Email_notification_model->_pk_email_notification = $this->uri->segment(3);
        $read                                                   = $this->Email_notification_model->read();
        if (!$read) {
            redirect('email_notification/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Email_notification_model->upsert();
                redirect('email_notification/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'email_notification/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_email_notification') > 0) {
            $this->Email_notification_model->_pk_email_notification = $this->input->post('pk_email_notification');
            $this->Email_notification_model->delete();
        }
        redirect('email_notification/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('', '', 'trim|required');
    }

    private function _fill_model() {
        $this->Email_notification_model->_       = $this->input->post('');
        $this->Email_notification_model->_status = $this->Email_notification_model->_status_active;
    }

}
