<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Business extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Business_model');
    }

    function index() {
        $this->Business_model->_status = $this->Business_model->_status_active;
        $data['list']                  = $this->Business_model->fetch();
        $data['js_include']            = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']           = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']          = 'business/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Business_model->_pk_business = $this->Business_model->create();

                if ($this->Business_model->_pk_business) {
                    //inserir a unidade selecionada na lista de unidades deste negócio
                    $this->load->model('Business_unity_model');
                    $this->Business_unity_model->_fk_business = $this->Business_model->_pk_business;
                    $this->Business_unity_model->_fk_unity    = $this->Business_model->_fk_unity;
                    $this->Business_unity_model->_status      = $this->Business_unity_model->_status_active;
                    $this->Business_unity_model->create();

                    //criar uma regra de pagamento padrão
                    $this->load->model('Business_payment_model');
                    $this->Business_payment_model->_fk_business = $this->Business_model->_pk_business;
                    $this->Business_payment_model->_status      = $this->Business_payment_model->_status_active;
                    $this->Business_payment_model->create();

                    //salva o arquivo
                    $this->_process_files();

                    redirect('business/view/' . $this->Business_model->_pk_business);
                }
            }
            $data['error'] = true;
        }

        //listar as unidades
        $data['units'] = $this->_process_units();

        //listar os serviços
        $data['services'] = $this->_process_services();

        //listar os usuarios
        $data['users'] = $this->_process_users();

        //listar os agentes
        $data['agents'] = $this->_process_agents();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
                <script src="' . base_url() . 'web/js/business/create.js"></script>
                ';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
                <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />    
                ';
        $data['main_content'] = 'business/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('business/index');
        }
        $this->Business_model->_pk_business = $this->uri->segment(3);
        $read                               = $this->Business_model->read();
        if (!$read) {
            redirect('business/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Business_model->update();
                $this->_process_files();
            }
        }
        redirect('business/view/' . $this->Business_model->_pk_business);
    }

    function delete() {
        if ($this->input->post('pk_business') > 0) {
            $this->Business_model->_pk_business = $this->input->post('pk_business');
            $this->Business_model->delete();
        }
        redirect('business/index');
    }

    function view() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('business/index');
        }
        $this->Business_model->_pk_business = $this->uri->segment(3);
        $read                               = $this->Business_model->read();
        if (!$read) {
            redirect('business/index');
        }

        //<início negócio>
        //listar as unidades

        $data['units'] = $this->_process_units();

        //listar os usuarios
        $data['users'] = $this->_process_users();

        //listar os agentes representantes
        $data['agents'] = $this->_process_agents();

        //pega o agente da unidade
        $data['agent_by_unity'] = $this->_process_agent_by_unity();

        //<fim negócio>
        //
        //<início unidades>
        //pega as unidades deste contrato
        $this->load->model('Business_unity_model');
        $this->Business_unity_model->_status      = $this->Business_unity_model->_status_active;
        $this->Business_unity_model->_fk_business = $this->Business_model->_pk_business;
        $data['list_business_units']              = $this->Business_unity_model->fetch();

        //pega a lista de unidades, separado por agentes
        $data['units2']                             = $this->_process_business_unity($data['list_business_units'], $data['units']);
        //<fim unidades>
        //
        //<início pagamentos>
        $this->load->model('Business_payment_model');
        $this->Business_payment_model->_fk_business = $this->Business_model->_pk_business;
        $read                                       = $this->Business_payment_model->read();

        $data['unities'] = $data['list_business_units'];
        if ($data['unities']) {
            //processa os valores "mensal por unidade"
            $data['monthly_unities'] = $this->_proccess_monthly_unities($data['unities']);

            //processa os valores "valor fixo mais percentual de economia"
            $data['payment_fixed_value_unities'] = $this->_proccess_payment_fixed_value_unities($data['unities']);
        }
        //<fim pagamentos>
        //
        //
        //<início servicos>
        //pega a lista de serviços
        $data['services'] = $this->_process_services();

        //verifica se um serviço já foi salvo anteriormente
        $data['business_service']          = $this->_process_business_service();
        $data['business_service_products'] = array();
        if ($data['business_service']) {
            //pega a lista de business_service_products (no caso de update)
            $data['business_service_products'] = $this->_process_business_service_products();
        }

        //pega a lista de produtos (para listar os checkboxes)
        $data['products'] = $this->_process_product();

        //<fim servicos>
        //
        //<início faturamentos>
        $this->load->model('Business_billing_model');
        $this->Business_billing_model->_status      = $this->Business_billing_model->_status_active;
        $this->Business_billing_model->_fk_business = $this->Business_model->_pk_business;
        $data['list_business_billing']              = $this->Business_billing_model->fetch();

        //<fim faturamentos>

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>           
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
                <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
                <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
                <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
                <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
                <script src="' . base_url() . 'web/js/business/view.js?'. microtime().'"></script>
                ';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
                <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
                <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />    
                <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
                <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
                ';
        $data['main_content'] = 'business/view';
        $this->load->view('includes/template', $data);
    }

    private function _process_units() {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();
        $option                     = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_company_name']][$unity['pk_unity']] = $unity;
            }
        }
        return $option;
    }

    private function _process_users() {
        $this->load->model("User_model");
        $this->User_model->_status = $this->User_model->_status_active;
        return $this->User_model->fetch();
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status        = $this->Agent_model->_status_active;
        $this->Agent_model->_fk_agent_type = $this->config->item("pk_agent_type_representante");
        $agents                            = $this->Agent_model->fetch();
        $option                            = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_agent_by_unity() {
        $this->load->model("Unity_model");
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $this->Unity_model->_pk_unity = $this->Business_model->_fk_unity;
        $unity                        = $this->Unity_model->read();
    }

    private function _process_business_unity($list_business_units, $units_by_agent) {
        if ($list_business_units) {
            foreach ($units_by_agent as $agent => $units) {
                foreach ($units as $k => $unit) {
                    foreach ($list_business_units as $business_unit) {
                        if ($business_unit['fk_unity'] == $unit['pk_unity']) {
                            unset($units_by_agent[$agent][$k]);
                        }
                    }
                }
            }
        }
        return $units_by_agent;
    }

    private function _proccess_monthly_unities($unities) {
        $return = array();
        foreach ($unities as $unity) {

            $return[$unity['fk_unity']]['fk_business_payment'] = $this->Business_payment_model->_pk_business_payment;
            $return[$unity['fk_unity']]['community_name']      = $unity['community_name'];
            $return[$unity['fk_unity']]['monthly_value']       = 0.0;
        }

        //pega a lista de Business_payment_monthly_unity
        $this->load->model("Business_payment_monthly_unity_model");
        $this->Business_payment_monthly_unity_model->_status              = $this->Business_payment_monthly_unity_model->_status_active;
        $this->Business_payment_monthly_unity_model->_fk_business_payment = $this->Business_payment_model->_pk_business_payment;
        $monthly_unities                                                  = $this->Business_payment_monthly_unity_model->fetch();

        if ($monthly_unities) {
            foreach ($monthly_unities as $monthly_unity) {
                $return[$monthly_unity['fk_unity']]['monthly_value'] = $monthly_unity['monthly_value'];
            }
        }

        return $return;
    }

    private function _proccess_payment_fixed_value_unities($unities) {
        $return = array();
        foreach ($unities as $unity) {
            $return[$unity['fk_unity']]['fk_business_payment'] = $this->Business_payment_model->_pk_business_payment;
            $return[$unity['fk_unity']]['community_name']      = $unity['community_name'];
            $return[$unity['fk_unity']]['fixed_value']         = 0.0;
            $return[$unity['fk_unity']]['percent']             = 0.0;
            $return[$unity['fk_unity']]['roof']                = 0.0;
            $return[$unity['fk_unity']]['floor']               = 0.0;
        }

        $this->load->model("Business_payment_fixed_value_unity_model");
        $this->Business_payment_fixed_value_unity_model->_status              = $this->Business_payment_fixed_value_unity_model->_status_active;
        $this->Business_payment_fixed_value_unity_model->_fk_business_payment = $this->Business_payment_model->_pk_business_payment;
        $payment_fixed_value_unities                                          = $this->Business_payment_fixed_value_unity_model->fetch();

        if ($payment_fixed_value_unities) {
            foreach ($payment_fixed_value_unities as $payment_fixed_value_unity) {
                $return[$payment_fixed_value_unity['fk_unity']]['fixed_value'] = $payment_fixed_value_unity['fixed_value'];
                $return[$payment_fixed_value_unity['fk_unity']]['percent']     = $payment_fixed_value_unity['percent'];
                $return[$payment_fixed_value_unity['fk_unity']]['roof']        = $payment_fixed_value_unity['roof'];
                $return[$payment_fixed_value_unity['fk_unity']]['floor']       = $payment_fixed_value_unity['floor'];
            }
        }
        return $return;
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/business/' . $this->Business_model->_pk_business;
        $config['allowed_types'] = '*';
        $config['max_size']      = 80000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('business_file')) {
            $this->Business_model->_business_file = str_replace(" ", "_", $_FILES['business_file']['name']);
            $this->Business_model->update();
        }
    }

    function ajax_process_unity_differentiated_duration() {
        if ($this->input->post("pk_business")) {
            $this->Business_model->_pk_business = $this->input->post("pk_business");
            $read                               = $this->Business_model->read();
            if ($read) {
                $this->Business_model->_unity_differentiated_duration = $this->input->post("unity_differentiated_duration");
                $this->Business_model->update();
            }
        }
    }

    //métodos para business_services
    private function _process_services() {
        $this->load->model('Service_model');
        $this->Service_model->_status = $this->Service_model->_status_active;
        return $this->Service_model->fetch();
    }

    private function _process_business_service() {
        $this->load->model("Business_service_model");
        $this->Business_service_model->_fk_business = $this->Business_model->_pk_business;
        $this->Business_service_model->_status      = $this->Business_service_model->_status_active;
        return $this->Business_service_model->read();
    }

    private function _process_product() {
        $this->load->model("Product_model");
        $this->Product_model->_status = $this->Product_model->_status_active;
        $products                     = $this->Product_model->fetch();

        $return = array();
        if ($products) {
            foreach ($products as $product) {
                $return[$product['category']][$product['fk_product_category']][] = $product;
            }
        }
        return $return;
    }

    private function _process_business_service_products() {
        $this->load->model("Business_service_product_model");
        $this->Business_service_product_model->_fk_business_service = $this->Business_service_model->_pk_business_service;
        $this->Business_service_product_model->_status              = $this->Business_service_product_model->_status_active;
        return $this->Business_service_product_model->fetch();
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('fk_user', 'fk_user', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim');
        $this->form_validation->set_rules('start_date', 'start_date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'end_date', 'trim|required');
        $this->form_validation->set_rules('automatic_renovation', 'automatic_renovation', 'trim');
        $this->form_validation->set_rules('memo', 'memo', 'trim');
    }

    private function _fill_model() {
        $this->Business_model->_fk_unity                      = $this->input->post('fk_unity');
        $this->Business_model->_fk_user                       = $this->input->post('fk_user');
        $this->Business_model->_fk_agent                      = $this->input->post('fk_agent');
        $this->Business_model->_start_date                    = human_date_to_date($this->input->post('start_date'));
        $this->Business_model->_end_date                      = human_date_to_date($this->input->post('end_date'));
        $this->Business_model->_automatic_renovation          = $this->input->post('automatic_renovation') == 1 ? 1 : 0;
        $this->Business_model->_memo                          = $this->input->post('memo');
        $this->Business_model->_unity_differentiated_duration = 0;
        $this->Business_model->_status                        = $this->Business_model->_status_active;
    }

}
