<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Demand extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        if ($this->router->fetch_method() != "ajax_find_unity") {
            $this->load->model("Agent_model");
            $this->Agent_model->_pk_agent = $this->uri->segment(3, 0);
            $read                         = $this->Agent_model->read();
            if (!$read) {
                redirect('agent/index');
            }

            $this->load->model("Unity_model");
            $this->Unity_model->_pk_unity = $this->uri->segment(4, 0);
            $this->Unity_model->_fk_agent = $this->Agent_model->_pk_agent;
            $read                         = $this->Unity_model->read();
            if (!$read) {
                redirect('agent/index');
            }
        }


        $this->load->model('Demand_model');
    }

    function index() {
        //listagem
        $this->load->model('Demand_model');
        $this->Demand_model->_fk_unity = $this->Unity_model->_pk_unity;
        $this->Demand_model->_status   = $this->Demand_model->_status_active;
        $data['list']                  = $this->Demand_model->fetch();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content'] = 'demand/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create_demand') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Demand_model->create();
                if ($create) {
                    if ($this->Demand_model->_current) {
                        $this->Demand_model->change_old_current();
                    }
                    redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/demanda');
                }
                $data['error'] = true;
            }
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
            <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
            <script src="' . base_url() . 'web/js/demand/index.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />  ';
        $data['main_content'] = 'demand/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;

        $this->Demand_model->_pk_demand = $this->uri->segment(5, 0);
        $this->Demand_model->_fk_unity  = $this->Unity_model->_pk_unity;
        $read                           = $this->Demand_model->read();
        $_current                       = $this->Demand_model->_current;
        if (!$read) {
            redirect('agent/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                
                $update = $this->Demand_model->update();
                if ($update) {
//                    if ($this->Demand_model->_current != $_current) {
//                        $this->Demand_model->change_old_current();
//                    }
                    redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/demanda');
                }
                $data['error'] = true;
            }
            $data['error'] = true;
        }

        $data['js_include']   = '
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
            <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
            <script src="' . base_url() . 'web/js/demand/index.js"></script>';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />  ';
        $data['main_content'] = 'demand/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_demand') > 0) {
            $this->Demand_model->_pk_demand = $this->input->post('pk_demand');
            $this->Demand_model->delete();
        }
        redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/demanda');
    }

    function status() {
        $this->Demand_model->_pk_demand = $this->uri->segment(5, 0);
        $this->Demand_model->_fk_unity  = $this->Unity_model->_pk_unity;
        $read                           = $this->Demand_model->read();
        if (!$read) {
            redirect('agent/index');
        }

        $this->Demand_model->_status = $this->Demand_model->_status == $this->Demand_model->_status_active ? $this->Demand_model->_status_inactive : $this->Demand_model->_status_active;
        $this->Demand_model->update();
        redirect('unity/view/' . $this->Agent_model->_pk_agent . '/' . $this->Unity_model->_pk_unity . '/demanda');
    }

    function ajax_find_unity() {
        header('Content-type: application/json');
        $this->disableLayout             = TRUE;
        $this->Demand_model->_status     = $this->Demand_model->_status_active;
        $this->Demand_model->_fk_unity   = $this->input->post("fk_unity");
        $this->Demand_model->_start_date = $this->input->post("year") . "-" . $this->input->post("month") . "-01";
        $this->Demand_model->_end_date   = $this->Demand_model->_start_date;
        $read                            = $this->Demand_model->read_by_unity_and_date();
        $read ? print_r(json_encode($this->Demand_model)) : print(json_encode($read));
    }

    private function _validate_form() {
        $this->form_validation->set_rules('start_date', 'start_date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'end_date', 'trim');
        $this->form_validation->set_rules('tension', 'tension', 'trim');
        $this->form_validation->set_rules('tariff_mode', 'tariff_mode', 'trim');
        $this->form_validation->set_rules('end_demand', 'end_demand', 'trim');
        $this->form_validation->set_rules('demand_tip_out', 'demand_tip_out', 'trim');
        $this->form_validation->set_rules('captive_situation', 'captive_situation', 'trim');
        $this->form_validation->set_rules('current', 'current', 'trim');
        $this->form_validation->set_rules('edge_generators', 'edge_generators', 'trim');
        $this->form_validation->set_rules('price_kwh', 'price_kwh', 'trim');
        $this->form_validation->set_rules('not_consumed_at_the_tip', 'not_consumed_at_the_tip', 'trim');
    }

    private function _fill_model() {
        $this->Demand_model->_fk_unity                = $this->Unity_model->_pk_unity;
        $this->Demand_model->_start_date              = human_date_to_date($this->input->post('start_date'));
        $this->Demand_model->_end_date                = $this->input->post('current') <= 0 ? human_date_to_date($this->input->post('end_date')) : NULL;
        $this->Demand_model->_tension                 = mb_strtoupper($this->input->post('tension'), 'UTF-8');
        $this->Demand_model->_tariff_mode             = mb_strtoupper($this->input->post('tariff_mode'), 'UTF-8');
        $this->Demand_model->_end_demand              = $this->input->post('end_demand');
        $this->Demand_model->_demand_tip_out          = $this->input->post('demand_tip_out');
        $this->Demand_model->_captive_situation       = $this->input->post('captive_situation') == 1 ? 1 : 0;
        $this->Demand_model->_current                 = $this->input->post('current') == 1 ? 1 : 0;
        $this->Demand_model->_edge_generators         = $this->input->post('edge_generators') == 1 ? 1 : 0;
        $this->Demand_model->_price_kwh               = number_format($this->input->post('price_kwh'), 2, '.', '');
        $this->Demand_model->_not_consumed_at_the_tip = $this->input->post('not_consumed_at_the_tip') == 1 ? 1 : 0;
        $this->Demand_model->_status                  = $this->Demand_model->_status_active;
        $this->Demand_model->_created_at              = date("Y-m-d H:i:s");
    }

}
