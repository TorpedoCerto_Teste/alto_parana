<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_product extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Service_product_model');
    }

    function index() {
        $this->Service_product_model->_status = $this->Service_product_model->_status_active;
        $data['list']                         = $this->Service_product_model->fetch();
        $data['js_include']                   = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                  = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                 = 'service_product/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Service_product_model->_       = mb_strtoupper($this->input->post(''), 'UTF-8');
                $this->Service_product_model->_status = $this->Service_product_model->_status_active;
                $create                               = $this->Service_product_model->create();
                if ($create) {
                    redirect('service_product/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'service_product/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('service_product/index');
        }
        $this->Service_product_model->_pk_service_product = $this->uri->segment(3);
        $read                                             = $this->Service_product_model->read();
        if (!$read) {
            redirect('service_product/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Service_product_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $update                         = $this->Service_product_model->update();
                redirect('service_product/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'service_product/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('service_product/index');
        }
        $this->Service_product_model->_pk_service_product = $this->uri->segment(3);
        $read                                             = $this->Service_product_model->read();
        if (!$read) {
            redirect('service_product/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('', '', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Service_product_model->_ = mb_strtoupper($this->input->post(''), 'UTF-8');
                $upsert                         = $this->Service_product_model->upsert();
                redirect('service_product/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'service_product/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_service_product') > 0) {
            $this->Service_product_model->_pk_service_product = $this->input->post('pk_service_product');
            $this->Service_product_model->delete();
        }
        redirect('service_product/index');
    }

    function ajax_get_service_product() {
        header('Content-type: application/json');
        $this->disableLayout = TRUE;
        
        $this->Service_product_model->_fk_service = $this->input->post('fk_service');
        $this->Service_product_model->_status     = $this->Service_product_model->_status_active;
        $service_products                         = $this->Service_product_model->fetch();
        print_r(json_encode($service_products));
    }

}
