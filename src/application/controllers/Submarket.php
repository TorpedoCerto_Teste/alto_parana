<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Submarket extends Public_Controller {

    function __construct() {
        parent::__construct();

        //verificar session
        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Submarket_model');
    }

    function index() {
        $this->Submarket_model->_status = $this->Submarket_model->_status_active;
        $data['list']                   = $this->Submarket_model->fetch();
        $data['js_include']             = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']            = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']           = 'submarket/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Submarket_model->create();
                if ($create) {
                    redirect('submarket/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'submarket/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('submarket/index');
        }
        $this->Submarket_model->_pk_submarket = $this->uri->segment(3);
        $read                                 = $this->Submarket_model->read();
        if (!$read) {
            redirect('submarket/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Submarket_model->update();
                redirect('submarket/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'submarket/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_submarket') > 0) {
            $this->Submarket_model->_pk_submarket = $this->input->post('pk_submarket');
            $this->Submarket_model->delete();
        }
        redirect('submarket/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('initials', 'Iniciais', 'trim|required');
        $this->form_validation->set_rules('submarket', 'Submercado', 'trim|required');
    }

    private function _fill_model() {
        $this->Submarket_model->_initials  = mb_strtoupper($this->input->post('initials'), 'UTF-8');
        $this->Submarket_model->_submarket = mb_strtoupper($this->input->post('submarket'), 'UTF-8');
    }

}
