<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Return_investment_migration extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Return_investment_migration_model');
    }

    function index() {
        $this->Return_investment_migration_model->_status = $this->Return_investment_migration_model->_status_active;
        $data['list']                                     = $this->Return_investment_migration_model->fetch();
        $data['js_include']                               = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                              = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                             = 'return_investment_migration/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Return_investment_migration_model->_created_at = date("Y-m-d H:i:s");
                $create                                               = $this->Return_investment_migration_model->create();
                if ($create) {
                    redirect('return_investment_migration/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'return_investment_migration/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('return_investment_migration/index');
        }
        $this->Return_investment_migration_model->_pk_return_investment_migration = $this->uri->segment(3);
        $read                                                                     = $this->Return_investment_migration_model->read();
        if (!$read) {
            redirect('return_investment_migration/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Return_investment_migration_model->update();
                redirect('return_investment_migration/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'return_investment_migration/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('return_investment_migration/index');
        }
        $this->Return_investment_migration_model->_pk_return_investment_migration = $this->uri->segment(3);
        $read                                                                     = $this->Return_investment_migration_model->read();
        if (!$read) {
            redirect('return_investment_migration/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Return_investment_migration_model->upsert();
                redirect('return_investment_migration/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'return_investment_migration/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_return_investment_migration') > 0) {
            $this->Return_investment_migration_model->_pk_return_investment_migration = $this->input->post('pk_return_investment_migration');
            $this->Return_investment_migration_model->delete();
        }
        redirect('return_investment_migration/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('return_investment_migration_file', 'return_investment_migration_file', 'trim|required');
    }

    private function _fill_model() {
        $this->Return_investment_migration_model->_month                            = $this->input->post('month');
        $this->Return_investment_migration_model->_year                             = $this->input->post('year');
        $this->Return_investment_migration_model->_fk_agent                         = $this->input->post('fk_agent');
        $this->Return_investment_migration_model->_return_investment_migration_file = $this->input->post('return_investment_migration_file');
        $this->Return_investment_migration_model->_status                           = $this->Return_investment_migration_model->_status_active;
    }

}
