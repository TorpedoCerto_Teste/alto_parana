<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Energy_closing extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Energy_closing_model');
    }

    function index() {
        redirect('energy_closing/upsert');
        $this->Energy_closing_model->_status = $this->Energy_closing_model->_status_active;
        $data['list']                        = $this->Energy_closing_model->fetch();
        $data['js_include']                  = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                 = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                = 'energy_closing/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Energy_closing_model->_created_at = date("Y-m-d H:i:s");
                $create                                  = $this->Energy_closing_model->create();
                if ($create) {
                    redirect('energy_closing/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_closing/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('energy_closing/index');
        }
        $this->Energy_closing_model->_pk_energy_closing = $this->uri->segment(3);
        $read                                           = $this->Energy_closing_model->read();
        if (!$read) {
            redirect('energy_closing/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Energy_closing_model->update();
                redirect('energy_closing/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_closing/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error']         = false;
        $data['success']       = false;
        $data['unities']       = false;
        $data['free_captives'] = false;

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Energy_closing_model->_pk_energy_closing = $this->Energy_closing_model->upsert();
                redirect('energy_closing/index');
            }
            $data['error'] = true;
        }
        elseif ($this->input->post('upsert') == 'false') {
            $this->Energy_closing_model->_fk_agent = $this->input->post("fk_agent");
            $this->Energy_closing_model->_month    = $this->input->post("month");
            $this->Energy_closing_model->_year     = $this->input->post("year");
            $data["free_captives"]                 = $this->Energy_closing_model->read();
            $data['unities']                       = $this->_process_unities();
//            easyPrint($data['unities'], 1);
        }

        //pega a lista de agentes
        $data['agents'] = $this->_process_agents();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>    
                <script src="' . base_url() . 'web/js/energy_closing/upsert.js"></script>';
        $data['css_include']  = '
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'energy_closing/upsert';
        $this->load->view('includes/template', $data);
    }

    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_fk_agent = $this->Energy_closing_model->_fk_agent;
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $unities                      = $this->Unity_model->fetch();

        if ($unities) {
            foreach ($unities as $k => $unity) {
                $unities[$k]["fk_energy_closing"]         = false;
                $unities[$k]["consumption"]               = false;
                $unities[$k]["loosing"]                   = false;
                $unities[$k]["proinfa"]                   = false;
                $unities[$k]["total"]                     = false;
                $unities[$k]["acl_average"]               = false;
                $unities[$k]["energy_closing_unity_file"] = false;
            }
            $unities = $this->_process_energy_closing_unity($unities);
        }
        return $unities;
    }

    private function _process_energy_closing_unity($unities) {
        $this->load->model("Energy_closing_unity_model");
        $this->Energy_closing_unity_model->_fk_energy_closing = $this->Energy_closing_model->_pk_energy_closing;
        $this->Energy_closing_unity_model->_status            = $this->Energy_closing_unity_model->_status_active;
        $energy_closing_unity                                 = $this->Energy_closing_unity_model->fetch();

        if ($energy_closing_unity) {
            foreach ($unities as $k => $unity) {
                foreach ($energy_closing_unity as $pu) {
                    if ($unity['pk_unity'] == $pu['fk_unity']) {
                        $unities[$k]["fk_energy_closing"]         = $pu["fk_energy_closing"];
                        $unities[$k]["consumption"]               = $pu["consumption"];
                        $unities[$k]["loosing"]                   = $pu["loosing"];
                        $unities[$k]["proinfa"]                   = $pu["proinfa"];
                        $unities[$k]["total"]                     = $pu["total"];
                        $unities[$k]["acl_average"]               = $pu["acl_average"];
                        $unities[$k]["energy_closing_unity_file"] = $pu["energy_closing_unity_file"];
                    }
                }
            }
        }
        return $unities;
    }

    function delete() {
        if ($this->input->post('pk_energy_closing') > 0) {
            $this->Energy_closing_model->_pk_energy_closing = $this->input->post('pk_energy_closing');
            $this->Energy_closing_model->delete();
        }
        redirect('energy_closing/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
    }

    private function _fill_model() {
        $this->Energy_closing_model->_month      = $this->input->post('month');
        $this->Energy_closing_model->_year       = $this->input->post('year');
        $this->Energy_closing_model->_fk_agent   = $this->input->post('fk_agent');
//        $this->Energy_closing_model->_energy_closing_file = $this->input->post('energy_closing_file');
        $this->Energy_closing_model->_status     = $this->Energy_closing_model->_status_active;
        $this->Energy_closing_model->_created_at = date("Y-m-d H:i:s");
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/energy_closing/' . $this->Energy_closing_model->_pk_energy_closing;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('energy_closing_file')) {
            $this->Energy_closing_model->_energy_closing_file = str_replace(" ", "_", $_FILES["energy_closing_file"]['name']);
            $this->Energy_closing_model->update();
        }
    }

}
