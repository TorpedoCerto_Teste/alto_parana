<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_slip_ccee_market extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Payment_slip_ccee_market_model');
    }

    function index() {
        $data['list'] = $this->_process_payment_slip_ccee_market();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/payment_slip_ccee_market/index.js"></script>            
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            ';
        $data['main_content'] = 'payment_slip_ccee_market/index';
        $this->load->view('includes/template', $data);
    }

    private function _process_payment_slip_ccee_market() {
        $this->Payment_slip_ccee_market_model->_year = date("Y");
        if ($this->input->post('year')) {
            $this->Payment_slip_ccee_market_model->_year = $this->input->post('year');
        }
        $this->Payment_slip_ccee_market_model->_status = $this->Payment_slip_ccee_market_model->_status_active;
        $payment_slip_ccee_market_list                 = $this->Payment_slip_ccee_market_model->fetch();

        //gera os meses
        $return = array();
        for ($i = 1; $i <= 12; $i++) {
            $return[$i]['pk_payment_slip_ccee_market'] = 0;
            $return[$i]['month']                       = retorna_mes($i);
            $return[$i]['year']                        = $this->Payment_slip_ccee_market_model->_year;
            $return[$i]['payment_slip_date']           = "";
            $return[$i]['validate']                    = "";
            $return[$i]['total_market']                = "";
        }

        if ($payment_slip_ccee_market_list) {
            foreach ($payment_slip_ccee_market_list as $list) {
                $return[$list['month']]['pk_payment_slip_ccee_market'] = $list['pk_payment_slip_ccee_market'];
                $return[$list['month']]['payment_slip_date']           = date_to_human_date($list['payment_slip_date']);
                $return[$list['month']]['validate']                    = date_to_human_date($list['validate']);
                $return[$list['month']]['total_market']                = str_replace(",", ".", $list['total_market']);
            }
        }
        return $return;
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('payment_slip_ccee_market/index');
        }
        $this->Payment_slip_ccee_market_model->_pk_payment_slip_ccee_market = $this->uri->segment(3);
        $read                                                               = $this->Payment_slip_ccee_market_model->read();
        if (!$read) {
            redirect('payment_slip_ccee_market/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Payment_slip_ccee_market_model->upsert();
                redirect('payment_slip_ccee_market/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'payment_slip_ccee_market/update';
        $this->load->view('includes/template', $data);
    }
    
    public function ajax_upsert_payment_slip_ccee_market() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');

        $this->Payment_slip_ccee_market_model->_month = $this->input->post('month');
        $this->Payment_slip_ccee_market_model->_year  = $this->input->post('year');

        switch ($this->input->post('field')) {
            case 'payment_slip_date':
                $this->Payment_slip_ccee_market_model->_payment_slip_date = human_date_to_date($this->input->post('value'), 1);
                break;
            case 'validate':
                $this->Payment_slip_ccee_market_model->_validate = human_date_to_date($this->input->post('value'), 1);
                break;
            case 'total_market':
                $this->Payment_slip_ccee_market_model->_total_market = str_replace(",", ".",$this->input->post('value'));
                break;
        }
        $this->Payment_slip_ccee_market_model->_created_at = date("Y-m-d H:m:s");
        
        $this->Payment_slip_ccee_market_model->upsert();
        
    }
    

    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('payment_slip_date', 'payment_slip_date', 'trim|required');
        $this->form_validation->set_rules('validate', 'validate', 'trim|required');
        $this->form_validation->set_rules('total_market', 'total_market', 'trim|required');
    }

    private function _fill_model() {
        $this->Payment_slip_ccee_market_model->_month             = $this->input->post('month');
        $this->Payment_slip_ccee_market_model->_year              = $this->input->post('year');
        $this->Payment_slip_ccee_market_model->_payment_slip_date = human_date_to_date($this->input->post('payment_slip_date'));
        $this->Payment_slip_ccee_market_model->_validate          = human_date_to_date($this->input->post('validate'));
        $this->Payment_slip_ccee_market_model->_total_market      = str_replace(",", ".", $this->input->post('total_market'));
        $this->Payment_slip_ccee_market_model->_status            = $this->Payment_slip_ccee_market_model->_status_active;
    }

}
