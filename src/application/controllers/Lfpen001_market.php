<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lfpen001_market extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Lfpen001_market_model');
    }

    function index() {
        $data['list'] = $this->_process_lfpen001_market();
        
        $data['js_include']   = '
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/lfpen001_market/index.js"></script>            
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            ';
        $data['main_content']                 = 'lfpen001_market/index';
        $this->load->view('includes/template', $data);
    }

    private function _process_lfpen001_market() {
        $this->Lfpen001_market_model->_year = date("Y");
        if ($this->input->post('year')) {
            $this->Lfpen001_market_model->_year = $this->input->post('year');
        }
        $this->Lfpen001_market_model->_status = $this->Lfpen001_market_model->_status_active;
        $lfpen001_market_list                 = $this->Lfpen001_market_model->fetch();

        //gera os meses
        $return = array();
        for ($i = 1; $i <= 12; $i++) {
            $return[$i]['pk_lfpen001_market'] = 0;
            $return[$i]['month']            = retorna_mes($i);
            $return[$i]['year']             = $this->Lfpen001_market_model->_year;
            $return[$i]['reporting_date']   = "";
            $return[$i]['payment_date']     = "";
        }

        if ($lfpen001_market_list) {
            foreach ($lfpen001_market_list as $list) {
                $return[$list['month']]['pk_lfpen001_market'] = $list['pk_lfpen001_market'];
                $return[$list['month']]['reporting_date']   = date_to_human_date($list['reporting_date']);
                $return[$list['month']]['payment_date']   = date_to_human_date($list['payment_date']);
            }
        }
        return $return;
    }

    function ajax_upsert_lfpen001_market() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');

        $this->Lfpen001_market_model->_month = $this->input->post('month');
        $this->Lfpen001_market_model->_year  = $this->input->post('year');
        
        switch ($this->input->post('field')) {
            case 'reporting_date':
                $this->Lfpen001_market_model->_reporting_date = human_date_to_date($this->input->post('value'), 1);
                break;
            case 'payment_date':
                $this->Lfpen001_market_model->_payment_date = human_date_to_date($this->input->post('value'), 1);
                break;
        }
        $this->Lfpen001_market_model->_created_at = date("Y-m-d H:m:s");
        
        $this->Lfpen001_market_model->upsert();
    }
    
    
    private function _validate_form() {
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('reporting_date', 'reporting_date', 'trim|required');
        $this->form_validation->set_rules('payment_date', 'payment_date', 'trim|required');
    }

    private function _fill_model() {
        $this->Lfpen001_market_model->_month          = $this->input->post('month');
        $this->Lfpen001_market_model->_year           = $this->input->post('year');
        $this->Lfpen001_market_model->_reporting_date = human_date_to_date($this->input->post('reporting_date'));
        $this->Lfpen001_market_model->_payment_date   = human_date_to_date($this->input->post('payment_date'));
        $this->Lfpen001_market_model->_status         = $this->Lfpen001_market_model->_status_active;
    }

}
