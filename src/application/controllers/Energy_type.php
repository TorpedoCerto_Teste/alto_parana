<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Energy_type extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Energy_type_model');
    }

    function index() {
        $this->Energy_type_model->_status = $this->Energy_type_model->_status_active;
        $data['list']                     = $this->Energy_type_model->fetch();
        $data['js_include']               = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/index_index.js"></script>
            ';
        $data['css_include']              = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']             = 'energy_type/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Energy_type_model->create();
                if ($create) {
                    redirect('energy_type/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '<script src="' . base_url() . 'js/mascara.js"></script>';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_type/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('energy_type/index');
        }
        $this->Energy_type_model->_pk_energy_type = $this->uri->segment(3);
        $read                                     = $this->Energy_type_model->read();
        if (!$read) {
            redirect('energy_type/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Energy_type_model->update();
                redirect('energy_type/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'energy_type/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_energy_type') > 0) {
            $this->Energy_type_model->_pk_energy_type = $this->input->post('pk_energy_type');
            $this->Energy_type_model->delete();
        }
        redirect('energy_type/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('source', 'source', 'trim|required');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('discount', 'discount', 'trim|required');
    }

    private function _fill_model() {
        $this->Energy_type_model->_source   = mb_strtoupper($this->input->post('source'), 'UTF-8');
        $this->Energy_type_model->_name     = mb_strtoupper($this->input->post('name'), 'UTF-8');
        $this->Energy_type_model->_discount = number_format($this->input->post('discount'), 2, '.', '');
        $this->Energy_type_model->_status   = $this->Energy_type_model->_status_active;
    }

}
