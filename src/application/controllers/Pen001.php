<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pen001 extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Pen001_model');
    }

    function index() {
        redirect('pen001/upsert');
        $this->Pen001_model->_status = $this->Pen001_model->_status_active;
        $data['list']                = $this->Pen001_model->fetch();
        $data['js_include']          = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']         = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']        = 'pen001/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Pen001_model->_created_at = date("Y-m-d H:i:s");
                $create                          = $this->Pen001_model->create();
                if ($create) {
                    redirect('pen001/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'pen001/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('pen001/index');
        }
        $this->Pen001_model->_pk_pen001 = $this->uri->segment(3);
        $read                           = $this->Pen001_model->read();
        if (!$read) {
            redirect('pen001/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Pen001_model->update();
                redirect('pen001/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'pen001/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $this->load->model('Pen001_market_model');
        $data['unities'] = false;
        $data['error']   = false;
        $data['success'] = false;

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Pen001_model->_pk_pen001 = $this->Pen001_model->upsert();
                if ($this->Pen001_model->_pk_pen001) {
                    $this->_create_pen001_unity();
                    $uploads = $this->_process_files();
                }
                $data['success'] = true;
            }
            else {
                $data['error'] = true;
            }
        }
        elseif ($this->input->post('upsert') == 'false') {
            $this->Pen001_model->_fk_agent = $this->input->post("fk_agent");
            $this->Pen001_model->_month    = $this->input->post("month");
            $this->Pen001_model->_year     = $this->input->post("year");
            $this->Pen001_model->read();

            $data['unities'] = $this->_process_unities();
            $this->_process_pen001_market();

            //pega a lista de notificações enviadas
            $data['email_notifications'] = $this->_fetch_email_notification($this->input->post('year'), $this->input->post('month'), $this->input->post('fk_agent'));
        }

        //pega a lista de agentes
        $data['agents'] = $this->_process_agents_group();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>            
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>    
                <script src="' . base_url() . 'web/js/pen001/upsert.js?'. microtime().'"></script>';
        $data['css_include']  = '
                <link rel="stylesheet" type="text/css" href="' . base_url() . 'js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'pen001/upsert';

        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_pen001') > 0) {
            $this->Pen001_model->_pk_pen001 = $this->input->post('pk_pen001');
            $this->Pen001_model->delete();
        }
        redirect('pen001/index');
    }

    private function _validate_form() {

        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('pen001_file', 'pen001_file', 'trim');
        $this->form_validation->set_rules('notification_term_file', 'notification_term_file', 'trim');
        $this->form_validation->set_rules('provided_insuficiency', 'provided_insuficiency', 'trim');
        $this->form_validation->set_rules('provided_penalty', 'provided_penalty', 'trim');
        $this->form_validation->set_rules('provided_cicle_month', 'provided_cicle_month', 'trim');
        $this->form_validation->set_rules('provided_cicle_year', 'provided_cicle_year', 'trim');
        $this->form_validation->set_rules('provided_payment_date', 'provided_payment_date', 'trim');
        $this->form_validation->set_rules('realized_insuficiency', 'realized_insuficiency', 'trim');
        $this->form_validation->set_rules('realized_penalty', 'realized_penalty', 'trim');
        $this->form_validation->set_rules('realized_cicle_month', 'realized_cicle_month', 'trim');
        $this->form_validation->set_rules('realized_cicle_year', 'realized_cicle_year', 'trim');
        $this->form_validation->set_rules('realized_payment_date', 'realized_payment_date', 'trim');
        $this->form_validation->set_rules('realized_notification_term', 'realized_notification_term', 'trim');
        $this->form_validation->set_rules('memo', 'memo', 'trim');
    }

    private function _fill_model() {
        $this->Pen001_model->_pk_pen001                  = $this->input->post('pk_pen001');
        $this->Pen001_model->_month                      = $this->input->post('month');
        $this->Pen001_model->_year                       = $this->input->post('year');
        $this->Pen001_model->_fk_agent                   = $this->input->post('fk_agent');
        $this->Pen001_model->_provided_insuficiency      = str_replace(",", ".", $this->input->post('provided_insuficiency'));
        $this->Pen001_model->_provided_penalty           = str_replace(",", ".", $this->input->post('provided_penalty'));
        $this->Pen001_model->_provided_cicle_month       = $this->input->post('provided_cicle_month');
        $this->Pen001_model->_provided_cicle_year        = $this->input->post('provided_cicle_year');
        $this->Pen001_model->_provided_payment_date      = human_date_to_date($this->input->post('provided_payment_date'), 1);
        $this->Pen001_model->_realized_insuficiency      = str_replace(",", ".", $this->input->post('realized_insuficiency'));
        $this->Pen001_model->_realized_penalty           = str_replace(",", ".", $this->input->post('realized_penalty'));
        $this->Pen001_model->_realized_cicle_month       = $this->input->post('realized_cicle_month');
        $this->Pen001_model->_realized_cicle_year        = $this->input->post('realized_cicle_year');
        $this->Pen001_model->_realized_payment_date      = human_date_to_date($this->input->post('realized_payment_date'), 1);
        $this->Pen001_model->_realized_notification_term = $this->input->post('realized_notification_term');
        $this->Pen001_model->_memo                       = $this->input->post('memo');
        $this->Pen001_model->_status                     = $this->Pen001_model->_status_active;
    }


    private function _process_unities() {
        $this->load->model("Unity_model");
        $this->Unity_model->_fk_agent = $this->Pen001_model->_fk_agent;
        $this->Unity_model->_status   = $this->Unity_model->_status_active;
        $unities                      = $this->Unity_model->fetch();

        if ($unities) {
            foreach ($unities as $k => $unity) {
                $unities[$k]["checked"] = false;
            }
            $unities = $this->_process_pen001_unity($unities);
        }
        return $unities;
    }

    private function _process_pen001_unity($unities) {
        $this->load->model("Pen001_unity_model");
        $this->Pen001_unity_model->_fk_pen001 = $this->Pen001_model->_pk_pen001;
        $this->Pen001_unity_model->_status    = $this->Pen001_unity_model->_status_active;
        $pen001_unity                         = $this->Pen001_unity_model->fetch();

        if ($pen001_unity) {
            foreach ($unities as $k => $unity) {
                foreach ($pen001_unity as $pu) {
                    if ($unity['pk_unity'] == $pu['fk_unity']) {
                        $unities[$k]["checked"] = true;
                    }
                }
            }
        }
        return $unities;
    }

    private function _process_pen001_market() {
        $this->Pen001_market_model->_month  = $this->Pen001_model->_month;
        $this->Pen001_market_model->_year   = $this->Pen001_model->_year;
        $this->Pen001_market_model->_status = $this->Pen001_market_model->_status_active;
        $this->Pen001_market_model->read();
    }

    private function _create_pen001_unity() {
        $unities = $this->input->post('chk');
        if ($unities) {
            $this->load->model("Pen001_unity_model");
            $this->Pen001_unity_model->_fk_pen001 = $this->Pen001_model->_pk_pen001;
            $this->Pen001_unity_model->delete();

            $this->Pen001_unity_model->_status     = $this->Pen001_unity_model->_status_active;
            $this->Pen001_unity_model->_created_at = date("Y-m-d H:i:s");
            foreach ($unities as $unity) {
                $this->Pen001_unity_model->_fk_unity = $unity;
                $this->Pen001_unity_model->create();
            }
        }
    }

    private function _process_files() {
        $config['upload_path']   = './uploads/pen001/' . $this->Pen001_model->_pk_pen001;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        $return = array(
            'pen001_file'            => "",
            'notification_term_file' => ""
        );

        foreach ($return as $key => $value) {
            if ($this->upload->do_upload($key)) {
                $return[$key] = $_FILES[$key]['name'];
            }
        }
        
        foreach ($return as $key => $value) {
            if ($value != "") {
                $this->Pen001_model->{"_" . $key} = $value;
            }
        }    
        $this->Pen001_model->update();
    }
    
    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $this->Pen001_model->_pk_pen001 = $this->input->post("pk_pen001_sendmail");
            $pen001                                    = $this->Pen001_model->read();
            if ($pen001) {

                //pega o agente
                $agent = $this->_get_agent($this->Pen001_model->_fk_agent);

                if ($agent) {
                    if ($this->input->post('test')) {
                        $destination = $this->input->post('test_email');
                        $this->_post_mail($destination);
                    }
                    else {
                        $contacts = $this->input->post("contacts");
                        if (!empty($contacts)) {
                            $contact_list = [];
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $this->Pen001_model->_fk_agent);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($this->Pen001_model->_year, $this->Pen001_model->_month);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($contact_list);
                            }

                        }
                    }
                }
            }
        }
        redirect("pen001/upsert");
    }

    private function _post_mail($to = null) {
        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = "Penalidades / Lastro - PEN001";
        $this->load->clear_vars();
        $msg = $this->load->view('pen001/email/upsert', array(), true);
        $post['message'] = base64_encode($msg);

        $file = [];
        if ($this->Pen001_model->_pen001_file != "") {
            $file[] = 'uploads/pen001/' . $this->Pen001_model->_pk_pen001 . '/' . $this->Pen001_model->_pen001_file;
        }
        if ($this->Pen001_model->_notification_term_file != "") {
            $file[] = 'uploads/pen001/' . $this->Pen001_model->_pk_pen001 . '/' . $this->Pen001_model->_notification_term_file;
        }

        if (!empty($file)) {
            $post['files'] = join(",", $file);
        }

        $pymail = pymail($post);
    }
    
    

}
