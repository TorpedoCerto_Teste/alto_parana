<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_slip_ccee_unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Payment_slip_ccee_unity_model');
    }

    function index() {
        $this->Payment_slip_ccee_unity_model->_status = $this->Payment_slip_ccee_unity_model->_status_active;
        $data['list']                                 = $this->Payment_slip_ccee_unity_model->fetch();
        $data['js_include']                           = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                          = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                         = 'payment_slip_ccee_unity/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Payment_slip_ccee_unity_model->_created_at = date("Y-m-d H:i:s");
                $create                                           = $this->Payment_slip_ccee_unity_model->create();
                if ($create) {
                    redirect('payment_slip_ccee_unity/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'payment_slip_ccee_unity/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('payment_slip_ccee_unity/index');
        }
        $this->Payment_slip_ccee_unity_model->_pk_payment_slip_ccee_unity = $this->uri->segment(3);
        $read                                                             = $this->Payment_slip_ccee_unity_model->read();
        if (!$read) {
            redirect('payment_slip_ccee_unity/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Payment_slip_ccee_unity_model->update();
                redirect('payment_slip_ccee_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'payment_slip_ccee_unity/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('payment_slip_ccee_unity/index');
        }
        $this->Payment_slip_ccee_unity_model->_pk_payment_slip_ccee_unity = $this->uri->segment(3);
        $read                                                             = $this->Payment_slip_ccee_unity_model->read();
        if (!$read) {
            redirect('payment_slip_ccee_unity/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Payment_slip_ccee_unity_model->upsert();
                redirect('payment_slip_ccee_unity/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'payment_slip_ccee_unity/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_payment_slip_ccee_unity') > 0) {
            $this->Payment_slip_ccee_unity_model->_pk_payment_slip_ccee_unity = $this->input->post('pk_payment_slip_ccee_unity');
            $this->Payment_slip_ccee_unity_model->delete();
        }
        redirect('payment_slip_ccee_unity/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('pk_payment_slip_ccee_unity', 'pk_payment_slip_ccee_unity', 'trim|required');
        $this->form_validation->set_rules('fk_payment_slip_ccee', 'fk_payment_slip_ccee', 'trim|required');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
    }

    private function _fill_model() {
        $this->Payment_slip_ccee_unity_model->_pk_payment_slip_ccee_unity = $this->input->post('pk_payment_slip_ccee_unity');
        $this->Payment_slip_ccee_unity_model->_fk_payment_slip_ccee       = $this->input->post('fk_payment_slip_ccee');
        $this->Payment_slip_ccee_unity_model->_fk_unity                   = $this->input->post('fk_unity');
        $this->Payment_slip_ccee_unity_model->_status                     = $this->Payment_slip_ccee_unity_model->_status_active;
    }

}
