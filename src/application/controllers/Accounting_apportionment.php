<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting_apportionment extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Accounting_apportionment_model');
    }

    function index() {
        $this->Accounting_apportionment_model->_status = $this->Accounting_apportionment_model->_status_active;
        $data['list']                                  = $this->Accounting_apportionment_model->fetch();
        $data['js_include']                            = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                           = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                          = 'accounting_apportionment/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Accounting_apportionment_model->_created_at = date("Y-m-d H:i:s");
                $create                                            = $this->Accounting_apportionment_model->create();
                if ($create) {
                    redirect('accounting_apportionment/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'accounting_apportionment/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('accounting_apportionment/index');
        }
        $this->Accounting_apportionment_model->_pk_accounting_apportionment = $this->uri->segment(3);
        $read                                                               = $this->Accounting_apportionment_model->read();
        if (!$read) {
            redirect('accounting_apportionment/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Accounting_apportionment_model->update();
                redirect('accounting_apportionment/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'accounting_apportionment/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('accounting_apportionment/index');
        }
        $this->Accounting_apportionment_model->_pk_accounting_apportionment = $this->uri->segment(3);
        $read                                                               = $this->Accounting_apportionment_model->read();
        if (!$read) {
            redirect('accounting_apportionment/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Accounting_apportionment_model->upsert();
                redirect('accounting_apportionment/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'accounting_apportionment/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_accounting_apportionment') > 0) {
            $this->Accounting_apportionment_model->_pk_accounting_apportionment = $this->input->post('pk_accounting_apportionment');
            $this->Accounting_apportionment_model->delete();
        }
        redirect('accounting_apportionment/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('pk_accounting_apportionment', 'pk_accounting_apportionment', 'trim|required');
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('fk_accounting', 'fk_accounting', 'trim|required');
    }

    private function _fill_model() {
        $this->Accounting_apportionment_model->_pk_accounting_apportionment = $this->input->post('pk_accounting_apportionment');
        $this->Accounting_apportionment_model->_fk_unity                    = $this->input->post('fk_unity');
        $this->Accounting_apportionment_model->_fk_accounting               = $this->input->post('fk_accounting');
        $this->Accounting_apportionment_model->_status                      = $this->Accounting_apportionment_model->_status_active;
    }

}
