<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletter extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Newsletter_model');
    }

    function index() {
        $this->Newsletter_model->_status = $this->Newsletter_model->_status_active;
        $data['list']                    = $this->Newsletter_model->fetch();
        $data['js_include']              = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']             = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']            = 'newsletter/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Newsletter_model->_created_at = date("Y-m-d H:i:s");
                $create                              = $this->Newsletter_model->create();
                if ($create) {
                    redirect('newsletter/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'newsletter/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('newsletter/index');
        }
        $this->Newsletter_model->_pk_newsletter = $this->uri->segment(3);
        $read                                   = $this->Newsletter_model->read();
        if (!$read) {
            redirect('newsletter/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Newsletter_model->update();
                redirect('newsletter/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'newsletter/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('newsletter/index');
        }
        $this->Newsletter_model->_pk_newsletter = $this->uri->segment(3);
        $read                                   = $this->Newsletter_model->read();
        if (!$read) {
            redirect('newsletter/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Newsletter_model->upsert();
                redirect('newsletter/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'newsletter/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_newsletter') > 0) {
            $this->Newsletter_model->_pk_newsletter = $this->input->post('pk_newsletter');
            $this->Newsletter_model->delete();
        }
        redirect('newsletter/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[newsletter.email]');
    }

    private function _fill_model() {
        $this->Newsletter_model->_email       = $this->input->post('email');
        $this->Newsletter_model->_status = $this->Newsletter_model->_status_active;
    }

}
