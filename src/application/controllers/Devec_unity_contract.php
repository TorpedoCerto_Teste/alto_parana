<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Devec_unity_contract extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Devec_unity_contract_model');
    }

    function index() {
        $this->Devec_unity_contract_model->_status = $this->Devec_unity_contract_model->_status_active;
        $data['list']                              = $this->Devec_unity_contract_model->fetch();
        $data['js_include']                        = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                       = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                      = 'devec_unity_contract/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Devec_unity_contract_model->_created_at = date("Y-m-d H:i:s");
                $create                                        = $this->Devec_unity_contract_model->create();
                if ($create) {
                    redirect('devec_unity_contract/index');
                }
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'devec_unity_contract/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('devec_unity_contract/index');
        }
        $this->Devec_unity_contract_model->_pk_devec_unity_contract = $this->uri->segment(3);
        $read                                                       = $this->Devec_unity_contract_model->read();
        if (!$read) {
            redirect('devec_unity_contract/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Devec_unity_contract_model->update();
                redirect('devec_unity_contract/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'devec_unity_contract/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('devec_unity_contract/index');
        }
        $this->Devec_unity_contract_model->_pk_devec_unity_contract = $this->uri->segment(3);
        $read                                                       = $this->Devec_unity_contract_model->read();
        if (!$read) {
            redirect('devec_unity_contract/index');
        }

        if ($this->input->post('upsert') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $upsert = $this->Devec_unity_contract_model->upsert();
                redirect('devec_unity_contract/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'devec_unity_contract/update';
        $this->load->view('includes/template', $data);
    }

    function delete() {
        if ($this->input->post('pk_devec_unity_contract') > 0) {
            $this->Devec_unity_contract_model->_pk_devec_unity_contract = $this->input->post('pk_devec_unity_contract');
            $this->Devec_unity_contract_model->delete();
        }
        redirect('devec_unity_contract/index');
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_devec_unity', 'fk_devec_unity', 'trim|required');
        $this->form_validation->set_rules('cliqCCEE', 'cliqCCEE', 'trim|required');
        $this->form_validation->set_rules('contract_mhw', 'contract_mhw', 'trim|required');
        $this->form_validation->set_rules('contract_value', 'contract_value', 'trim|required');
    }

    private function _fill_model() {
        $this->Devec_unity_contract_model->_fk_devec_unity = $this->input->post('fk_devec_unity');
        $this->Devec_unity_contract_model->_cliqCCEE       = $this->input->post('cliqCCEE');
        $this->Devec_unity_contract_model->_contract_mhw   = $this->input->post('contract_mhw');
        $this->Devec_unity_contract_model->_contract_value = $this->input->post('contract_value');
        $this->Devec_unity_contract_model->_status         = $this->Devec_unity_contract_model->_status_active;
    }

}
