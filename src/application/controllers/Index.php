<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Index
 *
 * @author rogeriopellarin
 */
class Index extends Public_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        //verificar session
        if (!$this->session->userdata('pk_user')) {
            redirect('index/login');
        }
        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'index/index';
        $this->load->view('includes/template', $data);
    }

    function login() {
        $data['error'] = 0;
        $data['expiration'] = $this->_validate_payment();


        if ($this->input->post('login') == 'true') {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                if ($data['expiration'] < 3) {
                    $this->load->model('User_model');
                    $this->User_model->_email    = $this->input->post('email');
                    $this->User_model->_password = md5($this->input->post('password'));
                    $find                        = $this->User_model->find_by_email_and_password();
                    if ($find) {
                        $this->_register_session();
                        if ($this->session->has_userdata('url_return')) {
                            redirect($this->session->userdata('url_return'));
                        }
                        redirect('agent');
                    }
                }
                $data['error'] = 1;
            }
        }

        $this->load->view('index/login', $data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('index/login');
    }

    private function _register_session() {
        $login_session['pk_user']      = $this->User_model->_pk_user;
        $login_session['name']         = $this->User_model->_name;
        $login_session['email']        = $this->User_model->_email;
        $login_session['access_level'] = $this->User_model->_access_level;
        $this->session->set_userdata($login_session);
    }

    private function _send_email_forgot_password($new_password) {
        $this->load->library('email');
        $this->load->config('email_config');
        $this->load->clear_vars();
        $this->email->from($this->config->item('smtp_user'), $this->config->item('remetente'));
        $destinatarios   = $this->config->item('to');
        $destinatarios[] = $this->input->post('input_enter_user');
        $this->email->to($destinatarios);
        $this->email->subject('Recuperar password');

        $data['new_password'] = $new_password;
        $msg                  = $this->load->view('emails/customer/forgot_password', $data, TRUE);
        $this->email->message($msg);

        return $this->email->send();
    }

    public function ajax_set_sidebar_collapsed() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $this->session->set_userdata(array('sidebar_collapsed' => !$this->session->userdata('sidebar_collapsed') ? true : false));
    }

    private function _validate_payment() {
        return 1; //forçando para liberar acesso sempre
//        $payment = $this->_check_payment();
//        if ($payment) {
//            return 1; //PAGO! ACESSO LIBERADO
//        }
//        else {
//            if (date("d") <= 16) { 
//                return 2; //ESTÁ PARA VENCER
//            }
//        }
//        return 3; //NÃO PAGO
    }
    
    private function _check_payment() {
        $this->load->model("Payment_model");
        $this->Payment_model->_month  = date("m");
        $this->Payment_model->_year   = date("Y");
        $this->Payment_model->_status = $this->Payment_model->_status_active;
        return $this->Payment_model->fetch();
    }
    
    
}
