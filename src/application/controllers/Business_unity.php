<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Business_unity extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Business_unity_model');
    }

    function create() {
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('business/index');
        }
        $this->load->model('Business_model');
        $this->Business_model->_pk_business = $this->uri->segment(3);
        $read                               = $this->Business_model->read();
        if (!$read) {
            redirect('business/index');
        }

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Business_unity_model->create();
            }
        }
        redirect('business/view/' . $this->Business_model->_pk_business . '/unidades');
    }

    function delete() {
        if ($this->input->post('pk_business_unity') > 0) {
            $this->Business_unity_model->_pk_business_unity = $this->input->post('pk_business_unity');
            $this->Business_unity_model->delete();
        }
        redirect('business/view/' . $this->uri->segment(3) . '/unidades');
    }

    private function _process_agent_unity($list_units) {
        $this->load->model('Unity_model');
        $this->Unity_model->_status = $this->Unity_model->_status_active;
        $units                      = $this->Unity_model->fetch();

        //retirar as unidades que já foram selecionadas
        if ($units && $list_units) {
            $units = $this->_clean_units($units, $list_units);
        }

        $option = array();
        if ($units) {
            foreach ($units as $unity) {
                $option[$unity['agent_company_name']][$unity['pk_unity']] = $unity;
            }
        }

        return $option;
    }

    private function _clean_units($units, $list_units) {
        foreach ($units as $k => $unit) {
            foreach ($list_units as $list_unit) {
                if ($unit['pk_unity'] == $list_unit['fk_unity']) {
                    unset($units[$k]);
                }
            }
        }
        return $units;
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_unity', 'fk_unity', 'trim|required');
        $this->form_validation->set_rules('start_date', 'start_date', 'trim');
        $this->form_validation->set_rules('end_date', 'end_date', 'trim');
    }

    private function _fill_model() {
        $this->Business_unity_model->_fk_business = $this->Business_model->_pk_business;
        $this->Business_unity_model->_fk_unity    = $this->input->post('fk_unity');
        $this->Business_unity_model->_start_date  = human_date_to_date($this->input->post('start_date'));
        $this->Business_unity_model->_end_date    = human_date_to_date($this->input->post('end_date'));
        $this->Business_unity_model->_status      = $this->Business_unity_model->_status_active;
    }

}
