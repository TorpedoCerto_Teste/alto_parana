<?php

/**
  fk_business
  remuneration_type
  percent_economy_value
  percent_economy_floor
  percent_economy_roof
  over_energy_value
  over_energy_floor
  over_energy_roof
  closed_value
  closed_installment
  closed_installment_quantity
  monthly_counterpart_unity_quantity_consumption
  monthly_counterpart_value
  due
  due_day
  due_period
  due_month
  financial_index
  index_readjustment_calculation
  database
  adjustment_montly_manually
  date_adjustment
  date_adjustment_frequency
  penalty
  interest
  interest_frequency

 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Business_payment extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        if ($this->uri->segment(3, 0) > 0) {
            $this->load->model("Business_model");
            $this->Business_model->_pk_business = $this->uri->segment(3);
            $read                               = $this->Business_model->read();
            if (!$read) {
                redirect("business");
            }
        }
        else {
            redirect("business");
        }

        $this->load->model('Business_payment_model');
    }


    function update() {
        //pega o registro pela chave extrangeira do business
        $this->Business_payment_model->_fk_business = $this->Business_model->_pk_business;
        $read                                       = $this->Business_payment_model->read();

        if ($this->input->post('update') == 'true') {
            $this->form_validation->set_rules('remuneration_type', 'remuneration_type', 'trim');
            $this->form_validation->set_rules('percent_economy_value', 'percent_economy_value', 'trim');
            $this->form_validation->set_rules('percent_economy_floor', 'percent_economy_floor', 'trim');
            $this->form_validation->set_rules('percent_economy_roof', 'percent_economy_roof', 'trim');
            $this->form_validation->set_rules('over_energy_value', 'over_energy_value', 'trim');
            $this->form_validation->set_rules('over_energy_floor', 'over_energy_floor', 'trim');
            $this->form_validation->set_rules('over_energy_roof', 'over_energy_roof', 'trim');
            $this->form_validation->set_rules('closed_value', 'closed_value', 'trim');
            $this->form_validation->set_rules('closed_installment', 'closed_installment', 'trim');
            $this->form_validation->set_rules('closed_installment_quantity', 'closed_installment_quantity', 'trim');
            $this->form_validation->set_rules('monthly_counterpart_unity_quantity_consumption', 'monthly_counterpart_unity_quantity_consumption', 'trim');
            $this->form_validation->set_rules('monthly_counterpart_value', 'monthly_counterpart_value', 'trim');
            $this->form_validation->set_rules('due', 'due', 'trim');
            $this->form_validation->set_rules('due_day', 'due_day', 'trim');
            $this->form_validation->set_rules('due_period', 'due_period', 'trim');
            $this->form_validation->set_rules('due_month', 'due_month', 'trim');
            $this->form_validation->set_rules('financial_index', 'financial_index', 'trim');
            $this->form_validation->set_rules('index_readjustment_calculation', 'index_readjustment_calculation', 'trim');
            $this->form_validation->set_rules('database', 'database', 'trim');
            $this->form_validation->set_rules('adjustment_montly_manually', 'adjustment_montly_manually', 'trim');
            $this->form_validation->set_rules('date_adjustment', 'date_adjustment', 'trim');
            $this->form_validation->set_rules('date_adjustment_frequency', 'date_adjustment_frequency', 'trim');
            $this->form_validation->set_rules('penalty', 'penalty', 'trim');
            $this->form_validation->set_rules('interest', 'interest', 'trim');
            $this->form_validation->set_rules('interest_frequency', 'interest_frequency', 'trim');
            if ($this->form_validation->run() == TRUE) {
                $update = $this->_validate_and_update();
            }
        }
        redirect('business/view/'.$this->Business_model->_pk_business.'/pagamento');
    }

    private function _validate_and_update() {
        $this->Business_payment_model->_remuneration_type                              = $this->input->post('remuneration_type');
        $this->Business_payment_model->_percent_economy_value                          = number_format(str_replace(",", ".", $this->input->post('percent_economy_value')), 2, ".", "");
        $this->Business_payment_model->_percent_economy_floor                          = number_format(str_replace(",", ".", $this->input->post('percent_economy_floor')), 2, ".", "");
        $this->Business_payment_model->_percent_economy_roof                           = number_format(str_replace(",", ".", $this->input->post('percent_economy_roof')), 2, ".", "");
        $this->Business_payment_model->_over_energy_value                              = number_format(str_replace(",", ".", $this->input->post('over_energy_value')), 2, ".", "");
        $this->Business_payment_model->_over_energy_floor                              = number_format(str_replace(",", ".", $this->input->post('over_energy_floor')), 2, ".", "");
        $this->Business_payment_model->_over_energy_roof                               = number_format(str_replace(",", ".", $this->input->post('over_energy_roof')), 2, ".", "");
        $this->Business_payment_model->_closed_value                                   = number_format(str_replace(",", ".", $this->input->post('closed_value')), 2, ".", "");
        $this->Business_payment_model->_closed_installment                             = $this->input->post('closed_installment') == 1 ? 1 : 0;
        $this->Business_payment_model->_closed_installment_quantity                    = $this->input->post('closed_installment_quantity') > 0 ? $this->input->post('closed_installment_quantity') : 0;
        $this->Business_payment_model->_monthly_counterpart_unity_quantity_consumption = $this->input->post('monthly_counterpart_unity_quantity_consumption');
        $this->Business_payment_model->_monthly_counterpart_value                      = number_format(str_replace(",", ".", $this->input->post('monthly_counterpart_value')), 2, ".", "");
        $this->Business_payment_model->_due                                            = $this->input->post('due');
        $this->Business_payment_model->_due_day                                        = $this->input->post('due_day');
        $this->Business_payment_model->_due_period                                     = $this->input->post('due_period');
        $this->Business_payment_model->_due_month                                      = $this->input->post('due_month');
        $this->Business_payment_model->_financial_index                                = $this->input->post('financial_index');
        $this->Business_payment_model->_index_readjustment_calculation                 = $this->input->post('index_readjustment_calculation');
        $this->Business_payment_model->_database                                       = human_date_to_date($this->input->post('database'));
        $this->Business_payment_model->_adjustment_montly_manually                     = $this->input->post('adjustment_montly_manually') == 1 ? 1 : 0;
        $this->Business_payment_model->_date_adjustment                                = human_date_to_date($this->input->post('date_adjustment'));
        $this->Business_payment_model->_date_adjustment_frequency                      = $this->input->post('date_adjustment_frequency');
        $this->Business_payment_model->_penalty                                        = number_format(str_replace(",", ".", $this->input->post('penalty')), 2, ".", "");
        $this->Business_payment_model->_interest                                       = number_format(str_replace(",", ".", $this->input->post('interest')), 2, ".", "");
        $this->Business_payment_model->_interest_frequency                             = $this->input->post('interest_frequency');
        $update                                                                        = $this->Business_payment_model->update();

        if ($this->Business_payment_model->_remuneration_type == "mensal_por_unidade") {
            $this->load->model("Business_payment_monthly_unity_model");

            //inativar todos os registros ativos pertencentes ao business_payment
            $this->Business_payment_monthly_unity_model->_fk_business_payment = $this->Business_payment_model->_pk_business_payment;
            $this->Business_payment_monthly_unity_model->delete();
            
            //percorrer o array de registros enviados pelo post e reinserir os valores
            $monthly_value = $this->input->post('monthly_value');
            foreach ($monthly_value as $unity => $value) {
                $this->Business_payment_monthly_unity_model->_fk_unity      = $unity;
                $this->Business_payment_monthly_unity_model->_status        = $this->Business_payment_monthly_unity_model->_status_active;
                $this->Business_payment_monthly_unity_model->_monthly_value = number_format(str_replace(",", ".", $value), 2, ".", "");
                $this->Business_payment_monthly_unity_model->create();
            }
            
        }

        if ($this->Business_payment_model->_remuneration_type == "valor_fixo_mais_percentual_de_economia") {
            $this->load->model("Business_payment_fixed_value_unity_model");

            //inativar todos os registros ativos
            $this->Business_payment_fixed_value_unity_model->_fk_business_payment = $this->Business_payment_model->_pk_business_payment;
            $this->Business_payment_fixed_value_unity_model->delete();

            //percorrer o array de registros enviados pelo post e inserir os valores
            $payment_fixed_value_unities = $this->input->post('payment_fixed_value_unities');
            foreach ($payment_fixed_value_unities as $unity => $values) {
                $this->Business_payment_fixed_value_unity_model->_fk_unity    = $unity;
                $this->Business_payment_fixed_value_unity_model->_fixed_value = number_format(str_replace(",", ".", $values['fixed_value']), 2, ".", "");
                $this->Business_payment_fixed_value_unity_model->_percent     = number_format(str_replace(",", ".", $values['percent']), 2, ".", "");
                $this->Business_payment_fixed_value_unity_model->_roof        = number_format(str_replace(",", ".", $values['roof']), 2, ".", "");
                $this->Business_payment_fixed_value_unity_model->_floor       = number_format(str_replace(",", ".", $values['floor']), 2, ".", "");
                $this->Business_payment_fixed_value_unity_model->_status = $this->Business_payment_fixed_value_unity_model->_status_active;
                $this->Business_payment_fixed_value_unity_model->create();
            }
        }
    }


}
