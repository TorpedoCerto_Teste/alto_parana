<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pen001_market extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Pen001_market_model');
    }

    function index() {
        $data['list'] = $this->_process_pen001_market();

        $data['js_include']   = '
            <script src="' . base_url() . 'js/bootstrap-datepicker.js"></script>
            <script src="' . base_url() . 'js/touchspin.js"></script>
            <script src="' . base_url() . 'js/mascara.js"></script>
            <script src="' . base_url() . 'web/js/pen001_market/index.js"></script>            
            ';
        $data['css_include']  = '
            <link href="' . base_url() . 'css/datepicker.css" rel="stylesheet" type="text/css" >
            <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
            ';
        $data['main_content'] = 'pen001_market/index';
        $this->load->view('includes/template', $data);
    }

    private function _process_pen001_market() {
        $this->Pen001_market_model->_year = date("Y");
        if ($this->input->post('year')) {
            $this->Pen001_market_model->_year = $this->input->post('year');
        }
        $this->Pen001_market_model->_status = $this->Pen001_market_model->_status_active;
        $pen001_market_list                 = $this->Pen001_market_model->fetch();

        //gera os meses
        $return = array();
        for ($i = 1; $i <= 12; $i++) {
            $return[$i]['pk_pen001_market'] = 0;
            $return[$i]['month']            = retorna_mes($i);
            $return[$i]['year']             = $this->Pen001_market_model->_year;
            $return[$i]['reporting_date']   = "";
            $return[$i]['value_reference']  = 0;
            $return[$i]['PLD_average']      = 0;
        }

        if ($pen001_market_list) {
            foreach ($pen001_market_list as $list) {
                $return[$list['month']]['pk_pen001_market'] = $list['pk_pen001_market'];
                $return[$list['month']]['reporting_date']   = date_to_human_date($list['reporting_date']);
                $return[$list['month']]['value_reference']  = number_format($list['value_reference'], 2, ",", "");
                $return[$list['month']]['PLD_average']      = number_format($list['PLD_average'], 2, ",", "");
            }
        }
        return $return;
    }

    function ajax_process_pen001_market() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');
        $pen001              = $this->_process_pen001_market();
        print_r(json_encode($pen001));
    }

    function ajax_upsert_pen001_market() {
        $this->disableLayout = TRUE;
        $this->output->set_content_type('application/json');

        $this->Pen001_market_model->_month = $this->input->post('month');
        $this->Pen001_market_model->_year  = $this->input->post('year');
        
        switch ($this->input->post('field')) {
            case 'reporting_date':
                $this->Pen001_market_model->_reporting_date = human_date_to_date($this->input->post('value'), 1);
                break;
            case 'value_reference':
                $this->Pen001_market_model->_value_reference = str_replace(",", ".", $this->input->post('value'));
                break;
            case 'PLD_average':
                $this->Pen001_market_model->_PLD_average = str_replace(",", ".", $this->input->post('value'));
                break;
        }
        $this->Pen001_market_model->_created_at = date("Y-m-d H:m:s");
        
        $this->Pen001_market_model->upsert();
    }

    private function _validate_form() {
        $this->form_validation->set_rules('pk_pen001_market', 'pk_pen001_market', 'trim');
        $this->form_validation->set_rules('month', 'month', 'trim');
        $this->form_validation->set_rules('year', 'year', 'trim');
        $this->form_validation->set_rules('reporting_date', 'reporting_date', 'trim');
        $this->form_validation->set_rules('value_reference', 'value_reference', 'trim');
        $this->form_validation->set_rules('PLD_average', 'PLD_average', 'trim');
    }

    private function _fill_model() {
        $this->Pen001_market_model->_pk_pen001_market = $this->input->post('pk_pen001_market');
        $this->Pen001_market_model->_month            = $this->input->post('month');
        $this->Pen001_market_model->_year             = $this->input->post('year');
        $this->Pen001_market_model->_reporting_date   = $this->input->post('reporting_date');
        $this->Pen001_market_model->_value_reference  = $this->input->post('value_reference');
        $this->Pen001_market_model->_PLD_average      = $this->input->post('PLD_average');
        $this->Pen001_market_model->_status           = $this->Pen001_market_model->_status_active;
    }

}
