<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Business_service extends Public_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Business_service_model');

        $this->load->model('Business_model');
        $this->Business_model->_pk_business = $this->uri->segment(3, 0);
    }

    function index() {
        $this->Business_service_model->_status = $this->Business_service_model->_status_active;
        $data['list']                          = $this->Business_service_model->fetch();
        $data['js_include']                    = '
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/dataTables.tableTools.min.js"></script>
            <script src="js/bootstrap-dataTable.js"></script>
            <script src="js/dataTables.colVis.min.js"></script>
            <script src="js/dataTables.responsive.min.js"></script>
            <script src="js/dataTables.scroller.min.js"></script>
            <script src="web/js/index_index.js"></script>
            ';
        $data['css_include']                   = '
            <link href="css/jquery.dataTables.css" rel="stylesheet">
            <link href="css/dataTables.tableTools.css" rel="stylesheet">
            <link href="css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="css/dataTables.responsive.css" rel="stylesheet">
            <link href="css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']                  = 'Business_service/index';
        $this->load->view('includes/template', $data);
    }

    function create() {
        $data['error'] = false;

        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $create = $this->Business_service_model->create();
                if ($create) {
                    redirect('Business_service/index');
                }
            }
            $data['error'] = true;
        }

        //pega a lista de serviços
        $data['services'] = $this->_process_services();

        $data['products'] = $this->_process_product();

        $data['js_include']   = '
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery.min.js"></script>
                <script src="' . base_url() . 'js/switchery/switchery-init.js"></script>
            ';
        $data['css_include']  = '
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">
                <link href="' . base_url() . 'js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
            ';
        $data['main_content'] = 'Business_service/create';
        $this->load->view('includes/template', $data);
    }

    function update() {
        $data['error'] = false;
        if ($this->uri->segment(3, 0) <= 0) {
            redirect('Business_service/index');
        }
        $this->Business_service_model->_pk_Business_service = $this->uri->segment(3);
        $read                                               = $this->Business_service_model->read();
        if (!$read) {
            redirect('Business_service/index');
        }

        if ($this->input->post('update') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $update = $this->Business_service_model->update();
                redirect('Business_service/index');
            }
            $data['error'] = true;
        }

        $data['js_include']   = '';
        $data['css_include']  = '';
        $data['main_content'] = 'Business_service/update';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        if ($this->input->post('create') == 'true') {
            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $pk_business_service = $this->Business_service_model->upsert();
                if ($pk_business_service) {
                    //inserir os registros em business_service_product
                    $this->_insert_business_service_product($pk_business_service);
                }
            }
        }
        redirect("business/view/" . $this->Business_model->_pk_business . "/servicos");
    }

    function delete() {
        if ($this->input->post('pk_Business_service') > 0) {
            $this->Business_service_model->_pk_Business_service = $this->input->post('pk_Business_service');
            $this->Business_service_model->delete();
        }
        redirect('Business_service/index');
    }

    private function _insert_business_service_product($pk_business_service) {
        $this->load->model("Business_service_product_model");

        //inativar os registros ativos
        $this->Business_service_product_model->_fk_business_service = $pk_business_service;
        $this->Business_service_product_model->delete();

        //inserir todos os novos registros 
        $this->Business_service_product_model->_status = $this->Business_service_product_model->_status_active;
        $products                                      = $this->input->post("product");
        if (!empty($products)) {
            foreach ($products as $pk_product) {
                $this->Business_service_product_model->_fk_product = $pk_product;
                $this->Business_service_product_model->create();
            }
        }
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_service', 'fk_service', 'trim|required');
        $this->form_validation->set_rules('personalized', 'personalized', 'trim');
    }

    private function _fill_model() {
        $this->Business_service_model->_fk_business  = 1; //$this->Business_model->_pk_business;
        $this->Business_service_model->_fk_service   = $this->input->post('fk_service');
        $this->Business_service_model->_personalized = $this->input->post('personalized') == 1 ? 1 : 0;
        $this->Business_service_model->_status       = $this->Business_service_model->_status_active;
    }

}
