<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting extends Group_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('pk_user')) {
            $this->session->set_userdata(array('url_return' => $this->uri->uri_string()));
            redirect('index/login');
        }

        $this->load->model('Accounting_model');
    }

    function index() {
        $this->Accounting_model->_status = $this->Accounting_model->_status_active;
        $data['list']                    = $this->Accounting_model->fetch();
        $data['js_include']              = '
            <script src="' . base_url() . 'js/jquery.dataTables.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.tableTools.min.js"></script>
            <script src="' . base_url() . 'js/bootstrap-dataTable.js"></script>
            <script src="' . base_url() . 'js/dataTables.colVis.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.responsive.min.js"></script>
            <script src="' . base_url() . 'js/dataTables.scroller.min.js"></script>
            <script src="' . base_url() . 'web/js/accounting/index.js?'. microtime().'"></script>
            ';
        $data['css_include']             = '
            <link href="' . base_url() . 'css/jquery.dataTables.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.tableTools.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.colVis.min.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.responsive.css" rel="stylesheet">
            <link href="' . base_url() . 'css/dataTables.scroller.css" rel="stylesheet">
            ';
        $data['main_content']            = 'accounting/index';
        $this->load->view('includes/template', $data);
    }

    function upsert() {
        $data['success'] = $this->uri->segment(3, 0) == 0 ? false : true;
        $data['error']   = false;

        if ($this->uri->segment(4, 0) > 0) {
            $this->Accounting_model->_pk_accounting = $this->uri->segment(4);
            $this->Accounting_model->_status        = $this->Accounting_model->_status_active;
            $read                                   = $this->Accounting_model->read();
        }

        if ($this->input->post('upsert') == 'true') {

            $this->_validate_form();
            if ($this->form_validation->run() == TRUE) {
                $this->_fill_model();
                $this->Accounting_model->_pk_accounting = $this->Accounting_model->upsert();
                if ($this->Accounting_model->_pk_accounting) {
                    if (!empty($this->input->post('chk'))) {
                        $this->_process_accounting_apportionment($this->Accounting_model->_pk_accounting);
                    }
                    $upload = $this->_upload($this->Accounting_model->_pk_accounting);

                    foreach ($upload as $key => $value) {
                        if ($value != "") {
                            $this->Accounting_model->{"_" . $key} = $value;
                        }
                    }
                    $this->Accounting_model->update();
                }
                redirect(current_url() . "/1/" . $this->Accounting_model->_pk_accounting);
            }
            $data['error'] = true;
        }

        $data['agents'] = $this->_process_agents();

        //pega a lista de notificações enviadas
        $data['email_notifications'] = $this->_fetch_email_notification($this->Accounting_model->_year, $this->Accounting_model->_month, $this->Accounting_model->_fk_agent);

        $data['js_include']   = '
                <script src="' . base_url() . 'js/axios.js"></script>
                <script src="' . base_url() . 'js/vue.js"></script>
                <script src="' . base_url() . 'web/js/controller_group/group_contacts.vue.js?'. uniqid().'"></script>        
                <script src="' . base_url() . 'js/touchspin.js"></script>
                <script src="' . base_url() . 'js/mascara.js"></script>
                <script src="' . base_url() . 'js/select2.js"></script>
                <script src="' . base_url() . 'js/select2-init.js"></script>
                <script src="' . base_url() . 'web/js/accounting/upsert.js?'.date("YmdHis").'"></script>';
        $data['css_include']  = ' 
                <link href="' . base_url() . 'css/bootstrap-touchspin.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2.css" rel="stylesheet">
                <link href="' . base_url() . 'css/select2-bootstrap.css" rel="stylesheet">';
        $data['main_content'] = 'accounting/upsert';
        $this->load->view('includes/template', $data);
    }

    function sendmail() {
        if ($this->input->post("sendmail") == "true") {
            $contacts = array();
            $step     = $this->input->post("step");
            $this->Accounting_model->_pk_accounting = $this->input->post("pk_accounting_sendmail");
            $accounting                             = $this->Accounting_model->fetch_by_agent();

            if ($accounting) {
                if ($this->input->post('test')) {
                    $destination = $this->input->post('test_email');
                    $this->_post_mail($step, $accounting, $destination);

                }
                else {
                    $contacts = $this->input->post("contacts");
                    if (!empty($contacts)) {
                        $contact_list = [];
                        //pega o agente
                        $agent = $this->_get_agent($accounting[0]["fk_agent"]);

                        if ($agent) {
                            foreach ($contacts as $contact) {
                                //pega o contato
                                $contact = $this->_get_contact($contact, $accounting[0]["fk_agent"]);
                                if ($contact) {
                                    $contact_list[] = $this->Contact_model->_email;
                                    //registra no log
                                    $this->_process_email_notification($accounting[0]["year"], $accounting[0]["month"]);
                                }
                            }

                            if (!empty($contact_list)) {
                                //envia o email
                                $this->_post_mail($step, $accounting, $contact_list);
                            }
                        }
                    }
                }
            }
        }
        redirect("accounting");
    }

    function delete() {
        if ($this->input->post('pk_accounting') > 0) {
            $this->Accounting_model->_pk_accounting = $this->input->post('pk_accounting');
            $this->Accounting_model->delete();
        }
        redirect('accounting');
    }

    private function _upload($pk_accounting) {
        $config['upload_path']   = './uploads/accounting/' . $pk_accounting;
        $config['allowed_types'] = '*';
        $config['max_size']      = 80000;
        check_dir_exists($config['upload_path']);
        $this->load->library('upload', $config);

        $return = array(
            'rel_gfn001' => "",
            'mem_gfn001' => "",
            'sum001'     => "",
            'lfn002'     => "",
        );

        foreach ($return as $key => $value) {
            if ($this->upload->do_upload($key)) {
                $return[$key] = str_replace(" ", "_", $_FILES[$key]['name']);
            }
        }
        return $return;
    }

    private function _validate_form() {
        $this->form_validation->set_rules('fk_agent', 'fk_agent', 'trim|required');
        $this->form_validation->set_rules('month', 'month', 'trim|required');
        $this->form_validation->set_rules('year', 'year', 'trim|required');
        $this->form_validation->set_rules('value', 'value', 'trim');
        $this->form_validation->set_rules('ess', 'ess', 'trim');
        $this->form_validation->set_rules('mcp', 'mcp', 'trim');
        $this->form_validation->set_rules('total', 'total', 'trim');
        $this->form_validation->set_rules('previous_summary', 'previous_summary', 'trim');
        $this->form_validation->set_rules('adjust', 'adjust', 'trim');
        $this->form_validation->set_rules('type', 'type', 'trim');
        $this->form_validation->set_rules('value_paid', 'value_paid', 'trim');
    }

    private function _fill_model() {
        $this->Accounting_model->_pk_accounting    = $this->input->post('pk_accounting');
        $this->Accounting_model->_fk_agent         = $this->input->post('fk_agent');
        $this->Accounting_model->_month            = $this->input->post('month');
        $this->Accounting_model->_year             = $this->input->post('year');
        $this->Accounting_model->_value            = str_replace(",", ".", $this->input->post('value'));
        $this->Accounting_model->_ess              = str_replace(",", ".", $this->input->post('ess'));
        $this->Accounting_model->_mcp              = str_replace(",", ".", $this->input->post('mcp'));
        $this->Accounting_model->_total            = str_replace(",", ".", $this->input->post('total'));
        $this->Accounting_model->_previous_summary = str_replace(",", ".", $this->input->post('previous_summary'));
        $this->Accounting_model->_adjust           = str_replace(",", ".", $this->input->post('adjust'));
        $this->Accounting_model->_type             = $this->input->post('type');
        $this->Accounting_model->_value_paid       = str_replace(",", ".", $this->input->post('value_paid'));
        $this->Accounting_model->_status           = $this->Accounting_model->_status_active;
        $this->Accounting_model->_created_at       = date("Y-m-d H:m:s");
    }

    private function _process_agents() {
        $this->load->model("Agent_model");
        $this->Agent_model->_status = $this->Agent_model->_status_active;
        $agents                     = $this->Agent_model->fetch();
        $option                     = array();
        if ($agents) {
            foreach ($agents as $agent) {
                $option[$agent['type']][] = $agent;
            }
        }
        return $option;
    }

    function ajax_find() {
        $return                            = false;
        $this->disableLayout               = TRUE;
        $this->output->set_content_type('application/json');
        $this->Accounting_model->_fk_agent = $this->input->post('fk_agent');
        $this->Accounting_model->_month    = $this->input->post('month');
        $this->Accounting_model->_year     = $this->input->post('year');
        $this->Accounting_model->_status   = $this->Accounting_model->_status_active;
        $read                              = $this->Accounting_model->read();
        if ($read) {
            $return['accounting']                                 = $this->Accounting_model;
            //pegar os ids das unidades que estão marcadas para fazer rateio
            $this->load->model("Accounting_apportionment_model");
            $this->Accounting_apportionment_model->_fk_accounting = $this->Accounting_model->_pk_accounting;
            $this->Accounting_apportionment_model->_status        = $this->Accounting_apportionment_model->_status_active;
            $return['accounting_apportionment']                   = $this->Accounting_apportionment_model->fetch();
        }
        print_r(json_encode($return));
    }

    private function _process_accounting_apportionment($pk_accounting) {
        $this->load->model("Accounting_apportionment_model");
        $this->Accounting_apportionment_model->_fk_accounting = $pk_accounting;
        $this->Accounting_apportionment_model->delete();

        $this->Accounting_apportionment_model->_status     = $this->Accounting_apportionment_model->_status_active;
        $this->Accounting_apportionment_model->_created_at = date("Y-m-d H:i:s");
        foreach ($this->input->post('chk') as $fk_unity) {
            $this->Accounting_apportionment_model->_fk_unity = (int) $fk_unity;
            $this->Accounting_apportionment_model->create();
        }
    }

    private function _post_mail($step, $accounting, $to = null) {
        $this->load->clear_vars();
        
        $data['accounting'] = $accounting[0];

        switch ($step) {
            case "aporte":
                $data['subject'] = "Aporte de Garantia Financeira";
                //carrega a view
                $msg             = $this->load->view('accounting/email/warranty', $data, true);
                //carrega os anexos
                if ($data['accounting']["rel_gfn001"] != "") {
                    $data['files'][] = ('uploads/accounting/' . $data['accounting']["pk_accounting"] . '/' . $data['accounting']["rel_gfn001"]);
                }
                if ($data['accounting']["mem_gfn001"] != "") {
                    $data['files'][] = ('uploads/accounting/' . $data['accounting']["pk_accounting"] . '/' . $data['accounting']["mem_gfn001"]);
                }
                break;
            case "sumario":
                $data['subject'] = "Sumário da contabilização na CCEE";
                $msg             = $this->load->view('accounting/email/summary', $data, true);
                //carrega os anexos
                if ($data['accounting']["sum001"] != "") {
                    $data['files'][] = ('uploads/accounting/' . $data['accounting']["pk_accounting"] . '/' . $data['accounting']["sum001"]);
                }
                break;
            case "efetivacao":
                $data['subject'] = "Efetivação da Liquidação Financeira na CCEE";
                $msg             = $this->load->view('accounting/email/liquidation', $data, true);
                //carrega os anexos
                if ($data['accounting']["lfn002"] != "") {
                    $data['files'][] = ('uploads/accounting/' . $data['accounting']["pk_accounting"] . '/' . $data['accounting']["lfn002"]);
                }
                break;
        }

        $post['emails'] = is_null($to) ? $this->Contact_model->_email : (is_array($to) ? join(",", $to) : $to);
        $post['subject'] = $data['subject'];
        $post['message'] = base64_encode($msg);
        if (isset($data['files']) && !empty($data['files'])) {
            $post['files'] = join(",", $data['files']);
        }

        $pymail = pymail($post);
    }

}
