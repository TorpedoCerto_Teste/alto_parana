<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/**
 * Funcao de debug
 */
if (!function_exists('easyPrint')) {

    function easyPrint($data, $die = 0)
    {
        print "<pre>";
        print_r($data);
        print "</pre>";
        $die == 1 ? die : "";
    }
}

if (!function_exists('pd')) {
    function dd(...$args)
    {
        print "<pre>";
        print_r($args);
        die;
    }
}

if (!function_exists('pp')) {
    function pp(...$args)
    {
        print "<pre>";
        print_r($args);
    }
}


/**
 * Converte uma data humana: 20/01/1980 em data 1980-01-20.
 * Caso a data nao esteja no formato: dd/mm/YYYY, o sistema retorna a data atual
 * 
 * @param string $date
 * @return date
 */
if (!function_exists('human_date_to_date')) {

    function human_date_to_date($date, $null = 0)
    {
        $date = trim($date);
        $pattern = '/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/([12][0-9]{3}|[0-9]{2})$/';
        if (preg_match($pattern, $date)) {
            $h_date = explode("/", $date);
            $year   = $h_date[2] < 100 ? '20' . $h_date[2] : $h_date[2];
            return $year . "-" . $h_date[1] . "-" . $h_date[0];
        } else {
            if ($null == 0) {
                return date("Y-m-d");
            } else {
                return NULL;
            }
        }
    }
}


/**
 * Converte uma data de timestamp para dd/mm/YYYY
 * 
 * @param string $date
 * @return string
 */
if (!function_exists('date_to_human_date')) {

    function date_to_human_date($date, $current = false)
    {
        if (trim($date) != "" && preg_match('/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/', $date)) {
            return date("d/m/Y", strtotime($date));
        } elseif ($current) {
            return date("d/m/Y");
        }
        return "";
    }
}



/**
 * Funcao de validacao de CPF
 * @param type $cpf
 * @return boolean
 */
if (!function_exists('validar_cpf')) {

    function validar_cpf($cpf)
    { // Verifiva se o número digitado contém todos os digitos
        $cpf = str_pad(preg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);

        // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return false;
        } else {   // Calcula os números para verificar se o CPF é verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf[$c] != $d) {
                    return false;
                }
            }
            return true;
        }
    }
}


if (!function_exists('retorna_mes')) {

    function retorna_mes($mes)
    {
        switch ($mes + 0) {
            case 1:
                return 'Janeiro';
            case 2:
                return 'Fevereiro';
            case 3:
                return 'Março';
            case 4:
                return 'Abril';
            case 5:
                return 'Maio';
            case 6:
                return 'Junho';
            case 7:
                return 'Julho';
            case 8:
                return 'Agosto';
            case 9:
                return 'Setembro';
            case 10:
                return 'Outubro';
            case 11:
                return 'Novembro';
            case 12:
                return 'Dezembro';
        }
    }
}

if (!function_exists('retorna_estados')) {

    function retorna_estados()
    {
        return array(
            'AC' => 'ACRE',
            'AL' => 'ALAGOAS',
            'AP' => 'AMAPÁ',
            'AM' => 'AMAZONAS',
            'BA' => 'BAHIA',
            'CE' => 'CEARÁ',
            'DF' => 'DISTRITO FEDERAL',
            'ES' => 'ESPÍRITO SANTO',
            'GO' => 'GOIÁS',
            'MA' => 'MARANHÃO',
            'MT' => 'MATO GROSSO',
            'MS' => 'MATO GROSSO DO SUL',
            'MG' => 'MINAS GERAIS',
            'PA' => 'PARÁ',
            'PB' => 'PARAÍBA',
            'PR' => 'PARANÁ',
            'PE' => 'PERNAMBUCO',
            'PI' => 'PIAUÍ',
            'RJ' => 'RIO DE JANEIRO',
            'RN' => 'RIO GRANDE DO NORTE',
            'RS' => 'RIO GRANDE DO SUL',
            'RO' => 'RONDÔNIA',
            'RR' => 'RORAIMA',
            'SC' => 'SANTA CATARINA',
            'SP' => 'SÃO PAULO',
            'SE' => 'SERGIPE',
            'TO' => 'TOCANTINS'
        );
    }
}



if (!function_exists('get_array_from_object')) {

    function get_array_from_object($obj)
    {
        $return = array();
        foreach ($obj as $key => $value) {
            if ($value !== "") {
                $return[substr($key, 1)] = $value;
            }
        }
        unset($return['status_active']);
        unset($return['status_inactive']);
        unset($return['status_deleted']);
        return $return;
    }
}

if (!function_exists('set_object_from_array')) {

    function set_object_from_array($array, $obj)
    {
        foreach ($array[0] as $key => $value) {
            if ($value !== "") {
                $obj["_" . $key] = $value;
            }
        }
        return $obj;
    }
}



/**
 * Returns the amount of weeks into the month a date is
 * @param $date a YYYY-MM-DD formatted date
 * @param $rollover The day on which the week rolls over
 */
if (!function_exists('getWeeks')) {

    function getWeeks($date, $rollover)
    {
        $cut    = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first     = strtotime($cut . "00");
        $elapsed   = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++) {
            $dayfind      = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if ($day == strtolower($rollover))
                $weeks++;
        }

        return $weeks;
    }
}


if (!function_exists('directory_copy')) {

    function directory_copy($source, $destination, $move = 0)
    {
        if (!copy($source, $destination)) {
            return false;
        } else {
            if ($move == 1) {
                unlink($source);
            }
            return true;
        }
    }
}

if (!function_exists('checkEmail')) {

    function checkEmail($email)
    {
        $pattern = preg_match("/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD", $email);
        $isValid = ($pattern == 1) ? true : false;
        return $isValid;
    }
}

if (!function_exists('check_dir_exists')) {

    function check_dir_exists($dir_name)
    {
        if (!is_dir($dir_name)) {
            mkdir($dir_name, 0777, true);
        }
    }
}


/**
 * Retorna a quantidade de horas de um mês, pela data informada
 * Caso houver alguma inconsistência, será devolvido 0.
 * 
 * @param str $date (dd/mm/yyyy)
 * @param str $estado
 * @return int 
 */
if (!function_exists('hours_of_month')) {

    function hours_of_month($date, $estado = "DF")
    {
        (int) $hours   = 0;
        $pattern = '/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/([12][0-9]{3}|[0-9]{2})$/';
        if (preg_match($pattern, $date)) {
            list($dd, $mm, $yyyy) = explode('/', $date);
            if (checkdate($mm, $dd, $yyyy)) {
                $qtd_dias                  = date("t", strtotime($yyyy . $mm . $dd));
                $hours                     = $qtd_dias * 24;
                $estados_com_horario_verao = array(
                    "MT",
                    "DF",
                    "GO",
                    "MS",
                    "ES",
                    "RJ",
                    "SP",
                    "SC",
                    "MG",
                    "PR",
                    "RS"
                );
                //MODIFICADO PARA NÃO ATRIBUIR NENHUM HORÁRIO DE VERÃO
                //                if ($mm == 2 || $mm == 10) {
                //                    if (in_array($estado, $estados_com_horario_verao)) {
                //                        $hours = $mm == 2 ? ++$hours : --$hours;
                //                    }
                //                }
            }
        }

        return $hours;
    }
}

if (!function_exists('sendgrid')) {

    function sendgrid($from, $destination, $subject, $message, $attachment = array())
    {
        require 'vendor/autoload.php';

        $email    = new \SendGrid\Mail\Mail();
        $email->setFrom($from['email'], $from['name']);
        $email->setSubject($subject);
        $email->addTo($destination['email'], $destination['responsible_name']);
        $email->addContent(
            "text/html",
            $message
        );

        if (!empty($attachment)) {
            foreach ($attachment as $attach) {
                $file_encoded = base64_encode(file_get_contents($attach));
                $email->addAttachment(
                    $file_encoded,
                    $attach,
                    "attachment"
                );
            }
        }

        $sendgrid = new \SendGrid('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');
        try {
            $send       = $sendgrid->send($email);
            $statusCode = $send->statusCode();
            if ($statusCode != 202) {
                throw new Exception($send->headers() . " " . $send->body());
            }
            return $statusCode;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            return false;
        }
    }
}

// Function to eliminate any caracter different to letters and numbers
if (!function_exists('slug_')) {

    function slug_($label)
    {
        $slug = strtolower($label);
        $slug = str_replace('(', ' ', $slug);
        $slug = str_replace(')', ' ', $slug);
        $slug = str_replace('/', ' ', $slug);
        $slug = str_replace('-', ' ', $slug);
        $slug = str_replace('À', 'a', $slug);
        $slug = str_replace('Á', 'a', $slug);
        $slug = str_replace('Â', 'a', $slug);
        $slug = str_replace('Ã', 'a', $slug);
        $slug = str_replace('Ä', 'a', $slug);
        $slug = str_replace('Å', 'a', $slug);
        $slug = str_replace('à', 'a', $slug);
        $slug = str_replace('á', 'a', $slug);
        $slug = str_replace('â', 'a', $slug);
        $slug = str_replace('ã', 'a', $slug);
        $slug = str_replace('ä', 'a', $slug);
        $slug = str_replace('å', 'a', $slug);
        $slug = str_replace('Ò', 'o', $slug);
        $slug = str_replace('Ó', 'o', $slug);
        $slug = str_replace('Ô', 'o', $slug);
        $slug = str_replace('Õ', 'o', $slug);
        $slug = str_replace('Ö', 'o', $slug);
        $slug = str_replace('Ø', 'o', $slug);
        $slug = str_replace('ò', 'o', $slug);
        $slug = str_replace('ó', 'o', $slug);
        $slug = str_replace('ô', 'o', $slug);
        $slug = str_replace('õ', 'o', $slug);
        $slug = str_replace('ö', 'o', $slug);
        $slug = str_replace('ø', 'o', $slug);
        $slug = str_replace('È', 'e', $slug);
        $slug = str_replace('É', 'e', $slug);
        $slug = str_replace('Ê', 'e', $slug);
        $slug = str_replace('Ë', 'e', $slug);
        $slug = str_replace('è', 'e', $slug);
        $slug = str_replace('é', 'e', $slug);
        $slug = str_replace('ê', 'e', $slug);
        $slug = str_replace('ë', 'e', $slug);
        $slug = str_replace('Ç', 'c', $slug);
        $slug = str_replace('ç', 'c', $slug);
        $slug = str_replace('Ì', 'i', $slug);
        $slug = str_replace('Í', 'i', $slug);
        $slug = str_replace('Î', 'i', $slug);
        $slug = str_replace('Ï', 'i', $slug);
        $slug = str_replace('ì', 'i', $slug);
        $slug = str_replace('í', 'i', $slug);
        $slug = str_replace('î', 'i', $slug);
        $slug = str_replace('ï', 'i', $slug);
        $slug = str_replace('Ù', 'u', $slug);
        $slug = str_replace('Ú', 'u', $slug);
        $slug = str_replace('Û', 'u', $slug);
        $slug = str_replace('Ü', 'u', $slug);
        $slug = str_replace('ù', 'u', $slug);
        $slug = str_replace('ú', 'u', $slug);
        $slug = str_replace('û', 'u', $slug);
        $slug = str_replace('ü', 'u', $slug);
        $slug = str_replace('ÿ', 'y', $slug);
        $slug = str_replace('Ñ', 'n', $slug);
        $slug = str_replace('ñ', 'n', $slug);
        $slug = str_replace('Č', 'c', $slug);
        $slug = str_replace('é', 'e', $slug);
        $slug = str_replace('ß', 's', $slug);
        $slug = str_replace('!', ' ', $slug);
        $slug = str_replace('"', ' ', $slug);
        $slug = str_replace('#', ' ', $slug);
        $slug = str_replace('$', ' ', $slug);
        $slug = str_replace('%', ' ', $slug);
        $slug = str_replace('&', ' ', $slug);
        $slug = str_replace("'", ' ', $slug);
        $slug = str_replace('(', ' ', $slug);
        $slug = str_replace(')', ' ', $slug);
        $slug = str_replace('*', ' ', $slug);
        $slug = str_replace('+', ' ', $slug);
        $slug = str_replace(',', ' ', $slug);
        $slug = str_replace('-', ' ', $slug);
        $slug = str_replace('.', ' ', $slug);
        $slug = str_replace('/', ' ', $slug);
        $slug = str_replace(':', ' ', $slug);
        $slug = str_replace(';', ' ', $slug);
        $slug = str_replace('<', ' ', $slug);
        $slug = str_replace('=', ' ', $slug);
        $slug = str_replace('>', ' ', $slug);
        $slug = str_replace('?', ' ', $slug);
        $slug = str_replace('@', ' ', $slug);
        $slug = str_replace('[', ' ', $slug);
        $slug = str_replace(']', ' ', $slug);
        $slug = str_replace('^', ' ', $slug);
        $slug = str_replace('_', ' ', $slug);
        $slug = str_replace('`', ' ', $slug);
        $slug = str_replace('{', ' ', $slug);
        $slug = str_replace('|', ' ', $slug);
        $slug = str_replace('}', ' ', $slug);
        $slug = str_replace('~', ' ', $slug);
        $slug = str_replace('€', ' ', $slug);
        $slug = str_replace('‚', ' ', $slug);
        $slug = str_replace('ƒ', ' ', $slug);
        $slug = str_replace('„', ' ', $slug);
        $slug = str_replace('…', ' ', $slug);
        $slug = str_replace('†', ' ', $slug);
        $slug = str_replace('‡', ' ', $slug);
        $slug = str_replace('ˆ', ' ', $slug);
        $slug = str_replace('‰', ' ', $slug);
        $slug = str_replace('Š', ' ', $slug);
        $slug = str_replace('‹', ' ', $slug);
        $slug = str_replace('Œ', ' ', $slug);
        $slug = str_replace('Ž', ' ', $slug);
        $slug = str_replace('‘', ' ', $slug);
        $slug = str_replace('’', ' ', $slug);
        $slug = str_replace('“', ' ', $slug);
        $slug = str_replace('”', ' ', $slug);
        $slug = str_replace('•', ' ', $slug);
        $slug = str_replace('–', ' ', $slug);
        $slug = str_replace('—', ' ', $slug);
        $slug = str_replace('˜', ' ', $slug);
        $slug = str_replace('™', ' ', $slug);
        $slug = str_replace('š', ' ', $slug);
        $slug = str_replace('›', ' ', $slug);
        $slug = str_replace('œ', ' ', $slug);
        $slug = str_replace('ž', ' ', $slug);
        $slug = str_replace('¡', ' ', $slug);
        $slug = str_replace('¢', ' ', $slug);
        $slug = str_replace('£', ' ', $slug);
        $slug = str_replace('¤', ' ', $slug);
        $slug = str_replace('¥', ' ', $slug);
        $slug = str_replace('¦', ' ', $slug);
        $slug = str_replace('§', ' ', $slug);
        $slug = str_replace('¨', ' ', $slug);
        $slug = str_replace('©', ' ', $slug);
        $slug = str_replace('ª', ' ', $slug);
        $slug = str_replace('«', ' ', $slug);
        $slug = str_replace('¬', ' ', $slug);
        $slug = str_replace(' ', ' ', $slug);
        $slug = str_replace('®', ' ', $slug);
        $slug = str_replace('¯', ' ', $slug);
        $slug = str_replace('°', ' ', $slug);
        $slug = str_replace('±', ' ', $slug);
        $slug = str_replace('²', ' ', $slug);
        $slug = str_replace('³', ' ', $slug);
        $slug = str_replace('´', ' ', $slug);
        $slug = str_replace('µ', ' ', $slug);
        $slug = str_replace('¶', ' ', $slug);
        $slug = str_replace('·', ' ', $slug);
        $slug = str_replace('¸', ' ', $slug);
        $slug = str_replace('¹', ' ', $slug);
        $slug = str_replace('º', ' ', $slug);
        $slug = str_replace('»', ' ', $slug);
        $slug = str_replace('¼', ' ', $slug);
        $slug = str_replace('½', ' ', $slug);
        $slug = str_replace('¾', ' ', $slug);
        $slug = str_replace('¿', ' ', $slug);
        $slug = str_replace('À', 'a', $slug);
        $slug = str_replace('Á', 'a', $slug);
        $slug = str_replace('Â', 'a', $slug);
        $slug = str_replace('Ã', 'a', $slug);
        $slug = str_replace('Ä', 'a', $slug);
        $slug = str_replace('Å', 'a', $slug);
        $slug = str_replace('Æ', 'a', $slug);
        $slug = str_replace('Ç', 'c', $slug);
        $slug = str_replace('È', 'e', $slug);
        $slug = str_replace('É', 'e', $slug);
        $slug = str_replace('Ê', 'e', $slug);
        $slug = str_replace('Ë', 'e', $slug);
        $slug = str_replace('Ì', 'i', $slug);
        $slug = str_replace('Í', 'i', $slug);
        $slug = str_replace('Î', 'i', $slug);
        $slug = str_replace('Ï', 'i', $slug);
        $slug = str_replace('Ð', 'd', $slug);
        $slug = str_replace('Ñ', 'n', $slug);
        $slug = str_replace('Ò', 'o', $slug);
        $slug = str_replace('Ó', 'o', $slug);
        $slug = str_replace('Ô', 'o', $slug);
        $slug = str_replace('Õ', 'o', $slug);
        $slug = str_replace('Ö', 'o', $slug);
        $slug = str_replace('×', 'x', $slug);
        $slug = str_replace('Ø', 'o', $slug);
        $slug = str_replace('Ù', 'u', $slug);
        $slug = str_replace('Ú', 'u', $slug);
        $slug = str_replace('Û', 'u', $slug);
        $slug = str_replace('Ü', 'u', $slug);
        $slug = str_replace('Ý', 'y', $slug);
        $slug = str_replace('Þ', 'a', $slug);
        $slug = str_replace('ß', 'b', $slug);
        $slug = str_replace('à', 'a', $slug);
        $slug = str_replace('á', 'a', $slug);
        $slug = str_replace('â', 'a', $slug);
        $slug = str_replace('ã', 'a', $slug);
        $slug = str_replace('ä', 'a', $slug);
        $slug = str_replace('å', 'a', $slug);
        $slug = str_replace('æ', 'a', $slug);
        $slug = str_replace('ç', 'c', $slug);
        $slug = str_replace('è', 'e', $slug);
        $slug = str_replace('é', 'e', $slug);
        $slug = str_replace('ê', 'e', $slug);
        $slug = str_replace('ë', 'e', $slug);
        $slug = str_replace('ì', 'i', $slug);
        $slug = str_replace('í', 'i', $slug);
        $slug = str_replace('î', 'i', $slug);
        $slug = str_replace('ï', 'i', $slug);
        $slug = str_replace('ð', 'o', $slug);
        $slug = str_replace('ñ', 'n', $slug);
        $slug = str_replace('ò', 'o', $slug);
        $slug = str_replace('ó', 'o', $slug);
        $slug = str_replace('ô', 'o', $slug);
        $slug = str_replace('õ', 'o', $slug);
        $slug = str_replace('ö', 'o', $slug);
        $slug = str_replace('÷', ' ', $slug);
        $slug = str_replace('ø', 'o', $slug);
        $slug = str_replace('ù', 'u', $slug);
        $slug = str_replace('ú', 'u', $slug);
        $slug = str_replace('û', 'u', $slug);
        $slug = str_replace('ü', 'u', $slug);
        $slug = str_replace('ý', 'y', $slug);
        $slug = str_replace('þ', ' ', $slug);
        $slug = str_replace('ÿ', 'y', $slug);
        $slug = str_replace('Ā', 'a', $slug);
        $slug = str_replace('ā', 'a', $slug);
        $slug = str_replace('Ă', 'a', $slug);
        $slug = str_replace('ă', 'a', $slug);
        $slug = str_replace('Ą', 'a', $slug);
        $slug = str_replace('ą', 'q', $slug);
        $slug = str_replace('Ć', 'c', $slug);
        $slug = str_replace('ć', 'c', $slug);
        $slug = str_replace('Ĉ', 'c', $slug);
        $slug = str_replace('ĉ', 'c', $slug);
        $slug = str_replace('Ċ', 'c', $slug);
        $slug = str_replace('ċ', 'c', $slug);
        $slug = str_replace('Č', 'c', $slug);
        $slug = str_replace('č', 'c', $slug);
        $slug = str_replace('Ď', 'd', $slug);
        $slug = str_replace('ď', 'd', $slug);
        $slug = str_replace('Đ', 'd', $slug);
        $slug = str_replace('đ', 'd', $slug);
        $slug = str_replace('Ē', 'e', $slug);
        $slug = str_replace('ē', 'e', $slug);
        $slug = str_replace('Ĕ', 'e', $slug);
        $slug = str_replace('ĕ', 'e', $slug);
        $slug = str_replace('Ė', 'e', $slug);
        $slug = str_replace('ė', 'e', $slug);
        $slug = str_replace('Ę', 'e', $slug);
        $slug = str_replace('ę', 'e', $slug);
        $slug = str_replace('Ě', 'e', $slug);
        $slug = str_replace('ě', 'e', $slug);
        $slug = str_replace('Ĝ', 'g', $slug);
        $slug = str_replace('ĝ', 'g', $slug);
        $slug = str_replace('Ğ', 'g', $slug);
        $slug = str_replace('ğ', 'g', $slug);
        $slug = str_replace('Ġ', 'g', $slug);
        $slug = str_replace('ġ', 'g', $slug);
        $slug = str_replace('Ģ', 'g', $slug);
        $slug = str_replace('ģ', 'g', $slug);
        $slug = str_replace('Ĥ', 'h', $slug);
        $slug = str_replace('ĥ', 'h', $slug);
        $slug = str_replace('Ħ', 'h', $slug);
        $slug = str_replace('ħ', 'h', $slug);
        $slug = str_replace('Ĩ', 'i', $slug);
        $slug = str_replace('ĩ', 'i', $slug);
        $slug = str_replace('Ī', 'i', $slug);
        $slug = str_replace('ī', 'i', $slug);
        $slug = str_replace('Ĭ', 'i', $slug);
        $slug = str_replace('ĭ', 'i', $slug);
        $slug = str_replace('Į', 'i', $slug);
        $slug = str_replace('į', 'i', $slug);
        $slug = str_replace('İ', 'i', $slug);
        $slug = str_replace('ı', 'i', $slug);
        $slug = str_replace('Ĳ', 'i', $slug);
        $slug = str_replace('ĳ', 'j', $slug);
        $slug = str_replace('Ĵ', 'j', $slug);
        $slug = str_replace('ĵ', 'j', $slug);
        $slug = str_replace('Ķ', 'k', $slug);
        $slug = str_replace('ķ', 'k', $slug);
        $slug = str_replace('ĸ', 'k', $slug);
        $slug = str_replace('Ĺ', 'l', $slug);
        $slug = str_replace('ĺ', 'l', $slug);
        $slug = str_replace('Ļ', 'l', $slug);
        $slug = str_replace('ļ', 'l', $slug);
        $slug = str_replace('Ľ', 'l', $slug);
        $slug = str_replace('ľ', 'l', $slug);
        $slug = str_replace('Ŀ', 'l', $slug);
        $slug = str_replace('ŀ', 'l', $slug);
        $slug = str_replace('Ł', 'l', $slug);
        $slug = str_replace('ł', 'l', $slug);
        $slug = str_replace('Ń', 'n', $slug);
        $slug = str_replace('ń', 'n', $slug);
        $slug = str_replace('Ņ', 'n', $slug);
        $slug = str_replace('ņ', 'n', $slug);
        $slug = str_replace('Ň', 'n', $slug);
        $slug = str_replace('ň', 'n', $slug);
        $slug = str_replace('ŉ', 'n', $slug);
        $slug = str_replace('Ŋ', 'n', $slug);
        $slug = str_replace('ŋ', 'n', $slug);
        $slug = str_replace('Ō', 'o', $slug);
        $slug = str_replace('ō', 'o', $slug);
        $slug = str_replace('Ŏ', 'o', $slug);
        $slug = str_replace('ŏ', 'o', $slug);
        $slug = str_replace('Ő', 'o', $slug);
        $slug = str_replace('ő', 'o', $slug);
        $slug = str_replace('Œ', 'o', $slug);
        $slug = str_replace('œ', 'o', $slug);
        $slug = str_replace('Ŕ', 'r', $slug);
        $slug = str_replace('ŕ', 'r', $slug);
        $slug = str_replace('Ŗ', 'r', $slug);
        $slug = str_replace('ŗ', 'r', $slug);
        $slug = str_replace('Ř', 'r', $slug);
        $slug = str_replace('ř', 'r', $slug);
        $slug = str_replace('Ś', 's', $slug);
        $slug = str_replace('ś', 's', $slug);
        $slug = str_replace('Ŝ', 's', $slug);
        $slug = str_replace('ŝ', 's', $slug);
        $slug = str_replace('Ş', 's', $slug);
        $slug = str_replace('ş', 's', $slug);
        $slug = str_replace('Š', 's', $slug);
        $slug = str_replace('š', 's', $slug);
        $slug = str_replace('Ţ', 't', $slug);
        $slug = str_replace('ţ', 't', $slug);
        $slug = str_replace('Ť', 't', $slug);
        $slug = str_replace('ť', 't', $slug);
        $slug = str_replace('Ŧ', 't', $slug);
        $slug = str_replace('ŧ', 't', $slug);
        $slug = str_replace('Ũ', 'u', $slug);
        $slug = str_replace('ũ', 'u', $slug);
        $slug = str_replace('Ū', 'u', $slug);
        $slug = str_replace('ū', 'u', $slug);
        $slug = str_replace('Ŭ', 'u', $slug);
        $slug = str_replace('ŭ', 'u', $slug);
        $slug = str_replace('Ů', 'u', $slug);
        $slug = str_replace('ů', 'u', $slug);
        $slug = str_replace('Ű', 'u', $slug);
        $slug = str_replace('ű', 'u', $slug);
        $slug = str_replace('Ų', 'v', $slug);
        $slug = str_replace('ų', 'v', $slug);
        $slug = str_replace('Ŵ', 'w', $slug);
        $slug = str_replace('ŵ', 'w', $slug);
        $slug = str_replace('Ŷ', 'y', $slug);
        $slug = str_replace('ŷ', 'y', $slug);
        $slug = str_replace('Ÿ', 'y', $slug);
        $slug = str_replace('Ź', 'z', $slug);
        $slug = str_replace('ź', 'z', $slug);
        $slug = str_replace('Ż', 'z', $slug);
        $slug = str_replace('ż', 'z', $slug);
        $slug = str_replace('Ž', 'z', $slug);
        $slug = str_replace('ž', 'z', $slug);
        $slug = str_replace('ſ', ' ', $slug);
        $slug = str_replace('ƀ', 'b', $slug);
        $slug = str_replace('Ɓ', 'b', $slug);
        $slug = str_replace('Ƃ', 'b', $slug);
        $slug = str_replace('ƃ', 'b', $slug);
        $slug = str_replace('Ƅ', 'b', $slug);
        $slug = str_replace('ƅ', 'b', $slug);
        $slug = str_replace('Ɔ', 'c', $slug);
        $slug = str_replace('Ƈ', 'c', $slug);
        $slug = str_replace('ƈ', 'c', $slug);
        $slug = str_replace('Ɖ', 'd', $slug);
        $slug = str_replace('Ɗ', 'd', $slug);
        $slug = str_replace('Ǎ', 'a', $slug);
        $slug = str_replace('ǎ', 'a', $slug);
        $slug = str_replace('Ǐ', 'i', $slug);
        $slug = str_replace('ǐ', 'i', $slug);
        $slug = str_replace('Ǒ', 'o', $slug);
        $slug = str_replace('ǒ', 'o', $slug);
        $slug = str_replace('Ǔ', 'u', $slug);
        $slug = str_replace('ǔ', 'u', $slug);
        $slug = str_replace('Ǖ', 'u', $slug);
        $slug = str_replace('ǖ', 'u', $slug);
        $slug = str_replace('Ǘ', 'u', $slug);
        $slug = str_replace('ǘ', 'u', $slug);
        $slug = str_replace('Ǚ', 'u', $slug);
        $slug = str_replace('ǚ', 'u', $slug);
        $slug = str_replace('Ǜ', 'u', $slug);
        $slug = str_replace('ǜ', 'u', $slug);
        $slug = str_replace('ǝ', 'e', $slug);
        $slug = str_replace('Ǟ', 'a', $slug);
        $slug = str_replace('ǟ', 'a', $slug);
        $slug = str_replace('Ǡ', 'a', $slug);
        $slug = str_replace('ǡ', 'a', $slug);
        $slug = str_replace('Ǣ', 'a', $slug);
        $slug = str_replace('ǣ', 'a', $slug);
        $slug = str_replace('Ǥ', 'g', $slug);
        $slug = str_replace('ǥ', 'g', $slug);
        $slug = str_replace('Ǧ', 'g', $slug);
        $slug = str_replace('ǧ', 'g', $slug);
        $slug = str_replace('Ǩ', 'k', $slug);
        $slug = str_replace('ǩ', 'k', $slug);
        $slug = str_replace('Ǫ', 'o', $slug);
        $slug = str_replace('ǫ', 'o', $slug);
        $slug = str_replace('Ǭ', 'o', $slug);
        $slug = str_replace('ǭ', 'o', $slug);
        $slug = str_replace('ǰ', 'j', $slug);
        $slug = str_replace('Ǵ', 'g', $slug);
        $slug = str_replace('ǵ', 'g', $slug);
        $slug = str_replace('Ƿ', 'p', $slug);
        $slug = str_replace('Ǹ', 'n', $slug);
        $slug = str_replace('ǹ', 'n', $slug);
        $slug = str_replace('Ǻ', 'a', $slug);
        $slug = str_replace('ǻ', 'a', $slug);
        $slug = str_replace('Ǽ', 'a', $slug);
        $slug = str_replace('ǽ', 'a', $slug);
        $slug = str_replace('Ǿ', 'o', $slug);
        $slug = str_replace('ǿ', 'o', $slug);
        $slug = str_replace('Ȁ', 'a', $slug);
        $slug = str_replace('ȁ', 'a', $slug);
        $slug = str_replace('Ȃ', 'a', $slug);
        $slug = str_replace('ȃ', 'a', $slug);
        $slug = str_replace('Ȅ', 'e', $slug);
        $slug = str_replace('ȅ', 'e', $slug);
        $slug = str_replace('Ȇ', 'e', $slug);
        $slug = str_replace('ȇ', 'e', $slug);
        $slug = str_replace('Ȉ', 'i', $slug);
        $slug = str_replace('ȉ', 'i', $slug);
        $slug = str_replace('Ȋ', 'i', $slug);
        $slug = str_replace('ȋ', 'i', $slug);
        $slug = str_replace('Ȍ', 'o', $slug);
        $slug = str_replace('ȍ', 'o', $slug);
        $slug = str_replace('Ȏ', 'o', $slug);
        $slug = str_replace('ȏ', 'o', $slug);
        $slug = str_replace('Ȑ', 'r', $slug);
        $slug = str_replace('ȑ', 'r', $slug);
        $slug = str_replace('Ȓ', 'r', $slug);
        $slug = str_replace('ȓ', 'r', $slug);
        $slug = str_replace('Ȕ', 'u', $slug);
        $slug = str_replace('ȕ', 'u', $slug);
        $slug = str_replace('Ȗ', 'u', $slug);
        $slug = str_replace('ȗ', 'u', $slug);
        $slug = str_replace('Ȧ', 'a', $slug);
        $slug = str_replace('ȧ', 'a', $slug);
        $slug = str_replace('Ȩ', 'e', $slug);
        $slug = str_replace('ȩ', 'e', $slug);
        $slug = str_replace('Ȫ', 'o', $slug);
        $slug = str_replace('ȫ', 'o', $slug);
        $slug = str_replace('Ȭ', 'o', $slug);
        $slug = str_replace('ȭ', 'o', $slug);
        $slug = str_replace('Ȯ', 'o', $slug);
        $slug = str_replace('ȯ', 'o', $slug);
        $slug = str_replace('Ȱ', 'o', $slug);
        $slug = str_replace('ȱ', 'o', $slug);
        $slug = str_replace('Ȳ', 'y', $slug);
        $slug = str_replace('ȳ', 'y', $slug);
        $slug = str_replace('  ', ' ', $slug);
        $slug = preg_replace('/\s+/', ' ', $slug);
        $slug = trim($slug);
        $slug = stripslashes($slug);
        return $slug;
    }
}

if (!function_exists('slug')) {
    function slug($label)
    {

        // Convert the string to lowercase
        $slug = mb_strtolower($label, 'UTF-8');

        // Replace accented characters with their non-accented equivalents
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $slug);

        // Remove any character that is not a letter, number, or space
        $slug = preg_replace('/[^a-z0-9\s]/', '', $slug);

        // Replace multiple spaces with a single space
        $slug = preg_replace('/\s+/', ' ', $slug);

        // Trim spaces from the beginning and end
        $slug = trim($slug);

        // Replace spaces with hyphens
        $slug = str_replace(' ', '-', $slug);

        return $slug;
    }
}

if (!function_exists('pymail')) {

    function pymail($post)
    {
        $CI =& get_instance();
        $CI->load->config('email_config');
        $post['reply_to'] = is_array($CI->config->item('reply_to')) ? join(",", $CI->config->item('reply_to')) : $CI->config->item('reply_to');
        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => "https://sendmail.altoparanaenergia.com.br/sendmail",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $post
            )
        );

        $send = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($send, true);
        return $response;
    }
}
