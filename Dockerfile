FROM php:7.2-apache

WORKDIR /var/www/html
RUN apt update

RUN apt-get update && apt-get install -y python3 python3-pip
COPY src/application/third_party/email/requirements.txt /var/www/html
RUN pip3 install -r requirements.txt

RUN docker-php-ext-install mysqli
RUN a2enmod rewrite